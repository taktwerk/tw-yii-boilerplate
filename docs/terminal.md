# Terminal

The boilerplate comes with two `web terminals`. These solutions only work on linux based servers.

The `web shell` allows executing yii console commands directly.

The `web console` allows a full `pipe_exec` environment.