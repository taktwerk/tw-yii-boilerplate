# Taktwerk Model

### Show/Hide a field based on other field value on Create and Update form
JS Library Repository: [https://github.com/bomsn/mf-conditional-fields](https://github.com/bomsn/mf-conditional-fields)
 - Override formDependentShowFields function in the model
 - In the method you need to mention the dependent field(attribute) + it's html tag id and the field and it's html tag id and then the array of values on which the dependent field is displayed, it shoud be like in the below example
 
 ```php
 public function formDependentShowFields()
    {
        $dependentFields = [];
        $dependentFields = [
            'end_date' => $this->generateDependentFieldRules('end_date', [
				            "field" => "Project[end_date]", //name of the field in the html form
				            "container" => ".form-group-block", //field container selector
				            "rules" => [
				                [
				                    "name" => "Project[has_end_date]",
				                    "operator" => "is", //condition/check is passed if field value is same as given in this rule
				                    "value" => "yes"
				                ]
				            ]
        					]),
		'category' => $this->generateDependentFieldRules('category', [
			            "field" => "Project[category]",
			            "container" => ".form-group-block",
			            "rules" => [
			                [
			                    "name" => "Project[classification_id]",
			                    "operator" => "contains", //condition/check is passed if the field value is among the value provided in the rule.
			                    "value" =>  [1,2,10] //if the Project[classification_id] field has any values from this array then category field will show up 
			                ]
			            ],
		//Below is a rule with multiple conditions check and if the condition matches then the field hides.
		'recruitment' => $this->generateDependentFieldRules('recruitment', [
				            "field" => "Project[recruitment]",
				            "container" => ".form-group-block",
				            "action" => "hide" //on passing the condition the field hides
				            "rules" => [
				                [
				                    "name" => "Project[type_id]",
				                    "operator"=>"isnot",
				                    "value"=> "fundamental_research"
				                ],
				                [
				                    "name"=>"Project[status_id]",
				                    "operator"=>"is",
				                    "value"=>"active"
				                ],
				            ]
				        ],true)
                
        ];
        return $dependentFields;
    } 
```

In the above example the *end_date* field is displayed when <i>has_end_date</i> of the *Project* model has value "yes". In the next one category field is displayed when classification_id has value among the given array i.e [1,2,10]. The third attribute *recruitment* is a little diffrent from the previous two, cuz first it has multiple conditions(rules) to pass and second instead of displaying the field hides on matching the condition, so until unless the condition is passed the field is displayed.
  

##### Fieldthere are two model attributes whose visiblity is dependent upon other field value 
the first one is *options* field which is visible when the *type* field has any value of the value from the given array i.e [1,2,3]. The last(6th) argument if passed <code>true</code> in the <code>generateDependentFieldRule()</code> method allows to set the dependent field required when visible.<br>
Similarly the <i>sect
The name attribute of the field you want to show/hide based on the provided rules.

##### Container
The conditional field parent element where you want to perform the hiding/showing action, leave empty to show/hide the field itself.

##### Action
  - show
  - hide

##### Logic

  - or ( ***meeting one of the rules is enough perform the action*** )
  - and ( ***all the rules must be met to perform the action*** )

##### Rules
This should contain the rules you want to meet before showing/hiding the field. The rules can accept one rule in simple format `["name"=> "a", "operator"=> "is", "value"=> "yes"]` or multiple rules `[["name"=> "a", "operator"=> "is", "value"=> "yes"], ["name"=> "b", "operator"=> "is", "value"=> "no"]]`

##### Name
The name attribute of the parent field where the script should be listening for changes

##### Operators
Comparision operators to compare between the parent field value and the rule value

- is
- isnot
- greaterthan
- lessthan
- contains
- doesnotcontain
- beginswith
- doesnotbeginwith
- endswith
- doesnotendwith
- isempty
- isnotempty