# Backend

The `Backend` module represents part of the application that has limited access to it through the `en/backend` url.


### Controller Mapping

The backend module (\taktwerk\yiiboilerplate\modules\backend\Module) contains some default controller mappings in the property `$defaultControllerMap` which is merged with the `$controllerMap`. To add more mappings or to override the default mappings we can set the `$controllerMap` property

```php
'modules' => [
...,
		'backend' => [
            'class' => \taktwerk\yiiboilerplate\modules\backend\Module::class,
            'controllerMap' => [
                // ...
               'example' => 'app\modules\example\controllers\DefaultController',
            ],
        	],
        ]   
```