# Queue
The Queue is used to run intensive jobs in the background so that a user can continue using the application while processing.

A cron calls the queue every minute and searches for the oldest queue job.

## Configuration
The queue is automatically called every minute in the `@app/config/schedule.php` file of the `starter-kit`. In case it is missing, add the following command to it:
```php
$schedule->command('queue/process')->everyMinute();
```

or set up a cronjob
```cron
* * * * * php /path/to/yii queue/process >> /dev/null 2>&1
```

## Queue Interface
Create a new class that will contain the code to process for your queue. This class should `inherit` the `taktwerk\yiiboilerplate\modules\queue\commands\BackgroundCommandInterface` interface.

## Queuing a job
To queue a job, create a new `taktwerk\yiiboilerplate\modules\queue\models\QueueJob` model.

```php
$job = new \taktwerk\yiiboilerplate\modules\queue\models\QueueJob();
$job->queue(
    $titleOfJob,
    'class\That\Inherits\The\BackgroundCommandInterface',
    ['list' => 'of', 'parameters']
);
```