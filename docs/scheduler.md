# Scheduler

The `Scheduler` is a small utility that should be set up through a cronjob to run every minute.

Each project can have jobs running in the `src/config/schedule.php` file.

```php
/** 
@var $schedule taktwerk\yiiboilerplate\components\Schedule
*/

$schedule->command('some/process')->everyNminutes(60);
```

Setting up the cronjob should look like this:

```
* * * * * php /path/to/yii yii schedule/run >> /dev/null 2>&1
```

Each module in the project can have jobs running in the `config/schedule.php` file.  This needs to be called in project schedule config: 
    
    $schedule->getAllModuleSchedules();

To run scheduled tasks from another file, use the `--scheduleFile=@app/config/other.php` options.

Package: [https://github.com/omnilight/yii2-scheduling](https://github.com/omnilight/yii2-scheduling)
