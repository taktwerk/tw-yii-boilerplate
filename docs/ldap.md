# Ldap

The boilerplate can be set up to communicate with an LDAP server using some plugins and custom classes.

## Setup

> composer requir eedvlerblog/yii2-adldap-module:^4.0.0

Edit your `src/config/common.php` file and add the following blocks.

```php
return [
    'components' => [
        // LDAP
        'ldap' => [
            'class' => 'Edvlerblog\Adldap2\Adldap2Wrapper',
            'providers' => [
                'default' => [
                    // Connect this provider on initialisation of the LdapWrapper Class automatically
                    'autoconnect' => false,

                    // The config has to be defined as described in the Adldap2 documentation.
                    // https://github.com/Adldap2/Adldap2/blob/master/docs/configuration.md
                    'config' => [
                        // Your account suffix, for example: matthias.maderer@example.lan
                        'account_suffix' => "",

                        // You can use the host name or the IP address of your controllers.
                        'domain_controllers'    => ['tw-ldaptest.jcloud.ik-server.com'],

                        // Your base DN. This is usually your account suffix.
                        'base_dn'               => 'dc=planetexpress,dc=com',

                        // The account to use for querying / modifying users. This
                        // does not need to be an actual admin account.
                        'admin_username'        => 'cn=admin,dc=planetexpress,dc=com',
                        'admin_password'        => 'GoodNewsEveryone',

                        // To enable SSL/TLS read the docs/SSL_TLS_AD.md and uncomment
                        // the variables below
                        'port' => 11000,
                        //'use_ssl' => true,
                        //'use_tls' => true,
                    ]
                ],
            ],
        ],
    ],
    'params' => [
        'ldap' => [
            // Name of the default role for new users from LDAP, or false if a user without a valid group can't access
            'defaultRole' => 'Viewer',
            // sometimes we need the DN for auth
            'dn' => 'ou=people,dc=planetexpress,dc=com',
            // Parameter for the user login. cn for user name, uid for user id, sn for name etc
            'login' => 'uid',
            // The ldap attribute containing the fullname of the user for his profile
            'fullname' => 'displayname',
            // Mapping of ldap groups to local groups
            'roleMapping' => [
                'admin_staff' => 'Administrator',
                'ship_crew' => 'Viewer',
            ],
        ],
    ],
```

## Test

You can now navigate to `en/user/login-ldap` and try login in with `professor:professor`.