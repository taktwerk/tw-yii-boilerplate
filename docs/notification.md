# Notifications

## Configuration
The module is enabled by default in boilerplate, with default settings
to use Ajax request each 10 seconds to check for new notifications.
To change settings in config file do following:

```php
'modules' => [
    'notification' => [
        'class' => 'taktwerk\yiiboilerplate\modules\notification\Module',
        'useAjax' => false, // To disable Ajax request
        'refreshInterval' => 10, // Interval in seconds between Ajax requests
    ],
],
/**
 * Show import option
 * @var bool
 */
protected $importer = true;
```
## Sending notifications
In order to send notification, there is simple static method for this:
```php
taktwerk\yiiboilerplate\modules\notification\models\Notification::send($userId, $message);
```
where $userId is ID of user to which notification need to be sent,
and $message is text of notification.

# Tasks
Tasks in frontend are displayed based on QueueJob table.
Where there is new or running job in table (`STATUS_QUEUED` or `STATUS_RUNNING`) it will
show tasks in dropdown menu, along with progress for running tasks.
Background task itself need to update progress value in QueueJob table.
