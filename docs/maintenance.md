# Maintenance Mode

The app can be put into a maintenance mode which will great all users (except those in the `Authority` role). To activate, use the following command.

```
php yii maintenance/enable
```

And to disable:
```
php yii maintenance/disable
```

## Warning users about an upcoming downtime

Users can be notified regarding an upcoming migration by creating the following `settings key`: `maintenance.planned` as a boolean to `true`.

This adds a warning on every page. The content can be changed using the translation key `app`:`The application will be updated today at HH:II and won't be available for XX minutes.`.