Overriding Classes
==================

The taktwerk\yiiboilerplate\Module class makes use of the Yii2's Dependency Injection Container. The module has a special attribute named `classMap` where it allows you to override specific classes. 

The following are some classes that you can override(map) throughout that attribute: 

 - \taktwerk\yiiboilerplate\helpers\FileHelper
 - \taktwerk\yiiboilerplate\components\Helper
 - \taktwerk\yiiboilerplate\helpers\CrudHelper
 - \taktwerk\yiiboilerplate\modules\user\helpers\ResponseHelper;

and many more 

How to Override
---------------

Now, to tell the module to use your class instead, you simply need to update the definition of that class into the 
the `Module::classMap` attribute.

```php

// ...
 
'modules' => [
    'user' => [
        'class' => \taktwerk\yiiboilerplate\Module::class,
        'classMap' => [
            \taktwerk\yiiboilerplate\helpers\CrudHelper::class => \app\components\Helper::class,
            '\taktwerk\yiiboilerplate\helpers\FileHelper' => '\app\components\Helper',
        ]
    ]
]

```


To Make a class mappable you must access using method `getMappedClass` of class `ClassDispenser` e.g<br>

`\taktwerk\yiiboilerplate\components\ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\components\Helper::class)`

or to create an instance use `makeObject` method like below<br>


`\taktwerk\yiiboilerplate\components\ClassDispenser::makeObject(QrCode::class);`
 


