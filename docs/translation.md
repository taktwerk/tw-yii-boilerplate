# Taktwerk Translation conventions

To make a model or it's field support multiple languages perform the following steps( let say we are creating translation for table named **xyz**):
 1. create a table named xyz_translation , the naming convention will be like this first part of the name will be the actual table name whose translation table we are creating in our case it's xyz and the second part wil have **_translation** postfix.
 2. The fields in the xyz_translation table will be follow
     `id` int(11) NOT NULL,
      `xyz_id` int(11) DEFAULT NULL => **with foreign key to xyz(id)**,
      `language_id` varchar(5)  => **with foreign key to language(language_id)**,
      `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
      `created_by` int(11) DEFAULT NULL,
      `created_at` datetime DEFAULT NULL,
      `updated_by` int(11) DEFAULT NULL,
      `updated_at` datetime DEFAULT NULL,
      `deleted_by` int(11) DEFAULT NULL,
      `deleted_at` datetime DEFAULT NULL
3. Generate models for both xyz and xyz_translation tables using Gii's Tw-model or SchemaChecker in admin panel.
4. Generate CRUD of xyz table using Gii's Tw-model or SchemaChecker in admin panel.
5. Make sure you have one or more active languages in **language** table
6. Open the create/update form of the **xyz** CRUD, you'll see the translation fields at the end of the form where you can enter the translation according to the languages. 
That's it