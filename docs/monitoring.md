# Monitoring

Any app or project using the boilerplate can be integrated with (pushbeat.io)[https://pushbeat.io] by providing a few simple tweaks.

## Config

Edit the project's `.env` file and add the following definition.

```
APP_PUSHBEAT_PING_URL = 'https://pushbeat.io/ping/{endpoint}
```

### Proxy

If requests are filtered through a proxy, you can set the curl's proxy setting with the following `.env` variable.

```
APP_CURL_PROXY = 'http://proxy@proxy'
```

## Scheduler

Set the following scheduling task in `src/config/schedule.php`.

```php
$schedule->command('monitoring/ping')->everyFiveMinutes();
```