# Theming

Theming is build in the boilerplate and can be easily enabled. 

## Setup
Themes can be added in the `main.php` params.

```php
'params' => [
    'themes' => [
        'mobile',
        'terminal'
    ]
]
```

Also in the `main.php`, add the following line in the `web` configuration.

```php
    // Theme swapping
    'as beforeAction' => [
        'class' => 'taktwerk\yiiboilerplate\behaviors\BeforeActionBehavior'
    ]
```

### Create a theme
Create a new folder in `app/themes/{theme}`. If your theme is activated, it will load the views from there before loading the standard views.

### Activate a theme
Each theme can be enabled by navigating to `site/theme-switch/{theme}`. This created a cookine names `applicationTheme` containing the selected theme, guaranteeing that the theme stays selected even on logout or session timeout.

### Reset
To remove the theme, navigate to `site/theme-switch/reset`. This will delete the theme.