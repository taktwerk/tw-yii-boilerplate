# Crawler

The `Crawler` is a small utility that will analyse your app's pages and check all internal links.

To call the crawler, execute

    php yii crawler --url=http://app.loc

## Authentication

To auth the user automatically on all requests, you can provide the following options

    php yii crawler --url=http://app.tw --username=admin --password=admin


## Limitations

The crawler won't call urls with the following segments:

* `delete`
* `block`
* `remove`
* `logout`
* `adminer`
* `/create?`
* `debug`
* `scan`
* `index?`
* `view?`
* `update?`
* `translate?`
* `sort=`

It will however call paginated grids.