# Single and Multi Files in BP

### Single file upload
To make a model's field support file uploading automatically from Gii generated CRUD, the field name should have '_upload' or '_file' as postfix; or should have field/column comment with value `{"inputtype":"upload"}` or `{"inputtype":"file"}`
Below is an example sql for table with single file upload support
```
CREATE TABLE `test_file` (
  `id` int(11) NOT NULL,
  `design` varchar(255) DEFAULT NULL COMMENT '{"inputtype":"upload"}',
  `upload_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pdf_upload` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL  
) ENGINE=InnoDB;
```

Now if you generate the model(Giiant Model) and CRUD(Giiant TW-CRUD) from Gii or SchemaChecker(admin panel) and then check the model's create/update form in browser you'll see the file upload option for the fields.

##### To get the uploaded file url 
 Just call the **getFileUrl()** method of the create model(UploadTrait) and pass the attribute/field_name which is assigned to file.
e.g `$fileUrl = $model->getFileUrl('upload_file');`

##### To get the uploaded file path 
 Just call the **getFilePath()** method of the create model(UploadTrait) and pass the attribute/field_name which is assigned to file.
e.g `$filePath = $model->getFilePath('upload_file');`


### Multi file upload

To make a model's field support muli-file uploading automatically from Gii generated CRUD, the field name should have '_media' as postfix.
Below is an example sql for table with multi-file upload support
```sql
CREATE TABLE `test_file` (
  `id` int(11) NOT NULL,
  `certificate_media` varchar(255) DEFAULT NULL
) ENGINE=InnoDB;
```

The **Media module must be active** in the App configuration add below code in common.php modules array to activate the BP media module
```php
'modules' => [
...
'media' => [
            'class' => 'taktwerk\yiiboilerplate\modules\media\Module',
        ],
        ...
]
```
Now if you generate the model(Giiant Model) and CRUD(Giiant TW-CRUD) from Gii or SchemaChecker(admin panel) and then check the model's create/update form in browser you'll see the multi-file upload option for the field. 
The multi file input widget `taktwerk\yiiboilerplate\modules\media\widgets\MultiFileInput`  used in the multi-file upload extends [Yii2 kartik File Input](https://github.com/kartik-v/yii2-widget-fileinput)  widget so the available options can be directly checked from the official Yii2 kartik File Input [documentation](https://demos.krajee.com/widget-details/fileinput).

To get the multiple files associate with a field like certificate_media
You'll first need to call the `getMediaFiles()` of the model and will have to pass the model type which will be in the model class constant called `MEDIA_MODEL_TYPE`   and the media category which will also be the model's class constant with name like `MEDIA_CATEGORY_CERTIFICATE_MEDIA`, This method will return an array of  `taktwerk\yiiboilerplate\modules\media\models\MediaFile` models, each model represent single file. and to access the file you can use the below example
```php
$mediaFiles = $testFileModel->getMediaFiles(TestFile::MEDIA_MODEL_TYPE, TestFile::MEDIA_CATEGORY_CERTIFICATE_MEDIA);
$filesUrl=[];
$filesPath=[];
foreach($mediaFiles as $mediaFile){
    $filesUrl[] = $mediaFile->getUrl();
    $filesPath[] = $mediaFile->getFilePath('file_name');
}

```