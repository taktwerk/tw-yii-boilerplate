# Search

The search module allows for a weighted search to be conducted on several tables.

## Setup

Edit your `app/config/common.php` and add the following module.
```php
'search' => [
    'class' => 'taktwerk\yiiboilerplate\modules\search\Module',
    'models' => [
        app\models\Address::class
    ]
],
```

Next, edit your model's definition and have it implement the following interface.

```php
use taktwerk\yiiboilerplate\modules\search\models\interfaces\SearchableInterface;
class Address extends BaseAddress implements SearchableInterface
{
    /**
     * @return string
     */
    public function searchableKey()
    {
        return 'address';
    }

    /**
     * @return array
     */
    public function searchableFields()
    {
        return [
            'firstname',
            'lastname'
        ];
    }
}
```

Now you can call the controller `/search/default?query=taktwerk` and enjoy the results.

## Layout

To change the way results are shown with a custom view, add `'layout' => '@app/view'` to the module's configuration in `src/config/common.php`.

## Limiting number of results

Each field executes a where query on that field and limits the results to `10` models. This value can be changed by adding the `limitPerQuery` option to the module initialisation in the `app/config/common.php` configuration file.

## Complexe Search

If your model requires complex searches, you can define a custom search method instead.

In your `model` class, add the following method.

```php
public function searchableCustom($query) {
    return self::find()->where(['field'  => $query]);
}
```

Do not include `->limit()` or `->all()` to the query.
