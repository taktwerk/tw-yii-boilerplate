# User

## Foreign Lookup
The boilerplate is set up in a way to allow foreign lookups on user login to link a prexisting user table with the yii user table. This is achieved by editing your main.php to add the following.

```php
'modules' => [
    'user' => [
        'foreignLookup' => [
            'lookupModel' => 'app\models\StammMitarbeiter',
            'lookupColumn' => 'mitanr',
            'lookupFullnameColumn' => 'mitana',
            'lookupPattern' => '/([0-9]+)06/',
            'lookupRole' => 'Factory',
        ]
    ]
]
```

**lookupModel**. Indicates the model class that defines the foreign table used to link up a user.

**lookupColumn**. The field that translates to Yii's `username` column.

**lookupFullnameColumn**. The field that translated to our `fullname` column. Can be left blank.

**lookupPattern**. This is a way to set up a pattern recognition between the username sent in the form and the lookup. For example for MSR, usernames are an internal id + 06 appended at the end. The pattern splits it so we can use just the id later on.

**lookupRole**. This is the role given to a user when he is created through this lookup mechanism.

## User Events
The Yii2 framework and Dektrium User module are prepared with various events that can trigger when a user does certain action, but it can be confusing setting up events properly. If for example you want to have an event on each user login, you must do the following.

First, edit the user component to add your new event binding.
```php
'components' => [
    'user' => [
        'on ' . \yii\web\User::EVENT_AFTER_LOGIN => ['app\modules\user\events\AfterLoginEvent', 'handleAfterLogin']
    ]
]
```

This will call the static `handleAfterLogin` in your Event class.

## Two Factor Authentication

`2FA` can be enabled on each project. Edit the `user` module in `config/common.php` and add the following.

```php
'twoWay' => [
    'enabled' => \taktwerk\yiiboilerplate\modules\user\Module::ENABLED,
    'expired_time' => 60 // Seconds until link for verification is valid (default 60)
]
```

### Options

The following options are available for the user module.

```php
\taktwerk\yiiboilerplate\modules\user\Module::ENABLED // enable 2-way globally for all users
\taktwerk\yiiboilerplate\modules\user\Module::DISABLED // disable 2-way
```

It is possible to active 2FA only for some users by providing the following configuration:

```php
\taktwerk\yiiboilerplate\modules\user\Module::USER // enable 2-way per user.
```

And in the `userParameter` module, add the following:
```php
'twoWayAuthentication' => [
    'name' => \Yii::t('twbp', 'Two Way Authentication'),
    'type' => \taktwerk\yiiboilerplate\modules\userParameter\Module::CHECKBOX,
    'default' => false,
],
```