#!/bin/bash

#DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

CMD="cp $PWD/vendor/taktwerk/yii-boilerplate/* $DIR/ -r"

printf "copy all but hidden files (with . in front)\n"
printf "$CMD\n"
eval $CMD