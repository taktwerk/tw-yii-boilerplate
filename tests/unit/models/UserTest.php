<?php

namespace taktwerk\yiiboilerplatetests\unit\models;

use app\models\User;
use taktwerk\yiiboilerplatetests\fixtures\UserImportFixtures;
use Codeception\Test\Unit;

class UserTest extends Unit
{

    public $appConfig = '@tests/codeception/_config/unit.php';

    public function _fixtures()
    {
        return [
            'User' => [
                'class' => UserImportFixtures::class,
                // fixture data located in tests/_data/user.php
                'dataFile' => codecept_data_dir() . 'user.php'
            ]
        ];
    }

    protected function setUp()
    {
        parent::setUp();
    }

    /**
     * @group mandatory
     */
    public function testUserLogin()
    {

        $identity = User::find()->one();
        $this->assertTrue(\Yii::$app->user->login($identity, 3600));
    }

//    /**
//     * @group mandatory
//     */
//    public function testNonExistingUserModel()
//    {
//        $identity = User::findIdentity(99999);
//        $this->assertNull($identity);
//    }
//
//    /**
//     * @group mandatory
//     */
//    public function testUserLogout()
//    {
//        $this->assertTrue(\Yii::$app->user->logout());
//    }
}
