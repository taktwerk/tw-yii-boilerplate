<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 6/30/2017
 * Time: 2:14 PM
 */

namespace tests\codeception\unit\models;

use Codeception\Test\Unit;

class YiiCommandTest extends Unit
{
    public $appConfig = '@tests/codeception/_config/unit.php';

    public function testYiiCommand()
    {
        $output = shell_exec('php yii');
        $this->assertContains('This is Yii version', $output);
        $this->assertContains('The following commands are available', $output);
        $this->assertNotContains('An Error occurred while', $output);
    }
}
