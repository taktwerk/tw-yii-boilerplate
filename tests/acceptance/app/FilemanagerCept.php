<?php

use tests\codeception\_pages\LoginPage;

$I = new AcceptanceTester($scenario);

$fixtures = new \tests\codeception\_support\FixtureHelper();
$fixtures->unloadFixtures();
$fixtures->loadFixtures();

$autoLogin = new \tests\codeception\_support\AutoLogin($I);
$autoLogin->adminLogin();

$I->wantTo('ensure Elfinder Filemanager is working');
$I->amOnPage('/elfinder/manager');

$I->canSeeElement('div', ['id' => 'elfinder']);

$fixtures->unloadFixtures();
