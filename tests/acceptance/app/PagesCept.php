<?php

use tests\codeception\_pages\LoginPage;

$I = new AcceptanceTester($scenario);

$fixtures = new \tests\codeception\_support\FixtureHelper();
$fixtures->unloadFixtures();
$fixtures->loadFixtures();

$autoLogin = new \tests\codeception\_support\AutoLogin($I);
$autoLogin->adminLogin();

$I->wantTo('ensure pages module is working');
$I->amOnPage('/backend');

if (Yii::$app->hasModule('pages')) {
    $I->seeInSource('<span>Pages</span>');
} else {
    $I->dontSeeInSource('<span>Pages</span>');
}

$fixtures->unloadFixtures();
