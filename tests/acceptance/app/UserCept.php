<?php

use tests\codeception\_pages\LoginPage;

$I = new AcceptanceTester($scenario);

$fixtures = new \tests\codeception\_support\FixtureHelper();
$fixtures->unloadFixtures();
$fixtures->loadFixtures();

$autoLogin = new \tests\codeception\_support\AutoLogin($I);
$autoLogin->adminLogin();

$I->wantTo('ensure user create is working');
$I->amOnPage('/user/admin/create');
$I->seeElement('button', ['type' => 'submit', 'class' => 'btn btn-block btn-success']);
$I->seeElement('input', ['name' => 'User[email]']);

$I->fillField(['name' => 'User[email]'], 'test@test.com');
$I->fillField(['name' => 'User[username]'], 'testUser');
$I->fillField(['name' => 'User[password]'], 'testPassword');

$I->click('button[type=submit]');

$user_id = $I->grabFromCurrentUrl('~update\?id=(\d+)~');

$I->amOnPage('/user/admin/index');
$I->seeInSource('test@test.com');

$I->wantTo('ensure update user is working');
$I->amOnPage('/user/admin/update?id=' . $user_id);
$I->seeElement('button', ['type' => 'submit', 'class' => 'btn btn-block btn-success']);

$I->fillField(['name' => 'User[email]'], 'new@test.com');
$I->fillField(['name' => 'User[username]'], 'changedTestUser');
$I->click('button[type=submit]');

$I->amOnPage('/user/admin/index');
$I->seeInSource('new@test.com');

$I->wantTo('ensure delete user is working');
//@TODO
$I->click('a[href="/en/user/admin/delete?id=' . $user_id .'"]');
$I->dontseeInSource('new@test.com');


$fixtures->unloadFixtures();
