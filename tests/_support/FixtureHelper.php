<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 6/14/2017
 * Time: 11:07 AM
 */

namespace tests\codeception\_support;

use yii\test\FixtureTrait;
use Codeception\TestCase;

class FixtureHelper
{
    use FixtureTrait;

    /**
     * @var array
     */
    public static $excludeActions = ['loadFixtures', 'unloadFixtures', 'getFixtures', 'globalFixtures', 'fixtures'];

    /**
     * @param TestCase $testcase
     */
    public function _before(TestCase $testcase)
    {
        $this->unloadFixtures();
        $this->loadFixtures();
        parent::_before($testcase);
    }

    /**
     * @param TestCase $testcase
     */
    public function _after(TestCase $testcase)
    {
        $this->unloadFixtures();
        parent::_after($testcase);
    }

    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'User' => [
                'class' => \tests\codeception\fixtures\UserFixtures::className(),
                'dataFile' =>  __DIR__ . '/../fixtures/data/user.php',
            ],
        ];
    }

}