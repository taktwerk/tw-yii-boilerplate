<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 6/15/2017
 * Time: 1:44 PM
 */

namespace tests\codeception\_support;

use tests\codeception\_pages\LoginPage;

class AutoLogin
{
    protected $actor;

    public function __construct($actor)
    {
        $this->actor = $actor;
    }

    public function adminLogin()
    {
        $this->goToPage();
        $loginPage = LoginPage::openBy($this->actor);
        $loginPage->login('admin', 'passepartout.0');
        $this->assert();
    }

    public function login($username, $password)
    {
        $this->goToPage();
        $loginPage = LoginPage::openBy($this->actor);
        $loginPage->login($username, $password);
        $this->assert();
    }

    protected function goToPage()
    {
        $this->actor->wantTo('login as admin');
        $this->actor->amGoingTo('try to login as admin');
        $this->actor->amOnPage('/');
    }

    protected function assert()
    {
        $this->actor->dontSeeElement('button', ['type' => 'submit']);
        $this->actor->dontSeeElement('input', ['name' => 'login-form[login]']);
        $this->actor->dontSeeElement('input', ['name' => 'login-form[password]']);
    }
}