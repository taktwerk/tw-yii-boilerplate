<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 6/5/2017
 * Time: 10:52 AM
 */

namespace tests\codeception\fixtures;

class UserFixtures extends ActiveFixture
{
    public $modelClass = 'app\models\User';
}