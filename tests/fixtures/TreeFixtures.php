<?php

namespace taktwerk\yiiboilerplatetests\fixtures;

class TreeFixtures extends ActiveFixture
{
    public $modelClass = 'taktwerk\pages\models\Tree';
}