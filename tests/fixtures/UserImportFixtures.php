<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 6/5/2017
 * Time: 10:52 AM
 */

namespace taktwerk\yiiboilerplatetests\fixtures;

class UserImportFixtures extends ActiveFixture
{
    public $modelClass = 'app\models\User';
}