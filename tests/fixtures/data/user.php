<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 6/5/2017
 * Time: 10:53 AM
 */

return [
    'User1' => [
        'username' => 'admin',
        'email' => 'email@email.com',
        'password_hash' => '$2y$10$WX97djHYdkIBSv1UaM5Ee.qAP7NN2QGaD06aGY2bCWdCkay40FEDG', //passepartout.0
        'auth_key' => 'auth_key 1',
        'confirmed_at' => '1',
        'unconfirmed_email' => 'unconfirmed_email 1',
        'blocked_at' => null,
        'registration_ip' => 'registration_ip 1',
        'flags' => '1',
        'fullname' => 'fullname 1',
        'last_login_at' => '1',
    ],
];
