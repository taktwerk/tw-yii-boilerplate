<?php
use taktwerk\pages\models\Tree;

return [
    'Tree1' => [
        'id' => '1',
        'root' => 'en',
        'name' => 'name 1',
        'domain_id' => Tree::ROOT_NODE_PREFIX,
        'active' => Tree::ACTIVE,
        'visible' => Tree::VISIBLE,
        'lft' => 0,
        'rgt' => 0,
        'lvl' => 0,
    ],
    'Tree2' => [

        'id' => '2',
        'root' => 'en',
        'name' => 'name 2',
        'domain_id' => Tree::ROOT_NODE_PREFIX,
        'active' => Tree::ACTIVE,
        'visible' => Tree::VISIBLE,
        'lft' => 0,
        'rgt' => 0,
        'lvl' => 0,
    ],
    'Tree3' => [

        'id' => '3',
        'root' => 'en',
        'name' => 'name 3',
        'domain_id' => Tree::ROOT_NODE_PREFIX,
        'active' => Tree::ACTIVE,
        'visible' => Tree::VISIBLE,
        'lft' => 0,
        'rgt' => 0,
        'lvl' => 0,
    ],
    'Tree4' => [

        'id' => '4',
        'root' => 'en',
        'name' => 'name 4',
        'domain_id' => Tree::ROOT_NODE_PREFIX,
        'active' => Tree::ACTIVE,
        'visible' => Tree::VISIBLE,
        'lft' => 0,
        'rgt' => 0,
        'lvl' => 0,
    ],
];
