<?php

// @group optional

use tests\codeception\_pages\LoginPage;

$I = new FunctionalTester($scenario);
$I->wantTo('ensure that login works');

$I->amGoingTo('try to login with correct credentials');
LoginPage::openBy($I)->login('admin', 'admin');
$I->expectTo('see user info');

$I->amOnPage('/');
$I->see('en/site/index', '.label');

// See the datagrid
$I->amOnPage('/taktwerk/pages/tree');
$I->see('New', '.btn-success');


// See the create form
$I->amOnPage('/taktwerk/pages/tree/create');
$I->see('Create', '.btn-success');

// Submit the form, view errors
$I->click('Create');
$I->seeElement('.has-error');
