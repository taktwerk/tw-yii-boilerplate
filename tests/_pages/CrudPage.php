<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 6/16/2017
 * Time: 8:34 AM
 */

namespace tests\codeception\_pages;

class CrudPage
{

    protected $url;
    protected $actor;

    public function __construct($actor)
    {
        $this->actor = $actor;
    }

    public function assertIndex($url)
    {
        $this->actor->wantTo('ensure index page works');

        $this->actor->amGoingTo('try index page');
        $this->actor->amOnPage($url);
        $this->actor->seeElement('div', ['class' => 'table-responsive kv-grid-container']);
    }

    public function assertCreate($url)
    {
        $this->actor->wantTo('ensure create page works');

        $this->actor->amGoingTo('try create page');
        $this->actor->amOnPage($url);
        $this->actor->seeElement('button', ['type' => 'submit', 'name' => 'submit-default']);
    }

}