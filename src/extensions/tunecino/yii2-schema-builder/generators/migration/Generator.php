<?php
namespace tunecino\builder\generators\migration;

use Yii;
use yii\db\TableSchema;
use tunecino\builder\models\Entity;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\TwMigration;
use yii\helpers\StringHelper;
use tunecino\builder\models\Attribute;
use tunecino\builder\models\Relationship;
use yii\db\Schema;

class Generator extends \tunecino\builder\Generator
{

    private $_toJunction = [];
    private $_toUpdateJunction = [];
    private function createdJunction($rel1, $rel2)
    {
        return (isset($this->_toJunction[$rel2]) && $this->_toJunction[$rel2] === $rel1) or (isset($this->_toJunction[$rel1]) && $this->_toJunction[$rel1] === $rel2);
    }
    private function createdUpdateJunction($rel1, $rel2)
    {
        return (isset($this->_toUpdateJunction[$rel2]) && $this->_toUpdateJunction[$rel2] === $rel1) or (isset($this->_toUpdateJunction[$rel1]) && $this->_toUpdateJunction[$rel1] === $rel2);
    }
    public function getCoreAttributes()
    {
        return [
            'db' => 'db',
            'migrationTable' => '{{%migration}}',
            'migrationPath' => '@root/build/schema-builder/migrations',
            'templateFile' => '@vendor/taktwerk/yii-boilerplate/src/views/migration/migration.php',
            'useTablePrefix' => false
        ];
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'db' => 'Migration Database',
            'migrationTable' => 'Migration Table',
            'migrationPath' => 'Migration Path',
            'templateFile' => 'Template File',
            'useTablePrefix' => 'Use Table Prefix'
        ]);
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                [
                    'db'
                ],
                'required'
            ],
            [
                [
                    'db'
                ],
                'match',
                'pattern' => '/^\w+$/',
                'message' => 'Only word characters are allowed.'
            ],
            [
                [
                    'db'
                ],
                'validateDb'
            ],
            [
                [
                    'migrationTable',
                    'migrationPath'
                ],
                'string'
            ], /*could be improved*/
            [
                [
                    'templateFile'
                ],
                'validateFilePath'
            ],
            [
                [
                    'useTablePrefix'
                ],
                'boolean'
            ]
        ]);
    }

    public function attributeHints()
    {
        return array_merge(parent::attributeHints(), [
            'db' => 'This is the ID of the DB application component that will be used by the migration tool.',
            'migrationTable' => 'The name of the table for keeping applied migration information.',
            'migrationPath' => 'The directory containing the migration classes. This can be either a path alias or a directory path.',
            'useTablePrefix' => 'Indicates whether the table names generated should consider the <code>tablePrefix</code> setting of the DB connection. For example, if the table name is <code>post</code> the generator wil return <code>{{%post}}</code>.'
        ]);
    }

   

    public function getMigrationFolder()
    {
        $path = Yii::getAlias($this->migrationPath) ?: $this->migrationPath;
        return $this->migrationPath . '/' . $this->schema->name;
    }

    /**
     *
     * @return array
     */
    protected function getMigrationClearCommands()
    {
        $migDownCmd = 'yii migrate/down all';
        if ($this->appconfig)
            $migDownCmd .= ' --appconfig="' . $this->appconfig . '"';
        if ($this->db)
            $migDownCmd .= ' --db="' . $this->db . '"';
        if ($this->migrationPath)
            $migDownCmd .= ' --migrationPath="' . $this->migrationFolder . '"';
        if ($this->migrationTable)
            $migDownCmd .= ' --migrationTable="' . $this->migrationTable . '"';
        $migDownCmd .= ' --interactive=0';
        
        // TW HACK START
        $tables = [];
        foreach ($this->schema->entities as $entity) {
            $tables[] = $entity->name;
            
            foreach ($entity->relationships as $relation) {
                if ($relation->isManyToMany() && $this->createdJunction($entity->name, $relation->relatedTo->name) === false) {
                    $tables[] = $entity->name . '_' . $relation->relatedTo->name;
                }
            }
        }
        
        //exit;
        // $dropDbCmd = 'yii '. Yii::$app->controller->module->id . '/helpers/drop-all-tables';
        // if ($this->db) $dropDbCmd .= ' '.$this->db;
        
        // TW HACK END
        
        $dropDbCmd = 'yii ' . Yii::$app->controller->module->id . '/helpers/drop-tables';
        if ($this->db)
            $dropDbCmd .= ' ' . $this->db;
        $dropDbCmd .= ' ' . implode(',', $tables);
        
        $flushDbCmd = 'yii cache/flush-schema ' . $this->db;
        if ($this->appconfig)
            $flushDbCmd .= ' --appconfig="' . $this->appconfig . '"';
        $flushDbCmd .= ' --interactive=0';
        
        $rmvDirectoryCmd = 'yii ' . Yii::$app->controller->module->id . '/helpers/remove-directory';
        if ($this->migrationPath)
            $rmvDirectoryCmd .= ' ' . $this->migrationFolder;
        
         
        return (file_exists($this->migrationFolder)) ? [
            $migDownCmd,
            $dropDbCmd,
            $flushDbCmd,
            $rmvDirectoryCmd
        ] : [
            $dropDbCmd,
            $flushDbCmd,
            $rmvDirectoryCmd
        ];
    }

    protected function getMigrationCreateCommands()
    {
        $commands =[];
        foreach ($this->schema->entities as $entity) {
            $commands = array_merge($commands,$this->createEntityMigration($entity));
        }
        
        $upcmd = 'yii migrate/up';
        if ($this->migrationPath)
            $upcmd .= ' --migrationPath="' . $this->migrationFolder . '"';
        if ($this->migrationTable)
            $upcmd .= ' --migrationTable="' . $this->migrationTable . '"';
        $upcmd .= ' --interactive=0';
        $commands[] = $upcmd;
        
        return $commands;
    }
    protected function createEntityMigration(Entity $entity)
    {
        $dbIndexFree = $dbIndexRequired = $junction = '';
        $cmd = 'yii migrate/create create_' . $entity->name . '_table';
        if ($entity->migrationFields)
            $cmd .= ' --fields="' . $entity->migrationFields . '"';
        $cmd .= $this->getMigrationDefaultParams();
        
        if ($entity->isHoldingForeignKey())
            $dbIndexRequired = $cmd;
        else
            $dbIndexFree = $cmd;

        foreach ($entity->relationships as $relation) {
            
            if ($relation->isManyToMany() && $this->createdJunction($entity->name, $relation->relatedTo->name) === false) 
            {
                $this->_toJunction[$relation->relatedTo->name] = $entity->name;
                $jcmd = 'yii migrate/create create_junction_table_for_' . $entity->name . '_and_' . $relation->relatedTo->name . '_tables';
                if ($relation->ownRelAttributes)
                    $jcmd .= ' --fields="' . $relation->junctionFields . '"';
                
                    $jcmd .= $this->getMigrationDefaultParams();
                $junction = $jcmd;
                
            }
        }
        
        return array_filter([$dbIndexFree,$dbIndexRequired,$junction]);
    }
    protected function getMigrationUpdateCommands()
    {
        $commands = [];
        
        foreach ($this->schema->entities as $entity) {
            $tblName = Yii::$app->db->tablePrefix . $entity->name;
            $isUpdate = false;
            if (Yii::$app->db->getTableSchema($tblName, true) instanceof TableSchema) {
                $isUpdate = true;
            }
            if($isUpdate==false){
                $commands = array_merge($commands,$this->createEntityMigration($entity));
            }else{
                $commands = array_merge($commands,$this->updateEntityMigration($entity));
            }
        }
        
        if(count($commands)>0){
        $upcmd = 'yii migrate/up';
        if ($this->migrationPath)
            $upcmd .= ' --migrationPath="' . $this->migrationFolder . '"';
        if ($this->migrationTable)
            $upcmd .= ' --migrationTable="' . $this->migrationTable . '"';
        $upcmd .= ' --interactive=0';
        $commands[] = $upcmd;
        }
        
        return $commands;
    }

    protected function getMigrationDefaultParams()
    {
        $cmd = '';
        if ($this->appconfig)
            $cmd .= ' --appconfig="' . $this->appconfig . '"';
        if ($this->db)
            $cmd .= ' --db="' . $this->db . '"';
        if ($this->migrationPath)
            $cmd .= ' --migrationPath="' . $this->migrationFolder . '"';
        if ($this->migrationTable)
            $cmd .= ' --migrationTable="' . $this->migrationTable . '"';
        if($this->schema->moduleNamespace)
            $cmd .= " --comment='{\"base_namespace\":\"".addslashes(addslashes($this->schema->moduleNamespace))."\"}'";
        if ($this->templateFile)
            $cmd .= ' --templateFile="' . $this->templateFile . '"';
        if ($this->useTablePrefix)
            $cmd .= ' --useTablePrefix=1';
        $cmd .= ' --interactive=0';
        
        return $cmd;
    }
    public function getConsoleCommands()
    {
        return [
            'create' => array_merge($this->getMigrationClearCommands(), $this->getMigrationCreateCommands()),
            'update' => $this->getMigrationUpdateCommands()
        ];
    }
    public static function getMergedFieldsOfEntity($entity){
        $attributes = $entity->relatedAttributes;
        $relations = $entity->relationships;
        $mergedFields = array_merge($attributes, $relations);
        usort($mergedFields, function ($item1, $item2) {
            return $item1['column_order'] <=> $item2['column_order'];
        });
        return $mergedFields;
    }
    protected function updateEntityMigration(Entity $entity)
    {
        if(\Yii::$app->get('cache')){
            \Yii::$app->cache->flush();
        }
        $attributes = $entity->relatedAttributes;
        $relations = $entity->relationships;

        $tblName = Yii::$app->db->tablePrefix . $entity->name;

        $tblSchema = Yii::$app->db->getTableSchema($tblName, true);

        $cmds = [];
        $attrNames = [];
        $defaultColumns = TwMigration::getTableDefaultColumns();

        $mergedFields = $this->getMergedFieldsOfEntity($entity);
        $renamedAttributes = [];
        foreach ($mergedFields as $atr) {
            if ($atr instanceof Attribute) {
                $attrNames[] = $atr->name;
            } else {
                if ($atr instanceof Relationship && $atr->rel_type == Relationship::HAS_ONE) {
                    $attrNames[] = $atr->getRelationshipFieldName();
                }
            }
            if($atr->old_name && $atr->name!=$atr->old_name){
                if($tblSchema->getColumn($atr->old_name)){
                    $renamedAttributes[] = $atr->old_name;
                }
            }
        }

        /* Columns to be deleted */
        $deleteColumns = [];
        foreach ($tblSchema->columns as $col) {
            if ($col->isPrimaryKey || in_array($col->name,$renamedAttributes) || in_array($col->name, $defaultColumns) || in_array($col->name, $attrNames) || StringHelper::endsWith($col->name, '_meta')) {
                continue;
            }
            $deleteColumns[] = $col->name;
        }
        foreach ($relations as $relation) {
            if ($relation->rel_type == Relationship::HAS_ONE) {
                $relFName = $relation->getRelationshipFieldName();
                $relCol = $tblSchema->getColumn($relFName);
                if ($relCol == null){
                    $idxCols = [];
                    if(isset($tblSchema->columns['deleted_at'])){
                        $idxCols = ['deleted_at',$relFName];
                    }
                    $cmds[] = 'yii migrate/create add_' . $relFName . '_column_to_' . $entity->name . '_table --fields="' . $relation->getMigrationField($idxCols) . '" ' . $this->getMigrationDefaultParams();
                    
                }
            }
        }

        foreach ($attributes as $atr) {
            $col = $tblSchema->getColumn($atr->name);
            if($col==null && $atr->old_name){
                $col = $tblSchema->getColumn($atr->old_name);
            }
            if ($col != null) {
                $comment = json_decode($col->comment, true);
                $jsonValid = false;
                if (json_last_error() === JSON_ERROR_NONE) {
                    $jsonValid = true;
                }
                $isSameComment = true;
                if ($jsonValid && is_array($comment)) {
                    if (json_encode($atr->getCommentConfig(false)) != $col->comment){
                        $isSameComment = false;
                    }
                }else{
                    if($atr->getCommentConfig(false) && json_encode($atr->getCommentConfig(false))!= $col->comment){
                        $isSameComment = false;
                    }
                }

                $atrReq = (bool) $atr->required;
                $colReq = (bool) ! $col->allowNull;
                $isFKSame = true;
                if ($atr->type == Attribute::TYPE_FOREIGN_KEY_FROM_NON_SCHEMA_BUILDER_TBL) {
                    foreach ($tblSchema->foreignKeys as $fk) {
                        if (isset($fk[$col->name])) {
                            if ($fk[0] !== $atr->foreign_key_table || $fk[$col->name] !== $atr->foreign_key_column) {
                                $isFKSame = false;
                            }
                        }
                    }
                }
                $fieldRawTypeMappedToDBType = [
                    Attribute::TYPE_ENUM,
                    Schema::TYPE_DECIMAL
                ];

                /* If Column has diffrent data type or comment then alter statement is generated */
                if (((in_array($atr->type,$fieldRawTypeMappedToDBType) && $atr->rawType != $col->dbType) || (in_array($atr->type,$fieldRawTypeMappedToDBType)==false && $atr->rawType != $col->type && \Yii::$app->db->queryBuilder->typeMap[$atr->rawType]!=$col->dbType)) || (($atr->length!=='' && $atr->length!==null) && $atr->length != $col->size) || $atrReq != $colReq || (is_object($col->defaultValue)==false && $atr->default != $col->defaultValue) || $isSameComment == false || $isFKSame == false) {

                    $fCode = $this->getExistingFieldMigrateCode($atr->name, $tblSchema).$this->getDefaultValueCode($atr->name, $tblSchema);

                    if ($atr->migrationField) {
                        $idxCols = [];
                        if ($isFKSame == false) {
                            foreach ($tblSchema->foreignKeys as $fk) {
                                if (isset($fk[$col->name])) {
                                    // PENDING - Working on dropping the old foreign key before creating a new one
                                    $cmds[] = "yii migrate/create drop_" . $col->name . "_foreign_keys_from_" . $entity->name . '_table --fields="' . $fCode . '" ' . $this->getMigrationDefaultParams();
                                    if(isset($tblSchema->columns['deleted_at'])){
                                        $idxCols = ['deleted_at',$col->name];
                                    }
                                }
                            }
                        }
                        
                        $cmd = 'yii migrate/create alter_' . $atr->name . '_columns_of_' . $entity->name . '_table' . ' --fields="' . $atr->getMigrationField($idxCols) . '" --oldFields="' . $fCode . '"';

                        $cmd .= $this->getMigrationDefaultParams();
                        $cmds[] = $cmd;
                    }
                }
            } else {
                if($atr->type==Attribute::TYPE_FOREIGN_KEY_FROM_NON_SCHEMA_BUILDER_TBL && isset($tblSchema->columns['deleted_at'])){
                    $idxCols = ['deleted_at',$atr->name];
                }
                
                $cmds[] = 'yii migrate/create add_' . $atr->name . '_column_to_' . $entity->name . '_table --fields="' . $atr->getMigrationField($idxCols) . '" ' . $this->getMigrationDefaultParams();
            }
            if($atr->old_name &&  $atr->name!=$atr->old_name){
                if(isset($tblSchema->columns[$atr->old_name])){
                    // renaming migration
                    $cmds[] =  'yii migrate/create rename_'
                        . $atr->old_name
                        . '_column_to_'.$atr->name.'_of_'
                            . $entity->name . '_table'.$this->getMigrationDefaultParams() ;
                }
            }
        }

        if ($entity->isHoldingForeignKey())
            $dbIndexRequired = $cmd;
        else
            $dbIndexFree = $cmd;
            
        foreach ($entity->relationships as $relation) {

            $relField = $relation->getRelationshipFieldName();
            $col = $tblSchema->getColumn($relField);
            
            if ($col) {
                // continue;
            }

            if (
                $relation->isManyToMany() && 
$this->createdUpdateJunction($entity->name, $relation->relatedTo->name) === false){

                $junctionTblName = $entity->name . '_' . $relation->relatedTo->name;
                $jTblSchema = Yii::$app->db->getTableSchema($junctionTblName);
                
                if ($jTblSchema != null) {
                    continue; // If junction table already exists then we delete it.
                }
                $junctionTblName = $relation->relatedTo->name.'_'.$entity->name;
                $jTblSchema = Yii::$app->db->getTableSchema($junctionTblName);
                if ($jTblSchema != null) {
                    continue; // If junction table already exists then we delete it.
                }
                
                $this->_toUpdateJunction[$relation->relatedTo->name] = $entity->name;

                $jcmd = 'yii migrate/create create_junction_table_for_' . $entity->name . '_and_' . $relation->relatedTo->name . '_tables';
                
                if ($relation->ownRelAttributes)
                    $jcmd .= ' --fields="' . $relation->junctionFields . '"';

                $jcmd .= $this->getMigrationDefaultParams();

                $cmds[] = $jcmd;
 }
}
        foreach ($deleteColumns as $name) {
           $fCode = $this->getExistingFieldMigrateCode($name,$tblSchema).$this->getDefaultValueCode($name, $tblSchema);
           $cmds[] = 'yii migrate/create '.'drop_'.$name.'_column_from_'.$entity->name.'_table --fields="'.$fCode.'" '.$this->getMigrationDefaultParams();
        }
        
        return $cmds;
    }
    public function moveColumn(Entity $entity,$columnName,int $newPosition){
        if(\Yii::$app->get('cache')){
            \Yii::$app->cache->flush();
        }
        $mergedFields = self::getMergedFieldsOfEntity($entity);
        $tblName = Yii::$app->db->tablePrefix . $entity->name;
        $tblSchema = Yii::$app->db->getTableSchema($tblName, true);
        if($tblSchema==null || $tblSchema->getColumn($columnName)==null){
            //If column doesn't exists in the database yet then we dont do anything
            return false;
        }
        $cmds = [];
        $attrNames = [];
        foreach ($mergedFields as $atr) {
            if ($atr instanceof Attribute) {
                $attrNames[] = $atr->name;
            } else {
                if ($atr instanceof Relationship && $atr->rel_type == Relationship::HAS_ONE){
                    $attrNames[] = $atr->getRelationshipFieldName();
                }
            }
        }
        $colNames = [];
        foreach ($tblSchema->columnNames as $colName) {
            if (in_array($colName, $attrNames)) {
                $colNames[] = $colName;
            }
        }
        $currentPosition = array_search($columnName, $colNames);
        if($currentPosition==$newPosition){
            return true;
        }
        $after = $tblSchema->primaryKey[0];
        if($newPosition>0){
            if($newPosition==(count($colNames)-1)){
                $after = $colNames[$newPosition];
            }else{
                $after = $colNames[$newPosition-1];
            }
        }
        
            
            // Placing the attributes after primary key(id)
            $cmd = [];
            foreach ($mergedFields as $atr) {
                $cName = null;
                if ($atr instanceof Relationship) {
                    if ($atr->rel_type == Relationship::HAS_ONE) {
                        $cName = $atr->getRelationshipFieldName();
                    } else {
                        continue;
                    }
                } else {
                    $cName = $atr->name;
                }
                if($cName!=$columnName){
                    continue;
                }
                
                $fCode = self::getExistingFieldMigrateCode($cName, $tblSchema)
                .self::getDefaultValueCode($cName, $tblSchema);
                if ($atr->migrationField && $tblSchema->getColumn($cName)) {
                    
                    foreach ($tblSchema->foreignKeys as $fk) {
                        foreach ($fk as $k => $r) {
                            if ($cName === $k) {
                                $cmds[] = "migrate/create drop_" . $cName . "_foreign_keys_from_" . $entity->name . '_table --fields="' . $atr->migrationField . '" '.$this->getMigrationDefaultParams();
                            }
                        }
                    }
                    
                    $mCode = $atr->getMigrationField([],$after);
                    //If column already exists then we create its alter column statement otherwise
                    $cmd = 'migrate/create alter_' . $cName . '_columns_of_' . 
                        $entity->name . '_table' . ' --fields="' . $mCode . "\"";
                    
                    
                    if ($tblSchema->getColumn($cName) && $fCode) {
                        $cmd .= ' --oldFields="' . $fCode . '"';
                    } else {
                        // $cmd .= ' --oldFields=""';
                    }
                    $cmd .= $this->getMigrationDefaultParams();
                    $cmds[] = $cmd;
                }
        }
        
        return $cmds;
    }
    private static function getDefaultValueCode($colName, TableSchema $tblSchema){
        $col = $tblSchema->getColumn($colName);
        $fCode = '';
        if($col->allowNull==1 && $col->defaultValue!==''){
            if($col->defaultValue===null){
                $fCode .= ':defaultValue(null)';
            }else{
                $fCode .= ':defaultValue('.(is_string($col->defaultValue)?"'$col->defaultValue'":$col->defaultValue).')';
            }
        }
        return $fCode;
    }
    private static function getExistingFieldMigrateCode($colName, TableSchema $tblSchema){
        $col = $tblSchema->getColumn($colName);
        $fCode = $col->name.':'.$col->type;
        if(($col->size!==null || $col->size !=='') && strlen($col->size)>0){
            $fCode .= '('.$col->size.')';
        }
        
        if($col->allowNull===true){
            $fCode .= ':null';
        }else if($col->allowNull===false){
            $fCode .= ':notNull';
        }
        foreach($tblSchema->foreignKeys as $fk){
            if(isset($fk[$col->name])){
                $fCode .= ':foreignKey('.$fk[0].' '.$fk[$col->name].')';
            }
        }
        
        
        if($col->comment){
            $cmnt = $col->comment;
            $comment = json_decode($col->comment,true);
            $jsonValid = false;
            if (json_last_error() === JSON_ERROR_NONE)
            {
                $jsonValid = true;
            }

            if($jsonValid && is_array($comment)){
                $cmnt = addslashes($col->comment);
                
            }
            
            $fCode .=':comment(\''.$cmnt.'\')';
        }
        return $fCode;
    }
}
