<?php

namespace tunecino\builder\models;

use Yii;
use yii\db\ActiveRecord;
use yii\base\InvalidConfigException;
use \yii\helpers\Inflector;
use yii\helpers\StringHelper;


class Schema extends \yii2tech\filedb\ActiveRecord
{
    
    public static function getDb()
    {
        return \tunecino\builder\Module::getInstance()->get('filedb');
    }

    public function attributes()
    {
        return array_keys($this->attributeLabels());
    }
    /**
    *@desc Re-orders the create table command lists to prevent 
    *Foreign key constraint issue occurred due to parent table generation after the child tables  
    * 
    **/
    public function orderCommands($cmds){
        /*Getting commands after migrate/create, to run them after creation/alteration of tables - START*/
        foreach($cmds as $k => $cmd){
            if(
                preg_match('~migrate/create create_(.+)~', $cmd, $output)
                || preg_match('~migrate/create alter_(.+)~', $cmd, $output)
                || preg_match('~migrate/create add_(.+)~', $cmd, $output)
                || preg_match('~create_junction(?:_table_for_|_for_|_)(.+)_and_(.+)_tables~', $cmd, $output)
                ){
                    $lastCreateIndex = $k;
                }
        }
        $i = 0;
        $cmdsAfterCreate = [];
        $found = false;
        
        foreach($cmds as $k=>$cmd){
            if($found == true){
                $cmdsAfterCreate[] = $cmd;
                unset($cmds[$k]);
            }
            if($lastCreateIndex==$k){
                $found = true;
            }
        }
        
        /*Getting commands after migrate/create, to run them after creation/alteration of tables - END*/
        $tablesWithFk = [];
        
        foreach($cmds as $index=>$cmd){
            if (preg_match('~create_junction_table_(.*?)_tables~', $cmd, $output)){
                continue;
            }
            preg_match('~create_(.*?)_table~', $cmd, $output);
            if(count($output)>0)
            {
                $tbl = $output[1];
                
                $tablesWithFk[$tbl] = [
                    'table'=>$tbl,'relatedTables'=>[],'cmd'=>$cmd
                ];
                
                preg_match_all('~foreignKey\((.*?)\)~', $cmd, $fKeysTables);
                if(count($fKeysTables[0])>0){
                    foreach($fKeysTables[1] as $relatedTbl){
                        $tb = explode(' ',$relatedTbl);
                        if(\Yii::$app->db->getTableSchema($tb[0])==null){
                            $tablesWithFk[$tbl]['relatedTables'][] = $relatedTbl;
                        }
                    }
                }
            }
        }
        
        $orderList = [];
        foreach ($tablesWithFk as $k=>$item){
            $list = $this->getBaseRelationFirst($tablesWithFk, $k);
            foreach($list as $items){
                $orderList[$items]=$items;
            }
        }
        $oList = array_values($orderList);
        $i = 0;
        
        if($oList && count($oList)>0){
            
        foreach($cmds as $k=>$cmd){
            if (preg_match('~create_junction_table_(.*?)_tables~', $cmd, $output)){
                continue;
            }
            preg_match('~create_(.*?)_table~', $cmd, $output);
            if(count($output)>0)
            {
                $cmds[$k] = $tablesWithFk[$oList[$i]]['cmd'];
                
                $i++;
            }
        }
        }
        
        /*create junction tables after other tables*/
        $junctionCmds = [];
        $lastCreateIndex = 0;
        /*Finding the junction table commands to place them after create/alter table/column cmds - START */
        foreach($cmds as $k => $cmd){
            if(preg_match('~create_junction(?:_table_for_|_for_|_)(.+)_and_(.+)_tables~', $cmd, $output))
            {
                if(count($output)>0)
                {
                    $junctionCmds[] = $cmd;
                    unset($cmds[$k]);
                    
                }
            }
        }
        /*Finding the junction table commands to place them after create/alter table/column cmds - END */
        
        $cmds = array_values(array_merge($cmds,$junctionCmds,$cmdsAfterCreate));
        
        return $cmds;
    }
    private function getBaseRelationFirst($tablesWithFk,$tbl){
        $cmdOrder = [];
        foreach ($tablesWithFk[$tbl]['relatedTables'] as $rel){
            $cmdOrder = array_merge($cmdOrder,$this->getBaseRelationFirst($tablesWithFk, $rel));
        }
        $cmdOrder[$tbl] = $tbl;
        return $cmdOrder;
    }
    
    public function getConsoleCommands()
    {
        $generatedCMD = [];
        $commands = Yii::$app->controller->module->commands;
        foreach ($commands as $cmd) {
            if (is_string($cmd)) $generatedCMD[] = $cmd;
            else if (is_callable($cmd)) $generatedCMD = array_merge($generatedCMD, $this->parseCallable($cmd));
            else if (is_array($cmd)) $generatedCMD = array_merge($generatedCMD, $this->parseClass($cmd));
            else throw new InvalidConfigException('commands config array should only hold "strings", "arrays" or "callable functions".');
        }
        
        
        $generatedUpdateCMDs = $this->orderCommands($generatedCMD['update']);
        $generatedCMDs = $this->orderCommands($generatedCMD['create']);
        return ['create'=>$generatedCMDs,'update'=>$generatedUpdateCMDs];
    }


    protected function parseCallable($fn)
    {
        $callableCmd = call_user_func($fn, $this);

        if (is_string($callableCmd) === false && is_array($callableCmd) === false)
            throw new InvalidConfigException('unexpected output for "callable funtion".');

        return is_array($callableCmd) ? $callableCmd : [$callableCmd];
    }


    protected function parseClass($config)
    {
        $obj = Yii::createObject($config);
        $model = $this->getConfigModel($obj::className())->one();

        if ($model) {
            $modelCmd = $model->consoleCommands;

            if (is_string($modelCmd) === false && is_array($modelCmd) === false)
                throw new InvalidConfigException('unexpected output for "getConsoleCommands()".');

            return is_array($modelCmd) ? $modelCmd : [$modelCmd];
        }
        
        return [];        
    }


    protected function getConfigModel($class)
    {
        return $this->hasOne($class::className(), ['schema_id' => 'id']);
    }


    public function loadForms($modelOnly = false)
    {
        $forms = [];
        $commands = Yii::$app->controller->module->commands;
        
        foreach ($commands as $config) {
            if (is_array($config)) {
                $obj = Yii::createObject($config);
                $viewFile = $obj::formView();

                $model = $this->getConfigModel($obj::className())->one();
                $modelData = $model ?: $obj;

                if ($modelOnly) $forms[$obj::fileName()] = $modelData;
                else $forms[$obj::fileName()] = ['viewFile'=>$viewFile, 'model'=>$modelData];
            }
        }

        return $forms;
    }


    public function attributeLabels()
    {
        return [
            'id' => 'Primary Key',
            'name' => 'Name',
            'isModule' => 'Built as Module',
        ];
    }


    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'string'],
            ['name', 'unique', 'message' => 'schema "{value}" has already been created.'],
            ['isModule', 'boolean'],
            ['isModule', 'default', 'value' => '0'],
        ];
    }


    public function validateFilePath($attribute, $params)
    {
        $file = $this->$attribute;
        $path = Yii::getAlias($file, false);
        if (substr($path, -4) !== '.php') $this->addError($attribute, "A php file is expected.");
        if ($path === false or !is_file($path)) {
            $this->addError($attribute, "File '$file' does not exist or has syntax error.");
        }
    }


    public function attributeHints()
    {
        return [
            'name' => 'The Schema name e.g., <code>blog</code>, <code>v1</code>, <code>alpha-01</code>. If this is going to be generated as a seperate Module <b>yii\helpers\Inflector::variablize()</b> will be applied to it then used to fill the <b>moduleID</b> and <b>moduleClass</b> attributes.',
        ];
    }


    public function getEntities()
    {
        return $this->hasMany(Entity::className(), ['schema_id' => 'id']);
    }


    public function getModuleID()
    {
        return $this->isModule ? Inflector::variablize($this->name) : null;
    }
    public function getModule(){
       // Module
        if(!$this->isModule){
            return null;
        }
        $forms = $this->loadForms(true);
        return isset($forms['Module'])?$this->loadForms(true)['Module']:null;
    }
    public function getModuleNamespace(){
        if(!$this->isModule){
            return null;
        }
        $forms = $this->loadForms(true);
        return isset($forms['Module'])?$this->loadForms(true)['Module']->moduleNamespace:null;
    }
    public function readyToGenerate($create=true)
    {
        if (count($this->consoleCommands[$create?'create':'update']) === 0) return false;
        $entities = $this->entities;
        if (count($entities) === 0) return false;
        foreach ($entities as $entity) {
            if ($entity->getRelatedAttributes()->count() === 0) return false;
        }
        return true;
    }
    

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->id = uniqid();
        }
        return parent::beforeSave($insert);
    }


    public function afterDelete()
    {
        Entity::deleteAll(['schema_id' => $this->id]);
        parent::afterDelete();
    }
}
