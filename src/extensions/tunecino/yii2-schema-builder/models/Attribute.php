<?php
namespace tunecino\builder\models;

use Yii;
use yii\db\Schema;
use yii\base\Exception;
use yii2tech\filedb\ActiveRecord;
/**
 * @property string $foreign_key_table
 * @property string $foreign_key_column
 * @property string $value
 */
class Attribute extends \yii2tech\filedb\ActiveRecord
{

    const SCENARIO_JUNCTION = 'junction';

    const TYPE_HIDDEN = 'hidden';

    const TYPE_CANVAS = 'canvas';

    const TYPE_META = 'meta';

    const TYPE_FILE = 'file';

    const TYPE_FILES = 'files';
    const TYPE_ENUM = 'enum';
    const TYPE_COLOR = 'color';
    /**
     * @desc foreign key from non-schema-builder tables e.g user,language etc
     */
    const TYPE_FOREIGN_KEY_FROM_NON_SCHEMA_BUILDER_TBL = 'foreignKeyNonSchema';
    
    const FORM_FIELD_TYPE_RADIO = 'radio';
    const FORM_FIELD_TYPE_CHECKBOX = 'checkbox';
    /**
     * 
     * @desc This property contains all the keys which can be a part of comment configuration of a column/field of a table
    */
    public $commentAttributes = [
        'file_extension',
        'min_file_count',
        'max_file_count',
        'file_size',
    ];
    public static function getDb()
    {
        return \tunecino\builder\Module::getInstance()->get('filedb');
    }

    public function attributes()
    {
        return array_keys($this->attributeLabels());
    }

    public function rules()
    {
        return [
            [
                [
                    'name',
                    'type',
                    'entity_id'
                ],
                'required'
            ],
            [
                'type',
                'in',
                'range' => array_keys($this->types)
            ],
            [
                [
                    'required',
                    'unique'
                ],
                'boolean'
            ],
            [
                [
                    'entity_id',
                    'name',
                    'type',
                    'default'
                ],
                'string'
            ],
            [
                'name',
                'compare',
                'compareValue' => 'id',
                'operator' => '!=',
                'message' => 'Primary key will be added automatically. No need to create it.'
            ],
            [
                'name',
                'unique',
                'targetAttribute' => [
                    'name',
                    'entity_id'
                ],
                'comboNotUnique' => 'Attribute "{value}" has already been defined.'
            ],
            [
                'entity_id',
                'exist',
                'skipOnError' => true,
                'targetClass' => Entity::className(),
                'targetAttribute' => [
                    'entity_id' => 'id'
                ],
                'on' => self::SCENARIO_DEFAULT
            ],
            [
                'entity_id',
                'exist',
                'skipOnError' => true,
                'targetClass' => Relationship::className(),
                'targetAttribute' => [
                    'entity_id' => 'id'
                ],
                'on' => self::SCENARIO_JUNCTION
            ],
            [
                'entity_id',
                'validateJunctionRelation',
                'on' => self::SCENARIO_JUNCTION
            ],
            [
                'length',
                'required',
                'when' => function ($model) {
                return $model->lengthRequired();
                }
                ],
            [
                'length',
                'integer',
                'when' => function ($model) {
                    return $model->lengthRequired();
                }
            ],
            [
                'precision',
                'integer',
                'when' => function ($model) {
                    return $model->precisionRequired();
                }
            ],
            [
                'scale',
                'integer',
                'when' => function ($model) {
                    return $model->scaleRequired();
                }
            ],
            [
                [
                    'advanced_mode',
                    'keep_meta_data'
                ],
                'boolean',
                'when' => function ($model) {
                    return $model->commentRequired();
                }
            ],
            [
                [
                    'rich_text'
                ],
                'boolean',
                'when' => function ($model) {
                    return $model->textRequired();
                }
            ],
            [
                [
                    'form_field_type'
                ],
                'string',
                'when' => function ($model) {
                    return $model->formFieldTypeRequired();
                }
            ],
            ['enum_options','string'],
            [
                'enum_options',
                'required',
                'when'=> function ($model) {
                return $model->enumOptionsRequired();
                }
            ],
            [
                [
                    'file_size',
                    'file_extension'
                ],
                'string',
                'when' => function ($model) {
                    return $model->fileRequired();
                }
            ],
            [
                [
                    'file_size',
                    'file_extension'
                ],
                'string',
                'when' => function ($model) {
                    return $model->fileRequired();
                }
            ],
            [
                [
                    'min_file_count',
                    'max_file_count'
                ],
                'number',
                'min' => '0',
                'max' => '20',
                'when' => function ($model) {
                    return $model->fileRequired();
                }
            ],
            [
                [
                    'column_order'
                
                ],
                'integer',
                'min' => 1
            ],
            [
                ['foreign_key_table','foreign_key_column'],
                'string'
            ],
            [
                [
                    'foreign_key_table',
                    'foreign_key_column'
                ],
                'required',
                'when' => function ($model) {
                    return $model->foreignKeyRequired();
                }
             ],
             [
                 'foreign_key_table',function ($attribute, $params, $validator) {
                        $list = $this->getNonSchemaTables($this->entity);
                        if(!in_array($this->$attribute,$list))
                        {
                            $this->addError($attribute, 'Please select a valid table from the list');
                        }
                 }
             ]
             
        ];
        
    }

    public function validateJunctionRelation($attribute, $params)
    {
        if ($this->relationship->isManyToMany() === false)
            $this->addError($attribute, "Entities to share this attribute are not sharing a many_to_many relationship");
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Primary Key',
            'name' => 'Attribute Name',
            'entity_id' => 'Entity Name',
            'type' => 'Type',
            'required' => 'Required',
            'unique' => 'Unique',
            'length' => 'Length',
            'precision' => 'Precision',
            'scale' => 'Scale',
            'comment' => 'Comment',
            'advanced_mode' => 'Advanced Mode',
            'keep_meta_data' => 'Keep Meta Data',
            'rich_text' => 'Rich Text Field',
            'file_size' => 'File Size',
            'file_extension' => 'File Extension/Type',
            'enum_options' => 'Enum Options',
            'min_file_count' => 'Min File Count',
            'max_file_count' => 'Max File Count',
            'hidden' => 'Hidden',
            'default' => 'Default Value',
            'column_order' => 'Column Order',
            'foreign_key_table'=>'Foreign Table',
            'foreign_key_column'=>'Foreign Column',
            'form_field_type' => 'Form Field Type',
            'old_name'=>'Old Attribute Name'
        ];
    }
    public function getRawType(){
        $type = $this->type;
        if(in_array($type,[self::TYPE_CANVAS, self::TYPE_FILE, self::TYPE_FILES,self::TYPE_HIDDEN,self::TYPE_META,self::TYPE_COLOR]))
        {
            $type = 'string';
        }
        if($type == self::TYPE_FOREIGN_KEY_FROM_NON_SCHEMA_BUILDER_TBL){
            $col = \Yii::$app->db->getTableSchema($this->foreign_key_table)->getColumn($this->foreign_key_column);
            return $col->type;
        }
        if($type==self::TYPE_ENUM){
            $enumOptions = explode(',',$this->enum_options);
            return "enum('" . implode( "','", $enumOptions ) . "')";
        }
        if($type==Schema::TYPE_DECIMAL){
            return "decimal(".$this->precision.","."$this->scale".")";
        }
        return $type;
    }
    public static function getNonSchemaTables($model=null){
        $q = Entity::find();
        if($model){
            $q->andWhere(['id'=>$model->id]);
        }
        $entities = $q->all();
        
        $tables = \Yii::$app->db->getSchema()->tableNames;
        foreach($entities as $entity){
            $k = array_search($entity->name, $tables);
            unset($tables[$k]);
        }
        if(\Yii::$app->db->tablePrefix){
            $temTables = $tables;
            $tables = [];
            $tblPref = \Yii::$app->db->tablePrefix;
            foreach($temTables as $tbl){
                if (substr($tbl, 0, strlen($tblPref)) == $tblPref) {
                    $tables[] = substr($tbl, strlen($tblPref));
                }
            }
        }
        return array_values($tables);
    }

    public function getMigrationField($idxCols=[],$after=null)
    {
        $type = $this->type;
        $length = $this->length;
        $precision = $this->precision;
        $scale = $this->scale;
        
        if (in_array($type, [self::TYPE_COLOR,self::TYPE_CANVAS,self::TYPE_FILE,self::TYPE_FILES,self::TYPE_HIDDEN,self::TYPE_META])) {
            $type = 'string';
        }
        $foreignKeySetup = null;
        if ($type == self::TYPE_FOREIGN_KEY_FROM_NON_SCHEMA_BUILDER_TBL) {
            $col = \Yii::$app->db->getTableSchema($this->foreign_key_table)->getColumn($this->foreign_key_column);
            if ($col == null) {
                // throw new \Exception('Foreign Key Column not found: '. $this->foreign_key_column . ' in table '.$this->foreign_key_table);
            }
            $type = $col->type;
            $length = $col->precision;
            $scale = $col->scale;
            $precision = $col->precision;
            $foreignKeySetup = ':foreignKey(' . $this->foreign_key_table . ' ' . $this->foreign_key_column . ')';
            $charsetAndCollation = null;
            $colDbType = null;
            if (\Yii::$app->db->driverName == 'mysql') {

                $sql = "SHOW FULL COLUMNS FROM " . \Yii::$app->db->quoteTableName(\Yii::$app->db->tablePrefix . $this->foreign_key_table) . " WHERE `Field`=" . \Yii::$app->db->quoteValue($this->foreign_key_column) . "";

                preg_match('/dbname=([^;]*)/', \Yii::$app->db->dsn, $match);
                $dbName = @$match[1];
                if ($dbName == '') {
                    $dbName = \Yii::$app->db->createCommand("SELECT DATABASE()")->queryScalar();
                }

                $sql = "SELECT COLUMN_NAME,CHARACTER_SET_NAME,COLLATION_NAME FROM information_schema.`COLUMNS` WHERE table_schema = " . \Yii::$app->db->quoteValue($dbName) . " AND table_name = " . \Yii::$app->db->quoteValue($this->foreign_key_table) . " AND `COLUMN_NAME`=" . \Yii::$app->db->quoteValue($this->foreign_key_column);

                $foreignColumn = \Yii::$app->db->createCommand($sql)->queryOne();
                if ($foreignColumn['CHARACTER_SET_NAME'] && $foreignColumn['COLLATION_NAME']) {
                    $colDbType = $col->dbType;
                    $charsetAndCollation = "CHARACTER SET " . $foreignColumn['CHARACTER_SET_NAME'] . " COLLATE " . $foreignColumn['COLLATION_NAME'];
                }
            }
            
        }
        if ($charsetAndCollation && $colDbType) {
            //When the foreign key is a varchar or char then we need to make sure that schema table has the same CHARACTER SET and Collation as the one it's going to linked with. otherwise mysql won't let us create the relation. 
            $field = $this->name . ":customColumnDefinition('{$colDbType} {$charsetAndCollation}')";
            
        }else if($type == self::TYPE_ENUM){
            $enumOptions = explode(',',$this->enum_options);
            $enumOptions = "'" . implode( "','", $enumOptions ) . "'";
            
            //When the foreign key is a varchar or char then we need to make sure that schema table has the same CHARACTER SET and Collation as the one it's going to linked with. otherwise mysql won't let us create the relation.
            $field = $this->name . ':customColumnDefinition(\"ENUM('.$enumOptions.')\")';
        }
        else {
            $field = $this->name . ':' . $type;
            if(in_array($type,[Schema::TYPE_SMALLINT,Schema::TYPE_BIGINT,Schema::TYPE_TINYINT])){
                //completes the smallint to smallinteger(), bigint to biginteger AND tinyint to tinyinteger
                $field .='eger';
            }
            if ($this->lengthRequired($type) && $length) {
                $field .= '(' . $length . ')';
            } else if ($this->precisionRequired($type) && $this->scaleRequired($type) && $precision && $scale) {
                $field .= '(' . $precision . ',' . $scale . ')';
            } else if ($this->precisionRequired($type) && $precision) {
                $field .= '(' . $precision . ')';
            } else if ($this->scaleRequired($type) && $scale) {
                $field .= '(null,' . $scale . ')';
            } else {
                if ($type == 'string') {
                    $field .= '(255)';
                }
            }
        }
        if ($type == self::TYPE_HIDDEN) {
            $data = [
                "inputtype" => self::TYPE_HIDDEN
            ];
            $commentData = addslashes(json_encode($data));
            $field .= ":comment('" . $commentData . "')";
        }
        
        $comment = $this->getCommentConfig();
        if ($comment) {
            $field .= ":comment('" . $comment . "')";
        }

        if ($this->required) {
            $field .= ':notNull';
        }else{
            $field .= ':null';
        }
        if ($this->unique) {
            $field .= ':unique';
        }
        if($after){
            $field .= ':after(\''.$after.'\')';
        }
        if (strlen($this->default) !== 0) {
            $default = (is_int($this->default) or ctype_digit($this->default)) ? $this->default : '\'' . $this->default . '\'';
            $field .= ':defaultValue(' . $default . ')';
        }
        if ($this->keep_meta_data == 1) {
            $commentMetaData = '{\"inputtype\":\"canvas_meta\"}';
            $field .= ', ' . $this->name . '_meta:getDb()->getSchema()->createColumnSchemaBuilder(\"longtext\"):comment(\'' . $commentMetaData . '\'):notNull';
        }
        if ($foreignKeySetup) {
            $field .= $foreignKeySetup;
        }
        if($idxCols && count($idxCols)>0){
            $field .= ':createIndex('. implode(' ',$idxCols) .')';
        }
        return $field;
    }
    
    public function getCommentConfig($json=true){
        $data = [];
        if ($this->type == self::TYPE_HIDDEN) {
            $data = [
                "inputtype" => self::TYPE_HIDDEN
            ];
        }
        if ($this->type == self::TYPE_COLOR) {
            $data = [
                "inputtype" => self::TYPE_COLOR
            ];
        }
        if ($this->type == self::TYPE_META) {
            $data = [
                "inputtype" => self::TYPE_META
            ];
        }
        
        if ($this->type == Schema::TYPE_TEXT && $this->rich_text != 0) {
            $data = [
                "inputtype" => "editor",
                "editorversion" => "5"
            ];
        }
        if ($this->type == Schema::TYPE_BOOLEAN) {
            $data = [
                "inputtype" => $this->form_field_type==self::FORM_FIELD_TYPE_RADIO? "radio":"checkbox",
                "options" =>[1=>'On',0=>'Off']
            ];
        }
        if ($this->type == self::TYPE_CANVAS) {
            if ($this->advanced_mode != 0 || $this->keep_meta_data != 0) {
                $data = [
                    'isAdvanceMode'=>(bool)$this->advanced_mode,
                    'keepMetaData' =>(bool) $this->keep_meta_data,
                    'inputtype'=>'canvas'
                ];
            } else {
                $data = [
                    "inputtype" => "canvas"
                ];
            }
        }
        
        if ($this->type == self::TYPE_FILE) {
            if (! empty($this->file_size) || ! empty($this->file_extension)) {
                $data = [
                    "fileSize" => $this->file_size,
                    "allowedExtensions" => array_map('trim', explode(',', $this->file_extension)),
                    "inputtype" => "file"
                ];
            } else {
                $data = [
                    "inputtype" => "file"
                ];
            }
        }
        if ($this->type == self::TYPE_FILES) {
            if (! empty($this->file_size)
                || ! empty($this->file_extension)
                || ! empty($this->min_file_count)
                || ! empty($this->max_file_count))
            {
                $data = [
                    "fileSize" => $this->file_size,
                    "allowedExtensions" => array_map('trim', explode(',', $this->file_extension)),
                    "minFileCount" => $this->min_file_count,
                    "maxFileCount" => $this->max_file_count,
                    "inputtype" => "files"
                ];
            } else {
                $data = [
                    "inputtype" => "files"
                ];
            }
        }
        
        if(count($data)>0){
            if($json){
                return addslashes(json_encode($data));
            }
            return $data;
        }
        return null;
    }

    public function getEntity()
    {
        return $this->hasOne(Entity::className(), [
            'id' => 'entity_id'
        ]);
    }

    public function getRelationship()
    {
        return $this->hasOne(Relationship::className(), [
            'id' => 'entity_id'
        ]);
    }

    public function getTypes()
    {
        return [
            Schema::TYPE_STRING => 'String',
            Schema::TYPE_TEXT => 'Text',
            Schema::TYPE_INTEGER => 'Integer',
            self::TYPE_FOREIGN_KEY_FROM_NON_SCHEMA_BUILDER_TBL => 'Foreign Key',
            self::TYPE_ENUM => 'Enum',
            self::TYPE_COLOR=>'Color',
            Schema::TYPE_SMALLINT => 'Small Integer',
            Schema::TYPE_BIGINT => 'Big Integer',
            Schema::TYPE_FLOAT => 'Float',
            Schema::TYPE_DOUBLE => 'Double',
            Schema::TYPE_DECIMAL => 'Decimal',
            Schema::TYPE_DATETIME => 'DateTime',
            Schema::TYPE_TIMESTAMP => 'Timestamp',
            Schema::TYPE_TIME => 'Time',
            Schema::TYPE_DATE => 'Date',
            Schema::TYPE_BINARY => 'Binary',
            Schema::TYPE_BOOLEAN => 'Boolean',
            Schema::TYPE_CHAR => 'Char',
            Schema::TYPE_MONEY => 'Money',
            self::TYPE_HIDDEN => 'Hidden',
            self::TYPE_CANVAS => 'Canvas',
            self::TYPE_META => 'Meta',
            self::TYPE_FILE => 'Single File',
            self::TYPE_FILES => 'Multiple Files'
        
        ];
    }

    public function getTypeLabel()
    {
        return $this->types[$this->type];
    }
    public static function getValidForeignKeyColumnsFromTbl($tbl){
        
        if ($tblSchema = \Yii::$app->db->getTableSchema($tbl)) {
            $validColumns = [];
            $columns = $tblSchema->columns;
            foreach ($columns as $col) {
                if ($col->isPrimaryKey) {
                    $validColumns[]  = $col->name;
                }
            }
            foreach (\Yii::$app->db->schema->findUniqueIndexes($tblSchema) as $cols){
                if(count($cols)==1){
                    $validColumns[]  = array_values($cols)[0];
                }
            }
            return array_unique($validColumns);
        }
        return [];
    }
    public function lengthRequired($type=null)
    {
        if($type==null){
            $type = $this->type;
        }
        
        return $type && in_array($type, [
            Schema::TYPE_STRING,
            Schema::TYPE_INTEGER,
            Schema::TYPE_SMALLINT,
            Schema::TYPE_BIGINT,
            Schema::TYPE_BINARY,
            Schema::TYPE_CHAR
        ]);
    }

    public function precisionRequired($type=null)
    {
        if($type==null){
            $type = $this->type;
        }
        return $type && in_array($type, [
            Schema::TYPE_FLOAT,
            Schema::TYPE_DOUBLE,
            Schema::TYPE_DECIMAL,
            Schema::TYPE_DATETIME,
            Schema::TYPE_TIMESTAMP,
            Schema::TYPE_TIME,
            Schema::TYPE_MONEY
        ]);
    }

    public function scaleRequired($type=null)
    {
        if($type==null){
            $type = $this->type;
        }
        return $type && in_array($type, [
            Schema::TYPE_DECIMAL,
            Schema::TYPE_MONEY
        ]);
    }

    public function commentRequired($type=null)
    {
        if($type==null){
            $type = $this->type;
        }
        return $type && in_array($type, [
            self::TYPE_CANVAS
        ]);
    }
    
    public function foreignKeyRequired($type=null)
    {
        if($type==null){
            $type = $this->type;
        }
        return $type && in_array($type, [
            self::TYPE_FOREIGN_KEY_FROM_NON_SCHEMA_BUILDER_TBL
        ]);
    }

    public function textRequired($type=null)
    {
        if($type==null){
            $type = $this->type;
        }
        return $type && in_array($type, [
            Schema::TYPE_TEXT
        ]);
    }

    public function fileRequired($type=null)
    {
        if($type==null){
            $type = $this->type;
        }
        return $type && in_array($type, [
            self::TYPE_FILE,
            self::TYPE_FILES
        ]);
    }
    public function enumOptionsRequired($type=null)
    {
        if($type==null){
            $type = $this->type;
        }
        return $type && in_array($type, [
            self::TYPE_ENUM,
        ]);
    }
    public function formFieldTypeRequired($type=null){
        if($type==null){
            $type = $this->type;
        }
        return $type && in_array($type, [
            Schema::TYPE_BOOLEAN
        ]);
    }
    public function filesRequired($type=null)
    {
        if($type==null){
            $type = $this->type;
        }
        return $type && in_array($type, [
            self::TYPE_FILES
        ]);
    }

    public function beforeSave($insert)
    {
        if ($insert)
            $this->id = uniqid();
        
        if ($this->lengthRequired() === false) {
            if ($insert)
                unset($this->length);
            else if (isset($this->length))
                $this->length = null;
        }
        
        if ($this->precisionRequired() === false) {
            if ($insert)
                unset($this->precision);
            else if (isset($this->precision))
                $this->precision = null;
        }
        
        if ($this->scaleRequired() === false) {
            if ($insert)
                unset($this->scale);
            else if (isset($this->scale))
                $this->scale = null;
        }
        if ($this->commentRequired() === false) {
            if ($insert) {
                unset($this->advanced_mode);
                unset($this->keep_meta_data);
            } else if (isset($this->advanced_mode)) {
                $this->advanced_mode = null;
            } else if (isset($this->keep_meta_data)) {
                $this->keep_meta_data = null;
            }
        }
        
        if ($this->textRequired() === false) {
            if ($insert) {
                unset($this->rich_text);
            } else if (isset($this->rich_text)) {
                $this->rich_text = null;
            }
        }
        
        if ($this->foreignKeyRequired() === false) {
            if ($insert) {
                unset($this->foreign_key_table);
                unset($this->foreign_key_column);
            } else if (isset($this->foreign_key_table)) {
                $this->foreign_key_table = null;
                $this->foreign_key_column = null;
            }
        }
        
        if ($this->fileRequired() === false) {
            if ($insert) {
                //unset($this->required);
                unset($this->file_size);
                unset($this->file_extension);
            } else {
                if (isset($this->file_size)) {
                    $this->file_size = null;
                }
                if (isset($this->file_extension)) {
                    $this->file_extension = null;
                }
            }
        }
        
        if ($this->filesRequired() === false) {
            if ($insert) {
                unset($this->min_file_count);
                unset($this->max_file_count);
                //unset($this->required);
            } else {
                if(isset($this->min_file_count)) {
                    $this->min_file_count = null;
                }
                if (isset($this->max_file_count)) {
                    $this->max_file_count = null;
                }
                /* if (isset($this->required)) {
                       $this->required = null;
                } */
            }
        }else{
            $this->required = 0;
        }
        
        if($this->foreignKeyRequired()==true){
            $col = \Yii::$app->db->getTableSchema($this->foreign_key_table)->getColumn($this->foreign_key_column);
            if ($col != null){
                $this->precision = $col->precision;
                $this->length = $col->size;
            }
        }
        if($this->name != $this->getOldAttribute('name')){
            //set old_name in case the attribute name is changed
            //and the old one exists in the table
            $entity = $this->entity;
            $tblName = Yii::$app->db->tablePrefix . $entity->name;
            
            $tblSchema = Yii::$app->db->getTableSchema($tblName, true);
            if($tblSchema){
                $column = $tblSchema->getColumn($this->getOldAttribute('name'));
                if($column){
                    $this->old_name = $this->getOldAttribute('name');
                }
            }
        }else{
            $this->old_name = '';
        }
        return parent::beforeSave($insert);
    }

    public static function sortArrayOfArray(&$array, $subfield,$attributeModel)
    {
        $model = $attributeModel;
        $ordersList = [];
        $tempList = [];
        foreach ($array as $k => $attr) {
            if ($attr['entity_id'] == $model->entity_id && $model->id != $attr['id']) {
                $ordersList[] = $attr['column_order'];
                if($model->column_order==$attr['column_order']){
                    $n = $attr['column_order']+1;
                    while(in_array($n,$ordersList) || in_array($n,$tempList)){
                        $n++;
                    }
                    $array[$k]['column_order'] = $n;
                    $tempList[] = $n;
                }else{
                    if(in_array( $attr['column_order'],$tempList)){
                        $n = $array[$k]['column_order'];
                        while(in_array($n,$tempList)){
                            $n++;
                        }
                        $array[$k]['column_order'] = $n;
                    }
                    $tempList[] = $array[$k]['column_order'];
                }
                
            }
        }
        
        $sortarray = array();
        foreach ($array as $key => $row) {
            $sortarray[$key] = $row[$subfield];
        }
        array_multisort($sortarray, SORT_ASC, $array);
    }

    public static function insertValueAtPosition($arr, $newValue, $position, $max)
    {
        if ($max > $position) {
            foreach ($max as $m) {
                if ($m == $position) {
                    $arr[$m]['column_content'] = $position + 1;
                }
            }
        } else {
            array_push($arr, $newValue);
        }
        return $arr;
    }

    /**
     *
     * @see ActiveRecord::insert()
     */
    protected function insertInternal($attributes = null)
    {
        if (! $this->beforeSave(true)) {
            return false;
        }
        $values = $this->getDirtyAttributes($attributes);
        if (empty($values)) {
            $currentAttributes = $this->getAttributes();
            foreach ($this->primaryKey() as $key) {
                if (isset($currentAttributes[$key])) {
                    $values[$key] = $currentAttributes[$key];
                }
            }
        }
        
        $db = static::getDb();
        $pkName = $db->primaryKeyName;
        if (! isset($values[$pkName])) {
            throw new Exception("'" . get_class($this) . "::{$pkName}' must be set.");
        }
        $dataFileName = static::fileName();
        $data = $db->readData($dataFileName);
        if (isset($data[$values[$pkName]])) {
            throw new Exception("'{$pkName}' value '{$values[$pkName]}' is already taken.");
        }
        $data[$values[$pkName]] = $values;
        //Attribute::sortArrayOfArray($data, 'column_order',$this);
        $db->writeData($dataFileName, $data);
        
        $changedAttributes = array_fill_keys(array_keys($values), null);
        $this->setOldAttributes($values);
        $this->afterSave(true, $changedAttributes);
        
        return true;
    }

    /**
     *
     * @see ActiveRecord::update()
     */
    protected function updateInternal($attributes = null)
    {
        if (! $this->beforeSave(false)) {
            return false;
        }
        $values = $this->getDirtyAttributes($attributes);
        if (empty($values)) {
            $this->afterSave(false, $values);
            return 0;
        }
        
        $db = static::getDb();
        $pkName = $db->primaryKeyName;
        $attributes = $this->getAttributes();
        if (! isset($attributes[$pkName])) {
            throw new Exception("'" . get_class($this) . "::{$pkName}' must be set.");
        }
        $dataFileName = static::fileName();
        $data = $db->readData($dataFileName);
        if (! isset($data[$attributes[$pkName]])) {
            throw new Exception("'{$pkName}' value '{$values[$pkName]}' does not exist.");
        }
        $changedAttributes = [];
        foreach ($values as $name => $value) {
            $data[$attributes[$pkName]][$name] = $value;
            $changedAttributes[$name] = $this->getOldAttribute($name);
            $this->setOldAttribute($name, $value);
        }
        /* if (key_exists('column_order', $changedAttributes)) {
            Attribute::sortArrayOfArray($data, 'column_order',$this);
        } */
        
        $db->writeData($dataFileName, $data);
        
        $this->afterSave(false, $changedAttributes);
        
        return 1;
    }
}
