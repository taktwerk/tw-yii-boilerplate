<?php
namespace tunecino\builder\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\gii\generators\model\Generator as GiiGenerator;

class Entity extends \yii2tech\filedb\ActiveRecord
{

    public static function getDb()
    {
        return \tunecino\builder\Module::getInstance()->get('filedb');
    }

    public function attributes()
    {
        return array_keys($this->attributeLabels());
    }
    /**
     * if columns can be reordered by drag n drop
     * @return boolean
     */
    public function isDragSortingAllowed(){
        /*
        Drag n Drop reordering is allowed if
        1. The entity table doesn't exists yet in the database
                OR
        2. The entity table and all the enity attributes & relationship exists in the database. If any
           attribute or relationship is present in entity but not in the
           actual table yet then sorting will not be allowed)
        */
       $models = \tunecino\builder\generators\migration\Generator::getMergedFieldsOfEntity($this);
       $tblName = \Yii::$app->db->tablePrefix . $this->name;
       if($tblName == null){
           return true;
       }
       $tblSchema = \Yii::$app->db->getTableSchema($tblName, true);
       if($tblName ==null || $tblSchema==null){
           return true;
       }

       foreach($models as $model){
           $columnName = (($model instanceof Attribute)?$model->name:$model->getRelationshipFieldName());
 
           if($tblSchema && $tblSchema->getColumn($columnName)==null){
               //If there is any Attribute/Relationship which is not generated yet
               //in the table then drag sorting is not allowed
               if ($model instanceof Attribute || ($model instanceof Relationship && $model->rel_type == Relationship::HAS_ONE)) {
               return false;
               }
           }
       }
       return true;
    }
    public static function setOrdersOfAtrAndRelation($modelOrEntityId)
    {
        $isObj = is_object($modelOrEntityId);
        if($isObj && !($modelOrEntityId instanceof Relationship) && !($modelOrEntityId instanceof Attribute)){
            throw new \InvalidArgumentException('You can either pass an entity id or an Attribute/Relationship model to this method');
        }else{
            if($modelOrEntityId==null){
                throw new \InvalidArgumentException('Entity id cannot be empty/null');
            }
        }
        $entityId = $isObj ? $modelOrEntityId->entity_id : $modelOrEntityId;
        $number = $isObj ? $modelOrEntityId->column_order : - 1;
        $modelId = $isObj ? $modelOrEntityId->id : - 1;

        $attributes = Attribute::findAll([
            'entity_id' => $entityId
        ]);
        $relationships = Relationship::findAll([
            'entity_id' => $entityId
        ]);
        $columns = array_merge($attributes, $relationships);
        usort($columns, function ($item1, $item2) {
            return $item1['column_order'] <=> $item2['column_order'];
        });
        if ($isObj) {
            foreach ($columns as $k => $c) {
                if ($c->id == $modelId) {
                    unset($columns[$k]);
                    break;
                }
            }
        }

        $i = 1;
        foreach ($columns as $col) {
            if ($col instanceof Relationship && $col->rel_type == Relationship::HAS_MANY) {
                continue;
            }
            if ($number == $i) {
                $i ++;
            }
            //$col->column_order = $i;
            $col->updateAttributes(['column_order'=>$i]);
            $i++;
           }
            return true;
    }
    public function rules()
    {
        return [
            [
                [
                    'name'
                ],
                'required'
            ],
            [
                [
                    'name'
                ],
                'filter',
                'filter' => 'strtolower'
            ],
            [
                [
                    'name',
                    'schema_id'
                ],
                'filter',
                'filter' => 'trim'
            ],
            [
                [
                    'name'
                ],
                'match',
                'pattern' => '/^\w+$/',
                'message' => 'Only word characters are allowed.'
            ],
            [
                [
                    'name'
                ],
                'validateKeyword',
                'skipOnEmpty' => false
            ],
            [
                'schema_id',
                'string'
            ],
            [
                'name',
                'unique',
                'targetAttribute' => [
                    'name',
                    'schema_id'
                ],
                'comboNotUnique' => 'Entity "{value}" has already been defined.'
            ],
            [
                'schema_id',
                'exist',
                'skipOnError' => true,
                'targetClass' => Schema::className(),
                'targetAttribute' => [
                    'schema_id' => 'id'
                ]
            ]
        ];
    }

    public function validateKeyword()
    {
        if ((new GiiGenerator())->isReservedKeyword($this->name)) {
            $this->addError('name', 'Class name cannot be a reserved PHP keyword.');
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Primary Key',
            'name' => 'Name',
            'schema_id' => 'Schema Id'
        ];
    }

    public function getRelatedAttributes()
    {
        return $this->hasMany(Attribute::className(), [
            'entity_id' => 'id'
        ])->orderBy('column_order asc');
    }

    public function getRelationships()
    {
        return $this->hasMany(Relationship::className(), [
            'entity_id' => 'id'
        ])->orderBy('column_order asc');
    }

    public function isHoldingForeignKey()
    {
        return ! ! $this->getRelationships()
            ->where([
            'rel_type' => Relationship::HAS_ONE
        ])
            ->count();
    }

    public function getRelEntities()
    {
        return $this->hasMany(Entity::className(), [
            'id' => 'related_to'
        ])->via('relationships');
    }

    public function getSchema()
    {
        return $this->hasOne(Schema::className(), [
            'id' => 'schema_id'
        ]);
    }

    public function getMigrationFields()
    {
        $mergedFields = array_merge($this->relationships,$this->relatedAttributes);
        usort($mergedFields, function ($item1, $item2) {
            return $item1['column_order'] <=> $item2['column_order'];
        });
        
        return implode(',',(array_filter(ArrayHelper::getColumn($mergedFields,'migrationField'))));
        /* return implode(',', 
            array_merge(
                array_filter(ArrayHelper::getColumn($this->relationships, 'migrationField')),
                array_filter(ArrayHelper::getColumn($this->relatedAttributes, 'migrationField'))
                )
            ); */
    }

    public function getCommentDatas()
    {
        return implode(',', array_merge(array_filter(ArrayHelper::getColumn($this->relationships, 'commentData')), array_filter(ArrayHelper::getColumn($this->relatedAttributes, 'commentData'))));
    }
    
    
    public function getPreviewUrl()
    {
        $fn = Yii::$app->controller->module->previewUrlCallback;
        if ($fn !== null && is_callable($fn))
            return call_user_func($fn, $this);
        
        $module = $this->schema->moduleID;
        $moduleName = $module ? $module . '/' : '';
        return \yii\helpers\Url::toRoute('/' . $moduleName . $this->name, true);
    }

    public function beforeSave($insert)
    {
        if ($insert)
            $this->id = uniqid();
        return parent::beforeSave($insert);
    }

    public function afterDelete()
    {
        Attribute::deleteAll([
            'entity_id' => $this->id
        ]);
        Relationship::deleteAll([
            'entity_id' => $this->id
        ]);
        return parent::afterDelete();
    }
}
