<?php

namespace tunecino\builder\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\widgets\ActiveForm;
use tunecino\builder\models\Schema;
use tunecino\builder\models\Entity;
use tunecino\builder\models\Attribute;
use tunecino\builder\models\Relationship;
use tunecino\builder\models\EntitySearch;
use yii\base\Exception;


class DefaultController extends Controller
{
    public $layout = 'main';

    private $_ajaxOnlyActions = [
        'create-schema',
        'update-schema',
        'create-entity',
        'update-entity',
        'create-attribute',
        'update-attribute',
        'attribute-extra-fields',
        'create-relationship',
        'update-relationship',
        'get-commands',
        'std',
        'get-columns',
        'get-schema-columns',
        'update-atr-order'
    ];

    
    public function beforeAction($action) {
        if (!parent::beforeAction($action)) return false;
        foreach ($this->_ajaxOnlyActions as $action) {
            if ($this->action->id === $action) {
                if (Yii::$app->request->isAjax === false)
                    throw new \yii\web\BadRequestHttpException();
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            }
        }
        return true;
    }


    public function actionIndex()
    {
        $schemaProvider = new ActiveDataProvider(['query' => Schema::find()]);
        $schemaProvider->pagination = ['defaultPageSize' => 12];
        
        return $this->render('index', [
            'schemaProvider' =>  $schemaProvider,
            'schema' => new Schema(),
        ]);
    }


    public function actionView($id)
    {
        $entitySearchModel = new EntitySearch();
        $entitySearchModel->schema_id = $id;

        $entityProvider = $entitySearchModel->search(Yii::$app->request->queryParams);
        $entityProvider->pagination = ['defaultPageSize' => 10];

        $schema = Schema::findOne($id);
        if ($schema === null) throw new NotFoundHttpException('The requested page does not exist.');

        return $this->render('view', [
            'schema' => $schema,
            'entitySearchModel' => $entitySearchModel,
            'entityProvider' => $entityProvider,
            'entity' => new Entity(['schema_id' => $id]),
        ]);
    }


    public function actionViewEntity($id)
    {
        $entity = Entity::findOne($id);
        if ($entity === null) throw new NotFoundHttpException('The requested page does not exist.');

        $juctionAttributes = [];
        foreach ($entity->relationships as $relationship) {
            if ($relationship->isManyToMany()) {
                $juctionAttributes[$relationship->relatedTo->name] = [
                    'attribute' => new Attribute(['entity_id' => $relationship->id, 'scenario' => 'junction']),
                    'provider' => new ActiveDataProvider(['query' => $relationship->getRelAttributes()])
                ];
            }
        }

        return $this->render('entity/view', [
            'model' => $entity,
            'attributeProvider' => new ActiveDataProvider(['query' => $entity->getRelatedAttributes()]),
            'relationshipProvider' => new ActiveDataProvider(['query' => $entity->getRelationships()]),
            'juctionAttributes' => $juctionAttributes,
            'relationship' => new Relationship(['entity_id' => $id]),
            'attribute' => new Attribute(['entity_id' => $id]),
        ]);
    }


    public function actionCreateSchema()
    {
        $model = new Schema();
        return $this->_save_or_validate_schema($model);
    }


    public function actionUpdateSchema($id)
    {
        $model = Schema::findOne($id);
        return $this->_save_or_validate_schema($model);
    }


    private function _save_or_validate_schema($schema)
    {
        $data = Yii::$app->request->post();
        $ajaxValidation = isset($data['ajax']);
        $schema->load($data);

        $generators = [];
        foreach ($schema->loadForms(true) as $formName => $generator) {
            $generator->load($data);
            $generators[$formName] = $generator;
        }

        if ($ajaxValidation) return array_merge(
            ActiveForm::validate($schema),
            call_user_func_array('\yii\widgets\ActiveForm::validate',$generators)
        );

        if ($schema->save() === false) return;

        $savedGenerators = [];
        foreach ($generators as $generator) {
            $generator->schema_id = $schema->id;
            $savedGenerators[] = $generator->save();
        }

        return count(array_unique($savedGenerators)) === 1 && current($savedGenerators);
    }


    public function actionCreateEntity()
    {
        $model = new Entity();
        $model->load(Yii::$app->request->post());
        return isset(Yii::$app->request->post()['ajax']) ? ActiveForm::validate($model) : $model->save();
    }


    public function actionUpdateEntity($id)
    {
        $model = Entity::findOne($id);
        return $model && $model->load(Yii::$app->request->post()) && $model->save();
    }


    public function actionCreateAttribute()
    {
        $data = Yii::$app->request->post();
        $ajaxValidation = isset($data['ajax']);
        $entity_id = isset($data['Attribute']) && isset($data['Attribute']['entity_id']) ? $data['Attribute']['entity_id'] : null;
        $isJunction = count(explode('-', $entity_id)) === 2;
        $maxValue = $this->getMaxOrderNumber($entity_id);
        $data['Attribute']['column_order'] = ($maxValue+1);
        $model = new Attribute();
        if ($isJunction) $model->scenario = Attribute::SCENARIO_JUNCTION;
        $model->load($data, 'Attribute');

        return $ajaxValidation ? ActiveForm::validate($model) : $model->save();
    }


    public function actionUpdateAttribute($id)
    {
        $data = Yii::$app->request->post();
        $ajaxValidation = isset($data['ajax']);

        $entity_id = isset($data['Attribute']) && isset($data['Attribute']['entity_id']) ? $data['Attribute']['entity_id'] : null;
        $isJunction = count(explode('-', $entity_id)) === 2;

        $model = Attribute::findOne($id);
        if ($isJunction) $model->scenario = Attribute::SCENARIO_JUNCTION;
        $model->load($data, 'Attribute');

        return $ajaxValidation ? ActiveForm::validate($model) : ($model->save() && Entity::setOrdersOfAtrAndRelation($model));
    }

    public function actionAttributeExtraFields($type)
    {
        $attribute = new Attribute([
            'type' => $type
        ]);
        return [
            'lengthRequired' => $attribute->lengthRequired(),
            'precisionRequired' => $attribute->precisionRequired(),
            'scaleRequired' => $attribute->scaleRequired(),
            'commentRequired' => $attribute->commentRequired(),
            'textRequired' => $attribute->textRequired(),
            'fileRequired' => $attribute->fileRequired(),
            'filesRequired' => $attribute->filesRequired(),
            'foreignKeyRequired' => $attribute->foreignKeyRequired(),
            'formFieldTypeRequired' => $attribute->formFieldTypeRequired(),
            'enumOptionsRequired' => $attribute->enumOptionsRequired()
        ];
    }

    public function actionGetColumns($tbl)
    {
        \Yii::$app->response->format = 'json';
        return Attribute::getValidForeignKeyColumnsFromTbl($tbl);
    }

    protected function getMaxOrderNumber($entityId){
        $relSavedData = Relationship::findAll(['entity_id'=>$entityId]);
        if(array_column($relSavedData, 'column_order')){
            $relMaxvalue = max(array_column($relSavedData, 'column_order'));
        }
        else {
            $relMaxvalue = "0";
        }
        
        $atrSavedData = Attribute::findAll(['entity_id'=>$entityId]);
        if(array_column($atrSavedData, 'column_order')){
            $atrMaxvalue = max(array_column($atrSavedData, 'column_order'));
        } else {
            $atrMaxvalue = "0";
        }
        
        return $relMaxvalue>$atrMaxvalue?$relMaxvalue:$atrMaxvalue;
    }
    public function actionGetAtrOrderList($entity_id){
        $entity = Entity::findOne($entity_id);
        $columns = \tunecino\builder\generators\migration\Generator::getMergedFieldsOfEntity($entity);
        return $this->renderAjax('entity/_atr-rel-order-list',['models'=>$columns,'sortable'=>$entity->isDragSortingAllowed()]);
    }
    public function actionUpdateAtrOrder($entity_id){
        $newOrder = \Yii::$app->request->post('newOrder');
        $attribute = \Yii::$app->request->post('atr');
        
        $entity = Entity::findOne($entity_id);
        $forms = $entity->schema->loadForms();
        \Yii::$app->response->format='json';
        $response = ['success'=>false,'message'=>\Yii::t('twbp','Sorry, reordering failed')];
        if($entity->isDragSortingAllowed()==false){
            return $response = ['success'=>false,'message'=>\Yii::t('twbp','Ordering is only allowed if all the entity fields already exists in the table, Please GENERATE UPDATES first before reordering.')];
        }
        /**
        @var $generator \tunecino\builder\generators\migration\Generator
        */
        $generator = $forms['Migration']['model'];
        if($generator && is_numeric($newOrder) && $attribute)
        {
            $entity  = Entity::findOne($entity_id);
            $tblName = Yii::$app->db->tablePrefix . $entity->name;
            $tblSchema = Yii::$app->db->getTableSchema($tblName, true);
            $columns = $generator->getMergedFieldsOfEntity($entity);
            //If Table exists then we run the order change migration right away
            //otherwise we just update the column order in schema
            if($tblSchema){
                $cmds = $generator->moveColumn($entity, $attribute, $newOrder);
                if(is_array($cmds)){
                    $p  = [];
                    foreach($cmds as $cmd){
                        sleep(1);//Sleeps needs to be added to avoid the same timestamp for two diffrent files 
                        list ($status, $output) = $this->runConsole($cmd);
                        $p[] = $output;
                    }
                    $upcmd = 'migrate/up';
                    if ($generator->migrationPath){
                        $upcmd .= ' --migrationPath="' . $generator->migrationFolder . '"';
                    }
                    if ($generator->migrationTable){
                        $upcmd .= ' --migrationTable="' . $generator->migrationTable . '"';
                    }
                    $upcmd .= ' --interactive=0';
                    list ($status, $output) = $this->runConsole($upcmd);
                    $p[] = $output;
                    $response = ['success'=>true,'message'=>\Yii::t('twbp','Reordering successful'),'cmd'=>$cmds,'output'=>$p];
    
                }
                if($cmds==false){
                    $response = ['success'=>false,'message'=>\Yii::t('twbp','Sorry couldn\'t reorder the column')];
                }elseif($cmds===true){
                    $response = ['success'=>true,'message'=>\Yii::t('twbp','Reordering successful')];
                }
            }else{
                $response = ['success'=>true,'message'=>\Yii::t('twbp','Reordering successful')];
            }
        }
            if($response['success']==true){
                $sAtrribute = null; 
                foreach($columns as $c){
                    $name = null;
                    if ($c instanceof Attribute) {
                        $name = $c->name;
                    } else {
                        if ($c instanceof Relationship && $c->rel_type == Relationship::HAS_ONE) {
                            $name = $c->getRelationshipFieldName();
                        }
                    }

                    if($name && $name==$attribute){
                        $c->updateAttributes(['column_order'=>$newOrder+1]);
                        $sAtrribute = $c;
                        break;
                    }
                }
                Entity::setOrdersOfAtrAndRelation($sAtrribute);
            }
        return $response;
    }
    public function actionCreateRelationship()
    {
        $data = Yii::$app->request->post();
        $ajaxValidation = isset($data['ajax']);
        $entity_id = isset($data['Relationship']) && isset($data['Relationship']['entity_id']) ? $data['Relationship']['entity_id'] : null;
        
        if($data['Relationship']['rel_type']==Relationship::HAS_ONE){
            $maxValue = $this->getMaxOrderNumber($entity_id);
            $data['Relationship']['column_order'] = ($maxValue+1);
        }
        $model = new Relationship();
        $model->load($data, 'Relationship');

        if ($ajaxValidation) return ActiveForm::validate($model);

        if ($model->validate() && $model->isDuplicationOfSame() === false) {
            $revModel = new Relationship();
            $revModel->entity_id = $model->related_to;
            $revModel->related_to = $model->entity_id;
            $revModel->rel_type = $model->reversed;
            $revModel->reversed = $model->rel_type;
            return $revModel->save() && $model->save(false);
        }

        return $model->isDuplicationOfSame() && $model->save(false) && Entity::setOrdersOfAtrAndRelation($model);
    }

    
    public function actionUpdateRelationship($id)
    {
        $data = Yii::$app->request->post();
        $ajaxValidation = isset($data['ajax']);
        
        $model = Relationship::findOne($id);
        $preColumnOrder = $model->column_order;
        $model->load($data, 'Relationship');
        if($model->rel_type != Relationship::HAS_ONE){
            $model->column_order = null;
        }
        if($model->rel_type == Relationship::HAS_ONE && $model->column_order==null){
            $maxValue = $this->getMaxOrderNumber($model->entity_id);
            if($preColumnOrder==$maxValue){
                $model->column_order = $preColumnOrder;
            }else{
                $model->column_order = ($maxValue+1);
            }
        }
        if ($ajaxValidation) return ActiveForm::validate($model);

        $revModel = $model->reversedRelation;
        if ($revModel && $model->validate()) {
            $revModel->rel_type = $model->reversed;
            if($revModel->rel_type == Relationship::HAS_ONE && $revModel->column_order==null){
                $revModel->column_order = $this->getMaxOrderNumber($revModel->entity_id);
            }else{
                $revModel->column_order = null;
            }
            return $revModel->save() && $model->save(false) && Entity::setOrdersOfAtrAndRelation($model);
        }
    }


    public function actionDeleteRelationship($id)
    {
        $model = Relationship::findOne($id);
        if ($model && Yii::$app->request->method === 'POST') {
            
            $model->delete(); Entity::setOrdersOfAtrAndRelation($model->entity_id);
            $this->redirect(['view-entity','id'=>$model->entity_id]);
            //return $this->actionViewEntity($model->entity_id);
        }
    }


    public function actionDeleteAttribute($id, $entity_id = null)
    {
        $model = Attribute::findOne($id);
        if ($model && Yii::$app->request->method === 'POST'){
            $model->column_order = -1;
            $model->delete(); Entity::setOrdersOfAtrAndRelation($model->entity_id);
            $this->redirect(['view-entity','id'=>$entity_id ?: $model->entity_id]);
            //return $this->actionViewEntity($entity_id ?: $model->entity_id);
        }
    }


    public function actionDeleteEntity($id)
    {
        $model = Entity::findOne($id);
        if ($model && Yii::$app->request->method === 'POST') {
            $model->delete();
            return $this->redirect(['view', 'id' => $model->schema_id]);
        }
    }


    public function actionDelete($id)
    {
        $model = Schema::findOne($id);
        if ($model && Yii::$app->request->method === 'POST') {
            $model->delete();
            return $this->redirect(['index']);
        }
    }


    public function actionStd()
    {
        $post = Yii::$app->request->post();
        list ($status, $output) = $this->runConsole($post['cmd']);
        return $output;
    }


    private function runConsole($command)
    {
        // source: https://github.com/samdark/yii2-webshell/blob/master/controllers/DefaultController.php
        $cmd = Yii::getAlias($this->module->yiiScript) . ' ' . $command . ' 2>&1';
        $handler = popen($cmd, 'r');
        $output = '';
        while (!feof($handler)) {
            $output .= fgets($handler);
        }
        $output = trim($output);
        $status = pclose($handler);
        return [$status, $output];
    }


    public function actionGetCommands($id,$update=null)
    {
        $schema = Schema::findOne($id);
        if ($schema === null) throw new NotFoundHttpException('The requested page does not exist.');
        if($update=='yes')
        return $schema->consoleCommands['update'];
        
        return $schema->consoleCommands['create'];
    }
}