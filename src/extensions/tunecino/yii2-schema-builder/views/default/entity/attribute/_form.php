<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use tunecino\builder\models\Attribute;
use taktwerk\yiiboilerplate\widget\FileInput;
use wbraganca\tagsinput\TagsinputWidget;
use yii\web\View;

/**
*    @var $this yii\web\View 
*    @var $model \tunecino\builder\models\Attribute
*    @var $form yii\widgets\ActiveForm
*/
$rid = uniqid();
$css = <<<EOT
.bootstrap-tagsinput{
border-color:#bdc3c7;
height:auto;
}
EOT;
$this->registerCss($css,'tag-input-schema');
?>

<div class="attribute-form">

    <?php
    
    $form = ActiveForm::begin([
        'options' => [
            'data-type' => 'ajax'
        ],
        'action' => $model->isNewRecord ? [
            'create-attribute'
        ] : [
            'update-attribute',
            'id' => $model->id
        ],
        'enableClientValidation' => false,
        'enableAjaxValidation' => true
    ]);
    ?>

    <?php
    Modal::begin([
        'id' => 'attribute-' . ($model->isNewRecord ? 'new-' . $rid : $model->id . '-' . $rid),
        'size' => Modal::SIZE_LARGE,
        'header' => $model->isNewRecord ? '<h6>Create Attribute</h6>' : '<h6>Update Attribute ' . $model->name . '</h6>',
        'toggleButton' => $model->isNewRecord ? [
            'label' => '<span class="fui-plus"></span>',
            'class' => 'btn btn-primary pull-right'
        ] : [
            'label' => '',
            'tag' => 'a',
            'class' => 'glyphicon glyphicon-pencil text-default',
            'data-pjax' => 0
        ],
        'footer' => Html::resetButton('Reset', [
            'class' => 'btn btn-default'
        ]) . Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
            'class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-info'
        ])
    ]);
    ?>

    <div class="row text-left">
		<div class="col-sm-6"
			style="border-right: 1px solid #ddd; padding: 0 25px;">
        <?=$form->field($model, 'type')
        ->dropDownList($model->types, 
            ['prompt' => '','data' => ['url' => Url::to(['default/attribute-extra-fields'], true),'rid' => $rid],
'onchange' => <<<EOT
                var url = $(this).data('url'),
                    rid = $(this).data('rid'),
                    type = $(this).val();

                var lengthField = $('.field-length-' + rid),
                    foreignTblField = $('.field-foreign_tbl-' + rid),
                    foreignColField = $('.field-foreign_col-' + rid),
                    precisionField = $('.field-precision-' + rid),
                    scaleField = $('.field-scale-' + rid);
                    foreignTblField = $('.field-foreign_tbl-' + rid);
                    richTextField = $('.field-rich_text-' + rid);
                    advancedModeField = $('.field-advanced_mode-' + rid);
                    keepMetaField = $('.field-keep_meta_data-' + rid);
                    enumOptField = $('.field-enum-options-' + rid);
                    formFieldTypeField = $('.field-form_field_type-' + rid);

                    fileSizeField = $('.field-file_size-' + rid);
                    fileExtensionField = $('.field-file_extension-' + rid);
                    requireField = $('.field-required-' + rid);
                    fileMinField = $('.field-min_file_count-' + rid);
                    fileMaxField = $('.field-max_file_count-' + rid);
                    $(this).closest('form').find('input:text:not([name="Attribute[name]"]), input:password, input:file, textarea').val('');
                    $(this).closest('form').find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
                    $.get(url, {type: type}, function(fields) {
                    fields.foreignKeyRequired ? foreignColField.show() : foreignColField.hide();
                    fields.foreignKeyRequired ? foreignTblField.show() : foreignTblField.hide();
                    fields.lengthRequired ? lengthField.show() : lengthField.hide();
                    fields.precisionRequired ? precisionField.show() : precisionField.hide();
                    fields.scaleRequired ? scaleField.show() : scaleField.hide();
                    fields.commentRequired ? advancedModeField.show() : advancedModeField.hide();
                    fields.commentRequired ? keepMetaField.show() : keepMetaField.hide();
                    fields.textRequired ? richTextField.show() : richTextField.hide();
                    fields.fileRequired ? fileSizeField.show() : fileSizeField.hide();
                    fields.fileRequired ? fileExtensionField.show() : fileExtensionField.hide();
                    fields.fileRequired ? requireField.hide() : requireField.show();
                    fields.filesRequired ? fileMinField.show() : fileMinField.hide();
                    fields.filesRequired ? fileMaxField.show() : fileMaxField.hide();
                    fields.enumOptionsRequired?enumOptField.show():enumOptField.hide();
                    fields.formFieldTypeRequired ? formFieldTypeField.show():formFieldTypeField.hide();
                });
EOT
])?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        
        <?= $form->field($model, 'entity_id')->hiddenInput()->label(false) ?>

        <hr>

			<div class="form-group field-required-<?= $rid ?>"
			<?= ($model->type == Attribute::TYPE_FILE || $model->type == Attribute::TYPE_FILES)? 'style="display:none;"' : '';  ?>>
				<label class="checkbox" for="req-<?= $rid ?>"> <input type="hidden"
					name="Attribute[required]" value="0"> <input
					name="Attribute[required]" type="checkbox" data-toggle="checkbox"
					value="1" id="req-<?= $rid ?>"
					<?= $model->required ? 'checked' : '' ?>> Required
				</label>
			</div>

			<div class="form-group">
				<label class="checkbox" for="unq-<?= $rid ?>"> <input type="hidden"
					name="Attribute[unique]" value="0"> <input name="Attribute[unique]"
					type="checkbox" data-toggle="checkbox" value="1"
					id="unq-<?= $rid ?>" <?= $model->unique ? 'checked' : '' ?>> Unique
				</label>
			</div>


		</div>

		<div class="col-sm-6" style="padding: 0 25px;">
		<?php
		$tblKey = array_combine($model->getNonSchemaTables($model->entity),$model->getNonSchemaTables($model->entity));
			
			?>
			<?= $form->field($model, 'foreign_key_table',$model->foreignKeyRequired() ? [] : ['options' => ['style' => 'display:none;']])
			->dropDownList($tblKey,['id' => 'foreign_tbl-' . $rid,'prompt'=>\Yii::t('twbp', 'Select Table') ,
			    'data' => ['url' => Url::to(['default/get-columns'], true),'rid' => $rid],
			    'onchange' =>  
<<<EOT
    var url = $(this).data('url'),
    rid = '{$rid}',
    value = $(this).val();
    $.get(url, {tbl: value}, function(fields) {
        var el =  $('select#foreign_col-{$rid}');
        el.empty();
        if(fields.length!=1){
            el.append(new Option('Select Column', ''));
        }
        fields.forEach(function(vl){
            el.append(new Option(vl, vl));
        });
        
});
   
EOT
			]);?>
			<?php 
			$foreignKeyColumns =[];
			if($model->foreign_key_table){
			    $foreignKeyColumns = Attribute::getValidForeignKeyColumnsFromTbl($model->foreign_key_table);
			    $foreignKeyColumns = array_combine($foreignKeyColumns,$foreignKeyColumns);
			}
			echo $form->field($model, 'foreign_key_column',$model->foreignKeyRequired() ? [] :
			    ['options' => ['style' => 'display:none;']])
			    ->dropDownList(
			        $foreignKeyColumns,
			        ['id' => 'foreign_col-' . $rid,'prompt'=>'Select Column']);?>
			
    		
    		<?= $form->field($model, 'length', $model->lengthRequired() ? [] : ['options' => ['style' => 'display:none;']])->textInput(['id' => 'length-' . $rid]) ?>
            <?= $form->field($model, 'precision', $model->precisionRequired() ? [] : ['options' => ['style' => 'display:none;']])->textInput(['id' => 'precision-' . $rid]) ?>
            <?= $form->field($model, 'scale',$model->scaleRequired() ? [] : ['options' =>['style' => 'display:none;']])->textInput(['id' => 'scale-' . $rid]) ?>
            <?php echo $form->field($model, 'enum_options',
                $model->enumOptionsRequired() ? [] : ['options' => ['style' => 'display:none;']])
            ->widget(TagsinputWidget::classname(), [
                'options' =>[ 'id' => 'enum-options-' . $rid,'class'=>'form-control'],
                'clientOptions' => [
                    'trimValue' => true,
                    'allowDuplicates' => false
                ]
            ]);  ?>
            
            <?= $form->field($model, 'form_field_type', $model->formFieldTypeRequired() ? [] : 
                ['options' => ['style' => 'display:none;']])
            ->dropDownList([Attribute::FORM_FIELD_TYPE_CHECKBOX=>'Checkbox',Attribute::FORM_FIELD_TYPE_RADIO=>'Radio button'],[
                'id' => 'form_field_type-' . $rid
            ]) ?>
            
            <?= $form->field($model, 'file_size',$model->fileRequired() ? [] : ['options' => ['style' => 'display:none;']])->textInput(['id' => 'file_size-' . $rid]); ?>
            <?= $form->field($model, 'file_size', $model->fileRequired() ? [] : ['options' => ['style' => 'display:none;']])->textInput(['id' => 'file_size-' . $rid]) ?>
            <?= $form->field($model, 'file_extension', $model->fileRequired() ? [] : ['options' => ['style' => 'display:none;']])
            ->textInput(['id' => 'file_extension-' . $rid,'placeholder'=>'jpg, png'])
->hint('You can either specify the file types or extensions comma seperated. Available File Types: '.implode(', ', FileInput::$availableFileTypes)); ?>
            <?= $form->field($model, 'min_file_count', $model->filesRequired() ? [] : ['options' => ['style' => 'display:none;']])->textInput(['id' => 'min_file_count-' . $rid,'type' => 'number']) ?>
            <?= $form->field($model, 'max_file_count', $model->filesRequired() ? [] : ['options' => ['style' => 'display:none;']])->textInput(['id' => 'max_file_count-' . $rid, 'type' => 'number']) ?>
            <?php $checkedAdvanced = ($model->advanced_mode) ? 'checked' : ''; $advancedModeFieldTemplate = '<label class="checkbox" for="advanced_mode-'. $rid.'"> <input type="hidden" name="Attribute[advanced_mode]" value="0"> <input name="Attribute[advanced_mode]" type="checkbox" data-toggle="checkbox" value="1" id="advanced_mode-'.$rid.'" '.$checkedAdvanced.'> Advanced Mode </label>'; ?>
            <?= $form->field($model, 'advanced_mode', $model->commentRequired() ? ['template' => $advancedModeFieldTemplate,'options' => ['class' => 'form-group']] : ['template' => $advancedModeFieldTemplate,'options' => ['style' => 'display:none', 'class' => 'form-group']])->checkbox(['id' => 'advanced_mode-' . $rid], false) ?>
            <?php $checkedKeepMeta = ($model->keep_meta_data) ? 'checked' : ''; $keepMetaModeFieldTemplate = '<label class="checkbox" for="keep_meta_data-'. $rid.'"> <input type="hidden" name="Attribute[keep_meta_data]" value="0"> <input name="Attribute[keep_meta_data]" type="checkbox" data-toggle="checkbox" value="1" id="keep_meta_data-'.$rid.'" '.$checkedKeepMeta.'> Keep Meta Data </label>'; ?>
            <?= $form->field($model, 'keep_meta_data', $model->commentRequired() ? ['template' => $keepMetaModeFieldTemplate,'options' => ['class' => 'form-group']] : ['template' => $keepMetaModeFieldTemplate,'options' => ['style' => 'display:none', 'class' => 'form-group']])->checkbox(['id' => 'keep_meta_data-' . $rid], false) ?>
            <?php $checkedRich = ($model->rich_text) ? 'checked' : ''; $richTextFieldTemplate = '<label class="checkbox" for="rich_text-'. $rid.'"> <input type="hidden" name="Attribute[rich_text]" value="0"> <input name="Attribute[rich_text]" type="checkbox" data-toggle="checkbox" value="1" id="rich_text-'.$rid.'" '.$checkedRich.'> Rich Text Field </label>'; ?>
            <?= $form->field($model, 'rich_text', 
                $model->textRequired() ? 
                ['template' => $richTextFieldTemplate,
                    'options' => ['class' => 'form-group']] : 
                ['template' => $richTextFieldTemplate,
                'options' => ['style' => 'display:none', 'class' => 'form-group']])
                ->checkbox(['id' => 'rich_text-' . $rid], false) ?>
            <?php 
            if(!$model->isNewRecord)
            {
            ?>
            <?= $form->field($model, 'column_order')->textInput([
                'disabled'=>true,
                'readOnly'=>true, 'maxlength' => true,'placeholder'=>'Leave this blank to autoset the order']) ?>
            <?php } ?>
            <?= $form->field($model, 'default')->textInput(['maxlength' => true]) ?>
    	</div>

    <?= Html::hiddenInput('reload-pjax', 'attribute-form', ['disabled' => true, 'data-close-modal' => 'true']); ?>
    <?= Html::hiddenInput('reload-pjax', $model->name . '-attribute-form'); ?>
    <?= Html::hiddenInput('reload-pjax', 'attribute-list', ['disabled' => true]); ?>
    <?= Html::hiddenInput('reload-pjax', 'relationship-list', ['disabled' => true]); ?>

    </div>

    <?php Modal::end(); ActiveForm::end(); ?>

</div>


<?php
// http://designmodo.github.io/Flat-UI/docs/components.html#fui-checkbox
$this->registerJs(<<<JS
jQuery(':checkbox').radiocheck();
JS
);
$this->registerJs(<<<JS
$('.bootstrap-tagsinput').addClass('form-control');
JS
,View::POS_LOAD);
?>