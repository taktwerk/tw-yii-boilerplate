<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\web\View;
use kartik\sortable\Sortable;
use kartik\sortable\SortableAsset;

/* @var $this yii\web\View */
/* @var $model tunecino\models\Entity */
/* @var $form yii\widgets\ActiveForm */
SortableAsset::register($this);
$drapDropAllowed = $model->isDragSortingAllowed();
?>
<?php if(!$model->isNewRecord){?>
<span class="entity-orders">
   <?php
    $orderModalId = 'entity-order' . $model->id;
    Modal::begin([
        'id' => $orderModalId,
        'size' => modal::SIZE_LARGE,
        'bodyOptions'=>[
            'style'=>'max-height: calc(100vh - 150px);overflow-y:auto;padding-bottom: 10px;',
            'class' => 'modal-body'
        ],
        'options' => ['data-backdrop' => 'false', 'data-dismiss' => 'modal' ],
        'header' =>'<h6 class="text-center">Column orders '.$model->schema->name.'</h6>',
        'toggleButton' =>['label' => '<span class="fui-list-numbered"></span> ', 'class' => 'btn btn-info', 'title'=>'View Columns Generation Order'],
    ]);
   ?>
   <?php if($drapDropAllowed==false){?>
   <p>To re-order the columns please make sure there are no updates pending for this table/entity on this <?php 
    echo Html::a('page',['/builder/default/view','id'=>$model->schema->id],['target'=>'_blank', 'data'=>['pjax'=>0]])?></p>
   <?php }?>
<ul style="font-size:20px" class="list-group" id="sortable-<?php echo $orderModalId?>">
</ul>
    <?php Modal::end();?>
    <?php
    $ulId = "sortable-".$orderModalId;
    $listUrl = Url::toRoute(['/builder/default/get-atr-order-list','entity_id'=>$model->id]);
    $updateOrderUrl =  Url::toRoute(['/builder/default/update-atr-order','entity_id'=>$model->id]);
    if($drapDropAllowed==false){
        $sortableInitJs = '';
    }else{
    $sortableInitJs = <<<EOT
setTimeout(function(){
                $('#{$ulId}').kvHtml5Sortable({handle:'.handle'});
                $('#{$ulId}').kvHtml5Sortable('enable');
            },50);
EOT;
        }
    $js = <<<EOT
function setOrderList(){
    var el = $('#{$ulId}');
    $.ajax({
        url:"{$listUrl}",
        success:function(html){
            el.empty().append(html);
            $sortableInitJs
        }
    });
}
setOrderList();
$('#$orderModalId').on('show.bs.modal', function (e) {
        setOrderList();
});
EOT;
    $orderingJs = '';
    if($drapDropAllowed){
    $orderingJs = <<<JS

sortable('#{$ulId}')[0].addEventListener('sortupdate', function(e) {
    
    if(confirm("This will move the columns and run the migrations right now if the entity table exists")){
        var atr = $(e.detail.item).data('name');
        var newOrder = e.detail.destination.index;
        console.log('Move from ' + e.detail.origin.index+ ' to '+ e.detail.destination.index);
        $('#{$orderModalId} .modal-body').addClass('modal-ajax-loading');
        $.ajax({
                method:'post',
                data:{atr:atr,newOrder:newOrder},
                url:'{$updateOrderUrl}',
                complete:function(){
                   setOrderList();
                   $.pjax.reload({container: '#attribute-list', async: false});
                   $.pjax.reload({container:'#relationship-list', async: false});
                   $('#{$orderModalId} .modal-body').removeClass('modal-ajax-loading');
                },
                error:function(){
                    alert('Encountered an error while reordering');
                },
                success:function(data){
                   if(data.success==false){
                       alert(data.message);
                   }
                }
        });
    }else{
    setOrderList();
}
});
JS;
    }
     $this->registerJs($js.$orderingJs,View::POS_READY);
    ?> 
</span>
<?php }?>

<span class="entity-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'options' => ['data-type'=>'ajax', 'style' => ['display' => 'inherit']],
        'action' => $model->isNewRecord ? ['create-entity'] : ['update-entity', 'id' => $model->id],
        'enableAjaxValidation' => true
    ]); ?>

    <?php
    Modal::begin([
        'id' => 'entity-' . ($model->isNewRecord ? 'new' : $model->id),
        'size' => modal::SIZE_LARGE,
        'options' => ['data-backdrop' => 'false', 'data-dismiss' => 'modal' ],
        'header' =>  $model->isNewRecord ? 
            '<h6 class="text-center">Create & add a new Entity to Schema '.$model->schema->name.'</h6>' : '<h6 class="text-center">Update Entity '.$model->name.'</h6>',
        'toggleButton' => $model->isNewRecord ? 
            ['label' => '<span class="fui-plus"></span> NEW', 'class' => 'btn btn-primary pull-right'] :
            ['label' => '<span class="fui-new"></span> UPDATE', 'class' => 'btn btn-info'] ,
        'footer' => 
            Html::resetButton('Reset', ['class' => 'btn btn-default']) .
            Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-info'])
    ]);
    ?>

    <?= $form->field($model, 'schema_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <?= Html::hiddenInput('reload-pjax', 'entity-form', ['disabled' => true, 'data-close-modal' => 'true']); ?>
    <?= Html::hiddenInput('reload-pjax', 'entity-list', ['disabled' => true]); ?>
    <?= Html::hiddenInput('reload-pjax', 'entity-info', ['disabled' => true]); ?>
    <?= Html::hiddenInput('reload-pjax', 'entity-title', ['disabled' => true]); ?>
    <?= Html::hiddenInput('reload-pjax', 'breadcrumbs', ['disabled' => true]); ?>
    <?php Modal::end();?> 
    <?php ActiveForm::end(); ?>
</span>