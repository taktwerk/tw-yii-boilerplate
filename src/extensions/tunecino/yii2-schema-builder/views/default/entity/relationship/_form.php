<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model tunecino\models\Relationship */
/* @var $form yii\widgets\ActiveForm */
$css = <<<EOT
.help-block.help-block-error {
    text-align: left;
}

EOT;
$this->registerCss($css);
?>

<div class="relationship-form">

    <?php $form = ActiveForm::begin([
        'id'=>$model->isNewRecord?'relation-form-create':'relation-form-update'.$model->id,
        'layout' => 'horizontal',
        'options' => ['data-type'=>'ajax', 'style' => ['display' => 'inherit']],
        'action' => $model->isNewRecord ? ['create-relationship'] : ['update-relationship', 'id' => $model->id],
        'enableClientValidation' => false,
        'enableAjaxValidation' => true
    ]); ?>

    <?php
    Modal::begin([
        'id' => 'relationship-' . ($model->isNewRecord ? 'new' : $model->id),
        'header' =>  $model->isNewRecord ? "<h6>add new relation to <b><i>".$model->entity->name."</i></b></h6>" : "<h6>update <b><i>".$model->entity->name."-".$model->relatedTo->name."</i></b> relationship</h6>",
        'toggleButton' => $model->isNewRecord ? 
            ['label' => '<span class="fui-plus"></span>', 'class' => 'btn btn-primary pull-right'] :
        ['label' => '', 'tag' => 'a', 'class' => 'glyphicon glyphicon-pencil','data-pjax' => 0] ,
        'footer' => 
            Html::resetButton('Reset', ['class' => 'btn btn-default']) .
            Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-info'])
    ]);
    ?>

    <?= $form->field($model, 'entity_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'rel_type')->dropDownList($model->types, ['prompt' => '',
        'onchange'=> <<<EOT
                            setTimeout(function(){ \$('#relationship-related_to').trigger('change');},80);
        EOT
    ])?>

    <?php 
        if ($model->isNewRecord) echo $form->field($model, 'related_to')->dropDownList(
            ArrayHelper::map($model->entity->schema->entities, 'id', 'name'), 
               [
                'prompt' => '',
                'onchange'=> <<<EOT
                                $(".reversed-rel-to").text(this.options[this.selectedIndex].text);
                                $(".field-relationship-reversed").show();
                                var val = $(this).val();
                                var relType = $('#relationship-rel_type').val();
                                var currentTblName = '{$model->entity->id}';
                                if(currentTblName == val && relType=='hasOne'){
                                    $(".field-relationship-column_name").show();
                                }else{
                                    $(".field-relationship-column_name").hide();
                                }
            EOT,
            ]
            
           
         );

        else echo $form->field($model->relatedTo, 'name')->textInput(['readonly' => true]);
    ?>
    <?= $form->field($model, 'column_name' ,
        [
            'options'=>
                    [
                        'class'=>'form-group' ,
                        'style'=>($model->column_name=='')?'display:none':'',
                    ]
        ])->textInput(['readonly' => ($model->column_name=='')?false:true])?>
    <?php 
            if(!$model->isNewRecord)
            {
            ?>
    <?= $form->field($model, 'column_order')
    ->input('number',[
        'disabled'=>true,
        'readOnly'=>true,
        'placeholder'=>'Leave this blank to autoset the order']) ?>
    <?php }?>
    <div class="form-group field-relationship-required">
    <?php $reqId= 'relationship-required'.rand(1,100);?>
       <?php   /*  <label class="control-label col-sm-3" for="<?= $reqId?>">Required</label>
        <div class="col-sm-6">
		<?php $checkedNull = ($model->required) ? 'checked' : '';
$isNullFieldTemplate = '<label class="checkbox  mt-8" for="'.$reqId.'"> <input type="hidden" name="Relationship[required]" value="0"> <input name="Relationship[required]"
 type="checkbox" data-toggle="checkbox" value="1" id="'.$reqId.'" '.$checkedNull.'> </label>{error}'; ?>
            <?php   /* echo $form->field($model, 'required',
                [
                    'template' => $isNullFieldTemplate,
                    'options' => ['class' => 'form-group','tag'=>false]])
                ->checkbox(['id' => $reqId], false) 
            ?>
                
               
         </div>*/ 
            ?>

         </div>
         <div class="required-check-parent">
<?php echo $form->field($model, 'required', 
[
    'options' => [
        'class' => 'form-group'
    ],
   
]
)->checkbox([
    'id' => $reqId,
    'class' => 'custom-checkbox',
    'data' => [
        'toggle' => "checkbox"
    ],
], 
    true)->label('Required',['class' => 'control-label col-sm-3']);
            ?>
            </div> 
    <?= $form->field($model, 'reversed', ['options' => $model->isNewRecord ? ['style'=>'display:none'] : ['class'=>'form-group'] ])
             ->label('Reversed Case')
             ->radioList($model->inversedRelationLabels,
                [
                    'item' => $model-> isNewRecord
                    ? function($index, $label, $name, $checked, $value) {
                        $item = '<label class="radio text-left">';
                        $item .= '<input type="radio" name="' . $name . '" value="' . $value . '">';
                        $item .= $label;
                        $item .= '</label>';
                        return $item;
                    }
                    : function($index, $label, $name, $checked, $value) {
                        $isChecked = $checked ? 'checked' : '';
                        $item = '<label class="radio text-left">';
                        $item .= '<input type="radio" name="' . $name . '" value="' . $value . '" '. $isChecked .'>';
                        $item .= $label;
                        $item .= '</label>';
                        return $item;
                    }
                ])
    ?>
    <?= Html::hiddenInput('reload-pjax', 'relationship-form', ['disabled' => true, 'data-close-modal' => 'true']); ?>
    <?= Html::hiddenInput('reload-pjax', 'attribute-list', ['disabled' => true]); ?>
    <?= Html::hiddenInput('reload-pjax', 'relationship-list', ['disabled' => true]); ?>

    <?php Modal::end(); ActiveForm::end(); ?>

</div>

<?php
// http://designmodo.github.io/Flat-UI/docs/components.html#fui-checkbox
$this->registerJs(<<<JS
jQuery(':checkbox').radiocheck();
jQuery(':radio').radiocheck();
JS
);
?>