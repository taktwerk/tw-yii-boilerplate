<?php use tunecino\builder\models\Attribute;
use tunecino\builder\models\Relationship;
/* @var $models \tunecino\builder\models\Attribute|\tunecino\builder\models\Relationship */
?>
<?php
$handleHtml = '<span class="handle" style="cursor: move;"><i class="glyphicon glyphicon-move"></i> </span>';
if($sortable==false){
    $handleHtml = '';
}
$i=1;foreach($models as $model){
if($model instanceof Relationship && $model->rel_type==Relationship::HAS_MANY){
    continue;
}
$columnName = (($model instanceof Attribute)?$model->name:$model->getRelationshipFieldName());
?>
<li class="list-group-item" data-name="<?php echo $columnName?>">
<?php echo $handleHtml;?>
 <?php echo ($i.'. ').(($model instanceof Attribute)?$model->name:'<i>Relation: </i>'. 
    $model->relatedTo->name)?></li>
<?php $i++;}?>