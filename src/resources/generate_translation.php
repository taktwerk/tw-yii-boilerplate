<?php

function array_to_csv($data,$args=false) {
	$csv = '';
	foreach($data as $key => $value) {
		$csv .= '"' . $key . '";"' . $value . '"' . "\r\n";
	}
	file_put_contents('csv.csv', $csv);
}

function generate_translation() {
	$content = [];
	$row = 1;
	$languages = [];

	if (($handle = fopen(__DIR__ . DIRECTORY_SEPARATOR . "translation.csv", "r")) !== FALSE) {
	    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
	        $num = count($data);

	        for ($c=0; $c < $num; $c++) {
		        if ($row == 1) {
		        	if($c > 0) {
		        		$languages[] = $data[$c];
		        	}
				}
				else {
					if($c > 0) {
						foreach($languages as $key => $language) {
							if($key == $c-1) {
								$content[$language][addslashes($data[0])] = addslashes($data[$c]);
							}
						}
					}
				}
	        }

	        $row++;
	    }
	    fclose($handle);
	}
	echo '<pre>';
	print_r($languages);
	print_r($content);

	foreach($content as $language => $data) {

		$content = '<?php return [' . "\r\n";
		foreach($data as $key => $value) {
			$content .= "'" . $key . "' => '" . $value . "',\r\n";
		}
		$content .= '];';

		$dir = __DIR__ . DIRECTORY_SEPARATOR . 'i18n' . DIRECTORY_SEPARATOR . $language;
		if(!file_exists($dir)) {
			mkdir($dir);
		}

		file_put_contents($dir . DIRECTORY_SEPARATOR . 'twbp.php', print_r($content, true));
	}

	echo 'translation files generation successful' . "\r\n";
}

//array_to_csv($arr);

generate_translation();


?>