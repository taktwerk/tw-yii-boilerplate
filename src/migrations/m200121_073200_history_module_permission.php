<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200121_073200_history_module_permission extends TwMigration
{
    public function up()
    {
        $mi = $this;
        $auth = $mi->getAuth();
        $supAdmin = $auth->getRole('Superadmin');
        /*Created history controller permission and assigned it to superadmin*/
        $mi->createCrudControllerPermissions('backend_history');
        $historyPer = $auth->getPermission('x_backend_history_admin');
        $auth->addChild($supAdmin, $historyPer);
    }

    public function down()
    {
        echo "m200121_073200_history_module_permission cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
