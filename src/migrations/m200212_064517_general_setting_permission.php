<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200212_064517_general_setting_permission extends TwMigration
{
    public function up()
    {
        $mi = $this;
        $auth = $mi->getAuth();
        $viewer = $auth->getRole('Viewer');
        $generalSettingPer = $auth->getPermission('x_backend_general-setting_edit');
        $auth->addChild($viewer, $generalSettingPer);
    }

    public function down()
    {
        echo "m200212_064517_general_setting_permission cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
