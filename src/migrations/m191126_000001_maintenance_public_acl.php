<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191126_000001_maintenance_public_acl extends TwMigration
{
    public function up()
    {
        $this->createPermission('app_maintenance_index', 'Maintenance Page', ['Public']);
    }

    public function down()
    {
        $this->removePermission('app_maintenance_index', ['Public']);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
