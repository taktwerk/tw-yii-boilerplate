<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200424_180306_add_permission_to_backend extends TwMigration
{
    public function up()
    {
        $this->createPermission('app_elfinder_connect ', 'App Site Image', ['Backend']);

    }

    public function down()
    {
        echo "m200424_180306_add_permission_to_backend cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
