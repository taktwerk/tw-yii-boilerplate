<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200423_122435_remove_redundant_permissions extends TwMigration
{
    public function up()
    {
        $this->removePermission('feedback_feedback');
        $new_feedback = $this->getPermission('feedback_feedback_new-feedback');
        $viewer = $this->getAuth()->getRole('Backend');
        $viewer->description = "Backend access";
        $this->getAuth()->addChild($viewer, $new_feedback);

    }

    public function down()
    {
        echo "m200423_122435_remove_redundant_permissions cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
