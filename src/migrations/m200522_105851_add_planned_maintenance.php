<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200522_105851_add_planned_maintenance extends TwMigration
{
    public function up()
    {
        $query = <<<EOF
INSERT INTO `settings` (`id`, `type`, `section`, `key`, `value`, `active`, `created`, `modified`) VALUES (NULL, 'boolean', 'maintenance', 'planned', '1', '0', '2020-05-22 16:22:55', '2020-05-22 16:23:17')
EOF;
        $this->execute($query);


    }

    public function down()
    {
        echo "m200522_105851_add_planned_maintenance cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
