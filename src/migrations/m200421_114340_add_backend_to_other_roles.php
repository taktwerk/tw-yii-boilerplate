<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200421_114340_add_backend_to_other_roles extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $backend = $auth->getRole('Backend');
        
        $this->createRole('FaqViewer');
        $this->createRole('FaqAdmin', 'FaqViewer');

        // Init the permission cascade for see
        $this->addSeeControllerPermission('backend_faq', 'FaqViewer');
        // Administration permission assignment
        $this->addAdminControllerPermission('faq_default','FaqAdmin');
            
        $faq_viewer = $auth->getRole('FaqViewer');
        $auth->addChild($faq_viewer, $backend);
        $feedback_viewer = $auth->getRole('FeedbackViewer');
        $auth->addChild($feedback_viewer, $backend);
        $translator_viewer = $auth->getRole('TranslatorViewer');
        $auth->addChild($translator_viewer, $backend);
        $user_viewer = $auth->getRole('UsermanagerViewer');
        $auth->addChild($user_viewer, $backend);
    }

    public function down()
    {
        echo "m200421_114340_add_backend_to_other_roles cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
