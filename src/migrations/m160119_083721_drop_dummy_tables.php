<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m160119_083721_drop_dummy_tables extends TwMigration
{
    public function up()
    {
        $this->dropTable("{{%person}}");
    }

    public function down()
    {
        echo "m160119_083721_drop_dummy_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
