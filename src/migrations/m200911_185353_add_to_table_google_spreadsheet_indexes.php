<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200911_185353_add_to_table_google_spreadsheet_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_google_spreadsheet_deleted_at_user_id', '{{%google_spreadsheet}}', ['deleted_at','user_id']);
    }

    public function down()
    {
        echo "m200911_185353_add_to_table_google_spreadsheet_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
