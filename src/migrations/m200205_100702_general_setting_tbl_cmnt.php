<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200205_100702_general_setting_tbl_cmnt extends TwMigration
{
    public function up()
    {
        $tbl = "{{%general_setting}}";
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\backend"}';
        $this->addCommentOnTable($tbl, $comment);
    }

    public function down()
    {
        echo "m200205_100702_general_setting_tbl_cmnt cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
