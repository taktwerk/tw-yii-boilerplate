<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200212_081732_general_setting_permission_to_admin extends TwMigration
{
    public function up()
    {
        $mi = $this;
        $auth = $mi->getAuth();
        $viewer = $auth->getRole('Viewer');
        $generalSettingPer = $auth->getPermission('x_backend_general-setting_edit');
        $auth->removeChild($viewer, $generalSettingPer);
        $admin = $auth->getRole('Administrator');
        $auth->addChild($admin, $generalSettingPer);
    }

    public function down()
    {
        echo "m200212_081732_general_setting_permission_to_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
