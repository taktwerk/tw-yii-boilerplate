<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200416_130301_google_sheet_cmnt_config extends TwMigration
{
    public function up()
    {
        $comment= '{"base_namespace":"taktwerk\\\\yiiboilerplate"}';
        $this->addCommentOnTable('google_spreadsheet', $comment);       
    }

    public function down()
    {
        echo "m200416_130301_google_sheet_cmnt_config cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
