<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m190121_000001_tw_country extends TwMigration
{
    public function up()
    {
        $this->createTable(
            '{{%country}}',
            [
                'id' => Schema::TYPE_PK . "",
                'iso' => Schema::TYPE_CHAR . '(5) NOT NULL',
                'name' => Schema::TYPE_STRING . '(255) NOT NULL',
            ]
        );

        $this->createIndex('country_idx', '{{%country}}', ['iso'], true);
    }

    public function down()
    {
        $this->dropTable('{{%country}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
