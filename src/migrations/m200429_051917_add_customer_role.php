<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200429_051917_add_customer_role extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $this->createRole('Customer');

    }

    public function down()
    {
        echo "m200429_051917_add_customer_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
