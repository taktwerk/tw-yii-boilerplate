<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200424_124431_add_permissions_to_admin extends TwMigration
{
    public function up()
    {
        $this->createCrudControllerPermissions('setting_general-setting');
        $this->addAdminControllerPermission('setting_general-setting','Administrator');

    }

    public function down()
    {
        echo "m200424_124431_add_permissions_to_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
