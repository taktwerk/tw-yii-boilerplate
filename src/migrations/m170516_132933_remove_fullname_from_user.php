<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170516_132933_remove_fullname_from_user extends TwMigration
{
    public function up()
    {
//        $this->dropColumn('{{%user}}', 'fullname');
    }

    public function down()
    {
        echo "m170516_132933_remove_fullname_from_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
