<?php

use yii\db\Migration;

class m150625_000000_reset_audit_tables extends Migration
{
    public function up()
    {
        $this->execute("SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `audit_javascript`;
DROP TABLE IF EXISTS `audit_mail`;
DROP TABLE IF EXISTS `audit_trail`;
DROP TABLE IF EXISTS `audit_data`;
DROP TABLE IF EXISTS `audit_entry`;
DROP TABLE IF EXISTS `audit_error`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

DELETE FROM `migration` WHERE  `version`='m150626_000001_create_audit_entry';
DELETE FROM `migration` WHERE  `version`='m150626_000002_create_audit_data';
DELETE FROM `migration` WHERE  `version`='m150626_000003_create_audit_error';
DELETE FROM `migration` WHERE  `version`='m150626_000004_create_audit_trail';
DELETE FROM `migration` WHERE  `version`='m150626_000005_create_audit_javascript';
DELETE FROM `migration` WHERE  `version`='m150626_000006_create_audit_mail';
DELETE FROM `migration` WHERE  `version`='m150714_000001_alter_audit_data';
DELETE FROM `migration` WHERE  `version`='m170126_000001_alter_audit_mail';");
    }

    public function down()
    {
        echo "m150625_000000_reset_audit_tables cannot be reverted.\n";

        return false;
    }
}
