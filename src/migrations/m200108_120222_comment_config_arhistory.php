<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200108_120222_comment_config_arhistory extends TwMigration
{
    public function up()
    {
        $tbl = "{{%arhistory}}";
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\backend"}';
        $this->addColumn($tbl, 'deleted_by', $this->integer(11)->null());
        $this->addColumn($tbl, 'deleted_at', $this->dateTime()->null());
        $this->addColumn($tbl, 'updated_by', $this->integer(11)->null());
        $this->addColumn($tbl, 'updated_at', $this->dateTime()->null());
        $this->addCommentOnTable($tbl, $comment);
    }

    public function down()
    {
        echo "m200108_120222_comment_config_arhistory cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
