<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200629_120539_add_export_permissions extends TwMigration
{
    public function up()
    {
        $this->createPermission('app_export-csv', 'General Export for CSV');
        $this->createPermission('app_export-xls', 'General Export for XLS');
        $this->createPermission('app_export-html', 'General Export for HTML');
        $this->createPermission('app_export-text', 'General Export for TEXT');
    }

    public function down()
    {
        echo "m200629_120539_add_export_permissions cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
