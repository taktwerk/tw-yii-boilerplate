<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191219_114951_cache_data_mediumblob extends TwMigration
{
    public function up()
    {
        $this->alterColumn('{{%cache}}', 'data', 'mediumblob');//timestamp new_data_type
        
    }

    public function down()
    {
        echo "m191219_114951_cache_data_mediumblob cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
