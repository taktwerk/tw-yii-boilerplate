<?php

use yii\db\Migration;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%general_setting}}`.
 */
class m200127_130747_create_general_setting_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%general_setting}}', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer(11)->notNULL(),
            'sla_plan' => $this->string(255)->notNULL(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%general_setting}}');
    }
}
