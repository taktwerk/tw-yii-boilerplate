<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200630_082347_add_switch_identity_to_backend extends TwMigration
{
    public function up()
    {
        $this->createPermission('user_admin_switch-identity', 'Switch User Back', ['Backend']);
    }

    public function down()
    {
        echo "m200630_082347_add_switch_identity_to_backend cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
