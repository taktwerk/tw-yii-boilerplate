<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200424_060356_fix_translator_permissions extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $viewer = $auth->getRole('TranslatorViewer');
        $admin = $auth->getRole('TranslatorAdmin');
        $backend = $auth->getRole('Backend');
        $translator_translate_permission = $auth->createPermission('translatemanager_translator_translate');
        $translator_list_permission = $auth->createPermission('translatemanager_translator_list');
        $translator_save_permission = $auth->createPermission('translatemanager_translator_save');
        $auth->removeChildren($viewer);
        $auth->removeChildren($admin);
        $auth->addChild($viewer,$backend);
        $auth->addChild($viewer, $translator_translate_permission);
        $auth->addChild($viewer, $translator_list_permission);
        $auth->addChild($admin, $viewer);
        $auth->addChild($admin, $translator_save_permission);

        
    }

    public function down()
    {
        echo "m200424_060356_fix_translator_permissions cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
