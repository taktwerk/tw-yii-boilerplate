<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200117_130246_superadmin_webshell_permission extends TwMigration
{
    public function up()
    {
        $mi = $this;
        $auth = $mi->getAuth();
        /*Created new role/permission superadmin and inherited admin*/
        $supAdmin = $auth->getRole('Superadmin');
        /*Created permissions for log-reader and assigned it to superadmin*/
        $mi->createCrudControllerPermissions('webshell_index', 'WebShell');
        $shellPer = $auth->getPermission('x_webshell_index_admin');
        $auth->addChild($supAdmin, $shellPer);
    }

    public function down()
    {
        echo "m200117_130246_superadmin_webshell_permission cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
