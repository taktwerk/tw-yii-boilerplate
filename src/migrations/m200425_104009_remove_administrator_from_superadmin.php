<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200425_104009_remove_administrator_from_superadmin extends TwMigration
{
    public function up()
    {

        $auth = $this->getAuth();
        $admin = $auth->getRole('Administrator');
        $super = $auth->getRole('Superadmin');
        $auth->removeChild($super, $admin);

    }

    public function down()
    {
        echo "m200425_104009_remove_administrator_from_superadmin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
