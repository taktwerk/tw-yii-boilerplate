<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200117_043943_superadmin_role_add extends TwMigration
{
    public function up()
    {
        $mi = $this;
        $auth = $mi->getAuth();
        
        /*Created new role/permission superadmin and inherited admin*/
        $mi->createRole('Superadmin',null,'Parent of Administrator');
        $supAdmin = $auth->getRole('Superadmin');
        $admin = $auth->getRole('Administrator');
        $authority = $auth->getRole('Authority');
        $auth->removeChild($authority, $admin);
        $auth->addChild($supAdmin, $admin);
        $auth->addChild($authority,$supAdmin);
        /*Created permissions for log-reader and assigned it to superadmin*/
        $mi->createCrudControllerPermissions('log-reader_default', 'Log Reader View');
        $mi->createPermission('log-reader_default_download','Download Log');
        $auth->addChild($auth->getPermission('x_log-reader_default_edit'), $auth->getPermission('log-reader_default_download'));
        
        $logPer = $auth->getPermission('x_log-reader_default_admin');
        $auth->addChild($supAdmin, $logPer);
        
        /*Created permissions for Queue Job and assigned it to superadmin*/
        $mi->createCrudControllerPermissions('queue_queue-job','Queue Job');
        $quePer = $auth->getPermission('x_queue_queue-job_admin');
        $auth->addChild($supAdmin, $quePer);
    }

    public function down()
    {
        echo "m200117_043943_superadmin_role_add cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
