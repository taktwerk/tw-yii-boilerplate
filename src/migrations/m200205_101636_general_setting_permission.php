<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200205_101636_general_setting_permission extends TwMigration
{
    public function up()
    {
        $mi = $this;
        $auth = $mi->getAuth();
        $admin = $auth->getRole('Administrator');
        /*Created general setting controller permission and assigned it to Administrator*/
        $mi->createCrudControllerPermissions('backend_general-setting');
        $historyPer = $auth->getPermission('x_backend_general-setting_admin');
        $auth->addChild($admin, $historyPer);
    }

    public function down()
    {
        echo "m200205_101636_general_setting_permission cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
