<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200120_045248_add_log_reader_history_permission extends TwMigration
{

    public function up()
    {
        $auth = $this->getAuth();
        $this->createPermission('log-reader_default_history', 'Download Log');
        $auth->addChild($auth->getPermission('x_log-reader_default_edit'), $auth->getPermission('log-reader_default_history'));
    }

    public function down()
    {
        echo "m200120_045248_add_log_reader_history_permission cannot be reverted.\n";
        
        return false;
    }
    
    /*
     * // Use safeUp/safeDown to run migration code within a transaction
     * public function safeUp()
     * {
     * }
     *
     * public function safeDown()
     * {
     * }
     */
}
