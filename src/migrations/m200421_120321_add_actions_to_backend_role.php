<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200421_120321_add_actions_to_backend_role extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $backend = $auth->getRole('Backend');
        $image_permission = $auth->getPermission('app_site_image');
        if(!$image_permission){
            $image_permission = $auth->createPermission('app_site_image');
            $auth->add($image_permission);
        }
        if(!$auth->hasChild($backend, $image_permission)){
            $auth->addChild($backend, $image_permission);
        }
        $site_permission = $auth->getPermission('app_site');
        if(!$site_permission){
            $site_permission = $auth->createPermission('app_site');
            $auth->add($site_permission);
        }
        if(!$auth->hasChild($backend, $site_permission)){
            $auth->addChild($backend, $site_permission);
        }
        $error_permission = $auth->getPermission('app_site_error');
        if(!$error_permission){
            $error_permission = $auth->createPermission('app_site_error');
            $auth->add($error_permission);
        }
        if(!$auth->hasChild($backend, $error_permission)){
            $auth->addChild($backend, $error_permission);
        }
        
    }

    public function down()
    {
        echo "m200421_120321_add_error_to_backend_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
