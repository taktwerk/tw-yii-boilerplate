<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200212_053357_general_setting_col_client extends TwMigration
{
    public function up()
    {
        $tbl = '{{%general_setting}}';
      /*   $t = $this->db->beginTransaction();
        try { */
            $this->alterColumn($tbl, 'client_id', $this->integer(11)
                ->notNull()
                ->unique());
            $this->addForeignKey('fk_general_setting_client_id', $tbl, 'client_id', '{{%client}}', 'id', 'CASCADE');
            
            
      //      $t->commit();
      /*   } catch (\Exception $e) {
            $t->rollBack();
            throw $e;
        } */
    }

    public function down()
    {
        echo "m200212_053357_general_setting_col_client cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
