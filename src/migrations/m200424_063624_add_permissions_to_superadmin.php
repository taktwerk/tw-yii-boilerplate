<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200424_063624_add_permissions_to_superadmin extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $translator_admin = $auth->getRole('TranslatorAdmin');
        $user_admin = $auth->getRole('UsermanagerAdmin');
        $superadmin = $auth->getRole('Superadmin');
        if(!$auth->hasChild($superadmin, $translator_admin)){
            $auth->addChild($superadmin, $translator_admin);
        }
        if(!$auth->hasChild($superadmin, $user_admin)){
            $auth->addChild($superadmin, $user_admin);
        }

        //system information
        $sys_info = $auth->getPermission('backend_system-information');
        if(!$sys_info){
            $sys_info = $auth->createPermission('backend_system-information');
            $auth->add($sys_info);
        }
        if(!$auth->hasChild($superadmin, $sys_info)){
            $auth->addChild($superadmin, $sys_info);
        }

        //file manager
        $file = $auth->getPermission('app_elfinder');
        if(!$file){
            $file = $auth->createPermission('app_elfinder');
            $auth->add($file);
        }
        if(!$auth->hasChild($superadmin, $file)){
            $auth->addChild($superadmin, $file);
        }

        //web console
        $webconsole_default_index = $auth->getPermission('webconsole_default_index');
        if(!$webconsole_default_index){
            $webconsole_default_index = $auth->createPermission('webconsole_default_index');
            $auth->add($webconsole_default_index);
        }
        if(!$auth->hasChild($superadmin, $webconsole_default_index)){
            $auth->addChild($superadmin, $webconsole_default_index);
        }

        //web console server
        $webconsole_server = $auth->getPermission('webconsole_server');
        if(!$webconsole_server){
            $webconsole_server = $auth->createPermission('webconsole_server');
            $auth->add($webconsole_server);
        }
        if(!$auth->hasChild($superadmin, $webconsole_server)){
            $auth->addChild($superadmin, $webconsole_server);
        }

        //web shell
        $webshell_index_rpc = $auth->getPermission('webshell_index_rpc');
        if(!$webshell_index_rpc){
            $webshell_index_rpc = $auth->createPermission('webshell_index_rpc');
            $auth->add($webshell_index_rpc);
        }
        if(!$auth->hasChild($superadmin, $webshell_index_rpc)){
            $auth->addChild($superadmin, $webshell_index_rpc);
        }

        //backup manager
        $backup = $auth->getPermission('backup-manager_default');
        if(!$backup){
            $backup = $auth->createPermission('backup-manager_default');
            $auth->add($backup);
        }
        if(!$auth->hasChild($superadmin, $backup)){
            $auth->addChild($superadmin, $backup);
        }

        //adminer
        $adminer = $auth->getPermission('adminer_adminer');
        if(!$adminer){
            $adminer = $auth->createPermission('adminer_adminer');
            $auth->add($adminer);
        }
        if(!$auth->hasChild($superadmin, $adminer)){
            $auth->addChild($superadmin, $adminer);
        }
    }

    public function down()
    {
        echo "m200424_063624_add_permissions_to_superadmin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
