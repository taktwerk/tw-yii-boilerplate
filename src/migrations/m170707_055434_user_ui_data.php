<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170707_055434_user_ui_data extends TwMigration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%user_ui_data}}',
            [
                'id' => Schema::TYPE_PK . "",
                'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'key' => Schema::TYPE_STRING . '(255)',
                'param' => Schema::TYPE_STRING . '(255)',
                'value' => Schema::TYPE_TEXT,
            ],
            $tableOptions
        );

        $this->createIndex('user_idx', '{{%user_ui_data}}', ['user_id', 'key', 'param'], true);
        $this->addForeignKey('user_ui_data_fk_user_user_id', '{{%user_ui_data}}', 'user_id', '{{%user}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('user_ui_data_fk_user_user_id', '{{%user_ui_data}}');
        $this->dropTable('{{%user_ui_data}}');
        echo "m170707_055434_user_ui_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
