<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200424_183402_add_Backend_to_Viewer extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $viewer = $auth->getRole('Viewer');
        $backend = $auth->getRole('Backend');
        $auth->addChild($viewer, $backend);
    }

    public function down()
    {
        echo "m200424_183402_add_Backend_to_Viewer cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
