<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;
use yii\helpers\StringHelper;

class m200619_151150_permission_fix extends TwMigration
{
    public function up()
    {
        $query = (new \yii\db\Query())->from('auth_item_child');
        $rowsChanged = 0;
        echo '
======== PERMISSION CHANGES - START=========
';
        foreach($query->batch() as $rows){
            foreach($rows as $row){
                if(StringHelper::endsWith($row['parent'],'_edit') && StringHelper::endsWith($row['child'],'_admin')){
                    $this->exchange($row);
                    $rowsChanged++;
                }
                if($this->stringEndsWithAnyItem($row['parent'],
                    ['_create','_delete','_delete-multiple','_related-form','_update','_update-multiple','_see'])
                    && StringHelper::endsWith($row['child'],'_edit')){
                        $this->exchange($row);
                        $rowsChanged++;
                }
                if($this->stringEndsWithAnyItem($row['parent'],['_entry-details','_index','_list','_system-entry','_view','_depend']) && StringHelper::endsWith($row['child'],'_see')){
                    $this->exchange($row);
                    $rowsChanged++;
                }
               }
            
        }
        
        echo '
======== PERMISSION CHANGES - END=========
';
        echo "
        
{$rowsChanged} number of rows updated

";
    }
    private function stringEndsWithAnyItem($string,$list=[]){
        foreach ($list as $i){
            if(StringHelper::endsWith($string,$i)){
                return true;
            }
        }
        return false;
    }
    private function exchange($row){
        echo '
'.$row['child'].' - '.$row['parent']
.'
';
\Yii::$app->db->createCommand()
->update('auth_item_child',
    ['child'=>$row['parent'],'parent'=>$row['child']],['child'=>$row['child'],'parent'=>$row['parent']])
    ->execute();
    }

    public function down()
    {
        echo "m200619_151150_permission_fix cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
