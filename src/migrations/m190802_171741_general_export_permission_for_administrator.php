<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m190802_171741_general_export_permission_for_administrator extends TwMigration
{
    public function up()
    {
        $this->createPermission('app_export-xlsx', 'General Export for Excel', ['Administrator']);

        // missing connection from Administrator to Authority
        $auth = $this->getAuth();
        if($auth->canAddChild('Authority', 'Administrator')){

            $this->createRole('Authority', 'Administrator');
        }
    }

    public function down()
    {
        echo "m190802_171741_general_export_permission_for_administrator cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
