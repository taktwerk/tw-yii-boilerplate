<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200916_161453_add_roles_to_Administrator extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $news = $auth->getRole('NewsletterViewer');
        $faq_view = $auth->getRole('FaqViewer');
        $backend = $auth->getRole('Backend');

        /*FaqViewer to Backend, need to remove Backend from FaqViewer*/
        if($auth->hasChild($faq_view,$backend)){
            $auth->removeChild($faq_view, $backend);
        }
        if($auth->canAddChild($backend, $faq_view) && !$auth->hasChild($backend, $faq_view)){
            $auth->addChild($backend, $faq_view);
        }
    }

    public function down()
    {
        echo "m200916_161453_add_roles_to_Administrator cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
