<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m160118_140502_test_tables_2 extends TwMigration
{
    public function up()
    {
        $this->createTable("{{%person}}",[
            'person_id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING. ' NOT NULL DEFAULT ""',
            'birthday' => Schema::TYPE_DATETIME ,
        ]);
    }

    public function down()
    {
        $this->dropTable("{{%person}}");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
