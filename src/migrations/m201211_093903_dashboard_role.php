<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m201211_093903_dashboard_role extends TwMigration
{
    public function safeUp()
    {
        $auth = $this->getAuth();
        
        $this->createRole('Dashboard',null,'Dashboard');
        $dashboard = $auth->getRole('Dashboard');
        $this->createPermission('backend_default_index','Backend Dashboard Page',['Dashboard']);
        $this->createPermission('backend_default','',['Dashboard']);
        $this->createPermission('backend_faq_index','',['Dashboard']);
        $faqViewer = $auth->getRole('FaqViewer');
        $auth->addChild($dashboard, $faqViewer);
    }

    public function safeDown()
    {
        echo "m201211_093903_dashboard_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
