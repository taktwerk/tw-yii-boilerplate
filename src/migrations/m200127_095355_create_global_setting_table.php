<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%global_setting}}`.
 */
class m200127_095355_create_global_setting_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%global_setting}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(255)->notNULL()->unique(),
            'value' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%global_setting}}');
    }
}
