<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200911_185417_add_to_table_user_ui_data_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_user_ui_data_deleted_at_user_id', '{{%user_ui_data}}', ['deleted_at','user_id']);
    }

    public function down()
    {
        echo "m200911_185417_add_to_table_user_ui_data_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
