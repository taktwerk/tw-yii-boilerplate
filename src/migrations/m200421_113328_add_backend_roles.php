<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200421_113328_add_backend_roles extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $this->createRole('Backend',null,'Parent of Administrator');
        $backend = $auth->getRole('Backend');
        $backend_permission = $auth->getPermission('backend_default');
        $auth->addChild($backend, $backend_permission);
    }

    public function down()
    {
        echo "m200421_113328_add_backend_roles cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
