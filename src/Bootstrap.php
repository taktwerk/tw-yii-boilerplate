<?php
namespace taktwerk\yiiboilerplate;

use Da\User\Helper\SecurityHelper;
use schmunk42\giiant\generators\crud\callbacks\base\Callback;
use schmunk42\giiant\generators\crud\callbacks\yii\Db;
use schmunk42\giiant\generators\crud\callbacks\yii\Html;
use taktwerk;
use taktwerk\yiiboilerplate\behaviors\AuthLogWebUserBehavior;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use taktwerk\yiiboilerplate\components\Schedule;
use taktwerk\yiiboilerplate\helpers\ClassMapHelper;
use taktwerk\yiiboilerplate\helpers\CrudHelper;
use taktwerk\yiiboilerplate\models\GlobalSetting;
use taktwerk\yiiboilerplate\models\Language;
use taktwerk\yiiboilerplate\modules\backupmanager\commands\DumpController;
use taktwerk\yiiboilerplate\modules\share\models\FsComponent;
use taktwerk\yiiboilerplate\modules\user\controllers\SecurityController;
use taktwerk\yiiboilerplate\modules\user\controllers\UserAuthCodeController;
use taktwerk\yiiboilerplate\modules\user\controllers\UserAuthLogController;
use taktwerk\yiiboilerplate\modules\user\controllers\UserDeviceController;
use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\base\Event;
use yii\caching\DbCache;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\i18n\Formatter;
use yii\i18n\PhpMessageSource;
use yii\web\AssetManager;
use yii\web\UrlManager;
use yii\web\UrlRule;
use yii\web\View;
use Da\User\Service\UpdateAuthAssignmentsService;

class Bootstrap implements BootstrapInterface
{

    /**
     *
     * @param $app Application
     */
    public function bootstrap($app)
    {
        // Before the rest, we want to set up some aliases
        $this->registerAliases();
        $this->configureCommon($app);
        $this->configureWeb($app);
        $this->configureConsole($app);
        $this->configureGlobalEnvSetting($app);
        $this->initTranslations($app);

        // Register Settings
        $this->registerConfig($app);
        $this->registerViews($app);
        $this->registerSchedule($app);
    }
    public function registerCommonContainer($app){
        $bp = $app->getModule('twboilerplate');
        if($bp){
            $map = $this->buildClassMap($bp->classMap);
            $di = Yii::$container;
            $modelClassMap = [];
            foreach ($map as $class => $definition) {
                $di->set($class, $definition);
                $model = is_array($definition) ? $definition['class'] : $definition;
                $modelClassMap[$class] = $model;
            }
            $di->setSingleton(ClassMapHelper::class, ClassMapHelper::class, [$modelClassMap]);
        }

    }
    public function buildClassMap(array $userClassMap){
        $map = [];

        $defaults = [
            // --- models
            //\taktwerk\yiiboilerplate\components\Helper::class => \taktwerk\yiiboilerplate\components\Helper::class,
        ];
        $mapping = array_merge($defaults, $userClassMap);
        return $mapping;
    }
    /**
     * Register Aliases for the app
     */
    protected function registerAliases()
    {
        Yii::setAlias('@taktwerk-boilerplate', '@vendor/taktwerk/yii-boilerplate/src/');
        Yii::setAlias('@taktwerk-views', '@vendor/taktwerk/yii-boilerplate/src/views');
        Yii::setAlias('@taktwerk-backend-views', '@vendor/taktwerk/yii-boilerplate/src/modules/backend/views');
        Yii::setAlias('@pages', '@app/modules/backend/views');
        Yii::setAlias('@tunecino/builder', '@vendor/taktwerk/yii-boilerplate/src/extensions/tunecino/yii2-schema-builder');
        Yii::setAlias('@Da', '@vendor/yii2-usuario/src/');
    }

    protected function configureGlobalEnvSetting(Application $app)
    {
        try {

            $tableName = GlobalSetting::tableName();
            if ($app->db->getTableSchema($tableName, true) == null) {
                return true;
            }
            $envFile = \Yii::getAlias('@root/.env-admin');

            if (is_file($envFile)) {
                return true;
            }
            $handle = fopen($envFile, "w") or die(\Yii::t('twbp', "Unable to open file!") . ' ' . $envFile);
            $content = [];
            $query = GlobalSetting::find();
            $content = [];
            foreach ($query->batch() as $items) {
                foreach ($items as $model) {
                    $content[] = "{$model->key} = \"" . addslashes($model->value) . "\"";
                }

            }
            fwrite($handle, implode(PHP_EOL, $content));
            fclose($handle);
            if ($app instanceof \yii\web\Application) {
                header('Location: ' . $_SERVER['REQUEST_URI']);
            } else {

                $configPath = FileHelper::normalizePath(Yii::getAlias('@app/config/main.php'));
                if (! is_file($configPath)) {
                    $configPath = FileHelper::normalizePath(Yii::getAlias('@app/config/console.php'));
                }
                if (is_file($configPath)) {
                    $config = require ($configPath);
                    $appClass = get_class($app);
                    $newApp = new $appClass($config);
                    $newApp->run();
                    die();
                }
                die('
-----------------------------------------------------------------------------------------
Configuration loaded from the Database, please re-run the application/command to execute
-----------------------------------------------------------------------------------------

');
            }
        } catch (\Exception $e) {
            if (isset($handle) && is_resource($handle)) {
                fclose($handle);
            }
            unlink($envFile);
            throw $e;
        }
    }

    /**
     *
     * @param $app Application
     *            Moved all general common configuration from starter-kit to here
     */
    protected function configureCommon($app)
    {
        $this->registerCommonContainer($app);
        $this->registerCommonComponents($app);

        $app->get('i18n')->translations = array_merge([
            '*' => [
                'class' => 'taktwerk\yiiboilerplate\components\DbMessageSource',
                'db' => 'db',
                'sourceLanguage' => 'en',
                'sourceMessageTable' => '{{%language_source}}',
                'messageTable' => '{{%language_translate}}',
                'cachingDuration' => 86400,
                'enableCaching' => YII_DEBUG ? false : true,
                'forceTranslation' => true // getenv('APP_FORCE_TRANSLATION'),
            ],
            'app' => [
                'class' => 'taktwerk\yiiboilerplate\components\DbMessageSource',
                'db' => 'db',
                'sourceLanguage' => 'en',
                'sourceMessageTable' => '{{%language_source}}',
                'messageTable' => '{{%language_translate}}',
                'cachingDuration' => 86400,
                'enableCaching' => YII_DEBUG ? false : true,
                'forceTranslation' => true

            ]
        ],$app->get('i18n')->translations);

        $this->registerCommonModules($app);
        // Register Gii and Giiant
        $this->configGiiant($app);
        $this->configGii($app);
        $this->configDefaultTableModelMapForSchemaChecker();
    }

    protected function configDefaultTableModelMapForSchemaChecker(){
        //DEFAULT_MODEL_TABLE_MAP

        ClassDispenser::getMappedClass(CrudHelper::class)::$defaultModelTableMap = array_merge(CrudHelper::$defaultModelTableMap,[
            'language'=>'\taktwerk\yiiboilerplate\models\Language',
            'user'=>'\taktwerk\yiiboilerplate\models\User'
        ]);
    }
    /**
     *
     * @param $app Application
     *            Moved all general web configuration from starter-kit to here
     */
    protected function configureWeb($app)
    {
        if (Yii::$app instanceof \yii\web\Application) {
            $app->get('request')->cookieValidationKey = getenv('APP_COOKIE_VALIDATION_KEY');
            $app->get('user')->identityClass = 'app\models\User';

            $this->registerWebErrorHandler($app);
            $this->registerWebComponents($app);
            $this->registerWebModules($app);
            $this->configureElfinder($app);
            $this->registerCrawlerLogin($app);
            $this->registerWebDAV($app);
            $this->registerWebContainer($app);
            $app->bootstrap = ArrayHelper::merge($app->bootstrap,  ['brussens\maintenance\Maintenance']);
            $app->controllerMap = \yii\helpers\ArrayHelper::merge($app->controllerMap, [
                'google-spreadsheet' => [
                    'class' => 'taktwerk\yiiboilerplate\controllers\GoogleSpreadsheetController',
                    'viewPath' => '@taktwerk-views/google-spreadsheet'
                ],
                'country' => [
                    'class' => 'taktwerk\yiiboilerplate\controllers\CountryController'
                ]
            ]);

            $manualConfigArray = \Yii::$app->getComponents();
            /* view - component - if default view class is loaded then it's overridden */
            if (get_class(\Yii::$app->view) === View::class || get_class(\Yii::$app->view) === \taktwerk\yiiboilerplate\components\View::class) {
                $this->registerComponent('view', [
                    'class' => 'taktwerk\yiiboilerplate\components\View',
                    'theme' => [
                        'pathMap' => \Yii::$app->view->theme->pathMap
                    ]
                ], false);
            }

            /* Adding BP theme paths to the view - START*/
            $pathMap = [
                '@Da/User/resources/views' => '@vendor/taktwerk/yii-boilerplate/src/modules/user/views',
                '@yii/gii/views/layouts' => '@taktwerk-backend-views/layouts',
                // remove to activate project views (@app/views),
                // or overwrite with module views ('@app/views' => '@modulename/views') with module alias
                // 'aliases' => ['@modulename' => '@app/modules/modulename']

                // Switch to @taktwerk-views to enable frontend
                '@app/views' => [
                    '@app/views',
                    '@taktwerk-backend-views', // comment this ligne to enable frontend views for cruds
                    '@taktwerk-views'
                ],

                // All admin views (backend module) can be overridden in the app folder before looking
                //// in the boilerplate backend module.
                '@taktwerk-backend-views' => [
                    '@app/modules/backend/views',
                    '@vendor/taktwerk/yii-boilerplate/src/modules/backend/views',
                ],
            ];

            if(\Yii::$app->view->theme->pathMap){
                $pathMap= ArrayHelper::merge($pathMap,\Yii::$app->view->theme->pathMap);
                foreach ($pathMap as $k=>$value) {
                    if(is_array($value)){
                        $pathMap[$k]=array_unique($value);
                    }
                }

            }
            \Yii::$app->view->theme->pathMap = $pathMap;

            /* Adding BP theme paths to the view - END */


            /*ASSET MANAGER - START*/
            if (get_class(\Yii::$app->assetManager) === AssetManager::class || get_class(\Yii::$app->assetManager) === \taktwerk\yiiboilerplate\assets\AssetManager::class){
                $manualAssetManConfig = $manualConfigArray['assetManager'];
                unset($manualAssetManConfig['class']);
                $defaultAssetManConfig = [
                    'class'=>'taktwerk\yiiboilerplate\assets\AssetManager',
                    // 'updateBaseDirOnNewModifyTime'=>YII_ENV_DEV,
                    'linkAssets' => YII_ENV_DEV,
                    'appendTimestamp' => true,
                    'converter'=> [
                        'class'=> 'nizsheanez\assetConverter\Converter',
                        'force'=> false, // true : If you want convert your sass each time without time dependency
                        'destinationDir' => './',// 'compiled', //at which folder of @webroot put compiled files
                        'parsers' => [
                            'sass' => [ // file extension to parse
                                'class' => 'nizsheanez\assetConverter\Sass',
                                'output' => 'css', // parsed output file type
                                'options' => [
                                    'cachePath' => '@app/runtime/cache/sass-parser' // optional options
                                ],
                            ],
                            'scss' => [ // file exsigtension to parse
                                'class' => 'nizsheanez\assetConverter\Scss',
                                'output' => 'css', // parsed output file type
                                'options' => [ // optional options
                                    'enableCompass' => true, // default is true
                                    'importPaths' => [
                                        '@app/modules/frontend/assets/web/sass',
                                        '@vendor/twbs',
                                        //'@vendor/taktwerk/yii-boilerplate/src/assets/web'
                                    ], // import paths, you may use path alias here,
                                    // e.g., `['@path/to/dir', '@path/to/dir1', ...]`
                                    'lineComments' => false, // if true - compiler will place line numbers in your compiled output
                                    'outputStyle' => 'nested', // May be `compressed`, `crunched`, `expanded` or `nested`,
                                    // see more at http://sass-lang.com/documentation/file.SASS_REFERENCE.html#output_style
                                ],
                            ],
                            'less' => [ // file extension to parse
                                'class' => 'nizsheanez\assetConverter\Less',
                                'output' => 'css', // parsed output file type
                                'options' => [
                                    'importDirs' => [], // import paths, you may use path alias here ex. '@app/assets/common/less'
                                    'auto' => true, // optional options
                                ]
                            ]
                        ]
                    ]

                ];
                $defaultAssetManConfig = ArrayHelper::merge($defaultAssetManConfig,$manualAssetManConfig);
                $defaultAssetManConfig['converter']['parsers']['scss']['options']['importPaths'] = array_unique($defaultAssetManConfig['converter']['parsers']['scss']['options']['importPaths']);
                $this->registerComponent('assetManager', $defaultAssetManConfig, false);
            }
            /*ASSET MANAGER - END*/
        }
    }

    /**
     *
     * @param $app Application
     *            Moved all general console configuration from starter-kit to here
     */
    protected function configureConsole($app)
    {
        if (Yii::$app instanceof \yii\console\Application) {
            $this->registerConsoleErrorHandler($app);
            $this->registerConsoleComponents($app);
            $this->registerConsoleModules($app);
            $this->registerConsoleContainer($app);
            $app->controllerNamespace = 'app\commands';
            $app->controllerMap = \yii\helpers\ArrayHelper::merge($app->controllerMap, [
                'db' => 'dmstr\console\controllers\MysqlController',
                'migrate' => [
                    'class' => 'taktwerk\yiiboilerplate\components\console\controllers\MigrateController',
                    'templateFile' => '@vendor/taktwerk/yii-boilerplate/src/views/migration/migration.php'
                ],
                'translate' => 'taktwerk\yiiboilerplate\commands\TranslateController',
                'schedule' => 'taktwerk\yiiboilerplate\controllers\ScheduleController',
                'crawler' => 'taktwerk\yiiboilerplate\commands\CrawlerController',
                // 'notification' => 'taktwerk\yiiboilerplate\modules\notification\commands\NotificationController',
                'image-cache' => 'taktwerk\yiiboilerplate\commands\ImageCacheController',
                'zip' => 'taktwerk\yiiboilerplate\commands\ZipController',
                'zipupdate' => 'taktwerk\yiiboilerplate\commands\ZipupdateController',
                'import' => [
                    'class' => 'taktwerk\yiiboilerplate\modules\import\commands\ImportController'
                ],
                'maintenance' => [
                    'class' => 'brussens\maintenance\commands\MaintenanceController',
                ],
                'sync-index'=>'taktwerk\yiiboilerplate\modules\sync\commands\SyncIndexController'
            ]);
            if(!$app->params['modules_activation'] || ($app->params['modules_activation'] && isset($app->params['modules_activation']['notification']) && $app->params['modules_activation']['notification'])){
                $app->controllerMap['notification'] = 'taktwerk\yiiboilerplate\modules\notification\commands\NotificationController';
            }
            if (! isset($app->controllerMap['backup'])) {
                $app->controllerMap['backup'] = DumpController::class;
            }
        }
    }

    /**
     * Register View data
     *
     * @param Application $app
     */
    protected function registerViews(Application $app)
    {
        // $app->assetManager->bundles['dmstr\web\AdminLteAsset']['skin'] = false; // skin loading not working with assets-prod generation
        if (! Yii::$app instanceof \yii\console\Application) {
            // Override 2amigos user views with ours
            if (! isset($app->view->theme->pathMap['@Da/User/resources/views/admin'])) {
                $app->view->theme->pathMap['@Da/User/resources/views/admin'] = '@taktwerk-backend-views/users/admin';
            }

            // Set proper backend views path
            $app->getModule('backend')->setViewPath('@taktwerk-backend-views');
        }
    }

    /**
     * Register changed to the application
     *
     * @param Application $app
     */
    protected function registerConfig(Application $app)
    {
        $this->registerMigrationPaths($app);
        $app->params = \yii\helpers\ArrayHelper::merge([
            'adminEmail' => getenv('APP_ADMIN_EMAIL'),
            // Presets for CkEditor
            'richTextSimple' => [
                'source',
                '|',
                'undo',
                'redo',
                'selectall',
                '|',
                'preview',
                'print',
                '|',
                'fontname',
                'fontsize',
                'bold',
                'italic',
                'underline',
                'strikethrough',
                '|',
                'justifyleft',
                'justifycenter',
                'justifyright',
                '|'
            ],
            'richTextStandard' => [
                'insertorderedlist',
                'insertunorderedlist',
                'indent',
                'outdent',
                '|',
                'formatblock',
                'forecolor',
                'hilitecolor',
                'hr',
                '|'
            ],
            'richTextAdvanced' => [
                'subscript',
                'superscript',
                'clearhtml',
                'quickformat',
                '|',
                'image',
                'multiimage',
                'flash',
                'media',
                'insertfile',
                '|',
                'table',
                'emoticons',
                'pagebreak',
                'anchor',
                'link',
                'unlink',
                '|'
            ],
            'GoogleCredentials' => '@app/config/secret/client_secret.json',
            'hasFrontend' => false // enable to show a "back to frontend button in admin header"
        ], $app->params);

        // Disable prefix for icons in Menu widget
        \dmstr\widgets\Menu::$iconClassPrefix = '';
    }

    /**
     * Override the configuration of the Giiant package
     */
    protected function configGiiant(Application $app)
    {
        if (YII_ENV == 'dev' || YII_ENV == 'test') {
            $aceEditorField = function ($attribute, $model, $generator) {
                return "\$form->field(\$model, '{$attribute}')->widget(\\trntv\\aceeditor\\AceEditor::class)";
            };

            /**
             * Checkbox-Field | Provider for TINYINT(1) Fields
             * -------------------------------------
             *
             * @author : Benjamin Bur | taktwerk.ch
             */
            $checkBoxField = function ($attribute, $model, $generator) {
                return "\$form->field(\$model, '{$attribute}')->checkbox()";
            };

            /**
             * Date-Field | Provider
             * -------------------------------------
             *
             * @author : Benjamin Bur | taktwerk.ch
             */
            $dateTimePickerField = function ($attribute, $generator) {
                return "\$form->field(\$model, '{$attribute}')->widget(\\kartik\\datecontrol\\DateControl::classname(), [
                'type' => \\kartik\\datecontrol\\DateControl::FORMAT_DATETIME,
                'ajaxConversion' => true,
                'options' => [
                    'type' => \\kartik\\datetime\\DateTimePicker::TYPE_COMPONENT_APPEND,
                    'pickerButton' => ['icon' => 'time'],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true,
                        'class' => 'form-control'
                    ]
                ],
            ])";
            };
            $datePickerField = function ($attribute, $generator) {
                return "\$form->field(\$model, '{$attribute}')->widget(\\kartik\\datecontrol\\DateControl::classname(), [
                'type' => \\kartik\\datecontrol\\DateControl::FORMAT_DATE,
                'options' => [
                    'type' => \\kartik\\date\\DatePicker::TYPE_COMPONENT_APPEND,
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true,
                        'class' => 'form-control'
                    ]
                ],
            ])";
            };
            $timePickerField = function ($attribute, $generator) {
                return "\$form->field(\$model, '{$attribute}')->widget(\\kartik\\datecontrol\\DateControl::classname(), [
                'type' => \\kartik\\datecontrol\\DateControl::FORMAT_TIME,
                'options' => [
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true,
                        'class' => 'form-control',
                        'showSeconds' => false,
                    ]
                ],
            ])";
            };
            $colourPickerField = function ($attribute, $generator) {
                return "\$form->field(\$model, '{$attribute}')->widget(\\kartik\\color\\ColorInput::classname(), [
                'options' => [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'class' => 'form-control',
                    ]
                ],
            ])";
            };

            Yii::$container->set('taktwerk\yiiboilerplate\templates\crud\providers\CallbackProvider', [
                'columnFormats' => [
                    // hide system fields, but not ID in table
                    'created_at$|updated_at$|created_by$|updated_by$|deleted_at$|deleted_by$' => Callback::false(),
                    // hide all TEXT or TINYTEXT columns
                    '.*' => Db::falseIfText()
                ],
                'activeFields' => [
                    // hide system fields in form
                    'id$' => Db::falseIfAutoIncrement(),
                    'id$|created_at$|updated_at$|created_by$|updated_by$|deleted_at$|deleted_by$' => Callback::false(),
                    '_at$' => $dateTimePickerField,
                    '_date$' => $datePickerField,
                    //'is_' => $checkBoxField,
                    'has_' => $checkBoxField,
                    '_time$' => $timePickerField,
                    '_colour$' => $colourPickerField, // Europeans happy
                    '_color$' => $colourPickerField // Americans happy
                ],
                'attributeFormats' => [
                    // render HTML output
                    '_html$' => Html::attribute()
                ]
            ]);

            $app->controllerMap = \yii\helpers\ArrayHelper::merge($app->controllerMap, [
                'batch' => [
                    'class' => 'taktwerk\yiiboilerplate\commands\BatchController',
                    'overwrite' => true,
                    'singularEntities' => false,
                    //'singularEntities' => true, This is commented(set to false) as sometimes when name of model/controller name differs from the table name e.g table named 'pathology_slice' name is converted to 'PatholgySlouse' model when converted to singular form.
                    'modelNamespace' => 'app\\models',
                    'modelQueryNamespace' => 'app\\models\\query',
                    'crudTidyOutput' => false,
                    'crudAccessFilter' => true,
                    // 'crudControllerNamespace' => 'app\\controllers',
                    'crudControllerNamespace' => 'app\\modules\\backend\\controllers',
                    'crudSearchModelNamespace' => 'app\\models\\search',
                    // 'crudViewPath' => '@app/views',
                    'crudViewPath' => '@app/modules/backend/views',
                    'crudPathPrefix' => null,
                    'crudMessageCategory' => 'app',
                    'crudProviders' => [
                        'taktwerk\\yiiboilerplate\\templates\\crud\\providers\\OptsProvider',
                        'taktwerk\\yiiboilerplate\\templates\\crud\\providers\\CallbackProvider',
                        'taktwerk\\yiiboilerplate\\templates\\crud\\providers\\DateTimeProvider',
                        'taktwerk\\yiiboilerplate\\templates\\crud\\providers\\DateProvider',
                        'taktwerk\\yiiboilerplate\\templates\\crud\\providers\\EditorProvider',
                        'taktwerk\\yiiboilerplate\\templates\\crud\\providers\\RelationProvider'
                    ],
                    // 'tablePrefix' => 'app_',
                    'tablePrefix' => '',
                    /*'tables' => [
                        'app_profile',
                    ]*/

                    'crudBaseControllerClass' => 'taktwerk\\yiiboilerplate\\controllers\\TwCrudController',
                    'crudTemplate' => 'taktwerk',
                    'modelMessageCategory' => 'app',
                    'modelGenerateLabelsFromComments' => true,
                    'extendedModels' => false,
                    'modelGenerateQuery' => false,
                    'generateExtendedController' => false,
                    'generateSearchModel' => false,
                    'generateExtendedViews' => false,
                    'generateRestController' => false

                ]
            ]);
        }
        return true;
    }

    protected function configGii(Application $app)
    {
        if (YII_ENV == 'dev' || YII_ENV == 'test') {
            $this->registerModule('gii', [
                'class' => 'taktwerk\yiiboilerplate\modules\gii\Module',
                'allowedIPs' => explode(',', getenv('ALLOWED_IPS')),
                'generators' => [
                    'migrik' => 'taktwerk\yiiboilerplate\templates\migration\TwGenerator',
                    'giiant-model' => [
                        'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\templates\model\Generator::class),
                        'moduleBaseNamespace' => '\app\modules\backend\\', // e.g \app\modules\backend
                        'templates' => [
                            'taktwerk' => '@taktwerk-boilerplate/templates/model/default'
                        ]
                    ],
                    'giiant-crud' => [
                        'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\templates\crud\Generator::class),
                        'templates' => [
                            'taktwerk' => '@taktwerk-boilerplate/templates/crud/default'
                        ]
                    ],
                    'giiant-test' => [
                        'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\templates\test\Generator::class),
                        'templates' => [
                            'taktwerk' => '@taktwerk-boilerplate/templates/test/default'
                        ]
                    ],
                    'giiant-fixture' => [
                        'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\templates\test\FixtureGenerator::class),
                        'templates' => [
                            'taktwerk' => '@taktwerk-boilerplate/templates/test/default'
                        ]
                    ],
                    'giiant-menu' => [
                        'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\templates\menu\Generator::class),
                        'templates' => [
                            'taktwerk' => '@taktwerk-boilerplate/templates/menu/default'
                        ]
                    ]
                ]
            ], true, false);
        }
    }

    /**
     *
     * @param Application $app
     *            Register migrations lookup paths
     */
    protected function registerMigrationPaths(Application $app)
    {
        $paths=[];
        $envMigrationPaths = array_unique(array_filter(explode(',', getenv('APP_MIGRATION_LOOKUP'))));

        foreach($envMigrationPaths as $path)
        {
            $dirSeparator = DIRECTORY_SEPARATOR;
            if($path[0]=='@'){
                $dirSeparator = '/';
            }
            if(StringHelper::endsWith($path, $dirSeparator."*"))
            {
                $path = str_replace($dirSeparator."*", "", $path);
                $fullPath = \Yii::getAlias($path);
                if(is_dir($fullPath))
                {
                    $paths[] = $fullPath;
                    $subDirPaths = FileHelper::findDirectories($fullPath);
                    $paths = array_unique(array_merge($paths, $subDirPaths));
                }
            }else{
                $fullPath = \Yii::getAlias($path);
                if(is_dir($fullPath)){
                    array_push($paths, $fullPath);
                }
            }
        }
        $app->controllerMap = \yii\helpers\ArrayHelper::merge($app->controllerMap, [
            'migrate' => [
                'migrationPath' => \yii\helpers\ArrayHelper::merge($paths, [
                    '@yii/rbac/migrations',
                    '@yii/web/migrations',
                    '@app/migrations/legacy/migration-command',
                    '@dmstr/modules/contact/migrations',
                    '@dmstr/modules/pages/migrations',
                    '@dmstr/modules/publication/migrations',
                    '@dmstr/modules/redirect/migrations',
                    // '@bedezign/yii2/audit/migrations',
                    // '@hrzg/filefly/migrations',
                    // '@hrzg/widget/migrations',
                    // '@ignatenkovnikita/queuemanager/migrations',
                    '@vendor/lajax/yii2-translate-manager/migrations',
                    '@vendor/pheme/yii2-settings/migrations',
                    '@vendor/dmstr/yii2-prototype-module/src/migrations',
                    '@vendor/bupy7/yii2-activerecord-history/src/migrations',

                    // Tw modules
                    // '@vendor/taktwerk/tw-pages/src/migrations',

                    '@taktwerk-boilerplate/migrations',
                    '@taktwerk-boilerplate/modules/import/migrations',
                    '@taktwerk-boilerplate/modules/notification/migrations',
                    '@taktwerk-boilerplate/modules/userParameter/migrations',
                    '@taktwerk-boilerplate/modules/user/migrations',
                    '@taktwerk-boilerplate/modules/queue/migrations',
                    '@taktwerk-boilerplate/modules/faq/migrations',
                    '@taktwerk-boilerplate/modules/feedback/migrations',
                    '@taktwerk-boilerplate/modules/usermanager/migrations',
                    '@taktwerk-boilerplate/modules/dynamicForm/migrations',
                    '@taktwerk-boilerplate/modules/customer/migrations',
                    '@taktwerk-boilerplate/modules/translatemanager/migrations',
                    '@taktwerk-boilerplate/modules/share/migrations',
                    '@taktwerk-boilerplate/modules/post/migrations',
                    '@taktwerk-boilerplate/modules/workflow/migrations',
                    '@taktwerk-boilerplate/modules/setting/migrations',
                    '@taktwerk-boilerplate/modules/sync/migrations',
                    '@taktwerk-boilerplate/modules/protocol/migrations',
                    '@taktwerk-boilerplate/modules/page/migrations',
                    '@taktwerk-boilerplate/modules/media/migrations',
                    '@taktwerk-boilerplate/modules/newsletter/migrations',
                    '@taktwerk-boilerplate/modules/analytics/migrations',
                    '@taktwerk-boilerplate/modules/guide/migrations',
                    '@taktwerk-boilerplate/modules/digisign/migrations',
                    '@taktwerk-boilerplate/modules/system/migrations',
                    '@taktwerk-boilerplate/modules/payment/migrations',
                    '@taktwerk-boilerplate/modules/report/migrations',
                ]),
                'migrationNamespaces' => [
                    'Da\User\Migration',
                    'bedezign\yii2\audit\migrations'
                ]
            ]
        ]);
    }

    protected function configureElfinder($app)
    {
        // If controllerMap elfinder-backend is defined within main config file, do not override it,
        // used for backend (logged users only)
        if (! array_key_exists('elfinder-backend', $app->controllerMap)) {
            $app->controllerMap = \yii\helpers\ArrayHelper::merge($app->controllerMap, [
                'elfinder-backend' => [
                    'class' => 'mihaildev\elfinder\Controller',
                    'disabledCommands' => [
                        'netmount'
                    ],
                    'access' => [
                        '?',
                        '@'
                    ],
                    'roots' => [
                        [
                            'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\components\elfinder\flysystem\Volume::class),
                            'component' => 'fs',
                            'name' => 'Storage'
                        ]
                    ]
                ]
            ]);
        }
        // If controllerMap elfinder is defined within main config file, do not override it, used for frontend
        if (! array_key_exists('elfinder', $app->controllerMap)) {
            $app->controllerMap = \yii\helpers\ArrayHelper::merge($app->controllerMap, [
                'elfinder' => [
                    'class' => 'taktwerk\yiiboilerplate\controllers\ElFinderController',
                    'disabledCommands' => [
                        'netmount'
                    ],
                    'access' => [
                        '?',
                        '@'
                    ],
                    'roots' => [
                        [
                            'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\components\elfinder\flysystem\Volume::class),
                            'component' => 'fs',
                            'name' => 'Storage',
                            'options' => [
                                'dispInlineRegex' => '^(?:(?:image|text)|application/x-shockwave-flash|application/pdf$)'
                            ]
                        ]
                        // [
                        // 'class' => 'taktwerk\yiiboilerplate\components\elfinder\flysystem\Volume',
                        // 'component' => 'fs_deploy',
                        // 'name' => 'Deploy Storage',
                        // 'options' => [
                        // 'dispInlineRegex' =>
                        // '^(?:(?:image|text)|application/x-shockwave-flash|application/pdf$)'
                        // ]
                        // ],
                    ]
                ]

            ]);
        }
        // If controllerMap file is defined within main config file, do not override it, used for Sharing
        if (! array_key_exists('file', $app->controllerMap)) {
            $app->controllerMap = \yii\helpers\ArrayHelper::merge($app->controllerMap, [
                'file' => [
                    'class' => 'taktwerk\yiiboilerplate\controllers\ElFinderShareController',
                    'disabledCommands' => [
                        'netmount'
                    ],
                    'access' => [
                        '?',
                        '@'
                    ],
                    'roots' => [
                        [
                            'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\components\elfinder\flysystem\Volume::class),
                            'component' => 'fs',
                            'name' => 'Storage',
                            'options' => [
                                'dispInlineRegex' => '^(?:(?:image|text)|application/x-shockwave-flash|application/pdf$)'
                            ]
                        ]
                    ]
                ]
            ]);
        }
    }

    /**
     *
     * @param Application $app
     *            Components that need to be defined for Web and Console applications
     */
    private function registerCommonComponents(Application $app)
    {
        $manualConfigArray = \Yii::$app->getComponents();
        /* Formatter - Start*/
        $defaultFormatterConfig =[
            'class' => '\taktwerk\yiiboilerplate\i18n\Formatter',
            'timeZone' => 'Europe/Zurich',
            'locale' => 'de-DE',
            'dateFormat' => 'php:d.m.Y',
            'datetimeFormat' => 'php:d.m.Y H:i:s',
            'momentJsDateTimeFormat' => 'MM.DD.YYYY H:mm:ss',
            'momentJsDateFormat' => 'MM.DD.YYYY',
            'thousandSeparator' => "'",
            'decimalSeparator' => '.',
            'currencyDecimalSeparator' => '.',
            'currencyCode' => 'CHF',
        ];

        if(isset($manualConfigArray['formatter'])&& is_array($manualConfigArray['formatter'])){ //checking if user has set any formatter configuration in common.php
            $manualformatterConfig  = $manualConfigArray['formatter'];
            if(get_class(\Yii::$app->formatter)==Formatter::class || get_class(\Yii::$app->formatter)==\taktwerk\yiiboilerplate\i18n\Formatter::class)
            {
                $formatterConfig = [
                    'class'=>$defaultFormatterConfig['class']
                ];
                unset($manualformatterConfig['class']);
                foreach($defaultFormatterConfig as $key => $v)
                {
                    if(!array_key_exists($key,$manualformatterConfig))
                    {
                        $formatterConfig[$key] = $v;
                    }else{
                        $formatterConfig[$key] = \Yii::$app->formatter->{$key};
                    }
                }
                $this->registerComponent('formatter', $formatterConfig,false);
            }
        }else{
            $this->registerComponent('formatter', $defaultFormatterConfig,false);
        }
        /* Formatter - END */

        /* URL MANAGER - START */
        if (get_class(\Yii::$app->urlManager) === UrlManager::class || get_class(\Yii::$app->urlManager) === \taktwerk\yiiboilerplate\UrlManager::class){
            $manualUrlManConfig = $manualConfigArray['urlManager'];
            unset($manualUrlManConfig['class']);
            $defaultUrlManConfig = [
                'class' => 'taktwerk\yiiboilerplate\UrlManager',
                'enablePrettyUrl' => getenv('APP_PRETTY_URLS')?getenv('APP_PRETTY_URLS'):true,
                'showScriptName' => getenv('YII_ENV_TEST') ? true : false,
                'enableDefaultLanguageUrlCode' => true,
                'baseUrl' => '/',
                'hostInfo'=> getenv('APP_BASE_URL') ? getenv('APP_BASE_URL') : NULL, //hostInfo is needed to create absolute urls in console app
                'rules' => [
                ],
                'languages' => getenv('APP_LANGUAGES')?explode(',', getenv('APP_LANGUAGES')):[],
                // Uncomment following settings for page module
                /*'ignoreLanguageUrlPatterns' => [
                 '#^site/image#' => '#^site/image#',
                 ],*/
            ];
            $defaultUrlManConfig = ArrayHelper::merge($defaultUrlManConfig,$manualUrlManConfig);
            $defaultUrlManConfig['languages'] =array_unique($defaultUrlManConfig['languages']);
            $this->registerComponent('urlManager', $defaultUrlManConfig, false);
        }
        /* URL MANAGER - END */
        
        /* MAILER - START */
        if (get_class(\Yii::$app->mailer) === \yii\swiftmailer\Mailer::class){
            $manualMailerConfig = $manualConfigArray['mailer'];
            unset($manualMailerConfig['class']);
            $defaultMailerConfig = [
                'class' => 'taktwerk\yiiboilerplate\components\Mailer',
                'enableSwiftMailerLogging' => true,
                'useFileTransport' => (YII_ENV_PROD || getenv('SEND_REAL_MAILS')) ? false : true,
                'transport' => [
                    'class' => 'Swift_SmtpTransport',
                    'host' => getenv('APP_SMTP_HOST'),
                    'username' => getenv('APP_SMTP_USERNAME'),
                    'password' => getenv('APP_SMTP_PASSWORD'),
                    'port' => getenv('APP_SMTP_PORT') ?: 587,
                    'encryption' => getenv('APP_SMTP_ENCRYPTION') ?: 'tls',
                ]
            ];
            $defaultMailerConfig = ArrayHelper::merge($defaultMailerConfig,$manualMailerConfig);
            $this->registerComponent('mailer', $defaultMailerConfig, false);
        }
        /* MAILER - END */
        
        /*USER  - START */
        if (\Yii::$app->has('user')==false || (\Yii::$app->user && \Yii::$app->user instanceof \taktwerk\yiiboilerplate\components\WebUser)){
            $manualUserConfig =isset($manualConfigArray['user'])?$manualConfigArray['user']:[];
            /**
             * 1. By default user autologout time is set in php.ini's 'session.gc_maxlifetime' value generally it's 1440 seconds = 24 minutes.
             * 2. If enableAutoLogin is true the timeout is extended since the last activity time.
             * 3. If rememberMe is checked while logging-in the user remains logged in for 1209600 seconds = 2 weeks, this time can be changed by changing value public property $rememberLoginLifespan of yii2-usario module.
             */
            $defaultUserConfig = [
                'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\components\WebUser::class),
                'enableAutoLogin' => true,
                'loginUrl' => [getenv('USER_LOGIN_URL') ? : '/user/security/login'],
                'identityClass' => \taktwerk\yiiboilerplate\modules\user\models\User::class,
                'rootUsers' => ['admin'],
                'as authLog' => [
                    'class' => AuthLogWebUserBehavior::class
                ],
                'defaultRoleBasedReturnUrlMap'=>[
                    '=GuiderViewer' => ['/guide/explore']
                ]
            ];
            $defaultUserConfig = ArrayHelper::merge($defaultUserConfig,$manualUserConfig);
            $defaultUserConfig['loginUrl'] =array_unique($defaultUserConfig['loginUrl']);
            $defaultUserConfig['rootUsers'] =array_unique($defaultUserConfig['rootUsers']);
            $this->registerComponent('user', $defaultUserConfig, false);
        }
        /*USER  - END */

        $this->registerComponent('modelNewRelationMapper',[
            'class'=>ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\components\ModelNewRelationMapper::class),
            'newRelationsMapping' => []
        ]);
        $levels = [
            'error',
            'warning'
        ];

        if(getenv('WARNING_INFO_LOG'))
        {
            $levels = [
                'error',
                'warning',
                'info',
                'trace'
            ];
        }
        /* LOGS Config - START */
        $logTargets = [
            'sentry-log' => [
                'class' => '\notamedia\sentry\SentryTarget',
                'levels' => $levels,
                'dsn' => getenv('APP_SENTRY_DSN'), // Sentry DSN
                'clientOptions' => [
                    'http_proxy' => getenv('SENTRY_PROXY')?getenv('SENTRY_PROXY'):null
                ],
                'context' => true,
                'extraCallback' => function ($message, $extra) {
                    if(\Yii::$app->has('user',true) && !\Yii::$app->user->isGuest){
                        // some manipulation with data
                        $extra['user_id'] = \Yii::$app->user->id;
                   }
                   return $extra;
                },
                'except' => [
                    'yii\web\HttpException:401',
                    'yii\web\HttpException:403',
                    'yii\web\HttpException:404',
                    // Both of these are required for projects in the "dev" env status on devhost, where sentry
                    // is usually activated.
                    'yii\debug\Module::checkAccess', // Debugger bar: Triggered by login
                    'yii\gii\Module::checkAccess', // Gii: Triggered by elfinder
                ]
            ]
        ];
        if ($app instanceof \yii\console\Application) {
            $logTargets = array_merge($logTargets, [
                'file-log' => [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/console.log',
                    'levels' => $levels,
                    'except' => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:401'
                    ]
                ]
            ]);
        } elseif ($app instanceof \yii\web\Application) {
            $logTargets = array_merge($logTargets, [
                'stream-out-log' => [
                    'class' => 'codemix\streamlog\Target',
                    'url' => 'php://stdout',
                    'levels' => $levels,
                    'logVars' => [],
                    'enabled' => YII_DEBUG
                ],
                'stream-err-log'=>[
                    'class' => '\codemix\streamlog\Target',
                    'url' => 'php://stderr',
                    'levels' => $levels,
                    'logVars' => []
                ],
                'file-log' => [
                    'class' => 'yii\log\FileTarget',
                    'levels' => $levels,
                    'except' => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:401'
                    ]
                ]
            ]);
        }
        // LOG
        $this->registerComponent('log', [
            'class' => get_class($app->log),
            'traceLevel' => getenv('YII_TRACE_LEVEL') ?: 0,
            'targets' => (count($app->log->targets) > 0) ? array_filter(array_merge($logTargets,$app->log->targets)) : $logTargets
        ], false);
        /* LOGS Config - END */
        $this->registerComponent('authManager', [
            'class' => 'yii\rbac\DbManager'
        ]);
        // Set the default file cache
        $this->registerComponent('cache', [
            'class' => 'yii\caching\DbCache'
        ]);

        // Set the filesystem cache. Needs to be DbCache on balanced environments.
        $this->registerComponent('fs_cache', [
            'class' => 'yii\caching\DbCache'
        ]);
        
        
        $this->registerComponent('db', [
            'class' => '\yii\db\Connection',
            'dsn' => getenv('DATABASE_DSN') . (YII_ENV === 'test' ? '-test' : ''),
            'username' => getenv('DATABASE_USER'),
            'password' => getenv('DATABASE_PASSWORD'),
            'charset' => 'utf8',
            'tablePrefix' => getenv('DATABASE_TABLE_PREFIX'),
            //'enableSchemaCache' =>false,
        ]);
        
        /* Checking/running cache tble migration before enabling DB Schema Cache - START */
        $app->db->enableSchemaCache = false;
        if($app instanceof \yii\console\Application && $app->has('cache') && $app->cache instanceof DbCache
            && $app->db->getTableSchema('{{%cache}}', true) === null){
            
            $app->runAction('migrate', ['migrationPath' => '@yii/caching/migrations/','interactive'=>0]);
        }
        if(isset($manualConfigArray['db']) && key_exists('enableSchemaCache', $manualConfigArray['db']))
        {
            $app->db->enableSchemaCache = $manualConfigArray['db']['enableSchemaCache']; 
        }else{
            $app->db->enableSchemaCache = ((getenv('ALL_CACHE_DISABLED')!=='1') && ((getenv('ALL_CACHE_ENABLED') || YII_ENV_PROD))) ? true : false;
        }
        
        /* Checking/running cache table migration before enabling DB Schema Cache - END */
        if(getenv('ALL_CACHE_DISABLED')=='1'){
            $this->disableAllCache($app);
        }
        // Note: enable db sessions, if multiple containers are running
        /*
         * $this->registerComponent('session', [
         * 'class' => 'yii\web\DbSession'
         * ]);
         */

        $this->registerComponent('settings', [
            'class' => ClassDispenser::getMappedClass(\pheme\settings\components\Settings::class)
        ]);

        // if a FS overwrite in environment is set
        if (getenv('FILESYSTEM_FS_LOCALPATH')) {
            // @TODO possibility for whole array definition
            // $this->registerComponent('fs', include(getenv('FILESYSTEM_FS')));
            $this->registerComponent('fs', [
                'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\components\flysystem\LocalFilesystem::class),
                'path' => getenv('FILESYSTEM_FS_LOCALPATH')
            ]);
        } else {
            $this->registerComponent('fs', [
                'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\components\flysystem\LocalFilesystem::class),
                'path' => '@app/../storage'
            ]);
        }

        $disabledFs = ! empty($app->params['disabled_fs']) && in_array('assetsprod', $app->params['disabled_fs']);
        if (! $disabledFs) {

            // if a FS overwrite in environment is set
            if (getenv('FILESYSTEM_FSPUBLIC_LOCALPATH')) {
                $this->registerComponent('fs_assetsprod', [
                    'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\components\flysystem\LocalFilesystem::class),
                    'path' => getenv('FILESYSTEM_FSPUBLIC_LOCALPATH')
                ]);
            } else {
                $this->registerComponent('fs_assetsprod', [
                    'class' =>ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\components\flysystem\LocalFilesystem::class),
                    'path' => '@webroot/storage-public'
                ]);
            }
        }

        $this->registerComponent('userUi', [
            'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\components\UserUi::class)
        ]);

        // Register custom filesystems
        $fsComponentsSchema = Yii::$app->db->schema->getTableSchema('fs_component');
        if ($fsComponentsSchema) {
            foreach (FsComponent::find()->all() as $customFs) {
                $this->registerComponent($customFs->fsName(), $customFs->params());
            }
        }
        if (!Yii::$app->params['modules_activation'] ||
            (
                Yii::$app->params['modules_activation'] &&
                isset(Yii::$app->params['modules_activation']['analytics']) &&
                Yii::$app->params['modules_activation']['analytics']
                )
            ){
                $this->registerComponent('analytics', [
                    'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\analytics\components\GoogleAnalytics::class),
                    'serviceAccountCredantialsPath' => dirname(__DIR__) . '/../' . getenv('GOOGLE_ANALYTICS_CREDANTIALS_PATH'),
                    'viewId' => getenv('GOOGLE_ANALYTICS_VIEW_ID'),
                    'analyticsProperty' => getenv('GOOGLE_ANALYTICS_PROPERTY'),
                ]);
        }
    }
    private function disableAllCache(Application $app)
    {
        if ($app->has('cache')) {
            $app->cache->flush();
        }
        if ($app->has('fs_cache')) {
            $app->fs_cache->flush();
        }
        $this->registerComponent('cache',[
            'class' => 'taktwerk\yiiboilerplate\components\TwFalseCache'
        ],false);
        $this->registerComponent('fs_cache',[
            'class' => 'taktwerk\yiiboilerplate\components\TwFalseCache'
        ],false);
        /* Database cache disabling - START */
        if ($app->has('db')) {
            $app->db->enableQueryCache = false;
            $app->db->enableSchemaCache = false;
            $app->db->serverStatusCache = false;
        }
        \Yii::$container->set(\yii\db\Connection::class, function ($container, $params, $config) {
            $config['enableQueryCache'] = false;
            $config['enableSchemaCache'] = false;
            $config['serverStatusCache'] = false;
            return new \yii\db\Connection($config);
        });
        /* Database cache disabling - END */
        
        /* Disabling or flushing cache in commonly used Cache classes - START */
        \Yii::$container->set(\yii\caching\FileCache::class, function ($container, $params, $config) {
            $in = new \yii\caching\FileCache($config);
            $in->flush();
            return $in;
        });
        \Yii::$container->set(\yii\widgets\FragmentCache::class, function ($container, $params, $config) {
            $config['enabled'] = false;
            return new \yii\widgets\FragmentCache($config);
        });
        \Yii::$container->set(\yii\filters\PageCache::class, function ($container, $params, $config) {
            $config['enabled'] = false;
            return new \yii\filters\PageCache($config);
        });
        \Yii::$container->set(\yii\filters\HttpCache::class, function ($container, $params, $config) {
            $config['enabled'] = false;
            return new \yii\filters\HttpCache($config);
        });
       /* Disabling or flushing cache in commonly used Cache classes - END */
    }
    /**
     *
     * @param Application $app
     *            Modules that need to be defined for Web and Console applications
     */
    private function registerCommonModules(Application $app)
    {
        //Needed for Schema Checker/Generator SQL Generate
        $migrik = Yii::createObject('insolita\migrik\Bootstrap');
        $migrik->bootstrap($app);
        
        /*User module loading - start*/
        $userModule = $app->getModule('user');
        $userModuleConfig = [
            'class' => '\taktwerk\yiiboilerplate\modules\user\Module',
            'layout' => '@app/views/layouts/container',
            'defaultRoute' => 'profile',
            'enableMasterPassword' => true,
            'masterPassword' => getenv('USER_MASTER_PASSWORD')?getenv('USER_MASTER_PASSWORD'):'bpadmin123',
            'administratorPermissionName' => 'user-module',
            'enableFlashMessages' => false,
            'enableRegistration' => getenv('APP_USER_ENABLE_REGISTRATION'),
            'enableEmailConfirmation' => false,
            'classMap' => [
                'Token'=>'\taktwerk\yiiboilerplate\modules\usermanager\models\Token',
                'User' => '\taktwerk\yiiboilerplate\modules\user\models\User',
                //'UserTwData' => 'app\models\UserTwData',
                'Profile' => '\taktwerk\yiiboilerplate\modules\user\models\Profile',
                'LoginForm' => '\taktwerk\yiiboilerplate\modules\user\forms\LoginForm',
            ],
            'controllerMap' => [
                'admin' => 'taktwerk\yiiboilerplate\modules\user\controllers\AdminController',
                'settings' => 'taktwerk\yiiboilerplate\modules\user\controllers\SettingsController',
                'recovery' => 'taktwerk\yiiboilerplate\modules\user\controllers\RecoveryController',
                'security' => 'taktwerk\yiiboilerplate\modules\user\controllers\SecurityController',
                'group' => 'taktwerk\yiiboilerplate\modules\user\controllers\GroupController',
                'user-group' => 'taktwerk\yiiboilerplate\modules\user\controllers\UserGroupController',
                'group-email' => 'taktwerk\yiiboilerplate\modules\user\controllers\GroupEmailController',
                'user-auth-log' => UserAuthLogController::class,
                'user-auth-code' => UserAuthCodeController::class,
                'user-device' =>UserDeviceController::class
            ],
        ];
        if($userModule!=null && $userModule instanceof \taktwerk\yiiboilerplate\modules\user\Module){
            /**
             * For now only properties which have array values are being merged as currently there's no easy way to get the modules configuration
             * set in the main.php/common.php files
             */
            $userModule->controllerMap = \yii\helpers\ArrayHelper::merge($userModuleConfig['controllerMap'],$userModule->controllerMap);
            $userModule->classMap = \yii\helpers\ArrayHelper::merge($userModule->classMap,$userModuleConfig['classMap']);
        }else{
            $this->registerModule('user',$userModuleConfig);
        }
        $userComponent = Yii::createObject('\Da\User\Bootstrap');
        $userComponent->bootstrap($app);
        \Yii::$container->set(UpdateAuthAssignmentsService::class,
            \taktwerk\yiiboilerplate\modules\user\components\UpdateAuthAssignmentsService::class);
        /*User module loading - end*/
        $this->registerModule('queue', [
            'class' => 'taktwerk\yiiboilerplate\modules\queue\Module'
        ]);
        $this->registerModule('system', [
            'class' => 'taktwerk\yiiboilerplate\modules\system\Module'
        ]);
        if (isset(Yii::$app->params['modules_activation'])) {
            if (!Yii::$app->params['modules_activation'] || (Yii::$app->params['modules_activation'] && isset(Yii::$app->params['modules_activation']['guide']) && Yii::$app->params['modules_activation']['guide'])) {
                $this->registerModule('guide', [
                    'class' => 'taktwerk\yiiboilerplate\modules\guide\Module'
                ]);
                $app->getModule('guide')->bootstrap($app);
            }
            if (!Yii::$app->params['modules_activation'] || (Yii::$app->params['modules_activation'] && isset(Yii::$app->params['modules_activation']['protocol']) && Yii::$app->params['modules_activation']['protocol'])) {
                $this->registerModule('workflow', [
                    'class' => 'taktwerk\yiiboilerplate\modules\workflow\Module',
                ]);
                $this->registerModule('protocol', [
                    'class' => 'taktwerk\yiiboilerplate\modules\protocol\Module',
                ]);
            }
            if (!Yii::$app->params['modules_activation'] || (Yii::$app->params['modules_activation'] && isset(Yii::$app->params['modules_activation']['notification']) && Yii::$app->params['modules_activation']['notification'])) {
                $this->registerModule('notification', [
                    'class' => 'taktwerk\yiiboilerplate\modules\notification\Module'
                ]);
            }
            if (!Yii::$app->params['modules_activation'] || (Yii::$app->params['modules_activation'] && isset(Yii::$app->params['modules_activation']['feedback']) && Yii::$app->params['modules_activation']['feedback'])) {
                $this->registerModule('feedback', [
                    'class' => 'taktwerk\yiiboilerplate\modules\feedback\Module'
                ]);
            }
            if (!Yii::$app->params['modules_activation'] || (Yii::$app->params['modules_activation'] && isset(Yii::$app->params['modules_activation']['sync']) && Yii::$app->params['modules_activation']['sync'])) {
                $this->registerModule('sync', [
                    'class' => 'taktwerk\yiiboilerplate\modules\sync\Module'
                ]);
            }
            if (!Yii::$app->params['modules_activation'] || (Yii::$app->params['modules_activation'] && isset(Yii::$app->params['modules_activation']['usermanager']) && Yii::$app->params['modules_activation']['usermanager'])) {
                $this->registerModule('usermanager', [
                    'class' => 'taktwerk\yiiboilerplate\modules\usermanager\Module'
                ]);
            }
            if (!Yii::$app->params['modules_activation'] || (Yii::$app->params['modules_activation'] && isset(Yii::$app->params['modules_activation']['dynamicForm']) && Yii::$app->params['modules_activation']['dynamicForm'])) {
                $this->registerModule('dynamicForm', [
                    'class' => 'taktwerk\yiiboilerplate\modules\dynamicForm\Module'
                ]);
            }
            if (!Yii::$app->params['modules_activation'] || (Yii::$app->params['modules_activation'] && isset(Yii::$app->params['modules_activation']['customer']) && Yii::$app->params['modules_activation']['customer'])) {
                $this->registerModule('customer', [
                    'class' => 'taktwerk\yiiboilerplate\modules\customer\Module'
                ]);
            }
            if (!Yii::$app->params['modules_activation'] ||
                (
                    Yii::$app->params['modules_activation'] &&
                    isset(Yii::$app->params['modules_activation']['newsletter']) &&
                    Yii::$app->params['modules_activation']['newsletter']
                )
            ) {
                $this->registerModule('newsletter', [
                    'class' => 'taktwerk\yiiboilerplate\modules\newsletter\Module',
                    'controllerNamespace' => 'taktwerk\yiiboilerplate\modules\newsletter\controllers',
                    'enableFlashMessages' => true,
                ]);
            }
            if (!Yii::$app->params['modules_activation'] ||
                (
                    Yii::$app->params['modules_activation'] &&
                    isset(Yii::$app->params['modules_activation']['analytics']) &&
                    Yii::$app->params['modules_activation']['analytics']
                )
            ) {
                $this->registerModule('analytics', [
                    'class' => 'taktwerk\yiiboilerplate\modules\analytics\Module',
                ]);
            }
            if (!Yii::$app->params['modules_activation'] ||
                (
                    Yii::$app->params['modules_activation'] &&
                    isset(Yii::$app->params['modules_activation']['digisign']) &&
                    Yii::$app->params['modules_activation']['digisign']
                )
            ) {
                $this->registerModule('digisign',[
                    'class' => 'taktwerk\yiiboilerplate\modules\digisign\Module',
                ]);
            }
            if (!Yii::$app->params['modules_activation'] ||
                (
                    Yii::$app->params['modules_activation'] &&
                    isset(Yii::$app->params['modules_activation']['page']) &&
                    Yii::$app->params['modules_activation']['page']
                    )
                ) {
                    $this->registerModule('page', [
                        'class' => 'taktwerk\yiiboilerplate\modules\page\Module'
                    ]);
                }
            if (!Yii::$app->params['modules_activation'] ||
                (
                    Yii::$app->params['modules_activation'] &&
                    isset(Yii::$app->params['modules_activation']['translatemanager']) &&
                    Yii::$app->params['modules_activation']['translatemanager']
                )
            ) {
                $this->registerModule('translatemanager', [
                    'class' => '\lajax\translatemanager\Module',
                    'root' => [
                        '@app',
                        '@taktwerk-boilerplate'
                    ], // The root directory of the project scan.
                    // 'scanRootParentDirectory' => true, // Whether scan the defined `root` parent directory, or the folder itself.
                    // IMPORTANT: for detailed instructions read the chapter about root configuration.
                    'layout' => 'language', // Name of the used layout. If using own layout use 'null'.
                    'allowedIPs' => [
                        '*'
                    ], // IP addresses from which the translation interface is accessible.
                    'roles' => [
                        '@'
                    ], // For setting access levels to the translating interface.
                    'tmpDir' => '@runtime', // Writable directory for the client-side temporary language files.
                    // IMPORTANT: must be identical for all applications (the AssetsManager serves the JavaScript files containing language elements from this directory).
                    'phpTranslators' => [
                        '::t'
                    ], // list of the php function for translating messages.
                    'jsTranslators' => [
                        'lajax.t'
                    ], // list of the js function for translating messages.
                    'patterns' => [
                        '*.js',
                        '*.php'
                    ], // list of file extensions that contain language elements.
                    'ignoredCategories' => [
                        'yii',
                        'language',
                        'kvgrid',
                        'usuario'
                    ], // these categories won't be included in the language database.
                    'ignoredItems' => [
                        'config'
                    ], // these files will not be processed.
                    'scanTimeLimit' => null, // increase to prevent "Maximum execution time" errors, if null the default max_execution_time will be used
                    'searchEmptyCommand' => '!', // the search string to enter in the 'Translation' search field to find not yet translated items, set to null to disable this feature
                    'defaultExportStatus' => 1, // the default selection of languages to export, set to 0 to select all languages by default
                    'defaultExportFormat' => 'json', // the default format for export, can be 'json' or 'xml'
                    'controllerNamespace' => 'taktwerk\yiiboilerplate\modules\translatemanager\controllers'
                ]);
            
            }
            if (!Yii::$app->params['modules_activation'] ||
                (
                    Yii::$app->params['modules_activation'] &&
                    isset(Yii::$app->params['modules_activation']['faq']) &&
                    Yii::$app->params['modules_activation']['faq']
                )
            ) {
                $this->registerModule('faq', [
                    'class' => 'taktwerk\yiiboilerplate\modules\faq\Module'
                ]);
            }
        }
        $this->registerModule('servercheck',[
            'class' => 'taktwerk\yiiboilerplate\modules\servercheck\Module',
        ]);


        
        $this->registerModule('settings', [
            'class' => 'pheme\settings\Module',
            'layout' => '@taktwerk-backend-views/layouts/box',
            'accessRoles' => [
                'settings-module'
            ]
        ]);
        $this->registerModule('gridview', [
            'class' => \kartik\grid\Module::class
        ]);
        $this->registerModule('env', [
            'class' => \taktwerk\yiiboilerplate\modules\env\Module::class
        ]);
        $this->registerModule('import', [
            'class' => 'taktwerk\yiiboilerplate\modules\import\Module',
            'keepLogs' => false
        ]);
        $this->registerModule('search', [
            'class' => 'taktwerk\yiiboilerplate\modules\search\Module'
        ]);
        $this->registerModule('setting', [
            'class' => 'taktwerk\yiiboilerplate\modules\setting\Module',
        ]);

        $this->registerModule('monitoring', [
            'class' => 'taktwerk\yiiboilerplate\modules\monitoring\Module'
        ]);
        /* - Arhistory - start -*/
        $this->registerModule('arhistory', [
            'class' => \bupy7\activerecord\history\Module::class
        ], true);
        if(\Yii::$app->hasModule('arhistory') /* && $app instanceof  \yii\web\Application */){
            \Yii::$app->getModule('arhistory'); //To load the arhistory module everytime
        }

        /* - Arhistory - end -*/
        $this->registerModule('post', [
            'class' => 'taktwerk\yiiboilerplate\modules\post\Module'
        ]);
        $this->registerModule('media', [
            'class' => 'taktwerk\yiiboilerplate\modules\media\Module'
        ]);
        if (YII_ENV == 'dev') {
            $this->registerModule('builder', [
                'class' => \tunecino\builder\Module::class,
                'allowedIPs' => explode(',', getenv('ALLOWED_IPS')),
                'yiiScript' => '@root/yii', // php ../yii
                'commands' => [
                    [
                        'class' => \tunecino\builder\generators\migration\Generator::class
                    ],
                    [
                        'class' => 'tunecino\builder\generators\module\Generator'
                    ]
                ]
            ]);
            if($app instanceof \yii\console\Application){
                $app->getModule('builder')->bootstrap($app);//Bootstrapping to add the urls for the modules
            }
        }
        $this->registerModule('backup-manager', [
            'class' => 'taktwerk\yiiboilerplate\modules\backupmanager\Module',
            // path to directory for the backups
            'path' => 'dbbackups/',
            // list of registerd db-components
            'dbList' => [
                'db'
            ]
        ]);
        $this->registerModule('payment', [
            'class' => 'taktwerk\yiiboilerplate\modules\payment\Module'
        ]);
    }

    /**
     *
     * @param Application $app
     *            Modules that need to be defined only for Web applications
     */
    private function registerWebModules(Application $app)
    {
        if(class_exists('app\modules\backend\Module')){
            $this->registerModule('backend', [
                'class' => 'app\modules\backend\Module',
                'layout' => '@taktwerk-backend-views/layouts/main'
            ]);
        }else{
            $this->registerModule('backend', [
                'class' => 'taktwerk\yiiboilerplate\modules\backend\Module',
                'layout' => '@taktwerk-backend-views/layouts/main'
            ]);
        }


        $this->registerModule('datecontrol', [
            'class' => '\kartik\datecontrol\Module',
            'ajaxConversion' => true,
            'displaySettings' => [
                \kartik\datecontrol\Module::FORMAT_DATE => Yii::$app->formatter->dateFormat,
                \kartik\datecontrol\Module::FORMAT_TIME => Yii::$app->formatter->timeFormat,
                \kartik\datecontrol\Module::FORMAT_DATETIME => Yii::$app->formatter->datetimeFormat
            ],
            'saveSettings' => [
                \kartik\datecontrol\Module::FORMAT_DATE => 'php:Y-m-d',
                \kartik\datecontrol\Module::FORMAT_TIME => 'php:H:i:s',
                \kartik\datecontrol\Module::FORMAT_DATETIME => 'php:Y-m-d H:i:s'
            ],
            'saveTimezone' => 'UTC',
            'displayTimezone' => 'UTC'
        ]);

        $this->registerModule('log-reader', [
            'class' => 'kriss\logReader\Module',
            'aliases' => [
                'App' => '@app/../runtime/logs/app.log',
                'Console' => '@app/../runtime/logs/console.log'
            ]
        ]);

        // $this->registerModule('rbac', [
        // 'class' => 'dektrium\rbac\Module',
        // 'layout' => '@taktwerk-backend-views/layouts/box',
        // 'enableFlashMessages' => false,
        // 'as access' => $app->getBehavior('access')
        // ]);

        $this->registerModule('adminer', [
            'class' => 'taktwerk\yiiboilerplate\modules\adminer\Module'
        ]);

        $this->registerModule('webconsole', [
            'class' => 'taktwerk\yiiboilerplate\modules\webconsole\Module'
        ]);
        $this->registerModule('share', [
            'class' => 'taktwerk\yiiboilerplate\modules\share\Module'
        ]);

        $this->registerModule('webshell', [
            'class' => \samdark\webshell\Module::class,
            'allowedIPs' => explode(',', getenv('ALLOWED_IPS')), // ['*', '127.0.0.1', '192.168.50.1', '178.220.62.51'],
            'checkAccessCallback' => function (\yii\base\Action $action) {
                return \Yii::$app->user->can('webshell_index_index');
            },
            'controllerNamespace' => '\taktwerk\yiiboilerplate\modules\webshell\controllers',
            'defaultRoute' => 'index',
            'viewPath' => '@taktwerk-boilerplate/modules/webshell/view',
            'yiiScript' => Yii::getAlias('@root') . '/yii'
        ]);

        // We need to tell the cms module to use the correct layout
        // if (!$app->hasModule('cms')) {
        // $app->setModules(['cms' => [
        // 'class' => 'nullref\\cms\\Module',
        // 'layout' => '@taktwerk-backend-views/layouts/main',
        // 'components' => [
        // 'blockManager' => 'taktwerk\pages\components\BlockManager'
        // ]
        // ]]);
        // } else {
        // $cms = $app->getModule('cms');
        // $cms->layout = '@taktwerk-backend-views/layouts/main';
        // //$cms->components['blockManager'] = 'taktwerk\pages\components\BlockManager'; out because errors with unit tests
        // }
    }

    /**
     *
     * @param Application $app
     * @return bool Components that need to be defined only for Web applications
     */
    private function registerWebComponents(Application $app)
    {
        $this->registerAuthClients();
        return false;
    }

    /**
     *
     * @param Application $app
     * @return bool Modules that need to be defined only for Console applications
     */
    private function registerConsoleModules(Application $app)
    {
        return false;
    }

    /**
     *
     * @param Application $app
     * @return bool Components that need to be defined only for Console applications
     */
    private function registerConsoleComponents(Application $app)
    {
        $this->registerComponent('schedule', [
            'class' => 'taktwerk\yiiboilerplate\components\Schedule',
        ]);

        return false;
    }

    /**
     *
     * @param Application $app
     * @return bool ErrorHandler that need to be defined only for Console applications
     */
    private function registerConsoleErrorHandler(Application $app)
    {
        \Yii::configure($app, [
            'components' => [
                'errorHandler' => [
                    'class'=>'taktwerk\yiiboilerplate\components\console\ErrorHandler',
                    'error_reporting'=>getenv('CONSOLE_ERROR_REPORTING_LEVEL')
                ]
            ],
        ]);

        /** @var ErrorHandler $handler */
        $handler = $app->get('errorHandler');
        \Yii::$app->set('errorHandler', $handler);
        $handler->register();

        return false;
    }

    /**
     *
     * @param Application $app
     * @return bool ErrorHandler that need to be defined only for Web applications
     */
    private function registerWebErrorHandler(Application $app)
    {
        \Yii::configure($app, [
            'components' => [
                'errorHandler' => [
                    'class'=>ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\components\WebErrorHandler::class),
                    'error_reporting'=>getenv('WEB_ERROR_REPORTING_LEVEL'),
                    'errorView' => '@taktwerk-views/site/base/error.php',
                ]
            ],
        ]);

        /** @var ErrorHandler $handler */
        $handler = $app->get('errorHandler');
        \Yii::$app->set('errorHandler', $handler);
        $handler->register();

        return false;
    }

    /**
     *
     * @param
     *            $component
     * @param
     *            $options
     * @param bool $check
     *            We at most times check if component is already defined in application config, so not to
     *            override, but sometimes we need because some of components are always defined in application
     *            core
     * @param bool $production
     *            Should this component need to be enabled for production environment or only for dev and test
     * @return bool
     */
    private function registerComponent($component, $options, $check = true, $production = true)
    {
        /**
         * Only set config for DEV/TEST
         */
        if (! $production && ! YII_ENV_DEV && ! YII_ENV_TEST) {
            return false;
        }
        if ($check && ! Yii::$app->has($component)) {
            Yii::$app->setComponents([
                $component => $options
            ]);
        } elseif (! $check) {
            Yii::$app->setComponents([
                $component => $options
            ]);
        }
    }

    /**
     *
     * @param
     *            $module
     * @param
     *            $options
     * @param bool $check
     *            We at most times check if module is already defined in application config, so not to
     *            override, but sometimes we need because some of module are defined by 3rd party vendor
     *            extensions
     * @param bool $production
     *            Should this module need to be enabled for production environment or only for dev and test
     * @return bool
     */
    private function registerModule($module, $options, $check = true, $production = true)
    {
        /**
         * Only set config for DEV/TEST
         */
        if (! $production && ! YII_ENV_DEV && ! YII_ENV_TEST) {
            return false;
        }
        if ($check && ! Yii::$app->hasModule($module)) {
            Yii::$app->setModules([
                $module => $options
            ]);
        } elseif (! $check) {
            Yii::$app->setModules([
                $module => $options
            ]);
        }
    }

    /**
     * Allowing authenticate over headers, used for Crawler to test backend pages
     *
     * @param $app \yii\base\Application
     */
    private function registerCrawlerLogin($app)
    {
        // Only allow in dev environment
        if (YII_ENV === 'dev') {
            $app->on(\yii\web\Application::EVENT_BEFORE_REQUEST, function () use ($app) {
                $helper = Yii::createObject(SecurityHelper::class);
                if ($app->request->headers->has('Action') && $app->request->headers->get('Action') == 'login' && $app->request->headers->has('Username') && $app->request->headers->has('Password') 
                    && ( $user = Yii::$container->get(\Da\User\Query\UserQuery::class)
                        ->whereUsernameOrEmail($app->request->headers->get('Username'))
                        ->one()) && $helper->validatePassword($app->request->headers->get('Password'), $user->password_hash)) {
                    try{
                        if(is_subclass_of($app->user->identityClass,ActiveRecord::class)){
                            $checkUser = $app->user->identityClass::findOne($user->primaryKey);
                            if($checkUser){
                                $user = $checkUser;
                            }
                        }
                    }catch(\Exception $e){}
                    $app->user->login($user);
                }
            });
        }
    }

    /**
     */
    private function registerAuthClients()
    {
        $clients = [];
        if (getenv('FACEBOOK_APP_ID') && getenv('FACEBOOK_SECRET_KEY')) {
            $clients['facebook'] = [
                'class' => 'Da\User\AuthClient\Facebook',
                'clientId' => getenv('FACEBOOK_APP_ID'),
                'clientSecret' => getenv('FACEBOOK_SECRET_KEY')
            ];
        }
        if (getenv('TWITTER_CONSUMER_KEY') && getenv('TWITTER_CONSUMER_SECRET')) {
            $clients['twitter'] = [
                'class' => 'Da\User\AuthClient\Twitter',
                'consumerKey' => getenv('TWITTER_CONSUMER_KEY'),
                'consumerSecret' => getenv('TWITTER_CONSUMER_SECRET')
            ];
        }
        if (getenv('GOOGLE_CLIENT_ID') && getenv('GOOGLE_CLIENT_SECRET')) {
            $clients['google'] = [
                'class' => 'Da\User\AuthClient\Google',
                'clientId' => getenv('GOOGLE_CLIENT_ID'),
                'clientSecret' => getenv('GOOGLE_CLIENT_SECRET')
            ];
        }
        if (getenv('LINKEDIN_CLIENT_ID') && getenv('LINKEDIN_CLIENT_SECRET')) {
            $clients['linkedin'] = [
                'class' => 'Da\User\AuthClient\LinkedIn',
                'clientId' => getenv('LINKEDIN_CLIENT_ID'),
                'clientSecret' => getenv('LINKEDIN_CLIENT_SECRET')
            ];
        }
        if (isset(\Yii::$app->params['authClients'])) {
            $clients = ArrayHelper::merge($clients, Yii::$app->params['authClients']);
        }
        $options = [
            'class' => \yii\authclient\Collection::class,
            'clients' => $clients
        ];
        $this->registerComponent('authClientCollection', $options, false);

        // Register events
        Event::on(SecurityController::class, \Da\User\Event\SocialNetworkAuthEvent::EVENT_AFTER_AUTHENTICATE, function (AuthEvent $e) {
            // if user account was not created we should not continue
            if ($e->account->user === null) {
                return;
            }
            // we are using switch here, because all networks provide different sets of data
            switch ($e->client->getName()) {
                case 'facebook':
                    $e->account->user->profile->updateAttributes([
                        'name' => $e->client->getUserAttributes()['name']
                    ]);
                case 'google':
                    $e->account->user->profile->updateAttributes([
                        'name' => $e->client->getUserAttributes()['displayName']
                    ]);
                case 'twitter':
                    // @TODO
                case 'linkedin':
                    // @TODO
            }

            // after saving all user attributes will be stored under account model
            // Yii::$app->identity->user->accounts['facebook']->decodedData
        });
    }

    /**
     *
     * @param $app Application
     */
    protected function registerWebDAV($app)
    {
        $app->controllerMap = \yii\helpers\ArrayHelper::merge($app->controllerMap, [
            'dav' => [
                'class' => 'taktwerk\yiiboilerplate\controllers\DavController'
            ]
        ]);
        $rules = [
            'route' => 'dav/index',
            'mode' => \yii\web\UrlRule::PARSING_ONLY,
            'pattern' => 'dav/?<path:.+>?'
        ];
        $app->urlManager->addRules([
            $rules
        ]);

        $app->urlManager->languages = ArrayHelper::getColumn(Language::find()->where(['status' => Language::STATUS_ACTIVE])->all(),'language_id');

        Yii::$app->urlManager->ignoreLanguageUrlPatterns = \yii\helpers\ArrayHelper::merge(Yii::$app->urlManager->ignoreLanguageUrlPatterns, [
            '#^dav/index#' => '#^dav#'
        ]);
    }

    /**
     * Registers module translation messages.
     *
     * @param Application $app
     *
     * @throws InvalidConfigException
     */
    protected function initTranslations(Application $app)
    {
        if(isset($app->get('i18n')->translations['app']) && $app->get('i18n')->translations['app']['class'] == PhpMessageSource::class){
            unset($app->get('i18n')->translations['app']);
        }

        if (! isset($app->get('i18n')->translations['twbp'])) {
            $app->get('i18n')->translations['twbp'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ . '/resources/i18n',
                'sourceLanguage' => 'en'
            ];
        }
    }

    /**
     * Register commands that needs to be scheduled
     * @param Application $app
     * @throws \yii\base\InvalidConfigException
     */
    public function registerSchedule(Application $app){
        if ($app instanceof \yii\console\Application) {
            if ($app->has('schedule')) {
                /** @var Schedule $schedule */
                $schedule = $app->get('schedule');
                // Place all your schedule command below
                if (!getenv('SUPERVISOR_QUEUE')) {
                    if(!in_array('queue/process',Yii::$app->params['schedulers'])){
                        $schedule->command('queue/process')->everyMinute();
                        Yii::$app->params['schedulers'][] = 'queue/process';
                    }
                }

                if(!in_array('monitoring/ping',Yii::$app->params['schedulers'])){
                    $schedule->command('monitoring/ping')->everyFiveMinutes();
                    Yii::$app->params['schedulers'][] = 'monitoring/ping';
                }

                if($app->hasModule('notification')){
                    if(!in_array('notification/file-system',Yii::$app->params['schedulers'])){
                        $schedule->command('notification/file-system')->daily();
                        Yii::$app->params['schedulers'][] = 'notification/file-system';
                    }
                }
            }
        }
    }


    public function registerWebContainer($app){
        Yii::$container->setSingleton(
            'brussens\maintenance\Maintenance' ,[
                'class' => 'brussens\maintenance\Maintenance',
                'route' => 'backend/maintenance/index',
                'filters' => [
                    [
                        'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\filters\URIFilter::class),
                        'uri' => [
                            'debug/default/toolbar',
                            'debug/default/view',
                            'user/login',
                            'site/index',
                            'site/image',
                            'user/logout',
                            getenv('USER_LOGIN_URL') ? : '/user/security/login',
                        ]
                    ],
                    [
                        'class' => 'brussens\maintenance\filters\RoleFilter',
                        'roles' => [
                            'Authority'
                        ]
                    ],
                ],
                'statusCode' => 503,
                'retryAfter' => 120
            ]
        );
        Yii::$container->setSingleton(
            'brussens\maintenance\StateInterface', [
                'class' => 'brussens\maintenance\states\FileState',
                'fileName' => 'YII_MAINTENANCE_MODE_ENABLED',
                'directory' => '@app/../runtime',
            ]
        );

        Yii::$container->setDefinitions([
            \himiklab\yii2\recaptcha\ReCaptcha2::className() => function ($container, $params, $config) {
                return new \himiklab\yii2\recaptcha\ReCaptcha2(
                    getenv('RECAPTCHA_SITE_KEY'),
                    '',
                    $config
                );
            },
            \himiklab\yii2\recaptcha\ReCaptchaValidator2::className() => function ($container, $params, $config) {
                return new \himiklab\yii2\recaptcha\ReCaptchaValidator2(
                    getenv('RECAPTCHA_SECRET_KEY'),
                    '',
                    null,
                    null,
                    $config
                );
            }
        ]);
    }

    public function registerConsoleContainer($app){
        Yii::$container->setSingleton('brussens\maintenance\StateInterface' , [
                'class' => 'brussens\maintenance\states\FileState',
            ]
        );
        Yii::$container->setDefinitions([
            \himiklab\yii2\recaptcha\ReCaptcha2::className() => function ($container, $params, $config) {
                return new \himiklab\yii2\recaptcha\ReCaptcha2(
                    getenv('RECAPTCHA_SITE_KEY'),
                    '',
                    $config
                );
            },
            \himiklab\yii2\recaptcha\ReCaptchaValidator2::className() => function ($container, $params, $config) {
                return new \himiklab\yii2\recaptcha\ReCaptchaValidator2(
                    getenv('RECAPTCHA_SECRET_KEY'),
                    '',
                    null,
                    null,
                    $config
                );
            }
        ]);
    }
}

require_once (dirname(__FILE__) . '/helpers/TwFunctions.php');
