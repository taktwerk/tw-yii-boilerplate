$(document).ready(function () {
    if($('#toolbar_toggle')){
        $('#toolbar_toggle').click(function(){
            $('.toolbar-container').fadeToggle();
            $("i", this).toggleClass("glyphicon-menu-down glyphicon-menu-up");
        });
    }

    $("form").each(function() {
        $(this).on('submit', function(e) {

            // Remove the old one before adding a new one
            $(window).off('beforeunload');
            $(window).on('beforeunload', function() {
                $('#loadingModal').modal();
            });
            return true;
        });
    });
});