function recompressMultiple(elm){
    var element = $(elm),
        keys=$("#" + gridViewKey + "-grid").yiiGridView('getSelectedRows'),
        url = element.attr('data-url');
    var msg  = "Do you really want to recompress {count} entries?";
    if(element.data('message')){
    	msg = element.data('message');
    }
    msg = msg.replace('{count}', keys.length);

    if(keys.length>0 && confirm(msg)){
	    $.post(url,{pk : keys},
	        function (){
	            if (useModal != true) {
	            	$(document).one("pjax:success", "#" + gridViewKey + "-pjax-container",  function(event){
	            		for(var i=0;i<keys.length;i++){
	            			$("#" + gridViewKey + "-grid .kv-row-select input[type=checkbox][value="+keys[i]+"]").prop('checked',true);
	            		}
	                  });
	                $.pjax.reload({container: "#" + gridViewKey + "-pjax-container"});
	            }
	        }
	    );
    }
}
