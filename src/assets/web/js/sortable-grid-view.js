(function ($) {
    $.SortableGridView = function (options) {
        var defaultOptions = {
        	handler:'.sortable-row',
            id: 'sortable-grid-view',
            action: 'sort-item',
            sortingPromptText: 'Loading...',
            sortingFailText: 'Fail to sort',
            csrfTokenName: '',
            csrfToken: '',
            reloadOnSort:false,
            pjaxContainerId:'',
        };

        $.extend({}, defaultOptions, options);
        $('body').append('<div class="modal fade" id="' + options.id + '-sorting-modal" tabindex="-1" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + options.sortingPromptText + '</div></div></div></div>');

        var regex = /items\[\]\_(\d+)/;

        $('#' + options.id + ' .sortable-grid-view tbody').sortable({
        	handle:options.handler,
            update : function () {
                $('#' + options.id + '-sorting-modal').modal('show');
                //serial = $('#' + options.id + ' .sortable-grid-view tbody').sortable('toArray', {attribute:'id'});
                serial = [];

                $('#' + options.id + ' .sortable-grid-view tbody tr[data-key]').each( function() {
                    //console.log($(this).data('key'));
                    serial.push($(this).data('key'));
                });

                
                var length = serial.length;
                var currentRecordNo = 0;
                var successRecordNo = 0;
                var data = [];

                if(length > 0){
                    for(var i=0; i<length; i++){
                        //var itemID = regex.exec(serial[i]);
                        var itemID = serial[i];
                        //data = data + 'items[' + i + ']=' + itemID[1] + '&';
                        data.push(itemID)
                        currentRecordNo++;

                        if(currentRecordNo == 500 || i == (length-1)){
                            //data =  data + options.csrfTokenName + '=' + options.csrfToken;

                            (function(currentRecordNo){
                                $.ajax({
                                    'url': options.action,
                                    'type': 'post',
                                    'data': { 'items': data, [options.csrfTokenName] : options.csrfToken },
                                    success: function(data){
                                        checkSuccess(currentRecordNo);
                                    },
                                    error: function(data){
                                        $('#' + options.id + '-sorting-modal').modal('hide');
                                        alert(options.sortingFailText);
                                    }
                                });
                            })(currentRecordNo);

                            currentRecordNo = 0;
                            data = [];
                        }
                    }
                }

                function checkSuccess(count){
                    successRecordNo += count;

                    if(successRecordNo >= length){
                        $('#' + options.id + '-sorting-modal').modal('hide');
                        if(options.reloadOnSort==true){
                        	if(options.pjaxContainerId){
                        		$.pjax.reload({container:'#'+options.pjaxContainerId});
                        	}
                        }
                    }
                }
            }
        });
    };
})(jQuery);