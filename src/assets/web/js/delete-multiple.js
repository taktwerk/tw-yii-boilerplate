function deleteMultiple(elm){
    var element = $(elm),
        keys=$("#" + gridViewKey + "-grid").yiiGridView('getSelectedRows'),
        url = element.attr('data-url');
    var selectAll = $("#" + gridViewKey + "-grid").find('[name="action_on_all_rec"]').prop('checked');
    var msg  = "Do you really want to delete {count} entries?";
    if(element.data('message')){
    	msg = element.data('message');
    }
    msg = msg.replace('{count}', keys.length);
    if(selectAll){
    	msg = "Delete entries of all pages?"
    	if(element.data('all-message')){
    		msg = element.data('all-message');
    	}
    }
    if((keys.length>0 || selectAll) && confirm(msg)){
	    $.post(url,{pk : keys,action_on_all_rec:(selectAll===true)?1:0},
	        function (){
	            if (useModal != true) {
	            	$(document).one("pjax:success", "#" + gridViewKey + "-pjax-container",  function(event){
	            		for(var i=0;i<keys.length;i++){
	            			$("#" + gridViewKey + "-grid .kv-row-select input[type=checkbox][value="+keys[i]+"]").prop('checked',true);
	            		}
	                  });
	                $.pjax.reload({container: "#" + gridViewKey + "-pjax-container"});
	            }
	        }
	    );
    }
}

