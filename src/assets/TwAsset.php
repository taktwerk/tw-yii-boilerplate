<?php
/**
 * @link http://www.yiiframework.com/
 *
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace taktwerk\yiiboilerplate\assets;

use dmstr\web\AdminLteAsset;
use yii\helpers\FileHelper;
use yii\web\AssetBundle;
use Yii;

/**
 * Configuration for `backend` client script files.
 *
 * @since 4.0
 */
class TwAsset extends AssetBundle
{
    public $sourcePath = '@vendor/taktwerk/yii-boilerplate/src/assets/web';

    public $css = [
        // Note: less files require a compiler (available by default on Phundament Docker images)
        // use .css alternatively
        #'less/app.less',
        'less/taktwerk.less',
        'css/style.css',
    ];

    public $js = [
        'js/jquery.ui.touch-punch.min.js',
        'js/delete-multiple.js',
        'js/recompress-multiple.js',
        'js/duplicate-multiple.js',
        'js/edit-multiple.js',
        'js/sortable-grid-view.js',
        'js/loading.js',
    ];

    // we recompile the less files from 'yii\bootstrap\BootstrapAsset' and include the css in app.css
    // therefore we set bundle to false in application config
    public $depends = [
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset',
        'dmstr\web\AdminLteAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
        'taktwerk\yiiboilerplate\widget\form\FormAsset'
    ];

    public function init()
    {
        parent::init();

        // /!\ CSS/LESS development only setting /!\
        // Touch the asset folder with the highest mtime of all contained files
        // This will create a new folder in web/assets for every change and request
        // made to the app assets.
        if (getenv('APP_ASSET_FORCE_PUBLISH')) {
            $path = \Yii::getAlias($this->sourcePath);
            $files = FileHelper::findFiles($path);
            $mtimes = [];
            foreach ($files as $file) {
                $mtimes[] = filemtime($file);
            }
            touch($path, max($mtimes));
        }

        // When using asset bundles, disable the "skin" of adminlte, as recommended by the documentation
        if (getenv('APP_ASSET_USE_BUNDLED')) {
            Yii::$container->set(
                AdminLteAsset::className(),
                [
                    'skin' => false,
                ]
            );
        }
    }
}
