<?php

namespace taktwerk\yiiboilerplate\assets;

use yii\web\AssetBundle;

/**
 * Class AjaxNewAsset
 *
 * @package inspire\widget
 */
class AjaxRelatedAsset extends AssetBundle
{
    public $js = [
        'jquery.ajaxrelated.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init()
    {
        $this->sourcePath = '@vendor/taktwerk/yii-boilerplate/src/assets/web/js';
        parent::init();
    }
}
