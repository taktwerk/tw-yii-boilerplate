<?php
namespace taktwerk\yiiboilerplate\assets;

use yii\web\AssetManager as AM;

class AssetManager extends AM
{
    /**
     * @desc Whether to update assets base directory modify time if any file inside the directory has greater modify time than the asset directory.
     * Notice: Setting this "true" will affect performance and load time.  
     * */
    public $updateBaseDirOnNewModifyTime = false;
    
    protected function publishDirectory($src, $options)
    {
        if($this->updateBaseDirOnNewModifyTime){
            $subdirs = glob($src . DIRECTORY_SEPARATOR . '*', GLOB_NOSORT | GLOB_ONLYDIR);
            foreach ($subdirs as $dir) {
                $temp = $mostRecentFileMTime = filemtime($src);
                $iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($src), \RecursiveIteratorIterator::CHILD_FIRST);
                foreach ($iterator as $fileinfo) {
                    /*If any file has modified time greater than src directory time than src diretory modified time will be updated to current time*/
                    if ($fileinfo->isFile() && $fileinfo->getMTime() > $mostRecentFileMTime) {
                        $temp = $fileinfo->getMTime();
                        @touch($src);
                        @touch($dir);
                        break;
                    }
                }
                if ($temp != $mostRecentFileMTime) {
                  break;
                }
            }
        }
        return parent::publishDirectory($src, $options);
    }
}
