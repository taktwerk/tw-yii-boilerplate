<?php
/**
 * Created by PhpStorm.
 * User: ben-g
 * Date: 18.01.2016
 * Time: 14:55
 */

namespace taktwerk\yiiboilerplate;

use Yii;
use yii\console\Exception;
use yii\db\ColumnSchema;
use yii\db\Migration;
use yii\db\Schema;
use yii\base\InvalidConfigException;
use yii\db\ColumnSchemaBuilder;

class TwMigration extends Migration
{

    public function customColumnDefinition($definition){
        return $this->getDb()->getSchema()->createColumnSchemaBuilder($definition);
    }
    /**
     * taktwerk's MySQL Conventions for Table-Names
     * - Checks conventions and throws Exception if the table-name does not follow them
     * @param string $columnName
     * @param string $method
     * @throws Exception
     */
    public function twMysqlConventionsColumn($columnName, $method)
    {
        if (preg_match('/^[a-z_]*$/', $columnName) != 1) {
            throw new Exception("The column name '$columnName' in your $method-Migration does not follow the tw mysql conventions. Please make sure to create columns with only lowercase letters a-z or '_'.");
        }
    }

    /**
     * taktwerk's MySQL Conventions for Table-Names
     * - Checks conventions and throws Exception if the table-name does not follow them
     * @param array $columns
     * @param string $method
     * @throws Exception
     */
    public function twMysqlConventionsColumns($columns, $method)
    {
        foreach ($columns as $column => $type) {
            if($type instanceof ColumnSchemaBuilder)
            $this->twMysqlConventionsColumn($column, $method);
        }
    }

    /**
     * taktwerk's MySQL Conventions for Table-Names
     * - Checks conventions and throws Exception if the table-name does not follow them
     * @param string $tableName
     * @param string $method
     * @throws Exception
     */
    public function twMysqlConventions($tableName, $method)
    {
        if (preg_match('/\{\{\%[a-z_]*\}\}/', $tableName) != 1) {
            throw new Exception("The table name '$tableName' in your $method-Migration does not follow the tw mysql conventions. Please make sure to create tables within {{% }} with only lowercase letters a-z or '_'.");
        }
    }
    public static function getTableDefaultColumns(){
        return [
            "id",
            "created_by",
            "created_at",
            "updated_by",
            "updated_at",
            "deleted_by",
            "deleted_at",
        ];
    }
    /**
     * @param string $table
     * @param array $columns
     * @param null $options
     * @param boolean $enableBehaviors if behaviors-fields (created_at,...) should be added or not
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function createTable($table, $columns, $options = null, $enableBehaviors = true)
    {
        $this->twMysqlConventions($table, 'createTable');
        $this->twMysqlConventionsColumns($columns, 'createTable');
        echo "    > create table $table " . (($enableBehaviors) ? "with behaviors" : "without behaviors") . " ...";
        $time = microtime(true);
        $this->db->createCommand()->createTable($table, $columns, $options)->execute();

        // Add Behaviors if enabled
        if ($enableBehaviors) {
            $behaviorColummns = [
                [
                    "created_by",
                    Schema::TYPE_INTEGER
                ],
                [
                    "created_at",
                    Schema::TYPE_DATETIME
                ],
                [
                    "updated_by",
                    Schema::TYPE_INTEGER
                ],
                [
                    "updated_at",
                    Schema::TYPE_DATETIME
                ],
                [
                    "deleted_by",
                    Schema::TYPE_INTEGER
                ],
                [   
                    "deleted_at",
                    Schema::TYPE_DATETIME
                ]
            ];
            $tableError = false;
            foreach ($behaviorColummns as $val){
                $checkCol = $this->checkColumnExist($table, $val[0],$val[1]);
                if($checkCol['column']===true){
                    if($checkCol['datatype']==false){
                        echo 'The column "' . $val[0] . '" of table "' . $table . '" is reserved for system and must have datatype of "' . $val[1] . '".
To fix this issue you can: 
Remove this column from the sql/migration and system will generate the column by itself
OR
Set the mentioned datatype of the column in the sql/migration
';
                        $tableError = true;
                    }
                }else{
                    $this->db->createCommand()->addColumn($table, $val[0], $val[1])->execute();
                }
            }
            if($tableError==true){
                throw new InvalidConfigException('Please fix the above errors to proceed');
            }
        }
        if($this->checkColumnExist($table, 'deleted_at'))
        {
            $this->createIndex('deleted_at_idx', $table, "deleted_at");
        }

        echo " done (time: " . sprintf('%.3f', microtime(true) - $time) . "s)\n";
    }
    public function checkColumnExist($tableName,$columnName,$columnDataType=null){
        $table = $this->db->schema->getTableSchema($tableName);
        if (!isset($table->columns[$columnName])) {
            return ($columnDataType===null)?false:['column'=>false,'datatype'=>false];
        }
        $check = ['column'=>true];
        $check['datatype']= strtolower($table->columns[$columnName]->type)==strtolower($columnDataType);
        return $check;
    }
    /**
     * @inheritdoc
     */
    public function renameTable($table, $newName)
    {
        $this->twMysqlConventions($table, 'renameTable');
        $this->twMysqlConventions($newName, 'renameTable');
        parent::renameTable($table, $newName);
    }

    /**
     * @inheritdoc
     */
    public function renameColumn($table, $name, $newName)
    {
        $this->twMysqlConventions($table, 'renameColumn');
        parent::renameColumn($table, $name, $newName);
    }

    /**
     * @inheritdoc
     */
    public function delete($table, $condition = '', $params = [])
    {
        $this->twMysqlConventions($table, 'delete');
        parent::delete($table, $condition, $params);
    }

    /**
     * @inheritdoc
     */
    public function update($table, $columns, $condition = '', $params = [])
    {
        $this->twMysqlConventions($table, 'update');
        $this->twMysqlConventionsColumns($columns, 'update');
        parent::update($table, $columns, $condition, $params);
    }

    /**
     * @inheritdoc
     */
    public function insert($table, $columns)
    {
        $this->twMysqlConventions($table, 'insert');
        parent::insert($table, $columns);
    }

    /**
     * @inheritdoc
     */
    public function alterColumn($table, $column, $type)
    {
        $this->twMysqlConventions($table, 'alterColumn');
        parent::alterColumn($table, $column, $type);
    }

    /**
     * @inheritdoc
     */
    public function addColumn($table, $column, $type)
    {
        $this->twMysqlConventions($table, 'addColumn');
        $this->twMysqlConventionsColumn($column, 'addColumn');
        parent::addColumn($table, $column, $type);
    }

    /**
     * @inheritdoc
     */
    public function dropColumn($table, $column)
    {
        $this->twMysqlConventions($table, 'dropColumn');
        parent::dropColumn($table, $column);
    }

    /**
     * @inheritdoc
     */
    public function addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete = null, $update = null)
    {
        $this->twMysqlConventions($refTable, 'addForeignKey');
        parent::addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete, $update);
    }

    /**
     * @inheritdoc
     */
    public function dropForeignKey($name, $table)
    {
        $this->twMysqlConventions($table, 'dropForeignKey');
        parent::dropForeignKey($name, $table);
    }

    /**
     * @inheritdoc
     */
    public function createIndex($name, $table, $columns, $unique = false)
    {
        $this->twMysqlConventions($table, 'createIndex');
        parent::createIndex($name, $table, $columns, $unique);
    }

    /**
     * @inheritdoc
     */
    public function dropIndex($name, $table)
    {
        $this->twMysqlConventions($table, 'dropIndex');
        parent::dropIndex($name, $table);
    }

    /**
     * @inheritdoc
     */
    public function addPrimaryKey($name, $table, $columns)
    {
        $this->twMysqlConventions($table, 'addPrimaryKey');
        parent::addPrimaryKey($name, $table, $columns);
    }

    /**
     * @inheritdoc
     */
    public function dropPrimaryKey($name, $table)
    {
        $this->twMysqlConventions($table, 'dropPrimaryKey');
        parent::dropPrimaryKey($name, $table);
    }

    /**
     * @inheritdoc
     */
    public function dropTable($table)
    {
        $this->twMysqlConventions($table, 'dropTable');
        parent::dropTable($table);
    }

    /**
     * Get the AuthManager
     * @return \yii\rbac\ManagerInterface
     * @throws \yii\base\Exception
     */
    public function getAuth()
    {
        $auth = Yii::$app->authManager;
        if ($auth instanceof \yii\rbac\DbManager) {
            return $auth;
        }

        throw new \yii\base\Exception('Application authManager must be an instance of \yii\rbac\DbManager');
    }

    /**
     * Create a role
     * @param string $name
     * @param null $childOf
     */
    public function createRole($name, $childOf = null, $description = '')
    {
        $auth = $this->getAuth();

        $role = $auth->getRole($name);
        if (empty($role)) {
            $role  = $auth->createRole($name);
            $role->description = $description;
            $auth->add($role);
            $role = $auth->getRole($name);
        }

        if (!empty($childOf)) {
            if (is_string($childOf)) {
                $childOf = $auth->getRole($childOf);
            }
            $auth->addChild($role, $childOf);
        }
        return true;
    }

    /**
     * Remove an existing role
     * @param string $name
     */
    public function removeRole($name)
    {
        $auth = $this->getAuth();
        $role = $auth->getRole($name);
        if (!empty($role)) {
            $auth->remove($role);
        }
        return true;
    }

    /**
     * Create a permission
     * @param $name
     * @param string $description
     * @param array $roles
     * @param array $permissions
     * @return bool
     * @throws \yii\base\Exception
     */
    public function createPermission($name, $description = '', $roles = [], $permissions = [])
    {
        $auth = $this->getAuth();
        $perm = $this->getPermission($name);
        if (empty($perm)) {
            $perm = $auth->createPermission($name);
            $perm->description = $description;
            $auth->add($perm);
            $perm = $auth->getPermission($name);
        }
        
        if (!empty($roles)) {
            if (!is_array($roles)) {
                $roles = [$roles];
            }
            foreach ($roles as $role) {
                $role = $auth->getRole($role);
                if ($role && !$auth->hasChild($role, $perm)) {
                    $auth->addChild($role, $perm);
                }
            }
        }
        
        if (!empty($permissions)) {
            if (!is_array($permissions)) {
                $permissions = [$permissions];
            }
            foreach ($permissions as $permission) {
                $permission = $auth->getPermission($permission);
                if ($permission) {
                    if(!$auth->hasChild($permission, $perm)){
                        $auth->addChild($permission, $perm);
                    }
                }
            }
        }
        
        return true;
    }

    /**
     * Create all the standard crud permissions
     * @param $name
     * @param string $description
     * @param array $roles
     */
    public function createCrudPermissions($name, $description = '', $roles = [])
    {
        $sub = [
            'index', 'view', 'create', 'update', 'delete', 'delete-multiple', 
            'update-multiple', 'entry-details', 'related-form',
            'list', 'depend', 'system-entry',
        ];
        $permList = [];
        foreach ($sub as $perm) {
            $permName = trim($name, '_') . '_' . $perm;
            $permList[] = $permName;
            $this->createPermission($permName, $description . ' ' . ucfirst($perm), $roles);
        }
        
        
       /*  $ownPermName = trim($name, '_') . '_own';
        if($this->createPermission($ownPermName)){
            $auth = $this->getAuth();
            $ownPerm = $this->getPermission($ownPermName);
            foreach($permList as $perm){
                $childPerm = $this->getPermission($perm);
                $auth->addChild($ownPerm, $childPerm);
            }
            $groupPermName = trim($name, '_') . '_group';
            $this->createPermission($groupPermName);
            $groupPerm = $this->getPermission($groupPermName);
            $auth->addChild($groupPerm, $ownPerm);
        } */
        
    }
    /**
     * Create _own and _group permissions
     * @param $name
     * @param string $description
     * @param array $roles
     */
    public function createOwnGroupPermission($name,$description = '', $roles = []){
        $ownPermName = trim($name, '_') . '_own';
        $this->createPermission($ownPermName, $description . ' ' . 'Own', $roles);
        
        $groupPermName = trim($name, '_') . '_group';
        $this->createPermission($groupPermName, $description . ' ' . 'Group', $roles);
    }
    /**
     * Create all the standard crud permissions by controller name
     * @param $name
     * @param string $description
     * @param array $roles
     */
    public function createCrudControllerPermissions($name)
    {
        $description = $name;
        $crud = 'x_' . $name;

        $this->createPermission($crud . '_admin' ,'x.' . $description . ' Admin');
        $this->createPermission($crud . '_edit' ,'x.' . $description . ' Edit', [], [$crud . '_admin']);
        $this->createPermission($crud . '_see' , 'x.' . $description . ' See', [], [$crud . '_edit']);

        $sub = ['index', 'view',  'entry-details', 'list', 'depend', 'system-entry'];
        foreach ($sub as $perm) {
            $this->createPermission(trim($name, '_') . '_' . $perm, $description . ' ' . ucfirst($perm), [], [$crud . '_see']);
        }

        $sub = ['create', 'update', 'delete', 'delete-multiple', 'update-multiple', 'related-form'];
        foreach ($sub as $perm) {
            $this->createPermission(trim($name, '_') . '_' . $perm, $description . ' ' . ucfirst($perm), [], [$crud . '_edit']);
        }
        
        $ownPermName = trim($name, '_') . '_own';
        $this->createPermission($ownPermName, $description . ' ' . 'Own');
        
        $groupPermName = trim($name, '_') . '_group';
        $this->createPermission($groupPermName, $description . ' ' . 'Group');
    }

    /**
     * Add controller to see permissions
     * @param $controller
     * @param $roles
     * @throws \yii\base\Exception
     */
    public function addSeeControllerPermission($controller, $roles)
    {
            $this->createPermission('x_' . $controller . '_see', 'x.' . $controller . ' See', $roles);
    }

    /**
     * Add controller to edit permissions
     * @param $controller
     * @param $roles
     * @throws \yii\base\Exception
     */
    public function addEditControllerPermission($controller, $roles)
    {
            $this->createPermission('x_' . $controller . '_edit', 'x.' . $controller . ' Edit', $roles);
    }

    /**
     * Add controller to admin permissions
     * @param $controller
     * @param $roles
     * @throws \yii\base\Exception
     */
    public function addAdminControllerPermission($controller, $roles)
    {
            $this->createPermission('x_' . $controller . '_admin', 'x.' . $controller . ' Admin', $roles);
    }

    /**
     * Remove a permission
     * @param string$name
     * @param array $roles
     * @return bool
     */
    public function removePermission($name, $roles = [])
    {
        $auth = $this->getAuth();
        $perm = $this->getPermission($name);
        if (empty($perm)) {
            return false;
        }

        if (!empty($roles)) {
            if (!is_array($roles)) {
                $roles = [$roles];
            }
            foreach ($roles as $role) {
                $role = $auth->getRole($role);
                if ($role) {
                    $auth->removeChild($role, $perm);
                }
            }
        }

        $auth->remove($perm);
        return true;
    }

    /**
     * @param $permission
     * @return \yii\rbac\Permission|null
     * @throws \yii\base\Exception
     */
    public function getPermission($permission)
    {
        return $this->getAuth()->getPermission($permission);
    }
}
