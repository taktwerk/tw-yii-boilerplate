<?php
/**
 * Moved to /actions namespace
 */

namespace taktwerk\yiiboilerplate\action;

/**
 * The ajax new action.
 *
 * Handles form submitting.
 *
 * @author Mehdi Achour <mehdi.achour@agence-inspire.com>
 */
class AjaxRelatedAction extends \taktwerk\yiiboilerplate\actions\AjaxRelatedAction
{
}
