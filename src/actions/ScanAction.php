<?php

namespace taktwerk\yiiboilerplate\actions;

use lajax\translatemanager\models\LanguageSource;
use lajax\translatemanager\models\LanguageTranslate;
use taktwerk\yiiboilerplate\services\DynamicTranslation;
use \lajax\translatemanager\controllers\actions\ScanAction as BaseAction;
use yii\db\Exception;
use yii\helpers\FileHelper;

class ScanAction extends BaseAction
{
    /**
     * Source directory path of translation files
     * @var string
     */
    public $translation_file_path = '@taktwerk-boilerplate/resources/i18n';

    /**
     * Detecting new language elements.
     *
     * @return string
     * @throws Exception
     */
    public function run()
    {
        $this->getAllTranslationFilesAndProcess();

        $translator = new DynamicTranslation();
        $count = $translator->run();

        return parent::run();
    }

    /**
     * Get all files of translations and scan for translation key and messages
     * @throws Exception
     */
    public function getAllTranslationFilesAndProcess(){

        $all_translation_sources = FileHelper::findFiles(
            \Yii::getAlias($this->translation_file_path),
            [
                'only' => ['*.php']
            ]
        );

        foreach ($all_translation_sources as $translation_source){

            $file_path = FileHelper::normalizePath($translation_source);
            $category_name = explode(".php",basename($file_path))[0];
            $language = basename(dirname($file_path));

            $messages = $this->getFileTranslations($file_path);
            $this->addFileTranslationsToDb($category_name,$language, $messages);
        }
    }

    /**
     * Add/Update Translation from files for all languages adn categories
     * @param $category
     * @param $language
     * @param $messages
     * @throws Exception
     */
    public function addFileTranslationsToDb($category, $language, $messages){

        if(is_array($messages)){
            foreach ($messages as $key => $message){

                $message = trim($message);

                /**@var  $exists LanguageSource */
                $exists = LanguageSource::find()
                    ->where(
                        [
                            'category'=>$category,
                            'message'=>$key
                        ])
                    ->one();

                if($message && $exists){

                    /**@var  $language_translation_exists LanguageTranslate */
                    $language_translation_exists = LanguageTranslate::find()
                        ->where(
                            [
                                'id'=>$exists->id,
                                'language'=>$language
                            ]
                        )
                        ->one();

                    /**@var  $translation_empty LanguageTranslate */
                    $translation_empty = LanguageTranslate::find()
                        ->where([
                            'OR',
                            [
                                'AND',
                                ['id'=>$exists->id,'language'=>$language],
                                [
                                    'is',
                                    'translation',
                                    new \yii\db\Expression('null')
                                ]
                            ],
                            [
                                'AND',
                                ['id'=>$exists->id,'language'=>$language],
                                ['=', "LENGTH(translation)", 0]
                            ]
                        ])
                        ->one();

                    if(!$language_translation_exists){
                        LanguageTranslate::getDb()
                            ->createCommand()
                            ->insert(
                                LanguageTranslate::tableName(),
                                [
                                    'id'=>$exists->id,
                                    'language'=>$language,
                                    'translation'=>$message
                                ]
                            )
                            ->execute();
                    }elseif ($translation_empty){
                        $translation_empty->translation = $message;
                        $translation_empty->save(false);
                    }
                }
            }
        }
    }

    /**
     * Get all translation messages
     * @param $file_path
     * @return array|mixed
     */
    public function getFileTranslations($file_path)
    {
        $messages = [];
        if (is_file($file_path)) {
            $messages = include $file_path;
            if (!is_array($messages)) {
                $messages = [];
            }
        }
        return $messages;
    }

}
