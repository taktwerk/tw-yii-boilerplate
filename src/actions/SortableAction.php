<?php

namespace taktwerk\yiiboilerplate\actions;

use yii\base\Action;
use yii\web\HttpException;
use yii\base\InvalidConfigException;
use Yii;
use yii\db\Expression;
/**
 * Class SortableAction
 * @package taktwerk\yiiboilerplate\actions
 */
class SortableAction extends Action
{
    /**
     * (required) The ActiveRecord Class name
     *
     * @var string
     */
    public $activeRecordClassName;

    /**
     * (required) The attribute name where your store the sort order.
     *
     * @var string
     */
    public $orderColumn;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (!isset($this->activeRecordClassName)) {
            throw new InvalidConfigException("You must specify the activeRecordClassName");
        }

        if (!isset($this->orderColumn)) {
            throw new InvalidConfigException("You must specify the orderColumn");
        }
    }

    /**
     *
     * @throws HttpException
     */
    public function run()
    {
        if (! Yii::$app->request->isAjax) {
            throw new HttpException(404);
        }
        $items = \Yii::$app->request->post('items');
        if (isset($items) && is_array($items)) {
            $activeRecordClassName = $this->activeRecordClassName;

            $pk = $activeRecordClassName::primaryKey()[0];
            $oc = $this->orderColumn;
            
            $records = $activeRecordClassName::find()/* ->select([
                $pk,
                $oc
            ]) */->where([
                $pk => $items
            ])->orderBy($oc . ' ASC')
                ->all();
            $newOrder = [];
            foreach ($items as $i => $item) {
                $newOrder[$i] = $records[$i]->$oc;
            }
            $previousOrder = null;
            foreach ($newOrder as $i => $order) {
                $id = $items[$i];
                if ($previousOrder == $order) {
                    $order ++;
                } else if ($previousOrder > $order) {
                    $order = $previousOrder + 1;
                }
                $activeRecordClassName::updateAll([$oc => $order], [$pk=>$id]);
            $previousOrder = $order;
        }
        
        $rec = array_shift(array_slice($records,0,1));
        if($rec){
            $behavior = $rec->getBehavior('sort');
            if($behavior && $behavior->strictOrdering)
            {
                $rec->gaplessOrder();
            }
        }
        }
    }
}