<?php

namespace taktwerk\yiiboilerplate\filters;

/**
 * Class URIChecker
 * @package brussens\maintenance\filters
 */
class URIFilter extends \brussens\maintenance\filters\URIFilter
{

    public function isAllowed()
    {
        if (is_array($this->uri) && !empty($this->uri)) {
           $currentUri = str_replace(\Yii::$app->language.'/','',$this->request->getPathInfo());
           
           return (bool) (in_array($currentUri, $this->uri) || in_array('/' . $currentUri, $this->uri));
        }
        return false;
    }
}
