<?php
// Raven is Sentry's library to flag JS errors. Should only be enabled if sentry is set and external
// calls aren't disabled.
if (getenv('APP_SENTRY_DSN') != '') {
    $this->registerJs("var sentryDsn = '" . getenv('APP_SENTRY_DSN') . "' ;\n", \yii\web\View::POS_HEAD);

    $js = <<<EOT
Sentry.init({ dsn: sentryDsn });
EOT;
    $user = Yii::$app->has('user', true) ? Yii::$app->get('user', false) : null;
    if ($user && ($identity = $user->getIdentity(false))) {
        $js .= " Sentry.setUser({ id: '{$identity->getId()}' });";
    }
    $this->registerJs($js, \yii\web\View::POS_HEAD);
    $this->registerJsFile('https://browser.sentry-cdn.com/5.12.1/bundle.min.js',[
        'integrity'=>"sha384-y+an4eARFKvjzOivf/Z7JtMJhaN6b+lLQ5oFbBbUwZNNVir39cYtkjW1r6Xjbxg3",
        'crossorigin'=>"anonymous",
        'position'=>\yii\web\View::POS_HEAD
    ]);   
}