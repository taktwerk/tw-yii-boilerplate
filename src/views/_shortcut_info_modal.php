<?php
/**
 * Created by PhpStorm.
 * User: Shraddha
 */
?>
<div class="row">
    <div class="col-md-12">
        <p><?= Yii::t('twbp', 'Possible shortcuts'); ?>:</p>
        <ul class="list-group">
            <li class="list-group-item"><b>CTRL/CMD + S</b> <?= Yii::t('twbp', 'Save Form'); ?></b></li>
            <li class="list-group-item"><b>CTRL/CMD + SHIFT + S</b> <?= Yii::t('twbp', 'Save and Close Form'); ?></b></li>
            <li class="list-group-item"><b>CTRL/CMD + Y</b> <?= Yii::t('twbp', 'Create New Entry'); ?></b></li>
            <li class="list-group-item"><b>CTRL/CMD + SHIFT + Y</b> <?= Yii::t('twbp', 'Save and Create New Entry'); ?></b></li>
            <li class="list-group-item"><b>ESC</b> <?= Yii::t('twbp', 'Cancel'); ?></b></li>
        </ul>
    </div>
</div>