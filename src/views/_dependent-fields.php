<?php
use yii\helpers\Json;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;

/*code to show/hide a field based on the value of another field*/
if(!isset($formDependentShowFieldsOverwrite) || $formDependentShowFieldsOverwrite==true){
$dependentFields = Yii::$app->controller->formDependentShowFieldsOverwrite($model);
}else{
    $dependentFields = $model->formDependentShowFields();
}
if(!isset($formId)){
    $formId = StringHelper::basename(get_class($model));
}
if (count($dependentFields) > 0) {
    $js = '';

    foreach ($dependentFields as $dependentField){
        $rule = Json::encode([$dependentField['jsData']]);
        $js .= <<<EOT
mfConditionalFields('#{$formId}',{$rule});
EOT;
    }
    //Trigger not working on select2 changes - fix it on monday, then work on the when client and when condition
    $this->registerJs($js);
}