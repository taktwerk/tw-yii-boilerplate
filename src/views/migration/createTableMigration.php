<?php
/**
 * This view is used by console/controllers/MigrateController.php.
 *
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name without namespace */
/* @var $namespace string the new migration class namespace */
/* @var $table string the name table */
/* @var $tableComment string the comment table */
/* @var $fields array the fields */
/* @var $foreignKeys array the foreign keys */

echo "<?php\n";
if (!empty($namespace)) {
    echo "\nnamespace {$namespace};\n";
}
?>
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `<?= $table ?>`.
<?= $this->render('@yii/views/_foreignTables', [
    'foreignKeys' => $foreignKeys,
]) ?>
 */
class <?= $className ?> extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
<?= $this->render('@yii/views/_createTable', [
    'table' => $table,
    'fields' => $fields,
    'foreignKeys' => $foreignKeys,
])
?>
<?php
$deleteIndexList = [];
//Adding deleted_at column index with foreign keys - START
if($foreignKeys && count($foreignKeys)>0){
    foreach($foreignKeys as $k=>$fk){
        $indexName = 'idx_'.$table.'_deleted_at_'.$k;
        $indexName = str_replace(['{{%','}}'],['',''] , $indexName);
        if(strlen($indexName)>63){
            //limiting the max-length of the index name to around 60-64 characters
            $indexName = substr($indexName, 0, 55).'_'.substr(str_shuffle(str_repeat($x='abcdefghijklmnopqrstuvwxyz', ceil(5/strlen($x)) )),1,5);
        }
        $deleteIndexList[]=$indexName;
        $ind = <<<EOT
        \$this->createIndex('$indexName', '$table', ['deleted_at','{$k}']);

EOT;
        echo $ind;
    }
}
//Adding deleted_at column index with foreign keys - END
?>
<?php if (!empty($tableComment)) {
    echo $this->render('@yii/views/_addComments', [
        'table' => $table,
        'tableComment' => $tableComment,
    ]);
}
?>
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    <?php
    foreach($deleteIndexList as $i){
    ?>$this->dropIndex('<?= $i?>', '<?= $table?>');
    <?php }?>
<?= $this->render('@yii/views/_dropTable', [
    'table' => $table,
    'foreignKeys' => $foreignKeys,
])
?>
    }
}
