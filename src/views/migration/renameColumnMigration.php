<?php
/**
 * This view is used by console/controllers/MigrateController.php.
 *
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name without namespace */
/* @var $namespace string the new migration class namespace */
/* @var $table string the name table */
/* @var $fields array the fields */

echo "<?php\n";
if (!empty($namespace)) {
    echo "\nnamespace {$namespace};\n";
}
?>

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the dropping of table `<?= $table ?>`.
<?= $this->render('@yii/views/_foreignTables', [
    'foreignKeys' => $foreignKeys,
]) ?>
 */
class <?= $className ?> extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
<?= $this->render('@vendor/taktwerk/yii-boilerplate/src/views/migration/_renameColumn', [
    'table' => $table,
    'newName' => $newName,
    'oldName' => $oldName,
])
?>
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
 
<?= $this->render('@vendor/taktwerk/yii-boilerplate/src/views/migration/_renameColumn', [
    'table' => $table,
    'newName' => $oldName,
    'oldName' => $newName,
])?>
    }
}