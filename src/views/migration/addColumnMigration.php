<?php
/**
 * This view is used by console/controllers/MigrateController.php.
 *
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name without namespace */
/* @var $namespace string the new migration class namespace */
/* @var $table string the name table */
/* @var $fields array the fields */

echo "<?php\n";
if (!empty($namespace)) {
    echo "\nnamespace {$namespace};\n";
}
?>

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `<?= $table ?>`.
<?= $this->render('@yii/views/_foreignTables', [
     'foreignKeys' => $foreignKeys,
 ]) ?>
 */
class <?= $className ?> extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
<?= $this->render('@yii/views/_addColumns', [
    'table' => $table,
    'fields' => $fields,
    'foreignKeys' => $foreignKeys,
]);
    
?>

<?php $idxList = [];foreach($indexes as $idx){ ?>
		<?php

		    $indexName = 'idx_'.$table.'_'.implode('_',$idx['columns']);
		    $indexName = str_replace(['{{%','}}'],['',''] , $indexName);
		    if(strlen($indexName)>63){
		        //limiting the max-length of the index name to around 60-64 characters
		        $indexName = substr($indexName, 0, 55).'_'.substr(str_shuffle(str_repeat($x='abcdefghijklmnopqrstuvwxyz', ceil(5/strlen($x)) )),1,5);
		    }
		    $t = "[";
		    foreach($idx['columns'] as $c){
		        $t .= "'$c',";
		    }
		    $t .= "]";
		    $idxList[] = $indexName;
		    $ind = <<<EOT
    \$this->createIndex('$indexName', '$table', $t);
        
EOT;
		    echo $ind;
		?>
<?php }?>
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    <?php
    foreach($idxList as $i){
    ?>$this->dropIndex('<?= $i?>', '<?= $table?>');
    <?php }?>
<?= $this->render('@yii/views/_dropColumns', [
    'table' => $table,
    'fields' => $fields,
    'foreignKeys' => $foreignKeys,
])
?>
    }
}
