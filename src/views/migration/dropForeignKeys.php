<?php 
echo "<?php\n";
?>
<?php
if (!empty($namespace)) {
    echo "\nnamespace {$namespace};\n";
}
?>

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class <?= $className ?> extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
<?= $this->render('@yii/views/_dropForeignKeys.php',['foreignKeys'=>$foreignKeys,'table'=>$table]);?>
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
<?= $this->render('@vendor/taktwerk/yii-boilerplate/src/views/migration/_addForeignKeys.php',['foreignKeys'=>$foreignKeys,'table'=>$table]);?>
    }
}