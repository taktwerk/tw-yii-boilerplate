<div class="record-history text-muted">
    <?php if ($created_by != null) { ?>
        <?= Yii::t('twbp', "Created By") ?>: <?= $created_by->toString() ?> (<?= Yii::$app->formatter->asDatetime($model->created_at) ?>)
    <?php } ?>
    <?php if ($created_at != null) { ?>
        <br/><?= Yii::t('twbp', "Created At") ?>: <?= Yii::$app->formatter->asDatetime($model->created_at) ?>
    <?php } ?>
    <?php if ($updated_by != null) { ?>
        <br/><?= Yii::t('twbp', "Updated By") ?>: <?= $updated_by->toString() ?> (<?= Yii::$app->formatter->asDatetime($model->updated_at) ?>)
    <?php } ?>
    <?php if ($updated_at != null) { ?>
        <br/><?= Yii::t('twbp', "Updated At") ?>: <?= Yii::$app->formatter->asDatetime($model->updated_at) ?>
    <?php } ?>
    <?php if ($deleted_by != null) { ?>
        <br/><?= Yii::t('twbp', "Deleted By") ?>: <?= $deleted_by->toString() ?> (<?= Yii::$app->formatter->asDatetime($model->deleted_at) ?>)
    <?php } ?>
    <?php if ($deleted_at != null) { ?>
        <br/><?= Yii::t('twbp', "Deleted At") ?>: <?= Yii::$app->formatter->asDatetime($model->deleted_at) ?>
    <?php } ?>
</div>