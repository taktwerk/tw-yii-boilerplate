<?php
use kartik\dialog\Dialog;
if(isset($model) && $model->isNewRecord){
    echo Dialog::widget([
        'dialogDefaults'=>[
            Dialog::DIALOG_ALERT=>[
                'type' => Dialog::TYPE_WARNING
            ]
        ],
        'libName' => 'krajeeDialogRelatedTab',
        'options' => [], // default options
    ]);
    $msg = addslashes(Yii::t('twbp','Is available after saving this form'));

$js = <<<EOT
$('[data-new-rec-alert=1]').click(function(e){
    e.preventDefault();
    if("$msg"){
        krajeeDialogRelatedTab.alert("$msg");
    }
    return false;
});
EOT;
$this->registerJs($js);
}