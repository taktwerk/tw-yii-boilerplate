<?php

use yii\helpers\Html;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */
/* @var $handler \yii\web\ErrorHandler */
$handler = new \yii\web\ErrorHandler();
$css = <<< CSS

.error-template {
    padding: 40px 15px;
    text-align: center;
}
.error-actions {
    margin-top:15px;
    margin-bottom:15px;
}
.error-details {
    font-size:20px !important;  
}
.error-actions .btn { 
    margin-right:10px; 
}
CSS;

$this->registerCss($css);
if ($exception instanceof \yii\web\HttpException) {
    $code = $exception->statusCode;
} elseif($exception->getCode()) {
    $code = $exception->getCode();
}else{
    $code = 500;
}
$name = $handler->getExceptionName($exception);
if ($name === null) {
    $name = Yii::t('twbp','Error');
}

if ($exception instanceof \yii\base\UserException) {
    $message = $exception->getMessage();
} else {
    $message = Yii::t('twbp','An internal server error occurred.');
}
$this->title =$handler->htmlEncode($name);

\taktwerk\yiiboilerplate\assets\TwAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <div class="container">
        <div class="row">
            <?php if (YII_DEBUG && $exception) : ?>
                <div class="col-md-12">
                    <p>
                        <?php ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\helpers\DebugHelper::class)::pre($exception->getTraceAsString())?>
                    </p>
                </div>
            <?php endif; ?>
            <div class="col-md-12">
                <div class="error-template">
                    <div class="error-details">
                        <h1><?=Yii::t('twbp','Oops!')?></h1>
                        <h1><?= $handler->htmlEncode($name) ?></h1>
                        <h2><?=$handler->htmlEncode($code)?></h2>
                        <?= nl2br($handler->htmlEncode($message))?>
                    </div>
                    <div class="error-actions">
                        <a href="<?=Yii::$app->homeUrl;?>" class="btn btn-primary btn-lg">
                            <span class="glyphicon glyphicon-home"></span> <?=Yii::t('twbp','Home')?>
                        </a>
                        <a href="#" onclick="history.back()" class="btn btn-default btn-lg">
                            <span class="glyphicon glyphicon-chevron-left"></span> <?=Yii::t('twbp','Back')?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


