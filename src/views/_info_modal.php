<?php
/**
 * Copyright (c) 2016.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 12/20/2016
 * Time: 1:31 PM
 */
?>
<div class="row">
    <div class="col-md-12">
        <p><?= Yii::t('twbp', 'Possible search operators'); ?>:</p>
        <ul class="list-group">
            <li class="list-group-item"><b><></b>, <b>!</b>, <b>!=</b> Operator <b>NOT</b></li>
            <li class="list-group-item"><b>=></b>, <b>>=</b> Operator <b>GREATER OR EQUAL THAN</b></li>
            <li class="list-group-item"><b>in=</b> Operator <b>IN</b> example <b>in= </b>xx,yy</li>
            <li class="list-group-item"><b>></b> Operator <b>GREATER THAN</b></li>
            <li class="list-group-item"><b>=<</b>, <b><=</b> Operator <b>SMALLER OR EQUAL THAN</b></li>
            <li class="list-group-item"><b><</b> Operator <b>SMALLER THAN</b></li>
            <li class="list-group-item"><b>==</b> Operator <b>Equal To(for exact match)</b></li>
            <li class="list-group-item">xx <b>between</b> yy, xx <b>to</b> yy, xx<b>...</b>yy Operator <b>BETWEEN</b> xx and yy</li>
            <li class="list-group-item">xx <b>or</b> yy Operator <b>LIKE</b> %xx% <b>OR</b> <b>LIKE</b> %yy%</li>
            <li class="list-group-item">xx <b>and</b> yy Operator <b>LIKE</b> %xx% <b>AND</b> <b>LIKE</b> %yy%</li>
            <li class="list-group-item"><b>=0</b> Operator (<?= Yii::t('twbp', 'Find empty entries'); ?>)</li>
            <li class="list-group-item"><b>!=0</b> Operator (<?= Yii::t('twbp', 'Find non-empty entries'); ?>)</li>
        </ul>
    </div>
</div>
<?= $this->render('_shortcut_info_modal');?>