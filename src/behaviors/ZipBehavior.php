<?php
namespace taktwerk\yiiboilerplate\behaviors;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use yii\base\Behavior;
use yii\helpers\StringHelper;

class ZipBehavior extends Behavior{
    
    public $zipJobClass='\taktwerk\yiiboilerplate\commands\GenerateZipJob';
    public $excludedRelationsListForZip = [];
    public function findZipGenerateQueueJobModel($type){
        if (($model = QueueJob::findOne([
            'command' => $this->zipJobClass,
            'parameters' => json_encode($this->owner->generateParametersForZipJobQueue(
                $this->owner->id, $type)),
            'status' => QueueJob::STATUS_QUEUED
        ])) !== null) {
            return $model;
        }
        return null;
    }
    public function createZipGenerateQueueJobModel($type='processed'){
        $job = new QueueJob();
        $base = StringHelper::basename(get_class($this->owner));
        $toString = $this->owner->toString;
        return $job->queue("Generate ZIP for $base {$toString}", $this->zipJobClass, $this->generateParametersForZipJobQueue($this->owner, $type), true);
    }
    public function additionalFilesForZip():Array{
        return [];
    }

    public function generateParametersForZipJobQueue($type)
    {
        return [
            'id' => $this->owner->id,
            'name_space' => get_class($this->owner),
            'type' => $type,
            'additional_files' => $this->owner->additionalFilesForZip(),
            'excluded_relations' => $this->excludedRelationsForZip(),
        ];
    }
    public function excludedRelationsForZip():Array{
        return $this->excludedRelationsListForZip;
    }
    public function allZipFiles($type='processed',$excludedRelations=[], $additionalFiles=[]){
        
        $parentModel = $this->owner;
        //array that will contain all the upload fields relational wise along with parent model
        $parsedRelation = [];

        //get all the fields of current model that can have upload files
        $parsedRelation[$parentModel->tableName()]['fields'] = $parentModel->getUploadFields(true);
        $parsedRelation[$parentModel->tableName()]['number_fields'] = $parentModel->getAttributes();
        
        $allRelations = $parentModel->extraFields();
        $allRelations = array_diff($allRelations, $excludedRelations);
        
        //loop to analyse all the relations of parent model and get their relation functions
        foreach ($allRelations as $relation){
            $upload_fields = [];
            $relationalFunction = 'get'.ucfirst($relation);
            if(method_exists($parentModel, $relationalFunction)){
                /**@var $all_relation_objects */
                $relationalClass = $parentModel->$relationalFunction()->modelClass;
                $relationalObject = new $relationalClass;
                if(method_exists($relationalObject,'getUploadFields')){
                    $upload_fields = $relationalObject->getUploadFields(true);
                    $parsedRelation[$relationalObject->tableName()]['fields'] = $relationalObject->getUploadFields(true);
                    $parsedRelation[$relationalObject->tableName()]['number_fields'] = $relationalObject->getAttributes();
                    $parsedRelation[$relationalObject->tableName()]['relational_function'] = $relationalFunction;
                }
                
                //if no upload fields, in direct relational model
                //loop to analyse all the relations of relational models and get their sub relation functions
                if(empty($upload_fields)){
                    $allSubRelations = $relationalObject->extraFields();
                    $allSubRelations = array_diff($allSubRelations, $excludedRelations);
                    foreach ($allSubRelations as $subRelation){
                        $subRelationalFunction = 'get'.ucfirst($subRelation);
                        if(method_exists($relationalObject, $subRelationalFunction)) {
                            $subRelationalClass = $relationalObject->$subRelationalFunction()->modelClass;
                            $subRelationalObject = new $subRelationalClass;
                            
                            $subRelationalTable = $subRelationalObject->tableName();
                            if($subRelationalTable == $parentModel->tableName()){
                                $parentRelation = $subRelation;
                            }

                            if(method_exists($subRelationalObject,'getUploadFields') && $subRelationalTable!=$parentModel->tableName()){
                                $parsedRelation[$subRelationalTable][$relationalClass]['fields'] = $subRelationalObject->getUploadFields(true);
                                $parsedRelation[$subRelationalTable][$relationalClass]['number_fields'] = $subRelationalObject->getAttributes();
                                $parsedRelation[$subRelationalTable][$relationalClass]['sub_relation'] = $subRelation;
                                $parsedRelation[$subRelationalTable][$relationalClass]['parent_class'] = $parentRelation;
                            }

                        }
                    }
                }
            }
        }
        
        $allFiles = [];
        
        //loop to get all the paths of the files
        foreach ($parsedRelation as $relation){
            $numberField = "";
            if(isset($relation['number_fields'])){
                if(array_key_exists('order_number', $relation['number_fields'])){
                    $numberField = 'order_number';
                }
            }
            if(key_exists('fields',$relation) &&!key_exists('sub_relation',$relation) && !key_exists('relational_function',$relation)){
                $array1 = explode('\\', get_class($parentModel));
                $modelName = end($array1);
                foreach ($relation['fields'] as $field){
                    if($parentModel->$field){
                        if($type=='processed'){
                            $fileName = $parentModel->getFilePath($field,[],true);
                        }else{
                            $fileName = $parentModel->getUploadPath() . $parentModel->$field;
                        }
                        if($numberField){
                            $fileName .= "|;;|".$parentModel->$numberField;
                        }
                        $allFiles[$modelName][] = $fileName;
                    }
                }
            }elseif(key_exists('relational_function',$relation)){
                $function = $relation['relational_function'];
                $foreignObjects = $parentModel->$function()->all();
                foreach ($foreignObjects as $foreignObject){
                    $array2 = explode('\\', get_class($foreignObject));
                    $modelName = end($array2);
                    foreach ($relation['fields'] as $field){
                        if($foreignObject->$field){
                            if($type=='processed'){
                                $fileName = $foreignObject->getFilePath($field,[],true);
                            }else{
                                $fileName = $foreignObject->getUploadPath() . $foreignObject->$field;
                            }
                            if($numberField){
                                $fileName .= "|;;|".$foreignObject->$numberField;
                            }
                            $allFiles[$modelName][] = $fileName;
                        }
                    }
                }
            }else{
                foreach ($relation as $class=>$classRelation){
                    $numberField = "";
                    if(isset($classRelation['number_fields'])){
                        if(array_key_exists('order_number', $classRelation['number_fields'])){
                            $numberField = 'order_number';
                        }
                    }
                    $objectClass = $class;
                    $subRelation = $classRelation['sub_relation'];
                    $relationObject = new $objectClass();
                    if(!$relationObject->hasMethod('get'.$classRelation['parent_class'])){
                        continue;
                    }
                    $foreignObjects = $relationObject::find()->joinWith($classRelation['parent_class'])->where([$parentModel->tableName().".".$parentModel::primaryKey()[0]=>$parentModel->primaryKey])->all();
                    foreach ($foreignObjects as $foreignObject){
                        $array = explode('\\', get_class($foreignObject->$subRelation));
                        $modelName = end($array);
                        /**@var $foreignObject  \yii\db\ActiveRecord*/
                        foreach ($classRelation['fields'] as $field){
                            if($foreignObject->$subRelation->$field){
                                if($type=='processed'){
                                    $fileName = $foreignObject->$subRelation->getFilePath($field,[],true);
                                }else{
                                    $fileName = $foreignObject->$subRelation->getUploadPath() . $foreignObject->$subRelation->$field;
                                }
                                if($numberField){
                                    $fileName .= "|;;|".$foreignObject->$subRelation->$numberField;
                                }
                                $allFiles[$modelName][] = $fileName;
                            }
                            
                        }
                    }
                }
            }
        }
        
        //merge files that are not present relationally(extra files)
        if(!empty($additionalFiles)){
            $allFiles['Other']=$additionalFiles;
        }
        
        if($type=='processed'){
            $allFiles['type'] = $type;
        }       
        return $allFiles;
    }
}