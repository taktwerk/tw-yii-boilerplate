<?php

namespace taktwerk\yiiboilerplate\behaviors;

use yii2tech\authlog\AuthLogIdentityBehavior;

class AuthLogWebUserBehavior extends \yii2tech\authlog\AuthLogWebUserBehavior
{

    /**
     * Handles owner 'afterLogin' event, logging the authentication success.
     * @param \yii\web\UserEvent $event event instance.
     */
    public function afterLogin($event)
    {
        /* @var $identity \yii\web\IdentityInterface|AuthLogIdentityBehavior */
        $identity = $event->identity;
        $identity->logAuth([
            'cookie_based' => $event->cookieBased,
            'duration' => $event->duration,
        ]);
    }

}