<?php

namespace taktwerk\yiiboilerplate\behaviors;

use taktwerk\yiiboilerplate\modules\user\models\LoginAttempt;
use Yii;
use yii\base\Model;
use yii\db\Expression;
use yii\helpers\Inflector;
use yii\base\Behavior;

/**
 * Class LoginAttemptBehavior
 *
 * @package taktwerk\yiiboilerplate\behaviors
 */
class LoginAttemptBehavior extends Behavior
{
    /**
     * @var int
     */
    public $attempts = 3;

    /**
     * @var int
     */
    public $duration = 300;

    /**
     * @var string
     */
    public $durationUnit = 'second';

    /**
     * @var int
     */
    public $disableDuration = 900;

    /**
     * @var string
     */
    public $disableDurationUnit = 'second';

    /**
     * @var string
     */
    public $usernameAttribute = 'email';

    /**
     * @var string
     */
    public $passwordAttribute = 'password';
    /**
     * @var string
     */
    public $message = '';

    /**
     * @var
     */
    private $_attempt;

    /**
     * @var string[]
     */
    private $_safeUnits = [
        'second',
        'minute',
        'day',
        'week',
        'month',
        'year',
    ];

    /**
     * @return string[]
     */
    public function events()
    {
        return [
            Model::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            Model::EVENT_AFTER_VALIDATE => 'afterValidate',
        ];
    }

    public function beforeValidate()
    {
        if ($this->_attempt = LoginAttempt::find()->where(['key' => $this->key])->andWhere(['>', 'reset_at', new Expression('NOW()')])->one())
        {
            if ($this->_attempt->amount >= $this->attempts)
            {
                $minutes = floor($this->disableDuration / 60);
                $this->message = Yii::t('twbp', 'You have exceeded the password attempts. You have to wait {x} minutes to try again.', ['x'=>$minutes]);
                $this->owner->addError($this->usernameAttribute, $this->message);
            }
        }
    }

    /**
     * @throws \Exception
     */
    public function afterValidate()
    {
        if ($this->owner->hasErrors($this->passwordAttribute))
        {
            if (!$this->_attempt)
            {
                $this->_attempt = new LoginAttempt;
                $this->_attempt->key = $this->key;
            }

            $this->_attempt->amount += 1;

            if ($this->_attempt->amount >= $this->attempts)
                $this->_attempt->reset_at = $this->intervalExpression($this->disableDuration, $this->disableDurationUnit);
            else
                $this->_attempt->reset_at = $this->intervalExpression($this->duration, $this->durationUnit);

            $this->_attempt->save();
        }
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return sha1($this->owner->{$this->usernameAttribute});
    }

    /**
     * @param int    $length
     * @param string $unit
     *
     * @return Expression
     * @throws \Exception
     */
    private function intervalExpression(int $length, $unit = 'second')
    {
        $unit = Inflector::singularize(strtolower($unit));

        if (!in_array($unit, $this->_safeUnits))
        {
            $safe = join(', ', $this->_safeUnits);
            throw new \Exception("$unit is not an allowed unit. Safe units are: [$safe]");
        }

        if (Yii::$app->db->driverName === 'pgsql')
            $interval = "'$length $unit'";
        else
            $interval = "$length $unit";

        return new Expression("NOW() + INTERVAL $interval");
    }
}
