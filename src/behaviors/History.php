<?php
/**
 * Created by PhpStorm.
 * User: Jeremy
 * Date: 19/04/2016
 * Time: 16:56
 */

namespace taktwerk\yiiboilerplate\behaviors;

use bupy7\activerecord\history\behaviors\History as BaseHistory;
use bupy7\activerecord\history\entities\History as HistoryEntity;
use yii\db\BaseActiveRecord;
use yii\web\Application;
use yii\base\Event;
use yii\helpers\StringHelper;
use yii\base\NotSupportedException;
use Yii;

/**
 * Class History
 * @package taktwerk\yiiboilerplate\behaviors
 *
 * We've had to re-declare a lot of the bupy7-History class, since 1.1.1 has switched most methods to private for some
 * reason.
 */
class History extends BaseHistory
{
    /**
     * To enable/disable history in a process/codeblock if needed. 
     */
    public static $globalSwitch = true;

    public $skippedTables = [];
    /**
     * @inhertidoc
     */
    public function init()
    {
        $this->module = new \stdClass();
        $this->module->db = \Yii::$app->getDb();
        if (php_sapi_name() != "cli") {
            $this->module->user = \Yii::$app->getUser();
        } else {
            $this->module->user = null;
        }
        $this->module->storage = 'bupy7\activerecord\history\storages\Database';
        $this->skipAttributes = array_fill_keys($this->skipAttributes, true);

        $configSkippedTables = [];
        if (Yii::$app->params &&
            !empty(Yii::$app->params['arhistory']) &&
            !empty(Yii::$app->params['arhistory']['skippedTables'])
        ) {
            $configSkippedTables = Yii::$app->params['arhistory']['skippedTables'];
        }
        $this->skippedTables = array_merge($this->skippedTables, $configSkippedTables);
    }

    /**
     * Process of saving history to storage.
     * @param Event $event
     */
    public function saveHistory(Event $event)
    {
        if ($this->isSkippedTable()) {
            return;
        }
        if(self::$globalSwitch===false){
            return;
        }
        $rowId = $this->getRowId();
        $tableName = $this->getTableName();
        $createdBy = $this->getCreatedBy();
        $createdAt = time();

        /** @var \bupy7\activerecord\history\storages\Base $storage */
        $storage = new $this->module->storage;

        switch ($event->name) {
            case BaseActiveRecord::EVENT_AFTER_INSERT:
                $model = new HistoryEntity([
                    'tableName' => $tableName,
                    'rowId' => $rowId,
                    'event' => HistoryEntity::EVENT_INSERT,
                    'createdAt' => $createdAt,
                    'createdBy' => $createdBy,
                ]);
                $storage->add($model);
                break;

            case BaseActiveRecord::EVENT_AFTER_DELETE:
                $model = new HistoryEntity([
                    'tableName' => $tableName,
                    'rowId' => $rowId,
                    'event' => HistoryEntity::EVENT_DELETE,
                    'createdAt' => $createdAt,
                    'createdBy' => $createdBy,
                ]);
                $storage->add($model);
                break;

            case BaseActiveRecord::EVENT_AFTER_UPDATE:
                foreach ($event->changedAttributes as $name => $value) {
                    if (isset($this->skipAttributes[$name]) || $this->isEquals($name, $value)) {
                        continue;
                    }
                    if (isset($this->customAttributes[$name])) {
                        $oldValue = call_user_func($this->customAttributes[$name], $event, false);
                        $newValue = call_user_func($this->customAttributes[$name], $event, true);
                    } else {
                        $oldValue = $value;
                        $newValue = $this->owner->$name;
                    }
                    //To store upto the text data type store limit of the database. 63000 is the bytes = 63kb.
                        $oldValue = StringHelper::truncate($oldValue,63000);
                        $newValue = StringHelper::truncate($newValue,63000);
                        $model = new HistoryEntity([
                            'tableName' => $tableName,
                            'rowId' => $rowId,
                            'fieldName' => $name,
                            'oldValue' => $oldValue,
                            'newValue' => $newValue,
                            'event' => HistoryEntity::EVENT_UPDATE,
                            'createdAt' => $createdAt,
                            'createdBy' => $createdBy,
                        ]);
                        $storage->add($model);
                    
                }
                break;
        }
        $storage->flush();
    }

    protected function isSkippedTable()
    {
        return in_array($this->getTableName(), $this->skippedTables);
    }

    /**
     * @return int|string
     */
    protected function getCreatedBy()
    {
        if (!empty($this->module->user) && \Yii::$app instanceof Application) {
            return $this->module->user->id;
        }

        return 0;
    }

    /**
     * Return row id which updated, created or deleted.
     * @return integer
     */
    protected function getRowId()
    {
        $primaryKey = $this->getPrimaryKey();
        return $this->owner->$primaryKey;
    }

    /**
     * Return primary key of attached model.
     * @return string
     * @throws NotSupportedException
     */
    protected function getPrimaryKey()
    {
        $primaryKey = $this->owner->primaryKey();
        if (count($primaryKey) == 1) {
            $primaryKey = array_shift($primaryKey);
        } else {
            throw new NotSupportedException('Composite primary key not supported.');
        }
        return $primaryKey;
    }

    /**
     * Return table name of attached model.
     * @return string
     */
    protected function getTableName()
    {
        $owner = $this->owner;
        return $owner::tableName();
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return bool
     */
    protected function isEquals($name, $value)
    {
        if (is_object($value)) {
            return $value === $this->owner->$name;
        }
        return $value == $this->owner->$name;
    }
}