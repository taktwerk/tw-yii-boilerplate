<?php
namespace taktwerk\yiiboilerplate\behaviors;

use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\ActiveQuery;

/**
 * Behavior for sortable Yii2 GridView widget.
 *
 * For example:
 *
 * ```php
 * public function behaviors()
 * {
 * return [
 * 'sort' => [
 * 'class' => SortableGridBehavior::className(),
 * 'sortableAttribute' => 'sortOrder',
 * 'scope' => function ($query) {
 * $query->andWhere(['group_id' => $this->group_id]);
 * },
 * ],
 * ];
 * }
 * ```
 *
 * @author Samir Dixit
 */
class SortableGridBehavior extends Behavior
{

    /** @var string database field name for row sorting */
    public $sortableAttribute = 'sortOrder';

    /** @var string attribute/column names on which sorting will be based*/
    public $sortRestrictAttributes = [

    ];

    /** @var callable */
    public $scope;

    /** @var callable */
    public $afterGridSort;
    
    /** @var boolean If true then order numbers remain in continuous numbering without gaps even if user intentionaliy enters a higher order number then the number of rows*/
    public $strictOrdering = true;
    
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsert',
            ActiveRecord::EVENT_AFTER_INSERT => 'checkRedundancyAfterInsert',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'checkRedundancy',
            ActiveRecord::EVENT_BEFORE_DELETE => 'gaplessOrder',
        ];
    }

    public function gridSort($items)
    {
        /** @var ActiveRecord $model */
        $model = $this->owner;
        if (! $model->hasAttribute($this->sortableAttribute)) {
            throw new InvalidConfigException("Model does not have sortable attribute `{$this->sortableAttribute}`.");
        }

        $newOrder = [];
        $models = [];
        foreach ($items as $old => $new) {
            $models[$new] = $model::findOne($new);
            $newOrder[$old] = $models[$new]->{$this->sortableAttribute} ? $models[$new]->{$this->sortableAttribute} : $new;
        }
        $model::getDb()->transaction(function () use ($models, $newOrder) {
            foreach ($newOrder as $modelId => $orderValue) {
                /** @var ActiveRecord[] $models */
                $models[$modelId]->updateAttributes([
                    $this->sortableAttribute => $orderValue
                ]);
            }
        });

        if (is_callable($this->afterGridSort)) {
            call_user_func($this->afterGridSort, $model);
        }
    }

    public function beforeInsert()
    {
        /** @var ActiveRecord $model */
        $model = $this->owner;
        if (! $model->hasAttribute($this->sortableAttribute)) {
            throw new InvalidConfigException("Invalid sortable attribute `{$this->sortableAttribute}`.");
        }
        if (strlen($model->{$this->sortableAttribute}) > 0) {
            return true;
        }
        $query = $model::find();
        if (is_callable($this->scope)) {
            call_user_func($this->scope, $query);
        }

        /* Override model alias if defined in the model's class */
        $query->from([
            $model::tableName() => $model::tableName()
        ]);

        if($this->sortRestrictAttributes && count($this->sortRestrictAttributes)>0){
            foreach($this->sortRestrictAttributes as $at){
                $query->andWhere([$at=>$model->$at]);
            }
        }
        $maxOrder = $query->max('{{' . trim($model::tableName(), '{}') . '}}.[[' . $this->sortableAttribute . ']]');
        $model->{$this->sortableAttribute} = $maxOrder + 1;
    }
    public function checkRedundancyAfterInsert(){
        $this->checkRedundancy(true);
    }

    //    This variant fast
    public function checkRedundancy($insert=false)
    {
        $atr = $this->sortableAttribute;
        /** @var ActiveRecord $model */
        $model = $this->owner;
        $tblName = $model->getTableSchema()?$model->getTableSchema()->fullName:NULL;
        $existQ = $model->find()
            ->where([
                $tblName.'.'.$atr => $model->{$atr}
            ])->andWhere([
                '!=',
                $tblName.'.'.$model->primaryKey()[0],
                $model->primaryKey
            ]);

        $addOnCondition = [];
        if(is_array($this->sortRestrictAttributes) && count($this->sortRestrictAttributes)>0){
            foreach($this->sortRestrictAttributes as $at){
                $addOnCondition[$tblName.'.'.$at]=$model->$at;
            }
            $existQ->andWhere($addOnCondition);
        }
        $addOnConditionSql = [];
        if($model->hasAttribute('deleted_at')){
            $addOnConditionSql[] = \Yii::$app->db->quoteColumnName('deleted_at')." IS NULL";
        }
        foreach ($addOnCondition as $rAtr=>$val){
            $addOnConditionSql[] = \Yii::$app->db->quoteColumnName($rAtr)." = ".\Yii::$app->db->quoteValue($val);
        }
        $exist = $existQ->one();
        if ($exist) {
            $oldOrder = $model->oldAttributes[$atr];
            $newOrder = $model->{$atr};

            $rownumber = 0;
            if (strlen($newOrder) <= 0) {
                return;
            }

            $move_up = false;
            // insert new entry in-between the other records
            if ($insert === true) {
                $sortItemsQuery = $model::find()->where([
                    'AND',
                    ['!=', $tblName.'.'.$model->primaryKey()[0], $model->primaryKey],
                    ['>=', $tblName.'.'.$atr, $newOrder]
                ])
                    ->andWhere($addOnCondition)
                    ->orderBy($tblName.'.'.$atr);

            }
            // move down
            else if ( $oldOrder > $newOrder ) {
                $sortItemsQuery = $model::find()->where([
                    'AND',
                    ['!=', $tblName.'.'.$model->primaryKey()[0], $model->primaryKey],
                    ['>=', $tblName.'.'.$atr, $newOrder],
                    ['<', $tblName.'.'.$atr, $oldOrder],
                ])
                    ->andWhere($addOnCondition)
                    ->orderBy($tblName.'.'.$atr);

            }
            // move up
            else if ($oldOrder < $newOrder) {
                $move_up = true;
                $sortItemsQuery = $model::find()->where([
                    'AND',
                    ['>', $tblName.'.'.$atr, $oldOrder],
                    ['<=', $tblName.'.'.$atr, $newOrder],
                ])
                    ->andWhere($addOnCondition)
                    ->orderBy($tblName.'.'.$atr);
            }

            if($sortItemsQuery instanceof ActiveQuery){
            foreach ($sortItemsQuery->batch() as $sortItems) {
                foreach ($sortItems as $sortItem) {
                    if ($move_up) {
                        // shift down
                        $sortItem->{$atr} -= 1;
                    } else {
                        // shift down
                        $sortItem->{$atr} += 1;
                    }
                    $sortItem->detachBehavior('sort');
                    $sortItem->save();
                }
            }
            }
        }
        $model->updateAttributes([$atr=>$model->{$atr}]);
        if ($this->strictOrdering){
            $this->gaplessOrder();
            
            /* $db = \Yii::$app->getDb();
            $tbl = $model::getTableSchema()->fullName;
            $sql = <<<EOT
                            UPDATE `{$tbl}` SET `{$this->sortableAttribute}` = (@rownum := 1 + @rownum)
                            WHERE 0 = (@rownum:=0)
EOT;
            foreach ($addOnConditionSql as $cond) {
                $sql .= ' AND ' . $cond;
            }
            
            $sql .= " ORDER BY `{$this->sortableAttribute}`;";
            $db->createCommand($sql)->execute(); */
        }
    }
    public function gaplessOrder($event=null){
        $model = $this->owner;
        $addOnCondition = [];

        if(is_array($this->sortRestrictAttributes) && count($this->sortRestrictAttributes)>0){
            foreach($this->sortRestrictAttributes as $at){
                $addOnCondition[$at]=$model->$at;
            }
        }
        $addOnConditionSql = [];

        foreach ($addOnCondition as $rAtr=>$val){
            $addOnConditionSql[] = \Yii::$app->db->quoteColumnName($rAtr)." = ".\Yii::$app->db->quoteValue($val);
        }
        $db = \Yii::$app->getDb();
        $tbl = $model::getTableSchema()->fullName;
        $sql = <<<EOT
            UPDATE `{$tbl}` SET `{$this->sortableAttribute}` = (@rownum := 1 + @rownum)
            WHERE 0 = (@rownum:=0) 
EOT;
        if($model->hasAttribute('deleted_at')){
            $addOnConditionSql[] = \Yii::$app->db->quoteColumnName('deleted_at')." IS NULL";
        }
        if($event){
            if($event->name==ActiveRecord::EVENT_BEFORE_DELETE){
                /*If this fuction is called during deletion of the current model then we re-order other records except the current one*/
                $pk = $event->sender->getPrimaryKey(true);
                foreach($pk as $k=>$v){
                    //$sql .= ' AND '.\Yii::$app->db->quoteColumnName($k).' != '.\Yii::$app->db->quoteValue($v);
                    $addOnConditionSql[] = \Yii::$app->db->quoteColumnName($k).' != '.\Yii::$app->db->quoteValue($v);
                }
            }
        }
        if(count($addOnConditionSql)>0){
            $sql .= ' AND (' .implode(' AND ', $addOnConditionSql).') + (@rownum:=0)';
        }
        $sql .= " ORDER BY `{$this->sortableAttribute}`;";
        $db->createCommand($sql)->execute();
    }
//    This variant update all relations but is very slow
//    public function checkRedundancy($insert = false)
//    {
//        $atr = $this->sortableAttribute;
//        /** @var ActiveRecord $model */
//        $model = $this->owner;
//        $existQ = $model->find()
//            ->where([$atr => $model->{$atr}])
//            ->andWhere(['!=', $model->primaryKey()[0], $model->primaryKey]);
//        $addOnCondition = [];
//        if(is_array($this->sortRestrictAttributes) && count($this->sortRestrictAttributes) > 0){
//            foreach($this->sortRestrictAttributes as $at){
//                $addOnCondition[$at] = $model->$at;
//            }
//            $existQ->andWhere($addOnCondition);
//        }
//        $exist = $existQ->one();
//
//        if (!$exist) {
//            return;
//        }
//        if (strlen($model->{$atr}) <= 0) {
//            return;
//        }
//        $newOrder = (int) $model->{$atr};
//        $oldOrder = (int) $model->oldAttributes[$atr];
//
//        if ($oldOrder === $newOrder || !$insert) {
//            return;
//        }
//
//        $rownumber = 0;
//        $sortItemsQuery = $model::find()->where($addOnCondition);
//
//        if ($oldOrder > $newOrder || $insert) {
//            $rownumber = $newOrder;
//            $sortItemsQuery = $sortItemsQuery->where([
//                'AND',
//                ['!=', $model->primaryKey()[0], $model->primaryKey],
//                ['>=', $atr, $model->{$atr}]
//            ]);
//        } else if ($oldOrder < $newOrder) {
//            $sortItemsQuery = $sortItemsQuery->where(['AND', ['<', $atr, $model->{$atr}]]);
//        }
//        $sortItems = $sortItemsQuery->orderBy($atr)->all();
//
//        foreach ($sortItems as $sortItem) {
//            $sortItem->{$atr} = $rownumber++;
//            $sortItem->save();
//        }
//    }
}
