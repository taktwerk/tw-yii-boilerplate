<?php

namespace taktwerk\yiiboilerplate\components;


use taktwerk\yiiboilerplate\modules\user\models\Session;
use Yii;
use yii\helpers\Url;
use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;

/**
 * Class User.
 *
 * Custom user class with additional checks and implementation of a 'root' user, who
 * has all permissions (`can()` always return true)
 */
class WebUser extends \yii\web\User
{
    const PUBLIC_ROLE = 'Public';

    /**
     * @var array Users with all permissions
     */
    public $rootUsers = [];

    /**
     * @var array Roles with all permissions
     */
    public $rootRoles = [];

    /**
     * @var role based redirects
     *e.g
     *[
     * 'GuiderViewer'=>['guide/explore/'],
     * 'GuiderAdmin'=>['guide/guide/index'],
     *]
     */
    public $roleBasedReturnUrlMap = [];

    public $defaultRoleBasedReturnUrlMap = [];
    /**
     * Initializes the application component.
     */
    public function init()
    {
        parent::init();
        $this->loginUrl = [getenv('USER_LOGIN_URL') ? : '/user/security/login'];
    }
    /**
     * @inheritdoc
     */
    public function loginRequired($checkAjax = true, $checkAcceptHeader = true){
        $res = parent::loginRequired($checkAjax,$checkAcceptHeader);
        $request = Yii::$app->getRequest();
        $canRedirect = !$checkAcceptHeader || $this->checkRedirectAcceptable();
        if ($this->enableSession
            && $request->getIsGet()
            && (!$checkAjax || !$request->getIsAjax())
            && $canRedirect
            ) {
                if(\Yii::$app->controller->id!='site' || (\Yii::$app->controller->id=='site' && \Yii::$app->controller->action->id!='index')){
                    //Saving the url which requires login
                    Url::remember('','_afterLoginRedirectUrl');
                }
            }
        return $res;
    }
    /**
     * @inheritdoc
     */
    public function getReturnUrl($defaultUrl = null)
    {
       if(\Yii::$app->controller->action->id=='login'){
           $url = Url::previous('_afterLoginRedirectUrl');
           if(strlen($url)>0){
               //Redirecting back to the saved url which required login
               return $url;
           }
           if($this->isGuest==false){
               $userTwData = @$this->identity->userTwData;
               if($userTwData && strlen($userTwData->login_redirect)>0){
                   return $userTwData->login_redirect;
               }
           }
           if(!is_array($this->defaultRoleBasedReturnUrlMap)){
               throw new \Exception("The 'defaultRoleBasedReturnUrlMap' property must be an array of role and url mapping");
           }
           if(!is_array($this->roleBasedReturnUrlMap)){
               throw new \Exception("The 'roleBasedReturnUrlMap' property must be an array of role and url mapping");
           }
           $roleUrls = $this->roleBasedReturnUrlMap + $this->defaultRoleBasedReturnUrlMap;
           $explictlyAssignedRoles = $this->getRoles(true);
           foreach ($roleUrls as $role=>$url){
               $explicitCheck = false;
               if(StringHelper::startsWith($role,'=')){
                   $explicitCheck = true;
                   $role = substr($role, 1);
               }
               if($url && $this->can($role) && ($this->can('Authority')==false || $role==='Authority')){
                   if($explicitCheck==false || in_array($role, $explictlyAssignedRoles)){
                       if(isset($url[0])){
                           return Yii::$app->getUrlManager()->createUrl(array_unique($url));
                       }
                       return $url;
                   }
               }
           }
       }
       return parent::getReturnUrl($defaultUrl);
    }
    /**
     * Extended permission check with `Guest` role and `route`.
     *
     * @param string    $permissionName
     * @param array     $params
     * @param bool|true $allowCaching
     *
     * @return bool
     */
    public function can($permissionName, $params = [], $allowCaching = true)
    {
        switch (true) {
            
            // root users have all permissions
            case Yii::$app->user->identity && (in_array(Yii::$app->user->identity->username, $this->rootUsers) || Yii::$app->user->identity->isAdmin):
                
                return true;
                break;
            case Yii::$app->user->identity && Yii::$app->user->identity->isBlocked:
                \Yii::$app->user->logout();
                return false;
                break;
            case !empty($params['route']):
                Yii::debug("Checking route permissions for '{$permissionName}'", __METHOD__);
                
                return $this->checkAccessRoute($permissionName, $params, $allowCaching);
                break;
            default:
                return parent::can($permissionName, $params, $allowCaching);
        }
    }

    /**
     * Checks permissions from guest role, when no user is logged in.
     *
     * @param $permissionName
     * @param $params
     * @param $allowCaching
     *
     * @return bool
     */
    private function canGuest($permissionName, $params, $allowCaching)
    {
        $guestPermissions = $this->getAuthManager()->getPermissionsByRole(self::PUBLIC_ROLE);

        return array_key_exists($permissionName, $guestPermissions);
    }

    /**
     * Checks route permissions.
     *
     * Splits `permissionName` by underscore and match parts against more global rule
     * eg. a permission `app_site` will match, `app_site_foo`
     *
     * @param $permissionName
     * @param $params
     * @param $allowCaching
     *
     * @return bool
     */
    private function checkAccessRoute($permissionName, $params, $allowCaching)
    {
        $route = explode('_', $permissionName);
        $routePermission = '';
        $roles = array_map('strtolower',ArrayHelper::map(\Yii::$app->authManager->getRoles(),'name','name'));
        foreach ($route as $part) {
            $routePermission .= $part;
            if(in_array($routePermission, $roles)){
                $routePermission .= '_';
                continue;
            }
            if (Yii::$app->user->id){
                $canRoute = parent::can($routePermission, $params, $allowCaching);
            } else {
                $canRoute = $this->canGuest($routePermission, $params, $allowCaching);

                if (!$canRoute){
                    $this->setLoginUrl();
                }
            }
            if ($canRoute){
                return true;
            }
            $routePermission .= '_';
        }

        return false;
    }

    /**
     * Update the login url based on the cookie
     */
    protected function setLoginUrl()
    {
        $userModule = Yii::$app->getModule('user');
        if ($userModule->enableRememberLoginPage) {
            $cookieName = $userModule->originCookieName;
            if (Yii::$app->getRequest()->cookies[$cookieName]) {
                $origin = Yii::$app->getRequest()->cookies->getValue($cookieName);
                $this->loginUrl = base64_decode($origin);
            }
        }
    }

    public function logoutByUserId($userId)
    {
        return Session::deleteAll(['user_id' => $userId]);
    }

    /**
     * Can this user manage clients
     *
     * @return bool
     */
    public function canManageClients()
    {
        return \Yii::$app->user->can('Authority');
    }

    /**
     *
     * @param bool $onlyName
     * @return array|\yii\rbac\Role[]
     */
    public function getRoles($onlyName = false)
    {
        $roles = Yii::$app->authManager->getRolesByUser($this->id);
        if ($onlyName === false) {
            return $roles;
        }

        $roleNames = [];
        foreach ($roles as $role => $data) {
            $roleNames[] = $role;
        }
        return $roleNames;
    }

    public function getPermissions($onlyName = false)
    {
        $permissions = Yii::$app->authManager->getPermissionsByUser($this->id);
        if ($onlyName === false) {
            return $permissions;
        }

        $permissionNames = [];
        foreach ($permissions as $permission => $data) {
            $permissionNames[] = $permission;
        }
        return $permissionNames;
    }

    /**
     *
     * @param bool $onlyName
     * @return array|\yii\rbac\Role[]
     */
    public function getAllRoles($onlyName = false)
    {
        $roles = Yii::$app->authManager->getRolesByUser($this->id);

        $childRoleNames = [];
        $childRolesArray = [];
        foreach ($roles as $role) {
            $childRoles = Yii::$app->authManager->getChildRoles($role->name);
            foreach ($childRoles as $childRole => $childRoleData) {
                if (! in_array($childRole, $childRoleNames)) {
                    $childRoleNames[] = $childRole;
                    $childRolesArray[] = $childRoleData;
                }
            }
        }

        if ($onlyName === false) {
            return $childRolesArray;
        }

        return $childRoleNames;
    }

    public function haveAllPermissions()
    {
        return Yii::$app->user->identity && (in_array(Yii::$app->user->identity->username, $this->rootUsers) || Yii::$app->user->identity->isAdmin);
    }
}
