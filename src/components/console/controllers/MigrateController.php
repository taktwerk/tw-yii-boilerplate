<?php
namespace taktwerk\yiiboilerplate\components\console\controllers;

/**
 *
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
use yii\console\controllers\MigrateController as BaseMigrationController;
use yii\di\Instance;
use yii\helpers\Console;
use yii\db\Connection;
use yii\helpers\Inflector;

class MigrateController extends BaseMigrationController
{

    /**
     * {@inheritdoc}
     */
    public $generatorTemplateFiles = [
        'create_table' => '@vendor/taktwerk/yii-boilerplate/src/views/migration/createTableMigration.php',
        'drop_table' => '@vendor/taktwerk/yii-boilerplate/src/views/migration/dropTableMigration.php',
        'add_column' => '@vendor/taktwerk/yii-boilerplate/src/views/migration/addColumnMigration.php',
        'drop_column' => '@vendor/taktwerk/yii-boilerplate/src/views/migration/dropColumnMigration.php',
        'create_junction' => '@vendor/taktwerk/yii-boilerplate/src/views/migration/createTableMigration.php',
        'alter_column'=>'@vendor/taktwerk/yii-boilerplate/src/views/migration/alterColumnMigration.php',
        'drop_foreign_keys'=>'@vendor/taktwerk/yii-boilerplate/src/views/migration/dropForeignKeys.php',
        'rename_column'=>'@vendor/taktwerk/yii-boilerplate/src/views/migration/renameColumnMigration.php',
    ];
    /**
     * @var array column definition strings used for creating the down migration code when altering a column.
     *
     * The format of each definition is `COLUMN_NAME:COLUMN_TYPE:COLUMN_DECORATOR`. Delimiter is `,`.
     * For example, `--fields="name:string(12):notNull:unique"`
     * produces a string column of size 12 which is not null and unique values.
     *
     * @since 2.0.16
     */
    public $oldFields = [
        'from-db'
    ];
    /**
     * {@inheritdoc}
     */
    public function options($actionID)
    {
        return array_merge(
            parent::options($actionID),
            ['migrationTable', 'db'], // global for all actions
            $actionID === 'create'
            ? ['templateFile', 'fields', 'useTablePrefix', 'comment', 'oldFields']
            : []
            );
    }
   /**
     * Override function splitFieldIntoChunks($field) to add json datatype comment string in migration
     *
     * @param string $field
     * @return string[]|false
     */
    protected function splitFieldIntoChunks($field)
    {
        $hasDoubleQuotes = false;
        preg_match_all('/:comment\(.*?.*?\)/', $field, $matchesReplace);
       
        if (isset($matchesReplace[0][0])) {
            $field = str_replace($matchesReplace[0][0], "", $field);
        }
        preg_match_all('/defaultValue\(.*?:.*?\)/', $field, $matches);
        if (isset($matches[0][0])) {
            $hasDoubleQuotes = true;
            $originalDefaultValue = $matches[0][0];
            $defaultValue = str_replace(':', '{{colon}}', $originalDefaultValue);
            $field = str_replace($originalDefaultValue, $defaultValue, $field);
        }
        
        $chunks = preg_split('/\s?:\s?/', $field);
        
        if (is_array($chunks) && $hasDoubleQuotes) {
            foreach ($chunks as $key => $chunk) {
                $chunks[$key] = str_replace($defaultValue, $originalDefaultValue, $chunk);
            }
        }
        if (isset($matchesReplace[0][0])) {
            $comment = ltrim($matchesReplace[0][0], ':');
            array_push($chunks, $comment);
        }
        return $chunks;
    }
    /**
     * {@inheritdoc}
     * @since 2.0.7
     */
    protected function parseFields()
    {
        $fields = [];
        $foreignKeys = [];
        $indexes = [];
        foreach ($this->fields as $index => $field) {
            $chunks = $this->splitFieldIntoChunks($field);
            $property = array_shift($chunks);
            
            foreach ($chunks as $i => &$chunk) {
                if (strncmp($chunk, 'foreignKey', 10) === 0) {
                    preg_match('/foreignKey\((\w*)\s?(\w*)\)/', $chunk, $matches);
                    $foreignKeys[$property] = [
                        'table' => isset($matches[1])
                        ? $matches[1]
                        : preg_replace('/_id$/', '', $property),
                        'column' => !empty($matches[2])
                        ? $matches[2]
                        : null,
                    ];
                    
                    unset($chunks[$i]);
                    continue;
                }
                if (strncmp($chunk, 'createIndex', 11) === 0) {
                    preg_match('/createIndex\((\w*)\s?(\w*)\)/', $chunk, $matches);
                    unset($matches[0]);
                    
                    $indexes[$property] = [
                        'columns' => array_values($matches),
                    ];
                    
                    unset($chunks[$i]);
                    continue;
                }
                if (!preg_match('/^(.+?)\(([^(]+)\)$/', $chunk)) {
                    $chunk .= '()';
                }
            }
            $fields[] = [
                'property' => $property,
                'decorators' => implode('->', $chunks),
            ];
        }
        
        return [
            'fields' => $fields,
            'foreignKeys' => $foreignKeys,
            'indexes' => $indexes
        ];
    }
    /**
     * {@inheritdoc}
     * @since 2.0.8
     */
    protected function generateMigrationSourceCode($params)
    {
        $parsedFields = $this->parseFields($this->fields);
        
        $fields = $parsedFields['fields'];
        $foreignKeys = $parsedFields['foreignKeys'];
        $indexes = $parsedFields['indexes'];
        $oldFields = [];
        
        $name = $params['name'];
        
        $templateFile = $this->templateFile;
        $table = null;
        $newName = null;
        $oldName = null;
        if (preg_match('/^create_junction(?:_table_for_|_for_|_)(.+)_and_(.+)_tables?$/', $name, $matches)) {
            $templateFile = $this->generatorTemplateFiles['create_junction'];
            $firstTable = $matches[1];
            $secondTable = $matches[2];
            
            $fields = array_merge(
                [
                    [
                        'property' => $firstTable . '_id',
                        'decorators' => 'integer()',
                    ],
                    [
                        'property' => $secondTable . '_id',
                        'decorators' => 'integer()',
                    ],
                ],
                $fields,
                [
                    [
                        'property' => 'PRIMARY KEY(' .
                        $firstTable . '_id, ' .
                        $secondTable . '_id)',
                    ],
                ]
                );
            
            $foreignKeys[$firstTable . '_id']['table'] = $firstTable;
            $foreignKeys[$secondTable . '_id']['table'] = $secondTable;
            $foreignKeys[$firstTable . '_id']['column'] = null;
            $foreignKeys[$secondTable . '_id']['column'] = null;
            $table = $firstTable . '_' . $secondTable;
        } elseif (preg_match('/^add_(.+)_columns?_to_(.+)_table$/', $name, $matches)) {
            $templateFile = $this->generatorTemplateFiles['add_column'];
            
            $table = $matches[2];
        } elseif (preg_match('/^drop_(.+)_columns?_from_(.+)_table$/', $name, $matches)) {
            $templateFile = $this->generatorTemplateFiles['drop_column'];
            $table = $matches[2];
        } elseif (preg_match('/^drop_(.+)_foreign_keys?_from_(.+)_table$/', $name, $matches)) {
            $templateFile = $this->generatorTemplateFiles['drop_foreign_keys'];
            $table = $matches[2];
        } elseif (preg_match('/^create_(.+)_table$/', $name, $matches)) {
            $this->addDefaultPrimaryKey($fields);
            $templateFile = $this->generatorTemplateFiles['create_table'];
            $table = $matches[1];
        } elseif (preg_match('/^drop_(.+)_table$/', $name, $matches)) {
            $this->addDefaultPrimaryKey($fields);
            $templateFile = $this->generatorTemplateFiles['drop_table'];
            $table = $matches[1];
        } elseif (preg_match('/^alter_(.+)_columns?_of_(.+)_table$/', $name, $matches)) {
            $templateFile = $this->generatorTemplateFiles['alter_column'];
            $table = $matches[2];
            if (count($this->oldFields) == 1 && $this->oldFields[0] == 'from-db') {
                $oldFields = $this->getOldFieldsFromDatabase($table, $fields);
            }
            else {
                $fieldsTemp = $this->fields;
                
                $this->fields = $this->oldFields;
                $parsedOldFields = $this->parseFields($this->oldFields);
                if($parsedOldFields != []){
                    $oldFields = $parsedOldFields['fields'];
                }
                
                $this->fields = $fieldsTemp;
            }
        }elseif (preg_match('/^rename_(.+)_column_to_(.+)_of_(.+)_table$/', $name, $matches)) {
            $templateFile = $this->generatorTemplateFiles['rename_column'];
            $table = $matches[3];
            $oldName = $matches[1];
            $newName = $matches[2];
        }
        
        $foreignKeys = $this->collectForeignKeysMetadata($foreignKeys, $table);
        
        return $this->renderFile(\Yii::getAlias($templateFile), array_merge($params, [
            'table' => $this->generateTableName($table),
            'fields' => $fields,
            'oldFields' => $oldFields,
            'foreignKeys' => $foreignKeys,
            'indexes' => $indexes,
            'tableComment' => $this->comment,
            'newName'=>$newName,
            'oldName'=>$oldName
        ]));
    }
    /**
     * Collects information about foreign keys
     * @param $foreignKeys
     * @param $table
     * @return array
     * @since 2.0.16
     */
    protected function collectForeignKeysMetadata($foreignKeys, $table)
    {
        foreach ($foreignKeys as $column => $foreignKey) {
            $relatedColumn = $foreignKey['column'];
            $relatedTable = $foreignKey['table'];
            // Since 2.0.11 if related column name is not specified,
            // we're trying to get it from table schema
            // @see https://github.com/yiisoft/yii2/issues/12748
            if ($relatedColumn === null) {
                $relatedColumnFromTable = $this->getRelatedColumnFromTableSchema($relatedTable, $column);
                $relatedColumn = $relatedColumnFromTable === false ? 'id' : $relatedColumnFromTable;
            }
            $idx = $this->generateTableName("idx-$table-$column");
            $idx = str_replace(['{{%','}}'],['',''] , $idx);
            $fk = $this->generateTableName("fk-$table-$column");
            $fk = str_replace(['{{%','}}'],['',''] , $fk);
            if(strlen($idx)>63){
                //limiting the max-length of the index name to around 60-64 characters
                $idx = substr($idx, 0, 55).'_'.substr(str_shuffle(str_repeat($x='abcdefghijklmnopqrstuvwxyz', ceil(5/strlen($x)) )),1,5);
            }
            if(strlen($fk)>63){
                //limiting the max-length of the foreign key name to around 60-64 characters
                $fk = substr($fk, 0, 55).'_'.substr(str_shuffle(str_repeat($x='abcdefghijklmnopqrstuvwxyz', ceil(5/strlen($x)) )),1,5);
            }
            $foreignKeys[$column] = [
                'idx' => $idx,
                'fk' => $fk,
                'relatedTable' => $this->generateTableName($relatedTable),
                'relatedColumn' => $relatedColumn,
            ];
        }
        
        return $foreignKeys;
    }
    /**
     * Reads from the db schema the field definitions that will be altered
     *
     * @param string $table the table name for the migration
     * @param array $newFields the fields we want the definitions for
     *
     * @throws \InvalidArgumentException
     * @return array the fields definitions from the db schema
     * @since 2.0.16
     */
    protected function getOldFieldsFromDatabase($table, $newfields)
    {
        try {
            $this->db = Instance::ensure($this->db, Connection::className());
            $tableSchema = $this->db->getTableSchema($table);
            if ($tableSchema === null) {
                throw new \InvalidArgumentException("$table not found in database schema");
            }
            $ret = [];
            foreach ($newfields as $newfield) {
                $column = $tableSchema->getColumn($newfield['property']);
                if ($column == null) {
                   // throw new \InvalidArgumentException($newfield['property'] . " column not found in schema of table $table");
                }else{
                    $ret[] = [
                        'property' => $newfield['property'],
                        'decorators' => $this->genDecorators($column)
                    ];
                }
            }
            return $ret;
        } catch (\ReflectionException $e) {
            $this->stdout("Cannot initialize database component to try reading old table definitions\n", Console::FG_YELLOW);
            throw $e;
        }
    }
    
    /**
     * Generates decorators from a column schema
     * @param \yii\db\ColumnSchema $column
     * @since 2.0.16
     */
    protected function genDecorators($column)
    {
        $decorators = $column->type . "(";
        if ($column->size != 0 ) {
            $decorators .= intval($column->size);
            
            if ($column->precision != 0 || $column->scale != 0) {
                $decorators .= $column->precision != 0 ? intval($column->precision) : intval($column->scale);
            }
        }
        
        $decorators .= ")";
        $decorators .= $column->allowNull ? "->null()" : "->notNull()";
        $decorators .= $column->unsigned ? "->unsigned()" : '';
        $decorators .= $column->defaultValue != '' ? "->defaultValue('" . $column->defaultValue . "')" : '';
        $decorators .= $column->comment != '' ? "->comment('" . $column->comment . "')" : '';
        return $decorators;
    }
    /**
     * Gets related column from table schema
     * @param $relatedTable
     * @param $column
     * @return mixed primary key or false if error
     * @since 2.0.16
     */
    protected function getRelatedColumnFromTableSchema($relatedTable, $column)
    {
        try {
            $this->db = Instance::ensure($this->db, Connection::className());
            $relatedTableSchema = $this->db->getTableSchema($relatedTable);
            if ($relatedTableSchema === null) {
                return false;
            }
            $primaryKeyCount = count($relatedTableSchema->primaryKey);
            if ($primaryKeyCount === 1) {
                return $relatedTableSchema->primaryKey[0];
            }
            
            if ($primaryKeyCount > 1) {
                $this->stdout("Related table for field \"{$column}\" exists, but primary key is composite. Default name \"id\" will be used for related field\n",
                Console::FG_YELLOW);
            } elseif ($primaryKeyCount === 0) {
                $this->stdout("Related table for field \"{$column}\" exists, but does not have a primary key. Default name \"id\" will be used for related field.\n",
                Console::FG_YELLOW);
            }
            
        } catch (\ReflectionException $e) {
            $this->stdout("Cannot initialize database component to try reading referenced table schema for field \"{$column}\". Default name \"id\" will be used for related field.\n",
            Console::FG_YELLOW);
        }
        
        return false;
    }
}
