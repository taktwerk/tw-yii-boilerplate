<?php

namespace taktwerk\yiiboilerplate\components;

use yii\web\ErrorHandler as BaseErrorHandler;

class WebErrorHandler extends BaseErrorHandler {

    /**
     * @var string
     */
    public $error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT;

    /**
     * Register this error handler
     */
    public function register()
    {
        parent::register();
        if (!is_null($this->error_reporting)) ini_set('error_reporting', $this->error_reporting);

    }

}