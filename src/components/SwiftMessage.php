<?php

namespace taktwerk\yiiboilerplate\components;

use yii\swiftmailer\Message;

class SwiftMessage extends Message
{
    protected $prependTestContent = [];
    public function setTo($to)
    {
        $toList = is_array($to)?$to:[$to];
        $bypassAppTestmail = false;
        if(getenv('APP_BYPASS_TEST_MAIL')){
            $testMails = array_map('trim', explode(',', getenv('APP_BYPASS_TEST_MAIL')));
            $byPassMails = [];
            foreach($toList as $k=>$v){
                if((is_string($k) && in_array($k, $testMails)) || in_array("$v", $testMails)){
                    $bypassAppTestmail = true;
                    $byPassMails[$k] = $v;
                }
            }
            if($bypassAppTestmail){
                $toList = $byPassMails;
            }
        }
        if($bypassAppTestmail==false && getenv('APP_TEST_EMAIL')){
            $this->prependTestContent[] = "This email is sent to test receiver(s): ".(getenv('APP_TEST_EMAIL'));
            $this->prependTestContent[] = "Original receiver(s) would be: ". (is_array($toList)?implode(', ', $toList):$toList);
            return parent::setTo(explode(',', getenv('APP_TEST_EMAIL')));
        }

        return parent::setTo($toList);
    }

    public function setFrom($from=null)
    {
        if(getenv('APP_FROM_EMAIL')){
            return parent::setFrom(getenv('APP_FROM_EMAIL'));
        }
        if(empty($from) && empty(getenv('APP_FROM_EMAIL'))){
            throw new \Exception(\Yii::t('twbp','Set from address'));
        }
        return parent::setFrom($from);
    }
    protected function setBody($body, $contentType){
        if(count($this->prependTestContent)>0 && getenv('APP_TEST_EMAIL')){
            $body = implode((($contentType==='text/plain')?PHP_EOL:'<br>'),$this->prependTestContent).(($contentType==='text/plain')?(PHP_EOL.PHP_EOL):'<br><br>') . $body;
        }
        parent::setBody($body, $contentType);
    }

}