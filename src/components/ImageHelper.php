<?php
namespace taktwerk\yiiboilerplate\components;

use League\Flysystem\AdapterInterface;
use Yii;
use Exception;
use yii\base\Component;
use yii\base\Module;
use yii\caching\TagDependency;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\web\Controller;
use Imagine\Image\Box;
use yii\helpers\Html;

class ImageHelper extends Component
{
/**
     * publish a file to public bucket
     *
     * @param
     *            $path
     * @param string $bucket
     * @return string
     */
    public static function getPublicFileUrl($path, $bucket = 'fs')
    {
        // calculate cache
        $cache_path = ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\FileHelper::class)::fileCachePath($path, $bucket);
        
        // read from cache
        if (! Yii::$app->fs_assetsprod->has($cache_path)) {
            // load
            $cache_file_content = Yii::$app->$bucket->read($path);
            Yii::$app->fs_assetsprod->put($cache_path, $cache_file_content);
        }
        
        return self::getPublicUrl($path, $cache_path);
    }
    /**
     * get public url depending on CDN setting
     *
     * @param
     *            $path
     * @param
     *            $cache_path
     * @return string
     */
    public static function getPublicUrl($path, $cache_path)
    {
        if (getenv('CDN_ENABLED')) {
            $url = getenv('CDN_URL') . implode('/', array_map('rawurlencode', explode('/', $cache_path)));
        } else {
            $url = '/site/image?path=' . implode('/', array_map('rawurlencode', explode('/', $path)));
            // Remap parameters so that they are properly read
            $url = str_replace([
                '%26',
                '%3D'
            ], [
                '&',
                '='
            ], $url);
            $url = Url::to([
                $url
            ]); // Force language to avoid redirects
        }
        $timestamp = '';
        if (($timestamp = @filemtime(__FILE__)) > 0) {
            if (strpos($url, '?') !== false) {
                $url = $url . '&v=' . $timestamp;
            } else {
                $url = $url . '?v=' . $timestamp;
            }
        }
        return $url;
    }
    /**
     * get public url of an image with optional CDN prefix
     *
     * @param
     *            $path
     * @param null $options
     * @return string
     */
    public static function getImageUrl($path, $options = null)
    {
        $options_part = '';
        if ($options) {
            foreach ($options as $key => $value) {
                $options_part .= '&' . $key . '=' . $value;
            }
        }
        $cache_path = self::processImage($path, $options);
        
        return self::getPublicUrl($path . $options_part, $cache_path);
    }
    /**
     * Get (or sets) the modification time from the s3 and saves it in cache for future reference
     *
     * @param
     *            $key
     * @param
     *            $path
     * @param string $bucket
     * @return mixed
     */
    public static function getFileCachedModificationTime($key, $path, $bucket = 'fs')
    {
        $modificationTime = Yii::$app->fs_cache->get($key);
        if ($modificationTime === false) {
            try {
                $modificationTime = Yii::$app->$bucket->getTimestamp($path);
                self::setFileCachedModificationTime($key, $modificationTime);
            } catch (Exception $e) {
                $modificationTime = 0;
            }
        }
        return $modificationTime;
    }

    /**
     * Set the cached modification time for future reference
     *
     * @param
     *            $key
     * @param
     *            $modificationTime
     */
    public static function setFileCachedModificationTime($key, $modificationTime)
    {
        Yii::$app->fs_cache->set($key, $modificationTime);
    }

    /**
     * Creates a temprary file of the Image and returns the path
     *
     * @param string $path
     *            Path of file in the Flysystem
     * @param array $options
     *            Image options like size
     * @return boolean $getCompressImgPathIfAvailable If true return the compressed Image Path of the original image
     */
    public static function getImageTmpPath($path, $options = [], $getCompressImgPathIfAvailable = true)
    {
        if ($path) {
            if ($getCompressImgPathIfAvailable) {
                $compressImgPath = ImageHelper::getCompressedImageTmpPath($path, $options);
                if ($compressImgPath) {
                    Yii::$app->params['clean_up'][] = $compressImgPath;
                    return $compressImgPath;
            }
        }
        $preview_file = Helper::getLocalTempFile($path);
        Yii::$app->params['clean_up'][] = $preview_file;
        return $preview_file;
        }
        return null;
    }
    /**
     *
     * @param string $path Path of file in the Flysystem
     * @param array $options Image options like size
     * @return string|NULL
    */
    public static function getCompressedImageTmpPath($path, $options = []){
        $cache_path = self::processImage($path, $options);
        if ($cache_path) {
            $cache_image_content = Yii::$app->fs_assetsprod->read($cache_path);
            @mkdir(Yii::getAlias('@runtime') . '/cache');
            $tmp_path = Yii::getAlias('@runtime') . '/cache/' . md5($cache_image_content);
            if (file_put_contents($tmp_path, $cache_image_content)){
                return $tmp_path;
            }
        }
        return null;
    }

    /**
     * process image with options and show it or return its cache path
     *
     * @param
     *            $path
     * @param array $options
     * @param bool $show
     * @return bool|string
     */
    public static function processImage($path, $options = [], $show = false)
    {
        ini_set('memory_limit', '-1');
        $body = self::getMethodBody(self::class, 'processImage');
        $changeCheck = md5($body);
        $isMethodUnchanged = Yii::$app->fs_cache->get($changeCheck);
        if (! $isMethodUnchanged) {
            Yii::$app->fs_cache->set($changeCheck, 1, 0);
        }
        // defaults
        $bucket = 'fs'; // filesystem component: fs, fs_assetsprod, ...
        $width = null;
        $height = null; // if only one of widht|height is set, the other will be calculated by original ratio
        $format = null;
        $quality = 80;
        $resolution = 120;
        $ratio = null; // width x height, 4_3 (400 width x 300 height)
        $mode = 'landscape'; // or portrait
        $defaultWidth = 1920;
        $defaultHeight = 1080;

        // handle options
        if (strpos($path, '/') === 0)
            $path = substr($path, 1);
            if (isset($options['bucket']))
                $bucket = $options['bucket'];
            if (isset($options['width']))
                $width = $options['width'];
            if (isset($options['height']))
                $height = $options['height'];
            if (isset($options['format']))
                $format = $options['format'];
            if (isset($options['resolution']))
                $resolution = $options['resolution'];
            if (isset($options['ratio']))
                $ratio = $options['ratio'];
            if (isset($options['mode']))
                $mode = strtolower($options['mode']);

            // Manipulate dimensions
            $isLandscape = $mode == 'landscape';
            if (! empty($ratio)) {
                $ratios = explode('_', $ratio);
                if (count($ratios) == 2) {
                    if(empty($width) && empty($height)) {
                        $width = $defaultWidth;
                    }

                    if (empty($width)) {
                        $width = floor($height / $ratios[$isLandscape ? 1 : 0] * $ratios[$isLandscape ? 0 : 1]);
                    } elseif (empty($height)) {
                        $height = floor($width / $ratios[$isLandscape ? 0 : 1] * $ratios[$isLandscape ? 1 : 0]);
                    }
                }
            }

            if (empty($width)) {
                $width = $defaultWidth;
            }

            if (empty($height)) {
                $height = $defaultHeight;
            }

            $clearPath = strpos($path, '&') !== false ? substr($path, 0, strpos($path, '&')) : $path;
            
            $original_image_path = $clearPath;
            $asset_image_path = 'image-cache/' . $path;
            
            $ext = pathinfo($original_image_path, PATHINFO_EXTENSION);

            // convert file format
            if ($format) {
                $asset_image_path = str_replace($ext, $format, $asset_image_path);
            } // set original file format
            else {
                $format = $ext;
            }
            // Cast format to lower for Image library
            $format = strtolower($format);

            // Recast the path to not have options (
            $asset_image_path = 'image-cache/' . $clearPath;

            // Add a slash at the beginning of the url to help get the proper file
            $modificationCachePath = $original_image_path;
            if ($modificationCachePath[0] != '/') {
                $modificationCachePath = '/' . $modificationCachePath;
            }

            // Get the last timestamp of the picture
            $modificationCacheKey = md5($modificationCachePath . '_timestamp');
            $modification_time = self::getFileCachedModificationTime($modificationCacheKey, $original_image_path, $bucket);

            $cache_key = md5(serialize(compact('modification_time', 'path', 'bucket', 'width', 'height', 'format', 'quality')));
            $cache_path = dirname($asset_image_path) . '/' . $cache_key . '/' . basename($asset_image_path);

            // Check if image is cached (either in the local cache or on the prod assets server)
            $hasCacheKey = md5($cache_path . '_cachepath');
            $hasCache = Yii::$app->fs_cache->get($hasCacheKey);

            if($hasCache===true){
                $hasCache = Yii::$app->fs_assetsprod->has($cache_path);
            }
            if ($hasCache === false) {
                $hasCache = Yii::$app->fs_assetsprod->has($cache_path);
                Yii::$app->fs_cache->set($hasCacheKey, $hasCache, 0, new TagDependency([
                    'tags' => $modificationCacheKey
                ]));
            }

            if (isset($options['cache']) && $options['cache'] === "false") {
                if ($hasCache) {
                    $hasCache = false;
                    Yii::$app->fs_cache->delete($hasCacheKey);
                }
            }

            // read from cache
            if ($isMethodUnchanged && $show && $hasCache) {
                // load

                $cache_image_content = Yii::$app->fs_assetsprod->read($cache_path);
                $image = Image::getImagine()->load($cache_image_content);
            } elseif (! $hasCache && ($width || $height) || $show) {

                // Only process the image if we are resizing it and it isn't cached
                if (! Yii::$app->$bucket->has($original_image_path)) {
                    return false;
                }

                // Read from cache and write it localy (needed for resizing)
                @mkdir(Yii::getAlias('@runtime') . '/cache');
                $tmp_path = Yii::getAlias('@runtime') . '/cache/' . md5($original_image_path);
                file_put_contents($tmp_path, Yii::$app->$bucket->read($original_image_path));

                $rotateVal = 0;
                try {
                    if (function_exists('exif_read_data')) {
                        $exifData = @exif_read_data($tmp_path);
                        if (isset($exifData['Orientation'])) {
                            $orientation = (int) $exifData['Orientation'];

                            switch ($orientation) {
                                case 8:
                                    $rotateVal = - 90;
                                    break;
                                case 3:
                                    $rotateVal = 180;
                                    break;
                                case 6:
                                    $rotateVal = 90;
                                    break;
                            }
                        }
                    }
                } catch (\Exception $e) {
                    \Yii::error($e->getTraceAsString());
                }
                // resize
                // $image = Image::thumbnail($tmp_path, $width, $height); // Todo: mode?
                //print_r(compact('width','height','mode','ratio'));
                $image = Image::getImagine()->open($tmp_path)->thumbnail(new Box($width, $height));
                if ($rotateVal !== 0) {
                    $image->rotate($rotateVal);
                }
                // ->rotate($angle);


                // cleanup tmp file
                if (file_exists($tmp_path)) {
                    unlink($tmp_path);
                }

                // save image to the cache
                if ($image) {
                    Yii::$app->fs_assetsprod->put($cache_path, $image->get($format, [
                        'quality' => $quality
                    ]), [
                        'visibility' => AdapterInterface::VISIBILITY_PUBLIC
                    ]);
                }

                //cleanup imagick resources
                if($image && !$show && method_exists($image,'getImagick')){
                    $imagic = $image->getImagick();
                    if(method_exists($imagic,'clear')){
                        $imagic->clear();
                    }
                }
            }

            // Output image if we have it and it's required
            if (isset($image) && $show) {
                // Cache image for 60 days for the browser. A file contains the timestamp when it is uploaded so we'll
                // never have false positives.
                $expires = 60 * 60 * 24 * 60; // 60 days
                
                header('Pragma: public');
                header('Cache-Control: max-age=' . $expires);
                header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');
                if(isset($options['lastModified'])){
                header("Last-Modified: ".$options['lastModified']);
                }
                if(isset($options['etag'])){
                    header("Etag: ".$options['etag']);
                }
                $image->show($format, [
                    'quality' => $quality
                ]);

                if($image && $show && method_exists($image,'getImagick')){
                    $imagic = $image->getImagick();
                    if(method_exists($imagic,'clear')){
                        $imagic->clear();
                    }
                }
                die();
            }
            return $cache_path;
    }
    /**
     * provides body of the method in string
     *
     * @param
     *            $class
     * @param
     *            $method
     * @return string
     */
    private static function getMethodBody($class, $method, $removeWhiteSpace = true)
    {
        try {
            $func = new \ReflectionMethod($class, $method);
            $filename = $func->getFileName();
            $start_line = $func->getStartLine() - 1;
            $end_line = $func->getEndLine();
            $length = $end_line - $start_line;
            
            $source = file($filename);
            $body = implode("", array_slice($source, $start_line, $length));
            if ($removeWhiteSpace) {
                $body = preg_replace('/\s+/', '', $body);
            }
        } catch (\Exception $e) {
            return '';
        }
        return $body;
    }
}