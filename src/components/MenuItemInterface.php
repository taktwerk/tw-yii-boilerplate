<?php
namespace taktwerk\yiiboilerplate\components;

Interface MenuItemInterface
{

    public function getMenuItems();
}