<?php
namespace taktwerk\yiiboilerplate\components;

use yii\base\Component;
use yii\caching\CacheInterface;

class TwFalseCache extends Component implements CacheInterface
{
    public function buildKey($key){
        return 'abc';
    }
    public function get($key){
        return false;
    }
    public function offsetGet($key)
    {
        return $this->get($key);
    }
    public function exists($key){
        return false;
    }
    public function offsetExists($key)
    {
        return false;
    }
    public function multiGet($keys){
        $list =[];
        foreach($keys as $k){
            $list[$k]=false;
        }
        return $list;
    }
    public function set($key, $value, $duration = null, $dependency = null){
        return true;
    }
    public function offsetSet($key, $value)
    {
        $this->set($key, $value);
    }
    public function offsetUnset($key)
    {
        $this->delete($key);
    }
    public function multiSet($items, $duration = 0, $dependency = null){
        return [];
    }
    public function add($key, $value, $duration = 0, $dependency = null){
        return true;
    }
    public function multiAdd($items, $duration = 0, $dependency = null){
        return [];
    }
    public function delete($key){
        return true;
    }
    public function flush(){
        return true;
    }
    public function getOrSet($key, $callable, $duration = null, $dependency = null){
        $value = call_user_func($callable, $this);
        return $value;
    }
}
