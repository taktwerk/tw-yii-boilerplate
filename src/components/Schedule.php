<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 6/5/2017
 * Time: 9:15 AM
 */

namespace taktwerk\yiiboilerplate\components;

use omnilight\scheduling\Event;
use Yii;

class Schedule extends \omnilight\scheduling\Schedule
{
    /**
     * Add a new cli command event to the schedule.
     *
     * @param  string  $command
     * @return Event
     */
    public function command($command)
    {
        $exec = PHP_BINARY . ' ' . $this->cliScriptName . ' ' . $command;
        // Checking if we run under Windows
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $exec = '"' . PHP_BINARY . '"' . ' ' . '"' . $this->cliScriptName . '"' . ' ' . $command;
        }
        return $this->exec($exec);
    }

    /* needs to be called in project schedule config: $schedule->getAllModuleSchedules(); */
    public function getAllModuleSchedules(){

        /** @var Schedule $schedule */
        $schedule = Yii::$app->get('schedule');
        $all_modules = [];
        $commands = [];
        foreach (array_keys(Yii::$app->getModules(false)) as $module){
            if(isset(Yii::$app->params['modules_activation'][$module]) && !Yii::$app->params['modules_activation'][$module]){
                continue;
            }
            $all_modules[] = $module;
            $file_path = Yii::getAlias("@app/modules/$module/config/schedule.php");
            $bp_file_path = Yii::getAlias("@taktwerk-boilerplate/modules/$module/config/schedule.php");
            if(file_exists($file_path)){
                include $file_path;
            }elseif (file_exists($bp_file_path)){
                include $bp_file_path;
            }
        }
        return true;
    }
}