<?php
namespace taktwerk\yiiboilerplate\components;

use League\Flysystem\AdapterInterface;
use taktwerk\yiiboilerplate\assets\TwAsset;
use taktwerk\yiiboilerplate\modules\customer\models\Client;
use Yii;
use yii\base\Component;
use yii\base\InvalidRouteException;
use yii\base\Module;
use yii\caching\TagDependency;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\web\Controller;
use Imagine\Image\Box;
use ZipArchive;
use yii\db\ActiveRecord;

class Helper extends ImageHelper
{

    /**
     *
     * @return string
     */
    public static function version()
    {
        return Yii::t('app', getenv('APP_TITLE')) . ' ' . APP_VERSION . ' (' . APP_HASH . ')';
    }

    /**
     * This provides the all the menu items of all the modules which implements MenuItemInterface
     * 
     * @return array
     */
    public static function getModulesMenuItems()
    {
        $items = [];
        foreach (\Yii::$app->modules as $key => $m) {
            try {
                $moduleInstance = $m;
                if (is_array($m) && isset($m['class'])) {
                    $moduleInstance = \Yii::$app->getModule($key);
                }
            if ($moduleInstance instanceof MenuItemInterface) {
                if (! empty($moduleInstance->getMenuItems())) {
                    $items = array_merge($items, $moduleInstance->getMenuItems());
                }
            }
            }catch(\Exception $e){
            }
        }
        return $items;
    }

    /**
     * check Application
     */
    public static function checkApplication()
    {
        if (\Yii::$app->user->can('Admin')) {
            self::checkPassword(getenv('APP_ADMIN_PASSWORD'));
        }
    }
    /**
     * Checks if the server has the program/command passed to run
     * @param
     *            $command
     *            @return boolean
     */
    public static function shellProgramExists($command){
        $windows = strpos(PHP_OS, 'WIN') === 0;
        $test = $windows ? 'where' : 'command -v';
        return is_executable(trim(shell_exec("$test $command")));
    }

    /**
     * Password check
     * Based upon http://stackoverflow.com/a/10753064.
     *
     * @param
     *            $pwd
     */
    private static function checkPassword($pwd)
    {
        $errors = [];
        
        if (strlen($pwd) < 8) {
            $errors[] = 'Password too short!';
        }
        
        if (! preg_match('#[0-9]+#', $pwd)) {
            $errors[] = 'Password must include at least one number!';
        }
        
        if (! preg_match('#[a-zA-Z]+#', $pwd)) {
            $errors[] = 'Password must include at least one letter!';
        }
        
        if (count($errors) > 0) {
            $msg = implode('<br/>', $errors);
            \Yii::$app->session->addFlash('danger', "Application admin password from environment setting is not strong enough.<br/><i>{$msg}</i>");
        }
    }
    public static function removeTimestampFromFileName($baseFileName,$seprator = '_'){
        $parts = explode($seprator, $baseFileName, 2);
        $checkNumeric = $parts[0];
        if(is_numeric($checkNumeric) && strlen($checkNumeric)>7 && strlen($checkNumeric)<12){
            $tmp = $parts[1];
            if($tmp){
                $baseFileName = $tmp;
            }
        }
        return $baseFileName;
    }
    /**
     * get public url of file by path
     *
     * @param
     *            $component
     * @param
     *            $fileSystem
     * @param
     *            $path
     * @return string
     */
    public static function getFile($component, $fileSystem, $path,$options=[])
    {
        if (isset(\Yii::$app->controllerMap[$component]) && \Yii::$app->controllerMap[$component]['roots']) {
            foreach (\Yii::$app->controllerMap[$component]['roots'] as $key => $root) {
                if ($root['component'] === $fileSystem) {
                    $hash = rtrim(strtr(base64_encode($path), '+/=', '-_.'), '.');
                    $volume = 'fls' . ($key + 1) . '_' . $hash;
                    $url = '';
                    if (\Yii::$app->controllerMap[$component]['urlWithLanguage']) {
                        $url .= '/' . Yii::$app->language;
                    }
                    $extraParams = '';
                    //prd($options,$path);
                    if(isset($options['etag']) && $options['etag']){
                        $extraParams = '&etag='.urlencode($options['etag']);
                    }
                    if(isset($options['lastModified']) && $options['lastModified']){
                        $extraParams .= '&lastModified='.urlencode($options['lastModified']);
                    }
                    return  $url . '/' . $component . '/connect?cmd=file&target=' . $volume.$extraParams;
                }
            }
        }
    }

    /**
     * Get a local temp file of a flysystem file, for uses where a local file has to be read
     * Best practise: Unlink them after use
     *
     * @todo : If source flysystem is local it could directly read from there
     * @param
     *            $path
     * @param string $bucket
     * @throws \yii\base\Exception
     */
    public static function getLocalTempFile($path, $bucket = 'fs', $extension = '')
    {
        $file_content = null;
        try {
            $file_content = Yii::$app->$bucket->read($path);
        } catch (\Exception $exception) {
            Yii::error('File not found: ' . $path);
        }
        if ($file_content) {
            $tmp_folder = \Yii::getAlias('@runtime/tmp/');
            \yii\helpers\FileHelper::createDirectory($tmp_folder, $mode = 0775, $recursive = true);
            
            $tmp_file = $tmp_folder . md5($path);
            if ($extension) {
                $tmp_file .= '.' . $extension;
            }
            if (file_put_contents($tmp_file, $file_content)) {
                return $tmp_file;
            }
        }
        
        return false;
    }

    /**
     * cleanup specific tmp files
     *
     * @param array $cleanup
     */
    public static function cleanupLocalTempFiles($cleanup = [])
    {
        if (is_array($cleanup)) {
            foreach ($cleanup as $file) {
                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }
    }

    /**
     * Remove special charcters from string (filename) so they could be saved safe in every storage component
     *
     * @param string $fileName
     * @return string
     */
    public static function cleanFileName($fileName)
    {
        // Removes special chars leaving only letters and number and -_.,+ .
        return preg_replace('/[^A-Za-z0-9\-\.\+\_\s+]/', '', $fileName);
    }

    /**
     *
     * @param
     *            $format
     * @return string
     */
    public static function convertMomentToPHFormat($format)
    {
        $replacements = [
            'DD' => 'd',
            'MM' => 'm',
            'YYYY' => 'Y',
            'H' => 'H',
            'mm' => 'i',
            'ss' => 's',
        ];
        $phpFormat = strtr($format, $replacements);
        
        return $phpFormat;
    }

    /**
     * Create controller instance
     *
     * @param string $controllerClassName
     * @param string $controllerId
     * @param Module $module
     * @return Controller
     * @throws \yii\base\InvalidConfigException
     */
    public static function createControllerInstance(string $controllerClassName, string $controllerId, Module $module)
    {
        return Yii::createObject($controllerClassName, [
            $controllerId,
            $module
        ]);
    }

    /**
     * Minutes to Human readable time
     *
     * @param $duration string
     * @return string
     */
    public static function convertMinutesToHumanReadableTime($duration)
    {
        $day = floor($duration / 1440);
        $hour = ($day >= 1 ? ' ' : '') . floor(($duration - $day * 1440) / 60);
        $minute = ($hour >= 1 ? ' ' : '') . ($duration - ($day * 1440) - ($hour * 60));
        
        return Yii::t('twbp', '{day}{hour}{minute}', [
            'day' => $day >= 1 ? $day . ' ' . ($day >= 2 ? Yii::t('twbp', 'days') : Yii::t('twbp', 'day')) : '',
            'hour' => $hour >= 1 ? $hour . ' h' : '',
            'minute' => $minute >= 1 ? $minute . ' min' : ''
        ]);
    }


    /**
     * Function to create a zip file for all the files given in the array $all_files using folder as a index and for additional files that are not related to model "Other" index is used.
     * @param $all_files
     * @param $destination_path
     * @param $zip_name
     * @return bool
     * @throws InvalidRouteException
     * @throws \yii\base\Exception
     */
    public static function createZip($all_files, $destination_path, $zip_name){
        $local_path = \Yii::getAlias('@runtime/tmp/zip/');
        $zip_localfile = $local_path . '/' . $zip_name;
        if(!file_exists($local_path)){
            \yii\helpers\FileHelper::createDirectory($local_path,0777,true);
        }
        $zip = new ZipArchive();
        try{
            $zip->open($zip_localfile, ZipArchive::CREATE);
            if(array_key_exists('type',$all_files)){
                $type = $all_files['type'];
            }else{
                $type = null;
            }
            foreach ($all_files as $model_name=>$model_files){
                foreach($model_files as $k => $file_with_number){
                    $zipFiles = [];
                    $dirName = $model_name;
                    if(is_array($file_with_number)){
                        $dirName = $dirName.'/'.$k; 
                        foreach($file_with_number as $fname => $f){
                            $zipFiles[$fname] = $f;
                        }
                    }else{
                        $zipFiles[] = $file_with_number;
                    }
                    foreach($zipFiles as $fName => $f){
                        $file = explode('|;;|',$f)[0];
                        $number = str_pad(explode('|;;|',$f)[1], 4, "0", STR_PAD_LEFT);
                        $file_name = "";
                        $base_file_name = basename($file);
                        if(is_string($fName) && !is_numeric($fName)){
                            $base_file_name = $fName;
                        }
                        if($model_name=="Other"){
                            $file_name = $base_file_name;
                            if($number>=1){
                                $file_name = $number."_".$base_file_name;
                            }
                        }else{
                            $file_name = $dirName.'/'.$base_file_name;
                            if($number>=1){
                                $file_name = $dirName.'/'.$number."_".$base_file_name;
                            }
                        }

                        if(Yii::$app->fs_assetsprod->has($file)){
                            $zip->addFromString($file_name,Yii::$app->fs_assetsprod->read($file));
                        }elseif(Yii::$app->fs->has($file)){
                            $zip->addFromString($file_name,Yii::$app->fs->read($file));
                        }else{
                            //
                        }
                    }
                }
            }

            $zip->close();
            if(file_exists($zip_localfile)){
                $zip_content = file_get_contents($zip_localfile);
                Yii::$app->fs->put($destination_path, $zip_content);
                unlink($zip_localfile);
                $result = true;
            }else{
                $result = false;
            }

        }catch (\Exception $exception){
            throw new InvalidRouteException("Cannot create Archive",500);
        }

        return $result;
    }

    /**
     * Remove special characters from string
     *
     * @param string $string
     * @return string|string[]|null
     */
    public static function removeSpecialChars(string $string, $replaceWith='')
    {
        return preg_replace ('/[^\p{L}\p{N}]/u', $replaceWith, $string);
    }

    /**
     * Remove all the special characters and the invalid characters
     * @param string $str
     * @return null|string|string[]
     */
    public static function sanitizeString($str = ''){
        $str = strip_tags($str);
        $str = preg_replace('/[\r\n\t ]+/', ' ', $str);
        $str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
        $str = strtolower($str);
        $str = html_entity_decode( $str, ENT_QUOTES, "utf-8" );
        $str = htmlentities($str, ENT_QUOTES, "utf-8");
        $str = preg_replace("/(&)([a-z])([a-z]+;)/i", ' ', $str);
        $str = preg_replace('/[^A-Za-z0-9\-\.\+\_\s+]/', ' ', $str);
        $str = str_replace(' ', '_', $str);
        $str = preg_replace('/-+/', '-', $str);
        $str = preg_replace('/_+/', '_', $str);
        $str = str_replace('-_', '_', $str);
        $str = str_replace('_-', '_', $str);
        return $str;
    }

    public static function isLocalFileSystem(){

        if(isset(Yii::$app->fs) && stripos(get_class(Yii::$app->fs), 'taktwerk\yiiboilerplate\components\flysystem\LocalFilesystem')!==false){
            return true;
        }else{
            return false;
        }
        if(isset(Yii::$app->fs_assetsprod) && stripos(get_class(Yii::$app->fs_assetsprod), 'taktwerk\yiiboilerplate\components\flysystem\LocalFilesystem')!==false){
            return true;
        }else{
            return false;
        }
        
    }
    
    public static function getDisks(){
        $output = shell_exec('df | grep -vE \'^Filesystem|tmpfs|cdrom|loop\' | awk \'{ print ";"$3 "|" $4 "|" $5 "|" $1 "|" $6 "|" $2}\'');
        return explode(";",$output);
    }

    /**
     * Get logo dynamically based on env variable APP_MODE
     * on mode 0 it shows like today, just taktwerk logos
     * on mode 1 it shows the overwrites from env paths
     * on mode 2 it shows the logos of the clients model
     * For env path logo: Path - @web/customer/
     * @throws \yii\base\InvalidConfigException
     */
    public static function registerWhiteLabelAssets(){

        $assets = self::getWhiteLabelAssets();

        Yii::$app->view->registerLinkTag(['rel' => 'icon', 'href' => $assets['icon']]);

        $variables = <<<CSS
:root {
  --main-logo: url({$assets['logo']});
  --main-icon: url({$assets['icon']});
  --main-bg: url({$assets['bg']});
}
CSS;
        Yii::$app->view->registerCss($variables);
    }

    /**
     * @param null $type 'logo'|'icon'|'bg'
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public static function getWhiteLabelAssets($type = null){
        $baseUrl = Yii::$app->assetManager->getPublishedUrl(Yii::getAlias('@vendor/taktwerk/yii-boilerplate/src/assets/web'));
        if(getenv('APP_MODE')==='1' && strlen(getenv('APP_LOGO'))>=1 && strlen(getenv('APP_ICON'))>=1){
        $main_logo = Yii::$app->urlManager->getHostInfo().'/customer/'.getenv('APP_LOGO');
        $main_icon = Yii::$app->urlManager->getHostInfo().'/customer/'.getenv('APP_ICON');
    }elseif(getenv('APP_MODE')==='2'){

        foreach ( \taktwerk\yiiboilerplate\modules\customer\models\ClientUser::findAll(['user_id' => Yii::$app->getUser()->id]) as $client) {
            $id = $client->client_id;
            $model = Client::findOne($id);
            if($model){
                $main_logo = $model->appTheme("logo");
                $main_icon = $model->appTheme("logo","icon");
                if(strlen($main_logo)>=1 || strlen($main_icon)>=1){
                    break;
                }
            }
        }
        if(strlen($main_logo)<=1 && getenv('APP_LOGO')){
            $main_logo = Yii::$app->urlManager->getHostInfo().'/customer/'.getenv('APP_LOGO');
        }
        if(strlen($main_icon)<=1 && getenv('APP_ICON')){
            $main_icon = Yii::$app->urlManager->getHostInfo().'/customer/'.getenv('APP_ICON');
        }

    }else{
        $main_logo = $baseUrl.'/images/taktwerk_logo.svg';
        $main_icon = $baseUrl.'/images/taktwerk_icon.svg';
    }

        if(!$main_logo){
            $main_logo = $baseUrl.'/images/taktwerk_logo.svg';
        }
        if(!$main_icon){
            $main_icon = $baseUrl.'/images/taktwerk_icon.svg';
        }
        if(getenv('APP_BACKGROUND')){
            $main_bg = Yii::$app->urlManager->getHostInfo().'/customer/'.getenv('APP_BACKGROUND');
        }else{
            $main_bg = $baseUrl.'/images/background.svg';
        }

        $assets['logo'] = $main_logo;
        $assets['icon'] = $main_icon;
        $assets['bg'] = $main_bg;

        if($type){
            return $assets[$type];
        }

        return $assets;
    }
    
    public static function getSkin(){
        
        if(getenv('APP_MODE')==='1'){
            $skin = getenv('APP_SKIN');
        }elseif(getenv('APP_MODE')==='2'){

            foreach ( \taktwerk\yiiboilerplate\modules\customer\models\ClientUser::findAll(['user_id' => Yii::$app->getUser()->id]) as $client) {
                $id = $client->client_id;
                $model = Client::findOne($id);
                if($model){
                    $skin = $model->appTheme("skin");
                    if(strlen($skin)>=1){
                        break;
                    }
                }
            }

        }
        
        if(!isset($skin)){
            $skin = Yii::$app->params['adminSkin'];
        }
        return isset($skin) && strlen($skin)>=1?$skin:"skin-black";
    }
    
    public static function getMaintenanceContent($class=''){
        $str = '';
        if (Yii::$app->settings->get('planned', 'maintenance', false, 'boolean')){ 
            if($class){
                $str = '<div class="'.$class.'">';
            }else{
                $str = '<div class="callout callout-warning">';
            }
                    $str .=  '<h4>'.Yii::t('twbp', 'Important Information').'</h4>';
                    $str .=  '<p>'.Yii::t('app', 'The application will be updated today at HH:II and will not be available for XX minutes.').'</p>';
                    $str .= '</div>';
        } 
        return $str;
    }

    public static function arrayKeyExists( array $array, $keys ) {
        $count = 0;
        if ( ! is_array( $keys ) ) {
            $keys = func_get_args();
            array_shift( $keys );
        }
        foreach ( $keys as $key ) {
            foreach ($array as $item){
                if ( isset( $item[$key] ) || array_key_exists( $key, $item ) ) {
                    $count ++;
                }   
            }
        }

        return count( $keys ) === $count;
    }
    public static function getModelHasOneRelations(ActiveRecord $model){
        $reflector = new \ReflectionClass(get_class($model));
        $baseClassMethods = get_class_methods('yii\db\ActiveRecord');
        $hasOneRelations = [];
        foreach ($reflector->getMethods() as $method) {
            if (substr($method->name,0,3) !== 'get') continue;
            if (in_array($method->name, $baseClassMethods) || $method->getNumberOfParameters()>0) {
                continue;
            }
            $relation = call_user_func(array($model,$method->name));
            if($relation instanceof \yii\db\ActiveQuery){
                if($relation->multiple==false){
                    $hasOneRelations[] = [
                        'name' => $method->name,
                        'link' => $relation->link,
                        'modelClass' => $relation->modelClass,
                        'on' => $relation->on
                    ];
                }
            }
        }
        return $hasOneRelations;
    }
}
