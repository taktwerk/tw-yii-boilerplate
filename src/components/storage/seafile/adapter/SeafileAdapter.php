<?php

namespace taktwerk\yiiboilerplate\components\storage\seafile\adapter;

use League\Flysystem\Adapter\AbstractAdapter;
use League\Flysystem\Adapter\Polyfill\NotSupportingVisibilityTrait;
use League\Flysystem\Adapter\Polyfill\StreamedTrait;
use League\Flysystem\Config;
use League\Flysystem\Util;
use function Sabre\Xml\Deserializer\valueObject;
use Yii;

class SeafileAdapter extends AbstractAdapter
{

    use NotSupportingVisibilityTrait;
    use StreamedTrait;

    protected $library;
    protected $token;
    protected $libraryID;
    protected $url;
    protected $server = "https//tw-could.taktwerk.ch";
    protected $outputStream;

    protected $initiated = false;

    /**
     *
     * @param string $library
     * @param string $token
     */
    public function __construct($library, $token, $server)
    {
        $this->library = $library;
        $this->token = $token;
        $this->server = $server;
    }

    /**
     * Initialize library, if not exist, create new one and setup libraryID
     * @return void
     */
    protected function initLibrary()
    {
        // If the library is already initiated, don't re-do it. We do this to avoid initiating a connection on each
        // request when navigating in webdav in another filesystem.
        if ($this->initiated) {
            return;
        }
        // Set it to true, call() will call initLibrary() again and we'll be stuck in an endless loop otherwise.
        $this->initiated = true;

        $found = false;
        $response = $this->call("repos", "GET");
        if ($response['code'] == 200) {
            foreach ($response['result'] as $library) {
                if ($library->name === $this->library) {
                    $found = true;
                    $this->libraryID = $library->id;
                    break;
                }
            }
            if (!$found) {
                $data = ['name' => $this->library];
                $library = $this->call('repos', "POST", $data, false);
                $this->libraryID = $library->repo_id;
            }

        } elseif (!empty($response['error'])) {
            // Handle errors
            throw new \Exception('SeafileAdapter: library \'' . $this->library . '\' ' . $response['error']);
        }
    }

    /**
     * @param string $directory
     * @param bool $recursive
     * @return array
     * @throws \Exception
     */
    public function listContents($directory = '', $recursive = false)
    {
        // Need a libraryID
        $this->initLibrary();

        $location = $this->applyPathPrefix($directory);
        $response = $this->call('repos/' . $this->libraryID . '/dir/?p=' . $location);

        if ($response['code'] == 200) {
            $contents = [];
            foreach ($response['result'] as $object) {
                $path = $location . '/' . $object->name;
                $contents[] = $this->normalizeResponse($object, $path);
                if ($recursive && $object->type === 'dir') {
                    $deeper = $this->listContents($path, true);
                    if ($deeper) {
                        $contents = array_merge($contents, $deeper);
                    }
                }
            }
            return $contents;
        }

        throw new \Exception("SeafileAdapter: Can't get contents of directory '$directory'. Error" . $response['code'] . ': ' . $response['error'] . '.');
    }

    /**
     * @param string $path
     * @param Config $config
     * @return array|bool|false
     */
    public function createDir($path, Config $config)
    {
        $path = $this->applyPathPrefix($path);
        $data = ['operation' => 'mkdir'];
        $response = [];

        if (substr_count($path, '/') > 1) {
            // Recursive
            $dirs = explode('/', $path);
            $current = '';
            foreach ($dirs as $dir) {
                $dir = $this->applyPathPrefix($dir);
                $current .= $dir;
                $current = '/' . ltrim($current, '/');
                if ($this->has($current) || $current == '/') {
                    continue;
                }
                $response = $this->call('repos/' . $this->libraryID . '/dir/?p=' . $current, 'POST', $data);
            }
        } else {
            $response = $this->call('repos/' . $this->libraryID . '/dir/?p=' . $path, 'POST', $data);
        }
        if ($response['code'] == 201) {
            $parentPath = substr($path, 0, strrpos($path, '/') + 1);
            $this->resetCache($parentPath, 'url');
            $this->resetCache($parentPath, 'meta');

            return true;
        }
        return false;
    }

    /**
     * @param string $path
     * @return bool
     */
    public function deleteDir($path)
    {
        $path = $this->applyPathPrefix($path);
        $response = $this->call('repos/' . $this->libraryID . '/dir/?p=' . $path, 'DELETE');
        if ($response['code'] == 200) {
            // Clear cache
            $this->resetCache($path, 'url');
            $this->resetCache($path, 'meta');
            $parentPath = substr($path, 0, strrpos($path, '/') + 1);
            $this->resetCache($parentPath, 'url');
            $this->resetCache($parentPath, 'meta');
            return true;
        }
        return false;
    }

    /**
     * @param string $path
     * @return array|bool|false
     */
    public function read($path)
    {
        $location = $this->applyPathPrefix($path);
        $response = $this->call('repos/' . $this->libraryID . '/file/?p=' . $location, 'get', null, false);
        if ($response['code'] == 200) {
            $contents = $this->getFile($response['result']);
            return compact('contents', 'path');
        }
        return false;
    }

    /**
     * @param string $path
     * @param string $contents
     * @param Config $config
     * @return array|bool|false
     */
    public function write($path, $contents, Config $config)
    {
        $path = $this->applyPathPrefix($path);
        if ($this->has($path)) {
            return false;
        }
        if (substr_count($path, '/') > 1) {
            $directory = substr($path, 0, strrpos($path, '/') + 1);
            $this->createDir($directory, $config);
        }
        if ($this->upload($path, $contents)) {
            $metaData = $this->getMetadata($path);
            $size = $metaData['size'];
            $type = 'file';
            $result = compact('contents', 'type', 'size', 'path');

            return $result;
        }
        return false;
    }

    /**
     * @param string $path
     * @param string $contents
     * @param Config $config
     * @return array|bool|false
     */
    public function update($path, $contents, Config $config)
    {
        $path = $this->applyPathPrefix($path);
        if (!$this->has($path)) {
            return false;
        }
        if ($this->upload($path, $contents)) {
            $metaData = $this->getMetadata($path);
            $size = $metaData['size'];
            return compact('path', 'size', 'contents');
        }
        return false;
    }

    /**
     * Return metaData of file or directory
     * Since limitation of Seafile API, we assume that file must contain . (dot).
     * If there is no . (dot) input param will be processed as dir
     * @param type $path
     * @return bool|array
     */
    public function getMetadata($path)
    {
        $this->initLibrary();

        // Use cached result, won't have changed in this short amount of time.
        $cacheKey = $this->getCacheKey($path);
        $cached = Yii::$app->getCache()->get($cacheKey);
        if ($cached !== false) {
            return $cached;
        }

        $found = false;
        $folderInfo = ['type' => 'dir', 'path' => ''];

        $path = $this->applyPathPrefix($path);
        // Directory, checking first if there are directory with that name because of Seafile API limitation
        // Going up level up for listing directories, since Seafile don't have direct API for directory info
        $parentPath = substr($path, 0, strrpos($path, '/') + 1);
        $name = substr($path, strrpos($path, '/') + 1);
        $response = $this->call('repos/' . $this->libraryID . '/dir/?p=' . $parentPath);
        if ($response['code'] == 200) {
            foreach ($response['result'] as $element) {
                if ($element->name == $name) {
                    // Need to add a mimetype for dav to work properly
                    if ($element->type == 'dir') {
                        $element->mimetype = 'directory';
                    }
                    $found = true;
                    $folderInfo = (array)$element;
                    $folderInfo['timestamp'] = $element->mtime ?: null;
                    break;
                }
            }
        }
        // If there is no folder with that name, try to find a file
        if (!$found) {
            $fileResponse = $this->call('repos/' . $this->libraryID . '/file/detail/?p=' . $path);
            if ($fileResponse['code'] == 200) {
                $found = (array)$fileResponse['result'];
                $folderInfo['timestamp'] = $fileResponse->mtime ?: null;
            } else {
                $found = false;
            }
        }

        // If found is still true, we're still on the folder and nothing else, so use that
        if ($found === true) {
            $found = $folderInfo;
        }

        // Keep the cache for 120 seconds
        Yii::$app->getCache()->set($cacheKey, $found, 15);

        return $found;
//        $path = $this->applyPathPrefix($path);
//        if (strpos($path, '.')) {
//            // File
//            $responese = $this->call('repos/' . $this->libraryID . '/file/detail/?p=' . $path);
//            if ($responese['code'] == 200) {
//                return (array)$responese['result'];
//            } else {
//                return false;
//            }
//        } else {
//            // Directory
//            // Going up level up for listing directories, since Seafile don't have direct API for directory info
//            $parentPath = substr($path, 0, strrpos($path, '/') + 1);
//            $name = substr($path, strrpos($path, '/') + 1);
//            $response = $this->call('repos/' . $this->libraryID . '/dir/?p=' . $parentPath . '&t=d');
//            if ($response['code'] == 200) {
//                foreach ($response['result'] as $dir) {
//                    if ($dir->name == $name) {
//                        return (array)$dir;
//                    }
//                }
//                //@TODO: Not returning false, double check why
//                return false;
//            }
//            return false;
//        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMimetype($path)
    {
        return $this->getMetadata($path);
    }

    /**
     * {@inheritdoc}
     */
    public function getSize($path)
    {
        return $this->getMetadata($path);
    }

    /**
     * {@inheritdoc}
     */
    public function getTimestamp($path)
    {
        return $this->getMetadata($path);
    }

    /**
     * @param string $path
     * @param string $newpath
     * @return bool|mixed
     */
    public function rename($path, $newpath)
    {
        $path = $this->applyPathPrefix($path);
        if ($this->has($newpath)) {
            return false;
        }
        $newfile = explode('/', $newpath);
        $newfile = $newfile[count($newfile) - 1];
        $data = ['operation' => 'rename', 'newname' => $newfile];
        $response = $this->call('repos/' . $this->libraryID . '/file/?p=' . urlencode($path), 'POST', $data);

        // Clear cache
        $this->resetCache($path, 'meta');
        $this->resetCache($path, 'url');

        // Clear parent cache
        $parentPath = substr($path, 0, strrpos($path, '/') + 1);
        $this->resetCache($parentPath, 'meta');
        $this->resetCache($parentPath, 'url');


        return $response;
    }

    /**
     * @param string $path
     * @param string $newpath
     * @return bool
     */
    public function copy($path, $newpath)
    {
        $path = $this->applyPathPrefix($path);
        $newpath = $this->applyPathPrefix($newpath);

        $source = pathinfo($path);
        $destination = pathinfo($newpath);
        $sourceFilename = $source['basename'];
        $destinationFilename = $destination['basename'];
        $sourceDir = $source['dirname'];
        $destinationDir = $destination['dirname'];
        $sourceMetaData = $this->getMetadata($path);

        // Clear cache
        $this->resetCache($path, 'meta');
        $this->resetCache($path, 'url');

        // Clear parent cache
        $parentPath = substr($path, 0, strrpos($path, '/') + 1);
        $this->resetCache($parentPath, 'meta');
        $this->resetCache($parentPath, 'url');

        if ($sourceMetaData['type'] == 'file'
            && $sourceFilename != $destinationFilename
            && $sourceDir == $destinationDir
        ) {
            $tmpDir = '/' . md5($sourceFilename);
            $this->createDir($tmpDir, new Config());
            $this->copy($path, $tmpDir . '/' . $sourceFilename);
            $this->rename($tmpDir . '/' . $sourceFilename, $tmpDir . '/' . $destinationFilename);
            $result = $this->copy($tmpDir . '/' . $destinationFilename, $destinationDir . '/' . $destinationFilename);
            $this->deleteDir($tmpDir);
            return $result;
        } else {
            $directory = substr($newpath, 0, strrpos($newpath, '/') + 1);
            $name = substr($newpath, strrpos($newpath, '/') + 1);
            $data = [
                'dst_repo' => $this->libraryID,
                'dst_dir' => $directory,
                'file_names' => ltrim($path, '/')
            ];
            $response = $this->call('repos/' . $this->libraryID . '/fileops/copy/', 'POST', $data);
            if ($response['code'] == 200) {
                // Rename the copied folders
                foreach ($response['result'] as $folder) {
                    $this->rename($folder->parent_dir . $folder->obj_name, $newpath);
                }
                return true;
            }
            return false;
        }
    }

    /**
     * @param string $path
     * @return bool
     */
    public function delete($path)
    {
        $path = $this->applyPathPrefix($path);
        $response = $this->call('repos/' . $this->libraryID . '/file/?p=' . urlencode($path), 'DELETE');
        if ($response['code'] == 200) {
            // Clear cache
            $this->resetCache($path, 'meta');
            $this->resetCache($path, 'url');

            $parentPath = substr($path, 0, strrpos($path, '/') + 1);
            $this->resetCache($parentPath, 'meta');
            $this->resetCache($parentPath, 'url');
            return true;
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function has($path)
    {
        $path = $this->applyPathPrefix($path);
        return $this->getMetadata($path);
        return !$this->getMetadata($path) ? false : true;
    }

    /**
     * Seafile API Call
     * @param string $url like 'repos' or some other API-Action
     * @param string $type 'GET','POST','PUT','DELETE'
     * @param null $data array()|null, with POST-Data
     * @return mixed returned json-object
     */
    protected function call($url, $type = "GET", $data = null, $useCache = true)
    {
        $this->initLibrary();

        // Use cached result, won't have changed in this short amount of time.
        $segments = explode('?p=', $url);
        $path = count($segments) > 1 ? $segments[1] : $url;
        $path = trim($path, '/');
        $cacheKey = $this->getCacheKey($path, 'url');
        $cached = Yii::$app->getCache()->get($cacheKey);
        if ($useCache && $cached !== false) {
            if (strtolower($type) == 'get') {
                $this->debug("using cache for <strong>$path</strong>, key $cacheKey");
                return $cached;
            } else {
                // Not get? Delete cache
                $this->resetCache($path, 'url');
            }
        }

        $curl = curl_init();
        $url = $this->server . "/api2/$url";
        $this->debug("url: $url");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        if ($type == "POST") {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_VERBOSE, 1);
        } elseif ($type == "DELETE") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($curl, CURLOPT_VERBOSE, 1);
        } else {
            // Don't wait longer than 30 seconds for seafile. If it takes longer, there is an error.
            curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        }
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Authorization: Token ' . $this->token,
            'Accept: application/json; indent=4'
        ]);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $result = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $ch_error = curl_error($curl);
        curl_close($curl);

        $returnedResult = [
            'result' => json_decode($result),
            'code' => $httpcode,
            'error' => $ch_error
        ];

        // Keep the cache for 60 seconds for get requests
        if ($httpcode == 200 && $useCache && strtolower($type) == 'get') {
            $this->debug("- write cache for $path and $cacheKey");
            Yii::$app->getCache()->set($cacheKey, $returnedResult, 20);
        }
        return $returnedResult;
    }

    /**
     * Apply the path prefix.
     *
     * @param string $path
     *
     * @return string prefixed path
     */
    public function applyPathPrefix($path)
    {

        $path = parent::applyPathPrefix($path);

        return '/' . ltrim(rtrim($path, '/'), '/');
    }

    /**
     * @param $path
     * @param $content
     * @param bool $stream
     * @return bool|mixed
     */
    protected function upload($path, $content, $stream = false)
    {
        if ($this->has($path)) {
            $this->delete($path);
        }
        $directory = substr($path, 0, strrpos($path, '/') + 1);
        $fileName = substr($path, strrpos($path, '/') + 1);
        $response = $this->call('repos/' . $this->libraryID . '/upload-link/?p=' . $directory, 'get', null, false);

        // Clear cache
        $this->resetCache($path, 'meta');
        $this->resetCache($path, 'url');
        $this->resetCache($directory, 'meta');
        $this->resetCache($directory, 'url');

        if ($response['code'] == 200) {
            $uploadLink = $response['result'];
            if (!$stream) {
                $tmpFile = tmpfile();
                fwrite($tmpFile, $content);
                fseek($tmpFile, 0);
            } else {
                $tmpFile = $content;
            }
            $metaDatas = stream_get_meta_data($tmpFile);
            $tmpFilename = $metaDatas['uri'];
            $tmpFilenameType = $metaDatas['wrapper_type'];
            $data = ['filename' => $fileName, 'parent_dir' => $directory];
            $data['file'] = new \CurlFile($tmpFilename, $tmpFilenameType, $fileName);
            $url = $uploadLink;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_VERBOSE, 1);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Token ' . $this->token));
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            $result = curl_exec($curl);
            $ch_error = curl_error($curl);
            curl_close($curl);
            if (!$stream) {
                fclose($tmpFile);
            }

            return $result;
        }
        return false;
    }

    /**
     * @param $url
     * @param bool $stream
     * @return mixed
     */
    protected function getFile($url, $stream = false)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $result = curl_exec($curl);
        $ch_error = curl_error($curl);
        curl_close($curl);
        if ($stream) {
            fwrite($stream, $result);
            rewind($stream);
        } else {
            return $result;
        }
    }

    /**
     * Normalize a Seafile response.
     *
     * @param array $response
     *
     * @return array
     */
    protected function normalizeResponse($response, $path = '')
    {
        $result = ['path' => ltrim($path, '/')];

        if (isset($response->mtime)) {
            $result['timestamp'] = $response->mtime;
        }
        $result = array_merge($result, get_object_vars($response));

        return $result;
    }

    /**
     * Reset the cache for a path
     * @param $path
     */
    protected function resetCache($path, $feature = 'meta')
    {
        $path = trim($path, '/');
        $cacheKey = $this->getCacheKey($path, $feature);

        $this->debug("clear cache key <strong>$path</strong>, key $cacheKey");
        Yii::$app->getCache()->delete($cacheKey);
    }

    /**
     * @param $path
     * @param string $feature
     * @return string
     */
    protected function getCacheKey($path, $feature = 'meta')
    {
        return 'seafile_fs_' . $feature . '_' . md5(trim($path, '/'));
    }

    /**
     * @param $string
     */
    protected function debug($string)
    {
//        echo $string . '<br>';
    }
}
