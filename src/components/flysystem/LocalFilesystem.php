<?php
/**
 * @link https://github.com/creocoder/yii2-flysystem
 * @copyright Copyright (c) 2015 Alexander Kochetov
 * @license http://opensource.org/licenses/BSD-3-Clause
 */

namespace taktwerk\yiiboilerplate\components\flysystem;

use creocoder\flysystem\LocalFilesystem as LFilesystem;
use taktwerk\yiiboilerplate\components\flysystem\adapter\Local;

/**
 * LocalFilesystem
 *
 * @author Alexander Kochetov <creocoder@gmail.com>
 */
class LocalFilesystem extends LFilesystem
{

    protected function prepareAdapter()
    {
        return new Local($this->path);
    }
}
