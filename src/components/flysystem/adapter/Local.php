<?php
namespace taktwerk\yiiboilerplate\components\flysystem\adapter;

use League\Flysystem\Adapter\Local as L;
use yii\helpers\FileHelper;

class Local extends L
{

    /**
     * @inheritdoc
     */
    public function setVisibility($path, $visibility)
    {
        try {
            return parent::setVisibility($path, $visibility);
        } catch (\Exception $e) {}
        return compact('path', 'visibility');
    }

    public function delete($path, $delDirIfEmpty = true)
    {
        $dirName = dirname($this->applyPathPrefix($path));
        if (parent::delete($path)) {
            $isDirEmpty = ! (new \FilesystemIterator($dirName))->valid();
            if ($delDirIfEmpty && $isDirEmpty) {
                rmdir($dirName);
            }
            return true;
        }
        return true;
    }
}