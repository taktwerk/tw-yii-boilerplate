<?php

namespace taktwerk\yiiboilerplate\components\dav;

use League\Flysystem\AdapterInterface;
use creocoder\flysystem\AwsS3Filesystem;
use Sabre\DAV\ICollection;
use Sabre\DAV\FS\Node;
use Sabre\HTTP\URLUtil;
use Exception;
use taktwerk\yiiboilerplate\modules\share\models\FlysystemAttribute;

class Directory extends Node implements ICollection
{
    /**
     * @var AdapterInterface
     */
    protected $fs;

    /**
     *
     */
    protected $name;

    /**
     *
     */
    protected $component;

    /**
     * The attributes for file permissions
     * @var array
     */
    protected $attributes = [];

    /**
     * The default config for the share. If not provided, defaults to [read = true, write = true]
     * @var array
     */
    protected $defaults = [];

    /**
     *
     */
    public function __construct($component, $path = null)
    {
        $this->path = $path == null ? ($component['path'] == null ? '/' : $component['path']) : $path;
        $this->fs = \Yii::$app->get($component['component']);
        $this->component = $component;
        $this->name = isset($component['name']) ? $component['name'] : null;

        $this->defaults = [
            'read' => true,
            'write' => true,
        ];

        if (!empty($component['attributes'])) {
            $this->attributes = $component['attributes'];

//            echo '<pre>';
//            var_dump($component['attributes']);
//            echo '</pre>';
//            $this->attributes = [
//                [
//                    'pattern' => '/mcm\/0100_Objekte\/0200_Auftraege\/3E_2012/',
//                    'read' => true,
//                    'write' => true,
//                ],
//                [// Always hide archive folders
//                    'pattern' => '/archive$/',
//                    'read' => false,
//                    'write' => false,
//                ],
//                [// Files ending in .pdf
//                    'pattern' => '/\.pdf$/',
//                    'target' => 'file',
//                    'read' => true,
//                    'write' => true,
//                ]
//            ];

            // If the component has attributes, assume defaults should be to false
            $this->defaults = [
                'read' => false,
                'write' => false,
            ];

            // But if none of the attributes concern folder, assume true
            $hasFolderPermissions = false;
            foreach ($this->attributes as $attribute) {
                if (!empty($attribute['target'] && $attribute['target'] == 'directory')) {
                    $hasFolderPermissions = true;
                }
            }
            if (!$hasFolderPermissions) {
                $this->defaults = [
                    'read' => true,
                    'write' => true,
                ];
            }
        }

        unset($this->component['name']);
    }

    /**
     *
     */
    public function createFile($name, $data = null)
    {
        // Allowed to write in this path?
        if (!$this->permission($name, 'write', 'file')) {
            throw new \Sabre\DAV\Exception\Forbidden('Access denied');
        }

        // If the file exists, replace it
        if ($this->fs->has($this->path . '/' . $name)) {
            $this->fs->delete($this->path . '/' . $name);
        }

        $this->fs->writeStream($this->path . '/' . $name, $data);
    }

    /**
     * @param $name
     */
    public function createDirectory($name)
    {
        if (!$this->permission($name, 'write', 'directory')) {
            throw new \Sabre\DAV\Exception\Forbidden('Access denied');
        }
        $this->fs->createDir($this->path . '/' . $name);
    }

    /**
     * @param string $name
     * @return Directory|File
     * @throws \Sabre\DAV\Exception\NotFound
     */
    public function getChild($name)
    {
        $path = $this->path . '/' . $name;
        // We have to throw a NotFound exception if the file didn't exist
        if (!$this->fs->has($path)) {
            throw new \Sabre\DAV\Exception\NotFound('The file with name: ' . $path . ' could not be found');
        }
        // Some added security
        if ($name == '.') {
            throw new \Sabre\DAV\Exception\NotFound('Access denied');
        }

        // AWS doesn't have "directories", so we need to adapt a bit.
        $type = $this->fs->getMetadata($path);
        if ($type['type'] == 'dir' || ($this->fs instanceof AwsS3Filesystem && $type == false)) {
            return new self($this->component, $path);
        } else {
            return new File($path, $this->fs);
        }
    }

    /**
     * @param $path
     * @param string $action
     * @param string $forceType
     * @return bool
     */
    protected function permission($path, $action = 'read', $forceType = '')
    {
        try {
            // No attributes at all? Use the defaults
            if (empty($this->attributes)) {
                return $this->defaults[$action];
            }

            $relpath = $this->path . '/' . $path;
            $relPathSegments = explode('/', $relpath);
            $relPathSegmentCount = count($relPathSegments) - 2;

            $perm = $filePerm = null;
            $type = $forceType ? $forceType : $this->fs->getMimetype($relpath);

            if ($type == 'directory') {
                foreach ($this->attributes as $attribute) {
                    if (isset($attribute[$action]) && isset($attribute['pattern'])) {
                        // Split the pattern, we want some of it!
                        $segments = explode('\\/', trim($attribute['pattern'], '/'));

                        // We need to build the pattern based on how many segments our current path has.
                        if (count($segments) > 1) {
                            $pattern = '';
                            // Add the root to the path, since it needs to start with it
                            if (strpos($attribute['pattern'], $this->path) === false) {
                                $rootSegments = explode('/', trim($this->path, '/'));
                                $pattern = '\/' . trim($rootSegments[0], '/');
                            }

                            //              < because we start by adding root.
                            for ($i = 0; $i < $relPathSegmentCount; $i++) {
                                if (!empty($segments[$i])) {
                                    $pattern .= "\/" . $segments[$i];
                                }
                            }

                            // '/mcm\/0100_Objekte\/0200_Auftraege\/3E_2012/',
//                            echo "testing <strong>/^$pattern/</strong> on $relpath<br>";
                            if (preg_match("/^$pattern/", $relpath)) {
                                // If we have the action as false, we want to stop now
                                if ($attribute[$action] === FlysystemAttribute::READABLE_NO) {
                                    return false;
                                } elseif ($attribute[$action] === FlysystemAttribute::READABLE_YES) {
                                    $perm = true;
                                }
                                // Else: skip, keep the previous value
                            }
                        } elseif (preg_match($attribute['pattern'], $relpath)) {
                            // If we have the action as false, we want to stop now
                            if ($attribute[$action] === FlysystemAttribute::READABLE_NO) {
                                return false;
                            } elseif ($attribute[$action] === FlysystemAttribute::READABLE_YES) {
                                $perm = true;
                            }
                            // Else: skip, keep the previous value
                        }
                    }
                }

                // No permission? Use defaults
                if ($perm === null) {
                    // Defaults don't allow?
                    if (!$this->defaults[$action]) {
                        return false;
                    }
                } elseif (!$perm) {
                    // Don't have the permission
                    return false;
                }
            } else {
                // We need at least one file perm to be true
                $hasFolder = false;
                $hasFile = false;
                $filePermissionCount = $folderPermissionCount = 0;
                foreach ($this->attributes as $attribute) {

                    if (isset($attribute['target']) && $attribute['target'] == 'file') {
                        $filePermissionCount++;
                        if (preg_match($attribute['pattern'], $relpath)) {
                            if ($attribute[$action] == FlysystemAttribute::READABLE_NO) {
                                return false;
                            } elseif ($attribute[$action] == FlysystemAttribute::READABLE_YES) {
                                $hasFile = true;
                            }
                        }
                    } else {
                        $folderPermissionCount++;
                        // Make the pattern work for folders
                        $pattern = '/' . trim($attribute['pattern'], '/') . '/';
                        if (preg_match($pattern, $relpath)) {
                            if ($attribute[$action] == FlysystemAttribute::READABLE_NO) {
                                return false;
                            } elseif ($attribute[$action] == FlysystemAttribute::READABLE_YES) {
                                $hasFolder = true;
                            }
                        }
                    }
                }
                // If there are no folder permissions, use defaults
                if ($folderPermissionCount == 0) {
                    $hasFolder = $this->defaults[$action];
                }

                // If there are no file permissions, we just need the folder to be good.
                $result = $filePermissionCount == 0 ? $hasFolder : ($hasFolder && $hasFile);
                return $result;
            }
        } catch (Exception $e) {
            // Silence errors to avoid having errors with unexpected results.
            // It will return true.
//            die($e->getMessage());
        }

        return true;
    }

    /**
     * @return array
     * @throws \Sabre\DAV\Exception\NotFound
     */
    public function getChildren()
    {
        $children = array();
        // Loop through the directory, and create objects for each node
        foreach ($this->fs->listContents($this->path) as $node) {
            // Ignoring files staring with .
            if ($node['basename'] === '.') {
                continue;
            }
            if ($node['basename'] === '/') {
                continue;
            }
            if ($this->permission($node['basename'])) {
                $children[] = $this->getChild($node['basename']);
            }
        }

        return $children;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function childExists($name)
    {
        return $this->fs->has($this->path . '/' . $name);
    }

    /**
     * Delete a file or folder
     */
    public function delete()
    {
        if (!$this->permission('', 'write')) {
            throw new \Sabre\DAV\Exception\Forbidden('Access denied');
        }
        $this->fs->deleteDir($this->path);
    }

    /**
     * Rename a file or folder
     * @param $name
     */
    public function setName($name)
    {
        list($parentPath,) = URLUtil::splitPath($this->path);
        list(, $newName) = URLUtil::splitPath($name);
        $newPath = $parentPath . '/' . $newName;
        if (!$this->permission('', 'write')) {
            throw new \Sabre\DAV\Exception\Forbidden('Access denied');
        }
        $this->fs->rename($this->path, $newPath);
        $this->path = $newPath;
    }

    /**
     * @return null
     */
    public function getLastModified()
    {
        return $this->path == '/' ? null : $this->fs->getMetadata($this->path)['timestamp'];
    }

    /**
     * Get the name of an element
     * @return null
     */
    public function getName()
    {
        if ($this->name !== null) {
            return $this->name;
        }
        return parent::getName();
    }
}
