<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 7/31/2017
 * Time: 9:30 AM
 */

namespace taktwerk\yiiboilerplate\components\dav\auth;

use Da\User\Helper\SecurityHelper;
use Da\User\Query\UserQuery;
use \Sabre\DAV\Auth\Backend\AbstractBasic;
use \Yii;

class Auth extends AbstractBasic
{

    /**
     * @param string $username
     * @param string $password
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    protected function validateUserPass($username, $password)
    {
        $finder = Yii::createObject(UserQuery::class);
        $user = $finder->whereUsernameOrEmail($username)->one();
        if ($user !== null && SecurityHelper::validatePassword($password, $user->password_hash)) {
            Yii::$app->user->login($user);
            return true;
        }
        return false;
    }
}