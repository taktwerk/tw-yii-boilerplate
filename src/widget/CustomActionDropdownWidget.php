<?php

namespace taktwerk\yiiboilerplate\widget;

use taktwerk\yiiboilerplate\models\TwActiveRecord;
use Yii;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class CustomActionDropdownWidget extends ButtonDropdown
{
    /**
     * @var $key
     */
    public $key;

    /**
     * @var $model TwActiveRecord
     */
    public $model;

    public function run()
    {
        $this->encodeLabel = false;
        $this->label = '<i class="fa fa-gear"></i> '.Yii::t('twbp','Actions');
        $items = Yii::$app->controller->customActions;
        $this->key = $this->model->primaryKey;
        $this->options['id'] = 'tw-actions';
        $this->containerOptions['id'] = 'tw-container-actions';
        $dropdown_items = [];
        foreach($items as $action_key => $item){
            if($this->model->hasAttribute('deleted_at') && $this->model->deleted_at!=null && !in_array($action_key,['view','delete'])){
                continue;
            }
            $url = ArrayHelper::merge([$action_key],$this->model->getPrimaryKey(true));
            $action_link = call_user_func($item, Url::toRoute($url), $this->model, $this->key);
            $dropdown_items[] = Html::tag('li',$action_link);
        }
        if(count($dropdown_items)>=1){
            $this->dropdown['items'] = $dropdown_items;
            $this->dropdown['options'] =[
                'class' => 'dropdown-menu-right',
            ];
            $this->options['class'] = 'btn-default btn-group';
            return parent::run();
        }
        return false;
    }

}