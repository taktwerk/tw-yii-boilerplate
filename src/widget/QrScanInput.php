<?php

namespace taktwerk\yiiboilerplate\widget;

use app\assets\QrAsset;
use taktwerk\yiiboilerplate\widget\assets\QrScanAsset;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\widgets\InputWidget;

class QrScanInput extends InputWidget
{
    public $input_id = "";
    
    public $base_url = "";

    public $label = "";

    public static $autoIdPrefix = 'qr';

    public function run()
    {
        $id = $this->id;
        
        if($this->model && $this->attribute){
            $attribute = $this->attribute;
            $value = $this->model->$attribute;
        }else{
            $value = $this->value;
        }

        $input = Html::label($this->label);
        $input .= Html::beginTag('div', [
            'style' => 'display:flex;'
        ]);
        $input .= Html::beginTag('div', [
            'style' => 'display:grid;',
            'class' => 'text-center'
        ]);
        $input .= Html::a(
            Html::img($this->base_url.'/sample-qr.png',[
                'width' => '100px',
                'class' =>'img-thumbnail',
            ]),
            '#',
            [
                'title' =>\Yii::t('twbp','Click to scan'),
                'id' => "btn-{$id}",
                'class' => "btn-qr-scan",
                'data-input' => $this->input_id,
                'data-output' => 'output-'.$this->input_id,
                'data-canvas' => 'canvas-'.$this->input_id,
                'style' =>'margin-right:10px'
            ]
        );
        $input .= \Yii::t('twbp','Click to scan');
        $input .= Html::endTag('div');
        $input .= "<textarea readonly class='form-control' id='output-{$this->input_id}' value='{$value}'></textarea>";
        $input .= Html::endTag('div');
        $input .= "<canvas hidden='' id='canvas-{$this->input_id}' style='position: fixed;width: 100%;height: 100%;top: 0;left: 0;z-index: 100000;'></canvas><div id='close-camera' class='btn btn-primary' style='display:none;position: fixed;top: 20px;right: 20px;z-index: 110000;font-size: 14px;'>Close</div>";
        if($this->model){
            $input .= Html::activeHiddenInput($this->model, $this->attribute, $this->options);
        }else{
            $input .= Html::hiddenInput($this->name, $this->value, $this->options);
        }

        return $input;
    }

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        $id = $this->id;
        if($this->model && $this->attribute){
            $this->input_id = Html::getInputId($this->model, $this->attribute);
            $this->label = $this->model->getAttributeLabel($this->attribute);
        }else{
            $this->input_id = $id;
        }

        /**@var $assets QrScanAsset*/
        $assets = QrScanAsset::register($this->view);
        $this->base_url = $assets->baseUrl;
        parent::init();
    }
}
