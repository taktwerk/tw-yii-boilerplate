<?php

return [
    'height' => 200,
    'toolbarGroups' => [
        ['name' => 'undo'],
        ['name' => 'styles'],
        ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
        ['name' => 'colors'],
        ['name' => 'paragraph', 'groups' => [ 'list', 'indent', 'align' ]],
        ['name' => 'others', 'groups' => ['others', 'about']],
        ['name' => 'insert'],
    ],
    'removeButtons' => 'Subscript,Superscript,Flash,Image,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Styles,FontSize,Font',
    'removePlugins' => 'elementspath',
    'resize_enabled' => false,
    'format_tags' => 'h1;h2',
];
