<?php
/**
 * Copyright (c) 2016.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 *
 * basic preset returns the basic toolbar configuration set for CKEditor.
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 */
return isset(Yii::$app->params['CKEditor']['simple']) ? Yii::$app->params['CKEditor']['simple'] :
[
    'height' => 200,
    'toolbarGroups' => [
        ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
        ['name' => 'paragraph', 'groups' => [ 'list', 'indent', 'blocks', 'align', 'bidi' ]],
        ['name' => 'links', 'groups' => ['links']],
        ['name' => 'insert'],

        ['name' => 'clipboard', 'groups' => ['mode', 'selection', 'clipboard', 'doctools']],
        ['name' => 'undo'],

        ['name' => 'blocks'],
        ['name' => 'tools'],
    ],
    'removeButtons' => 'Subscript,Superscript,Flash,Table,Smiley,PageBreak,Iframe,Image,ShowBlocks,BidiLtr,BidiRtl,Language,CreateDiv,Templates',
    'removePlugins' => 'elementspath',
];
