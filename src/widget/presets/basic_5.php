<?php

return [
    "undo",
    "redo", "|",
    "heading", "|",
    "bold",
    "italic",
    "blockQuote", "|",
    "link", "|",
    "numberedList",
    "bulletedList","|"
];
