<?php

return [
    'undo','redo','alignment:left','alignment:right','alignment:center','alignment:justify','alignment','fontSize','fontFamily','fontColor','fontBackgroundColor','bold','italic','strikethrough','underline','blockQuote','ckfinder','imageTextAlternative','imageUpload','heading','imageStyle:full','imageStyle:alignLeft','imageStyle:alignRight','indent','outdent','link','numberedList','bulletedList','mediaEmbed','insertTable','tableColumn','tableRow','mergeTableCells'
];



