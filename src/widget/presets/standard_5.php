<?php

return [
    "undo",
    "redo", "|",
    "heading", "|",
    "bold",
    "italic",
    "blockQuote", "|",
    "link", "|",
    "numberedList",
    "bulletedList","|",
    "imageUpload",
    "imageTextAlternative",
    "imageStyle:full",
    "imageStyle:side","|",
    "mediaEmbed","|",
    "insertTable",
    "tableColumn",
    "tableRow",
    "mergeTableCells","|"
];

