<div class="threed-model"
	 data-model-path="<?=$modelPath?>"
	 style="height: 100%;width:100%;position:relative;">
	<div class="threed-model-loader"
	>
		<i class="fa fa-fw fa-spinner fa-spin"></i>
		Loading
	</div>
</div>

<?php if ($scenario && $scenario === 'fileInput') {
	$this->registerJs("
		$(document).ready(function() {
			if (typeof create3dModelFileInput === 'undefined' || typeof create3dModelFileInput !== 'function') {
				return;
			}
			create3dModelFileInput('$modelPath');
			$('.kv-file-zoom').on('click', function() {
				setTimeout(function() {
					create3dModelFileInput('$modelPath');
				}, 1000);
		        create3dModelFileInput('$modelPath');
		    });
		});
	");
}
?>

<?php \taktwerk\yiiboilerplate\widget\assets\Model3dAssets::register($this); ?>