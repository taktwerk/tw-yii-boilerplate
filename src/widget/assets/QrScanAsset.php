<?php

namespace taktwerk\yiiboilerplate\widget\assets;

use yii\web\AssetBundle;


class QrScanAsset extends AssetBundle
{
    public $sourcePath = '@taktwerk/yiiboilerplate/widget/assets/web';
    
    public $js = [
        'qr_packed.js',
        'qr.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
