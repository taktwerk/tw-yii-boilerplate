var db = new Dexie("three_models");
db.version(1).stores({
  three_models: 'name,value,childrenValues'
});

class Model3D {
    static modelObject;

    resizeCanvas = false;
    isRotateModel = true;
    stopRender = false;
    enableUserRotate = true;
    willStopRotate = true;
    backgroundColor = 'green';
    controls = null;
    gltfScene = null;

    init = async (modelElement, fileName) => {
        if (!modelElement && !this.modelElement) {
            console.log('no model element');
            return;
        }
        if (modelElement) {
            this.modelElement = modelElement;
        }
        if (!this.modelElement.clientWidth || !this.modelElement.clientHeight) {
            console.log('no model width or height');
            return;
        }
        this.scene = new THREE.Scene();
        const areaWidth = this.modelElement.clientWidth;
        const areaHeight = this.modelElement.clientHeight;
        if (this.backgroundColor) {
            this.scene.background = new THREE.Color(this.backgroundColor);
        }
        this.camera = new THREE.PerspectiveCamera(60, areaWidth/areaHeight, 0.01, 1000);

        const light1  = new THREE.AmbientLight(0xffffff, 3);
        this.scene.add( light1 );

        const light2  = new THREE.DirectionalLight(0xffffff, 4);
        light2.position.set(0.5, 0, 0.866); // ~60º
        this.scene.add( light2 );

        // var light = new THREE.PointLight( 0xffffff, 0.9 );
        // this.scene.add( light );

        this.renderer = new THREE.WebGLRenderer({antialias:true, alpha: true});
        this.renderer.physicallyCorrectLights = true;
        this.renderer.outputEncoding = THREE.sRGBEncoding;
        this.renderer.setPixelRatio( window.devicePixelRatio );
        this.renderer.setSize(areaWidth, areaHeight);

        this.modelElement.appendChild(this.renderer.domElement);
        console.log('this.modelElement', this.modelElement);
        this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
        this.controls.autoRotate = false;
        this.controls.autoRotateSpeed = -10;
        this.controls.screenSpacePanning = true;
        this.controls.enableRotate = this.enableUserRotate;

        const cachedFile = await db.three_models.get(fileName);
        let cachedSceneObject = null;
        if (cachedFile && cachedFile.value) {
            cachedSceneObject = cachedFile.value;
        }

        if (cachedFile && cachedSceneObject) {
            const loaderSecond = new THREE.ObjectLoader();
            const scene = loaderSecond.parse(cachedSceneObject);
            scene.position.set(0, 0, 0);
            this.gltfScene = scene;
            if (cachedFile.childrenValues) {
                for (let i = 0; i < cachedFile.childrenValues.length; i++) {
                    this.gltfScene.children[i].rotation.set(
                        cachedFile.childrenValues[i].rotation.x,
                        cachedFile.childrenValues[i].rotation.y,
                        cachedFile.childrenValues[i].rotation.z
                    );
                }
            }
        } else if (!this.gltfScene) {
            this.loader = new THREE.GLTFLoader();
            const dracoLoader = new THREE.DRACOLoader();
            dracoLoader.setDecoderPath( '/js/3d_model/' );
            dracoLoader.setDecoderConfig({type: 'js'});
            this.loader.setDRACOLoader( dracoLoader );
            this.gltf = await this.loader.loadAsync(fileName);
            if (this.gltf) {
                this.gltfScene = this.gltf.scene;
                const modelJSON = this.gltf.scene.toJSON();
                this.gltf.scene.matrix.toArray(modelJSON.object.matrix);
                let childrenValues = [];
                for (let i = 0; i < this.gltfScene.children.length; i++) {
                    childrenValues.push({
                        rotation: {
                            x: this.gltfScene.children[i].rotation.x,
                            y: this.gltfScene.children[i].rotation.y,
                            z: this.gltfScene.children[i].rotation.z
                        }
                    });
                }
                db.three_models.put({name: fileName, value: this.gltf.scene.toJSON(), childrenValues: childrenValues});
            }
        }
        // this.gltfScene.children[0].rotation.set(0.2, 0, 0);
        console.log('this.gltfScene.children', this.gltfScene.children);

        // return false;

        // this.loader = new THREE.GLTFLoader();
        // const dracoLoader = new THREE.DRACOLoader();
        // dracoLoader.setDecoderPath( '/js/3d_model/' );
        // dracoLoader.setDecoderConfig({type: 'js'});
        // this.loader.setDRACOLoader( dracoLoader );
        // this.gltf = await this.loader.loadAsync(fileName);
        // if (this.gltf) {
        //     this.gltfScene = this.gltf.scene;
        //     const modelJSON = this.gltf.scene.toJSON();
        //     this.gltf.scene.matrix.toArray(modelJSON.object.matrix);
        //     console.log('this.gltf.scene', this.gltf.scene);
        //     // db.three_models.put({name: fileName, value: this.gltf.scene.toJSON()});
        // }

        // return false;

        this.renderModel();

        $(modelElement.getElementsByTagName('canvas')[0])
            .on('mousedown', () => {
                this.isRotateModel = false;
            })
            .on('touchstart', () => {
                this.isRotateModel = false;
            })
            .on('touchend', () => {
                this.isRotateModel = false;
            })

        window.addEventListener('resize', () => {
            setTimeout(() => {
                if (!this.modelElement.clientWidth || !this.modelElement.clientHeight) {
                    return;
                }
                if (this.renderer.domElement.width !== this.modelElement.clientWidth ||
                    this.renderer.domElement.height !== this.modelElement.clientHeight
                ) {
                    this.resizeCanvas = true;
                }
            })
        }, false);
    }
    animate = () => {
        if (this.stopRender) {
            return;
        }
        if (this.isRotateModel || !this.willStopRotate) {
            this.pivot.rotation.y += 0.01;
        }
        if (this.resizeCanvas) {
            this.camera.aspect = this.modelElement.clientWidth / this.modelElement.clientHeight;
            this.renderer.setSize( this.modelElement.clientWidth, this.modelElement.clientHeight );
            this.camera.updateProjectionMatrix();
        }
        requestAnimationFrame(this.animate);

        this.renderer.render(this.scene, this.camera);
    }
    renderModel = () => {
        if (!this.gltfScene) {
            return;
        }
        const object = this.gltfScene;
        console.log(object.rotation);
        this.pivot = new THREE.Group();
        this.pivot.add( object );
        // const clips = this.gltf.animations;
        const box = new THREE.Box3().setFromObject( object );
        const size = box.getSize(new THREE.Vector3()).length();
        const center = box.getCenter(object.position);

        this.controls.maxDistance = size * 10;
        this.controls.enabled = true;

        this.camera.near = size / 100;
        this.camera.far = size * 100;

        this.camera.position.x += size;
        this.camera.position.y += size / 5.0;
        this.camera.position.z += size / 2.0 ;
        this.camera.lookAt(center);

        this.camera.updateProjectionMatrix();

        object.position.multiplyScalar( - 1 );

        this.controls.update();
        this.scene.add( this.pivot );

        this.controls.addEventListener('change', () => {
            console.log('was changed controles');
            this.renderer.render(this.scene, this.camera);
        });
        this.controls.addEventListener("gesturestart", () => {
            console.log('gesturestart');
            this.renderer.render(this.scene, this.camera);
        });

        this.renderer.render(this.scene, this.camera);
        this.animate();
        const loaderElement = this.modelElement.getElementsByClassName('threed-model-loader');
        if (loaderElement && loaderElement[0]) {
            loaderElement[0].style.display = 'none';
        }
    }
}

var modelObjects = null;

create3dModelFileInput = (fileName) =>  {
    if (!fileName) {
        return;
    }
    $('body').off().on('click', '.kv-file-zoom, .btn-toggleheader, .btn-fullscreen, .btn-borderless', function() {
        window.dispatchEvent(new Event('resize'));
    });
    setTimeout(function() {
        modelElements = document.getElementsByClassName('threed-model');
        if (!modelElements.length) {
            return;
        }
        Array.from(modelElements).forEach(async (modelElement) => {
            if (!modelElement) {
                return;
            }
            var alreadyRenderedModels = modelElement.getElementsByTagName('canvas');
            console.log('alreadyRenderedModels', alreadyRenderedModels);
            if (!alreadyRenderedModels || !alreadyRenderedModels.length) {
                var model = new Model3D();
                await model.init(modelElement, fileName);
            }
        });
    }, 1000);
}
