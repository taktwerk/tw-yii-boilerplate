qrcode = window.qrcode;

const video = document.createElement("video");

if(window.canvasElementId){
    const canvasElement = document.getElementById(window.canvasElementId);
    const canvas = canvasElement.getContext("2d");
}

let isShowScanning = false;
let scanning = false;
const cloaseCameraBtn = $('#close-camera');

$(document).on('click','.btn-qr-scan', function (){
    isShowScanning = true;
    $('body').css({"overflow": "hidden"});
    isShowScanning = true;
    const btnScanQR = $(this);

    const input = document.getElementById($(this).attr('data-input'));
    const output = document.getElementById($(this).attr('data-output'));
    window.canvasElementId = $(this).attr('data-canvas');
    const canvasElement = document.getElementById(window.canvasElementId);
    const canvas = canvasElement.getContext("2d");

    qrcode.callback = res => {
        if (res) {
            input.value = res;
            output.value = res;

            scanning = false;

            video.srcObject.getTracks().forEach(track => {
                track.stop();
            });

            canvasElement.hidden = true;
            btnScanQR.hidden = false;
            cloaseCameraBtn.hide();
        }
    };

    navigator.mediaDevices
        .getUserMedia({ video: { facingMode: "environment" } })
        .then(function(stream) {
            scanning = true;
            btnScanQR.hidden = true;
            canvasElement.hidden = false;
            video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
            video.srcObject = stream;
            video.play();
            tick();
            scan();
            cloaseCameraBtn.show();
        });

    function tick() {
        if (!isShowScanning) {
            return;
        }
        canvasElement.height = video.videoHeight;
        canvasElement.width = video.videoWidth;
        canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);

        scanning && requestAnimationFrame(tick);
    }

    function scan(canvasElement) {
        try {
            qrcode.decode();
        } catch (e) {
            setTimeout(scan, 300);
        }
    }

    $('#close-camera').on('click', function() {
        isShowScanning = false;
        video.srcObject.getTracks().forEach(track => {
            track.stop();
        });
        $('body').css({"overflow-y": "auto"});

        canvasElement.hidden = true;
        btnScanQR.hidden = false;
        cloaseCameraBtn.hide();
    });

});

