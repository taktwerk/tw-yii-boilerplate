<?php

namespace taktwerk\yiiboilerplate\widget\assets;

use yii\web\AssetBundle;

class CustomCkeditorAssets extends AssetBundle
{

    public $sourcePath = '@taktwerk/yiiboilerplate/widget/assets/web';
    public $css = [
        'sample.css'
    ];

    public $js = [
        'ckeditor.js',
    ];

    public $depends = [
    ];
}