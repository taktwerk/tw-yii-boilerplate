<?php

namespace taktwerk\yiiboilerplate\widget\assets;

use yii\web\AssetBundle;

class Model3dAssets extends AssetBundle
{

    public $sourcePath = '@taktwerk/yiiboilerplate/widget/assets/web/3d_model';

    public $js = [
        'three.min.js',
        'GLTFLoader.js',
        'OrbitControls.js',
        'DRACOLoader.js',
        'dexie.js',
        'index.js',
    ];

    public $css = [
    	'3d_model.css',
    ];
}