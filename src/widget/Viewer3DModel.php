<?php
namespace taktwerk\yiiboilerplate\widget;

use taktwerk\yiiboilerplate\models\TwActiveRecord;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

class Viewer3DModel extends Widget
{
    public $modelPath;
    public $scenario;

    /**
     * Renders the 3D model via three.js.
     * @return string the rendering result.
     */
    public function run()
    {
        if (!$this->modelPath) {
            return;
        }

        $this->registerAssets();

        return $this->render(
            'viewer-3d-model',
            ['modelPath' => $this->modelPath, 'scenario' => $this->scenario]
        );
    }

    protected function registerAssets()
    {
        \taktwerk\yiiboilerplate\widget\assets\Model3dAssets::register($this->getView());
    }
}
