<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 6/19/2017
 * Time: 11:01 AM
 */

namespace taktwerk\yiiboilerplate\widget;

use yii\web\View;
use yii\helpers\Html;

class FileInput extends \kartik\file\FileInput
{
    public static $availableFileTypes = ['image','html','document','office','gdocs', 'text','pdf', 'video', 'audio', 'flash', 'object', '3d','other'];
    /**
     * @var boolean whether to strictly check `allowedFileExtensions` if `allowedFileTypes` is mentioned 
     */
    public $strictExtensionTypeCheck = false;
    public $fileTypeExtensionMapping = [];
    /***
     * @var boolean if set TRUE hides file preview and only shows the file name
    **/
    public $showOnlyFileName = false;
    public function init()
    {
        parent::init();
        if($this->pluginOptions['allowedFileExtensions'] && count($this->pluginOptions['allowedFileExtensions'])){
            foreach($this->pluginOptions['allowedFileExtensions'] as $k=>$ex){
                if(in_array($ex,self::$availableFileTypes)){
                    $this->pluginOptions['allowedFileTypes'][] = $ex;
                    unset($this->pluginOptions['allowedFileExtensions'][$k]);
                }
            }
            $this->pluginOptions['allowedFileExtensions'] = array_values($this->pluginOptions['allowedFileExtensions']);
        }
        if(!isset($this->pluginOptions['theme'])){
            $this->pluginOptions['theme'] = 'fa';
        }
        if(!isset($this->pluginOptions['fileActionSettings']['showZoom'])){
            $this->pluginOptions['fileActionSettings']['showZoom'] = true;
        }

        $this->pluginOptions['allowedPreviewTypes'] = ['3d','document', 'image', 'html', 'text', 'video', 'audio', 'flash', 'pdf', 'object'];
        $this->pluginOptions['fileTypeSettings'] = [
            '3d' => new \yii\web\JsExpression("function (vType, vName) {
                return vName !== undefined && vName.match(/\.(gltf|glb|stp)$/i);
            }"),
            'text' => new \yii\web\JsExpression("function (vType, vName) {
                return !(vName !== undefined && vName.match(/\.(gltf|glb|stp)$/i)) &&
                    (
                        (vType !== undefined && vType.match('text.*')) ||
                        (vName !== undefined && vName.match(/\.(xml|javascript)$/i)) ||
                        (vName !== undefined && vName.match(/\.(txt|md|csv|nfo|ini|json|php|js|css)$/i))
                    );
            }"),
            'document'=>new \yii\web\JsExpression(<<<EOT
function (vType, vName) {
                return vType.match(/(pdf|word|excel)$/i) ||
            vName.match(/\.(pdf|docx?|xlsx?)$/i);;
}
EOT)
            ,
            
            // 'image' => new \yii\web\JsExpression("function (vType, vName) {
            //     return (vType !== undefined && vType.match('image.*') && !vType.match(/(tiff?|wmf)$/i)) ||
            //         (vName !== undefined && vName.match(/\.(gif|png|jpe?g)$/i));
            // }"),
            // 'html' => new \yii\web\JsExpression("function (vType, vName) {
            //     return (vType !== undefined && vType.match('text/html')) ||
            //         (vName !== undefined && vName.match(/\.(htm|html)$/i));
            // }"),
            // 'office' => new \yii\web\JsExpression("function (vType, vName) {
            //     return (vType !== undefined && vType.match(/(word|excel|powerpoint|office)$/i)) ||
            //         (vName !== undefined && vName.match(/\.(docx?|xlsx?|pptx?|pps|potx?)$/i));
            // }"),
            // 'gdocs' => new \yii\web\JsExpression("function (vType, vName) {
            //     return (vType !== undefined && vType.match(/(word|excel|powerpoint|office|iwork-pages|tiff?)$/i)) ||
            //         (vName !== undefined && vName.match(/\.(docx?|xlsx?|pptx?|pps|potx?|rtf|ods|odt|pages|ai|dxf|ttf|tiff?|wmf|e?ps)$/i));
            // }"),
            // 'video' => new \yii\web\JsExpression("function (vType, vName) {
            //     return (vType !== undefined && vType.match('video.*') && vType.match(/(ogg|mp4|mp?g|mov|webm|3gp)$/i)) ||
            //         (vName !== undefined && vName.match(/\.(og?|mp4|webm|mp?g|mov|3gp)$/i));
            // }"),
            // 'audio' => new \yii\web\JsExpression("function (vType, vName) {
            //     return (vType !== undefined && vType.match('audio.*') && vName.match(/(ogg|mp3|mp?g|wav)$/i)) ||
            //         (vName !== undefined && vName.match(/\.(og?|mp3|mp?g|wav)$/i));
            // }"),
            // 'flash' => new \yii\web\JsExpression("function (vType, vName) {
            //     return (vType !== undefined && vType === 'application/x-shockwave-flash') ||
            //         (vName !== undefined && vName.match(/\.(swf)$/i));
            // }"),
            // 'pdf' => new \yii\web\JsExpression("function (vType, vName) {
            //     return (vType !== undefined && vType === 'application/pdf') ||
            //         (vName !== undefined && vName.match(/\.(pdf)$/i));
            // }"),
            // 'object' => new \yii\web\JsExpression("function () {
            //     return true;
            // }"),
            // 'other' => new \yii\web\JsExpression("function () {
            //     return true;
            // }"),
        ];
        $allowedTypes  = $this->pluginOptions['allowedFileTypes'];
        $allowedExtensions  = $this->pluginOptions['allowedFileExtensions'];
        $fileTypeExtensionMapping = $this->getFileTypeExtensionMapping();
        foreach($allowedTypes as $t){
            $allowedExtensions = array_merge($allowedExtensions,$fileTypeExtensionMapping[$t]);
        }
        
        if($this->strictExtensionTypeCheck==false && (count($this->pluginOptions['allowedFileExtensions'])>0 && count($this->pluginOptions['allowedFileTypes'])>0))
        {
            $ext = implode('|',$this->pluginOptions['allowedFileExtensions']);            
            $this->pluginOptions['allowedFileTypes'][] = 'mix';
            $this->pluginOptions['fileTypeSettings']['mix'] = new \yii\web\JsExpression(<<<EOT
function (vType, vName) {
                return vName.match(/\.({$ext})$/i);
}
EOT);
        }
        if(count($allowedExtensions)>0){
            $this->pluginOptions['msgInvalidFileExtension'] = $this->pluginOptions['msgInvalidFileType'] = \Yii::t('twbp', 'Please only choose a file having {allowedExtensions} extension.',[
                'allowedExtensions' => implode(', ',$allowedExtensions)
            ]);
        }
        
        $this->pluginOptions['previewContentTemplates']['3d'] = Viewer3DModel::widget([
            'modelPath' => !empty($this->model->getMinFilePath($this->attribute)) ? 
                $this->model->getMinFilePath($this->attribute) :
                $this->model->getFileUrl($this->attribute, [], true),
            'scenario' => 'fileInput',
        ]);

        $this->pluginEvents['fileloaded'] = new \yii\web\JsExpression("function (event, file, previewId, index, reader) {
            if (previewId.match(/(gltf|glb|stp)$/i)) {
                $('.threed-model-loader').html('<div style=\"width: 100%;height: 100%;position: relative;font-size: 24px;\">3D MODEL</div>')
            }
        }");
        if($this->showOnlyFileName==true){
            $this->pluginOptions['preferIconicPreview'] = true;
            $elId = isset($this->options['id'])?$this->options['id']:'';
            if($elId==''){
            $elId = $this->hasModel()?Html::getInputId($this->model, $this->attribute) : $this->getId();
            }

            $js = <<<EOT
setTimeout(function(){
var elParent = null;
var tempParent = $('#$elId').parents('div:first');
while (elParent==null) {
    if($(tempParent).find('.kv-file-content').length){
        elParent = tempParent;
        break;
    }else{
        tempParent = $(tempParent).parents('div:first');
    }
    if(tempParent.length==0){
        break;
    }
}
            if(elParent){
                elParent.addClass('hide-kv-file-content');
            }
},200);
EOT;

            $this->view->registerJs($js,View::POS_READY);
            $css = '
.hide-kv-file-content .file-input .file-drop-zone{min-height:150px }
.hide-kv-file-content .file-input .file-drop-zone .file-drop-zone-title{padding:36px }
.hide-kv-file-content .krajee-default.file-preview-frame .kv-file-content {display:none;}';
            $this->view->registerCss($css,[],'hide-fileinput-icon-preview');
        }
    }
    public function getFileTypeExtensionMapping(){
        $mapping = [
            "3d"=>["gltf","glb","stp"],
            "text"=>["gltf","glb","stp","xml","javascript","txt","md","csv","nfo","ini","json","php","js","css"],
            "document"=>["pdf","doc","docx","xls","xlsx"],
            "image"=>["gif","png","jpg","jpeg"],
            "html"=>["htm","html"],
            "office"=>["doc","docx","xls","xlsx","ppt","pptx","pps","pot","potx"],
            "gdocs"=>["docx","doc","xlsx","xls","ppt","pptx","pps","potx","pot","rtf","ods","odt","pages","ai","dxf","ttf","tiff","tif","wmf","es","ep"],
            "video"=>["og","o","mp4","webm","mp","mg","mov","3gp"],
            "audio"=>["og","o","mp3","mp","mg","wav"],
            "flash"=>["swf"],
            "pdf" =>["pdf"],
        ];
        $mapping = array_merge($mapping,$this->fileTypeExtensionMapping);
        return $mapping;
    }
    public function registerAssets()
    {
        $this->registerAssetBundle();

        if ($this->pluginOptions['required']) {
            $this->registerPlugin($this->pluginName, null, 'function(){var prev = $("input[name=\'" + $(this).attr("name") + "\']")[0]; if($(this)[0].files.length){$(prev).val($(this)[0].files[0].name)}}');
        } else {
            $this->registerPlugin($this->pluginName);
        }
    }
}