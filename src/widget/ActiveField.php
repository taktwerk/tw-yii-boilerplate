<?php
namespace taktwerk\yiiboilerplate\widget;

use taktwerk\yiiboilerplate\models\TwActiveRecord;
use \yii\bootstrap\ActiveField as YiiActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

class ActiveField extends YiiActiveField
{

    /**
     * Renders the opening tag of the field container.
     * @return string the rendering result.
     */
    public function begin()
    {
        if ($this->form->enableClientScript) {
            $clientOptions = $this->getClientOptions();
            if (!empty($clientOptions)) {
                $this->form->attributes[] = $clientOptions;
            }
        }

        $inputID = $this->getInputId();
        $attribute = Html::getAttributeName($this->attribute);
        $options = $this->options;
        $class = isset($options['class']) ? (array) $options['class'] : [];
        $class[] = "field-$inputID";

        if ($this->model->isAttributeRequired($attribute)) {
            $class[] = $this->form->requiredCssClass;
        } elseif(($this->model instanceof TwActiveRecord) && $this->model->isAttributeNeededMarkedByAsterisk($attribute)) {
            $class[] = $this->form->requiredCssClass; // set `required` class for all fields with `required` validation rule (even with `when` attribute)
        }
        $options['class'] = implode(' ', $class);
        if ($this->form->validationStateOn === ActiveForm::VALIDATION_STATE_ON_CONTAINER) {
            $this->addErrorClassIfNeeded($options);
        }
        $tag = ArrayHelper::remove($options, 'tag', 'div');

        return Html::beginTag($tag, $options);
    }
    
    /**
     * Returns the JS options for the field.
     * @return array the JS options
     */
    protected function getClientOptions()
    {
        $options = parent::getClientOptions();
        $options['id'] = $this->getCustomInputId();
        return $options;
    }

    /**
     * @return mixed
     */
    protected function getCustomInputId()
    {
        return (isset($this->selectors['input']) ? str_replace('#', '', $this->selectors['input']) : Html::getInputId($this->model, $this->attribute));
    }
}
