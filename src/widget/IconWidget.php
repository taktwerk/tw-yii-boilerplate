<?php
namespace taktwerk\yiiboilerplate\widget;

use yii\base\Widget;
use Yii;

class IconWidget extends Widget
{
    public $isImage = false;
    public $iconName;
    public $tagClass = '';
    public $tagStyles = '';
    protected $iconList;

    public function __construct()
    {
        if (Yii::$app->params && !empty(Yii::$app->params['iconAliases'])) {
            $this->iconList = Yii::$app->params['iconAliases'];
        }
    }

    /**
     * Renders the 3D model via three.js.
     * @return string the rendering result.
     */
    public function run()
    {
        if (!$this->iconName ||
            empty($this->iconList[$this->iconName])
        ) {
            return '';
        }
        $icon = $this->iconList[$this->iconName];
        $style = ($this->tagStyles)?"style=\"{$this->tagStyles}\" ":'';
        if ($this->isImage) {
            if (empty($icon['imageSrc'])) {
                return '';
            }
            return "<img class='{$this->tagClass}' $style
                         src='{$icon['imageSrc']}'
                    >";
        }
        if (empty($icon['iconClass'])) {
            return '';
        }

        return "<i class='{$this->tagClass} {$icon['iconClass']}' $style
                >
                </i>";
    }
}
