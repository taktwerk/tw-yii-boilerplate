<?php
/**
 * Copyright (c) 2016.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 11/17/2016
 * Time: 12:46 PM
 */
namespace taktwerk\yiiboilerplate\widget;

use taktwerk\yiiboilerplate\widget\assets\CustomCkeditorAssets;
use yii\base\Widget;
use yii\helpers\Html;
use Yii;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\web\JsExpression;
use yii\widgets\InputWidget;

class CKEditor5 extends InputWidget
{
    /**
     * @var string
     */
    public $uploadUrl = "upload-image";

    /**
     * @var string
     */
    public $preset = 'standard';
    /**
     * @var array
     */
    public $clientOptions = [];

    /**
     * @var array Toolbar options array
     */
    public $toolbar = [];

    /**
     * @var array
     */
    public $options = [];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        $js = <<<JS
    delete window.FileUploadAdapter;
    class FileUploadAdapter {
        constructor(loader, config) {
            this.uploadUrl = config.get('uploadUrl') || 'upload';
            this.uploadField = config.get('uploadField') || 'upload';
            this.loader = loader;
        }
    
        upload() {
            return this.loader.file.then(file => new Promise((resolve, reject) => {
                this._initRequest();
                this._initListeners( resolve, reject, file );
                this._sendRequest( file );
            }));
        }
    
        abort() {
            if (this.xhr) {
                this.xhr.abort();
            }
        }
    
        _initRequest() {
            const xhr = this.xhr = new XMLHttpRequest();
            xhr.open('POST', this.uploadUrl, true);
            xhr.setRequestHeader('X-CSRF-Token', yii.getCsrfToken());
            xhr.responseType = 'json';
        }
    
        _initListeners(resolve, reject, file) {
            const xhr = this.xhr;
            const loader = this.loader;
            const genericErrorText = 'Could not upload file: '+file.name;
    
            xhr.addEventListener('error', () => reject(genericErrorText));
            xhr.addEventListener('abort', () => reject() );
            xhr.addEventListener('load', () => {
                const response = xhr.response;
    
                if ( !response || response.error ) {
                    return reject( response && response.error ? response.error.message : genericErrorText );
                }
    
                resolve({
                    default: response.url
                });
            });
    
            if (xhr.upload) {
                xhr.upload.addEventListener('progress', evt => {
                    if (evt.lengthComputable) {
                        loader.uploadTotal = evt.total;
                        loader.uploaded = evt.loaded;
                    }
                });
            }
        }
    
        _sendRequest(file) {
            const data = new FormData();
            data.append(this.uploadField, file);
            data.append(yii.getCsrfParam(), yii.getCsrfToken());
            this.xhr.send(data);
        }
    }
JS;

        Yii::$app->view->registerJs($js);
        if(Yii::$app->params['editorOptions']['preset'] && !$this->preset){
            $this->preset = Yii::$app->params['editorOptions']['preset'];
        }

        parent::init(); // TODO: Change the autogenerated stub
    }

    /**
     *
     */
    protected function registerEditorJS()
    {
        if ($this->preset == 'basic') {
            $preset = require(__DIR__ . '/presets/basic_5.php');
        }elseif ($this->preset == 'full') {
            $preset = require(__DIR__ . '/presets/full_5.php');
        }elseif ($this->preset == 'standard') {
            $preset = require(__DIR__ . '/presets/standard_5.php');
        }else{
            $preset = $this->presetCustom($this->preset);
        }

        $this->clientOptions['toolbar'] = $preset;

        if (!empty($this->toolbar)) {
            $this->clientOptions['toolbar'] = $this->toolbar;
        }

        $clientOptions = Json::encode($this->clientOptions);

        $js = <<<JS
    $(".content-container").attr('style', 'width:'+($("div.document-editor").width()-15)+'px');
$(window).resize(function() {
    $(".content-container").attr('style', 'width:'+($("div.document-editor").width()-15)+'px');
});	    
function FileCustomUploadAdapterPlugin( editor ) {
				editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => {
				    editor.config.define('uploadUrl','{$this->uploadUrl}');
					return new FileUploadAdapter(loader, editor.config);
				};
			}
			var clientOptions = JSON.parse('$clientOptions');
			clientOptions['extraPlugins'] = [FileCustomUploadAdapterPlugin];
	DecoupledEditor.create( document.querySelector('#{$this->options['id']}-ck'),clientOptions )
		.then( editor => {
			const toolbarContainer = document.querySelector( '.field-{$this->options['id']} .ck-toolbar-container' );

			toolbarContainer.prepend( editor.ui.view.toolbar.element );

			window.editor = editor;
        if(window.ckEditor5==undefined){
            window.ckEditor5 = [];
            window.ckEditor5.instances = [];
        }
        window.ckEditor5.instances["{$this->options['id']}"] = window.editor;
window.ckEditor5.instances["{$this->options['id']}"].updateElement = function(){
         var data = $("#{$this->options['id']}-ck").html();
         $("#{$this->options['id']}").attr("value",data);
}; 
		} )
		.catch( err => {
			console.error( err );
		} );
	$(document).on('click blur focus change keyup DOMSubtreeModified', "#{$this->options['id']}-ck", function(event) {
	    var data = $("#{$this->options['id']}-ck").html();
         $("#{$this->options['id']}").attr("value",data);
    });	
	
$(document).on('DOMSubtreeModified', "#{$this->options['id']}", function(event) {
    var data = $("#{$this->options['id']}").val();
    if(data!=$("#{$this->options['id']}-ck").html()){
        window.editor.setData(data);
       
    }
});
JS;
        $this->view->registerJs($js);
    }


    /**
     * @param $preset
     * @return mixed
     */
    protected function presetCustom($preset)
    {
        $all_modules = [];
        if($this->preset){
            foreach (array_keys(Yii::$app->getModules(false)) as $module){
                $all_modules[] = $module;
                $preset_path = Yii::getAlias("@app/modules/$module/widgets/presets/{$preset}_5.php");
                if(file_exists($preset_path)){
                    $preset = require($preset_path);
                    return $preset;
                    break;
                }
            }
            return require(__DIR__ . '/presets/basic_5.php');
        }
    }


    /**
     * @inheritdoc
     */
    protected function printEditorTag()
    {
        if(!$this->options['name']){
            $this->options['name'] = StringHelper::basename(get_class($this->model))."[$this->attribute]";
        }
        $attr = $this->attribute;
        if ($this->hasModel()) {
            print '<div class="document-editor">
			<div class="ck-toolbar-container"></div>
			<div class="content-container">
				<div id="'.$this->options['id']."-ck".'">
				'.$this->model->$attr.'
				</div>
			</div>
		</div><input type="hidden" name="'.$this->options['name'].'" id="'.$this->options['id'].'">';
        } else {
            print Html::tag('div',"",['id'=>$this->options['id']."-ck"]).
            Html::input('hidden',$this->name, $this->value, $this->option);
        }
    }

    /**
     * @param \yii\web\View $view
     */
    protected function registerAssets($view)
    {
        $assets = CustomCkeditorAssets::register($view);

        if (array_key_exists('language',$this->clientOptions)) {
            $assets->js[] = 'translations/'.$this->clientOptions['language'].'.js';
        }
    }

    public function run()
    {
        $this->registerAssets($this->getView());
        $this->registerEditorJS();
        $this->printEditorTag();
    }

}
