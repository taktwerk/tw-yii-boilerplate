<?php
namespace taktwerk\yiiboilerplate\widget\form;

use yii\bootstrap\ActiveForm as AF;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\base\Widget;
use yii\bootstrap\Modal;
use yii\web\View;
use Yii;

class ActiveForm extends AF
{

    public $fieldClass = '\taktwerk\yiiboilerplate\widget\ActiveField';

    public $autoSave = true;

    /**
     *
     * @var array visit for the available options https://sisyphus-js.herokuapp.com/#customization
     */
    public $autoSaveOptions = [
    ];

    public function init()
    {
        parent::init();
        $view = $this->getView();
        
        FormAsset::register($view);
         if ($this->autoSave == true) {
             $this->registerAutoSaveJs();
         }
    }

    protected function registerAutoSaveJs()
    {
        $view = $this->getView();
        
        FormAutosaveAsset::register($view);
        $formId = $this->options['id'];
        $this->autoSaveOptions = ArrayHelper::merge([
            'locationBased' => true,
            'excludeFields' => "$('[name=_csrf]')"
        ], $this->autoSaveOptions);
        
        $mdlId = "modal-{$formId}";
        $mdTitle = Yii::t('twbp', 'Load the unsaved form data?');
        $mdMessage = Yii::t('twbp', 'Last time this form was filled but not saved. Do you want to load this draft?');
        $mdYes = Yii::t('twbp', 'Yes');
        $mdNo = Yii::t('twbp', 'No');
        $modal = <<<EOT
        <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="$mdlId">
        <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h2 class="text-center modal-title">$mdTitle</h2>
        </div>
        <div class="modal-body">
            $mdMessage
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-primary as-btn-yes">$mdYes</button>
        <button type="button" class="btn btn-danger as-btn-no">$mdNo</button>
        </div>
        </div>
        </div>
        </div>
EOT;
        
        $this->autoSaveOptions['onBeforeRestore'] = <<<EOT
        function(cl){
                var sisy = this;
                var modalConfirm = function(callback){
                    if(sisy.haveDataToRestore()){
                        $("#{$mdlId}").modal({backdrop: 'static', keyboard: false});
                    }
                    $("#{$mdlId} .as-btn-yes").on("click", function(){
                            callback(true);
                             $("#{$mdlId}").modal('hide');
                    });
    
                    $("#{$mdlId} .as-btn-no").on("click", function(){
                                callback(false);
                                $("#{$mdlId}").modal('hide');
                    });
            };
            modalConfirm(function(confirm){
                if(confirm){
                   sisy.restoreAllData();
                }
            });
return false;


}
EOT;
        foreach ($this->autoSaveOptions as $key => $val) {
            if (in_array($key, [
                'onSave',
                'onBeforeRestore',
                'onRestore',
                'onRelease',
                'excludeFields'
            ])) {
                $function = $val instanceof JsExpression ? $val : new JsExpression($val);
                $this->autoSaveOptions[$key] = $function;
            }
        }
        $options = Json::htmlEncode($this->autoSaveOptions);
        $view->registerJs(<<<EOT
            jQuery('form#{$formId}').sisyphus($options);
        jQuery("input, textarea").keyup(function(){
	       jQuery(this).blur();
	       jQuery(this).focus();
        });
        $("input[type=hidden]").bind("change", function() {
	       jQuery(this).blur();
	       jQuery(this).focus();
	       jQuery(this).trigger('blur');
	       jQuery(this).trigger('propertychange');
        });
EOT
);
        $view->on(View::EVENT_BEGIN_BODY, function () use ($modal) {
            echo $modal;
        });
    }
}