<?php

namespace taktwerk\yiiboilerplate\widget;

use taktwerk\yiiboilerplate\modules\import\widget\Importer;
use Yii;
use yii\base\Widget;
use yii\bootstrap\ButtonDropdown;

class GridOptionDropdownWidget extends Widget
{
    /**
     * @var string
     */
    public $dataProvider;

    /**
     * @var string
     */
    public $pjaxContainerId;

    /**
     * @var boolean
     */
    public $importer = false;

    /**
     * @var string
     */
    public $model = '';


    public function init()
    {
        $this->registerAssests();
        parent::init();
    }

    public function run()
    {
        return  (($this->importer && Yii::$app->user->can('import'))?Importer::widget([
                'dataProvider' => $this->dataProvider,
                'header' => Yii::t('twbp', 'Import'),
                'pjaxContainerId' => $this->pjaxContainerId,
                'toggleButton'=>[
                    'style' => 'border-bottom-right-radius: 3px; border-top-right-radius: 3px;display:none;',
                    'id'=>'import-action'
                ]
            ]):'').ButtonDropdown::widget([
                'encodeLabel' => false,
                'label' => '<i class="fa fa-gear"></i>',
                'dropdown' => [
                    'encodeLabels' => false,
                    'items' => [
                        \taktwerk\yiiboilerplate\widget\RestoreWidget::widget([
                            'pjaxContainerId' => $this->pjaxContainerId,
                            'model' => $this->model
                        ]),
                        [
                            'label' => '<span class="fa fa-upload"></span> '. Yii::t('twbp', 'Import'),
                            'url' => 'javascript::void()',
                            'visible'=> ($this->importer && Yii::$app->user->can('import')),
                            'options'=>[
                                'class'=>'import-dummy-btn'
                            ]
                        ]
                    ],
                    'options' => [
                        'class' => 'dropdown-menu-right', // right dropdown
                    ],
                ],
                'options' => [
                    'class' => 'btn-default btn-group'
                ],
            ]);
    }

    public function registerAssests(){
        $js = <<<JS
$(document).on('click', '.import-dummy-btn', function() {
   $('#import-action').click();
});
JS;
        Yii::$app->view->registerJs($js);

    }

}