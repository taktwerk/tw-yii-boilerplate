<?php

namespace taktwerk\yiiboilerplate\widget;

use Yii;
use yii\base\Widget;
use yii\helpers\Url;
use yii\web\View;

class RestoreWidget extends Widget
{
    /**
     * @var string
     */
    public $pjaxContainerId = '';

    /**
     * @var string
     */
    public $model = '';
    
    public function init()
    {
        $this->registerAsset();
        parent::init(); 
    }

    public function run()
    {
        $getParam = Yii::$app->get('userUi')->get('show_deleted', $this->model);

        $showDeletedString = Yii::t('twbp', 'Show Deleted');
        $hideDeletedString = Yii::t('twbp', 'Hide Deleted');

        $string = $getParam!=='1'?$showDeletedString:$hideDeletedString;
        $icon = $getParam!=='1'?'glyphicon glyphicon-eye-open':'glyphicon glyphicon-eye-close';
        $checked = $getParam!=='1' || !isset($getParam)?'value="yes"':'value="no"';

        return '<li class="restore-deleted-btn">
    <a href="javascript::void()" id="restore-deleted" '.$checked.'><i id="show-deleted-icon" class="'.$icon.'"></i> <span class="restore-delete-span">'.$string.'</span></a>
</li>';
    }

    public function registerAsset(){
        $showDeletedString = Yii::t('twbp', 'Show Deleted');
        $hideDeletedString = Yii::t('twbp', 'Hide Deleted');
        $url = Url::toRoute(Yii::$app->controller->id."/"."save-ui-data");
        $modelClass = str_replace("\\","\\\\",$this->model);
        $js = <<<JS
restore();

$(document).on("pjax:success", function(event){
    restore();
});
$(document).on("load", function(event){
    restore();
});

function ajaxReset(value){
    $.ajax({
      type: 'POST',
      url: "$url",
      data: {
          flag: "show_deleted",
          model: "$modelClass",
          value: value,
      },
      success: function() {
        window.location.href = location.href.replace(location.search, '');
      }
    });
}
function restore() {
    $('.restore-deleted-btn').on('click', function(e) {
        $('#show-deleted-icon').toggleClass('glyphicon glyphicon-eye-close', 'glyphicon glyphicon-eye-open');
        if($('#restore-deleted').attr('value')==='yes'){
          $('.restore-delete-span').text("$hideDeletedString");
          $('#restore-deleted').attr('value','no');
          ajaxReset("1");
          $.pjax.reload({container: "#$this->pjaxContainerId"});
        }else {
          $('.restore-delete-span').text("$showDeletedString");
          $('#restore-deleted').attr('value','yes');
          ajaxReset("0");
          $.pjax.reload({container: "#$this->pjaxContainerId"});
        }
    });
}
JS;
        $this->view->registerJs($js, View::POS_END);
    }
}