<?php

/**
 * @copyright Copyright &copy; Kartik Visweswaran, Krajee.com, 2014 - 2019
 * @package yii2-widgets
 * @subpackage yii2-widget-fileinput
 * @version 1.0.9
 */
namespace taktwerk\yiiboilerplate\widget\canvasinput;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\InputWidget;
use yii\web\View;
use yii\helpers\Json;
use yii\web\AssetBundle;
use taktwerk\yiiboilerplate\components\ImageHelper;

/**
 * Wrapper for the Bootstrap FileInput JQuery Plugin by Krajee.
 * The FileInput widget is styled for Bootstrap 3.x
 * & 4.x with ability to multiple file selection and preview, format button styles and inputs. Runs on all modern
 * browsers supporting HTML5 File Inputs and File Processing API. For browser versions IE9 and below, this widget
 * will gracefully degrade to a native HTML file input.
 *
 * @see http://plugins.krajee.com/bootstrap-fileinput
 * @see https://github.com/kartik-v/bootstrap-fileinput
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @since 2.0
 * @see http://twitter.github.com/typeahead.js/examples
 */
class CanvasInput extends InputWidget
{

    public $isAdvanceMode = false;
    /**
     * Whether to keep meta data of the advance mode canvas
     */
    public $keepMetaData = false;
    /**
    * This model attribute stores the canvas meta data for advance mode
    */
    public $metaDataAttribute;
    /**
     *
     * @var url of the current sign image
     */
    public $currentImageUrl = '';

    /**
     *
     * @var boolean whether to auto orient images on client side
     */
    public $autoOrientImages = true;
    
    public $drawToolOptions = [
        
    ];

    /**
     *
     * @var boolean whether to load sortable plugin to rearrange initial preview images on client side
     */
    public $sortThumbs = true;

    /**
     *
     * @var boolean whether to load dom purify plugin to purify HTML content in purfiy
     */
    public $purifyHtml = true;

    /**
     *
     * @var boolean whether to show 'plugin unsupported' message for IE browser versions 9 & below
     */
    public $showMessage = true;

    /*
     * @var array HTML attributes for the container for the warning
     * message for browsers running IE9 and below.
     */
    public $messageOptions = [
        'class' => 'alert alert-warning'
    ];

    /**
     * @inheritdoc
     */
    public $pluginName = 'fileinput';

    /**
     *
     * @var array the list of inbuilt themes
     */
    protected static $_themes = [
        'fa',
        'fas',
        'gly',
        'explorer',
        'explorer-fa',
        'explorer-fas'
    ];

    /**
     * @inheritdoc
     * 
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        return $this->initWidget();
    }
    protected function getDataURI($imgPath) {
        if($imgPath && Yii::$app->fs->has($imgPath)){
            $mimeType = Yii::$app->fs->getMimetype($imgPath);
            return 'data:'.$mimeType.';base64,'.base64_encode(file_get_contents(ImageHelper::getImageTmpPath($imgPath)));
        }
        return $imgPath;
    }
    /**
     * Initializes widget
     * 
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    protected function initWidget()
    {
        $asset = $this->registerAssets();
       if($this->isAdvanceMode==false)
       {
           return $this->initSignPad();
       }else{
           return $this->initImageEditor($asset);
       }
    }

    public function initImageEditor(AssetBundle $asset)
    {
        if ($this->keepMetaData == true && $this->metaDataAttribute==null){
            throw new \Exception(\Yii::t('twbp','Please specify the Meta Data Attribute to allow canvas editing at later stage'));
        }
        $uId = uniqid('');
        $input = Html::activeHiddenInput($this->model, $this->attribute, $this->options);
        $metaInput ='';
        if ($this->keepMetaData == true)
        {
            $metaInput = Html::activeHiddenInput($this->model, $this->metaDataAttribute);
        }
        $divId = 'canvas-' . $uId;
        $btnId = 'btn-'.$uId;
        $btnDeleteId = 'btn-delete-'.$uId;
        $img = '';
        $actionDivClass = '';
        if($this->currentImageUrl){
            $img = Html::img($this->currentImageUrl, ['class'=>'img-responsive']);
            $actionDivClass = 'canvas-img-actions';
        }
        $deleteBtnText = \Yii::t('twbp','Clear Drawing');
        $clearButton = "";
        if($this->metaDataAttribute && !empty($this->model->{$this->metaDataAttribute}))
        {
            $clearButton = '<button id="'.$btnDeleteId.'" type="button" class="toggle-image-fullscr btn btn-danger">'.$deleteBtnText.'</button>';  
        }
        // Image editor
        $btnText = \Yii::t('twbp','Open design editor');
     
             $html = <<<ET
<div id="can-section{$uId}" class="canvas-field-sec">
<div id="outer{$divId}" class="ie-container-outer">
{$metaInput}
{$input}
<div id="{$divId}">

  </div>
</div>
<div class="design-preview">
    $img
</div>
<div class="$actionDivClass">
<button id="{$btnId}" type="button" class="toggle-image-fullscr btn btn-warning">{$btnText}</button>$clearButton</div>
</div>
ET;
$url = $asset->baseUrl;
$canvasMetaData = '';
if($this->metaDataAttribute)
{
    $canvasMetaData = $this->model->{$this->metaDataAttribute};
}

$theme = require('imageEditorBlackTheme.php');


$comments = $this->model->extractComments($this->attribute);

if($comments && property_exists($comments,'loadImageFromField')){
    //$theme['loadButton.display'] = 'none';
}
else 
{
    $theme['loadButton.display'] = 'inline-block';
}
$width = 1920;
$height = 1080;

$img =/*  ($this->currentImageUrl && $this->keepMetaData==false)?$this->currentImageUrl: */ "data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22{$width}%22%20height%3D%22{$height}%22%20viewBox%3D%220%200%20{$width}%20{$height}%22%3E%3Cg%20id%3D%22Rectangle_1%22%20data-name%3D%22Rectangle%201%22%20fill%3D%22%23fff%22%20stroke%3D%22%23707070%22%20stroke-width%3D%221%22%3E%3Crect%20width%3D%22{$width}%22%20height%3D%22{$height}%22%20stroke%3D%22none%22%2F%3E%20%3Crect%20x%3D%220.5%22%20y%3D%220.5%22%20width%3D%221919%22%20height%3D%221079%22%20fill%3D%22none%22%2F%3E%3C%2Fg%3E%20%3C%2Fsvg%3E";
$currentImage = $this->model->getFileUrl($this->attribute, []);
$imgPath = $this->model->getPath($this->attribute, true);
$currentAttribute = $this->attribute;
if($comments && property_exists($comments,'loadImageFromField') && $comments->isAdvanceMode == true && empty($this->model->$currentAttribute)){
    $newAttribute = $comments->loadImageFromField;
    $imgPath = $this->model->getPath($newAttribute, true);
}

if(!empty($canvasMetaData)){
    $img = "";
}
else if(!empty($this->model->$newAttribute) && !empty($currentImage))
{
    $isVideo = ($this->model->getFileType($newAttribute) == 'video');
    $isPdf = ($this->model->getFileType($newAttribute) == 'pdf');
    $isImage = ($this->model->getFileType($newAttribute) == 'image');
    if ($isVideo || $isPdf) {
        $path = $this->model->getThumbFilePath($newAttribute);
        if(!empty($path))
        {
            
            $img = $currentImage;
           
        }
    }
    else if($isImage)
    {
        $img = $currentImage;
    }
}

$inputName = Html::getInputName($this->model, $this->attribute);
$metaInputName ='';
if($this->metaDataAttribute){
$metaInputName = Html::getInputName($this->model, $this->metaDataAttribute);
}

$img = $this->getDataURI($imgPath);
//prd($img);
$toolOptions= [
    "loadImage"=> [
        "path"=> $img,
        "name"=> 'design'
],
"theme"=> $theme, // or whiteTheme
"menuBarPosition"=> 'left',
"initMenu"=>'draw', /* init menu options => ['crop', 'flip', 'rotate', 'draw', 'shape', 'icon', 'text', 'mask', 'filter'] */
"defaultRange"=> 6,
"defaultColor"=> '#ff4040'
];
$this->drawToolOptions = ArrayHelper::merge($toolOptions,$this->drawToolOptions);

if(isset($this->drawToolOptions['defaultRange']) && is_numeric($this->drawToolOptions['defaultRange'])){
    $this->drawToolOptions['defaultRange'] = intval($this->drawToolOptions['defaultRange']);
}
$canvasMetaData = addslashes($canvasMetaData);
$jOptions = Json::encode($this->drawToolOptions);

       $js = <<<EOT
try{
        
        var cssMaxWidth = parseInt(window.innerWidth*0.68);
        var cssMaxHeight = parseInt(window.innerHeight*0.75);
        var imageEditor{$uId} = new tui.ImageEditor('#{$divId}', {
            includeUI: $jOptions,
            cssMaxWidth: cssMaxWidth,
            cssMaxHeight: cssMaxHeight,
            borderColor: 'grey',
            usageStatistics: false,
            inputTag: $('input[name="{$inputName}"]')[0],
            metaInputTag: $('[name="{$metaInputName}"]')[0]
        });

    var canJ = '{$canvasMetaData}';
    
    if(canJ){
            var canvasMeta = JSON.parse(canJ);
            if(canvasMeta.backgroundImage.src){
                urltoFile(canvasMeta.backgroundImage.src, 'temp.tmp').then(function(file){
                             imageEditor{$uId}.getActions().main.load(file).then(function(){
                                var iCanvas = imageEditor{$uId}._graphics.getCanvas();
                                var selectionStyle =imageEditor{$uId}._graphics.cropSelectionStyle; 
                                setTimeout(function(){
                                    iCanvas.lowerCanvasEl.style.border="double #2d2d2b";
                                    iCanvas.loadFromJSON(canvasMeta,function(){
var list  = iCanvas.getObjects();
list.forEach(function(obj){
            obj.set(selectionStyle);
});
});



                                },'200'); 
                             });
        
                });
            }

}
}catch(e){
    console.log(e,'EORR');
}

        $('#$btnId').click(function(){
            $('#outer{$divId}').toggleClass('showon-top');
        });
        $('#$btnDeleteId').click(function(){
            $('input[name="{$inputName}"]').val("");
            $('input[name="{$metaInputName}"]').val("");
            $(this).remove();
            $('#can-section{$uId} .design-preview').empty();
            $(this).remove();
        });
        window.onresize = function() {
                imageEditor{$uId}.ui.resizeEditor();
        }
EOT;
       \Yii::$app->view->registerJs($js,View::POS_READY);
        return $html;
    }
    
    public function initSignPad(){
        $uId = uniqid('');
        $divId = 'canvas-' . $uId;
        
        $showCanvas = true;
        $signImg = '';
        if ($this->currentImageUrl != '') {
            $showCanvas = false;
            $signImg = Html::img($this->currentImageUrl, [
                'alt' => 'Signature',
                'class' => 'sign-img'
            ]);
        }
        
        $cnvs = Html::tag('canvas', '', [
            'class' => 'draw-area',
            'style' => 'border:solid 1px grey;width: 100%;height: 100%;',
            'id' => $divId
        ]);
        $wrapperId = "signature-pad{$uId}";
        $input = Html::activeHiddenInput($this->model, $this->attribute, $this->options);
        $signPadStyle = $showCanvas ? '' : 'display:none;';
        $canvasActionsStyle = $showCanvas ? '' : 'display:none;';
        $canvasImgActionsStyle = $showCanvas ? 'display:none;' : '';
        $cancelButton = '';
        if ($showCanvas == false) {
            $cancelButton = <<<EOT
        <button type="button" class="button clear btn-warning" data-action="cancel">Cancel</button>
EOT;
        }
        $html = <<<ET
<div id="{$wrapperId}" class="signature-pad">
    <div class="signature-pad--body" style="{$signPadStyle}">
        {$cnvs}
    </div>
    <div class="signature-pad--footer">
      <div class="sign-demo">{$signImg}
</div>
      <div class="signature-pad--actions">
        <div class="canvas-actions" style="$canvasActionsStyle">
            <button type="button" class="button clear btn-danger" data-action="clear">Clear</button>
            $cancelButton
            <button type="button" class="button btn-primary" data-action="fullscreen">Full screen</button>
            <button type="button" class="button restore btn-primary" style="display:none;" data-action="restore_screen">Restore Screen</button>
            {$input}
        </div>
        <div class="canvas-img-actions" style="$canvasImgActionsStyle">
            <button type="button" class="button clear btn-warning" data-action="re_sign">Re-Sign</button>
        </div>
      </div>
    </div>
  </div>
ET;
            $inputName = Html::getInputName($this->model, $this->attribute);
            $editMode = ($showCanvas==false)?'true':'false';
            $js = <<<EOT
jQuery(function(){
    	initializeSignPad({
            canvas: $('#{$divId}')[0],
            wrapper:$('#{$wrapperId}')[0],
            input: $('input[name="{$inputName}"]')[0],
        });
});
EOT;
            \Yii::$app->view->registerJs($js);

            return $html;
    }
    /**
     * Validates and returns content based on IE browser version validation
     *
     * @param string $content
     * @param string $validation
     *
     * @return string
     */
    protected function validateIE($content, $validation = 'lt IE 10')
    {
        return "<!--[if {$validation}]><br>{$content}<![endif]-->";
    }

    /**
     * Registers the asset bundle and locale
     * 
     * @throws \yii\base\InvalidConfigException
     */
    public function registerAssetBundle()
    {}

    /**
     * Registers the needed assets
     * 
     * @throws \yii\base\InvalidConfigException
     */
    public function registerAssets()
    {
        $view = \Yii::$app->controller->getView();
        if ($this->isAdvanceMode == false){
            $asset = CanvasAsset::register($view);
        } else{
            $asset = CanvasImageEditorAsset::register($view);
        }
        return $asset;
        /*
         * $base = ltrim($asset->baseUrl,'/');
         * $js = <<<JS
         * $.fn.wPaint.menus.text.img = "{$base}"+'/'+$.fn.wPaint.menus.text.img;
         * $.fn.wPaint.menus.main.img = "{$base}"+'/'+$.fn.wPaint.menus.main.img;
         * JS;
         * $view->registerJs($js);
         */
    }
}