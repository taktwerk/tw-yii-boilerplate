<?php

namespace taktwerk\yiiboilerplate\widget\canvasinput;

/*
 * @link http://www.yiiframework.com/
 *
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

use yii\helpers\FileHelper;
use yii\web\AssetBundle;

/**
 * Configuration for `backend` client script files.
 *
 * @since 4.0
 */
class CanvasImageEditorAsset extends AssetBundle
{
    public $sourcePath = '@vendor/taktwerk/yii-boilerplate/src/widget/canvasinput/web/image-editor';
    //public $basePath ='@vendor/taktwerk/yii-boilerplate/src/widget/canvasinput/web';
    public $js = [
        "src/js/fabric.js",
        "dist/tui-code-snippet.min.js",
        "dist/tui-color-picker.js",
        "dist/FileSaver.min.js",
        'dist/tui-image-editor.js',
      //  'examples/js/theme/white-theme.js',
     //   'examples/js/theme/black-theme.js'
    ];

    public $css = [
        'dist/tui-image-editor.css',
        'dist/tui-color-picker.css',
        'style.css'
      //  'signature_pad/style.css'
    ];

    public $depends = [
        //'yii\jui\JuiAsset'
    ];

    public function init()
    {
        parent::init();
        
        // /!\ CSS/LESS development only setting /!\
        // Touch the asset folder with the highest mtime of all contained files
        // This will create a new folder in web/assets for every change and request
        // made to the app assets.
        if (getenv('APP_ASSET_FORCE_PUBLISH')) {
            $path = \Yii::getAlias($this->sourcePath);
            $files = FileHelper::findFiles($path);
            $mtimes = [];
            foreach ($files as $file) {
                $mtimes[] = filemtime($file);
            }
            touch($path, max($mtimes));
        }
        
    }
}
