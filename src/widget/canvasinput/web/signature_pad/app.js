function initializeSignPad(options){
	var defaultOptions = {
			canvas:'canvas',
			input:'',
			editMode:false
	}
	$.extend({}, defaultOptions, options);
	var wrapper = options.wrapper;
	var clearButton = wrapper.querySelector("[data-action=clear]");
	var initialInputValue = options.input.value;
	var initialImgSrc = $(wrapper).find('.sign-img').attr('src');
	var cancelButton = wrapper.querySelector("[data-action=cancel]");
	var reSignButton = wrapper.querySelector("[data-action=re_sign]");
	var changeColorButton = wrapper.querySelector("[data-action=change-color]");
	var undoButton = wrapper.querySelector("[data-action=undo]");
	var fullScreenBtn = wrapper.querySelector("[data-action=fullscreen]");
	var restoreScreenBtn = wrapper.querySelector("[data-action=restore_screen]");
	var savePNGButton = wrapper.querySelector("[data-action=save-png]");
	var saveJPGButton = wrapper.querySelector("[data-action=save-jpg]");
	var saveSVGButton = wrapper.querySelector("[data-action=save-svg]");
	var canvas = options.canvas;
	var signaturePad = new SignaturePad(options.canvas,{
	  // It's Necessary to use an opaque color when saving image as JPEG;
	  // this option can be omitted if only saving as PNG or SVG
	  backgroundColor: 'rgb(255, 255, 255)',
	  onEnd: function(){
		  	options.input.value = signaturePad.toDataURL();
	  }
	});
	
	// Adjust canvas coordinate space taking into account pixel ratio,
	// to make it look crisp on mobile devices.
	// This also causes canvas to be cleared.
	
	function resizeCanvas() {
		//console.log('TRIGGED');;
	  // When zoomed out to less than 100%, for some very strange reason,
	  // some browsers report devicePixelRatio as less than 1
	  // and only part of the canvas is cleared then.
		
	  var ratio =  Math.max(window.devicePixelRatio || 1, 1);
	
	  // This part causes the canvas to be cleared
	  canvas.width = canvas.offsetWidth * ratio;
	  canvas.height = canvas.offsetHeight * ratio;
	  canvas.getContext("2d").scale(ratio, ratio);

	  // This library does not listen for canvas changes, so after the canvas is automatically
	  // cleared by the browser, SignaturePad#isEmpty might still return false, even though the
	  // canvas looks empty, because the internal data of this library wasn't cleared. To make sure
	  // that the state of this library is consistent with visual state of the canvas, you
	  // have to clear it manually.
	  signaturePad.clear();
	}
	
	// On mobile devices it might make more sense to listen to orientation change,
	// rather than window resize events.
	//window.onresize = resizeCanvas;
	$( window ).resize(function(){
		options.input.value = '';
		setTimeout(function(){resizeCanvas();},300);
	});
	resizeCanvas();
	
	function download(dataURL, filename) {
	  var blob = dataURLToBlob(dataURL);
	  var url = window.URL.createObjectURL(blob);
	
	  var a = document.createElement("a");
	  a.style = "display: none";
	  a.href = url;
	  a.download = filename;
	
	  document.body.appendChild(a);
	  a.click();
	
	  window.URL.revokeObjectURL(url);
	}
	
	// One could simply use Canvas#toBlob method instead, but it's just to show
	// that it can be done using result of SignaturePad#toDataURL.
	function dataURLToBlob(dataURL) {
	  // Code taken from https://github.com/ebidel/filer.js
	  var parts = dataURL.split(';base64,');
	  var contentType = parts[0].split(":")[1];
	  var raw = window.atob(parts[1]);
	  var rawLength = raw.length;
	  var uInt8Array = new Uint8Array(rawLength);
	
	  for (var i = 0; i < rawLength; ++i) {
	    uInt8Array[i] = raw.charCodeAt(i);
	  }
	
	  return new Blob([uInt8Array], { type: contentType });
	}
	if(clearButton){
	clearButton.addEventListener("click", function (event) {
	  options.input.value = '';
	  signaturePad.clear();
	});}
	if(undoButton){
	undoButton.addEventListener("click", function (event) {
	  var data = signaturePad.toData();
	
	  if (data) {
	    data.pop(); // remove the last dot or line
	    signaturePad.fromData(data);
	  }
	});
	}
	
	if(changeColorButton){
		changeColorButton.addEventListener("click", function (event) {
		  var r = Math.round(Math.random() * 255);
		  var g = Math.round(Math.random() * 255);
		  var b = Math.round(Math.random() * 255);
		  var color = "rgb(" + r + "," + g + "," + b +")";
	
		  signaturePad.penColor = color;
	});
	}
	function canvasElementDisplay(show=true){
		var el = $(wrapper);
		if(show==true)
		{
			el.find('.canvas-img-actions').show();
			el.find('.canvas-actions').hide();
			el.find('.signature-pad--body').hide();	
			el.find('.sign-demo').show();
		}else{
			el.find('.canvas-img-actions').hide();
			el.find('.canvas-actions').show();
			el.find('.signature-pad--body').show();	
			el.find('.sign-demo').hide();
		}
	}
	
	if(cancelButton){
		cancelButton.addEventListener("click", function (event) {
			canvasElementDisplay(true);
			$(options.wrapper).find('.signature-pad--actions.canvas-fullscreen-actions').removeClass('canvas-fullscreen-actions')
			options.input.value = initialInputValue;
			$(options.wrapper).find('.sign-img').attr('src',initialImgSrc);
		});
	}
	if(savePNGButton){
	savePNGButton.addEventListener("click", function (event) {
	  if (signaturePad.isEmpty()) {
	    alert("Please provide a signature first.");
	  } else {
		  saveAndShowImage();
		 // var dataURL = signaturePad.toDataURL();
	    //dataURLToBlob(dataURL);
		//  options.input.value = dataURL;
	    //download(dataURL, "signature.png");
	  }
	});
	}
	function saveAndShowImage(){
		options.input.value = signaturePad.toDataURL();
		if(options.input.value =='data:,'){ //To handle the invalid data on resign
			options.input.value = '';	
		}
		  var img = $('<img />', {
			  src: options.input.value,
			  alt: 'Signature',
			  "class":'sign-img',
			  style:"max-width:"+$(options.wrapper).width()+'px;'
			});
		  var demo  = $(options.wrapper).find('.sign-demo');
		  	demo.find('.sign-img').remove();
		  	$(options.wrapper).find('.c-sign-desc').hide();
			img.prependTo(demo);
			$(options.wrapper).find('.signature-pad--actions').removeClass('canvas-fullscreen-actions')
			canvasElementDisplay(true);
	}
	
	
	if(saveJPGButton){
	saveJPGButton.addEventListener("click", function (event) {
	  if (signaturePad.isEmpty()) {
	    alert("Please provide a signature first.");
	  } else {
	    var dataURL = signaturePad.toDataURL("image/jpeg");
	    download(dataURL, "signature.jpg");
	  }
	});
	}
	if(fullScreenBtn){
		fullScreenBtn.addEventListener("click", function (event) {
			$(restoreScreenBtn).show();
			$(fullScreenBtn).hide();
			var acEl = $(options.wrapper).find('.signature-pad--actions');
			var cEl = $(options.canvas); 
			
			var cElOldStyle = cEl.attr('style');
			var cElOldWidth = cEl.attr('width');
			var cElOldHeight = cEl.attr('height');
			acEl.addClass('canvas-fullscreen-actions');
		
			cEl.data('old-style',cElOldStyle);
			cEl.data('old-height',cElOldHeight);
			cEl.data('old-width',cElOldWidth);
			cEl.attr('style',"border:solid 2px black;width: 100%;height: 100%;position: fixed; top: 0; left: 0;z-index: 9999;");
			setTimeout(function(){ resizeCanvas(); }, 100);
			
		});
	}
	function restoreSignScreen(){
		saveAndShowImage();
		$(restoreScreenBtn).hide();
		$(fullScreenBtn).show();
		var acEl = $(options.wrapper).find('.signature-pad--actions');
		var cEl =$(options.canvas);
		
		acEl.removeClass('canvas-fullscreen-actions');
		cEl.attr('width',cEl.data('old-width'));
		cEl.attr('height',cEl.data('old-height'));
		cEl.attr('style',cEl.data('old-style'));
		setTimeout(function(){ resizeCanvas(); }, 200);
	}
	if(restoreScreenBtn){
		restoreScreenBtn.addEventListener("click", function (event) {
			restoreSignScreen();
		});
	}
	if(reSignButton){
		reSignButton.addEventListener("click", function (event) {
			restoreSignScreen();
			options.input.value = ''; //To clear the white background image
			canvasElementDisplay(false);
		});
	}
	if(saveSVGButton){
	saveSVGButton.addEventListener("click", function (event) {
	  if (signaturePad.isEmpty()) {
	    alert("Please provide a signature first.");
	  } else {
	    var dataURL = signaturePad.toDataURL('image/svg+xml');
	    download(dataURL, "signature.svg");
	  }
	});
	}
}