<?php


namespace taktwerk\yiiboilerplate\widget;


class GoogleAnalytics extends \TomLutzenberger\GoogleAnalytics\GoogleAnalytics
{
    public function init()
    {
        parent::init();
        $this->gaId = getenv('GOOGLE_ANALYTICS_ID');
    }
}