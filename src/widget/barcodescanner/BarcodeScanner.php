<?php
namespace taktwerk\yiiboilerplate\widget\barcodescanner;

use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\View;
use yii\helpers\Html;
use yii\web\JsExpression;

class BarcodeScanner extends Widget
{

    public $pluginOptions = [];
    
    public $hidden = false;
    public $scanButtonOptions=[];    
    public $hiddenScanHtml = '<div class="text-center"><span class="text-primary">Looking for a barcode scan...</span></div>';
    
    public function run()
    {
        return $this->initWidget();
    }

    public function initWidget()
    {
        $defaultPluginOptions = [
            "scanButtonKeyCode"=>false,
            "scanButtonLongPressTime"=>500,
            "timeBeforeScanTest"=>100,
            "avgTimeByChar"=>30,
            "minLength"=>6,
            "suffixKeyCodes"=>[9,13],
            "prefixKeyCodes"=>[],
            "ignoreIfFocusOn"=>false,
            "stopPropagation"=>false,
            "preventDefault"=>false,
            "reactToKeydown"=>true,
            "reactToPaste"=>true,
            "singleScanQty"=>1,
            "reactToKeyDown"=>true,
            "onScan"=> new JsExpression('function(sScanned, iQty){
                 alert("Item: " +sScanned+ ", Qty: " + iQty);
            }')
        ];
        
        $this->pluginOptions = ArrayHelper::merge($defaultPluginOptions, $this->pluginOptions);
        $jOptions = Json::encode($this->pluginOptions);
        $uId = uniqid('');
        $js = <<<EOT
        function initOnScan{$uId}(){
            try {
        			onScan.attachTo(document, {$jOptions});
        		} catch(e) {
        			onScan.setOptions(document, {$jOptions});
        		}
        }

EOT;
        if($this->hidden){
            $js .= "initOnScan{$uId}();";
        }
        BarcodeScannerAsset::register(\Yii::$app->view);
        \Yii::$app->view->registerJs($js,View::POS_END);
        
        $btnId = 'scanbtn-' . $uId;
        $scanbtnOptions = ArrayHelper::merge(['class'=>'btn btn-primary','id'=>$btnId],$this->scanButtonOptions);
        $enableText = \Yii::t('twbp','Click to enable code scanner');
        $disableText = \Yii::t('twbp','Click to disable code scanner');
        $html = '';
        
        if($this->hidden==false){
            $html = Html::button($enableText,$scanbtnOptions);
            $btnId = $scanbtnOptions['id'];
            $enableText = addslashes($enableText);
            $disableText = addslashes($disableText);
            $toggleJs = <<<EOT
            $('#{$btnId}').click(function(){
                if(onScan.isAttachedTo(document)){
                    onScan.detachFrom(document);
                    $(this).removeClass('btn-danger').addClass('btn-primary');
                    $(this).text("{$enableText}");
                    $(this).data('scanEnabled',0);
                }else{
                    initOnScan{$uId}();
                    $(this).addClass('btn-danger').removeClass('btn-primary');
                    $(this).text("{$disableText}");
                    $(this).data('scanEnabled',1);
                }
            })
EOT;
            \Yii::$app->view->registerJs($toggleJs,View::POS_END);
        }else{
            
            $html = $this->hiddenScanHtml;
        }
        return $html;
    }
}