<?php

namespace taktwerk\yiiboilerplate\widget\barcodescanner;

/*
 * @link http://www.yiiframework.com/
 *
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

use yii\helpers\FileHelper;
use yii\web\AssetBundle;

/**
 * Configuration for `backend` client script files.
 *
 * @since 4.0
 */
class BarcodeScannerAsset extends AssetBundle
{
    public $sourcePath = '@vendor/taktwerk/yii-boilerplate/src/widget/barcodescanner/web/';
    public $js = [
        "js/onscan.min.js",
    ];

    public $css = [
        'css/style.css'
    ];

    public $depends = [
        'yii\web\JqueryAsset' 
    ];

    public function init()
    {
        parent::init();
        
        // /!\ CSS/LESS development only setting /!\
        // Touch the asset folder with the highest mtime of all contained files
        // This will create a new folder in web/assets for every change and request
        // made to the app assets.
        if (getenv('APP_ASSET_FORCE_PUBLISH')) {
            $path = \Yii::getAlias($this->sourcePath);
            $files = FileHelper::findFiles($path);
            $mtimes = [];
            foreach ($files as $file) {
                $mtimes[] = filemtime($file);
            }
            touch($path, max($mtimes));
        }
        
    }
}
