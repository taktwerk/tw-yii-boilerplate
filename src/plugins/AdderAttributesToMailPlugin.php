<?php

namespace taktwerk\yiiboilerplate\plugins;

use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;
use Swift_Events_SendListener;
use Swift_Events_SendEvent;

class AdderAttributesToMailPlugin implements Swift_Events_SendListener
{
    /**
     * @param Swift_Events_SendEvent $event
     */
    public function beforeSendPerformed(Swift_Events_SendEvent $event)
    {
        $message = $event->getMessage();

        if ($message->getContentType() === 'text/html') {
            $html = $message->getBody();
            $this->addClassMailToBody($html);
            $message->setBody($html);
        }
    }

    /**
     * @param Swift_Events_SendEvent $event
     */
    public function sendPerformed(Swift_Events_SendEvent $event)
    {
        // Do nothing after sending the message
    }

    protected function addClassMailToBody(&$html)
    {
    	$dom = new \DOMDocument;
        @$dom->loadHTML($html);
        $bodyElements = $dom->getElementsByTagName('body');
        if ($bodyElements && $bodyElements[0]) {
        	$body = $bodyElements[0];
        	$body->setAttribute('class','mail');
        	$html = $dom->saveHtml();
        }

        return $cssContent;
    }
}
