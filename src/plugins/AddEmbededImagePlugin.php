<?php

namespace taktwerk\yiiboilerplate\plugins;

use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;
use Swift_Events_SendListener;
use Swift_Events_SendEvent;

class AddEmbededImagePlugin implements Swift_Events_SendListener
{
	public $searchQuery;
    protected $alreadyEmbededImages;
    /**
     * @param Swift_Events_SendEvent $event
     */
    public function beforeSendPerformed(Swift_Events_SendEvent $event)
    {
        $message = $event->getMessage();

        if ($message->getContentType() === 'text/html') {
            $html = $message->getBody();
            $dom = new \DOMDocument;
        	@$dom->loadHTML($html);
            $XPath = new \DOMXPath($dom);
            $query = '//img';
            if ($this->searchQuery) {
            	$query .= $this->searchQuery;
            }
			$images = $XPath->query($query);
			foreach ($images as $image) {
				$imagePath = $image->getAttribute('src');
                if (!empty($alreadyEmbededImages[$imagePath])) {
                    $image->setAttribute('src', $alreadyEmbededImages[$imagePath]);
                } else if (@file_get_contents($imagePath)) {
					$embedFile = \Swift_EmbeddedFile::fromPath($imagePath);
					$image->setAttribute('src', $message->embed($embedFile));
                    $alreadyEmbededImages[$imagePath] = $message->embed($embedFile);
				} else {
					continue;
				}
			}
			$html = $dom->saveHtml();
            $message->setBody($html);
        }
        foreach ($message->getChildren() as $part) {
            if (strpos($part->getContentType(), 'text/html') === 0) {
                $html = $part->getBody();
                $dom = new \DOMDocument;
                @$dom->loadHTML($html);
                $XPath = new \DOMXPath($dom);
                $query = '//img';
                if ($this->searchQuery) {
                    $query .= $this->searchQuery;
                }
                $images = $XPath->query($query);
                foreach ($images as $image) {
                    $imagePath = $image->getAttribute('src');
                    if (!empty($alreadyEmbededImages[$imagePath])) {
                        $image->setAttribute('src', $alreadyEmbededImages[$imagePath]);
                    } else if (@file_get_contents($imagePath)) {
                        $embedFile = \Swift_EmbeddedFile::fromPath($imagePath);
                        $image->setAttribute('src', $message->embed($embedFile));
                        $alreadyEmbededImages[$imagePath] = $message->embed($embedFile);
                    } else {
                        continue;
                    }
                }
                $html = $dom->saveHtml();
                $part->setBody($html);
            }
        }
    }

    /**
     * @param Swift_Events_SendEvent $event
     */
    public function sendPerformed(Swift_Events_SendEvent $event)
    {
        // Do nothing after sending the message
    }
}
