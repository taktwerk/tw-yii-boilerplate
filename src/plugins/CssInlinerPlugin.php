<?php

namespace taktwerk\yiiboilerplate\plugins;

use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;
use Swift_Events_SendListener;
use Swift_Events_SendEvent;
use yii\helpers\Url;

class CssInlinerPlugin implements Swift_Events_SendListener
{
    /**
     * @var CssToInlineStyles
     */
    protected $converter;

    /**
     * @param CssToInlineStyles $converter
     */
    public function __construct(CssToInlineStyles $converter = null)
    {
        if ($converter) {
            $this->converter = $converter;
        } else {
            $this->converter = new CssToInlineStyles();
        }
    }

    /**
     * @param Swift_Events_SendEvent $event
     */
    public function beforeSendPerformed(Swift_Events_SendEvent $event)
    {
        $message = $event->getMessage();

        if ($message->getContentType() === 'text/html') {
            $html = $message->getBody();
            $cssContent = $this->getCssContentFromHtml($html);
            $message->setBody($this->converter->convert($html, $cssContent));
        }

        foreach ($message->getChildren() as $part) {
            if (strpos($part->getContentType(), 'text/html') === 0) {
                $html = $part->getBody();
                $cssContent = $this->getCssContentFromHtml($html);
                $part->setBody($this->converter->convert($html, $cssContent));
            }
        }
    }

    /**
     * @param Swift_Events_SendEvent $event
     */
    public function sendPerformed(Swift_Events_SendEvent $event)
    {
        // Do nothing after sending the message
    }

    protected function getCssContentFromHtml($html)
    {
        $dom = new \DOMDocument;
        @$dom->loadHTML($html);
        $links = $dom->getElementsByTagName('link');
        $cssLinks = [];
        foreach ($links as $link){
            if ($link->getAttribute('rel') === 'stylesheet') {
                $cssLinks[] = $link->getAttribute('href');
            }
        }
        $cssContent = '';
        foreach ($cssLinks as $cssLink) {
            $cssFileContent = @file_get_contents($cssLink);
            if (!$cssFileContent) {
                $cssFileContent = @file_get_contents(Url::base(true) . $cssLink);
            }
            if ($cssFileContent) {
                $cssContent .= $cssFileContent;
            }
        }

        return $cssContent;
    }
}
