<?php

namespace taktwerk\yiiboilerplate;

use Yii;
use codemix\localeurls\UrlManager as BaseUrlManager;

/**
 * Class UrlManager
 * @package taktwerk\yiiboilerplate
 */
class UrlManager extends BaseUrlManager
{
    // TODO: find why not setting this raises error
    //public $cache = null;

    public function init(){
        if(getenv('ALL_CACHE_DISABLED')){
            $this->cache = null;
        }
        parent::init();
    }
    public function getLanguagesForDropdown()
    {
        $languages = [];
        $currentUrl = Yii::$app->request->url;
        $currentLang = strtolower(Yii::$app->language);
        $active_languages = \taktwerk\yiiboilerplate\modules\translatemanager\models\Language::getActiveLanguages();

        foreach ($active_languages as $language) {
            if ($language != Yii::$app->language) {
                $url = str_ireplace("/$currentLang/", "/$language/", $currentUrl);
                // If we're on the index page, needs to be a different rule
                if (strtolower($currentUrl) == strtolower("/$currentLang")) {
                    $url = str_ireplace("/$currentLang", "/$language", $currentUrl);
                }

                $languages[] = [
                    'label' => $language,
                    'url' => $url
                ];
            }
        }

        return $languages;
    }

    /**
     * Checks for a language or locale parameter in the URL and rewrites the pathInfo if found.
     * If no parameter is found it will try to detect the language from persistent storage (session /
     * cookie) or from browser settings.
     *
     * @var \yii\web\Request $request
     */
    protected function processLocaleUrl($request)
    {
        parent::processLocaleUrl($request);

        // If not redirected by parent function, reset locale of formatter
        if (Yii::$app->has('formatter')) {
            Yii::$app->formatter->locale = Yii::$app->language;
            Yii::$app->formatter->language = Yii::$app->language;

            // Get the language id
            /*$lang = Language::findOne(['language' => Yii::$app->language, 'status' => 1]);
            if ($lang) {
                Yii::$app->formatter->locale = $lang->language_id;
            }*/
        }

        return;
    }
}
