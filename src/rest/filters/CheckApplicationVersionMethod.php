<?php


namespace taktwerk\yiiboilerplate\rest\filters;


use yii\base\ActionFilter;
use Yii;
use yii\web\HttpException;

class CheckApplicationVersionMethod extends ActionFilter
{
    public function beforeAction($action)
    {
        $mobileVersion = Yii::$app->params['mobileVersion'];
        
        if (!$mobileVersion) {
            return true;
        }
        $request = Yii::$app->getRequest();
        $requestVersion = $request->getHeaders()->get('X-VERSION-NUMBER');

        if (!$requestVersion || $requestVersion !== $mobileVersion) {
            throw new HttpException(426, 'Wrong Application Version');

            return false;
        }
        
        $mobileDatabaseVersion = Yii::$app->params['mobileDatabaseVersion'];
        if (!$mobileDatabaseVersion) {
            return true;
        }

        $requestDatabaseVersion = $request->getHeaders()->get('X-DATABASE-VERSION-NUMBER');

        if (!$requestDatabaseVersion || $requestDatabaseVersion !== $mobileDatabaseVersion) {
            throw new HttpException(426, 'Wrong Database Application Version');

            return false;
        }

        return true;
    }
}
