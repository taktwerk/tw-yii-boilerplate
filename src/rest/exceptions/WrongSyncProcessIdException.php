<?php

namespace taktwerk\yiiboilerplate\rest\exceptions;

use Exception;

/**
 * Class MissingCurrentDatetimeException
 * @package taktwerk\yiiboilerplate\rest\exceptions
 *
 * Used for notifying the client that they forgot the current datetime header
 */
class WrongSyncProcessIdException extends Exception
{

}
