<?php
/**
 * Taktwerk.ch 2016
 * tw-yii2-rest package
 */

namespace taktwerk\yiiboilerplate\rest\actions;

use app\models\MobileFile;
use yii;
use yii\rest\ViewAction as Action;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\rest\CreateQueryHelper;
use yii\web\Response;

/**
 * Class IndexAction
 * @package taktwerk\yiiboilerplate\rest\actions
 */
class UploadAction extends Action
{
    /**
     * Create or update several items at the same time.
     *
     * @param string $id
     * @return ActiveRecord
     */
    public function run($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        /* @var $model ActiveRecord */
        $model = $this->findModel($id);
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        if ($model && !empty($_FILES)) {
            foreach ($_FILES as $file => $data) {
                if (!$model->uploadFile(
                    $file,
                    [$file => $data['tmp_name']],
                    [$file => $data['name']]
                )) {
                    throw new \Exception('Couldn\'t upload file');
                }
            }
        }

        return $model->toArray();
    }
}
