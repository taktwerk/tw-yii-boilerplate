<?php
/**
 * Created by PhpStorm.
 * User: ben-g
 * Date: 05.02.2016
 * Time: 09:50
 */

namespace taktwerk\yiiboilerplate\rest;

use taktwerk\yiiboilerplate\models\User;
use taktwerk\yiiboilerplate\models\UserQuery;
use Da\User\Helper\SecurityHelper;
use Da\User\Form\LoginForm;
use yii\rest\Action;
use Yii;
use taktwerk\yiiboilerplate\components\ClassDispenser;

class LoginAction extends Action
{
    /**
     *
     */
    public function run()
    {
        /** @var LoginForm $model */
        $username = Yii::$app->request->post()['username'];
        $password = Yii::$app->request->post()['password'];

        $user = User::find()->where(['username'=>$username])->one();

        if(SecurityHelper::validatePassword($password, $user->password_hash)) {


            /** @var Token $token */
            $token = Token::find()->where(['user_id'=>$user->id])->one();

            if($token != null && !$token->isExpired)
                return ["access_token" => $token->code];

            $token = ClassDispenser::makeObject(Token::class);
            $token->user_id = $user->id;
            $token->type = ClassDispenser::getMappedClass(Token::class)::TYPE_RECOVERY;
            if(!$token->save()){
                die("ERROR: " . print_r($token->getErrors(),true));
            }

            Yii::$app->getResponse()->setStatusCode(200);

            return ["access_token" => $token->code];

        }
        else {
            // Bad request
            Yii::$app->getResponse()->setStatusCode(400);
        }
    }
}