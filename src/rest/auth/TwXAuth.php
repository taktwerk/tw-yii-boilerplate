<?php
/**
 * Created by PhpStorm.
 * User: konstantin.tretyak
 * Date: 24.03.2020
 * Time: 14:20
 */

namespace taktwerk\yiiboilerplate\rest\auth;

use taktwerk\yiiboilerplate\TwXAuth as BaseTwXAuth;
use yii\web\UnauthorizedHttpException;

class TwXAuth extends BaseTwXAuth
{
    /**
     * @inheritdoc
     */
    public function authenticate($user, $request, $response)
    {
        $userIdentifire = parent::authenticate($user, $request, $response);

        if (!$userIdentifire) {
            return null;
        }

        if ($userIdentifire->blocked_at) {
            $this->handleFailure(json_encode(['error' => 'User was blocked', 'user_id' => $userIdentifire->id]));
            return null;
        }
        $authItemLastChangedAt = $request->getHeaders()->get('X-Auth-Item-Last-Changed-At');
        if ($authItemLastChangedAt && !is_numeric($authItemLastChangedAt)) {
            $this->handleFailure(json_encode(['error' => 'Auth items was changed', 'user_id' => $userIdentifire->id]));
            return null;
        }
        if ($authItemLastChangedAt < $userIdentifire->password_changed_at ||
            $authItemLastChangedAt < $userIdentifire->role_changed_at
        ) {
            $this->handleFailure(json_encode(['error' => 'Auth items was changed', 'user_id' => $userIdentifire->id]));
            return null;
        }

        return $userIdentifire;
    }

    public function handleFailure($response)
    {
        throw new UnauthorizedHttpException($response);
    }
}
