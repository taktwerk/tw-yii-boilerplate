<?php


namespace taktwerk\yiiboilerplate\rest;

use taktwerk\yiiboilerplate\helpers\CorsFilterHelper;
use taktwerk\yiiboilerplate\rest\auth\TwXAuth;
use taktwerk\yiiboilerplate\rest\filters\CheckApplicationVersionMethod;
use yii\rest\Controller;
use taktwerk\yiiboilerplate\components\ClassDispenser;

class TwMobileApplicationController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $auth = $behaviors['authenticator'] = ['class' => ClassDispenser::getMappedClass(TwXAuth::class)];
        unset($behaviors['authenticator']);
        $behaviors['corsFilter'] = [
            'class' => CorsFilterHelper::class
        ];
        $behaviors['authenticator'] = $auth;
        $behaviors['authenticator']['except'] = ['options'];
        $behaviors['versionChecker'] = [
            'class' => ClassDispenser::getMappedClass(CheckApplicationVersionMethod::class),
            'except' => ['options'],
        ];

        return $behaviors;
    }
}
