<?php

namespace taktwerk\yiiboilerplate\helpers;

/**
 * Class DropdownItemHelper
 * @package taktwerk\yiiboilerplate\helpers
 */
class DropdownItemHelper
{
    /**
     * @param array $items
     * @return bool
     */
    public static function hasVisibleItems(array $items = [])
    {
        $visible = 0;
        foreach ($items as $item) {
            if (empty($item)) {
                continue;
            }
            if (isset($item['visible']) && $item['visible']) {
                $visible++;
            } else {
                $visible++;
            }
        }
        return $visible > 0;
    }
}
