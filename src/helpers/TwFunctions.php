<?php
/**
 * Functions that are available everywhere
 */

if (!function_exists('dd')) {
    function dd(...$args)
    {
        echo '<pre>';
        foreach ($args as $x) {
            var_dump($x);
        }
        echo '</pre>';
        die(1);
    }
}

if (!function_exists('dump')) {
    function dump(...$args)
    {
        echo '<pre>';
        foreach ($args as $x) {
            var_dump($x);
        }
        echo '</pre>';
    }
}
