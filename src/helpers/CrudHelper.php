<?php
namespace taktwerk\yiiboilerplate\helpers;

use Yii;
use yii\db\ActiveRecord;
use yii\db\ColumnSchema;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

class CrudHelper
{
    public static $defaultModelTableMap=[
/*        'full_table_name'=>'\app\models\FullTableName' //table name should be with prefix*/
    ];
    /**
     * @return array
     */
    public static function getMultiUploadFields($class)
    {   $model = new $class();
        $crudGenerator = new \taktwerk\yiiboilerplate\templates\crud\Generator();
        $safeAttributes = Yii::$app->db->getTableSchema($model->tableName)->columnNames;
        $out = [];
        foreach ($safeAttributes as $attribute) {
            $column = ArrayHelper::getValue(Yii::$app->db->getTableSchema($model->tableName)->columns, $attribute);
            if ($crudGenerator->checkIfMultiUploaded($column)) {
                $out[] = $attribute;
            }
        }
        return $out;
    }
    /**
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public static function getUploadFields($class)
    {
        /** @var \yii\db\ActiveRecord $model */
        $model = new $class();
        $model->setScenario('crud');
        $safeAttributes = $model->safeAttributes();
        if (empty($safeAttributes)) {
            $safeAttributes = $model->getTableSchema()->columnNames;
        }
        $out = [];
        foreach ($safeAttributes as $attribute) {
            $column = ArrayHelper::getValue($model->getTableSchema()->columns, $attribute);
            if (self::checkIfUploaded($column)) {
                $out[] = $attribute;
            }
        }
        return $out;
    }

    public static function getCanvasFields($class)
    {
        /** @var \yii\db\ActiveRecord $model */
        $model = new $class();
        $model->setScenario('crud');
        $safeAttributes = $model->safeAttributes();
        if (empty($safeAttributes)) {
            $safeAttributes = $model->getTableSchema()->columnNames;
        }
        $out = [];
        foreach ($safeAttributes as $attribute) {
            $column = ArrayHelper::getValue($model->getTableSchema()->columns, $attribute);
            if (self::checkIfCanvas($column)) {
                $out[] = $attribute;
            }
        }
        return $out;
    }

    /**
     *
     * @param ColumnSchema $column
     * @return bool
     */
    public static function checkIfCanvas(ColumnSchema $column)
    {
        $comment = self::extractComments($column);
        if (preg_match('/(_canvas)$/i', $column->name) || ($comment && (@$comment->inputtype === 'canvas'))) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param ColumnSchema $column
     * @return bool
     */
    public static function checkIfUploaded(ColumnSchema $column)
    {
        
        $comment = self::extractComments($column);
        if (preg_match('/(_upload|_file)$/i', $column->name) || ($comment && ($comment->inputtype === 'upload' || $comment->inputtype === 'file'))) {
            return true;
        }
        return false;
    }
    /**
     *
     * @param ColumnSchema $column
     * @return bool
     */
    public static function checkIfMultiUploaded(ColumnSchema $column)
    {
        $comment = self::extractComments($column);
        if (preg_match('/(_media)$/i', $column->name) || ($comment && (@$comment->inputtype === 'uploads' || @$comment->inputtype === 'files'))) {
            return true;
        }
        return false;
    }

    /**
     * Extract comments from database
     *
     * @param
     *            $column
     * @return bool|mixed
     */
    public static function extractComments($column)
    {
        $output = json_decode($column->comment);
        if (is_object($output)) {
            return $output;
        }
        return false;
    }

    /**
     * get the full name (name \ namespace) of a class from its file path
     * result example: (string) "I\Am\The\Namespace\Of\This\Class"
     *
     * @param
     *            $filePathName
     *            
     * @return string
     */
    public static function getClassFullNameFromFile($filePathName)
    {
        $name = self::getClassNameFromFile($filePathName);
        if ($name == null) {
            return null;
        }
        return self::getClassNamespaceFromFile($filePathName) . '\\' . $name;
    }

    /**
     * build and return an object of a class from its file path
     *
     * @param
     *            $filePathName
     *            
     * @return mixed
     */
    public static function getClassObjectFromFile($filePathName)
    {
        $classString = self::getClassFullNameFromFile($filePathName);
        if ($classString == null) {
            return null;
        }
        $object = new $classString();
        
        return $object;
    }
    /**
     *Finds and returns matching model class path in BP or App directories. 
     */
    public static function findModelClass($modelName,$tableName){
        $defaultMapping = self::$defaultModelTableMap;
        try{
            $envMapping = Json::decode(getenv('DEFAULT_MODEL_TABLE_MAP'));
            if(is_array($envMapping))
            {
                $defaultMapping = array_merge($defaultMapping,$envMapping);
            }
        }catch(\Exception $e){
            \Yii::error('DEFAULT_MODEL_TABLE_MAP in env file is not in json format');
        }
        
        if(isset($defaultMapping[$tableName])){
            return $defaultMapping[$tableName];
        }
        /* Model searching in boilerplate files */
        $di = new \RecursiveDirectoryIterator(\Yii::getAlias('@taktwerk-boilerplate'), \FilesystemIterator::KEY_AS_PATHNAME | \FilesystemIterator::CURRENT_AS_SELF);
        foreach (new \RecursiveIteratorIterator($di) as $filename => $file) {
            if($file->isDot()){
                continue;
            }
            $path = $file->getPath();
            if(strpos($path, 'models') !== false &&
                strpos($path, FileHelper::normalizePath('models/mobile')) === false && 
                strpos($path, FileHelper::normalizePath('models/base')) === false &&
                strpos($path, FileHelper::normalizePath('models/search')) === false &&
                $file->getExtension()=='php' && $file->getFilename()==$modelName.'.php' ){
                $instance = self::getClassObjectFromFile($filename);
                if($instance && $instance instanceof ActiveRecord && $instance->tableSchema->name==$tableName){
                    return '\\'.get_class($instance);
                }
            }
        }
        
        /* Model searching in Project files */
        $di = new \RecursiveDirectoryIterator(\Yii::getAlias('@app'), \FilesystemIterator::KEY_AS_PATHNAME | \FilesystemIterator::CURRENT_AS_SELF);
        foreach (new \RecursiveIteratorIterator($di) as $filename => $file) {
            if($file->isDot()){
                continue;
            }
            $path = $file->getPath();
            if(strpos($path, 'models') !== false && strpos($path, FileHelper::normalizePath('models/mobile')) === false &&
                strpos($path, FileHelper::normalizePath('models/base')) === false && strpos($path, FileHelper::normalizePath('models/search')) === false
                && $file->getExtension()=='php' && $file->getFilename()==$modelName.'.php'
                )
            {
                
                $instance = self::getClassObjectFromFile($filename);
                if($instance && $instance instanceof ActiveRecord && $instance->tableSchema->name==$tableName){
                    return '\\'.get_class($instance);
                }
                
            }
        }
        return null;
    }

    /**
     * get the class namespace form file path using token
     *
     * @param
     *            $filePathName
     *            
     * @return null|string
     */
    protected static function getClassNamespaceFromFile($filePathName)
    {
        $src = file_get_contents($filePathName);
        
        $tokens = token_get_all($src);
        $count = count($tokens);
        $i = 0;
        $namespace = '';
        $namespace_ok = false;
        while ($i < $count) {
            $token = $tokens[$i];
            if (is_array($token) && $token[0] === T_NAMESPACE) {
                // Found namespace declaration
                while (++ $i < $count) {
                    if ($tokens[$i] === ';') {
                        $namespace_ok = true;
                        $namespace = trim($namespace);
                        break;
                    }
                    $namespace .= is_array($tokens[$i]) ? $tokens[$i][1] : $tokens[$i];
                }
                break;
            }
            $i ++;
        }
        if (! $namespace_ok) {
            return null;
        } else {
            return $namespace;
        }
    }

    /**
     * get the class name form file path using token
     *
     * @param
     *            $filePathName
     *            
     * @return mixed
     */
    protected static function getClassNameFromFile($filePathName)
    {
        $php_code = file_get_contents($filePathName);
        
        $classes = array();
        $tokens = token_get_all($php_code);
        $count = count($tokens);
        for ($i = 2; $i < $count; $i ++) {
            if ($tokens[$i - 2][0] == T_CLASS && $tokens[$i - 1][0] == T_WHITESPACE && $tokens[$i][0] == T_STRING) {
                
                $class_name = $tokens[$i][1];
                $classes[] = $class_name;
            }
        }
        return @$classes[0];
    }

    /**
     * @param $model
     * @param bool $useModal
     * @param $relatedForm
     * @param $multiple
     * @param $message_category
     * @param $url_params
     * @return string
     */
    public static function formButtons($model, $useModal=false, $relatedForm, $multiple, $message_category, $url_params){

        $controller = Yii::$app->controller->module->id . '_' . Yii::$app->controller->id ;
        $crud_btns = Yii::$app->controller->crudMainButtons;
        $str = '';
        $str .=  '<div class="col-md-12  form-action-group"';
        $str .= (!$relatedForm ? 'id="main-submit-buttons"' : '').'>';
        if($crud_btns['createsave']){
            $str .= Html::submitButton(
                '<span class="glyphicon glyphicon-check"></span> ' .
                ($model->isNewRecord && !$multiple ?
                    Yii::t($message_category, 'Save') :
                    Yii::t($message_category, 'Save')),
                [
                    'id' => 'save-' . $model->formName(),
                    'class' => 'btn btn-success',
                    'name' => 'submit-default'
                ]
            );
        }

        if ((!$relatedForm && !$useModal) && !$multiple) {
            if (Yii::$app->getUser()->can($controller . '_create')){
                if ($crud_btns['createnew']) {
                    $str .= ' '.Html::submitButton(
                        '<span class="glyphicon glyphicon-check"></span> ' .
                        ($model->isNewRecord && !$multiple ?
                            Yii::t($message_category, 'Save & New') :
                            Yii::t($message_category, 'Save & New')),
                        [
                            'id' => 'submit-new-' . $model->formName(),
                            'class' => 'btn btn-default',
                            'name' => 'submit-new'
                        ]
                    );
                }
                if ($crud_btns['createclose']) {
                    $str .= ' '.Html::submitButton(
                        '<span class="glyphicon glyphicon-check"></span> ' .
                        ($model->isNewRecord && !$multiple ?
                            Yii::t($message_category, 'Save & Close') :
                            Yii::t($message_category, 'Save & Close')),
                        [
                            'id' => 'submit-close-' . $model->formName(),
                            'class' => 'btn btn-default',
                            'name' => 'submit-close'
                        ]
                    );
                }
            }
        $str .= self::deleteButton($model, $message_category, $url_params);
        } elseif ($multiple) {
            $str .= ' '.Html::a(
                '<span class="glyphicon glyphicon-exit"></span> ' .
                Yii::t($message_category, 'Close'),
                $useModal ? false : [],
                [
                    'id' => 'close-' . $model->formName(),
                    'class' => 'btn btn-danger' . ($useModal ? ' closeMultiple' : ''),
                    'name' => 'close'
                ]
            );
        } else {
            $str .= ' '.Html::a(
                '<span class="glyphicon glyphicon-exit"></span> ' .
                Yii::t($message_category, 'Close'),
                ['#'],
                [
                    'class' => 'btn btn-danger',
                    'data-dismiss' => 'modal',
                    'name' => 'close'
                ]
            );
        }
        $str .= '</div>';
        return $str;
    }

    /**
     * @param $model
     * @param $message_category
     * @return string
     */
    public static function deleteButton($model, $message_category, $url_params, $type='controller'){

        $str = '';
        $controller = Yii::$app->controller->module->id . '_' . Yii::$app->controller->id ;
        $crud_btns = Yii::$app->controller->crudMainButtons;
                
        if (!$model->isNewRecord && Yii::$app->getUser()->can($controller . '_delete') && $model->deletable() && !$model->restorable() && $crud_btns['delete']) {
            $str .= ' '.Html::a(
                        '<span class="glyphicon glyphicon-trash"></span> ' . Yii::t($message_category, 'Delete'),
                        $useModal ? false : ArrayHelper::merge($url_params,['delete','fromRelation' => $fromRelation]),
                    $type=='view'?[
                            'class' => 'btn btn-danger' . ($useModal ? ' ajaxDelete' : ''),
                            'data-url' => Url::toRoute( ArrayHelper::merge($url_params,['delete','fromRelation' => $fromRelation])),
                            'data-confirm' => $useModal ? false : Yii::t($message_category, 'Are you sure to delete this item?'),
                            'data-method' => $useModal ? false : 'post',
                        ]:
                    [
                        'class' => 'btn btn-danger',
                        'data-confirm' => '' . Yii::t($message_category, 'Are you sure to delete this item?') . '',
                        'data-method' => 'post',
                    ]
                    );
        }
        if ($model->restorable() && Yii::$app->getUser()->can($controller . '_delete') &&  $crud_btns['restore']) {
            $str .= ' '.Html::a(
                    '<span class="glyphicon glyphicon-trash"></span> ' .
                    Yii::t($message_category, 'Restore'),
                    ArrayHelper::merge($url_params,[
                        'delete',
                        'show_deleted'=>Yii::$app->request->get('show_deleted'),
                        'fromRelation' => $fromRelation
                    ]),
                    [
                        'class' => 'btn btn-success',
                        'data-confirm' => '' . Yii::t($message_category, 'Are you sure to restore this item?') . '',
                        'data-method' => 'post',
                    ]
                );
        }
        
        return $str;
        
    }
}
