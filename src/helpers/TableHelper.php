<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace taktwerk\yiiboilerplate\helpers;


/**
 * @author Samir Dixit <samirdixit0@gmail.com>
 */
class TableHelper 
{
    /**
     * @inheritdoc
     */
    public static function getTableComment($tbl_name, $asArray = true)
    {
        try {
            $database = \Yii::$app->db->createCommand("SELECT DATABASE()")->queryScalar();
            $query = "SELECT table_comment FROM INFORMATION_SCHEMA.TABLES
                        WHERE table_name='" . $tbl_name . "' AND table_schema='" . $database . "';";

            $dataRaw = \Yii::$app->db->createCommand($query)->queryOne();
            $tableComment = null;
            if (!empty($dataRaw['table_comment'])) {
                $tableComment = $dataRaw['table_comment'];
            } else if (!empty($dataRaw['TABLE_COMMENT'])) {
                $tableComment = $dataRaw['TABLE_COMMENT'];
            }

            if ($tableComment && $asArray) {
                $data = json_decode($tableComment, true);
                
                return $data;
            }

            return $tableComment;
        }  catch (\Exception $e) {
            // Do nothing
        }

        return $tableComment;
    }
}
