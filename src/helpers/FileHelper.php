<?php
namespace taktwerk\yiiboilerplate\helpers;

use Exception;
use Yii;
use yii\helpers\StringHelper;
use yii\helpers\FileHelper as DefaultFileHelper;
use taktwerk\yiiboilerplate\components\Helper;
use taktwerk\yiiboilerplate\commands\FileCompressor;
use yii\db\ActiveRecord;
use taktwerk\yiiboilerplate\models\TwActiveRecord;
use Illuminate\Support\Str;
use taktwerk\yiiboilerplate\components\ClassDispenser;
/**
 * Class FileHelper
 *
 * @package taktwerk\yiiboilerplate\helpers
 */
class FileHelper
{
    /**
     * Determine if a file has a specified extension or not.
     *
     * @param
     *            $filename
     * @param array $extensions
     */
    public static function hasExtension($filename, $extensions = [])
    {
        $extensions = array_map('strtolower', $extensions);
        $extension = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
        return in_array($extension, $extensions);
    }

    /**
     * Encrypt a file
     *
     * @param
     *            $filename
     * @param string $cipher
     * @return bool
     * @throws Exception
     */
    public static function encrypt($filename, $cipher = 'aes-128-gcm')
    {
        if (! file_exists($filename)) {
            throw new Exception("File '$filename' not found");
        }
        
        if (file_exists($filename) and ! is_writable($filename)) {
            throw new Exception("File '$filename' is not writable");
        }
        
        if (! in_array($cipher, openssl_get_cipher_methods())) {
            throw new Exception("Cipher '$cipher' is not supported.");
        }
        
        $content = file_get_contents($filename);
        if (! $content) {
            throw new Exception("Problem while reading file '$filename'");
        }

        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivlen);
        $encrypted = openssl_encrypt($content, $cipher, "twRandomKey$2017", 0, $iv);

        $result = file_put_contents($filename, $encrypted);
        if (! $result) {
            throw new Exception("Problem while writing file '$filename'");
        }

        return true;
    }

    /**
     * Encrypt a file
     *
     * @param
     *            $filename
     * @param string $cipher
     * @return bool
     * @throws Exception
     */
    public static function decrypt($filename, $cipher = 'aes-128-gcm')
    {
        if (! file_exists($filename)) {
            throw new Exception("File '$filename' not found");
        }

        if (file_exists($filename) and ! is_writable($filename)) {
            throw new Exception("File '$filename' is not writable");
        }

        if (! in_array($cipher, openssl_get_cipher_methods())) {
            throw new Exception("Cipher '$cipher' is not supported.");
        }

        $content = file_get_contents($filename);
        if (! $content) {
            throw new Exception("Problem while reading file '$filename'");
        }

        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivlen);
        $content = openssl_decrypt($content, $cipher, "twRandomKey$2017", 0, $iv);

        return $content;
    }

    /**
     *
     * @return float|int
     */
    public static function maxFileSize()
    {
        static $max_size = - 1;

        if ($max_size < 0) {
            // Start with post_max_size.
            $post_max_size = self::parseSize(ini_get('post_max_size'));
            if ($post_max_size > 0) {
                $max_size = $post_max_size;
            }
            
            // If upload_max_size is less, then reduce. Except if upload_max_size is
            // zero, which indicates no limit.
            $upload_max = self::parseSize(ini_get('upload_max_filesize'));
            if ($upload_max > 0 && $upload_max < $max_size) {
                $max_size = $upload_max;
            }
        }
        return $max_size;
    }

    /**
     *
     * @param
     *            $size
     * @return float
     */
    public static function parseSize($size)
    {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
        $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
        if ($unit) {
            // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        } else {
            return round($size);
        }
    }
    public static function compressFileExtension($fileType='video'){
        if($fileType=='video'){
            return 'mp4';
        }
        if($fileType=='audio'){
            if(getenv('compressed_audio_format')){
                return getenv('compressed_audio_format');
            }
            return 'mp3';
        }
        if($fileType=='3d'){
            return 'glb';
        }
        return null;
    }
    /**
     * Get a asset file's cache path
     *
     * @param
     *            $path
     * @param string $bucket
     * @return string
     */
    public static function fileCachePath($path, $bucket = 'fs'): string
    {
        // calculate cache
        $modification_time = Yii::$app->$bucket->getTimestamp($path);
        $cache_key = md5(serialize(compact('modification_time', 'path', 'bucket')));
        $cache_path = dirname($path) . '/' . $cache_key . '/' . basename($path);

        return $cache_path;
    }
    public static function PdfToImageByModel(TwActiveRecord $model,$atr){
        $data = [
            'success' => false
        ];
        $bucket = 'fs';
        if ($model == null) {
            $data['success'] = false;
            $data['error'] = 'Model not found';
        } else {
            $filePath = $model->getUploadPath() . $model->$atr;
            $fileTempPath = ClassDispenser::getMappedClass(Helper::class)::getLocalTempFile($filePath);
            
            if (! is_file($fileTempPath)) {
                $data['success'] = false;
                $data['error'] = 'Model not found';
            } else {
                
                $mimeType = DefaultFileHelper::getMimeType($fileTempPath);
                if (preg_match('/pdf\/*/', $mimeType)) {
                    $compressFilePath = self::makePdfToImage($fileTempPath);
                    if (empty($compressFilePath['error'])) {
                        if (! empty($compressFilePath['detail']) && file_exists($compressFilePath['detail'])){
                            $newFile = self::getCompressedFilePathByAttribute($model, $atr,'pdf');
                            $stream = fopen($compressFilePath['detail'], 'r+');
                            \Yii::$app->$bucket->putStream($newFile, $stream);
                            fclose($stream);
                            $data['name'] = basename($newFile);
                            DefaultFileHelper::unlink($compressFilePath['detail']);
                        }
                        $data['success']=true;
                        if($model->hasAttribute('updated_at')){
                            $model->updateAttributes(['updated_at' => date('Y-m-d H:i:s')]);
                        }
                    }else{
                        $data['error'] = $compressFilePath['error'];
                    }
                }
            }
        }
            return $data;
    }
    /**
     * @desc Checks if a file has converted or all compressed files
     * In case if a file is a VIDEO it should have the compressed video and its thumnail.
     * In case if a file is a AUDIO it should have the compressed audio.
     * In case if a file is a PDF it should have a pdf thumbnail
     * @return Boolean
     * */
    public static function hasAllCompressedFiles(TwActiveRecord $model,$attribute){
        $fileType = $model->getFileType($attribute);
        
        $hasCompressedVideo = self::hasCompressedFile($model, $attribute,$fileType); //check compressed video thumbnail
        
        if($fileType=='pdf'){
            return self::hasCompressedFile($model, $attribute,'pdf');
        }
        if($fileType=='video'){
            $hasThumb = self::hasCompressedFile($model, $attribute);//check thumbnail
            return ($hasThumb && $hasCompressedVideo);
        }
        return $hasCompressedVideo;
    }
    /**
     * @desc Copies all the compressed/convered files of a model attribute
     * @return Boolean
    */    
    public static function copyCompressedFilesFromModelToModel(TwActiveRecord $sourceModel,TwActiveRecord $model, $attribute){
        $fileType = $model->getFileType($attribute);
        $isCopied = self::copyCompressedFileFromModelToModel($sourceModel, $model, $attribute,$fileType); //copies compressed video
        
        if($isCopied && $fileType=='video'){
            $thumbCopied = self::copyCompressedFileFromModelToModel($sourceModel,$model, $attribute); //copy thumbnail in case of video
            return ($isCopied && $thumbCopied);
        }
        return $isCopied;
    }
    public static function hasCompressedFile($model,$attribute,$type='thumbnail'){
        $filePath = DefaultFileHelper::normalizePath($model->getUploadPath() . $model->$attribute);
        $baseName = pathinfo($filePath)['filename'];
        $ext = self::compressFileExtension($type);
        if($type=='thumbnail')
        {
            return Yii::$app->fs->has($model->getUploadPath() . $baseName . FileCompressor::COMPRESSED_THUMB_POSTFIX);
        }
        if($type=='pdf')
        {
            return Yii::$app->fs->has($model->getUploadPath() . $baseName . FileCompressor::COMPRESSED_PDF_THUMB_POSTFIX);
        }
        if($ext){
            return Yii::$app->fs->has($model->getUploadPath() . $baseName . FileCompressor::COMPRESSED_FILE_POSTFIX . '.' . $ext);
        }
        return false;
    }
    public static function copyFileWithinStorage($sourceFilePath,$destinationFilePath){
        if (Yii::$app->fs->has($sourceFilePath)) {
            $tempStream = Yii::$app->fs->readStream($sourceFilePath);
            Yii::$app->fs->putStream($destinationFilePath, $tempStream);
            return true;
        }
        return false;
    }
    /**
     * This function copies the compressed/converts files of a model attribute to a diffrent model
     **/
    public static function copyCompressedFileFromModelToModel(TwActiveRecord $sourceModel,TwActiveRecord $destinationModel,$attribute,$type='thumbnail'){
        if($type=='thumbnail')
        {
            $thumbPath = $sourceModel->getThumbFilePath($attribute);
            if ($thumbPath) {
                $tempThumbPath = self::getCompressedFilePathByAttribute($destinationModel, $attribute);
                return self::copyFileWithinStorage($thumbPath, $tempThumbPath);
            }
            return false;
        }
        else if($type)
        {
            $compressedVidPath = self::getCompressedFilePathByAttribute($sourceModel, $attribute, $type);
            if ($compressedVidPath){
                $tempVidPath = self::getCompressedFilePathByAttribute($destinationModel, $attribute, $type);
                return self::copyFileWithinStorage($compressedVidPath, $tempVidPath);
            }
            return false;
        }
        return false;
    }
    /**
     * This function returns the suggested path(and name) of the thumbnail file for Videos
    **/
    public static function getCompressedFilePathByAttribute($model,$attribute,$type='thumbnail'){
        $filePath = DefaultFileHelper::normalizePath($model->getUploadPath() . $model->$attribute);
        $baseName = pathinfo($filePath)['filename'];
        $ext = self::compressFileExtension($type);
        if($type=='thumbnail')
        {
            return $model->getUploadPath() . $baseName . FileCompressor::COMPRESSED_THUMB_POSTFIX;
        }
        if($type=='pdf'){//For PDF a png thumbnail is generated
            return $model->getUploadPath() . $baseName . FileCompressor::COMPRESSED_PDF_THUMB_POSTFIX;
        }
        if($ext)
        {
            return $model->getUploadPath() . $baseName . FileCompressor::COMPRESSED_FILE_POSTFIX . '.' . $ext;
        }
        
        return null;
    }

    public static function compressFileByModel($model,$attribute){
        $data = [
            'success'=>false
        ];
        $atr = $attribute;
        if ($model == null) {
            $data['success'] = false;
            $data['error'] = 'Model not found';
        }
        $val = $model->$atr;
        if(strlen($val)>0){
        $mimeType =\Yii::$app->fs->getMimeType($model->getUploadPath().$val);
        
        if(preg_match('/video\/*/', $mimeType)){
            return self::compressVideoByModel($model, $attribute);
        } else if(preg_match('/pdf\/*/', $mimeType)){
            return self::PdfToImageByModel($model, $attribute);
        } else if(preg_match('/audio\/*/', $mimeType)){
            return self::compressAudioByModel($model, $attribute);
        } else if ($model->getFileType($attribute) === '3d') {
            return self::compress3DFileByModel($model, $attribute);
        } else{
            $data['error'] = 'File format not supported for compression';
        }
        }
        return $data;
    }

    public static function compress3DFileByModel(ActiveRecord $model, $attribute, $ext='glb', $bucket = 'fs')
    {
        $data = ['success' => false];
        $bucket = 'fs';
        $atr = $attribute;
        if ($model == null || $model->getFileType($attribute) !== '3d') {
            $data['success'] = false;
            $data['error'] = 'Model not found';

            return $data;
        }
        $filePath = $model->getUploadPath() . $model->$atr;
        $mimeType = Yii::$app->get('fs')->getMimetype($filePath);
        if ($mimeType === 'application/zip') {
            $filePath = self::generate3dModelFromZip($filePath);
        }
        
        $filePathExtension = array_pop(explode('.', $filePath));
        $fileTempPath = ClassDispenser::getMappedClass(Helper::class)::getLocalTempFile($filePath, $bucket, $filePathExtension);

        if (! is_file($fileTempPath)) {
            $data['success'] = false;
            $data['error'] = 'Model not found';

            return $data;
        }
        $compressFilePath = self::compress3DFile($fileTempPath, $filePath, $ext);
        if ($compressFilePath['success']) {
            if (! empty($compressFilePath['detail']) && file_exists($compressFilePath['detail'])) {
                $newFile = self::getCompressedFilePathByAttribute($model, $attribute,'3d',$ext);
                $stream = fopen($compressFilePath['detail'], 'r+');
                \Yii::$app->$bucket->putStream($newFile, $stream);
                fclose($stream);
                DefaultFileHelper::unlink($compressFilePath['detail']);
            }
            $data['success']=true;
            
            if($model->hasAttribute('updated_at')){
                $model->updateAttributes(['updated_at' => date('Y-m-d H:i:s')]);
            }
        } else {
            $data['error'] = $compressFilePath['error'];
        }

        return $data;
    }

    public static function generate3dModelFromZip($path)
    {
        $fileStream = Yii::$app->get('fs')->readStream($path);
        $zipFilePath = '';
        if ($fileStream) {
            $file = tmpfile();
            if ($file) {
                $zipFilePath = stream_get_meta_data($file)['uri'];
                file_put_contents($zipFilePath, $fileStream);
            }
        }
        if (!$zipFilePath || !FileHelper::isConsist3DModelZipFile($zipFilePath)) {
            return '';
        }

        $za = new \ZipArchive(); 

        if (!$za->open($zipFilePath)) {
            return '';
        }
        $modelContent = null;
        $directoryOfTheModel = '';
        for( $i = 0; $i < $za->numFiles; $i++ ){ 
            $stat = $za->statIndex( $i );
            $fileName = basename($stat['name']);
            $fileExtension = pathinfo($fileName, PATHINFO_EXTENSION);
            $isConsist3dModel = in_array($fileExtension, ['gltf']);

            if (!$isConsist3dModel || $fileName[0] === '.') {
                continue;
            }
            $fileContent = $za->getFromName($stat['name']);
            $modelContent = json_decode($fileContent, true);
            if ($modelContent) {
                $directoryOfTheModel = dirname($stat['name']);
                break;
            }
        }
        if (empty($modelContent)) {
            return '';
        }
        if (!empty($modelContent['buffers'])){
            for ($i = 0; $i < count($modelContent['buffers']); $i++) {
                if (empty($modelContent['buffers'][$i] ||
                    empty($modelContent['buffers'][$i]['uri']))) {
                    continue;
                }
                $bufferUri = $modelContent['buffers'][$i]['uri'];
                
                $placeOfTheBuffer = $directoryOfTheModel ? $directoryOfTheModel . '/' . $bufferUri : $bufferUri;
               
                if ($placeOfTheBuffer[0] === '.' && $placeOfTheBuffer[1] === '/') {
                    $placeOfTheBuffer = substr($placeOfTheBuffer, 2);
                }
                $bufferContent = $za->getFromName($placeOfTheBuffer);
                if (empty($bufferContent)) {
                    $modelContent['buffers'][$i]['uri'] = '';
                    continue;
                }
                $modelContent['buffers'][$i]['uri'] = 'data:application/octet-stream;base64,' . base64_encode($bufferContent);
            }
        }
        if (!empty($modelContent['images'])) {
            for ($i = 0; $i < count($modelContent['images']); $i++) {
                if (empty($modelContent['images'][$i] ||
                    empty($modelContent['images'][$i]['uri']))) {
                    continue;
                }
                $bufferUri = $modelContent['images'][$i]['uri'];
                $placeOfTheBuffer = $directoryOfTheModel ? $directoryOfTheModel . '/' . $bufferUri : $bufferUri;
                if ($placeOfTheBuffer[0] === '.' && $placeOfTheBuffer[1] === '/') {
                    $placeOfTheBuffer = substr($placeOfTheBuffer, 2);
                }
                $bufferContent = $za->getFromName($placeOfTheBuffer);
                if (empty($bufferContent)) {
                    continue;
                }
                $modelContent['images'][$i]['mimeType'] = 'image/png';
                $modelContent['images'][$i]['uri'] = 'data:image/png;base64,' . base64_encode($bufferContent);
            }
        }
        $encodeModelContent = json_encode($modelContent);
        $modelPath = self::str_lreplace('.zip', microtime() . '.gltf', $path);
        Yii::$app->get('fs')->write($modelPath, $encodeModelContent);

        return $modelPath;
    }

    protected static function str_lreplace($search, $replace, $subject)
    {
        $pos = strrpos($subject, $search);

        if($pos !== false)
        {
            $subject = substr_replace($subject, $replace, $pos, strlen($search));
        }

        return $subject;
    }

    public static function compressVideoByModel(ActiveRecord $model, $attribute, $ext='mp4', $bucket = 'fs')
    {
        $data = [
            'success' => false
        ];
        $bucket = 'fs';
        $atr = $attribute;
        if ($model == null) {
            $data['success'] = false;
            $data['error'] = 'Model not found';
        } else {
            $filePath = $model->getUploadPath() . $model->$atr;
            $fileTempPath = ClassDispenser::getMappedClass(Helper::class)::getLocalTempFile($filePath);

            if (! is_file($fileTempPath)) {
                $data['success'] = false;
                $data['error'] = 'Model not found';
            } else {
                
                $mimeType = DefaultFileHelper::getMimeType($fileTempPath);
                if (preg_match('/video\/*/', $mimeType)) {
                    $compressFilePath = self::compressVideo($fileTempPath, $ext);
                    if (empty($compressFilePath['error'])) {
                        if (! empty($compressFilePath['detail']) && file_exists($compressFilePath['detail'])) {
                            $newFile = self::getCompressedFilePathByAttribute($model, $attribute,'video',$ext);
                            $stream = fopen($compressFilePath['detail'], 'r+');
                            \Yii::$app->$bucket->putStream($newFile, $stream);
                            fclose($stream);
                            DefaultFileHelper::unlink($compressFilePath['detail']);
                            $newThumbFile = self::getCompressedFilePathByAttribute($model, $attribute);
                            if (! empty($compressFilePath['thumb']) && file_exists($compressFilePath['thumb'])) {
                                $streamThumb = fopen($compressFilePath['thumb'], 'r+');
                                \Yii::$app->$bucket->putStream($newThumbFile, $streamThumb);
                                fclose($streamThumb);
                             DefaultFileHelper::unlink($compressFilePath['thumb']);
                            }
                        }
                        $data['success']=true;
                        
                        if($model->hasAttribute('updated_at')){
                            $model->updateAttributes(['updated_at' => date('Y-m-d H:i:s')]);
                        }
                    }else{
                        $data['error'] = $compressFilePath['error'];
                    }
                }
            }}
            return $data;
    }

    public function compressAudioByModel(ActiveRecord $model, $attribute){
        $data = [
            'success' => false
        ];
        $ext = 'mp3';
        $tempExt = getenv('compressed_audio_format');
        if(strlen($tempExt)>0){
            $ext = $tempExt;
        }
        $bucket = 'fs';
        $atr = $attribute;
        if ($model == null) {
            $data['success'] = false;
            $data['error'] = 'Model not found';
        } else {
            $filePath = $model->getUploadPath() . $model->$atr;
            $fileTempPath = ClassDispenser::getMappedClass(Helper::class)::getLocalTempFile($filePath);
            
            if (! is_file($fileTempPath)) {
                $data['success'] = false;
                $data['error'] = 'Model not found';
            } else {
                
                $mimeType = DefaultFileHelper::getMimeType($fileTempPath);
                if (preg_match('/audio\/*/', $mimeType)) {
                    $compressFilePath = self::compressAudio($fileTempPath);
                    if (empty($compressFilePath['error'])) {
                        if (! empty($compressFilePath['detail']) && file_exists($compressFilePath['detail'])) {
                            $newFile = self::getCompressedFilePathByAttribute($model, $attribute,'audio',$ext);
                            $stream = fopen($compressFilePath['detail'], 'r+');
                            \Yii::$app->$bucket->putStream($newFile, $stream);
                            fclose($stream);
                            DefaultFileHelper::unlink($compressFilePath['detail']);
                        }
                        $data['success']=true;
                        
                        if($model->hasAttribute('updated_at')){
                            $model->updateAttributes(['updated_at' => date('Y-m-d H:i:s')]);
                        }
                    }else{
                        $data['error'] = $compressFilePath['error'];
                    }
                }else{
                    $data['error'] = 'Not an audio file';
                }
            }}
            return $data;
    }

    public static function compress3DFile($path, $originalFilePath, $ext = 'glb')
    {
        $compressType = 'generateGlb';
        $pathInfo = pathinfo($path);
        $baseName = pathInfo['filename'];
        $data = ['success' => false];
        $dirPath = StringHelper::dirname($path);

        $originalFilePathExtension = array_pop(explode('.', $originalFilePath));
        if (!$originalFilePathExtension) {
            throw new \Exception(\Yii::t('twbp', 'File is not 3D model'));
        }
        switch ($originalFilePathExtension) {
            case 'stp':
            case 'gltf':
                $tmpOutputFName = 'tmp_' . $baseName . '.gltf';
                $tmpFPath = DefaultFileHelper::normalizePath($dirPath . '/' . $tmpOutputFName);
                $program = 'gltf-pipeline';
                if (! ClassDispenser::getMappedClass(Helper::class)::shellProgramExists($program)) {
                    throw new \Exception(
                        \Yii::t(
                            'twbp',
                            'gltf-pipeline program is not installed. Please install it using `npm install -g gltf-pipeline` command'
                        )
                    );

                    return $data;
                }
                        try {
                /// generate DRACO GLTF            
                $cmd = "gltf-pipeline -i $path -o $tmpFPath -d";
                shell_exec($cmd);
                // Original uplaoded file is deleted
                DefaultFileHelper::unlink($path);
                $newPath = DefaultFileHelper::normalizePath($dirPath . '/' . $baseName . '.gltf');
                // File Renamed to original file with mp3 extension
                if (!rename($tmpFPath, $newPath)) {
                    throw new \Exception(\Yii::t('twbp', '3D model is not compressed'));
                    return $data;
                }

                /// generate GLB file
                $path = $newPath;
                $tmpOutputFName = 'tmp_' . $baseName . '.' . $ext;
                $tmpFPath = DefaultFileHelper::normalizePath($dirPath . '/' . $tmpOutputFName);
                $cmd = "gltf-pipeline -i $path -o $tmpFPath -b";
                shell_exec($cmd);
                // Original uplaoded file is deleted
                DefaultFileHelper::unlink($path);
                $newPath = DefaultFileHelper::normalizePath($dirPath . '/' . $baseName . '.' . $ext);
                // File Renamed to original file with mp3 extension
                if (!rename($tmpFPath, $newPath)) {
                    throw new \Exception(\Yii::t('twbp', '3D model is not compressed'));
                    return $data;
                }
                $data['success'] = true;
                $data['detail'] = $newPath;
                
                return $data;
            } catch (\Exception $e) {
                $data['error'] = $e->getMessage();
                return $data;
            }
            case 'glb':
                $tmpOutputFName = 'tmp_' . $baseName . '.glb';
                $newPath = DefaultFileHelper::normalizePath($dirPath . '/' . $tmpOutputFName);
                if (!rename($path, $newPath)) {
                    throw new \Exception(\Yii::t('twbp', '3D model is not compressed'));
                    return $data;
                }
                $data['success'] = true;
                $data['detail'] = $newPath;

                return $data;
            case 'drc':
            default:
                throw new \Exception(\Yii::t('twbp', '3D model is not compressed'));
                break;
        }

        return $data;
    }

    public static function compressAudio($path, $ext = 'mp3')
    {
        $baseName = pathinfo($path)['filename'];
        $data = ['success' => false];
        $dirPath = StringHelper::dirname($path);
        $tmpOutputFName = 'tmp_' . $baseName . '.' . $ext;
        $tmpFPath = DefaultFileHelper::normalizePath($dirPath . '/' . $tmpOutputFName);
        $program = 'ffmpeg';
        
        if (! ClassDispenser::getMappedClass(Helper::class)::shellProgramExists($program)) {
            throw new \Exception(\Yii::t('twbp', 'ffmpeg program is not installed. Please intall it from https://www.ffmpeg.org/'));
        } else {
            
            try {
                $cmd = "ffmpeg -i \"$path\" -b:a 128k -map a $tmpFPath";
                $output = shell_exec($cmd);
                // Original uplaoded file is deleted
                DefaultFileHelper::unlink($path);
                $newPath = DefaultFileHelper::normalizePath($dirPath . '/' . $baseName . '.' . $ext);
                // File Renamed to original file with mp3 extension
                if (rename($tmpFPath, $newPath)) {
                    $data['success'] = true;
                    $data['detail'] = $newPath;
                }
            } catch (\Exception $e) {
                $data['error'] = $e->getMessage();
            }
        }
        return $data;
    }

    public static function compressVideo($path, $ext = 'mp4', $maxHeight = '', $maxWidth = '')
    {
        $envWidth = getenv('VIDEO_COMPRESS_RESOLUTION_WIDTH');
        $envHeight = getenv('VIDEO_COMPRESS_RESOLUTION_HEIGHT');
        if(empty($maxWidth) && !empty($envWidth)){
            $maxWidth = $envWidth;
        }
        else if(empty($maxWidth) && empty($envWidth))
        {
            $maxWidth = '1280';
        }
        
        if(empty($maxHeight) && !empty($envHeight)){
            $maxHeight = $envHeight;
        }
        else if(empty($maxHeight) && empty($envHeight))
        {
            $maxHeight = '720';
        }
        $baseName = pathinfo($path)['filename'];
        $data = ['success' => false];
        $dirPath = StringHelper::dirname($path);
        $tmpOutputFName = 'tmp_' . $baseName . '.' . $ext;
        $tmpFPath = DefaultFileHelper::normalizePath($dirPath . '/' . $tmpOutputFName);
        $program = 'ffmpeg';
        
        if (! ClassDispenser::getMappedClass(Helper::class)::shellProgramExists($program)) {
            throw new \Exception(\Yii::t('twbp', 'ffmpeg program is not installed. Please intall it from https://www.ffmpeg.org/'));
        } else {
            
            try {
                $widthCmd = "ffmpeg -i \"$path\" 2>&1 | grep Video: | grep -Po '\d{3,5}x\d{3,5}' | cut -d'x' -f1";
                $width = shell_exec($widthCmd);
                $heightCmd = "ffmpeg -i \"$path\" 2>&1 | grep Video: | grep -Po '\d{3,5}x\d{3,5}' | cut -d'x' -f2";
                $height = shell_exec($heightCmd);
                $width = trim($width);
                $height = trim($height); 
                if (! empty($height) && ! empty($width) && $height > $width) {
                    $maxHeight = $maxHeight + $maxWidth;
                    $maxWidth = $maxHeight - $maxWidth;
                    $maxHeight = $maxHeight - $maxWidth;
                }
                
                $cmd = "ffmpeg -i \"$path\" -filter:v \"scale='min($maxWidth,iw)':min'($maxHeight,ih)':force_original_aspect_ratio=decrease,pad=ceil(iw/2)*2:ceil(ih/2)*2\" -c:a aac -strict -2 \"$tmpFPath\" -y 2>&1";
                
                
                $output = shell_exec($cmd);
                // where to save the image
                $tmpImageOutputFName = $baseName . FileCompressor::COMPRESSED_THUMB_POSTFIX;
                $tmpImagePath = DefaultFileHelper::normalizePath($dirPath . '/' . $tmpImageOutputFName);
                
                // time to take screenshot at
                $interval = 1;
                
                $resolutionCmd = "ffmpeg -i \"$tmpFPath\" 2>&1 | grep Video: | grep -Po '\d{3,5}x\d{3,5}'";
                // screenshot size
                
                $size = shell_exec($resolutionCmd);
                $size = trim($size);
                if (empty($size)) {
                    $size = $maxWidth . "x" . $maxHeight;
                }
                // ffmpeg command
                $imageCmd = "ffmpeg -i \"$tmpFPath\" -deinterlace -an -ss \"$interval\" -f mjpeg -t 1 -r 1 -y -s \"{$size}\" \"$tmpImagePath\" 2>&1";
                $outputImageCmd = shell_exec($imageCmd);
                
                // Original uplaoded file is deleted
                DefaultFileHelper::unlink($path);
                $newPath = DefaultFileHelper::normalizePath($dirPath . '/' . $baseName . '.' . $ext);
                // File Renamed to original file with mp4 extension
                if (rename($tmpFPath, $newPath)) {
                    $data['success'] = true;
                    $data['detail'] = $newPath;
                    $data['thumb'] = $tmpImagePath;
                }
            } catch (\Exception $e) {
                $data['error'] = $e->getMessage();
            }
        }
        return $data;
    }
    /**
     * @desc Makes png thumbnail of a pdf 
    */
    public static function makePdfToImage($pdfPath,$thumbnailBaseName='')
    {
        $data = ['success'=>false];
        if(!class_exists('Imagick')){
            $data['error'] = \Yii::t('twbp','Please include the Imagick class to make a pdf thumbnail');
            return $data;
        }
        
        $baseName = pathinfo($pdfPath)['filename'];
        $data = ['success' => false];
        $dirPath = StringHelper::dirname($pdfPath);
        $tmpOutputFName = $baseName . '.' .FileCompressor::COMPRESSED_PDF_THUMB_POSTFIX;
        if($thumbnailBaseName){
            $tmpOutputFName = $thumbnailBaseName.'.png'; 
        }
        
        try{
        $tmpFPath = DefaultFileHelper::normalizePath($dirPath . '/' . $tmpOutputFName);
            $imagick = new \Imagick();
            $imagick->setResolution(300, 300);
            $imagick->readImage($pdfPath . "[0]");
            $imagick->setImageFormat("png");
            $imagick->setImageBackgroundColor('white');
            $imagick->setImageAlphaChannel(11);
            $imagick->mergeImageLayers(\Imagick::LAYERMETHOD_FLATTEN);
            $imagick->setCompressionQuality(90);
            if($imagick->writeImage($tmpFPath)){
            $data['success']=true; 
            $data['name'] = $tmpOutputFName;
            $data['detail']=$tmpFPath;
            }else{
                $data['error'] = \Yii::t('twbp','Unable to make pdf image');
            }
            
        }catch(\Exception $e){
            $data['error'] = $e->getMessage();
        }
        return $data;
    }

    public static function isZipFile($path)
    {
        return mime_content_type($path) === 'application/zip';
    }

    public static function isConsist3DModelZipFile($path)
    {
        $za = new \ZipArchive(); 

        $za->open($path);

        $isConsist3dModel = false;

        for( $i = 0; $i < $za->numFiles; $i++ ){ 
            $stat = $za->statIndex( $i );
            $fileName = basename($stat['name']);

            $fileExtension = pathinfo($fileName, PATHINFO_EXTENSION);
            $isConsist3dModel = in_array($fileExtension, ['gltf', 'glb']);

            if ($isConsist3dModel) {
                return true;
            }
        }

        return false;
    }
}
