<?php
namespace taktwerk\yiiboilerplate\helpers;

use yii\helpers\StringHelper;
use taktwerk\yiiboilerplate\models\TwActiveRecord;

class MFConditionalField{
    
    public $model;
    public $rules;
    public $atrName;
    public function __construct(TwActiveRecord $model,$atrName, $rules = []){
        $this->model = $model;
        $this->rules = $rules;
        $this->atrName = $atrName;
    }

    public function isRequired(){
        $count = count($this->rules['rules']);
        $action = !empty($this->rules['action']) ? $this->rules['action'] : '';
        $logic = $this->rules['logic'];
        $rules = $this->rules['rules'];
        
        //STILL IN PROGRESS. LOGIC 
        $isConditionMet = false;
        if ($count > 0) {
            foreach($rules as $rule){
                $name = $rule['name'];
                if(preg_match('#\[(.*?)\]#', $name, $match)){
                    $name = $match[1];
                }
                
                $operator = $rule['operator'];
                $value = $rule['value'];
                $atrValue = $this->model->{$name};
                $isRuleMet = $this->compareValues($operator,$atrValue,$value);
                $isConditionMet = $isRuleMet;
                if ($isRuleMet === false && $logic == 'and') {
                    $isConditionMet = false;
                    break;
                } else if ($isRuleMet && $logic == 'or') {
                    $isConditionMet = true;
                    break;
                }
            }
         }
         if($action=='hide'){
             if($isConditionMet==false)
             {
                 return true;
             }
         }
        return $isConditionMet;
    }
    private function compareValues($operator, $searchVal, $targetVal){
        $searchVal = $searchVal ? strtolower($searchVal) : "";
        $targetVal = $targetVal ? strtolower($targetVal) : "";
        
        switch ($operator) {
            case "is":
                return $targetVal === $searchVal;
            case "isnot":
                return $targetVal !== $searchVal;
            case "greaterthan":
                return is_nan($targetVal) || is_nan($searchVal) ? false : number_format($targetVal) > number_format($searchVal);
            case "lessthan":
                return is_nan($targetVal) || is_nan($searchVal) ? false : number_format(targetVal) < number_format($searchVal);
            case "contains":
                return in_array($targetVal, $searchVal);
            case "doesnotcontain":
                return !in_array($targetVal, $searchVal);
            case "beginswith":
                return StringHelper::startsWith($searchVal, $targetVal);
            case "doesnotbeginwith":
                return !StringHelper::startsWith($searchVal, $targetVal);
            case "endswith":
                return StringHelper::endsWith($searchVal, $targetVal);
            case "doesnotendwith":
                return !StringHelper::endsWith($searchVal, $targetVal);
            case "isempty":
                return $searchVal === "";
            case "isnotempty":
                return $searchVal !== "";
        }
        
        return false;
        
    }
}