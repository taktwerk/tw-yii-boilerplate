<?php
namespace taktwerk\yiiboilerplate\helpers;
use taktwerk\yiiboilerplate\components\Helper;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use yii\helpers\FileHelper;
class FileConverter{
    /**
     * PDFA (does not support PNG with alpha channel)
     * @param $filePath
     * @throws \Exception
     */
    public static function pdf2pdfA($filePath){
        $tempPath = ClassDispenser::getMappedClass(Helper::class)::getLocalTempFile($filePath);
        $mime = \Yii::$app->fs->getMimetype($filePath);
        if($mime!=='application/pdf'){
            return;
        }
        $helperClass = ClassDispenser::getMappedClass(Helper::class);
        if(!$helperClass::shellProgramExists('gs')){
            throw new \Exception('Please install "ghostscript" program to convert PDF');
        }
        $outputFile = $tempPath.'.pdf';

        // compatibility mode outputs either noth a PDFA or converts all to image
        // dPDFA=2 not working
$cmd = <<<EOT
gs -dPDFA -dBATCH -dNOPAUSE -dUseCIEColor -sProcessColorModel=DeviceCMYK -sDEVICE=pdfwrite -sPDFACompatibilityPolicy=1 -dPDFACompatibilityPolicy=3 -sOutputFile="{$outputFile}" "{$tempPath}"
EOT;
        shell_exec($cmd);
        $stream = fopen($outputFile, 'r+');
        \Yii::$app->fs->putStream($filePath,$stream);
        FileHelper::unlink($tempPath);
        FileHelper::unlink($outputFile);
    }
    public static function word2pdf($filePath,$saveWithTheFile = false){
        $tempPath = ClassDispenser::getMappedClass(Helper::class)::getLocalTempFile($filePath);
        $mime = \Yii::$app->fs->getMimetype($filePath);
        $allowedTypes = ['application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/msword
','application/vnd.oasis.opendocument.text'];
        if(!in_array($mime, $allowedTypes)){
            return;
        }
        $pathInfo = pathinfo(\Yii::$app->fs->getMetadata($filePath)['path']);
        $helperClass = ClassDispenser::getMappedClass(Helper::class);
        if(!$helperClass::shellProgramExists('libreoffice')){
            throw new \Exception('Please install "libreoffice" program to convert word2PDF');
        }
        $tempPathInfo = pathInfo($tempPath);
        $cmd = <<<EOT
libreoffice --headless --convert-to pdf:writer_pdf_Export --outdir {$tempPathInfo['dirname']}  {$tempPath}
EOT;
        shell_exec($cmd);
        $pdfPath = $pathInfo['dirname'].'/'.$pathInfo['filename'].'.pdf';

        $pdfTempPath = $tempPathInfo['dirname'].'/'.$tempPathInfo['filename'].'.pdf';
        FileHelper::unlink($tempPath);
        if($saveWithTheFile){
            \Yii::$app->fs->put($pdfPath, file_get_contents($pdfTempPath));
            FileHelper::unlink($pdfTempPath);
            return $pdfPath;
        }

        return $pdfTempPath;
    }
}