<?php
namespace taktwerk\yiiboilerplate\helpers;

use PhpOffice\PhpWord\Style\Section;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use taktwerk\yiiboilerplate\components\Helper;
use PhpOffice\PhpWord\TemplateProcessor;
use yii\helpers\FileHelper as DefaultFileHelper;

use PhpOffice\PhpWord\Element\TextRun;

class FileTextReplace {

    public static function replace($filePath, $placeholderAndValues=[], $replaceTheFileOnPath=false, $rowValuesReplacement=[], $blockValuesReplacement=[]){
        $tempPath = ClassDispenser::getMappedClass(Helper::class)::getLocalTempFile($filePath);
        $mime = \Yii::$app->fs->getMimetype($filePath);
        $allowedTypes = ['application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/msword
','application/vnd.oasis.opendocument.text'
        ];
        if(!in_array($mime, $allowedTypes)){
            return false;
        }

        $templateProcessor = new TemplateProcessor($tempPath);
        $templateProcessor->setValues($placeholderAndValues);
        if(count($rowValuesReplacement)>0){
            /** Example of $rowValuesReplacement variable
               $rowValuesReplacement = [
               'case_id'=>[
                    ['case_id'=>1,'case_name'=>'Xyz','case_date'=>'2011-10-20'],
                    ['case_id'=>2,'case_name'=>'Foo','case_date'=>'2011-01-12'],
                    ['case_id'=>3,'case_name'=>'Bar','case_date'=>'2009-05-28'],
               ],
               'class_id'=>[
                    ['class_id'=>100,'name'=>'Hello','start_date'=>'2016-05-20'],
                    ['class_id'=>108,'name'=>'Hi','start_date'=>'2015-04-01'],
                    ['class_id'=>109,'name'=>'Bye','start_date'=>'2012-01-01'],
                ]
               ];
           */
            foreach($rowValuesReplacement as $key=>$rows){
                try{
                    $templateProcessor->cloneRowAndSetValues($key, $rows);
                }catch(\PhpOffice\PhpWord\Exception\Exception $e){
                }
            }
        }
        if(count($blockValuesReplacement)>0){
            foreach($blockValuesReplacement as $block=>$blockContent){
                if($blockContent === false) {
                    $templateProcessor->deleteBlock($block);
                }
                else if (is_object($blockContent)) {
                    $templateProcessor->setComplexBlock($block, $blockContent);
                }
                else {
                    $templateProcessor->replaceBlock($block, $blockContent);
                }
                //$templateProcessor->cloneBlock('paragraph', count($replacements), true, false, $replacements);
            }
        }

        $path = $templateProcessor->save();
        DefaultFileHelper::unlink($tempPath);
        if($replaceTheFileOnPath==true){
            $stream = fopen($path, 'r+');
            \Yii::$app->fs->putStream($filePath, $stream);
            DefaultFileHelper::unlink($path);
            return true;
        }else{
            return $path;
        }
    }
}