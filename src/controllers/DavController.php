<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 7/31/2017
 * Time: 1:33 PM
 */
namespace taktwerk\yiiboilerplate\controllers;

use taktwerk\yiiboilerplate\components\dav\auth\Auth;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\Controller;
use taktwerk\yiiboilerplate\components\dav\Directory;
use taktwerk\yiiboilerplate\components\dav\Server;
use Yii;
use Da\User\Query\UserQuery;

class DavController extends Controller
{

    /**
     */
    public function init()
    {
        $this->enableCsrfValidation = false;
    }

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index'
                        ],
                        'roles' => [
                            '@',
                            '?'
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * Register all FS and display the current path in Dav.
     */
    public function actionIndex()
    {
        // Want to make sure we aren't limited by the slow API responses.
        set_time_limit(0);
        
        $shares = [];
        if (! empty($this->registerFs())) {
            foreach ($this->registerFs() as $component) {
                $shares[] = new Directory($component);
            }
        }
        $server = new Server($shares);
        $server->sapi = new \taktwerk\yiiboilerplate\components\dav\Sapi();
        $server->setBaseUri('/dav');
        $server->addPlugin(new \Sabre\DAV\Auth\Plugin(new Auth()));
        $server->addPlugin(new \Sabre\DAV\Browser\Plugin());
        $server->addPlugin(new \Sabre\DAV\Browser\GuessContentType());
        $this->getCredentials();
        $server->exec();
    }

    /**
     * Register the user/user role filesystems.
     * Admin has access to everything.
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    protected function registerFs()
    {
        $finder = Yii::createObject(UserQuery::class);
        $user = $finder->whereUsernameOrEmail($this->getCredentials()[0])
            ->one();
        if (! $user) {
            return [];
        }
        Yii::$app->user->login($user);
        $controller = new ElFinderShareController('elfinder', new Module('app'), [
            'roots' => Yii::$app->controllerMap['file']['roots']
        ]);
        $controller->init();
        $components = [];
        if (! empty($controller->roots)) {
            foreach ($controller->roots as $fs) {
                $components[] = [
                    'component' => $fs['component'],
                    'name' => $fs['name'],
                    'path' => $fs['path'],
                    'attributes' => $fs['options']['attributes']
                ];
            }
        }
        return $components;
    }

    /**
     * Get the Authorization from request (apache auth mod)
     * 
     * @return array|null
     */
    protected function getCredentials()
    {
        $auth = Yii::$app->request->headers->get('Authorization');
        
        if (! $auth) {
            return null;
        }
        
        if (strtolower(substr($auth, 0, 6)) !== 'basic ') {
            return null;
        }
        
        $credentials = explode(':', base64_decode(substr($auth, 6)), 2);
        
        if (2 !== count($credentials)) {
            return null;
        }
        
        return $credentials;
    }
}
