<?php
//Generation Date: 11-Sep-2020 02:34:55pm
namespace taktwerk\yiiboilerplate\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "UserUiDataController".
 */
class UserUiDataController extends \taktwerk\yiiboilerplate\controllers\base\UserUiDataController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\models\UserUiData';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\models\search\UserUiData';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
}
