<?php
//Generation Date: 11-Sep-2020 02:34:55pm
namespace taktwerk\yiiboilerplate\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * UserUiDataController implements the CRUD actions for UserUiData model.
 */
class UserUiDataController extends TwCrudController
{
}
