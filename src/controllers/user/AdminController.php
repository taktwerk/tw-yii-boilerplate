<?php

namespace taktwerk\yiiboilerplate\controllers\user;

use Da\User\Controller\AdminController as BaseController;
use Da\User\Filter\AccessRuleFilter;
use yii\filters\AccessControl;

/**
 * Class AdminController
 * @package taktwerk\yiiboilerplate\controllers\user
 */
class AdminController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'ruleConfig' => [
                    'class' => AccessRuleFilter::class,
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'Administrator'],
                    ],
                ],
            ],
        ]);
    }
}
