<?php

namespace taktwerk\yiiboilerplate\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "GoogleSpreadsheetController".
 */
class GoogleSpreadsheetController extends \taktwerk\yiiboilerplate\modules\system\controllers\GoogleSpreadsheetController
{
    //    /**
    //     * Additional actions for controllers, uncomment to use them
    //     * @inheritdoc
    //     */
    //    public function behaviors()
    //    {
    //        return ArrayHelper::merge(parent::behaviors(), [
    //            'access' => [
    //                'class' => AccessControl::class,
    //                'rules' => [
    //                    [
    //                        'allow' => true,
    //                        'actions' => [
    //                            'list-of-additional-actions',
    //                        ],
    //                        'roles' => ['@']
    //                    ]
    //                ]
    //            ]
    //        ]);
    //    }
}
