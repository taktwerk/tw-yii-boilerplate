<?php

namespace taktwerk\yiiboilerplate\controllers;

/**
 * Class Controller
 * @package mihaildev\elfinder
 * @property array $options
 */


class ElFinderController extends ElFinderBackendController
{
    /**
     * @return mixed
     */
    public function actionManager()
    {
        $this->layout = "@taktwerk-backend-views/layouts/main";
        return $this->render("@taktwerk-views/elfinder/manager.php", ['options' => $this->getManagerOptions()]);
    }
}
