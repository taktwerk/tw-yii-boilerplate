<?php
namespace taktwerk\yiiboilerplate\controllers;

use dmstr\bootstrap\Tabs;
use Exception;
use ReflectionClass;
use taktwerk\yiiboilerplate\components\ImageHelper;
use taktwerk\yiiboilerplate\helpers\CrudHelper;
use taktwerk\yiiboilerplate\models\TwActiveRecord;
use taktwerk\yiiboilerplate\modules\backend\actions\RelatedFormAction;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\modules\customer\models\Client;
use taktwerk\yiiboilerplate\modules\user\models\User;
use taktwerk\yiiboilerplate\traits\DependingTrait;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\Application;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\IdentityInterface;
use yii\web\Response;
use yii\web\UploadedFile;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use taktwerk\yiiboilerplate\commands\ModelDuplicater;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

abstract class TwCrudController extends Controller
{
    /**
     *
     */
    const POS_START = 1;
    const POS_MAIN = 2;
    const POS_END = 3;

    /**
     * Each controller extending this must override the model to the namespaced model name
     *
     * @var string
     */
    public $model = '';

    /**
     * The Search model namespace, also required
     *
     * @var string
     */
    public $searchModel = '';

    /**
     * Base model name
     *
     * @var string
     */
    protected $baseModel = '';

    /**
     *
     * @var string
     */
    protected $translationForeignKey = '';

    public $translationLabels = [];

    /**
     * Extra grid dropdown actions
     *
     * Add grid dropdown actions in the controller's init() function. These appear in the dropdown above the grid when
     * selecting several rows to send to a custom action.
     *
     * $this->gridDropdownActions = [
     * 'custom' => [
     * 'label' => '<i class="fa fa-globe" aria-hidden="true"></i> ' . Yii::t('twbp', 'Custom Label'),
     * 'url' => \yii\helpers\Url::to(['/route/custom']),
     * 'visible' => Yii::$app->getUser()->can('ACL'),
     * ],
     * ];
     *
     * @var array
     */
    public $gridDropdownActions = [];

    /**
     * Extra link action for grid, form & view
     *
     * gridLinkActions is obsolete
     *
     * gridLinkActions to be refactored as customActions
     *
     * Add link actions in the controller's init() function. These appear on each row as an
     * icon after the CRUD actions or as an action dropdown in view/form.
     *
     * $this->customActions = [
         'tw' => function ($url, $modal, $key) {
             if (!Yii::$app->getUser()->can('ACL')) {
             return false;
             }
             return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                 'title' => Yii::t('twbp', 'Tw'),
                 'data-toggle' => 'modal',
                 'data-url' => $url,
                 'data-pjax' => 1,
             ]);
         },
     ];
     * @deprecated  $gridLinkActions array
     * @var $customActions array
     */
    public $gridLinkActions = [];

    public $customActions = [];

    /**
     * Extra grid button actions
     *
     * Add grid buttons actions in the controller's init() function. These buttons will appear
     * after the "New" and "Relations" buttons on top of the grid.
     *
     * $this->gridButtonActions = [
     * 'button' => [
     * 'label' => '<i class="fa fa-lock" aria-hidden="true"></i> ' . Yii::t('twbp', 'Custom Button Text'),
     * 'url' => \yii\helpers\Url::to(['/route/custom']),
     * 'class' => 'btn btn-primary',
     * 'visible' => Yii::$app->getUser()->can('ACL'),
     * ],
     * ];
     *
     * @var array
     */
    public $gridButtonActions = [];

    /**
     * Visibility of main buttons in crud views
     *
     * @var array
     */
    public $crudMainButtons = [
        'createsave' => true,
        'createnew' => true,
        'createclose' => true,
        'delete' => true,
        'listall' => true,
        'restore' => true
    ];

    /**
     * Visibility of relation tabs
     * Set 'pluralizedRelationName' => true/false to show/hide
     *
     * @var
     */
    public $crudRelations;

    /**
     * Blocks to add to the Tabs widget
     * $this->tabBlocks = [
     *      [
     *          'content' => $this->blocks['Client Devices'],
     *          label' => '<small>' .Yii::t('twbp', 'Client Devices') .'&nbsp;<span class="badge badge-default">' .
     *          $model->getClientDevices()->count() .'</span></small>',
     *          'active' => false,
     *          'visible' => Yii::$app->user->can('x_customer_client-device_see')
     *      ]
     * ];
     * @var array
     */
    public $tabBlocks = [];

    /**
     * - [[POS_START]]: in the start of page.
     * - [[POS_MAIN]]: at the beginning of the body section. This is the default value.
     * - [[POS_END]]: at the end of the body section.
     * Blocks to add to the base view
     * $this->customBlocks = [
        'custom-block' => self::POS_MAIN,
        'custom-block-1' => self::POS_END,
        'custom-block-2' => self::POS_START,
     * ];
     * @var array
     */
    public $customBlocks = [];

    /**
     * Overwrite columns in views
     *
     * Add column overwrites in the controller's init() function.
     *
     * $this->crudColumnsOverwrite = [
     * 'before#title' => 'information', // add information attribute before title attribute
     * 'after#title' => 'information', // add information attribute after title attribute
     * 'replace#title' => 'information' // replace title attribute with information attribute
     * 'title' => 'information' // replace title attribute with information attribute (default, without operator)
     * 'replace#title' => false, // remove title attribute
     * 'title' => false, // remove title attribute
     * 'replace#title' => [ // single attribute string or full column array supported
     * 'attribute' => 'title',
     * 'format' => 'html',
     * 'label' => 'Title',
     * 'value' => function ($model) {
     * ...
     * },
     * ],
     * ];
     *
     * @var array
     */
    public $crudColumnsOverwrite = [];

    public $exportColumns = [];

    public $exportFilename = [];

    public $formFieldsOverwrite = [];

    public $crudUrlKey = '_crudUrl';

    /**
     * Depending Trait for Crud generation
     */
    use DependingTrait;

    /**
     *
     * @var boolean whether to enable CSRF validation for the actions in this controller.
     *      CSRF validation is enabled only when both this property and [[Request::enableCsrfValidation]] are true.
     */
    public $enableCsrfValidation = false;
    /**
     *
     * @var boolean whether to enable inline sub-forms
     */
    protected $inlineForm = false;

    /**
     *
     * @var boolean whether to enable modal editing / viewing
     */
    protected $useModal = false;

    /**
     *
     * @var boolean show import option
     */
    protected $importer = false;

    /**
     *
     * @var string Which type of related form should show, modal or tab
     */
    protected $relatedTypeForm = RelatedForms::TYPE_TAB;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'update',
                            'delete',
                            'delete-multiple',
                            'related-form',
                            'update-multiple',
                            'duplicate-multiple',
                            'delete-file',
                            'entry-details',
                            'system-entry',
                            'list',
                            'depend',
                            'sort-item',
                            'upload-image',
                            'recompress',
                            'recompress-multiple',
                            'save-ui-data',
                        ],
                        'roles' => [
                            '@'
                        ]
                    ],
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if(Yii::$app instanceof Application && !Yii::$app->user->isGuest){
            $this->updateOnline();
        }
        return parent::beforeAction($action);
    }

    /**
     *
     * @return object
     * @throws \yii\base\InvalidConfigException
     */
    public function getModel()
    {
        return Yii::createObject($this->model);
    }

    /**
     *
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function getSearchModel()
    {
        return Yii::createObject($this->searchModel);
    }

    /**
     * Initialize the variables used for getting the models
     *
     * @throws Exception
     */
    public function init()
    {
        $reflect = null;
        if (empty($this->model)) {
            $reflect = new ReflectionClass($this);
            // Todo: handle if in modules to automate more
            $this->model = 'app\models\\' . str_replace('Controller', '', $reflect->getShortName());
            // throw new Exception(Yii::t('twbp', 'Please provide a Model in your controller config.'));
        }
        if (empty($this->searchModel)) {
            if (empty($reflect)) {
                $reflect = new ReflectionClass($this);
            }
            // Todo: handle if in modules to automate more
            $this->searchModel = 'app\models\search\\' . str_replace('Controller', '', $reflect->getShortName());
            // throw new Exception(Yii::t('twbp', 'Please provide a Search Model in your controller config.'));
        }

        if (empty($this->baseModel)) {
            $this->baseModel = StringHelper::basename($this->model);
        }

        $this->translationForeignKey = strtolower(Inflector::slug(Inflector::camel2words($this->baseModel), '_'));

        if (empty($this->exportFilename)) {
            $this->exportFilename = date("ymd-His", time()) . '_' . getenv('APP_NAME') . '_' . $this->baseModel . '_export';
        }

        if(getenv('ENABLE_IMPORTER')=='1'){
            $this->importer = true;
        }

        if(isset($this->gridLinkActions) && count($this->customActions)<1){
            $this->customActions = $this->gridLinkActions;
        }
        
        return parent::init();
    }

    /**
     * Lists all models.
     *
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        $searchModel = $this->getSearchModel();
        $dataProvider = $searchModel->search($_GET);

        Tabs::clearLocalStorage();

        Url::remember('',$this->crudUrlKey);
        Yii::$app->session['__crudReturnUrl'] = null;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'useModal' => $this->useModal,
            'importer' => $this->importer
        ]);
    }

    /**
     * Displays a single model.
     *
     * @param integer $id
     *
     * @param bool $viewFull
     * @param null $fromRelation
     * @return mixed
     * @throws HttpException
     * @throws \yii\console\Exception
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionView($id, $viewFull = false, $fromRelation = null, $atr=null,$relatedBlock = null)
    {
        // Hook before creating
        $this->beforeView();
        if(strlen($atr)>0){
            $model = $this->findModel($id);
            Yii::$app->response->format = 'json';
            if($model && $model->hasAttribute($atr)){
                return ['success'=>true, 'value'=>$model->{$atr}];
            }
            return ['success'=>false, 'value'=>\Yii::t('twbp','Unable to load the value')];
        }
        
        $resolved = Yii::$app->request->resolve();
        $resolved[1]['_pjax'] = null;
        $url = Url::to(array_merge([
            '/' . $resolved[0]
        ], $resolved[1]));
        if(Url::current()!= Url::previous($this->crudUrlKey)){
            Yii::$app->session['__crudReturnUrl'] = Url::previous($this->crudUrlKey);
        }
        Url::remember($url,$this->crudUrlKey);
        Tabs::rememberActiveState();

        if ($this->useModal && ! $viewFull) {
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
                'useModal' => $this->useModal
            ]);
        } else {
            if ($fromRelation) {
                Yii::$app->controller->crudMainButtons['listall'] = false; // TODO: Remove, when creating new related model is implemented
            }
            $viewData = [
                'model' => $this->findModel($id),
                'useModal' => $this->useModal,
                'fromRelation' => $fromRelation,
                'relatedBlock' => $relatedBlock,
                'tabAction'=>'view'
            ];
            return ($relationBlock)?$this->renderAjax('_blocks', $viewData):$this->render('view', $viewData);
        }
    }
    protected function canvasFieldsReset($model)
    {
        $activeAttributes = $model->activeAttributes();
        $oldAttributes = $model->oldAttributes;

        if (! empty($canvasFields = ClassDispenser::getMappedClass(CrudHelper::class)::getCanvasFields($model))) {
            foreach ($canvasFields as $canvasField) {
                if (in_array($canvasField, $activeAttributes)) {
                    if ($model->$canvasField != '') {
                        $model->$canvasField = $oldAttributes[$canvasField];
                    }
                }
            }
        }
    }

    protected function canvasFieldUpload($model, $dataToLoad, $oldValues)
    {
        $activeAttributes = $model->activeAttributes();
        if (! empty($canvasFields = ClassDispenser::getMappedClass(CrudHelper::class)::getCanvasFields($model))) {
            $old_limit = @ini_set('memory_limit', '-1');
            foreach ($canvasFields as $canvasField) {
                if (! in_array($canvasField, $activeAttributes)) {
                    continue;
                }
                $data = $dataToLoad[$canvasField];
                if ($data) {
                    list ($type, $data) = explode(';', $data);
                    list (, $data) = explode(',', $data);
                    $extension = explode('/', explode(':', $type)[1])[1];
                    $data = base64_decode($data);
                    $tempFile = tempnam(sys_get_temp_dir(), 'can');
                    file_put_contents($tempFile, $data);
                    $image = @is_array(getimagesize($tempFile)) ? true : false;
                    if ($image) {
                        $uid = uniqid('');
                        $nameOfFile = 'sign' . $uid . '.' . $extension;
                        $pathInfo = [];
                        $pathInfo['tmp_name'] = [
                            $canvasField => $tempFile
                        ];
                        $pathInfo['name'] = [
                            $canvasField => $nameOfFile
                        ];
                        $model->uploadFile($canvasField, $pathInfo['tmp_name'], $pathInfo['name'], null, true);
                    }
                } else {
                    $model->$canvasField = $oldValues[$canvasField];
                    $pathInfo = [];
                    $pathInfo['tmp_name'] = [
                        $canvasField => ''
                    ];
                    $pathInfo['name'] = [
                        $canvasField => ''
                    ];
                    $params = \Yii::$app->request->getBodyParams();
                    $params[$model->formName()][$canvasField] = - 1;
                    \Yii::$app->request->setBodyParams($params);
                    $model->uploadFile($canvasField, $pathInfo['tmp_name'], $pathInfo['name'], null, true);
                    // $model->$canvasField = $dataToLoad[$canvasField];
                }
            }
            @ini_set('memory_limit', $old_limit);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @param int $id
     * @param null $fromRelation
     * @return mixed
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate(int $id = null, $fromRelation = null, $model_id = null)
    {
        $model = $this->getModel();
        $translations = $languageCodes = [];
        $postParams = \Yii::$app->request->post();

         // Hook before creating
        $model = $this->beforeCreate($model);
        if ($id) {
            $originalModel = $this->findModel($id);
            if ($originalModel && $originalModel->duplicatable()) {
                $executeTime = ini_get('max_execution_time');
                ini_set('max_execution_time', 0);
                $newRecord = $originalModel->duplicateRecord();
                ini_set('max_execution_time', $executeTime);
                return $this->redirect([
                    'update',
                    'id' => $newRecord->primaryKey,
                    'show_deleted'=>Yii::$app->request->get('show_deleted'),
                ]);
            }
        }
        if(!empty($model_id))
        {
            $previousModel = $this->findModel($model_id);
            if(!empty($previousModel))
            {
                $tableSchema = $previousModel->getTableSchema();
                foreach ($tableSchema->columns as $column)
                {
                    $columnName = $column->name;
                    $isForeignColumn = (($temp = strlen($columnName) - strlen('_id')) >= 0 && strpos($columnName, '_id', $temp) !== false );
                    if ($isForeignColumn) {
                        $model->$columnName = $previousModel->$columnName;
                    }
                }
            }
        }
        $fromRelationData = [];
        $hiddenFieldsName = [];
        if($fromRelation){
            //To prefill the parent id value and hide the field in the form
            $loadData = $model->formName()?$_GET[$model->formName()]:$_GET;
            foreach($loadData as $k=>$v){
                if($model->hasAttribute($k) && $model->hasErrors($k)==false){
                    $hiddenFieldsName[$k] = [
                        'parent_class' => 'form-group-block'
                    ];
                    /* $this->formFieldsOverwrite[$k] = Html::activeHiddenInput($model, $k,[
                        'value'=>$v
                    ]); */
                    if($model->formName()){
                        $fromRelationData[$model->formName()][$k] = $v;
                    }else{
                        $fromRelationData[$k] = $v;
                    }
                }
            }
        }
        if (method_exists($model, 'getLanguages')) {
            $pass = true;
            $languages = $model->getLanguages();
            if (! empty($languages)) {
                foreach ($languages as $language) {
                    $translation = Yii::createObject(ltrim($this->model, '\\') . 'Translation');
                    $translation->language_id = $language->language_id;
                    $languageCodes[$language->language_id] = Yii::t('twbp', $language->name);
                    $translations[$language->language_id] = $translation;
                }
            }
        }

        // try {
        if (method_exists($model, 'getLanguages')) {
            if (Yii::$app->request->isPost) {
                foreach ($postParams[$this->baseModel . 'Translation'] as $related) {
                    if (! is_array($related)) {
                        continue;
                    }
                    $translation = Yii::createObject(ltrim($this->model, '\\') . 'Translation');
                    $translation->load($related);
                    $attributes = $translation->attributes;
                    unset($attributes[$this->translationForeignKey . '_id']);
                    foreach ($attributes as $attribute => $value) {
                        $validateAttributes[] = $attribute;
                    }
                    if (! $translation->validate($validateAttributes)) {
                        $pass = false;
                        break;
                    }
                }
            }
        }

        // Translations are good (or no translations) ? save the model
        $langCheck = method_exists($model, 'getLanguages') ? $pass : true;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($langCheck && $model->load($postParams) && $model->save()) {

            if (method_exists($model, 'getLanguages')) {
                foreach ($postParams[$this->baseModel . 'Translation'] as $related) {
                    if (! is_array($related)) {
                        continue;
                    }
                    $translation = Yii::createObject(ltrim($this->model, '\\') . 'Translation');
                    $translation->load($related);
                    $translation->{$this->translationForeignKey . '_id'} = $model->id;
                    $translation->save();
                }
            }
            // Handle uploaded files
            if (! empty($uploadFields = ClassDispenser::getMappedClass(CrudHelper::class)::getUploadFields($this->getModel()))) {
                foreach ($uploadFields as $uploadField) {
                    $model->uploadFile($uploadField, $_FILES[$this->baseModel]['tmp_name'], $_FILES[$this->baseModel]['name']);
                }
            }
            $this->canvasFieldUpload($model, \Yii::$app->request->post($model->formName()), $model->activeAttributes());

            // Save translations
            if (method_exists($model, 'getLanguages')) {
                foreach ($postParams[$this->baseModel . 'Translation'] as $related) {
                    if (! is_array($related)) {
                        continue;
                    }
                    $translationObject = Yii::createObject(ltrim($this->model, '\\') . 'Translation');
                    $translation = $translationObject::findOne([
                        'language_id' => $related[$this->baseModel . 'Translation']['language_id'],
                        $this->translationForeignKey . '_id' => $model->id
                    ]);
                    if (! $translation) {
                        $translation = Yii::createObject(ltrim($this->model, '\\') . 'Translation');
                    }
                    $translation->load($related);
                    $translation->{$this->translationForeignKey . '_id'} = $model->id;
                    $translation->save();

                    // Upload translation files
                    if (! empty($uploadFields = ClassDispenser::getMappedClass(CrudHelper::class)::getUploadFields($translation))) {
                        $language = $related[$this->baseModel . 'Translation']['language_id'];
                        foreach ($uploadFields as $uploadField) {
                            $translation->uploadFile($uploadField, $_FILES[$this->baseModel . 'Translation']['tmp_name'][$language][$this->baseModel . 'Translation'], $_FILES[$this->baseModel . 'Translation']['name'][$language][$this->baseModel . 'Translation'], $language);
                        }
                    }
                }
            }
            if ($this->useModal) {
                return $this->actionIndex();
            }
            if (isset($postParams['submit-default'])) {
                return $this->redirect(array_merge($fromRelationData,[
                    'update',
                    'id' => $model->id,
                    'fromRelation' => $fromRelation
                ]));
            } elseif (isset($postParams['submit-new'])) {
                // On create and new, redirect to referrer to get copy parameters
                return $this->redirect(Yii::$app->request->referrer ? : [
                    'create'
                ]);
            } else if ($fromRelation) {
                return $this->redirect(urldecode($fromRelation));
            } else if (method_exists($this, 'workflowCloseAction')) {
                return $this->workflowCloseAction($model);
            } else {
                return $this->redirect([
                    $this->getModuleControllerUrl()
                ]);
            }
        } elseif (! Yii::$app->request->isPost) {
            $model->load($_GET);
        }
        // } catch (Exception $e) {
        // $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
        // $model->addError('_exception', $msg);
        // }
        
        if ($this->useModal) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'action' => Url::toRoute('create'),
                'useModal' => $this->useModal,
                'inlineForm' => $this->inlineForm,
                'relatedTypeForm' => $this->relatedTypeForm,
                'translations' => $translations,
                'languageCodes' => $languageCodes
            ]);
        } else {
            if ($fromRelation) {
                Yii::$app->controller->crudMainButtons['createnew'] = true; // TODO: Remove, when creating new related model is implemented
            }
            return $this->render('create', [
                'model' => $model,
                'inlineForm' => $this->inlineForm,
                'relatedTypeForm' => $this->relatedTypeForm,
                'translations' => $translations,
                'languageCodes' => $languageCodes,
                'fromRelation' => $fromRelation,
                'hiddenFieldsName' => $hiddenFieldsName
            ]);
        }
    }

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @param null $fromRelation
     * @return mixed
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdate($id, $fromRelation = null, $relationBlock = null)
    {
        $postParams = \Yii::$app->request->post();
        $model = $this->findModel($id);
        $translations = [];
        $languageCodes = [];

        // Hook before updating
        $model = $this->beforeUpdate($model);

        if (! $model->editable()) {
            throw new HttpException(404, Yii::t('twbp', 'The requested page does not exist.'));
        }

        $pass = true;
        $fromRelationData = [];
        $hiddenFieldsName = [];
        if($fromRelation){
            //To prefill the parent id value and hide the field in the form
            $loadData = $model->formName()?$_GET[$model->formName()]:$_GET;
            foreach($loadData as $k=>$v){
                if($model->hasAttribute($k) && $model->hasErrors($k)==false){
                    /* $this->formFieldsOverwrite[$k] = Html::activeHiddenInput($model, $k,[
                        'value'=>$v
                    ]); */
                    $hiddenFieldsName[$k] = [
                        'parent_class' => 'form-group-block'
                    ];
                    if($model->formName()){
                        $fromRelationData[$model->formName()][$k] = $v;
                    }else{
                        $fromRelationData[$k] = $v;
                    }
                }
            }
        }
        if (method_exists($model, 'getLanguages')) {
            $languages = $model->getLanguages();
            foreach ($languages as $language) {
                $translationObject = Yii::createObject(ltrim($this->model, '\\') . 'Translation');
                $translation = $translationObject::findOne([
                    'language_id' => $language->language_id,
                    $this->translationForeignKey . '_id' => $model->id
                ]);
                if (! $translation) {
                    $translation = Yii::createObject(ltrim($this->model, '\\') . 'Translation');
                    $translation->language_id = $language->language_id;
                    $translation->{$this->translationForeignKey . '_id'} = $model->id;
                }
                $languageCodes[$language->language_id] = Yii::t('twbp', $language->name);
                $translations[$language->language_id] = $translation;
            }
            if (Yii::$app->request->isPost) {

                foreach ($postParams[$this->baseModel . 'Translation'] as $related) {
                    if (! is_array($related)) {
                        continue;
                    }
                    $translation = Yii::createObject(ltrim($this->model, '\\') . 'Translation');
                    $translation->load($related);
                    $translation->{$this->translationForeignKey . '_id'} = $id;
                    if (! $translation->validate()) {
                        $pass = false;
                        break;
                    }
                }
            }
        }
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        $langCheck = method_exists($model, 'getLanguages') ? $pass : true;
        if ($langCheck && $model->load($postParams)) {
            foreach ($model::ckeditorFields() as $field){
                $model->$field = str_replace('<p><br data-cke-filler="true"></p>','',$model->$field);
            }
            $this->canvasFieldsReset($model);
            $oldAttributes = $model->oldAttributes;
            if ($model->save()) {
                // Handle uploaded files
                if (! empty($uploadFields = ClassDispenser::getMappedClass(CrudHelper::class)::getUploadFields($this->getModel()))) {
                    foreach ($uploadFields as $uploadField) {
                        $model->uploadFile($uploadField, $_FILES[$this->baseModel]['tmp_name'], $_FILES[$this->baseModel]['name']);
                    }
                }
                $this->canvasFieldUpload($model, \Yii::$app->request->post($model->formName()), $oldAttributes);
                // Save translations
                if (method_exists($model, 'getLanguages')) {
                    foreach ($postParams[$this->baseModel . 'Translation'] as $related) {
                        if (! is_array($related)) {
                            continue;
                        }
                        $translationObject = Yii::createObject(ltrim($this->model, '\\') . 'Translation');
                        $translation = $translationObject::findOne([
                            'language_id' => $related[$this->baseModel . 'Translation']['language_id'],
                            $this->translationForeignKey . '_id' => $model->id
                        ]);

                        if (! $translation) {
                            $translation = Yii::createObject(ltrim($this->model, '\\') . 'Translation');
                        }
                        $translation->load($related);
                        $translation->{$this->translationForeignKey . '_id'} = $id;
                        $translation->save();

                        // Upload translation files
                        if (! empty($uploadFields = ClassDispenser::getMappedClass(CrudHelper::class)::getUploadFields($translation))) {
                            $language = $related[$this->baseModel . 'Translation']['language_id'];
                            foreach ($uploadFields as $uploadField) {
                                $translation->uploadFile($uploadField, $_FILES[$this->baseModel . 'Translation']['tmp_name'][$language][$this->baseModel . 'Translation'], $_FILES[$this->baseModel . 'Translation']['name'][$language][$this->baseModel . 'Translation'], $language);
                            }
                        }
                    }
                }

                if ($this->useModal) {
                    return $this->actionIndex();
                }
                if (isset($postParams['submit-default'])) {
                    return $this->redirect(array_merge($fromRelationData,[
                        'update',
                        'id' => $model->id,
                        'fromRelation' => $fromRelation,
                        'show_deleted'=>Yii::$app->request->get('show_deleted')
                    ]));
                } elseif (isset($postParams['submit-new'])) {
                    return $this->redirect(array_merge($fromRelationData,[
                        'create',
                        'fromRelation' => $fromRelation,
                        'model_id' => $model->id,
                        'show_deleted'=>Yii::$app->request->get('show_deleted'),
                        ! $this->baseModel ? "" : $this->baseModel => Yii::$app->request->get($this->baseModel),
                    ]));
                } elseif (isset($postParams['submit-close'])) {
                    if ($fromRelation) {
                        return $this->redirect(urldecode($fromRelation));
                    } else {
                        return $this->redirect([
                            $this->getModuleControllerUrl()
                        ]);
                    }
                } else {
                    if (method_exists($this, 'workflowCloseAction')) {
                        return $this->workflowCloseAction($model);
                    }
                    return $this->redirect([
                        $this->getModuleControllerUrl()
                    ]);
                }
            }
        }
        
        if ($this->useModal) {
            return $this->renderAjax('_form', [
                'model' => $model,
                'action' => Url::toRoute([
                    'update',
                    'id' => $model->id,
                    'show_deleted'=>Yii::$app->request->get('show_deleted'),
                ]),
                'useModal' => $this->useModal,
                'inlineForm' => $this->inlineForm,
                'relatedTypeForm' => $this->relatedTypeForm,
                'translations' => $translations,
                'languageCodes' => $languageCodes
            ]);
        } else {
            if ($fromRelation) {
                Yii::$app->controller->crudMainButtons['createnew'] = true; // TODO: Remove, when creating new related model is implemented
            }
            return $this->render('update', [
                'model' => $model,
                'inlineForm' => $this->inlineForm,
                'relatedTypeForm' => $this->relatedTypeForm,
                'translations' => $translations,
                'languageCodes' => $languageCodes,
                'fromRelation' => $fromRelation,
                'tabAction'=>'update',
                'hiddenFieldsName' => $hiddenFieldsName
            ]);
        }
    }

    /**
     * Deletes File an existing model.
     * If deletion is successful, the json response will be sent.
     *
     * @param integer $id
     * @param string $attribute
     *            name of the attribute/field
     * @return mixed
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionDeleteFile($id, $attribute)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        if ($model->deletable() && $model->hasAttribute($attribute)) {
            $model->uploadFile($attribute, [
                $attribute => ''
            ], [
                $attribute => ''
            ]);
            $model->$attribute = null;
            
            $fileMetaAttr = $model->getFileMetaAttribute($attribute);
            if($fileMetaAttr){
                $model->{$fileMetaAttr}='';
            }
            
            $model->save(false);
        } else {
            throw new HttpException(404, Yii::t('twbp', 'The requested file does not exist.'));
        }
    }

    /**
     * Deletes an existing model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionDelete($id, $fromRelation = null)
    {
        try {
            $model = $this->findModel($id);
            if ($model->restorable()) {
                $model->restore();
            }elseif ($model->deletable()) {
                $model->delete();
            } else {
                throw new HttpException(404, Yii::t('twbp', 'The requested page does not exist.'));
            }
        } catch (Exception $e) {
            $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
            Yii::$app->getSession()->setFlash('error', $msg);
            return $this->redirect(Url::previous($this->crudUrlKey));
        }

        if (Yii::$app->request->isAjax) {
            return $this->actionIndex();
        }

        // TODO: improve detection
        $isPivot = strstr('$id', ',');
        if ($fromRelation) {
            return $this->redirect(urldecode($fromRelation));
        } else if ($isPivot == true) {
            return $this->redirect(Url::previous($this->crudUrlKey));
        } elseif (isset(Yii::$app->session['__crudReturnUrl']) && Yii::$app->session['__crudReturnUrl'] != '/') {
            Url::remember(null,$this->crudUrlKey);
            $url = Yii::$app->session['__crudReturnUrl'];
            Yii::$app->session['__crudReturnUrl'] = null;
            // Don't redirect to the view of the model that was just deleted
            if (strpos($url, '/backend') !== false || strpos($url, 'view?id=' . $id) !== false) {
                return $this->redirect([
                    'index',
                    'show_deleted'=>Yii::$app->request->get('show_deleted'),
                ]);
            }

            return $this->redirect($url);
        } else {
            return $this->redirect([
                'index',
                'show_deleted'=>Yii::$app->request->get('show_deleted'),
            ]);
        }
    }
    /**
     * Deletes an existing model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionRecompress($id, $fromRelation = null)
    {
        try {
            $model = $this->findModel($id);
            $fields = $model->getUploadFields();
            foreach ($fields as $atr)
            {
                if($model->$atr!=''){
                    $type = $model->getFileType($atr);
                    $compressibleTypes = get_class($model)::$compressibleFileTypes;
                    if(in_array($type, $compressibleTypes) && $model->findCompressQueueJobModel($model,$atr)==null)
                    {
                        $model->compressFile($model,$atr);
                    }
                }
            }
        } catch (Exception $e){
            $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
            Yii::$app->getSession()->setFlash('error', $msg);
            return $this->redirect(Url::previous($this->crudUrlKey));
        }

        if (Yii::$app->request->isAjax) {
            return $this->actionIndex();
        }

        // TODO: improve detection
        $isPivot = strstr('$id', ',');
        if ($fromRelation) {
            return $this->redirect(urldecode($fromRelation));
        } else if ($isPivot == true) {
            return $this->redirect(Url::previous($this->crudUrlKey));
        } elseif (isset(Yii::$app->session['__crudReturnUrl']) && Yii::$app->session['__crudReturnUrl'] != '/') {
            Url::remember(null,$this->crudUrlKey);
            $url = Yii::$app->session['__crudReturnUrl'];
            Yii::$app->session['__crudReturnUrl'] = null;

            // Don't redirect to the view of the model that was just deleted
            if (strpos($url, '/backend') !== false || strpos($url, 'view/?id=' . $id) !== false) {
                return $this->redirect([
                    'index'
                ]);
            }

            return $this->redirect($url);
        } else {
            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionRecompressMultiple()
    {
        $pk = Yii::$app->request->post('pk'); // Array or selected records primary keys
                                         // Preventing extra unnecessary query
        if (! empty($pk)) {
            foreach ($pk as $id) {
                $model = $this->findModel($id);
                $fields = $model->getUploadFields();
                foreach ($fields as $atr)
                {
                    if($model->$atr!=''){
                        $type = $model->getFileType($atr);
                        $compressibleTypes = get_class($model)::$compressibleFileTypes;
                        if(in_array($type, $compressibleTypes) && $model->findCompressQueueJobModel($model,$atr)==null)
                        {
                            $model->compressFile($model,$atr);
                        }
                    }
                }
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->actionIndex();
        }
    }
    public function getGridQuery(){
        $searchModel = $this->getSearchModel();
        $query = \Yii::$app->get('userUi')->get('GridQuery', get_class($searchModel),\Yii::$app->user->id);
        return $query;
    }
    /**
     *
     * @return mixed
     * @throws HttpException
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteMultiple()
    {
        $pk = Yii::$app->request->post('pk'); // Array or selected records primary keys
                                              // Preventing extra unnecessary query
        $runOnAllRecords = false;
        if(\Yii::$app->request->post('action_on_all_rec')==1){
            $runOnAllRecords = true;
            $query = $this->getGridQuery();
        }
        if($runOnAllRecords===true){
            if($query){
            foreach($query->batch(50) as $models){
                foreach($models as $model){
                    if ($model->deletable()){
                        $model->delete();
                    } else {
                        throw new HttpException(404, Yii::t('twbp', 'The requested page does not exist.'));
                    }
                }
            }
            }
        }else if (! empty($pk)) {
            foreach ($pk as $id) {
                $model = $this->findModel($id);
                if ($model->deletable()){
                    $model->delete();
                } else {
                    throw new HttpException(404, Yii::t('twbp', 'The requested page does not exist.'));
                }
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->actionIndex();
        }
    }

    /**
     *
     * @return mixed
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionDuplicateMultiple()
    {
        $pk = Yii::$app->request->post('pk'); // Array or selected records primary keys
                                              // Preventing extra unnecessary query
            if (! empty($pk)) {
                foreach ($pk as $id) {
                    $model = $this->findModel($id);
                    if($model){
                        break;
                    }
                }
            if(\Yii::$app->request->get('job',1)==1){
                // To run duplicating job in the background
                $job = new QueueJob();
                $modelTitle =\yii\helpers\StringHelper::basename(get_class($model));

                $job->queue("Duplicate records of ".$modelTitle." having primary key ".implode(', ',$pk), ClassDispenser::getMappedClass(ModelDuplicater::class), [
                    'model' => get_class($model),
                    'ids' => $pk
                ], true);
            }else{
                $executeTime = ini_get('max_execution_time');
                ini_set('max_execution_time', 0);
                foreach ($pk as $id) {
                    $model = $this->findModel($id);
                    if ($model->duplicatable()) {
                        $model->duplicateRecord();
                    } else {
                        throw new HttpException(404, Yii::t('twbp', 'The requested page does not exist.'));
                    }
                }
                ini_set('max_execution_time', $executeTime);
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->actionIndex();
        }
    }

    /**
     * Finds the model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @param boolean $throwError
     * @return TwActiveRecord the loaded model
     * @throws HttpException if the model cannot be found
     * @throws \yii\base\InvalidConfigException
     */
    protected function findModel($id, $throwError = true)
    {
        $modelClass = $this->getModel();
        if (($model = $modelClass->findOne($id)) !== null) {
            return $model;
        } elseif ($throwError) {
            throw new HttpException(404, Yii::t('twbp', 'The requested page does not exist.'));
        }
        return false;
    }

    /**
     * Call actions
     */
    public function actions()
    {
        $translations = $languageCodes = [];
        $model = $this->getModel();
        if (method_exists($model, 'getLanguages')) {
            $languages = $model->getLanguages();
            $translations = [];
            $languageCodes = [];
            if (isset($_GET['id'])) {
                foreach ($languages as $language) {
                    $translationObject = Yii::createObject(ltrim($this->model, '\\') . 'Translation');
                    $translation = $translationObject::findOne([
                        'language_id' => $language->language_id,
                        $this->translationForeignKey . '_id' => $_GET['id']
                    ]);
                    if (! $translation) {
                        $translation = Yii::createObject(ltrim($this->model, '\\') . 'Translation');
                        $translation->language_id = $language->language_id;
                        $translation->{$this->translationForeignKey . '_id'} = $_GET['id'];
                    }
                    $languageCodes[$language->language_id] = Yii::t('twbp', $language->name);
                    $translations[$language->language_id] = $translation;
                }
            } else {
                foreach ($languages as $language) {
                    $translation = Yii::createObject(ltrim($this->model, '\\') . 'Translation');
                    $translation->language_id = $language->language_id;
                    $languageCodes[$language->language_id] = Yii::t('twbp', $language->name);
                    ;
                    $translations[$language->language_id] = $translation;
                }
            }
        }
        $actions = [
            'related-form' => [
                'class' => RelatedFormAction::class,
                'model' => isset($_GET['id']) ? $this->findModel($_GET['id'], false) : $this->getModel(),
                'depend' => isset($_GET['depend']) ? true : false,
                'dependOn' => isset($_GET['dependOn']) ? true : false,
                'relation' => isset($_GET['relation']) ? $_GET['relation'] : '',
                'relationId' => isset($_GET['relationId']) ? $_GET['relationId'] : '',
                'relationIdValue' => isset($_GET['relationIdValue']) ? $_GET['relationIdValue'] : ''
            ]
        ];

        // Add data for translations
        if (method_exists($model, 'getLanguages')) {
            $actions['related-form']['update'] = isset($_GET['id']) ? true : false;
            $actions['related-form']['translations'] = $translations;
            $actions['related-form']['languageCodes'] = $languageCodes;
            $actions['related-form']['modelId'] = isset($_GET['id']) ? $_GET['id'] : null;
            $actions['related-form']['modelTranslationNamespace'] = '\\' . ltrim($this->model, '\\') . 'Translation';
            $actions['related-form']['modelTranslationAttribute'] = $this->translationForeignKey . '_id';
            $actions['related-form']['modelTranslationName'] = $this->baseModel . 'Translation';
        }

        return $actions;
    }

    /**
     * Update multiple models at once
     */
    public function actionUpdateMultiple()
    {
        $postParams = \Yii::$app->request->post();
        if ($postParams['no-post']) {
            if (! isset($postParams['id']) || empty($postParams['id'])) {
                if (Yii::$app->request->isAjax) {
                    return $this->actionIndex();
                }
                return $this->redirect('index');
            }
            $model = $this->getModel();
            $show = [];
            foreach ($postParams as $element => $value) {
                $show[$element] = $value;
            }
            $method = 'render' . ($this->useModal ? 'Ajax' : '');
            return $this->$method('update-multiple', [
                'model' => $model,
                'show' => $show,
                'pk' => $postParams['id'],
                'useModal' => $this->useModal,
                'action' => Url::toRoute('update-multiple')
            ]);
        } else {
            if (! isset($postParams['pk']) || isset($postParams['close'])) {
                if (Yii::$app->request->isAjax) {
                    return $this->actionIndex();
                }
                return $this->redirect('index');
            }
            foreach ($postParams['pk'] as $id) {
                $model = $this->findModel($id);
                $model->load($postParams);
                $model->save(false);
            }
            if (Yii::$app->request->isAjax) {
                return $this->actionIndex();
            }
            return $this->redirect('index');
        }
    }

    /**
     * Get details of one entry
     * @param $id
     * @return false|string
     */
    public function actionEntryDetails($id)
    {
        $model = $this->findModel($id);
        if ($model) {
            $output = [
                'success' => true,
                'data' => $model->entryDetails
            ];
        } else {
            $output = [
                'success' => false,
                'message' => 'Model does not exist'
            ];
        }
        return json_encode($output);
    }

    /**
     * Get details of one system entry
     * @param $id
     * @return false|string
     */
    public function actionSystemEntry($id)
    {
        $data['success'] = false;
        if ($id) {
            $model = $this->getModel();
            if (method_exists($model, 'findWithSystem')) {
                $pk = $model::primaryKey()[0];
                $model = $model::findWithSystem()->where([
                    $pk => $id
                ])->one();
                $field = strpos(get_class($model), "Client") === false ? "client_id" : "id";
                $client = Client::findWithSystem()->where([
                    'id' => $model->$field
                ])->one();
                if (!empty($client) && $client->is_system_entry) {
                    $data['success'] = true;
                }
            }
        }
        return json_encode($data);
    }

    /**
     * Call a function dynamically
     *
     * @param
     *            $m
     * @param null $q
     * @param null $id
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function actionList($m, $q = null, $id = null, $page=0)
    {
        $function = lcfirst($m) . 'List';
        return $this->getModel()->$function($q, $id,$page);
    }

    /**
     * Hook for controllers before a view
     */
    public function beforeView()
    {
        // Do something fun!
    }

    /**
     * Hook for controllers before creating a model
     *
     * @param TwActiveRecord $model
     * @return TwActiveRecord
     */
    public function beforeCreate(TwActiveRecord $model)
    {
        // Change the model
        $model->loadDefaultValues();
        $model->fillLanguage();

        return $model;
    }

    /**
     * Hook for controllers before updating a model
     *
     * @param TwActiveRecord $model
     * @return TwActiveRecord
     */
    public function beforeUpdate(TwActiveRecord $model)
    {
        // Change the model
        $model->fillLanguage();
        return $model;
    }

    public function crudColumnsOverwrite($crudColumns, $view, $model = null, $form = null, $owner=null)
    {
        if ($form) {
            $crudColumnsOverwrite = $this->formFieldsOverwrite($model, $form,$owner);
        }
        else {
            if (isset($this->crudColumnsOverwrite[$view]) && isset($this->crudColumnsOverwrite['crud']))
                $crudColumnsOverwrite = array_merge($this->crudColumnsOverwrite['crud'], $this->crudColumnsOverwrite[$view]);
            else if (isset($this->crudColumnsOverwrite[$view]))
                $crudColumnsOverwrite = $this->crudColumnsOverwrite[$view];
            else if ($view == 'tab' && isset($this->crudColumnsOverwrite['index']))
                $crudColumnsOverwrite = $this->crudColumnsOverwrite['index'];
            else if (isset($this->crudColumnsOverwrite['crud']))
                $crudColumnsOverwrite = $this->crudColumnsOverwrite['crud'];
        }

        if (! empty($crudColumnsOverwrite)) {

            $crudColumnsAttributeArray = [];
            foreach($crudColumns as $attribute) {
                // TODO needs to be handled correctly
                if(is_object($attribute)) {
                    //print_r($attribute);
                }
                else {
                    $crudColumnsAttributeArray[$attribute['attribute']] = $attribute;
                }
            }

            foreach ($crudColumnsOverwrite as $key => $column) {

                // if $column is only a string, use the original attribute definition
                if(!$form && is_string($column)) {
                    $column = $crudColumnsAttributeArray[$column];
                }

                if (strpos($key, '#') !== false) {
                    list ($operator, $targetAttribute) = explode('#', $key);
                } else {
                    $operator = 'replace';
                    $targetAttribute = $key;
                }

                if($targetAttribute=='client_id'){
                    if(!\Yii::$app->user->can('Authority'))
                    {
                        if($view!='form'){
                            foreach ($crudColumns as $targetKey => $targetColumn) {
                                if (! is_numeric($targetKey))
                                    $attribute = $targetKey;
                                    else if (is_array($targetColumn))
                                        $attribute = $targetColumn['attribute'];
                                        else
                                            $attribute = $targetColumn;
                                            if($attribute=='client_id'){
                                                unset($crudColumns[$targetKey]);
                                            }
                            }
                        }
                    }
                }
                // echo 'search ' . $targetAttribute . ' operator ' . $operator . ' attribute <br>';
                // var_dump($column); echo '<br />';

                /*foreach($crudColumns as $key => $column) {
                    echo $key . '<br>';
                }
                echo '<hr>';*/
                if (!$form) {
                    $crudColumns = array_values($crudColumns);
                }

                foreach ($crudColumns as $targetKey => $targetColumn) {
                    if (! is_numeric($targetKey))
                        $attribute = $targetKey;
                    else if (is_array($targetColumn))
                        $attribute = $targetColumn['attribute'];
                    else
                        $attribute = $targetColumn;

                    // for attributes like email:email
                    if (strpos($attribute, ':') !== false) {
                        $attribute = substr($attribute, 0, strpos($attribute, ':'));
                    }
                    if($attribute=='client_id'){
                        if(!\Yii::$app->user->can('Authority'))
                        {
                            if($view!='form'){
                                unset($crudColumns[$targetKey]);
                            }
                        }
                    }
                    if ($attribute == $targetAttribute) {
                        switch ($operator) {
                            case 'replace': // replace or remove column
                                            // echo 'replace ' . $targetAttribute . '<br />';
                                if (! $column) {
                                    unset($crudColumns[$targetKey]); // remove column
                                } else if ($column !== true) {
                                    $crudColumns[$targetKey] = $column;
                                }
                                break;
                            case 'before': // column before
                                           // echo 'before ' . $targetAttribute . ' now ' . $attribute . '<br>';
                                if (! is_numeric($targetKey)) {
                                    $keys = array_keys($crudColumns);
                                    $insertKey = array_search($targetKey, $keys, true);
                                } else {
                                    $insertKey = $targetKey;
                                }
                                // 'insert ' . $insertKey . ' target ' . $targetKey . '<br>';
                                array_splice($crudColumns, $insertKey, 0, [
                                    $column
                                ]);
                                break;
                            case 'after': // add column after
                                if (! is_numeric($targetKey)) {
                                    $keys = array_keys($crudColumns);
                                    $insertKey = array_search($targetKey, $keys, true) + 1;
                                } else {
                                    $insertKey = $targetKey + 1;
                                }
                                // echo 'after ' . $targetAttribute . ' now ' . $attribute . '<br>';
                                array_splice($crudColumns, $insertKey, 0, [
                                    $column
                                ]);
                                break;
                        }
                    }
                }
                
            }
        }
/*foreach($crudColumns as $key => $column) {
    echo $key . '<br>';
}
exit;*/
        return $crudColumns;
    }

    public function formFieldsOverwrite($model, $form, $owner=null)
    {
        return $this->formFieldsOverwrite;
    }

    public function tabBlocks($model)
    {
        return $this->tabBlocks;
    }

    public function customBlocks($model)
    {
        return $this->customBlocks;
    }

    public function exportColumnsOverwrite($exportColumns)
    {
        return $this->exportColumns;
    }

    public function formDependentShowFieldsOverwrite($model)
    {
        return $model->formDependentShowFields();
    }

    public function crudRelations($relation)
    {
        if (isset($this->crudRelations[$relation])) {
            return $this->crudRelations[$relation];
        } else if (Yii::$app->getUser()->can('Authority')) {
            return true;
        }
        return false; // hide by default
    }

    public function renderCustomBlocks($position,$page=''){
        $blocks = Yii::$app->controller->customBlocks;
        if($page!=''){
            $blocks = $blocks[$page];
        }
        if(property_exists(Yii::$app->view, 'blocks')){
            foreach ($blocks as $key => $value){
                if($value==$position){
                    echo Yii::$app->view->blocks[$key];
                    unset($blocks[$key]);
                }
            }
        }
    }

    /**
     *
     * @return string
     */
    public function getModuleControllerUrl()
    {
        $redirect_url = Yii::$app->controller->module->id ? "/" . Yii::$app->controller->module->id : "";
        $redirect_url .= Yii::$app->controller->id ? "/" . Yii::$app->controller->id : "";
        $redirect_url .= ! $redirect_url ? "index" : "/";
        return $redirect_url;
    }

    /**
     * Upload Image from CkEditor 5
     */
    public function actionUploadImage(){

        $file = UploadedFile::getInstanceByName('upload');
        if($file){
            $file_prefix = time() . '_';
            $file_name = $file_prefix.$file->name;
            $tmp_path = Yii::getAlias('@runtime/upload/'.$file_name);
            $file_path = "uploads/HTMLEditor/". $file_prefix.$file_name;
            $file->saveAs($tmp_path);

            try {
                $stream = fopen($tmp_path, 'r+');
                \Yii::$app->fs->putStream($file_path, $stream);
            } catch (\Exception $e) {
            }

            $data['url'] = ClassDispenser::getMappedClass(ImageHelper::class)::getPublicFileUrl($file_path);
            return json_encode($data);
        }
    }

    protected function updateOnline()
    {
        /**@var $user User|IdentityInterface*/
        $user = Yii::$app->user->identity;
        $lastOnline = Yii::$app->session->get('lastOnline');
        if ($user && ($lastOnline == null || time() - $lastOnline > 300)){
            $lastOnline = strtotime(\Yii::$app->formatter->asDatetime(time()));
            Yii::$app->session->set('lastOnline', $lastOnline);
            $user->last_login_at = $lastOnline;
            $user->save(false);
        }
    }

    public function actionSaveUiData(){
        $flag = Yii::$app->request->post('flag');
        $model = Yii::$app->request->post('model');
        $value = Yii::$app->request->post('value');
        Yii::$app->get('userUi')->set($flag, $model, $value);
        if($flag==='show_deleted' && $value!=='1'){
            $params = Yii::$app->get('userUi')->get('Grid', $model) ?: [];
            $reflection = new \ReflectionClass($model);
            $shortModelName = $reflection->getShortName();
            $params[$shortModelName]['is_deleted'] = null;
            Yii::$app->get('userUi')->set(
                'Grid',
                $model,
                $params
            );
        }

        return true;
    }

    public function redirect($url, $statusCode = 302)
    {
        return Yii::$app->getResponse()->redirect(Url::to($url), $statusCode);
    }
    public function relatedTabsOverwrite($tabs,$model){
        $tabList = [];
        if(isset($tabs['order']) && isset($tabs['list'])){
            $newList = [];
            $order = $tabs['order'];
            $tabList = $tabs['list'];
            asort($order);
            $count = count($tabList);
            $newOrder = [];
            $lastOrder = 1;
            foreach ($order as $k=>$num){
                //settting up order correctly
                $n = is_numeric($num)?intval($num):$lastOrder;
                checkexist:
                if(in_array($n,$newOrder)){
                    $n++;
                    goto checkexist;//to remove order number repition
                }else{
                    $newOrder[$k]=$n;
                    $lastOrder = $n;
                }
            }

            $orderedList = [];
            foreach($newOrder as $n=>$o){
                $orderedList[$n] = $tabList[$n];
                unset($tabList[$n]);
            }
            $tabList = array_values($tabList);
            for($i=1;$i<=$count;$i++){
                if(($k = array_search($i,$newOrder))!=false){
                    a:
                    if(empty($newList[$i])){
                        $newList[$i] = $orderedList[$k];
                        unset($orderedList[$k]);
                    }else{
                        $i++;
                        goto a;
                    }
                }else{
                    if(count($tabList)>0){
                        $newList[$i] = array_shift($tabList);
                    }
                }
            }
            return array_merge($newList,array_values($orderedList));
        }else{
            $tabList = $tabs['list'];
        }
        return $tabList;
    }

}
