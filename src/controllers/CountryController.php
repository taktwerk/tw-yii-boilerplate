<?php

namespace taktwerk\yiiboilerplate\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "CountryController".
 */
class CountryController extends \taktwerk\yiiboilerplate\modules\system\controllers\CountryController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\models\Country';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\models\search\Country';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
}
