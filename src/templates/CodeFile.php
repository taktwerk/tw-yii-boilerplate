<?php 
namespace taktwerk\yiiboilerplate\templates;

use yii\gii\CodeFile as CF;
use yii\helpers\StringHelper;

class CodeFile extends CF{
    
    public $ignoreLineTextList=[
    "//Generation Date:" 
    ];
    
    /**
     * Constructor.
     * @param string $path the file path that the new code should be saved to.
     * @param string $content the newly generated code content.
     * @param array $config name-value pairs that will be used to initialize the object properties
     */
    public function __construct($path, $content, $config = [])
    {
        parent::__construct($path,$content,$config);
        
        $this->path = strtr($path, '/\\', DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR);
        $this->content = $content;
        $this->id = md5($this->path);
        if (is_file($path)) {
            $this->operation = $this->removeIgnoredText(file_get_contents($path)) === $this->removeIgnoredText($content) ? self::OP_SKIP : self::OP_OVERWRITE;
        } else {
            $this->operation = self::OP_CREATE;
        }
    }
    protected function removeIgnoredText($content){
        $contentList = explode(PHP_EOL,$content);
        foreach($contentList as $key=>$line ) {
            
            foreach($this->ignoreLineTextList as $txt){
                
                if(StringHelper::startsWith($line, $txt)) {
                    unset($contentList[$key]);
                }
            }
        }
        
        return implode(PHP_EOL, $contentList);
    }
}