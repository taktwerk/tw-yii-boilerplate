<?php
/**
 * Created by PhpStorm.
 * User: tobias
 * Date: 14.03.14
 * Time: 10:21.
 */
namespace taktwerk\yiiboilerplate\templates\crud\providers;

use function Complex\theta;
use schmunk42\giiant\generators\model\Generator as ModelGenerator;
use taktwerk\yiiboilerplate\templates\crud\Generator as CrudGenerator;
use taktwerk\yiiboilerplate\modules\backend\models\RelationModelInterface;
use yii\db\ActiveRecord;
use yii\db\ColumnSchema;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

class RelationProvider extends \schmunk42\giiant\base\Provider
{
    /**
     * @var null can be null (default) or `select2`
     */
    public $inputWidget = null;

    /**
     * @var bool wheter to skip non-existing columns in relation grid
     *
     * @since 0.6
     */
    public $skipVirtualAttributes = false;

    /**
     * Formatter for relation form inputs.
     *
     * Renders a drop-down list for a `hasOne`/`belongsTo` relation
     * @param $attribute
     * @return string|void
     */
    public function activeField($attribute)
    {
        $column = $this->generator->getColumnByAttribute($attribute);
        if (!$column) {
            return;
        }

        $relation_belongs_to = $this->generator->getRelationByColumn($this->generator->modelClass, $column, ['belongs_to']);

        $relation = $this->generator->getRelationByColumn($this->generator->modelClass, $column);

        if ($relation_belongs_to && $relation->multiple) {
            switch (true) {
                case !$relation_belongs_to->multiple:
                    $pk = key($relation_belongs_to->link);
                    $name = $this->generator->getModelNameAttribute($relation_belongs_to->modelClass);
                    $method = __METHOD__;
                    switch ($this->inputWidget) {
                        case 'select2':
                            $code = <<<EOS
// generated by {$method}
\$form->field(\$model, '{$column->name}')->widget(\kartik\select2\Select2::classname(), [
    'name' => 'class_name',
    'model' => \$model,
    'attribute' => '{$column->name}',
    'data' => \yii\helpers\ArrayHelper::map({$relation_belongs_to->modelClass}::find()->all(), '{$pk}', '{$name}'),
    'options' => [
        'placeholder' => {$this->generator->generateString('Type to autocomplete')},
        'multiple' => false,
        'disabled' => (isset(\$relAttributes) && isset(\$relAttributes['{$column->name}'])),
    ]
])
EOS;
                            break;
                        default:
                            $code = <<<EOS
// generated by {$method}
\$form->field(\$model, '{$column->name}')->dropDownList(
    \yii\helpers\ArrayHelper::map({$relation_belongs_to->modelClass}::find()->all(), '{$pk}', '{$name}'),
    [
        'prompt' => {$this->generator->generateString('Select')},
        'disabled' => (isset(\$relAttributes) && isset(\$relAttributes['{$column->name}'])),
    ]
)
EOS;
                            break;
                    }

                    return $code;
                default:
                    return;

            }
        }

        if ($relation) {
            switch (true) {
                case !$relation->multiple:
                    $pk = key($relation->link);
                    $name = $this->generator->getModelNameAttribute($relation->modelClass);
                    $method = __METHOD__;
                    switch ($this->inputWidget) {
                        case 'select2':
                            $code = <<<EOS
// generated by {$method}
\$form->field(\$model, '{$column->name}')->widget(\kartik\select2\Select2::class, [
    'name' => 'class_name',
    'model' => \$model,
    'attribute' => '{$column->name}',
    'data' => \yii\helpers\ArrayHelper::map({$relation->modelClass}::find()->all(), '{$pk}', '{$name}'),
    'options' => [
        'placeholder' => \Yii::t('{$this->generator->messageCategorySystem}','Type to autocomplete'),
        'multiple' => false,
    ]
])
EOS;
                            
                            break;
                        default:
                            $code = <<<EOS
// generated by {$method}
\$form->field(\$model, '{$column->name}')->dropDownList(
    \yii\helpers\ArrayHelper::map({$relation->modelClass}::find()->all(), '{$pk}', '{$name}'),
    ['prompt' => \Yii::t('{$this->generator->messageCategorySystem}','Select')]
)
EOS;
                            break;
                    }

                    return $code;
                default:
                    return;
            }
        }
    }

    /**
     * Formatter for detail view relation attributes.
     *
     * Renders a link to the related detail view
     * @param $attribute
     * @return string|void
     */
    public function attributeFormat($attribute)
    {
        $column = $this->generator->getColumnByAttribute($attribute);
        if (!$column) {
            return;
        }

        # handle columns with a primary key, to create links in pivot tables (changed at 0.3-dev; 03.02.2015)
        # TODO double check with primary keys not named `id` of non-pivot tables
        # TODO Note: condition does not apply in every case
        if ($column->isPrimaryKey) {
            $code = <<<EOS
           [
            'attribute'=> '{$column->name}'
           ]
EOS;
            return $code;
            return; #TODO: double check with primary keys not named `id` of non-pivot tables
        }

        $relation = $this->generator->getRelationByColumn($this->generator->modelClass, $column, ['belongs_to']);
        $comment = $this->generator->extractComments($column);
        if ($relation) {
            if ($relation->multiple) {
                return;
            }
            $title = $this->generator->getModelNameAttribute($relation->modelClass);
            $route = $this->generator->createRelationRoute($relation, 'view');
            $modelClass = $this->generator->modelClass;
            $relationGetter = 'get' . (new ModelGenerator())->generateRelationName(
                    [$relation],
                    $modelClass::getTableSchema(),
                    $column->name,
                    $relation->multiple
                ) . '()';
            $relationModel = new $relation->modelClass();
            $pks = $relationModel->primaryKey();
            $paramArrayItems = '';
            foreach ($pks as $attr) {
                $paramArrayItems .= "'{$attr}' => \$model->{$relationGetter}->one()->{$attr},";
            }
            $aclID = $this->generator->createRelationPermission($relation, 'view');
            $crud_generator = new CrudGenerator();
            $visibility = $crud_generator->isClientField($this->generator->modelClass, $attribute)? '
                    \'visible\' => '.$crud_generator->checkAuthorityPermission().',
        ':"";
            $method = __METHOD__;
            $code = <<<EOS
                // generated by {$method}
                [
                    'format' => 'html',
                    'attribute' => '$column->name',{$visibility}
                    'value' => function (\$model) {
                        \$foreign = \$model->{$relationGetter}->one();
                        if (\$foreign) {
                            if (Yii::\$app->getUser()->can('app_{$aclID}') && \$foreign->readable()) {
                                return Html::a(\$foreign->toString, [
                                    '{$route}',
                                    {$paramArrayItems}
                                ], ['data-pjax' => 0]);
                            }
                            return \$foreign->toString;
                        }
                        return '<span class="label label-warning">?</span>';
                    },
                ]
EOS;
            return $code;
        } elseif ($comment && @$comment->inputtype == strtolower('qrcode')) {
            $method = __METHOD__;
            $code = <<<EOS
                // generated by {$method}
                [
                    'format' => 'raw',
                    'attribute' => '$column->name',
                    'value' => Html::img(\$model->renderQrCode('$column->name')),
                ]
EOS;
            return $code;
        } elseif ($comment && @$comment->inputtype == strtolower('googlemap')) {
            $method = __METHOD__;
            $code = <<<EOS
                // generated by {$method}
                [
                    'format' => 'raw',
                    'attribute' => '$column->name',
                    'value' => Html::img(\$model->renderGoogleMap('$column->name', ['format' => ClassDispenser::getMappedClass(\\taktwerk\yiiboilerplate\components\GoogleMap::class)::IMAGE])),
                ]
EOS;
            return $code;
        }
    }

    /**
     * Formatter for relation grid columns.
     *
     * Renders a link to the related detail view
     * @param $attribute
     * @param $model ActiveRecord
     * @return string|void
     */
    public function columnFormat($attribute, $model)
    {
        $column = $this->generator->getColumnByAttribute($attribute, $model);
        if (!$column) {
            return;
        }

        # handle columns with a primary key, to create links in pivot tables (changed at 0.3-dev; 03.02.2015)
        # TODO double check with primary keys not named `id` of non-pivot tables
        # TODO Note: condition does not apply in every case
        if ($column->isPrimaryKey) {
//            return null;
        }

        $relation = $this->generator->getRelationByColumn($model, $column);
        $relation_belongs_to = $this->generator->getRelationByColumn($model, $column, ['belongs_to']);

        if ($column->isPrimaryKey) {
            $method = __METHOD__;
            $code = <<<EOS
// generated by {$method}
[
    'attribute' => '{$column->name}',
]
EOS;

            return $code;
        }
        if ($relation_belongs_to && $relation->multiple) {
            if ($relation_belongs_to->multiple) {
                return;
            }
            $title = $this->generator->getModelNameAttribute($relation_belongs_to->modelClass);
            $route = $this->generator->createRelationRoute($relation_belongs_to, 'view');
            $method = __METHOD__;
            $modelClass = $this->generator->modelClass;
            $relation_belongs_toProperty = lcfirst((new ModelGenerator())->generateRelationName(
                [$relation_belongs_to],
                $modelClass::getTableSchema(),
                $column->name,
                $relation_belongs_to->multiple
            ));
            $relation_belongs_toModel = new $relation_belongs_to->modelClass();
            $pks = $relation_belongs_toModel->primaryKey();
            $paramArrayItems = '';

            foreach ($pks as $attr) {
                $paramArrayItems .= "'{$attr}' => \$rel->{$attr},";
            }

            $code = <<<EOS
// generated by {$method}
[
    'class' => yii\\grid\\DataColumn::class,
    'attribute' => '{$column->name}',
    'value' => function (\$model) {
        if (\$rel = \$model->{$relation_belongs_toProperty}) {
            return Html::a(\$rel->{$title}, ['{$route}', {$paramArrayItems}], ['data-pjax' => 0]);
        } else {
            return '';
        }
    },
    'format' => 'raw',
]
EOS;

            return $code;
        }

        if ($relation) {
            if ($relation->multiple) {
                return;
            }

            $title = $this->generator->getModelNameAttribute($relation->modelClass);
            $route = $this->generator->createRelationRoute($relation, 'view');
            $method = __METHOD__;
            $modelClass = $this->generator->modelClass;
            $relationGetter = 'get' . (new ModelGenerator())->generateRelationName(
                    [$relation],
                    $modelClass::getTableSchema(),
                    $column->name,
                    $relation->multiple
                ) . '()';
            $relationModel = new $relation->modelClass();
            $pks = $relationModel->primaryKey();
            $paramArrayItems = '';

            foreach ($pks as $attr) {
                $paramArrayItems .= "'{$attr}' => \$rel->{$attr},";
            }

            $code = <<<EOS
                // generated by {$method}
                [
                    'class' => \\yii\\grid\\DataColumn::class,
                    'attribute' => '{$column->name}',
                    'value' => function (\$model) {
                        if (\$rel = \$model->{$relationGetter}->one()) {
                            return Html::a(
                                \$rel->toString,
                                [
                                    '{$route}',
                                    {$paramArrayItems}
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ]
EOS;

            return $code;
        }

    }


    public static function getHiddenAttributesList(){
        return [ 'created_by',   'created_at',   'updated_by',   'updated_at',  'deleted_by', 'deleted_at', 'local_created_at', 'local_updated_at', 'local_deleted_at'];
    }

    /**
     * Renders a grid view for a given relation.
     * @param $name
     * @param $relation
     * @param bool $showAllRecords
     * @return array
     * @throws \ReflectionException
     */
    public function relationGrid($name, $relation, $showAllRecords = false)
    {
        $model = new $relation->modelClass();
        $parent_model = new $this->generator->modelClass;
        $parent_model_pk = $parent_model->primaryKey()[0];
        $relation_key = key($relation->link);

        // column counter
        $counter = 0;
        ${'columns'.$name} = '';

        $reflection = new \ReflectionClass($relation->modelClass);
        $controller = $this->generator->pathPrefix . Inflector::camel2id($reflection->getShortName(), '-', true);
        //$namespace = $reflection->getNamespaceName(); // model can be out of controller module
        $namespace = $this->generator->controllerClass;
        $module = $this->getModelModule($namespace);

        $basePermission =  $module . '_' . $controller;
        
        // Actions
        $template = '(Yii::$app->getUser()->can(\'' . $basePermission . '_view\') ? \'{view} \' : \'\')';

        if (!$this->generator->isPivotRelation($relation)) {
            // hasMany relations
            $template .= ' . (Yii::$app->getUser()->can(\'' . $basePermission . '_update\') ? \'{update} \' : \'\')';
            
            $uploadFields =  $this->generator->getUploadFields($relation->modelClass);
            
            if(count($uploadFields)>0){
                $template .= ' . (Yii::$app->getUser()->can(\'' . $basePermission . '_recompress\') ? \'{recompress} \' : \'\')';
            }
            $template .= ' . (Yii::$app->getUser()->can(\'' . $basePermission . '_delete\') ? \'{delete} \' : \'\')';
            $deleteButtonPivot = <<<EOS
'delete' => function (\$url, \$model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('{$this->generator->messageCategorySystem}', 'Delete'),
                                \$url,
                                [
                                    'title' => \Yii::t('{$this->generator->messageCategorySystem}', 'Delete'),
                                    'data-confirm' => \Yii::t('{$this->generator->messageCategorySystem}', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
EOS;
        } else {
            // manyMany relations
            $template .= ' . (Yii::$app->getUser()->can(\'' . $basePermission . '_delete\') ? \'{delete} \' : \'\')';
            $deleteButtonPivot = <<<EOS
'delete' => function (\$url, \$model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-remove"></span>'.\Yii::t('{$this->generator->messageCategorySystem}', 'Delete'),
                                \$url,
                                [
                                    'class' => 'text-danger',
                                    'title' => \Yii::t('{$this->generator->messageCategorySystem}', 'Delete'),
                                    'data-confirm' =>\Yii::t('{$this->generator->messageCategorySystem}', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                        'view' => function (\$url, \$model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-cog"></span>',
                                \$url,
                                [
                                    'data-title'  => \Yii::t('{$this->generator->messageCategorySystem}', 'View Pivot Record'),
                                    'data-toggle' => 'tooltip',
                                    'data-pjax'   => '0',
                                    'class'       => 'text-muted',
                                ]
                            );
                        },
EOS;
        }
        $routeAction = $this->generator->getForeignController($relation->modelClass);
        $actionColumn = <<<EOS
[
                    'class' => ClassDispenser::getMappedClass({$this->generator->actionButtonClass}::class),
                    'template' => $template,
                    'urlCreator' => function (\$action, \$relation_model, \$key, \$index) use(\$model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        \$params = is_array(\$key) ? \$key : [\$relation_model->primaryKey()[0] => (string) \$key];
                        \$params[0] = '$routeAction' . '/' . \$action;
                        \$params['{$reflection->getShortName()}'] = [
                            '{$relation_key}' => \$model->{$parent_model_pk}
                        ];
                        \$params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return \$params;
                    },
                    'buttons' => [
                        $deleteButtonPivot
                    ],
                    'controller' => '$controller'
                ]
EOS;

        $orderColumn = $this->generator->getOrderColumn( $model->getTableSchema());
        $orderColumnCmnt = $this->generator->getOrderColumnComment($model->getTableSchema());
        $useSortGrid = (($orderColumnCmnt!='') && property_exists($orderColumnCmnt,'sortRestrictAttributes') && count($orderColumnCmnt->sortRestrictAttributes)>0);

        if($orderColumn && $useSortGrid){
            ${'columns'.$name} .= <<<PHP
[       'headerOptions'=>['style'=>'width:60px'],
        'contentOptions'=>['align'=>'center'],
        'content'=>function(\$model){
                \$spn = Html::tag('span','',['class'=>"glyphicon glyphicon-resize-vertical"]);
                \$tspn = Html::tag('span',\$model->{$orderColumn},['class'=>'label']);
            return Html::a(\$spn.' '.\$tspn,'javascript:void(0)',['class'=>"sortable-row btn btn-default",'title'=>\Yii::t('{$this->generator->messageCategorySystem}','Drag to Re-order')]);
        }
],\n
PHP;
        }
        // add action column
        ${'columns'.$name} .= $actionColumn . ",\n";

        // prepare grid column formatters
        $model->setScenario('crud');
        $safeAttributes = $model->safeAttributes();
        if (empty($safeAttributes)) {
            $safeAttributes = $model->getTableSchema()->columnNames;
        }


        $hiddenAttributes = $this->getHiddenAttributesList();
        $hiddenAttributes = array_merge($hiddenAttributes,[$model->primaryKey()[0]]);
        // Remove _by and _at attributes from generating;
        $safeAttributes = array_diff($safeAttributes, $hiddenAttributes);
        foreach ($safeAttributes as $attr) {
            // max seven columns
            if ($counter > $this->generator->gridRelationMaxColumns) {
                continue;
            }
            // skip virtual attributes
            if ($this->skipVirtualAttributes && !isset($model->tableSchema->columns[$attr])) {
                continue;
            }
            // don't show current model
            if (key($relation->link) == $attr) {
                continue;
            }

            $code = $this->generator->columnFormat($attr, $model);
            if($orderColumn==$attr){
                continue;
            }
            if ($code == false) {
                continue;
            }
            ${'columns'.$name} .= $code . ",\n";
            ++$counter;
        }

        $query = $showAllRecords ?
            "'query' => \\{$relation->modelClass}::find()" :
            "'query' => \$model->get{$name}()";
        if($orderColumn){
        $query .= "->orderBy('{$orderColumn} asc')";
        }
        $pageParam = Inflector::slug("page-{$name}");
        $code = '';

        $code .= '$columns'.$name.' = ['.${'columns'.$name}.'];'."\n";

        if ($model instanceof RelationModelInterface) {
            try{
                $relationController = $model->getControllerInstance();
                $code .= '$relatedModelObject = Yii::createObject('.$relation->modelClass.'::class);'."\n"; //getControllerInstance
                $code .= '$relationController = $relatedModelObject->getControllerInstance();'."\n";
                $code .= '$columns'.$name.' = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite('.'$columns'.$name.', \'tab\'):'.'$columns'.$name.";\n";
            }
            catch(\Error  $e){

            }
        }

        $columnsString = '\'columns\' => $columns'.$name;
        $orderColumnOptions='';
        $grid = '\\taktwerk\\yiiboilerplate\\grid\\GridView';
        if($orderColumn && $useSortGrid){
        $orderColumnOptions = <<<EOT
'sortUrl' => Url::toRoute(['{$this->generator->createRelationRoute($relation, 'sort-item')}']),
EOT;
        $grid = '\\taktwerk\\yiiboilerplate\\grid\\SortableGridView';
}
$generator = $this->generator;
$model =new $generator->modelClass();
$model->setScenario('crud');
$btnTitle  ="\Yii::t('".$this->generator->messageCategory."','".Inflector::singularize(Inflector::camel2words($name))."')";
$routeAction = $generator->getForeignController($relation->modelClass). '/create';
$relSinglName= Inflector::singularize($name);

$relKey  = key($relation->link);
$relClassShortName = StringHelper::basename($relation->modelClass);
$pkName = $model->primaryKey()[0];
$btnNew = <<<EOT
((\$useModal !== true && Yii::\$app->getUser()->can('{$basePermission}_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('{$this->generator->messageCategorySystem}', 'Add {modelName}',['modelName'=>{$btnTitle}]),
                [
                    '{$routeAction}',
                    '{$relClassShortName}' => [
                        '{$relKey}' => \$model->{$pkName}
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'')
EOT;
$summary = <<<EOT
'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
EOT;

        $code .= <<<EOS
echo ClassDispenser::getMappedClass({$grid}::class)::widget([
            'layout' => '<div class="sowing-outer">'.{$btnNew}.'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            {$summary}
            'dataProvider' => new \\yii\\data\\ActiveDataProvider(
                [
                    {$query},
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => '{$pageParam}',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \\Yii::t('{$this->generator->messageCategorySystem}', 'First'),
                'lastPageLabel'  => \\Yii::t('{$this->generator->messageCategorySystem}', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            $orderColumnOptions
            $columnsString
        ])
EOS;

        return ['code' => $code, 'basePermission' => $basePermission];
    }

    /**
     * Determine the model's module for permission based on the namespace of said model.
     * @param $namespace
     * @return string
     */
    protected function getModelModule($namespace)
    {
        $namespaces = explode('\\', $namespace);
        if (in_array('modules', $namespaces)) {
            $foundModule = false;
            foreach ($namespaces as $segment) {
                if ($foundModule) {
                    return $segment;
                } elseif ($segment == 'modules') {
                    $foundModule = true;
                }
            }
        }

        return 'app';
    }
}
