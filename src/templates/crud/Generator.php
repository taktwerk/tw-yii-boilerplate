<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */
namespace taktwerk\yiiboilerplate\templates\crud;

use schmunk42\giiant\helpers\SaveForm;
use taktwerk\yiiboilerplate\helpers\DebugHelper;
use Yii;
use yii\base\Exception;
use yii\db\ColumnSchema;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use schmunk42\giiant\generators\crud\Generator as SchmunkGenerator;
use yii\db\Schema;
use yii\db\TableSchema;
use yii\helpers\BaseStringHelper;
use taktwerk\yiiboilerplate\modules\backend\models\RelationModelInterface;
use taktwerk\yiiboilerplate\templates\CodeFile;
use lajax\translatemanager\models\Language;

/**
 * This generator generates an extended version of CRUDs.
 *
 * @author Tobais Munk <schmunk@usrbin.de>
 * @since 1.0
 */
class Generator extends SchmunkGenerator
{
    use ProviderTrait, ModelTrait;
    // Default Values by taktwerk ...
    public $template = 'taktwerk';

    public $enableI18N = true;

    public $searchModelClass = 'app\\models\\search\\';

    public $controllerClass = 'app\\controllers\\';

    public $controllerApiModule = 'v1';

    public $modelClass = 'app\\models\\';

    public $twoColumnsForm = true;

    public $baseControllerClass = 'taktwerk\\yiiboilerplate\\controllers\\TwCrudController';

    /**
     *
     * @var bool whether to overwrite extended view files
     */
    public $overwriteExtendedViews = true;

    private $_p = [];

    /**
     *
     * @var null comma separated list of provider classes
     */
    public $providerList = null;

    /**
     *
     * @todo review
     * @var string
     */
    public $actionButtonClass = 'taktwerk\\yiiboilerplate\\grid\\ActionColumn';

    /**
     *
     * @var array relations to be excluded in UI rendering
     */
    public $skipRelations = [];

    /**
     *
     * @var string default view path
     */
    public $viewPath = '@backend/views';

    public $tablePrefix = null;

    public $pathPrefix = null;

    public $formLayout = 'horizontal';

    /**
     *
     * @var string translation category
     */
    public $messageCategory = 'app';
    public $messageCategorySystem = 'twbp';
    /**
     *
     * @var int maximum number of columns to show in grid
     */
    public $gridMaxColumns = 100;

    /**
     *
     * @return string
     */
    public function getName()
    {
        return 'Giiant TW-CRUD';
    }

    public function getTimestampComment(){
        $modelGenerator = new \taktwerk\yiiboilerplate\templates\model\Generator();
        return $modelGenerator->getTimestampComment();
    }

    /**
     *
     * @return string
     */
    public function getDescription()
    {
        return 'This generator generates an extended version of CRUDs.';
    }

    /**
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                'twoColumnsForm',
                'safe'
            ]
        ]);
    }

    /**
     * Generates code for active field
     *
     * @param string $attribute
     * @param string $name
     * @param boolean $hidden
     * @param string $id
     * @return string
     */
    public function generateActiveField($attribute, $name = null, $hidden = false, $id = null, $translate = false, $indentMore = 0)
    {
        $indentMore = str_repeat(' ', 4 * $indentMore);
        $tableSchema = $this->getTableSchema();
        // What is this scenario? No tableschema the column isn't set? Feels like generating a login page of some sort.
        if ($tableSchema === false || ! isset($tableSchema->columns[$attribute])) {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $attribute)) {
                return "\$form->field(\$model, '$attribute')->hint(\$model->getAttributeHint('$attribute')->passwordInput()";
            } else {
                return "\$form->field(\$model, '$attribute')->hint(\$model->getAttributeHint('$attribute'))";
            }
        }
        if($this->checkIfCanvasMeta($tableSchema->columns[$attribute]) || $this->checkIfFileMeta($tableSchema->columns[$attribute])){
            return null;
        }

        $model = $name === null ? "\$model" : "\$translation";
        $name = $name !== null ? "'name' => \"$name\", " : '';
        $selector = ",
$indentMore                [
$indentMore                    'selectors' => [
$indentMore                        'input' => '#' .
$indentMore                        " . ($id !== null ? ($translate ? "'$attribute' . " : "$attribute") . "\$key" : "    Html::getInputId($model, '$attribute')") . " . \$owner
$indentMore                    ]
$indentMore                ]";
        $id = "$indentMore'id' => " . ($id !== null ? "'$attribute' . \$key" : "Html::getInputId($model, '$attribute')") . " . \$owner";
        $column = $tableSchema->columns[$attribute];
        $comment = $this->extractComments($column);
        $nameid = trim(rtrim(trim($name) . "\n" . str_repeat(' ', 28) . $id, ','));
        if ($hidden || $this->checkIfHidden($tableSchema->columns[$attribute])) {
            $defaultvalue = $column->defaultValue;
            $valueOption = null;
            if(strlen($defaultvalue)>0){
                $valueOption = "'value' => '$defaultvalue'";
            }
            return "HTML::activeHiddenInput(
$indentMore                $model,
$indentMore                '$attribute',
$indentMore                [
$indentMore                    $nameid,
$indentMore                    $valueOption
$indentMore                ]
$indentMore            )";
        } elseif ($column->phpType === 'boolean' || $column->dbType === 'tinyint(1)' || substr($column->name, 0, 3) == 'is_' || substr($column->name, 0, 4) == 'has_' || ($comment && (@$comment->inputtype === 'checkbox' || @$comment->inputtype === 'radio'))) {
            
            if(($comment && @$comment->inputtype === 'radio')){
                
                if(isset($comment->options) && (is_array($comment->options) ||  is_object($comment->options))){
                    $radioOptions = '[';
                    
                    foreach ($comment->options as $k=>$v){
                        $radioOptions .= $k.'=>\Yii::t("twbp","'.$v.'"),'; 
                    }
                    $radioOptions .= ']';
                }else{
                    $radioOptions .= '[1=>"On",0=>"Off"]';
                }
                
                return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )->radioList($radioOptions,
$indentMore ['item' => function (\$index, \$label, \$name, \$checked, \$value) {
$indentMore            \$return = '<label class=\"btn btn-info '.(\$checked ? ' active' : '').'\">';
$indentMore            \$selected = \$checked ? 'checked' : '';
$indentMore            \$return .= '<input  type=\"radio\" name=\"' . \$name . '\" value=\"' . \$value . '\"' . \$selected . ' >';
$indentMore            \$return .= ucwords(\$label) . '</label>';
$indentMore            return \$return;
$indentMore        },'class' => 'btn-group btn-group-toggle',
$indentMore        'data' => [
$indentMore            'toggle' => \"buttons\"
$indentMore        ]
$indentMore    ]
$indentMore)->hint(" . $model . "->getAttributeHint('$attribute'))";
            }else{
            return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )
$indentMore                     ->checkbox(
$indentMore                         [
$indentMore                             $nameid
$indentMore                         ]
$indentMore                     )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
            }
        } elseif (($column->type === 'text' && ($comment && @$comment->inputtype === 'editor')) || $column->name == 'html' || (strpos($column->name, '_html') !== false)) {
            $toolset = '';
            if ($comment && @$comment->toolset) {
                $toolset = @$comment->toolset;
            }

                if($comment && ($comment->editorversion === '4' || $comment->editorversion === 4)){
                    return "\$form->field(
$indentMore                $model,
$indentMore                '{$attribute}'$selector
$indentMore            )
$indentMore                     ->widget(
$indentMore                         ClassDispenser::getMappedClass(\\taktwerk\\yiiboilerplate\\widget\\CKEditor::class),
$indentMore                         [
$indentMore                             'options' => [
$indentMore                                 $nameid,
$indentMore                              ],
$indentMore                             'editorOptions' => \\mihaildev\\elfinder\\ElFinder::ckeditorOptions(
$indentMore                                 'elfinder-backend',
$indentMore                                 [
$indentMore                                     'preset' => '$toolset', // basic, standard, full (set app config param editorOptions for app wide default)
$indentMore                                 ]
$indentMore                             ),
$indentMore                         ]
$indentMore                     )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
                }else{
                    return "\$form->field(
$indentMore                $model,
$indentMore                '{$attribute}'$selector
$indentMore            )
$indentMore                     ->widget(
$indentMore                         ClassDispenser::getMappedClass(\\taktwerk\\yiiboilerplate\\widget\\CKEditor5::class),
$indentMore                         [
$indentMore                             'options' => [
$indentMore                                 $nameid,
$indentMore                              ],
$indentMore                         ]
$indentMore                     )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
                }

        } elseif (($column->type === 'string' && ($comment && strtolower(@$comment->inputtype) === 'qrcode'))) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '{$attribute}'$selector
$indentMore            )
$indentMore                     ->widget(
$indentMore                         ClassDispenser::getMappedClass(\\taktwerk\\yiiboilerplate\\widget\\QrInput::class),
$indentMore                         [
$indentMore                             $nameid
$indentMore                         ]
$indentMore                     )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";

        } elseif (($column->type === 'string' && ($comment && strtolower(@$comment->inputtype) === 'qrscan')) || (strpos($column->name, '_qrscan') !== false)) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '{$attribute}'$selector
$indentMore            )
$indentMore                     ->widget(
$indentMore                         ClassDispenser::getMappedClass(\\taktwerk\\yiiboilerplate\\widget\\QrScanInput::class),
$indentMore                         [
$indentMore                             $nameid
$indentMore                         ]
$indentMore                     )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))->label(false)";
        } elseif (($column->type === 'string' && (@$comment && strtolower(@$comment->inputtype) === 'googlemap'))) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '{$attribute}'$selector
$indentMore            )
$indentMore                     ->widget(
$indentMore                         ClassDispenser::getMappedClass(\\taktwerk\\yiiboilerplate\\widget\\GoogleMapInput::class),
$indentMore                         [
$indentMore                             $nameid
$indentMore                         ]
$indentMore                     )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
            // HTML 5 input fields
        }elseif (($column->type === 'string' && ($comment && (in_array(strtolower(@$comment->inputtype),['color','colour'])))) 
            || (strpos($column->name, '_color') !== false || strpos($column->name, '_colour') !== false)
            ) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '{$attribute}'$selector
$indentMore            )
$indentMore                     ->widget(
$indentMore                         ClassDispenser::getMappedClass(\\kartik\\color\\ColorInput::class),
$indentMore                         [
$indentMore                            'pluginOptions' => [
$indentMore                              'autoclose' => true,
$indentMore                              'class' => 'form-control',
$indentMore                         ],
$indentMore                             $nameid
$indentMore                         ]
$indentMore                     )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
        }
        elseif ($column->name == 'email' || strpos($column->name, '_email') !== false || ($comment && @$comment->inputtype === 'email')) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )
$indentMore                    ->textInput(
$indentMore                        [
$indentMore                            'type' => 'email',
$indentMore                            $nameid
$indentMore                        ]
$indentMore                    )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif ($column->name == 'telephone' || strpos($column->name, '_telephone') !== false || $column->name == 'tel' || strpos($column->name, '_tel') !== false || ($comment && @$comment->inputtype === 'telephone') || $column->name == 'phone' || strpos($column->name, '_phone') !== false || ($comment && @$comment->inputtype === 'phone')) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )
$indentMore                    ->textInput(
$indentMore                        [
$indentMore                            'type' => 'tel',
$indentMore                            $nameid
$indentMore                        ]
$indentMore                    )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif ($column->name == 'search' || strpos($column->name, '_search') !== false || ($comment && @$comment->inputtype === 'search')) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )
$indentMore                    ->textInput(
$indentMore                        [
$indentMore                            'type' => 'search',
$indentMore                            'placeholder' => " . $model . "->getAttributePlaceholder('$attribute'),
$indentMore                            $nameid
$indentMore                        ]
$indentMore                    )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif ($column->name == 'url' || strpos($column->name, '_url') !== false || ($comment && @$comment->inputtype === 'url') || $column->name == 'link' || strpos($column->name, '_link') !== false) {
            return "\$form->field(
$indentMore                $model,
$indentMore                    '$attribute'$selector
$indentMore            )
$indentMore                    ->textInput(
$indentMore                        [
$indentMore                            'type' => 'url',
$indentMore                            'placeholder' => " . $model . "->getAttributePlaceholder('$attribute'),
$indentMore                            $nameid
$indentMore                        ]
$indentMore                    )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif ($column->type === 'text' || ($comment && @$comment->inputtype === 'text')) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )
$indentMore                    ->textarea(
$indentMore                        [
$indentMore                            'rows' => 6,
$indentMore                            'placeholder' => " . $model . "->getAttributePlaceholder('$attribute'),
$indentMore                            $nameid
$indentMore                        ]
$indentMore                    )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif ($column->dbType === 'date' || ($comment && @$comment->inputtype === 'date')) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '{$attribute}'$selector
$indentMore            )->widget(
$indentMore                \\kartik\\datecontrol\\DateControl::class,
$indentMore                [
$indentMore                    $nameid,
$indentMore                    'type' => \\kartik\\datecontrol\\DateControl::FORMAT_DATE,
$indentMore                    'widgetOptions' => [
$indentMore                        'pluginOptions' => [
$indentMore                            'todayHighlight' => true,
$indentMore                            'todayBtn' => true,
$indentMore                            'autoclose' => true,
$indentMore                            'class' => 'form-control'
$indentMore                        ]
$indentMore                    ],
$indentMore                    'options' => [
$indentMore                        'type' => \\kartik\\date\\DatePicker::TYPE_COMPONENT_APPEND
$indentMore                    ],
$indentMore                ]
$indentMore            )
$indentMore            ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif ($column->dbType === 'datetime' || ($comment && @$comment->inputtype === 'datetime')) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '{$attribute}'$selector
$indentMore            )->widget(
$indentMore                \\kartik\\datecontrol\\DateControl::class,
$indentMore                [
$indentMore                    $nameid,
$indentMore                    'type' => \\kartik\\datecontrol\\DateControl::FORMAT_DATETIME,
$indentMore                    'ajaxConversion' => true,
$indentMore                    'options' => [
$indentMore                        'type' => \\kartik\\datetime\\DateTimePicker::TYPE_COMPONENT_APPEND,
$indentMore                        'pickerButton' => ['icon' => 'time'],
$indentMore                        'pluginOptions' => [
$indentMore                            'todayHighlight' => true,
$indentMore                            'autoclose' => true,
$indentMore                            'class' => 'form-control'
$indentMore                        ]
$indentMore                    ],
$indentMore                ]
$indentMore            )
$indentMore            ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif ($column->dbType === 'time' || ($comment && @$comment->inputtype === 'time')) {
            return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )->widget(
$indentMore                \\kartik\\time\\TimePicker::class,
$indentMore                [
$indentMore                    $nameid,
$indentMore                    'pluginOptions' => [
$indentMore                        'showSeconds' => true,
$indentMore                        'class' => 'form-control'
$indentMore                    ],
$indentMore                ]
$indentMore            )
$indentMore            ->hint(" . $model . "->getAttributeHint('$attribute'))";
            
        } elseif (! empty($column) && $this->checkIfMultiUploaded($column)) {
            $limitation = false;
            if ($comment) {
                $str24 = str_repeat(' ', 24);
                if (@$comment->maxFileSize) {
                    $limitation = "\n" . $str24 . "'maxFileSize' => $comment->maxFileSize,";
                }
                if (@$comment->allowedExtensions && is_array(@$comment->allowedExtensions)) {
                    $limitation .= "\n" . $str24 . "'allowedFileExtensions' => [";
                    foreach (@$comment->allowedExtensions as $extension) {
                        if(count(@$comment->allowedExtensions)>1){
                            $limitation .= '"'. trim($extension).'", ';
                        }else {
                            $limitation .= '"'. trim($extension).'", ';
                        }
                    }
                    $limitation = rtrim($limitation, ', ') . '],';
                }
                
                if (@$comment->minFileCount) {
                    $limitation .= "\n" . $str24 . "'minFileCount' => {$comment->minFileCount},";
                }
                
                if (@$comment->maxFileCount) {
                    $limitation .= "\n" . $str24 . "'maxFileCount' => {$comment->maxFileCount},";
                }
            }
            if ($column->allowNull == false) {
                if (@$comment->minFileCount == false) {
                    $limitation .= "\n" . $str24 . "'minFileCount' => 1,";
                }
            }
            return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )->widget(
                    ClassDispenser::getMappedClass(\\taktwerk\\yiiboilerplate\\modules\\media\\widgets\\MultiFileInput::class),
                    [
                        'fileIdAttr'=>'" . $attribute . "_ids',
                        ".($translate?"'languageKey'=>\$key,":'')."
                        'modelType'=>\\" . $this->modelClass . "::MEDIA_MODEL_TYPE,
                        'mediaCategory'=>\\" . $this->modelClass . "::MEDIA_CATEGORY_" . strtoupper($attribute) . ",
                        'pluginOptions'=>[
$indentMore                        " . ($limitation ? $limitation : '') . "

                        ],
'options' => [
$indentMore                     $name
$indentMore                     $id
$indentMore                    ]
                    ]
                   )
$indentMore            ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } elseif (! empty($column) && $this->checkIfUploaded($column) && $column->allowNull) {
           
            $limitation = false;
            if ($comment) {
                if (@$comment->fileSize) {
                    $limitation = "\n" . str_repeat(' ', 24) . "'maxFileSize' => $comment->fileSize,";
                }
                if (@$comment->allowedExtensions && is_array(@$comment->allowedExtensions)) {
                    $limitation .= "\n" . str_repeat(' ', 24) . "'allowedFileExtensions' => [";
                    foreach (@$comment->allowedExtensions as $extension) {
                        if(count(@$comment->allowedExtensions)>1){
                        $limitation .= '"'. trim($extension).'", ';
                        }else {
                            $limitation .= '"'. trim($extension).'", ';
                        }
                    }
                    $limitation = rtrim($limitation, ', ') . '],';
                }
            }
            
            return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )->widget(
$indentMore                ClassDispenser::getMappedClass(\\taktwerk\\yiiboilerplate\\widget\\FileInput::class),
$indentMore                [
$indentMore                    'pluginOptions' => [
$indentMore                        'showUpload' => false,
$indentMore                        'showRemove' => false,
$indentMore                        " . (! $column->allowNull ? "'required' => true," : '') . "
$indentMore                        " . ($limitation ? $limitation : '') . "
                                   'initialPreview' => (!empty($model" . "->$attribute) ? [
$indentMore                            !empty({$model}->getMinFilePath('$attribute')  && {$model}->getFileType('$attribute') == 'video') ? {$model}->getMinFilePath('$attribute') : {$model}->getFileUrl('$attribute',[],true)
$indentMore                        ] : ''),
$indentMore                        'initialCaption' => $model" . "->$attribute,
$indentMore                        'initialPreviewAsData' => true,
$indentMore                        'initialPreviewFileType' => $model" . "->getFileType('$attribute'),
$indentMore                        'fileActionSettings' => [
$indentMore                            'indicatorNew' => $model->$attribute === null ? '' : '<a href=\" ' . (!empty({$model}->getMinFilePath('$attribute') && {$model}->getFileType('$attribute') == 'video') ? {$model}->getMinFilePath('$attribute') : {$model}->getFileUrl('$attribute')) . '\" target=\"_blank\"><i class=\"glyphicon glyphicon-hand-down text-warning\"></i></a>',
$indentMore                            'indicatorNewTitle' => \\Yii::t('".$this->messageCategorySystem."','Download'),
$indentMore                            'showDrag'=>false,
$indentMore                        ],
$indentMore                        'overwriteInitial' => true,
$indentMore                         'initialPreviewConfig'=> [
$indentMore                                                     [
$indentMore                                                         'url' => Url::toRoute(['delete-file','id'=>$model->" . "getPrimaryKey(), 'attribute' => '$attribute']),
$indentMore                                                         'downloadUrl'=> !empty({$model}->getMinFilePath('$attribute')  && {$model}->getFileType('$attribute') == 'video') ? {$model}->getMinFilePath('$attribute') : {$model}->getFileUrl('$attribute'),
$indentMore                                                         'filetype' => !empty({$model}->getMinFilePath('$attribute') && {$model}->getFileType('$attribute') == 'video') ? 'video/mp4':'',
$indentMore                                                     ],
$indentMore                                                 ],
$indentMore                    ],
$indentMore                     'pluginEvents' => [
$indentMore                         'fileclear' => 'function() { var prev = $(\"input[name=\\'\" + $(this).attr(\"name\") + \"\\']\")[0]; $(prev).val(-1); }',
$indentMore                        " . (! $column->allowNull ? "'fileclear' => 'function() { var prev = $(\"input[name=\\'\" + $(this).attr(\"name\") + \"\\']\")[0]; if($(this)[0].files.length>0){ $(prev).val($(this)[0].files[0].name);} }'," : '') . "
$indentMore                        'filepredelete'=>'function(xhr){ 
$indentMore                               var abort = true; 
$indentMore                               if (confirm(\"Are you sure you want to remove this file? This action will delete the file even without pressing the save button\")) { 
$indentMore                                   abort = false; 
$indentMore                               } 
$indentMore                               return abort; 
$indentMore                        }', 
$indentMore                     ],
$indentMore                    'options' => [
$indentMore                        $nameid
$indentMore                    ]
$indentMore                ]
$indentMore            )
$indentMore            ->hint(" . $model . "->getAttributeHint('$attribute'))";
        
        } elseif (! empty($column) && $this->checkIfCanvas($column)) {
            $canvasCmnt = $this->extractComments($column);
            $canvasAdvanceMode = 'true';
            $canvasAdvanceOptions = '';
            if($canvasCmnt && property_exists($canvasCmnt,'isAdvanceMode')){
                $canvasAdvanceOptions ="'isAdvanceMode'=>".(($canvasCmnt->isAdvanceMode===false||$canvasCmnt->isAdvanceMode==='false')?'false':'true').',';
                if(property_exists($canvasCmnt,'keepMetaData') && ($canvasCmnt->keepMetaData=='true' ||$canvasCmnt->keepMetaData===true))
                {
                    $canvasMetaDataAttribute =  $column->name. '_meta';
                    $canvasAdvanceOptions .= "$indentMore 
 $indentMore            'keepMetaData'=>true,
$indentMore             'metaDataAttribute'=>'{$canvasMetaDataAttribute}',";

if(!isset($tableSchema->columns[$canvasMetaDataAttribute])){
    throw new \Exception(\Yii::t($this->messageCategorySystem,"The meta data attribute(column) \"{metaDataAttribute}\" of the canvas is not present in the table.",
        [
            'metaDataAttribute' => $canvasMetaDataAttribute,
        ]
    ));
}
/* Data type must be longtext for meta data field of the canvas */
if($tableSchema->columns[$canvasMetaDataAttribute]->dbType!='longtext'){
    throw new \Exception(\Yii::t($this->messageCategorySystem,"The meta data attribute(column) \"{metaDataAttribute}\" must be of type \"longtext\".",
        [
            'metaDataAttribute' => $canvasMetaDataAttribute,
        ]
        ));
                    }
                }
            }


            return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )->widget(
                    ClassDispenser::getMappedClass(\\taktwerk\\yiiboilerplate\\widget\\canvasinput\\CanvasInput::class),
                    [
$indentMore             'currentImageUrl'=> !empty({$model}->$attribute) ? {$model}->getFileUrl('$attribute',null,true) : '',
$indentMore             $canvasAdvanceOptions
                    ]
                   )
$indentMore            ->hint(" . $model . "->getAttributeHint('$attribute'))";
        } else {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $column->name) || ($comment && @$comment->inputtype === 'password')) {
                $input = 'passwordInput';
            } else {
                $input = 'textInput';
            }
            if ((is_array($column->enumValues) && count($column->enumValues) > 0) || ($comment && @$comment->inputtype === 'enum')) {
                $dropOptions = $indentMore . "[";
                foreach ($column->enumValues as $enumValue) {
                    $dropOptions .= "\n$indentMore" . str_repeat(" ", 32) . "'" . $enumValue . "' => Yii::t('".$this->messageCategory."', '" . Inflector::humanize($enumValue) . "'),";
                }
                $dropOptions .= "\n$indentMore" . str_repeat(" ", 28) . "$indentMore]";
                return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )
$indentMore                    ->widget(
$indentMore                        Select2::class,
$indentMore                        [
$indentMore                            'options' => [
$indentMore                                $nameid,
$indentMore                                'placeholder' => !" . $model . "->isAttributeRequired('$attribute') ? Yii::t('".$this->messageCategorySystem."', 'Select a value...') : null,
$indentMore                            ],
$indentMore                            'data' => $dropOptions,
$indentMore                            'hideSearch' => 'true',
$indentMore                            'pluginOptions' => [
$indentMore                                'allowClear' => " . ($column->allowNull ? "true" : "false") . "
$indentMore                            ],
$indentMore                        ]
$indentMore                    )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
            } elseif ($column->phpType === 'integer' || $column->phpType === 'double') {
                $step = $column->phpType === 'double' ? 'any' : '1';
                $min = $column->unsigned ? "'min' => '0'," : '';
                return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )
$indentMore                    ->textInput(
$indentMore                        [
$indentMore                            'type' => 'number',
$indentMore                            'step' => '$step',
$indentMore                            'placeholder' => " . $model . "->getAttributePlaceholder('$attribute'),
$indentMore                            $min
$indentMore                            $nameid
$indentMore                        ]
$indentMore                    )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
            } elseif ($column->phpType !== 'string' || $column->size === null || ($comment && @$comment->inputtype === 'string')) {
                return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )
$indentMore                    ->$input(
$indentMore                        [
$indentMore                            $nameid,
$indentMore                            'placeholder' => " . $model . "->getAttributePlaceholder('$attribute'),
$indentMore                        ]
$indentMore                    )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
            } else {
                return "\$form->field(
$indentMore                $model,
$indentMore                '$attribute'$selector
$indentMore            )
$indentMore                    ->$input(
$indentMore                        [
$indentMore                            'maxlength' => true,
$indentMore                            'placeholder' => " . $model . "->getAttributePlaceholder('$attribute'),
$indentMore                            $nameid
$indentMore                        ]
$indentMore                    )
$indentMore                    ->hint(" . $model . "->getAttributeHint('$attribute'))";
            }
        }
    }

    /**
     * Specify the API Controller Location in modules/{module}/controllers
     * TODO: Check original generate function to adapt last changes
     * @inheritdoc
     */
    public function generate()
    {
        /*message category to be twbp when the files to be generated are for BP*/
        if($this->isPartOfBP($this->modelClass)){
            $this->messageCategory = $this->messageCategorySystem;
        }
        
        if (! is_array($this->providerList))
            $this->providerList = explode(',', $this->providerList);
        
        if ($this->singularEntities) {
            $this->modelClass = Inflector::singularize($this->modelClass);
            $this->controllerClass = Inflector::singularize(substr($this->controllerClass, 0, strlen($this->controllerClass) - 10)) . 'Controller';
            $this->searchModelClass = Inflector::singularize($this->searchModelClass);
        }

        $controllerFile = Yii::getAlias('@' . str_replace('\\', '/', ltrim($this->controllerClass, '\\')) . '.php');
        $baseControllerFile = StringHelper::dirname($controllerFile) . '/base/' . StringHelper::basename($controllerFile);
        $apiDir = StringHelper::dirname(Yii::getAlias('@' . str_replace('\\', '/', ltrim('\\app\\modules\\' . $this->controllerApiModule . '\\controllers\\', '\\'))));
        $restControllerFile = $apiDir . '/' . StringHelper::basename($controllerFile);

        if (! file_exists($apiDir)) {
            throw new Exception("Rest API Dir does not exist: " . $apiDir);
        }
        $files[] = new CodeFile($baseControllerFile, $this->render('controller.php'));
        $params['controllerClassName'] = \yii\helpers\StringHelper::basename($this->controllerClass);

        if ($this->overwriteControllerClass || ! is_file($controllerFile)) {
            $files[] = new CodeFile($controllerFile, $this->render('controller-extended.php', $params));
        }

        if ($this->overwriteRestControllerClass || ! is_file($restControllerFile)) {
            $files[] = new CodeFile($restControllerFile, $this->render('controller-rest.php', $params));
        }
        if (!empty($this->searchModelClass)) {
            $searchPrefix = explode('search\\',$this->searchModelClass)[0];
            $searchModelName = explode('search\\',$this->searchModelClass)[1];
            $searchBaseModelNameSpace = $searchPrefix."search\\base\\".$searchModelName;
            $searchBaseModel = Yii::getAlias('@' . str_replace('\\', '/', ltrim($searchBaseModelNameSpace, '\\')).".php");
            $searchModel = Yii::getAlias('@' . str_replace('\\', '/', ltrim($this->searchModelClass, '\\')).".php");
            $files[] = new CodeFile($searchBaseModel, $this->render('search/base/search.php', [
                'searchBaseModelNameSpace'=>$searchBaseModelNameSpace
            ]));
            if ($this->overwriteSearchModelClass || !is_file($searchModel)) {

                $files[] = new CodeFile($searchModel, $this->render('search/search.php', [
                    'searchBaseModelNameSpace'=>$searchBaseModelNameSpace
                ]));
            }
        }

        $viewPath = $this->getViewPath();
        $templatePath = $this->getTemplatePath() . '/views';
        foreach (scandir($templatePath) as $file) {
            if (empty($this->searchModelClass) && $file === '_search.php') {
                continue;
            }
            if (is_file($templatePath . '/' . $file) && pathinfo($file, PATHINFO_EXTENSION) === 'php') {
                if (is_file("$viewPath/$file") && $this->overwriteExtendedViews == false) {
                    continue;
                }
                $files[] = new CodeFile("$viewPath/$file", $this->render("views/$file"));
            }
        }

        $viewPath = $this->getViewPath();
        $templatePath = $this->getTemplatePath() . '/views/base';
        foreach (scandir($templatePath) as $file) {
            if (empty($this->searchModelClass) && $file === '_search.php') {
                continue;
            }
            if (is_file($templatePath . '/' . $file) && pathinfo($file, PATHINFO_EXTENSION) === 'php') {
                $files[] = new CodeFile("$viewPath/base/$file", $this->render("views/base/$file"));
            }
        }

        /*
         * create gii/[name]GiantCRUD.json with actual form data
         */
        $suffix = str_replace(' ', '', $this->getName());
        $controllerFileinfo = pathinfo($controllerFile);
        $formDataFile = StringHelper::dirname(StringHelper::dirname($controllerFile)) . '/gii/' . str_replace('Controller', $suffix, $controllerFileinfo['filename']) . '.json';
        // $formData = json_encode($this->getFormAttributesValues());
        $formData = json_encode(SaveForm::getFormAttributesValues($this, $this->formAttributes()));
        $files[] = new CodeFile($formDataFile, $formData);
       
        return $files;
    }

    /**
     * Get Foreign Model-ClassName (with namespace)
     *
     * @param
     *            $attribute
     * @return mixed|string
     */
    public function fetchForeignClass($attribute)
    {
        $class = '';
        foreach ($this->getModelRelations($this->modelClass) as $modelRelation) {
            foreach ($modelRelation->link as $linkKey => $link) {
                if ($attribute == $link) {
                    $modelObject = Yii::createObject($modelRelation->modelClass);
                    if(in_array($linkKey, $modelObject::primaryKey())){
                        $class = $modelRelation->modelClass;
                    }else{
                        if(empty($class)){
                            $class = $modelRelation->modelClass;
                        }
                    }
                }
            }
        }
        if($class){
            return $class;
        }
        return null;
    }

    /**
     *
     * @param
     *            $attribute
     * @return int|null|string
     */
    public function fetchForeignAttribute($attribute, $modelClass = null)
    {
        if($modelClass){
            foreach ($this->getModelRelations($this->modelClass) as $modelRelation) {
                foreach ($modelRelation->link as $linkKey => $link) {
                    if($modelRelation->modelClass == $modelClass && $attribute == $link){
                        $modelObject = Yii::createObject($modelRelation->modelClass);
                        if(stripos($modelClass, 'group')!==false){
                            return $linkKey;
                        }
                    }
                }
            }
        }

        $foreignModelClass = null;
        foreach ($this->getModelRelations($this->modelClass) as $modelRelation) {
            foreach ($modelRelation->link as $linkKey => $link) {
                if ($attribute == $link && $modelClass) {
                    $modelObject = Yii::createObject($modelRelation->modelClass);
                    if(in_array($linkKey, $modelObject::primaryKey())){
                        $foreign_attribute = $linkKey;
                    }else{
                        if(empty($foreign_attribute)){
                            $foreign_attribute = $linkKey;
                        }
                    }
                }
            }
        }
        if($foreign_attribute){
            return $foreign_attribute;
        }
        return null;
    }

    /**
     *
     * @param ColumnSchema $column
     * @param
     *            $comment
     * @return string
     */
    public function activeFieldDepend(ColumnSchema $column, $comment)
    {
        $attribute = $column->name;
        $tableSchema = $this->getTableSchema();

        $fullModel = $this->fetchForeignClass($attribute);
        $foreignPk = $fullModel::primaryKey()[0];
        $reflection = new \ReflectionClass($fullModel);
        $attributeLabel = $this->fetchForeignAttribute($attribute);

        $on = @$comment->inputtype->depend->on;
        $onFullModel = $reflection->getNamespaceName() . '\\' . $on;
        $onAttribute = @$comment->inputtype->depend->onAttribute;
        $onRelation = @$comment->inputtype->depend->onRelation;
        if ($attributeLabel == null) {
            $attributeLabel = $attribute;
        }
        $column = $tableSchema->columns[$attribute];
        $foreignController = $this->getForeignController($fullModel);
        $foreignTbl = $fullModel::tableName();
        $ajaxRelatedDep = $on . ucfirst(Inflector::singularize($onRelation));
        $ajaxRelatedDepOn = lcfirst($on) . '_id';
        $append = ! $this->twoColumnsForm ? "<div class=\"col-sm-3\"></div>" : '';
        return "\$form->field(
                \$model,
                '$attribute'
            )
                    ->widget(
                        DepDrop::class,
                        [
                            'type'=>DepDrop::TYPE_SELECT2,
                            'data'=>($onFullModel::findOne(\$model->$onAttribute)) ? ArrayHelper::map($onFullModel::findOne(\$model->$onAttribute)->$onRelation, '$attributeLabel', 'toString') : [],
                            'options'=>[
                                'id' => '$attribute' . \$owner,
                                'placeholder' => Yii::t('".$this->messageCategorySystem."', 'Select a value...'),
                            ],
                            'select2Options' => [
                                'pluginOptions' => [
                                    'allowClear' => " . ($column->allowNull ? "true" : "false") . ",
                                ],
                            'addon' => (!\$relatedForm || (\$relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL && !\$useModal)) ? [
                                'append' => [
                                    'content' => [
                                        ClassDispenser::getMappedClass(RelatedForms::class)::widget([
                                            'relatedController' => '$foreignController',
                                            'type' => \$relatedTypeForm,
                                            'selector' => '$attribute' . (\$ajax || \$useModal ? '_ajax_' . \$owner : ''),
                                            'primaryKey' => '$foreignPk',
                                            'depend' => true,
                                            'dependOn' => '$onAttribute',
                                            'relation' => '$ajaxRelatedDep',
                                            'relationId' => '$ajaxRelatedDepOn',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                            ],
                            'pluginEvents' => [
                                \"change\" => \"function() {
                                    if ($(this).prop('disabled')) {
                                        // Disable add icon
                                        $(this).next().next().next().children('button').prop('disabled', true);
                                    } else {
                                        // Enable add icon
                                        $(this).next().next().next().children('button').prop('disabled', false);
                                    }
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable update icon
                                        $(this).next().next().children('button').prop('disabled', false);
                                        $.post('\" .
                                        Url::toRoute('$foreignController/entry-details?id=', true) .
                                        \"' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#" . $attribute . "_well').html(json.data);
                                                //$('#" . $attribute . "_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }\",
                            ],
                            'pluginOptions'=>[
                                'depends' => ['$onAttribute'],
                                'url' => Url::to(['depend', 'on' => '$on', 'onRelation' => '$onRelation']),
                            ]
                        ]
                    ) . '
                $append
                <div id=\"$attribute" . "_well\" class=\"well col-sm-6 hidden\"
                    style=\"margin-left: 8px; display: none;' . 
                    ($fullModel::find()->where(['{$foreignTbl}.{$attributeLabel}' => \$model->$attribute])->one()
                        ->entryDetails == '' ?
                    ' display:none;' :
                    '') . '
                    \">' . 
                    ($fullModel::find()->where(['{$foreignTbl}.{$attributeLabel}' => \$model->$attribute])->one()->entryDetails != '' ?
                    $fullModel::find()->where(['{$foreignTbl}.{$attributeLabel}' => \$model->$attribute])->one()->entryDetails :
                    '') . ' 
                </div>
                <div class=\"clearfix\"></div>
                <div class=\"col-md-12 sub-form\" id=\"address_id_inline\" style=\"display: none;\">
                </div>'";
    }

    /**
     * DropDown-Field Select2 Generator Method / by taktwerk.com
     *
     * Generates code for active field dropdown by using the provider queue
     *
     * @param ColumnSchema $column
     * @param null $model
     *
     * @return mixed|string
     */
    public function activeFieldDropDown(ColumnSchema $column, $model = null)
    {
        if (($comment = $this->extractComments($column)) && isset($comment->inputtype->depend) && isset($comment->inputtype->depend->on) && isset($comment->inputtype->depend->onAttribute) && isset($comment->inputtype->depend->onRelation)) {
            return $this->activeFieldDepend($column, $comment);
        }
        $attribute = $column->name;
        $tableSchema = $this->getTableSchema();

        $foreignLabelAttribute = "toString";
        $fullModel = $this->fetchForeignClass($attribute);
        if(!$fullModel){
            return "\$form->field(\$model, '$attribute')";
        }
        $foreignPk = $fullModel::primaryKey()[0];
        $reflection = new \ReflectionClass($fullModel);
        $shortModel = $reflection->getShortName();
        $attributeLabel = $this->fetchForeignAttribute($attribute, $fullModel::className());

        if ($attributeLabel == null) {
            $attributeLabel = $attribute;
        }

        if ($tableSchema === false || ! isset($tableSchema->columns[$attribute])) {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $attribute)) {
                return "\$form->field(\$model, '$attribute')->passwordInput()";
            } else {
                return "\$form->field(\$model, '$attribute')";
            }
        }
        $column = $tableSchema->columns[$attribute];
        $foreignController = $this->getForeignController($fullModel);
        
        $append = ! $this->twoColumnsForm ? "<div class=\"col-sm-3\"></div>" : '';
        $acl = $this->getForeignPermission($fullModel, 'create');
        
        $change_event = "if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('\" .
                                        Url::toRoute('$foreignController/entry-details?id=', true) .
                                        \"' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#" . $attribute . "_well').html(json.data);
                                                //$('#" . $attribute . "_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }";
        if($this->isClientField($fullModel,null)){
            $change_event = "var data_id = $(this);
                                $.post('\" .
            Url::toRoute('$foreignController/system-entry?id=', true) .
            \"' + data_id.val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if(!json.success){
                                                if ((data_id.val() != null) && (data_id.val() != '')) {
                                        // Enable edit icon
                                        $(data_id.next().next().children('button')[0]).prop('disabled', false);
                                        $.post('\" .
            Url::toRoute('$foreignController/entry-details?id=', true) .
            \"' + data_id.val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#" . $attribute . "_well').html(json.data);
                                                //$('#" . $attribute . "_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $(data_id.next().next().children('button')[0]).prop('disabled', true);
                                    }
                                            }else{
                                        // Disable edit icon and remove sub-form
                                        $(data_id.next().next().children('button')[0]).prop('disabled', true);
                                            }
                                        });";
        }
        $val = null;
        $fullModelTbl = $fullModel::tableName();
        $relPK = $fullModel::primaryKey()[0];
        $groupBySt= '';
        if($relPK){
            $groupBySt = "->groupBy('{$fullModelTbl}.{$relPK}')";
        }
        $initValText = <<<EOT
        'initValueText' => $fullModel::find(){$groupBySt}->count() > 50 ? \\yii\\helpers\\ArrayHelper::map($fullModel::find()->andWhere(['{$fullModelTbl}.{$attributeLabel}' => \$model->{$attribute}]){$groupBySt}->all(), '$attributeLabel', '$foreignLabelAttribute') : ''
EOT;
        if(is_subclass_of($fullModel,Language::class)){
            //To pre-select the current language on a new record/model
         $val =  <<<EOT
            'value' => (\$model->isNewRecord && \$model->{$attribute}==null)?\Yii::\$app->language:\$model->{$attribute}
EOT;
         $initValText = <<<EOT
        'initValueText' => $fullModel::find(){$groupBySt}->count() > 50 ? \\yii\\helpers\\ArrayHelper::map($fullModel::find()->andWhere(['$attributeLabel' => (\$model->isNewRecord && \$model->{$attribute}==null) ?\Yii::\$app->language:\$model->{$attribute}])->all(), '$attributeLabel', '$foreignLabelAttribute') : ''
EOT;
        }
        return "\$form->field(
                \$model,
                '$attribute'
            )
                    ->widget(
                        Select2::class,
                        [
                            'data' => $fullModel::find()->count() > 50 ? null : ArrayHelper::map($fullModel::find(){$groupBySt}->all(), '$attributeLabel', '$foreignLabelAttribute'),
                            $initValText,
                            'options' => [
                                'placeholder' => Yii::t('".$this->messageCategorySystem."', 'Select a value...'),
                                'id' => '$attribute' . (\$ajax || \$useModal ? '_ajax_' . \$owner : ''),
                                $val
                            ],
                            'pluginOptions' => [
                                'allowClear' => " . ($column->allowNull ? "true" : "false") . ",
                                ($fullModel::find(){$groupBySt}->count() > 50 ? 'minimumInputLength' : '') => 3,
                                ($fullModel::find(){$groupBySt}->count() > 50 ? 'ajax' : '') => [
                                    'url' => \\yii\\helpers\\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \\yii\\web\\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \\'" . $shortModel . "\\', page:params.page || 1
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                \"change\" => \"function() {
                                    {$change_event}
                                }\",
                            ],
                            'addon' => (Yii::\$app->getUser()->can('$acl') && (!\$relatedForm || (\$relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL && !\$useModal))) ? [
                                'append' => [
                                    'content' => [
                                        ClassDispenser::getMappedClass(RelatedForms::class)::widget([
                                            'relatedController' => '$foreignController',
                                            'type' => \$relatedTypeForm,
                                            'selector' => '$attribute' . (\$ajax || \$useModal ? '_ajax_' . \$owner : ''),
                                            'primaryKey' => '$foreignPk',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    ) . '
                    
                $append
                <div id=\"$attribute" . "_well\" class=\"well col-sm-6 hidden\"
                    style=\"margin-left: 8px; display: none;' . 
                    ($fullModel::find()->where(['{$fullModelTbl}.{$attributeLabel}' => \$model->$attribute])->one()
                        ->entryDetails == '' ?
                    ' display:none;' :
                    '') . '
                    \">' . 
                    ($fullModel::find()->where(['{$fullModelTbl}.{$attributeLabel}' => \$model->$attribute])->one()->entryDetails != '' ?
                    $fullModel::find()->where(['{$fullModelTbl}.{$attributeLabel}' => \$model->$attribute])->one()->entryDetails :
                    '') . ' 
                </div>
                <div class=\"clearfix\"></div>
                <div class=\"col-md-12 sub-form\" id=\"address_id_inline\" style=\"display: none;\">
                </div>'";
    }

    /**
     * Generates code for active field by using the provider queue.
     *
     * @param
     *            $attribute
     * @param null $model
     * @return mixed|string|void
     */
    public function activeField($attribute, $model = null)
    {
        if ($model === null) {
            $model = $this->modelClass;
        }
        $code = $this->callProviderQueue(__FUNCTION__, $attribute, $model, $this);
        if ($code !== null) {
            Yii::debug("found provider for '{$attribute}'", __METHOD__);

            return $code;
        } else {
            $column = $this->getColumnByAttribute($attribute);
            if (! $column) {
                return;
            } else {
                return $this->generateActiveField($attribute);
            }
        }
    }

    /**
     * Extract comments from database
     *
     * @param
     *            $column
     * @return bool|mixed
     */
    public function extractComments($column)
    {
        $output = json_decode($column->comment);
        if (is_object($output)) {
            return $output;
        }
        return false;
    }

    public function extractCommentByColName($columnName, TableSchema $tableSchema)
    {
        foreach ($tableSchema->columns as $col) {
            if ($col->name == $columnName) {
                return $this->extractComments($col);
            }
        }
        return '';
    }

    /**
     * Helper function to get controller name of foreign model for ajax call
     *
     * @param string $model
     * @return string
     */
    public function getForeignController($model)
    {
        $modelName = $foreignController = substr($model, strrpos($model, '\\') + 1);
        // foreign model namespace
        $controllerClass = substr($model, 0, (strrpos($model, '\\models') + 1)) . "controllers\\";
        $controllerName = strtolower(Inflector::slug(Inflector::camel2words($modelName), '-'));
        $controllerTitleCaseName = str_replace(" ", "", ucwords(str_replace("-"," ", $controllerName)));
        
        if (! class_exists($controllerClass . $controllerTitleCaseName . "Controller")) {
            $controllerClass = $this->controllerClass;
        }
        $modelObject = Yii::createObject($model);
        if($modelObject instanceof RelationModelInterface){
            $controllerInstance = $modelObject->getControllerInstance();
            if($controllerInstance){
                $controllerClass = get_class($controllerInstance);
            }
        }
        $ns = \yii\helpers\StringHelper::dirname(ltrim($controllerClass, '\\'));
        $url = str_replace([
            '\\app',
            'app',
            '\\controllers',
            '\\modules'
        ], '', $ns);
        
        $url = str_replace([
            '\\taktwerk',
            'taktwerk',
            '\\controllers',
            '\\yiiboilerplate',
            '\\modules'
        ], '', $url);
        $url = str_replace('\\', '/', $url);
        
        return $url . '/' . $controllerName;
    }

    /**
     *
     * @param
     *            $model
     * @param null $action
     * @return string
     */
    protected function getForeignPermission($model, $action = null)
    {
        $namespace = explode('\\', $model);
        $module = 'backend'; //default module
        if ($namespace[1] !== 'models') {
            $module = $namespace[2];
        }
        $modelName = $foreignController = substr($model, strrpos($model, '\\') + 1);
        $controllerName = strtolower(Inflector::slug(Inflector::camel2words($modelName), '-'));

        return $module . '_' . $controllerName . (! empty($action) ? "_$action" : null);
    }

    /**
     * Generates search conditions
     *
     * @return array
     */
    public function generateSearchConditions()
    {
        $columns = [];
        if (($table = $this->getTableSchema()) === false) {
            $class = $this->modelClass;
            /* @var $model \yii\base\Model */
            $model = new $class();
            foreach ($model->attributes() as $attribute) {
                $columns[$attribute] = 'unknown';
            }
        } else {
            foreach ($table->columns as $column) {
                $columns[$column->name] = $column->type;
            }
        }

        $likeConditions = [];
        $hashConditions = [];
        $dateConditions = [];
        $conditions = [];
        foreach ($table->columns as $colName => $col) {
            switch ($col->type) {
                case Schema::TYPE_TINYINT:
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                case Schema::TYPE_BOOLEAN:
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                case Schema::TYPE_TIME:
                case StringHelper::startsWith($col->dbType,'enum('):
                case Schema::TYPE_TIMESTAMP:
                    $hashConditions[] = "\$this->applyHashOperator('{$colName}', \$query);";
                    break;
                case Schema::TYPE_DATETIME:
                    $dateConditions[] = "\$this->applyDateOperator('{$colName}', \$query, true);";
                    break;
                case Schema::TYPE_DATE:
                    $dateConditions[] = "\$this->applyDateOperator('{$colName}', \$query);";
                    break;
                default:
                    $likeConditions[] = "\$this->applyLikeOperator('{$colName}', \$query);";
                    break;
            }
        }

        if (! empty($hashConditions)) {
            $conditions[] = implode("\n" . str_repeat(' ', 8), $hashConditions);
        }
        if (! empty($likeConditions)) {
            $conditions[] = implode("\n" . str_repeat(' ', 8), $likeConditions);
        }
        if (! empty($dateConditions)) {
            $conditions[] = implode("\n" . str_repeat(' ', 8), $dateConditions);
        }

        return $conditions;
    }

    /**
     * Generates validation rules for the search model.
     *
     * @return array the generated validation rules
     */
    public function generateSearchRules()
    {
        if (($table = $this->getTableSchema()) === false) {
            return [
                "[['" . implode("', '", $this->getColumnNames()) . "'], 'safe']"
            ];
        }
        $types = [];
        foreach ($table->columns as $column) {
            switch ($column->type) {
                case Schema::TYPE_BOOLEAN:
                    $types['boolean'][] = $column->name;
                    break;
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                case Schema::TYPE_DATE:
                case Schema::TYPE_TIME:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                default:
                    $types['safe'][] = $column->name;
                    break;
            }
        }

        $types['safe'][] = 'is_deleted';

        $rules = [];
        foreach ($types as $type => $columns) {
            $rules[] = "[
                [
                    '" . implode("',\n" . str_repeat(' ', 20) . "'", $columns) . "'\n" . str_repeat(' ', 16) . "],
                '$type'
            ]";
        }

        return $rules;
    }

    /**
     * List of primary keys
     *
     * @return array
     */
    public function getPrimaryKeys()
    {
        $schema = $this->getTableSchema();
        $primaryKeys = [];
        foreach ($schema->columns as $column) {
            if ($column->isPrimaryKey) {
                $primaryKeys[] = $column->name;
            }
        }

        return $primaryKeys;
    }

    /**
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getUploadFields($class = null)
    {
        if ($class === null) {
            $class = $this->modelClass;
        }
        /** @var \yii\db\ActiveRecord $model */
        $model = new $class();
        $model->setScenario('crud');
        $safeAttributes = $model->safeAttributes();
        $tblSchema = $model->getTableSchema();
        if (empty($safeAttributes)) {
            $safeAttributes = $tblSchema->columnNames;
        }
        $out = [];
        
        foreach ($safeAttributes as $attribute) {
            $column = ArrayHelper::getValue($tblSchema->columns, $attribute);
            if (! empty($column) && $this->checkIfUploaded($column)) {
                $out[] = $attribute;
            }
        }
        return $out;
    }

    /**
     * Check whether client_id exists or not in the model
     *
     * @param null $class
     * @param null $attribute
     * @return bool
     */
    public function isClientField($class = null, $attribute = null)
    {
        if ($class === null) {
            $class = $this->modelClass;
        }

        /** @var \yii\db\ActiveRecord $model */
        $model = new $class();
        if (method_exists($model, 'getclient') && $model->getclient()) {
            
            $client_relation = $model->getRelation('client')->modelClass;

            if(stripos($client_relation,'Client')===false){
                return false;
            }

            $safeAttributes = $model->safeAttributes();

            if (isset($attribute)) {
                if (! empty($client_relation) && $attribute == "client_id") {
                    return true;
                }
            } else {
                if (! empty($client_relation) && in_array("client_id", $safeAttributes)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     *
     * @param ColumnSchema $column
     * @return bool
     */
    public function checkIfCanvas(ColumnSchema $column)
    {
        $comment = $this->extractComments($column);
        
        if (preg_match('/(_canvas)$/i', $column->name) || ($comment && (@$comment->inputtype === 'canvas'))) {
            return true;
        }
        return false;
    }
    /**
     *
     * @param ColumnSchema $column
     * @return bool
     */
    public function checkIfCanvasMeta(ColumnSchema $column)
    {
        $comment = $this->extractComments($column);
        if (preg_match('/(_canvas_meta)$/i', $column->name) || ($comment && (@$comment->inputtype === 'canvas_meta'))) {
            return true;
        }
        return false;
    }
    /**
     *
     * @param ColumnSchema $column
     * @return bool
     */
    public function checkIfFileMeta(ColumnSchema $column)
    {
        $comment = $this->extractComments($column);
        if (preg_match('/(_filemeta)$/i', $column->name) || ($comment && (@$comment->inputtype === 'filemeta'))) {
            return true;
        }
        return false;
    }
    public function checkIfHidden(ColumnSchema $column)
    {
        $comment = $this->extractComments($column);
        if (preg_match('/(_hidden)$/i', $column->name) || ($comment && (@$comment->inputtype === 'hidden')) || preg_match('/(_meta)$/i', $column->name) || ($comment && (@$comment->inputtype === 'meta'))) {
            return true;
        }
        return false;
    }
    
    /**
     *
     * @param ColumnSchema $column
     * @return bool
     */
    public function checkIfUploaded(ColumnSchema $column)
    {
        $comment = $this->extractComments($column);
        if (preg_match('/(_upload|_file)$/i', $column->name) || ($comment && (@$comment->inputtype === 'upload' || @$comment->inputtype === 'file'))) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param ColumnSchema $column
     * @return bool
     */
    public function checkIfMultiUploaded(ColumnSchema $column)
    {
        $comment = $this->extractComments($column);
        if (preg_match('/(_media)$/i', $column->name) || ($comment && (@$comment->inputtype === 'uploads' || @$comment->inputtype === 'files'))) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param TableSchema $tableSchema
     * @return bool |string
     */
    public function getOrderColumn(TableSchema $tableSchema)
    {
        if (in_array('order_number', $tableSchema->columnNames)) {
            return 'order_number';
        }
        return false;
    }

    /**
     *
     * @param TableSchema $tableSchema
     * @return bool |string
     */
    public function getOrderColumnComment(TableSchema $tableSchema)
    {
        if ($name = $this->getOrderColumn($tableSchema)) {
            foreach ($tableSchema->columns as $col) {
                if ($col->name == $name) {
                    return $this->extractComments($col);
                }
            }
        }
        return '';
    }
    public function isTranslationTbl($tblName){
        $modelGenerator = new \taktwerk\yiiboilerplate\templates\model\Generator();
        return $modelGenerator->isTranslationTbl($tblName);
    }
    public function isPartOfBP($namespaceOrPath=null){
        $modelGenerator = new \taktwerk\yiiboilerplate\templates\model\Generator();
        return $modelGenerator->isPartOfBP($namespaceOrPath);
    }

    /**
     * Check for client relation in model
     *
     * @param
     *            $relationName
     * @param
     *            $field
     * @return bool
     */
    public function checkClientRelation($relationName, $field)
    {
        if (strpos($relationName, "Client") !== false && strpos($field, "client_id") !== false) {
            return true;
        }
        return false;
    }

    /**
     * Function to check permission: Authority
     */
    public function checkAuthorityPermission()
    {
        return 'Yii::$app->getUser()->can(\'Authority\')';
    }
}
