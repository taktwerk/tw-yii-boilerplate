<?php
/**
 * This is the template for generating CRUD search class of the specified model.
 */

use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator \taktwerk\yiiboilerplate\templates\crud\Generator */
/* @var $searchBaseModelNameSpace string */

$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
    $modelAlias = $modelClass . 'Model';
}
$rules = $generator->generateSearchRules();
$labels = $generator->generateSearchLabels();
$searchAttributes = $generator->getSearchAttributes();
$searchConditions = $generator->generateSearchConditions();
$orderColumnName = $generator->getOrderColumn($generator->getTableSchema());
$removeDeleteCondition = "(Yii::\$app->get('userUi')->get('show_deleted', ".(isset($modelAlias) ? $modelAlias : $modelClass)."::class) === '1'|| \$showDeleted)?false:true";
$query_type = $generator->isClientField($generator->modelClass, null)?"findWithOutSystemEntry($removeDeleteCondition)":"find($removeDeleteCondition)";
echo "<?php\n";
?>
<?php echo $generator->getTimestampComment();?>
namespace <?= StringHelper::dirname(ltrim($searchBaseModelNameSpace, '\\')) ?>;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use <?= ltrim($generator->modelClass, '\\') . (isset($modelAlias) ? " as $modelAlias" : "") ?>;

/**
 * <?= $searchModelClass ?> represents the base model behind the search form about `<?= $generator->modelClass ?>`.
 */
class <?= $searchModelClass ?> extends <?= isset($modelAlias) ? $modelAlias : $modelClass . "\n"?>
{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            <?= implode(",\n" . str_repeat(' ', 12), $rules) ?>,
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$showDeleted = (property_exists(\Yii::$app->controller, 'model')
            &&
            (is_subclass_of(<?= isset($modelAlias) ? $modelAlias : $modelClass ?>::class, \Yii::$app->controller->model) 
            	||
            	is_subclass_of(\Yii::$app->controller->model, <?= isset($modelAlias) ? $modelAlias : $modelClass ?>::class))
            &&
              (Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model)
                ||
                Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model)))
            );
        $query = <?= isset($modelAlias) ? $modelAlias : $modelClass ?>::<?= $query_type?>;

        $this->parseSearchParams(<?= isset($modelAlias) ? $modelAlias : $modelClass ?>::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =><?php if($orderColumnName){
                ?>['<?= $orderColumnName?>'=>SORT_ASC]<?php }else{
                ?>$this->parseSortParams(<?= isset($modelAlias) ? $modelAlias : $modelClass ?>::class)<?php
 }?>,
				'params' => array_merge($params,['page'=>1])
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(<?= isset($modelAlias) ? $modelAlias : $modelClass ?>::class),
                'params' => array_merge($params,[
                    'page' => $this->parsePageParams(<?= isset($modelAlias) ? $modelAlias : $modelClass ?>::class),
                ])
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        <?= implode("\n        ", $searchConditions) ?>

        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(<?= isset($modelAlias) ? $modelAlias : $modelClass ?>::class);
    }
}
