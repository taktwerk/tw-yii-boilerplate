<?php
/**
 * This is the template for generating CRUD search class of the specified model.
 */

use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator \taktwerk\yiiboilerplate\templates\crud\Generator */
/* @var $searchBaseModelNameSpace string */

$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
    $searchModelAlias = $modelClass . 'SearchModel';
}
echo "<?php\n";
?>
<?php echo $generator->getTimestampComment();?>
namespace <?= StringHelper::dirname(ltrim($generator->searchModelClass, '\\')) ?>;

use <?= ltrim($searchBaseModelNameSpace, '\\') . (isset($searchModelAlias) ? " as $searchModelAlias" : "") ?>;

/**
* <?= $searchModelClass ?> represents the model behind the search form about `<?= $generator->modelClass ?>`.
*/
class <?= $searchModelClass ?> extends <?= isset($searchModelAlias) ? $searchModelAlias : $modelClass . "\n"?>
{

}