<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

echo "<?php\n";
?>
<?php echo $generator->getTimestampComment();?>
require(__DIR__ . DIRECTORY_SEPARATOR . 'base' . DIRECTORY_SEPARATOR . 'create.php');
