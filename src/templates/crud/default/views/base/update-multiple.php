<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\templates\crud\Generator $generator
 */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>
<?php echo $generator->getTimestampComment();?>
use yii\helpers\Html;
use Yii;
/**
 * @var yii\web\View $this
 * @var <?= ltrim($generator->modelClass, '\\') ?> $model
 * @var array $pk
 * @var array $show
 */

$this->title = '<?= Inflector::camel2words(
     StringHelper::basename($generator->modelClass)
) ?> ' . $model-><?= $generator->getNameAttribute() ?> . ', ' . Yii::t('<?= $generator->messageCategorySystem ?>', 'Edit Multiple');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('<?= $generator->messageCategory ?>','<?= Inflector::pluralize(
    Inflector::camel2words(StringHelper::basename($generator->modelClass))
) ?>'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('<?= $generator->messageCategorySystem ?>', 'Edit Multiple');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body <?= Inflector::camel2id(
            StringHelper::basename($generator->modelClass),
            '-',
            true
        )
        ?>-update">

        <h1>
            <?= "<?= \Yii::t('" .$generator->messageCategory ."','".Inflector::camel2words(StringHelper::basename($generator->modelClass))."');".
            ' ?>' .
            "\n"
            ?>
        </h1>

        <div class="crud-navigation">
        </div>

        <?= '<?php ' ?>echo $this->render('_form', [
            'model' => $model,
            'pk' => $pk,
            'show' => $show,
            'multiple' => true,
            'useModal' => $useModal,
            'action' => $action,
        ]); ?>

    </div>
</div>