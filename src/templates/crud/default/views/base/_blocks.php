<?php 
use yii\helpers\Inflector;
use taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider;
$model = new $generator->modelClass();
$model->setScenario('crud');
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->getTableSchema()->columnNames;
}
$hiddenAttributes = RelationProvider::getHiddenAttributesList();
// Remove _by and _at attributes from generating;
$safeAttributes = array_diff($safeAttributes, $hiddenAttributes);
$urlParams = $generator->generateUrlParams();

// Does the model have a predefined list of fields to show?
$uiFieldList = $model->displayFieldList('view');
if (! empty($uiFieldList)) {
    $safeAttributes = $uiFieldList;
}
?>
<?= "<?php\n"?> 
use yii\helpers\Html;
use yii\widgets\DetailView;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use yii\helpers\Url;
use yii\helpers\Inflector;
if(!isset($relatedBlock)){
    $relatedBlock = '<?= $generator->modelClass?>';
}
$blockFound = false;
?>
<?= '<?php'?> if($relatedBlock=='<?= $generator->modelClass?>'){
    $blockFound = true;
?>
 <?php
        echo "<?php \$this->beginBlock('{$generator->modelClass}'); ?>\n";
?>

        <?= $generator->partialView('detail_prepend', $model); ?>
<?= '<?php ' ?>$viewColumns = [
        <?php
        foreach ($safeAttributes as $attribute) {
            if ($generator->checkIfCanvasMeta($model->getTableSchema()->columns[$attribute])) {
                continue;
            }
            if ($generator->checkIfFileMeta($model->getTableSchema()->columns[$attribute])) {
                continue;
            }
            $format = $generator->attributeFormat($attribute);
            if (! $format) {
                continue;
            } else {
                echo $format . ",\n";
            }
        }
        ?>
        ];

        // view columns overwrite
        $viewColumns = Yii::$app->controller->crudColumnsOverwrite($viewColumns, 'view');

        echo DetailView::widget([
        'model' => $model,
        'attributes' => $viewColumns
        ]); ?>

        <?= $generator->partialView('detail_append', $model); ?>

        <hr />

        <?= '<?= ' ?> ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::deleteButton($model, "<?=$generator->messageCategorySystem;?>", [<?=$urlParams?>], 'view')?>

        <?= "<?php \$this->endBlock(); ?>\n\n"; ?>
		<?= "<?php } ?>\n"; ?>
        <?php
        $items = <<<EOS
[
                        'label' => '<b class=""># ' . \$model->{$model->primaryKey()[0]} . '</b>',
                        'content' => \$this->blocks['{$generator->modelClass}'],
                        'active' => true,
                    ],
EOS;
        
        foreach ($generator->getModelRelations($generator->modelClass, [
            'has_many'
        ]) as $name => $relation) {
            $relationObject = \Yii::createObject($relation->modelClass);
            if ($relation->via!=null || $generator->isTranslationTbl($relationObject->tableName())) {
                continue;
            }
            // Replace name of pivot tables with numbers
            $end = substr($name, - 2, 1);
            $last = substr($name, - 1);
            if (is_numeric($end) && ($last == 's')) {
                $name = substr($name, 0, strlen($name) - 1);
            }
            $modelName = $name;
            if(!method_exists($model,"get".$name)){
                continue;
            }
            // build tab items
            $label = Inflector::camel2words($name);
            
            // Make better label for numerical relations
            if (is_numeric(substr($label, - 1))) {
                $link = Inflector::camel2words(key($relation->link));
                $label = substr($label, 0, strlen($label) - 2) . ' ' . str_ireplace(' Id', null, Inflector::pluralize($link));
                $modelName = substr($name, 0, strlen($name) - 1);
            }
            $showAllRecords = false;
            // render pivot grid
            if ($relation->via !== null && false) {
                $pjaxId = "pjax-{$pivotName}";
                $gridRelation = $pivotRelation;
                $gridName = $pivotName;
            } else {
                $pjaxId = "pjax-{$name}";
                $gridRelation = $relation;
                $gridName = $name;
            }
            $relationGrid = $generator->relationGrid($gridName, $gridRelation, $showAllRecords);
            
            $output = $relationGrid['code'];
            $relationBasePermission = $relationGrid['basePermission'];
            
            echo "<?php if(\$relatedBlock=='$label' && Yii::\$app->user->can('x_{$relationBasePermission}_see') && Yii::\$app->controller->crudRelations('{$name}')){\n \$blockFound = true; ?>";
            echo "\n" . str_repeat(' ', 8) . "<?php \$this->beginBlock('$label'); ?>\n";
            
            if ($relation->via !== null && false) {
                $pivotName = Inflector::pluralize($generator->getModelByTableName($relation->via->from[0]));
                $pivotRelation = $model->{'get' . $pivotName}();
                $pivotPk = key($pivotRelation->link);
                $addButton = str_repeat(' ', 12) . "<?= Html::a(
                '<span class=\"glyphicon glyphicon-link\"></span> ' . " . Yii::t($generator->messageCategorySystem, 'Attach') . " . ' " . Inflector::singularize(Inflector::camel2words($label)) . "',\n" . str_repeat(' ', 16) . "['" . $generator->createRelationRoute($pivotRelation, 'create') . "', '" . Inflector::singularize($pivotName) . "' => ['" . key($pivotRelation->link) . "'=>\$model->{$model->primaryKey()[0]}]],
                ['class'=>'btn btn-info btn-xs']
            ) ?>\n";
            } else {
                $addButton = '';
            }
            
            // relation list, add, create buttons
            echo str_repeat(' ', 8) . "<?php if (\$useModal !== true) : ?>\n";
            ?>
            <?php if($addButton){?>
        <div style='position: relative'>
			<div style='position: absolute; right: 0px; top: 0px;'>
<?php
                // TODO: support multiple PKs
                ?>
<?= $addButton ?>
            </div>
		</div>
        <?php }?>
        <?= '<?php endif; ?>' . "\n" ?>
        <div class="clearfix"></div>
<?php
            // render relation grid
            if (! empty($output)) :
                ?>
        <div>
        <?= '<?php ' . $output . "?>\n" ?>
        </div>
        <?php
endif;
            
            echo str_repeat(' ', 8) . "<?php \$this->endBlock() ?>\n\n";
            echo '<?php }?>';
            $relModelClass = $relation->modelClass;
            $relRoute = $generator->getForeignController($relation->modelClass);
            $segments = array_values(array_filter(explode('/',$relRoute)));
            $relModuleName = '';
            $relPK = $relModelClass::primaryKey()[0];
            $relTblName = $relModelClass::tableName();
            $groupBySt= '';
            if($relPK){
                $groupBySt = "->groupBy('{$relTblName}.{$relPK}')";
            }
            if(count($segments)>=2){
                $relModuleName = $segments[0];
            }
            $items .= "\n";
            if($relModuleName){
                $items .= <<<EOS
        (\Yii::\$app->hasModule('{$relModuleName}'))?
EOS;
            }
            $msgCat = $generator->isPartOfBP($relation->modelClass)?$generator->messageCategorySystem:$generator->messageCategory;
        }
        echo "\n<?php if(\$relatedBlock=='History'){?>\n";
        echo "<?php \$this->beginBlock('History'); ?>";
        echo "<?php echo ClassDispenser::getMappedClass(\\taktwerk\\yiiboilerplate\\widget\\HistoryTab::class)::widget(['model' => \$model]);?>";
        echo "<?php \$this->endBlock() ?>";
        echo "<?php }?>\n";
        ?>
<?= "<?php"?> /* MAPPED MODELS RELATIONS - START*/?>
<?= "<?php"?> 
if($blockFound == false){
foreach(\Yii::$app->modelNewRelationMapper->newRelationsMapping as $modelClass=>$relations){
    if(is_a($model,$modelClass)){
        foreach($relations as $relName => $relation){
            $relationKey = array_key_first($relation[1]);
            $foreignKey= $relation[1][$relationKey];
            $relClass = $relation[0];
            $baseName = \yii\helpers\StringHelper::basename($relClass);
            $cntrler = Inflector::camel2id($baseName, '-', true);
            $pluralName = Inflector::camel2words(Inflector::pluralize($baseName));
            $singularName = \Yii::t('<?= $generator->messageCategory?>',Inflector::camel2words(Inflector::singularize($baseName)));
            $basePerm =  \Yii::$app->controller->module->id.'_'.$cntrler;
            if($relatedBlock != $pluralName){
                continue;
            }
            $blockFound = true;
            $this->beginBlock($pluralName);
?>
<?= "<?php"?> 
$relatedModelObject = Yii::createObject($relClass::className());
$relationController = $relatedModelObject->getControllerInstance();
//TODO CrudHelper::getColumnFormat('\app\modules\guide\models\Guide', 'id');
?>
                    <?= "<?php"?> if ($useModal !== true) : ?>
                    <?= "<?php"?> endif; ?>
       	<div class="clearfix"></div>
        <div>
        <?= "<?php"?> $relMapCols = [[
                    'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can($basePerm.'_view') ? '{view} ' : '') . (Yii::$app->getUser()->can($basePerm.'_update') ? '{update} ' : '') . (Yii::$app->getUser()->can($basePerm.'_delete') ? '{delete} ' : ''),
            'urlCreator' => function ($action, $relation_model, $key, $index) use($model,$relationKey,$foreignKey,$baseName,$relatedBaseUrl) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = $relatedBaseUrl . $action;
                        
                        $params[$baseName] = [
                            $relationKey => $model->{$foreignKey}
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) { 
                             return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => $cntrler
                ],
                
        ];

        $relMapCols =array_merge($relMapCols,array_slice(array_keys($relatedModelObject->attributes),0,5));
        
        $relMapCols = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($relMapCols, 'tab'):$relMapCols;
        
        echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can($basePerm.'_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'Add {modelName}',['modelName' => $singularName]),
                [
                    '/'.$relationController->module->id.'/'.$cntrler.'/create',
                    $baseName => [
                        $relationKey => $model->{$foreignKey}
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[
                'class'=>'grid-outer-div'
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->{'get'.$relName}(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-'.$relName,
                    ]
                ]
                ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            'columns' =>$relMapCols
        ])?>
        </div>
                <?= "<?php"?> $this->endBlock();
?>

<?= "<?php"?> }
    }
}
?>
<?= "<?php"?> }?>               
<?= "<?php"?> /* MAPPED MODELS RELATIONS - END*/ ?>
<?= "<?php"?> 
echo $this->blocks[$relatedBlock];
?>