<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\templates\crud\Generator $generator
 */

echo "<?php\n";
?>
<?php echo $generator->getTimestampComment();?>
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var <?= ltrim($generator->searchModelClass, '\\') ?> $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass), '-', true) ?>-search">

    <?= '<?php ' ?>$form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php
    $count = 0;
    foreach ($generator->getTableSchema()->getColumnNames() as $attribute) {
        if (++$count < 6) {
            echo str_repeat(' ', 8) . "<?= " . $generator->generateActiveSearchField($attribute) . " ?>\n\n";
        } else {
            echo str_repeat(' ', 8) . "<?php // echo " . $generator->generateActiveSearchField($attribute) . " ?>\n\n";
        }
    }
    ?>
    <div class="form-group">
        <?= '<?= ' ?>Html::submitButton(Yii::t('<?= $generator->messageCategorySystem ?>', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= '<?= ' ?>Html::resetButton(Yii::t('<?= $generator->messageCategorySystem ?>', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?= '<?php ' ?>ActiveForm::end(); ?>

</div>
