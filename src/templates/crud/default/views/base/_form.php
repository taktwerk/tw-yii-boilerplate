<?php

use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;
use taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider;
use taktwerk\yiiboilerplate\components\Helper;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * @var yii\web\View $this
 * @var \taktwerk\yiiboilerplate\templates\crud\Generator $generator
 */

/** @var \yii\db\ActiveRecord $model */
$model = new $generator->modelClass();

// Does the model have a predefined list of fields to show?
$uiFieldList = $model->displayFieldList('form');
if (!empty($uiFieldList)) {
    $safeAttributes = $uiFieldList;
}
// If not check for specific view, general crud or plain database scenario (for field order)
else {
    $model->setScenario('form');
    $safeAttributes = $model->safeAttributes();
    if (empty($safeAttributes)) {
        $model->setScenario('crud');
        $safeAttributes = $model->safeAttributes();
        if (empty($safeAttributes)) {
            $safeAttributes = $model->getTableSchema()->columnNames;
        }
    }
}

$urlParams = $generator->generateUrlParams();


echo "<?php\n";
?>
<?php echo $generator->getTimestampComment();?>
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\widget\form\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\components\ClassDispenser;
/**
 * @var yii\web\View $this
 * @var <?= ltrim($generator->modelClass, '\\') ?> $model
 * @var taktwerk\yiiboilerplate\widget\form\ActiveForm $form
 * @var boolean $useModal
 * @var boolean $multiple
 * @var array $pk
 * @var array $show
 * @var string $action
 * @var string $owner
 * @var array $languageCodes
 * @var boolean $relatedForm to know if this view is called from ajax related-form action. Since this is passing from
 * select2 (owner) it is always inherit from main form
 * @var string $relatedType type of related form, like above always passing from main form
 * @var string $owner string that representing id of select2 from which related-form action been called. Since we in
 * each new form appending "_related" for next opened form, it is always unique. In main form it is always ID without
 * anything appended
 */

$owner = $relatedForm ? '_' . $owner . '_related' : '';
$relatedTypeForm = Yii::$app->request->get('relatedType')?:$relatedTypeForm;

if (!isset($multiple)) {
$multiple = false;
}

if (!isset($relatedForm)) {
$relatedForm = false;
}

$tableHint = (!empty(<?= $generator->pathPrefix . $generator->modelClass ?>::tableHint()) ? '<div class="table-hint">' . <?= $generator->pathPrefix . $generator->modelClass ?>::tableHint() . '</div><hr />' : '<br />');

<?php if($generator->isClientField()): ?>
if (!<?=$generator->checkAuthorityPermission()?>) {
    $clients = [];
    $clientUsers = \taktwerk\yiiboilerplate\modules\customer\models\ClientUser::findAll(['user_id' => Yii::$app->getUser()->id]);
    foreach ($clientUsers as $client) {
        $model->client_id = $client->client_id;
        continue;
    }
    $show['client_id'] = false;
}
<?php endif;?>
?>
<div class="<?= \yii\helpers\Inflector::camel2id(
    StringHelper::basename($generator->modelClass),
    '-',
    true
) ?>-form">
    <?php
    $multipart = false;
    // Check for uploaded fields to know which form enctype to generate
    if (!empty($generator->getUploadFields())) {
        $multipart = true;
    }
    // Check in related translation models
    if (method_exists(ltrim($generator->modelClass, '\\'), 'getLanguages')) {
        $translationModelName = '\\' . ltrim(get_class($model), '\\') . 'Translation';
        if (class_exists($translationModelName) && !empty($generator->getUploadFields($translationModelName))) {
            $multipart = true;
        }
    }
    ?>
    <?= '<?php ' ?> $formId = '<?= $model->formName() ?>' . ($ajax || $useModal ? '_ajax_' . $owner : ''); <?= '?>' ?>
    
    <?= '<?php ' ?>$form = ActiveForm::begin([
        'id' => $formId,
        'layout' => '<?= !$generator->twoColumnsForm ? $generator->formLayout : 'default' ?>',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'action' => $useModal ? $action : '',
        'options' => [
        'name' => '<?= $model->formName() ?>',
    <?php echo ($multipart === true) ?
        "'enctype' => 'multipart/form-data'
        \n" : ''?>
        ],
    ]); ?>

    <div class="">
        <?php echo "<?php \$this->beginBlock('main'); ?>\n"; ?>
        <?php echo "<?php echo \$tableHint; ?>\n"; ?>

        <?="<?php if (\$multiple) : ?>\n"?>
            <?="<?=Html::hiddenInput('update-multiple', true)?>\n"?>
            <?="<?php foreach (\$pk as \$id) :?>\n"?>
                <?="<?=Html::hiddenInput('pk[]', \$id)?>\n"?>
            <?="<?php endforeach;?>\n"?>
        <?="<?php endif;?>\n"?>

        <?="<?php \$fieldColumns = ["; ?>

                <?php foreach ($safeAttributes as $attribute) {
    $column = ArrayHelper::getValue($generator->getTableSchema()->columns, $attribute);
    $hiddenAttributes = RelationProvider::getHiddenAttributesList();
    array_push($hiddenAttributes, 'id');
    if (in_array(strtolower($attribute), $hiddenAttributes)) {
        continue;
    }
    if ($column === null) {
        continue;
    }
    
    // Find foreign-key columns for dropdown-Fields
    // check if column is no primarykey and column->name ends with "_id"
    $isForeignColumn = ((($temp = strlen($column->name) - strlen('_id')) >= 0 &&
    strpos($column->name, '_id', $temp) !== false) || ClassDispenser::getMappedClass(Helper::class)::arrayKeyExists($generator->getTableSchema()->foreignKeys,$column->name)
        );

    if ($isForeignColumn) {
        $field = $generator->activeFieldDropDown($column, $model);
    }
    else
        $field = $generator->activeField($attribute, $model);

    $prepend = $generator->prependActiveField($attribute, $model);
    $append = $generator->appendActiveField($attribute, $model);

    $is_client_field = $generator->isClientField($generator->modelClass, $attribute);
    if($field) {
        echo "\n" . str_repeat(" ", 12) . "'$attribute' => ";

        echo $is_client_field?$generator->checkAuthorityPermission()."?":"";

        if ($prepend) {
            echo "\n" . str_repeat(" ", 12) . $prepend;
        }
        if ($field) {
            echo "\n" . str_repeat(" ", 12) . $field;
            /*echo $generator->twoColumnsForm ? "\n" . str_repeat(" ", 8) . "<div class=\"col-md-6\">" : '';
            echo "\n" . str_repeat(" ", 12) .
                "<?php if (!\$multiple || (\$multiple && isset(\$show['$attribute']))) :?>";
            echo "\n" . str_repeat(" ", 12) . "<?= " . $field . "\n" . str_repeat(" ", 12) . "?>";
             echo "\n" . str_repeat(" ", 12) . "<?php endif; ?>";*/
            //echo $generator->twoColumnsForm ? "\n" . str_repeat(" ", 8) . "</div>" : '';
        }
        if ($append) {
            echo "\n" . str_repeat(" ", 12) . $append;
        }

        echo $is_client_field?":\n".rtrim($generator->generateActiveField(
                $attribute,
                $name = null,
                $hidden = true,
                $id = null,
                $translation = false,
                $indentMore = 0
            ),";"):"";
        echo ",";
    }

}
echo "]; ?>";

?>
        <div class='clearfix'></div>
<?php echo $generator->twoColumnsForm ? "<?php \$twoColumnsForm = true; ?>" : "<?php \$twoColumnsForm = false; ?>" ?>

<?php
$fileUpload = false;
$uploadedFields = $generator->getUploadFields();

if (!empty($uploadedFields)) {
    $fileUpload = true;
}
?>
<?="<?php "; ?>

// form fields overwrite
$fieldColumns = Yii::$app->controller->crudColumnsOverwrite($fieldColumns, 'form', $model, $form, $owner);

foreach($fieldColumns as $attribute => $fieldColumn) {
    <?php
    if($fileUpload){
     ?>
         if($model->checkIfHidden($attribute))
            {
                echo $fieldColumn;
            }
            else if(!$model->isNewRecord || !$model->checkIfCanvas($attribute))
            {
                if (!$multiple || ($multiple && isset($show[$attribute]))) {
                    if ((isset($show[$attribute]) && $show[$attribute]) || !isset($show[$attribute])) {
                        echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
                        echo $fieldColumn;
                        echo $twoColumnsForm ? '</div>' : '';
                    }else{
                        echo $fieldColumn;
                    }
                }
            }
        <?php }else{ ?>
        if (count($model->primaryKey())>1 && in_array($attribute, $model->primaryKey()) && $model->checkIfHidden($attribute)) {
            echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
            echo $fieldColumn;
            echo $twoColumnsForm ? '</div>' : '';
        } elseif ($model->checkIfHidden($attribute) || (strpos($fieldColumn, 'input type="hidden"')!== false && strpos($fieldColumn, '<div') === false)) {
            echo $fieldColumn;
        } elseif (!$multiple || ($multiple && isset($show[$attribute]))) {
            if ((isset($show[$attribute]) && $show[$attribute]) || !isset($show[$attribute])) {
                echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
                echo $fieldColumn;
                echo $twoColumnsForm ? '</div>' : '';
            } else {
                echo $fieldColumn;
            }
        }
        <?php } ?>
        
<?php echo "} ?>"; ?>
<div class='clearfix'></div>

        <?php
// translations
// ============
if (method_exists(ltrim($generator->modelClass, '\\'), 'getLanguages') &&
    $translationModelName = '\\' . ltrim(get_class($model), '\\') . 'Translation' &&
    class_exists($translationModelName)
) { ?>
    <?php echo "\n<?php" ?> if (isset($translations)) {
    <?php
    $translationModelName = '\\' . ltrim(get_class($model), '\\') . 'Translation';
    $translationModel = new $translationModelName();
    $translationAttributes = $translationModel->attributes;
    $mainClass = $generator->modelClass;
    $modelName = get_class($translationModel);
    $reflect = new ReflectionClass($translationModel);
    $modelName = $reflect->getShortName();
    $reflect = new ReflectionClass($mainClass);
    $mainModelName = strtolower(Inflector::slug(Inflector::camel2words($reflect->getShortName()), '_'));
    $generator->modelClass = ltrim($translationModelName, '\\');
    ?>
    foreach ($translations as $key => $translation) {
        echo '<div class="col-md-6">';
        echo '<filedset><legend style="background-color: #fafafa">' .
            $languageCodes[$key] .
            ' ' .
            Yii::t('<?= $generator->messageCategorySystem?>', 'Translation') .
            '</legend></filedset>';
    ?>
    <?php
    foreach ($translationAttributes as $attribute => $value) {
        if (in_array(
            strtolower($attribute),
            RelationProvider::getHiddenAttributesList()
        )) {
            continue;
        }
        $hidden = false;
        $name = $modelName . '[$key][' . $modelName . '][' . $attribute . ']';
        $translationColumn = $generator->getColumnByAttribute($attribute,$translationModel);
        if($translationColumn && $generator->checkIfMultiUploaded($translationColumn)){
            $name = $name.'[]';
        }
        if (!substr_compare($attribute, 'id', -2) || !substr_compare($attribute, '_id', -3)) {
            $hidden = true;
        }
        $field = $generator->generateActiveField($attribute, $name, $hidden, true, true, 2);
//    $field = $generator->generateActiveField($attribute, $name, $hidden, null);

        if ($field) {
            if ($attribute === $mainModelName . '_id') {
                echo "\n" . str_repeat(' ', 20) . "<?php\n"; ?>
                if (!$model->isNewRecord) { ?><?php
            }
            echo "\n" . str_repeat(' ', 20) . "<?= " . $field . ' ?>';
            if ($attribute === $mainModelName . '_id') {
                echo "\n" . str_repeat(' ', 20) . "<?php
        }
        ?>";
            }
        }
    }

    echo "\n<?php\n"; ?>
        echo '</div>';
        }
    } ?>
<?php $generator->modelClass = $mainClass;
}
?>
        <?php echo '<?php $this->endBlock(); ?>'; ?>

        <?php
        $label = substr(strrchr(get_class($model), '\\'), 1);
        $items = "[
                    'label' => Yii::t('".$generator->messageCategory."', '" . Inflector::camel2words($label) . "'),
                    'content' => \$this->blocks['main'],
                    'active' => true,
                ],";?>

        <?=
        "<?= (\$relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_TAB) ?
            Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    $items
                ]
            ])
            : \$this->blocks['main']
        ?>";
        ?>
        <?=$generator->twoColumnsForm ? "\n" . str_repeat(" ", 8) . "<div class=\"col-md-12\">\n" : '' ?>
        <hr/>
        <?=$generator->twoColumnsForm ? "\n" . str_repeat(" ", 8) . "</div>\n" : '' ?>
        <?=$generator->twoColumnsForm ? "\n" . str_repeat(" ", 8) . "<div class=\"clearfix\"></div>\n" : '' ?>
        <?= '<?php ' ?>echo $form->errorSummary($model); ?>
        <?= "<?php echo"?> ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::formButtons($model, $useModal, $relatedForm, $multiple, "<?=$generator->messageCategorySystem?>", [<?=$urlParams?>]);?>
        <?= "<?php " ?>ClassDispenser::getMappedClass(ActiveForm::class)::end(); ?>
        <?= "<?="?> ($relatedForm && $relatedType == ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL) ?
        '<div class="clearfix"></div>' :
        ''
        ?>
        <div class="clearfix"></div>
    </div>
</div>

<?= "<?php\n" ?>
if(count($hiddenFieldsName)>0){
    $fName = $model->formName();
    $hideJs='';
    foreach($hiddenFieldsName as $name => $op){
        $pClass = $op['parent_class']?$op['parent_class']:'form-group-block';
        $hideJs .= <<<EOT
$('[name="{$fName}[{$name}]"]').parents('.{$pClass}:first').hide();

EOT;
    }
    if($hideJs){
        $this->registerJs($hideJs,\yii\web\View::POS_END);
    }
}
if ($useModal) {
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::class)::register($this);
}
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
if(!isset($useDependentFieldsJs) || $useDependentFieldsJs==true){
$this->render('@taktwerk-views/_dependent-fields', ['model' => $model,'formId'=>$formId]);
}
?>