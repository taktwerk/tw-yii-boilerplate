<?php
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\db\ActiveQuery;
use taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider;

/**
 *
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\templates\crud\Generator $generator
 */

/** @var \yii\db\ActiveRecord $model */
$model = new $generator->modelClass();
$orderColumnName = $generator->getOrderColumn($model->getTableSchema());
$orderColumnCmnt = $generator->getOrderColumnComment($model->getTableSchema());

$useSortGrid = $orderColumnName != false && (($orderColumnCmnt == '') || property_exists($orderColumnCmnt, 'sortRestrictAttributes') == false || count($orderColumnCmnt->sortRestrictAttributes) < 1);

// Does the model have a predefined list of fields to show?
$uiFieldList = $model->displayFieldList('index');
if (! empty($uiFieldList)) {
    $safeAttributes = $uiFieldList;
} // If not check for specific view, general crud or plain database scenario (for field order)
else {
    $model->setScenario('index');
    $safeAttributes = $model->safeAttributes();
    if (empty($safeAttributes)) {
        $model->setScenario('crud');
        $safeAttributes = $model->safeAttributes();
        if (empty($safeAttributes)) {
            $safeAttributes = $model->getTableSchema()->columnNames;
        }
    }
}

echo "<?php\n";
?>
<?php echo $generator->getTimestampComment();?>
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use taktwerk\yiiboilerplate\grid\GridView;
use taktwerk\yiiboilerplate\components\ClassDispenser;
<?php

if ($useSortGrid) {
    ?>use taktwerk\yiiboilerplate\grid\SortableGridView;
<?php }?>
use taktwerk\yiiboilerplate\widget\export\ExportMenu;
use taktwerk\yiiboilerplate\helpers\DropdownItemHelper;
// use <?= $generator->indexWidgetType === 'grid' ? 'yii\\grid\\GridView' : 'yii\\widgets\\ListView' ?>;
use yii\web\View;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var boolean $useModal
 * @var boolean $importer
<?php if ($generator->searchModelClass !== '') : ?>
 * @var <?= ltrim($generator->searchModelClass, '\\') ?> $searchModel
<?php endif; ?>
 */

$this->title = Yii::t('<?= $generator->messageCategory?>', '<?=Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))?>');
$this->params['breadcrumbs'][] = $this->title;
$basePermission = Yii::$app->controller->module->id . '_' . Yii::$app->controller->id;
Yii::$app->controller->renderCustomBlocks(Yii::$app->controller::POS_MAIN,'index');
<?php
// if translation class
if (method_exists($generator->modelClass, 'getLanguages')) :
    ?>
$translationLabels = Yii::$app->controller->translationLabels;
<?php endif; ?>

$tableHint = (!empty(<?= $generator->pathPrefix . $generator->modelClass ?>::tableHint()) ? '
<hr />
<div class="table-hint">' . <?= $generator->pathPrefix . $generator->modelClass ?>::tableHint() . '</div>
' : '');

/* ------- Multiple-Delete Batch Action ------ */
$inlineScript = 'var gridViewKey = "<?=Inflector::camel2id(StringHelper::basename($generator->modelClass), '-', true)?>", useModal = ' . ($useModal ? 'true' : 'false') . ';';
$this->registerJs($inlineScript, View::POS_HEAD, 'my-inline-js');

$dataProviderQuery = clone $dataProvider->query;
\Yii::$app->get('userUi')->set('GridQuery', get_class($searchModel), $dataProviderQuery, \Yii::$app->user->id);
$gridDropdownItems = array_filter(\yii\helpers\ArrayHelper::merge([
	'select_checkbox'=> [
       'url' =>'javascript:',
       'options' => ['onClick'=>'event.stopPropagation()'],
       'label' =>'<input type="hidden" value=""> <label class="fa" title="'.
                \Yii::t('twpb','Runs the below actions on all records')
                 .'"><input type="checkbox" name="action_on_all_rec" value="1"> Run on all</label>'
   ],
   'delete_multiple'=> (Yii::$app->getUser()->can(
        Yii::$app->controller->module->id . '_' .
        Yii::$app->controller->id . '_delete-multiple'
    ) ?
    [
        'url' =>'javascript:',
        'options' => [
            'onclick' => 'deleteMultiple(this);',
            'data-url' => Url::toRoute('delete-multiple'),
            'data-message'=>\Yii::t('twbp',"Do you really want to delete {count} entries?"),
            'data-all-message'=>\Yii::t('twbp',"Delete entries of all pages?")
        ],
        'label' => '
<i class="fa fa-trash" aria-hidden="true"></i>
' . Yii::t('<?= $generator->messageCategorySystem ?>', 'Delete'), ] : ''),'edit_multiple'=> (Yii::$app->getUser()->can(
Yii::$app->controller->module->id . '_' . Yii::$app->controller->id .
'_update-multiple' ) ? [ 'url' => '#edit-multiple', 'linkOptions' => [
'data-toggle' => 'modal', ], 'label' => '
<i class="fa fa-pencil" aria-hidden="true"></i>
' . Yii::t('<?= $generator->messageCategorySystem ?>', 'Update'), ] : ''),'duplicate_multiple'=> (Yii::$app->getUser()->can(
Yii::$app->controller->module->id . '_' . Yii::$app->controller->id .
'_copy-multiple' ) ? [ 'url' =>'javascript:', 'options' => [ 'onclick' =>
'duplicateMultiple(this);', 'data-url' =>
Url::toRoute('duplicate-multiple'),'data-message'=>\Yii::t('twbp',"Do you really want to duplicate {count} entries?") ], 'label' => '
<i class="fa fa-copy" aria-hidden="true"></i>
' . Yii::t('<?= $generator->messageCategorySystem ?>', 'Duplicate'),
     ] : ''),
     'multiple_recompress'=>((method_exists($dataProvider->query->modelClass,'getUploadFields') && Yii::$app->getUser()->can(
        Yii::$app->controller->module->id . '_' .
        Yii::$app->controller->id . '_recompress-multiple'
    )) ?
    [
        'url' =>'javascript:',
        'options' => [
            'onclick' => 'recompressMultiple(this);',
            'data-url' => Url::toRoute('recompress-multiple'),
            'data-message'=>\Yii::t('twbp',"Do you really want to recompress {count} entries?")
        ],
        'label' => '
<i class="glyphicon glyphicon-compressed" aria-hidden="true"></i>
' . Yii::t('<?= $generator->messageCategorySystem ?>', 'Recompress'), ] : '')
], Yii::$app->controller->gridDropdownActions));

$gridColumns = [
<?php
$actionButtonColumn = '';
$orderColumn = '';
if ($orderColumnName && $useSortGrid) {
    $orderColumn = <<<PHP
[
        'headerOptions'=>['style'=>'width:60px'],
        'contentOptions'=>['align'=>'center'],
        'content'=>function(\$model){
                \$spn = Html::tag('span','',['class'=>"glyphicon glyphicon-resize-vertical"]);
                \$tspn = Html::tag('span',\$model->{$orderColumnName},['class'=>'label']);
                return Html::a(\$spn.' '.\$tspn,'javascript:void(0)',['class'=>"sortable-row btn btn-default",'title'=>\Yii::t('{$generator->messageCategorySystem}','Drag to Re-order')]);
        }
],\n
PHP;
}

$actionButtonColumn .= <<<PHP
    [
        'class' => '{$generator->actionButtonClass}',
    ],\n
{$orderColumn}
PHP;
$pjax = Inflector::camel2id(StringHelper::basename($generator->modelClass), '-', true);
// action dropdown buttons first
?>
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'headerOptions' => [
            'class' => 'kartik-sheet-style'
        ],
        'vAlign'=>GridView::ALIGN_TOP,
        'visible' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\DropdownItemHelper::class)::hasVisibleItems($gridDropdownItems)
    ],
<?php
echo $actionButtonColumn;
?>
    [
        'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\RestoreColumn::class),
        'pjaxContainer' => '<?=$pjax?>-pjax-container',
        'model'=> Yii::$app->controller->model
    ],
<?php

$count = 0;
$selectedColumn = "";

foreach ($safeAttributes as $attribute) {
    $format = trim($generator->columnFormat($attribute, $model, 'index'));
    $hiddenAttributes = RelationProvider::getHiddenAttributesList();
    array_push($hiddenAttributes, 'order_number');
    if ($format == false || in_array(strtolower($attribute), $hiddenAttributes)) // TODO: not sure why id is not working
    {
        continue;
    }

    if ($generator->checkIfCanvasMeta($model->getTableSchema()->columns[$attribute])) {
        continue;
    }
    if ($generator->checkIfFileMeta($model->getTableSchema()->columns[$attribute])) {
        continue;
    }
    
    $foreignModel = null;
    $added_attributes = [];
    $fkFormat = null;
    foreach ($generator->getModelRelations($generator->modelClass) as $modelRelation) {
        foreach ($modelRelation->link as $linkKey => $link) {
            if ($attribute == $link && !in_array($attribute, $added_attributes)) {
                $foreignModelClass = $generator->fetchForeignClass($link);
                $foreignModelTblName = $foreignModelClass::tableName();
                $foreignControllerClass = str_replace("models", "controllers", $foreignModelClass);
                $foreignControllerClass .= "Controller";
                $foreignAttribute = $generator->fetchForeignAttribute($attribute, $foreignModelClass);
                $gen = new \taktwerk\yiiboilerplate\templates\crud\Generator();
                $gen->controllerClass = $foreignControllerClass;
                
                // is a relation-column
                $foreignModel = null;
                $fkFormat = str_replace("ID", "", $attribute) . ".toString";
                $reflection = new ReflectionClass($foreignModelClass);
                $foreignPK = $foreignModelClass::primaryKey()[0];
                $groupBySt= '';
                if($foreignPK){
                    $groupBySt = "->groupBy('{$foreignModelTblName}.{$foreignPK}')";
                }
                $shortForeignModel = $reflection->getShortName();
                $foreignObjectName = \yii\helpers\BaseInflector::variablize(str_replace("_id", "", $attribute));
                if ($model->hasMethod('get'.$foreignObjectName) && $model->{'get'.$foreignObjectName}() instanceof ActiveQuery) {
                    $format = false;
$actPerm = "'".($gen->getModuleId()!='app'?$gen->getModuleId().'_':'app_').$gen->getControllerID() . "_view'";
$urlModule = ($gen->getModuleId()!='app'?'/'.$gen->getModuleId():'');
                    $visibility = $gen->checkClientRelation($foreignModelClass, $attribute) ? '
        \'visible\' => ' . $gen->checkAuthorityPermission() . ',
        ' : "";
                    echo str_repeat(' ', 4) . "[
        'class' => '\\kartik\\grid\\DataColumn',
        'attribute' => '$attribute'," . $visibility . "
        'format' => 'html',
        'content' => function (\$model) {
            if (\$model->" . $foreignObjectName . ") {
                if (Yii::\$app->getUser()->can({$actPerm}) && \$model->" . $foreignObjectName . "->readable()) {
                    return Html::a(\$model->" . $foreignObjectName . "->toString, ['" .$urlModule.'/'. $gen->getControllerID() . "/view', '$foreignAttribute' => \$model->$attribute], ['data-pjax' => 0]);
                } else {
                    return \$model->" . $foreignObjectName . "->toString;
                }
            }
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'data' => $foreignModelClass::find(){$groupBySt}->count() > 50 ? null : \\yii\\helpers\\ArrayHelper::map($foreignModelClass::find()->all(), '$foreignAttribute', 'toString'),
            'initValueText' => $foreignModelClass::find(){$groupBySt}->count() > 50 ? \\yii\\helpers\\ArrayHelper::map($foreignModelClass::find()->andWhere(['{$foreignModelTblName}.{$foreignAttribute}' => \$searchModel->{$attribute}])->all(), '$foreignAttribute', 'toString') : '',
            'options' => [
                'placeholder' => '',
                'multiple' => true,
            ],
            'pluginOptions' => [
                'allowClear' => true,
                ($foreignModelClass::find(){$groupBySt}->count() > 50 ? 'minimumInputLength' : '') => 3,
                ($foreignModelClass::find(){$groupBySt}->count() > 50 ? 'ajax' : '') => [
                    'url' => \\yii\\helpers\\Url::to(['list']),
                    'dataType' => 'json',
                    'data' => new \\yii\\web\\JsExpression('function(params) {
                        return {
                            q:params.term, m: \\'$shortForeignModel\\', page:params.page || 1
                        };
                    }')
                ],
            ]
        ],
    ],\n";
                    
                }
                $added_attributes[] = $attribute;
            }
        }
    }
    if ($format == false) {
        continue;
    }
   // if (++ $count < $generator->gridMaxColumns) {
        $selectedColumn .= ($count - 1) . ", ";
        echo str_repeat(' ', 4) . "{$format},\n";
   // } else {
   //     echo str_repeat(' ', 4) . "/*{$format}*/\n";
   // }
}
$selectedColumn = rtrim($selectedColumn, ', ');
?>
    ];

// grid columns overwrite
$gridColumns = Yii::$app->controller->crudColumnsOverwrite($gridColumns, 'index');
$gridColumns = \yii\helpers\ArrayHelper::merge($gridColumns, $searchModel->getCustomFilters($searchModel, '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass), '-', true) ?>-pjax-container'));

// export widget
$exportWidget = false;
if(
    Yii::$app->user->can('app_export') ||
    Yii::$app->user->can('app_export-pdf') ||
    Yii::$app->user->can('app_export-xlsx') ||
    Yii::$app->user->can($basePermission . '_export-xlsx') ||
    Yii::$app->user->can($basePermission . '_export-pdf') ||
    Yii::$app->user->can($basePermission . '_export-csv') ||
    Yii::$app->user->can($basePermission . '_export-xls') ||
    Yii::$app->user->can($basePermission . '_export-html') ||
    Yii::$app->user->can($basePermission . '_export-text')
) {
    // overwrite export columns
    $exportColumns = Yii::$app->controller->exportColumnsOverwrite($gridColumns);

    if (!isset($exportColumns)) {
        $exportColumns = $gridColumns;
        foreach ($exportColumns as $column => $value) {
            // Remove checkbox and action columns from Excel export
            if (isset($value['class']) &&
                (strpos($value['class'], 'CheckboxColumn') !== false || strpos($value['class'], 'ActionColumn') !== false)
            ) {
                unset($exportColumns[$column]);
            }
        }
    }

    $exportWidget = ExportMenu::widget([
        'filename' => Yii::$app->controller->exportFilename,
        'asDropdown' => true,
        'basePermission' => $basePermission,
        'dataProvider' => $dataProvider,
        'showColumnSelector' => false,
        'columns' => $exportColumns,
        'fontAwesome' => true,
        'exportContainer' => [
            'class' => 'btn-group mr-2'
        ],
        'dropdownOptions' => [
            'label' => 'Export',
        ]
    ]);
}

// action buttons
$gridButtonActions = '';
foreach (Yii::$app->controller->gridButtonActions as $buttonCode => $buttonAction) {
    if ($buttonAction['visible']) {
    	$btnOptions = isset($buttonAction['options'])?$buttonAction['options']:[];
        $gridButtonActions .= ' ' . Html::a(
            $buttonAction['label'],
            $buttonAction['url'],
            array_merge([
                'class' => $buttonAction['class'],
                'data-pjax' => false,
                'data-toggle' => 'modal',
                'data-url' => $buttonAction['url']
            ],$btnOptions)
        );
    }
}
?>

<?php

echo "<?php \$this->beginBlock('info');
\yii\bootstrap\Modal::begin([
    'header' => '<h2>' . Yii::t('".$generator->messageCategorySystem."', 'Information') . '</h2>',
    'toggleButton' => [
        'tag' => 'btn',
        'label' => '?',
        'class' => 'btn btn-default',
        'title' => Yii::t('".$generator->messageCategorySystem."', 'Help')
],
]);?>"?>
<?php echo "<?= \$this->render('@taktwerk-boilerplate/views/_info_modal') ?>" ?>
<?php

echo "<?php \\yii\\bootstrap\\Modal::end();
\$this->endBlock(); ?>"?>

<div class="box box-default">
	<div
		class="giiant-crud box-body <?=Inflector::camel2id(StringHelper::basename($generator->modelClass), '-', true)?>-index">
<?= str_repeat(' ', 8) . "<?php\n" . str_repeat(' ', 8)  . ($generator->indexWidgetType === 'grid' ? '// ' : '') ?>
<?php if ($generator->searchModelClass !== '') : ?>
echo $this->render('_search', ['model' =>$searchModel]);
<?php endif; ?>
        ?>
            <?= '<?= ' ?>ClassDispenser::getMappedClass(<?php if($orderColumnName==false || $useSortGrid==false){?>GridView<?php }else{?>SortableGridView<?php }?>::class)::widget([
                'dataProvider' => $dataProvider,
<?php if($orderColumnName && $useSortGrid){?>
				'sortUrl' => Url::toRoute(['sort-item']), 
				'sortingPromptText' => 'Loading...', 
				'failText' => 'Fail to sort', 
<?php }?>
                'filterModel' => $searchModel,
                'filterSelector' => 'select[name="pageSize"]',
                'filterUrl' => Url::current(array_merge(\Yii::$app->request->queryParams,[$dataProvider->pagination->pageParam=>1])),
                'options' => [
                    'id' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass), '-', true) ?>-grid'
                ],
                'columns' => $gridColumns,
                'containerOptions' => [
                    'style' => 'overflow: auto'
                ], /* only set when $responsive = false */
                'headerRowOptions' => [
                    'class' => 'kartik-sheet-style'
                ],
                'filterRowOptions' => [
                    'class' => 'kartik-sheet-style'
                ],
                'toolbar' => [
                    [ 'content' =>
                        $this->blocks['info'].
                        ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\FilterWidget::class)::widget(['model' => $searchModel, 'pjaxContainer' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass), '-', true) ?>-pjax-container','modelClass'=><?= $generator->pathPrefix . $generator->modelClass ?>::class]) .

                        Html::a(
                            '<i class="glyphicon glyphicon-repeat"></i>',
                            ['index', 'reset' => 1],
                            ['class' => 'btn btn-default reset-form-icon', 'title' => Yii::t('<?= $generator->messageCategorySystem ?>', 'Reset Search')]
                        )
                    ],
                    [
                        'content' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\PageSize::class)::widget([
                        'pjaxContainerId' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass), '-', true) ?>-pjax-container',
                        'model' => $searchModel,
                        ])
                    ],
                    $exportWidget,
                    ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\GridOptionDropdownWidget::class)::widget([
                        'dataProvider' => $dataProvider,
                        'importer' => $importer,
                        'pjaxContainerId' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass), '-', true) ?>-pjax-container',
                        'model'=> Yii::$app->controller->model
                    ]),
                    [
                        'content' => \yii\bootstrap\ButtonDropdown::widget([
                            'id' => 'tw-actions',
                            'encodeLabel' => false,
                            'label' => '<span
			class="glyphicon glyphicon-flash"></span> ' . Yii::t('<?= $generator->messageCategorySystem ?>',
		'Selected'), 'dropdown' => [ 'options' => [ 'class' =>
		'dropdown-menu-right' ], 'encodeLabels' => false, 'items' =>
		$gridDropdownItems ], 'options' => [ 'class' => 'btn-default', 'style'
		=> (ClassDispenser::getMappedClass(DropdownItemHelper::class)::hasVisibleItems($gridDropdownItems) ? null :
		'display: none') ] ]) ], ], 'panel' => [ 'heading' => "
		<h3 class=\"panel-title\">
			<i class=\"glyphicon glyphicon-list\"></i>  " .\Yii::t('<?= $generator->messageCategory ?>','<?=
                            Inflector::pluralize(Inflector::camel2words(
                                StringHelper::basename($generator->modelClass)))?>') .
                        "</h3>
		" .
		ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\import\widget\ImportResult::class)::widget(),
		'type' => 'default', 'before' => (Yii::$app->getUser()->can(
		Yii::$app->controller->module->id . '_' . Yii::$app->controller->id .
		'_create' ) ? Html::a( '<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('<?= $generator->messageCategorySystem ?>', 'New'),
                            [$useModal ? '#modalForm' : 'create'],
                            [
                                'class' => 'btn btn-success create-new',
                                'data-pjax' => $useModal,
                                'data-toggle' => 'modal',
                                'data-url' => Url::toRoute('create')
                            ]
                        ) : '') . $gridButtonActions . ' ' . (Yii::$app->getUser()->can('Authority') ?
<?php
$items = '';
$model = new $generator->modelClass();
?>
<?php foreach ($generator->getModelRelations($model) as $relation) : ?>
<?php
    // relation dropdown links
    $iconType = ($relation->multiple) ? 'arrow-right' : 'arrow-left';
    if ($generator->isPivotRelation($relation)) {
        $iconType = 'random';
    }
    $controller = $generator->pathPrefix . Inflector::camel2id(StringHelper::basename($relation->modelClass), '-', true);
    $isUser = new ReflectionClass($relation->modelClass);
    if ($isUser->getShortName() == 'User') {
        $route = "/user/admin";
    } else {
        $route = $generator->getForeignController($relation->modelClass).'/index';
    }
    $segments = array_values(array_filter(explode('/',$route)));
    $moduleName = '';
    if(count($segments)>=3){
        $moduleName = $segments[0];
    }
    
    $label = Inflector::titleize(StringHelper::basename($relation->modelClass), true);
    if($moduleName){
     $items .= <<<PP

                                                (\Yii::\$app->hasModule('{$moduleName}'))?
PP;
    }
    $items .= <<<PHP
[
                                                    'url' => ['{$route}'],
                                                    'label' => '<i class="glyphicon glyphicon-arrow-right">&nbsp;</i>'.Yii::t('{$generator->messageCategory}', '{$label}'),
                                                ]
PHP;
    if($moduleName){
        $items .= <<<PP
:''
PP;
        
    }
    $items .= <<<PP
,
PP;
    
    ?>
<?php endforeach; ?>
<?php $items .= "\n";?>
                            \yii\bootstrap\ButtonDropdown::widget([
                                'id' => 'giiant-relations',
                                'encodeLabel' => false,
                                'label' => '<span
			class="glyphicon glyphicon-paperclip"></span> ' .
                                Yii::t('<?= $generator->messageCategorySystem ?>', 'Relations'),
                                'dropdown' => [
                                    'options' => [
                                    ],
                                    'encodeLabels' => false,
                                    'items' => [<?= $items ?>
                                    ]
                                ],
                                'options' => [
                                    'class' => 'btn-default'
                                ]
                            ])
                            : '') ." ". '<button id="toolbar_toggle" class
        ="btn pull-right"><i class="glyphicon glyphicon-menu-down "></i></button>' . $tableHint
                    ,
                    'after' => '{pager}',
                    'footer' => false
                ],
                'striped' => true,
                'pjax' => true,
                'pjaxSettings' => [
                    'options' => [
                        'id' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass), '-', true) ?>-pjax-container',
                    ],
                    'clientOptions' => [
                        'method' => 'POST'
                    ]
                ],
                'hover' => true,
                'pager' => [
                    'class' => yii\widgets\LinkPager::class,
                    'firstPageLabel' => Yii::t('<?= $generator->messageCategorySystem ?>', 'First'),
                    'lastPageLabel' => Yii::t('<?= $generator->messageCategorySystem ?>', 'Last')
                ],
            ])
            ?>
    </div>
</div>
<?= "<?php"?> \yii\bootstrap\Modal::begin([
    'size' => \yii\bootstrap\Modal::SIZE_DEFAULT,
    'header' => '
<h4>' . Yii::t('<?= $generator->messageCategorySystem ?>', 'Choose fields to edit') . ':</h4>
',
    'id' => 'edit-multiple',
    'clientOptions' => [
        'backdrop' => 'static',
    ],
]);
?>
<?= "<?=" ?> Html::beginForm(['update-multiple'], 'POST'); ?>
<?= "<?php" ?> $model = new <?= $generator->modelClass ?>(); ?>
<?php
foreach ($safeAttributes as $attribute) {
    $column = \yii\helpers\ArrayHelper::getValue($generator->getTableSchema()->columns, $attribute);
    if (in_array(strtolower($attribute), RelationProvider::getHiddenAttributesList())) {
        continue;
    }
    $db = Yii::$app->get('db', false);
    $uniqueIndexes = $db->getSchema()->findUniqueIndexes($generator->getTableSchema());
    
    // If is primary key or unique index skip
    if ($column === null || $column->isPrimaryKey || array_key_exists($attribute, $uniqueIndexes)) {
        continue;
    }
    ?>
<div class="form-group">
	<div class="col-sm-6 col-sm-offset-1">
		<div class="checkbox">
			<label for="<?=$attribute?>"> <input type="checkbox"
				id="<?=$attribute?>" name="<?=$attribute?>" value="1"><?= "<?=" ?>$model->getAttributeLabel('<?=$attribute?>') ?>
                </label>
		</div>
	</div>
</div>
<?php } ?>
<div class="clearfix"></div>
<button type="submit" class="btn btn-success" id="submit-multiple">Update</button>
<?= "<?="?> Html::endForm(); ?>

<?="<?php"?> \yii\bootstrap\Modal::end();
?>
<?= "\n<?php\n" ?>
$js = <<<JS
jQuery.fn.addHidden=function (name, value) {
    return
	this.each(function () {
        var input=$("<input>").attr("type", "hidden").attr("name", name).val(value);
        $(this).append($(input));
    });
};
$(document).ready(function () {
    $('#submit-multiple').on('click', function(e){
        e.preventDefault();
        var keys=$("#" + gridViewKey + "-grid").yiiGridView('getSelectedRows'),
            form = $(this).closest('form');
        // Remove old values to prevent duplicated Ids submit
        form.find('input[name="id[]"]').remove();
        form.addHidden('no-post', true);
        $.each(keys , function (key, value) {
            form.addHidden('id[]', value);
        });
        if (useModal) {
            $('body').addClass('kv-grid-loading');
            $.post(
                form.attr("action"),
                form.serialize()
            )
            .done(function (data) {
                $('#modalFormMultiple').modal('show');
                $('#modalFormMultiple').find('.modal-body').html(data);
                $('body').removeClass('kv-grid-loading');
                formSubmitEvent();
                $('.closeMultiple').on('click', function(e){
                    e.preventDefault();
                    $('#modalFormMultiple').modal('hide');
                });
            })
            return false;
        } else {
            form.submit();
        }
    });
});
JS;
$this->registerJs($js);
Yii::$app->controller->renderCustomBlocks(Yii::$app->controller::POS_END,'index');
?>
<?="<?php \\yii\\bootstrap\\Modal::begin([
    'size' => \\yii\\bootstrap\\Modal::SIZE_LARGE,
    'id' => 'modalFormMultiple',
    'clientOptions' => [
        'backdrop' => 'static',
    ],
]);
?>
<?php \\yii\\bootstrap\\Modal::end();
?>
<?php \\yii\\bootstrap\\Modal::begin([
    'size' => \\yii\\bootstrap\\Modal::SIZE_LARGE,
    'id' => 'modalForm',
    'clientOptions' => [
        'backdrop' => 'static',
    ],
]);?>
<?php \\yii\\bootstrap\\Modal::end();?>

<?php \\yii\\bootstrap\\Modal::begin([
    'size' => \\yii\\bootstrap\\Modal::SIZE_LARGE,
    'id' => 'modal-atr-val',
    'clientOptions' => [
        'backdrop' => 'static',
    ],
]);?>
<?php \\yii\\bootstrap\\Modal::end();?>
<?php
\$atrJs = <<<ET
$(document).on('click','.atr-val-a',function(e){
    e.preventDefault();

    var el = $(this);
    var url = el.prop('href');
    if(url){
        $.ajax({
            url:url,
            method:'GET',
            success: function(data){
                var body = $('#modal-atr-val').find('.modal-body');
                if(data.success){
                    body.removeClass('text-danger');
                }else{
                    body.addClass('text-danger');
                }
                body.empty().append(data.value);
                $('#modal-atr-val').modal('show');
            }
        });
    }
});
ET;
\$this->registerJs(\$atrJs);
?>
<?php
if (\$useModal) {
    ClassDispenser::getMappedClass(\\taktwerk\\yiiboilerplate\\modules\\backend\\assets\\ModalFormAsset::class)::register(\$this);
}
ClassDispenser::getMappedClass(\\taktwerk\\yiiboilerplate\\modules\\backend\\assets\\ShortcutsAsset::class)::register(\$this);
?>
"?>