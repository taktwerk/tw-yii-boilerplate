<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\templates\crud\Generator $generator
 */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>
<?php echo $generator->getTimestampComment();?>
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var <?= ltrim($generator->modelClass, '\\') ?> $model
 * @var string $relatedTypeForm
 */

$this->title = Yii::t('<?= $generator->messageCategory;?>', '<?= Inflector::camel2words(
     StringHelper::basename($generator->modelClass)
) ?>') . ' #' . (is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey) . ', ' . Yii::t('<?= $generator->messageCategorySystem;?>', 'Edit') . ' - ' . $model->toString();
if(!$fromRelation) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('<?= $generator->messageCategory;?>', '<?= Inflector::pluralize(
        Inflector::camel2words(StringHelper::basename($generator->modelClass))
    ) ?>'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => (string)is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey, 'url' => ['view', <?= $urlParams ?>]];
    $this->params['breadcrumbs'][] = Yii::t('<?= $generator->messageCategorySystem;?>', 'Edit');
}
?>
<div class="row">
<?= '<?php ' ?> ($fromRelation)?'':require_once '_relatedtabs.php';?>
<div class="col-md-<?= '<?= ' ?> ($fromRelation)?'12':'9' ?>">
<div class="box box-default">
    <div
        class="giiant-crud box-body <?= Inflector::camel2id(
            StringHelper::basename($generator->modelClass),
            '-',
            true
        )
        ?>-update">

        <div class="crud-navigation">

            <?= '<?= ' ?>Html::a(
                Yii::t('<?= $generator->messageCategorySystem;?>', 'Cancel'),
                $fromRelation ? urldecode($fromRelation) : \yii\helpers\Url::previous(\Yii::$app->controller->crudUrlKey),
                ['class' => 'btn btn-default cancel-form-btn']
            ) ?>
            <?= '<?= ' ?>(!$fromRelation ? Html::a(
                '<span class="glyphicon glyphicon-eye-open"></span> ' . Yii::t('<?= $generator->messageCategorySystem;?>', 'View'),
                [
                    'view',
                    <?= $urlParams ?>,
                    'show_deleted'=>Yii::$app->request->get('show_deleted')
                ],
                ['class' => 'btn btn-default']
            ) : '') ?>
            <?= '<?= ' ?> \taktwerk\yiiboilerplate\widget\CustomActionDropdownWidget::widget(['model'=>$model]) ?>
            <div class="pull-right">
                <?= '<?php ' ?>
                \yii\bootstrap\Modal::begin([
                'header' => '<h2>' . Yii::t('<?= $generator->messageCategorySystem;?>', 'Information') . '</h2>',
                'toggleButton' => [
                'tag' => 'btn',
                'label' => '?',
                'class' => 'btn btn-default',
                'title' => Yii::t('<?= $generator->messageCategorySystem;?>', 'Help')
                ],
                ]);?>

                <?= '<?= ' ?>$this->render('@taktwerk-boilerplate/views/_shortcut_info_modal') ?>
                <?= '<?php ' ?> \yii\bootstrap\Modal::end();
                ?>
            </div>
        </div>

        <?= '<?php ' ?>echo $this->render('_form', [
            'model' => $model,
            'inlineForm' => $inlineForm,
            'relatedTypeForm' => $relatedTypeForm,
            'fromRelation' => $fromRelation,
            'hiddenFieldsName' => $hiddenFieldsName,
<?php if (method_exists(ltrim($generator->modelClass, '\\'), 'getLanguages')) { ?>
            'translations' => $translations,
            'languageCodes' => $languageCodes
<?php } ?>
        ]); ?>

    </div>
</div>
</div></div>