<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\templates\crud\Generator $generator
 */

echo "<?php\n";
?>
<?php echo $generator->getTimestampComment();?>
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var <?= ltrim($generator->modelClass, '\\') ?> $model
 * @var string $relatedTypeForm
 */

$this->title = Yii::t('<?= $generator->messageCategory;?>', '<?= Inflector::camel2words(
    StringHelper::basename($generator->modelClass)
) ?>') . ', ' . Yii::t('<?= $generator->messageCategorySystem;?>', 'Create');
if(!$fromRelation) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('<?= $generator->messageCategory;?>', '<?= Inflector::pluralize(
        Inflector::camel2words(StringHelper::basename($generator->modelClass))
    ) ?>'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = Yii::t('<?= $generator->messageCategorySystem;?>', 'Create');
}
?>
<div class="row">
<?= '<?php ' ?> ($fromRelation)?'':require_once '_relatedtabs.php';?>
<div class="col-md-<?= '<?= ' ?> ($fromRelation)?'12':'9' ?>">
<div class="box box-default">
    <div
        class="giiant-crud box-body <?= Inflector::camel2id(
            StringHelper::basename($generator->modelClass),
            '-',
            true
        ) ?>-create">

        <div class="clearfix crud-navigation">
            <div class="pull-left">
                <?= '<?= ' ?>Html::a(
                    Yii::t('<?= $generator->messageCategorySystem;?>', 'Cancel'),
                    $fromRelation ? urldecode($fromRelation) : \yii\helpers\Url::previous(\Yii::$app->controller->crudUrlKey),
                    ['class' => 'btn btn-default cancel-form-btn']
                ) ?>
            </div>
            <div class="pull-right">
                <?= '<?php ' ?>
                \yii\bootstrap\Modal::begin([
                    'header' => '<h2>' . Yii::t('<?= $generator->messageCategorySystem;?>', 'Information') . '</h2>',
                    'toggleButton' => [
                        'tag' => 'btn',
                        'label' => '?',
                        'class' => 'btn btn-default',
                        'title' => Yii::t('<?= $generator->messageCategorySystem;?>', 'Information about possible search operators')
                    ],
                ]);?>

                <?= '<?= ' ?>$this->render('@taktwerk-boilerplate/views/_shortcut_info_modal') ?>
                <?= '<?php ' ?> \yii\bootstrap\Modal::end();
                ?>
            </div>
        </div>

        <?= '<?= ' ?>$this->render('_form', [
            'model' => $model,
            'inlineForm' => $inlineForm,
            'action' => $action,
            'relatedTypeForm' => $relatedTypeForm,
            'fromRelation' => $fromRelation,
            'hiddenFieldsName'=>$hiddenFieldsName,
        <?php if (method_exists(ltrim($generator->modelClass, '\\'), 'getLanguages')) { ?>
            'translations' => $translations,
            'languageCodes' => $languageCodes
<?php } ?>
        ]); ?>

    </div>
</div>
</div>
</div>