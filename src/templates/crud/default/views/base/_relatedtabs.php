<?php 
use yii\helpers\Inflector;
?>
<?= "<?php\n"?>
use yii\helpers\Html;
use Yii;
use yii\bootstrap\Nav;
use kartik\dialog\Dialog;
$tabAction = Yii::$app->request->get('parent_action',isset($tabAction)?$tabAction:'view');
$onClick = $model->isNewRecord?"krajeeDialogRelatedTab.alert('Is available after saving this form');return false;":'';
?>
<div class="col-md-3">
	<div class="box box-solid">
		<div class="box-body no-padding">
<?php
        // get relation info $ prepare add button
        $model = new $generator->modelClass();
        $msgCat = $generator->isPartOfBP($generator->modelClass)?$generator->messageCategorySystem:$generator->messageCategory;
        $bName = \yii\helpers\StringHelper::basename($generator->modelClass);
        $label = Inflector::camel2words($bName);
        $controller = Inflector::camel2id($bName);
        $items = <<<EOS
                    [
                        'label'=>'<i class="fa fa-star"></i>'.\Yii::t('$msgCat','{$label}'),
                        'options' => ['class'=>'bg-info', 'data'=>['new-rec-alert'=>(\$model->isNewRecord)?1:0]],
                        'active' => false,
                        'url' => (\$model->isNewRecord)?'#':['{$controller}/'.\$tabAction,'id'=>\$model->{$model->primaryKey()[0]}],
                    ],
EOS;
        
        foreach ($generator->getModelRelations($generator->modelClass, [
            'has_many'
        ]) as $name => $relation) {
            $relationObject = \Yii::createObject($relation->modelClass);
            if ($relation->via!=null ||  $generator->isTranslationTbl($relationObject->tableName())) {
                continue;
            }
            // Replace name of pivot tables with numbers
            $end = substr($name, - 2, 1);
            $last = substr($name, - 1);
            if (is_numeric($end) && ($last == 's')) {
                $name = substr($name, 0, strlen($name) - 1);
            }
            $modelName = $name;
            if(!method_exists($model,"get".$name)){
                continue;
            }
            // build tab items
            $label = Inflector::camel2words($name);
            
            // Make better label for numerical relations
            if (is_numeric(substr($label, - 1))) {
                $link = Inflector::camel2words(key($relation->link));
                $label = substr($label, 0, strlen($label) - 2) . ' ' . str_ireplace(' Id', null, Inflector::pluralize($link));
                $modelName = substr($name, 0, strlen($name) - 1);
            }
            
            $showAllRecords = false;
            if ($relation->via !== null && false) {
                $pivotName = Inflector::pluralize($generator->getModelByTableName($relation->via->from[0]));
                $pivotRelation = $model->{'get' . $pivotName}();
            }
            
            // relation list, add, create buttons
            ?>
<?php
            // render pivot grid
            if ($relation->via !== null && false) {
                $pjaxId = "pjax-{$pivotName}";
                $gridRelation = $pivotRelation;
                $gridName = $pivotName;
            } else {
                $pjaxId = "pjax-{$name}";
                $gridRelation = $relation;
                $gridName = $name;
            }
            $relationGrid = $generator->relationGrid($gridName, $gridRelation, $showAllRecords);
            
            $output = $relationGrid['code'];
            $relationBasePermission = $relationGrid['basePermission'];
            
            // render relation grid
            
            $relModelClass = $relation->modelClass;
            $relRoute = $generator->getForeignController($relation->modelClass);
            $segments = array_values(array_filter(explode('/',$relRoute)));
            $relModuleName = '';
            $relPK = $relModelClass::primaryKey()[0];
            $relTblName = $relModelClass::tableName();
            $groupBySt= '';
            if($relPK){
                $groupBySt = "->groupBy('{$relTblName}.{$relPK}')";
            }
            if(count($segments)>=2){
                $relModuleName = $segments[0];
            }
            $items .= "\n";
            if($relModuleName){
                $items .= <<<EOS
        (\Yii::\$app->hasModule('{$relModuleName}'))?
EOS;
            }
            $msgCat = $generator->isPartOfBP($relation->modelClass)?$generator->messageCategorySystem:$generator->messageCategory;
            $items .= <<<EOS
[

		            'label' => '<i class="fa fa-caret-right"></i>' .
		            \Yii::t('{$msgCat}', '$label').
		            Html::tag('span', \$model->get{$name}(){$groupBySt}->count(),
		                ['class'=>"label label-primary pull-right"]),
		            'url'=>(\$model->isNewRecord)?'#':[
		                      '{$controller}/'.'view',
		                      'id'=>\$model->{$model->primaryKey()[0]},
		                      'parent_action'=>\$tabAction,
		                      'relatedBlock'=>'{$label}'
		                  ],
		            'options'=>['data'=>['new-rec-alert'=>(\$model->isNewRecord)?1:0]],
		            'visible' => Yii::\$app->user->can('x_{$relationBasePermission}_see') && Yii::\$app->controller->crudRelations('{$name}'),
                    ]
EOS;
            if($relModuleName){
                $items .= <<<EOS
:null
EOS;
                $items .= <<<EOS
,
EOS;
            }
        }
        $items .= <<<EOS

                    [
                        'label'=>'<i class="fa fa-history"></i>' .
		            \Yii::t('$generator->messageCategorySystem', 'History').
		            Html::tag('span', \$model->getHistory()->count(),
		                ['class'=>"label label-primary pull-right"]),
                    'url'=>(\$model->isNewRecord)?'#':[
	                      '{$controller}/'.'view',
	                      'id'=>\$model->{$model->primaryKey()[0]},
	                      'parent_action'=>\$tabAction,
	                      'relatedBlock'=>'History'
	                ],
		            'options'=> ['data'=>['new-rec-alert'=>(\$model->isNewRecord)?1:0]],
                    'visible' => Yii::\$app->user->can('Administrator'),
                   ]
EOS;
        ?>
		<?= "<?php"?> 
		echo Nav::widget([
		    'encodeLabels'=>false,
		    'options' => ['class' =>'nav nav-pills nav-stacked'], // set this to nav-tab to get tab-styled navigation
		    'items' => array_filter([<?= $items?>]),
		]);
		?>
		</div>
	</div>
</div>
<?= "<?php\n"?>
echo $this->render('@taktwerk-views/_new_rec_dialog', ['model' => $model]);
?>