<?php
/**
 * Copyright (c) 2019.
 * @author Cedric Steiner (c.steiner@taktwerk.ch)
 */

echo "<?php\n";
?>
<?php echo $generator->getTimestampComment();?>
require(__DIR__ . DIRECTORY_SEPARATOR . 'base' . DIRECTORY_SEPARATOR . 'index.php');
