<?php
/**
 * Customizable controller class.
 * @var taktwerk\yiiboilerplate\templates\crud\Generator $generator
 * @var string $controllerClassName
 */
echo "<?php\n";

// Comment out the model if we are in app/models, otherwise don't
$isCommentedOut = strpos($generator->modelClass, 'app/models/') !== false;
?>
<?php echo $generator->getTimestampComment();?>
namespace <?= \yii\helpers\StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "<?= $controllerClassName ?>".
 */
class <?= $controllerClassName ?> extends \<?= \yii\helpers\StringHelper::dirname(
    ltrim(
        $generator->controllerClass,
        '\\'
    )
) . '\base\\' . $controllerClassName."\n"
?>
{
<?php if ($isCommentedOut): ?>
//    /**
//     * Model class with namespace
//     */
//    public $model = '<?= $generator->modelClass ?>';
//
//    /**
//     * Search Model class
//     */
//    public $searchModel = '<?= $generator->searchModelClass ?>';
<?php else: ?>
    /**
     * Model class with namespace
     */
    public $model = '<?= $generator->modelClass ?>';

    /**
     * Search Model class
     */
    public $searchModel = '<?= $generator->searchModelClass ?>';
<?php endif; ?>

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
}
