<?php
namespace taktwerk\yiiboilerplate\templates\crud;

/*
 * @link http://www.diemeisterei.de/
 * @copyright Copyright (c) 2015 diemeisterei GmbH, Stuttgart
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii\helpers\Json;

trait ProviderTrait
{

    /**
     * The first string of the datagrid should be a link to view the entity.
     * 
     * @var bool
     */
    public $hasViewLink = false;

    /**
     *
     * @return array Class names of the providers declared directly under crud/providers folder.
     */
    public static function getCoreProviders()
    {
        $files = FileHelper::findFiles(__DIR__ . DIRECTORY_SEPARATOR . 'providers', [
            'only' => [
                '*.php'
            ],
            'recursive' => false
        ]);
        
        foreach ($files as $file) {
            require_once $file;
        }
        
        return array_filter(get_declared_classes(), function ($a) {
            return stripos($a, __NAMESPACE__ . '\providers') !== false;
        });
    }

    /**
     *
     * @return array List of providers. Keys and values contain the same strings.
     */
    public function generateProviderCheckboxListData()
    {
        $coreProviders = self::getCoreProviders();
        
        return array_combine($coreProviders, $coreProviders);
    }

    /**
     */
    protected function initializeProviders()
    {
        // TODO: this is a hotfix for an already initialized provider queue on action re-entry
        if ($this->_p !== []) {
            return;
        }
        
        if ($this->providerList) {
            foreach ($this->providerList as $class) {
                $class = trim($class);
                if (! $class) {
                    continue;
                }
                $obj = \Yii::createObject([
                    'class' => $class
                ]);
                $obj->generator = $this;
                $this->_p[] = $obj;
                // \Yii::debug("Initialized provider '{$class}'", __METHOD__);
            }
        }
        
        \Yii::debug("CRUD providers initialized for model '{$this->modelClass}'", __METHOD__);
    }

    /**
     * Generates code for active field by using the provider queue.
     * 
     * @param
     *            $attribute
     * @param null $model
     * @return mixed|void
     */
    public function activeField($attribute, $model = null)
    {
        if ($model === null) {
            $model = $this->modelClass;
        }
        $code = $this->callProviderQueue(__FUNCTION__, $attribute, $model, $this);
        if ($code !== null) {
            Yii::debug("found provider for '{$attribute}'", __METHOD__);
            return $code;
        } else {
            $column = $this->getColumnByAttribute($attribute);
            if (! $column) {
                return;
            } else {
                return parent::generateActiveField($attribute);
            }
        }
    }

    /**
     *
     * @param
     *            $attribute
     * @param null $model
     * @return mixed
     */
    public function prependActiveField($attribute, $model = null)
    {
        if ($model === null) {
            $model = $this->modelClass;
        }
        $code = $this->callProviderQueue(__FUNCTION__, $attribute, $model, $this);
        if ($code) {
            Yii::debug("found provider for '{$attribute}'", __METHOD__);
        }
        
        return $code;
    }

    /**
     *
     * @param
     *            $attribute
     * @param null $model
     * @return mixed
     */
    public function appendActiveField($attribute, $model = null)
    {
        if ($model === null) {
            $model = $this->modelClass;
        }
        $code = $this->callProviderQueue(__FUNCTION__, $attribute, $model, $this);
        if ($code) {
            Yii::debug("found provider for '{$attribute}'", __METHOD__);
        }
        
        return $code;
    }

    /**
     *
     * @param
     *            $attribute
     * @param null $model
     * @param null $template
     * @return mixed|string|void
     */
    public function columnFormat($attribute, $model = null, $template = null)
    {
        if ($model === null) {
            $model = $this->modelClass;
        }
        
        // TODO refactor, make provider?
        $translated = false;
        if (strpos($attribute, 'translated.') !== false) {
            if (method_exists($this->modelClass, 'getLanguages')) {
                $translationModelName = $this->modelClass . 'Translation';
                $translationModel = new $translationModelName();
                
                $attribute = str_replace('translated.', '', $attribute);
                $translated = true;
                $model = $translationModel;
            }
        }
        
        $code = $this->callProviderQueue(__FUNCTION__, $attribute, $model, $this);
        
        
        /* fix for class not found issue - end */
        if ($code !== null) {
            /* fix for class not found issue - start */
            $code = str_replace(' yii\\', ' \\yii\\',$code);
            Yii::debug("found provider for '{$attribute}'", __METHOD__);
        } else {
            
            $code = $this->shorthandAttributeFormat($attribute, $model, $template, $translated);
            
            Yii::debug("using standard formatting for '{$attribute}'", __METHOD__);
        }
        
        return $code;
    }

    /**
     *
     * @param
     *            $attribute
     * @param null $model
     * @return mixed|string|void
     */
    public function attributeFormat($attribute, $model = null)
    {
        if ($model === null) {
            $model = $this->modelClass;
        }
        
        // TODO refactor, make provider?
        $translated = false;
        if (strpos($attribute, 'translated.') !== false) {
            if (method_exists($this->modelClass, 'getLanguages')) {
                $translationModelName = $this->modelClass . 'Translation';
                $translationModel = new $translationModelName();
                
                $attribute = str_replace('translated.', '', $attribute);
                $translated = true;
                $model = $translationModel;
                
                /*
                 * var_dump($translated );
                 * var_dump($attribute );
                 * var_dump($this->modelClass);
                 * var_dump($translationModelName);
                 */
                // ($translationModel);
            }
        }
        
        $code = $this->callProviderQueue(__FUNCTION__, $attribute, $model, $this);
        if ($code !== null) {
            Yii::debug("found provider for '{$attribute}'", __METHOD__);
            
            return $code;
        }
        
        // why check for column?
        // $column = $this->getColumnByAttribute($attribute);
        // if (!$column) {
        // return;
        // } else {
        return $this->shorthandAttributeFormat($attribute, $model, null, $translated);
        // }
        // don't call parent anymore
    }

    /**
     *
     * @param
     *            $name
     * @param null $model
     * @return mixed
     */
    public function partialView($name, $model = null)
    {
        if ($model === null) {
            $model = $this->modelClass;
        }
        $code = $this->callProviderQueue(__FUNCTION__, $name, $model, $this);
        if ($code) {
            Yii::debug("found provider for partial view '{name}'", __METHOD__);
        }
        
        return $code;
    }

    /**
     *
     * @param
     *            $name
     * @param
     *            $relation
     * @param bool $showAllRecords
     * @return mixed
     */
    public function relationGrid($name, $relation, $showAllRecords = false)
    {
        Yii::debug("calling provider queue for '$name'", __METHOD__);
        
        return $this->callProviderQueue(__FUNCTION__, $name, $relation, $showAllRecords);
    }

    /**
     *
     * @param
     *            $attribute
     * @param
     *            $model
     * @param null $template
     * @return string|void
     */
    protected function shorthandAttributeFormat($attribute, $model, $template = null, $translated = false)
    {
        
        $column = $this->getColumnByAttribute($attribute, $model);
        if (! $column) {
            Yii::debug("No column for '{$attribute}' found", __METHOD__);
            
            return;
        } else {
            Yii::debug("Table column detected for '{$attribute}'", __METHOD__);
        }
       
        $comment = $this->extractComments($column);
        
        if (is_array($column->enumValues) && count($column->enumValues) > 0) {
            if ($template === 'index') {
                $dropOptions = "[";
                foreach ($column->enumValues as $enumValue) {
                    $dropOptions .= "\n" . str_repeat(' ', 12) . "'" . $enumValue . "' => Yii::t('".$this->messageCategory."', '" . Inflector::humanize($enumValue) . "'),";
                }
                $dropOptions .= "\n" . str_repeat(' ', 8) . "],";
            }
            return "
                [
        " . ($template !== 'index' ? str_repeat(' ', 12) : '') . "'attribute' => '$column->name',
        " . ($template !== 'index' ? str_repeat(' ', 12) : '') . "'value' => function (\$model) {
            " . ($template !== 'index' ? str_repeat(' ', 12) : '') . "return \\Yii::t('".$this->messageCategory."', \yii\helpers\Inflector::humanize(\$model->$column->name));
        " . ($template !== 'index' ? str_repeat(' ', 12) : '') . "}," . ($template === 'index' ? "\n" . str_repeat(' ', 8) . "'filter' => $dropOptions
        'filterType' => GridView::FILTER_SELECT2,
        'class' => '\\kartik\\grid\\DataColumn',
        'filterWidgetOptions' => [
            'options' => [
                'placeholder' => ''
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ]
        ]," : "") . "\n" . str_repeat(' ', 4) . ($template !== 'index' ? str_repeat(' ', 12) : '') . "]";
        } elseif (
                strpos($column->name, '_hidden') !== false 
            || ($comment && @$comment->inputtype == strtolower('hidden'))
            || strpos($column->name, '_filemeta') !== false 
            || strpos($column->name, '_meta') !== false 
            || ($comment && (@$comment->inputtype == strtolower('filemeta')|| @$comment->inputtype == strtolower('meta')))
            ) {
            return "";
        } elseif ($column->phpType === 'boolean' || (strpos($column->name, "is_") === 0)) {
            if ($template === 'index') {
                return "[
        'attribute' => '$column->name',
        'class' => '\\kartik\\grid\\BooleanColumn',
        'trueLabel' => \\Yii::t('".$this->messageCategorySystem."','Yes'),
        'falseLabel' => \\Yii::t('".$this->messageCategorySystem."','No'),
        'content' => function (\$model) {
            return \$model->$column->name == 1 ? Yii::t('".$this->messageCategorySystem."', 'Yes') : Yii::t('".$this->messageCategorySystem."', 'No');
        },
    ]";
            }
            $format = 'boolean';
        } elseif ($comment && @$comment->inputtype == strtolower('qrcode')) {
            if ($template === 'index') {
                return "[
        'attribute' => '$column->name',
        'format' => 'html',
        'content' => function (\$model) {
            return Html::img(\$model->renderQrCode('$column->name'));
        },
    ]";
            }
        } elseif ($comment && @$comment->inputtype == strtolower('googlemap')) {
            if ($template === 'index') {
                return "[
        'attribute' => '$column->name',
        'format' => 'html',
        'content' => function (\$model) {
			return Html::img(\$model->renderGoogleMap('$column->name', [
				'format' => ClassDispenser::getMappedClass(\\taktwerk\\yiiboilerplate\\components\\GoogleMap::class)::IMAGE,
			]));
        },
    ]";
            }
        } elseif ($column->type === 'text') {
            $format = 'text';
        } elseif ($column->dbType == 'date' || ($column->dbType == 'datetime' && $template === 'index')) {
            if ($template === 'index') {
                $format = $column->dbType == 'datetime' ? 'Yii::$app->formatter->momentJsDateTimeFormat' : 'Yii::$app->formatter->momentJsDateFormat';
                return "[
        'attribute' => '$column->name',
        'content' => function (\$model) {
            return \\Yii::\$app->formatter->as" . ucfirst($column->dbType) . "(\$model->$column->name);
        },
        'class' => ClassDispenser::getMappedClass(taktwerk\\yiiboilerplate\\grid\\DateTimeColumn::class),
        'format' => '$column->dbType'
    ]";
            }
            $format = 'date';
        }
        elseif (strpos($column->name, '_media') !== false || ($comment && @$comment->inputtype == strtolower('files'))) {
            $mc = $this->modelClass;
            if ($model) {
                if (is_string($model)) {
                    $mc = $model;
                } else {
                    $mc = (get_class($model));
                }
            }
            
            // For files, we want to get a preview
            $c = "'content'";
            $files = '';
            if ($template != 'index') {
                $c = "'value'";
                
                $files = " \$files= \$model->getMediaFiles(\\" . $mc . "::MEDIA_MODEL_TYPE,\\" . $mc . "::MEDIA_CATEGORY_" . strtoupper($column->name) . ");";
                
                $files .= "\$fileHtml= [];";
                $files .= "
            foreach(\$files as \$f){
                \$fileHtml[]= Html::a(\$f->alt_name,\$f->getUrl(),['target'=>'_blank']);
            }\n";
                $files .= "\$html .='<br>'.implode(' <br> ', \$fileHtml);";
            }
            
            return "[
        'attribute' => '$column->name',
        'format' => 'raw',
        $c => function (\$model) {
            \$html = \$model->showMediaPreview(\\" . $mc . "::MEDIA_MODEL_TYPE,\\" . $mc . "::MEDIA_CATEGORY_" . strtoupper($column->name) . ");
            {$files}
            return \$html;

        },
    ]";
        } elseif (strpos($column->name, '_file') !== false  || ($comment && @$comment->inputtype == strtolower('file'))) {
            $c = "'content'";
            $files = '';
            if ($template != 'index') {
                $c = "'value'";
            }
            // For files, we want to get a preview
            return "[
        'attribute' => '$column->name',
        'format' => 'html',
        $c => function (\$model) {
                return \$model->showFilePreviewWithLink('" . $column->name . "');
                },
    ]";
        } elseif (strpos($column->name, '_canvas') !== false || ($comment && (@$comment->inputtype === 'canvas'))) {
            // For files, we want to get a preview
            $c = "'content'";
            $files = '';
            if ($template != 'index') {
                $c = "'value'";
            }
            
            return "[
        'attribute' => '$column->name',
        'format' => 'html',
        $c => function (\$model) {
            return \$model->showFilePreview('" . $column->name . "');
        },
    ]";
        }
        elseif ((($column->type === 'string' && ($comment && (in_array(strtolower(@$comment->inputtype),['color','colour']))))
            || (strpos($column->name, '_color') !== false || strpos($column->name, '_colour') !== false)
            )) {
                $c = "'content'";
                if ($template != 'index') {
                    $c = "'value'";
                }
            return "[
            'attribute'=>'$column->name',
            'format' => 'html',
            $c=> function(\$model){
                if(\$model->{$column->name}){
                    return Html::tag('div','', ['class'=>'crud-fld-color-type','style'=>'background-color:'.\$model->{$column->name}]);
                }
            },
            'filterType'=>\kartik\color\ColorInput::class
]";
        }
        elseif ((stripos($column->name, 'time') !== false && $column->phpType === 'integer') || $column->dbType == 'datetime') {
            $format = 'datetime';
        } elseif (stripos($column->name, 'email') !== false) {
            $format = 'email';
        } elseif (stripos($column->name, 'url') !== false || stripos($column->name, 'homepage') !== false || stripos($column->name, 'website') !== false) {
            $format = 'url';
        } else {
            $format = 'text';
        }
        
        if ($format === 'text') {
            // translated attribute
            if ($translated) {
                $valueexpr = "\$model->translated('" . $column->name . "')";
                $labelexpr = "\$translationLabels['" . $column->name . "']";
            } else {
                $valueexpr = "\$model->" . $column->name;
                $labelexpr = "";
            }
            
            // For texts, we want to limit to 500 char because otherwise it's unreadable
            $valueexpr = "nl2br(strlen($valueexpr) > 500 ? substr($valueexpr, 0, 500) . '...' : $valueexpr)";
            
            // If this is the first string, make it a link to view the model
            if ($this->hasViewLink === false && $attribute != 'id') {
                $this->hasViewLink = true;
                $modelObject = new $model(); // cast the model into an object to get the primary keys
                $valueexpr = "Html::a(" . $valueexpr . ", ['" . $this->getControllerID() . "/view', '" . $modelObject->primaryKey()[0] . "' => \$model->" . $modelObject->primaryKey()[0] . "])";
            }
            
            return "[
        'attribute' => '$column->name',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
        " . ($labelexpr ? "'label' => " . $labelexpr . "," : "") . " 
        'value' => function (\$model) {
            if(\$model->{$column->name} != strip_tags(\$model->{$column->name})){
                return \yii\helpers\StringHelper::truncate(\$model->{$column->name},500,Html::a('... see more',['view','id'=>\$model->id,'atr'=>'{$column->name}'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>\$model->id,'atr'=>'{$column->name}'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate(\$model->{$column->name},500,Html::a('... see more',['view','id'=>\$model->id,'atr'=>'{$column->name}'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>\$model->id,'atr'=>'{$column->name}'])]])));
            }
        },
    ]";
        }
        /*
         * if($column->name = 'title') {
         * ob_clean();
         * print_r($column);exit;
         * }
         */
        
        return str_repeat(' ', 16) . "'" . $column->name . ($format === 'text' ? '' : ':' . $format) . "'";
    }

    /**
     *
     * @param
     *            $func
     * @param
     *            $args
     * @param
     *            $generator
     * @return mixed
     */
    protected function callProviderQueue($func, $args, $generator)
    {
        // TODO: should be done on init, but providerList is empty
        $this->initializeProviders();
        
        $args = func_get_args();
        unset($args[0]);
        // walk through providers
        foreach ($this->_p as $obj) {
            if (method_exists($obj, $func)) {
                $c = call_user_func_array(array(
                    &$obj,
                    $func
                ), $args);
                // until a provider returns not null > changed to not null, false, ''
                if ($c) {
                    if (is_object($args)) {
                        $argsString = get_class($args);
                    } elseif (is_array($args)) {
                        $argsString = json_encode($args);
                    } else {
                        $argsString = $args;
                    }
                    $msg = 'Using provider ' . get_class($obj) . '::' . $func . ' ' . $argsString;
                    Yii::debug($msg, __METHOD__);
                    
                    return $c;
                }
            }
        }
    }
}
