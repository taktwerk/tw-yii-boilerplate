<?php

namespace taktwerk\yiiboilerplate\commands;

use taktwerk\yiiboilerplate\components\Helper;
use Yii;
use taktwerk\yiiboilerplate\modules\queue\commands\BackgroundCommandInterface;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use Exception;
use taktwerk\yiiboilerplate\components\ClassDispenser;

class GenerateZipJob implements BackgroundCommandInterface
{
    /**
     * Don't notify on success.
     * @var bool
     */
    public $send_mail = false;

    /**
     * @param QueueJob $queue
     * @return mixed|void
     * @throws Exception
     */
    public function run(QueueJob $queue)
    {
        $params = json_decode($queue->parameters, true);
        $model_id = $params['id'];
        $model_name_space = $params['name_space'];
        $type = $params['type'];
        $additional_files = isset($params['additional_files'])?$params['additional_files']:[];
        unset($additional_files['cmd']);
        $excluded_relations = isset($params['excluded_relations'])?$params['excluded_relations']:[];
        $model = $model_name_space::findOne(['id' => $model_id]);

        try {
            if (!$model) {
                $queue->status = QueueJob::STATUS_FAILED;
                $queue->error = 'Object not found';
            } else {

                $zip_name = method_exists($model,'getZipName')?$model->getZipName():$model->toString();
                $destination_path = 'zip/'.$model->client_id.'/'.$zip_name;
                $result = ClassDispenser::getMappedClass(Helper::class)::createZip($model->allZipFiles($type, $excluded_relations,$additional_files), $destination_path, $zip_name);

                if ($result) {
                    $queue->status = QueueJob::STATUS_FINISHED;
                } else {
                    $queue->status = QueueJob::STATUS_FAILED;
                    $queue->error = 'Generating zip error';
                }
            }
            $queue->save();
        } catch (Exception $e) {
            $queue->status = QueueJob::STATUS_FAILED;
            $queue->error = $e->getMessage();
            throw new Exception($queue->error);
        }
    }

}