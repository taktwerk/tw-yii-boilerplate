<?php

namespace taktwerk\yiiboilerplate\commands;

use Yii;
use \taktwerk\yiiboilerplate\modules\setting\models\GeneralSetting;
use yii\console\Controller;

class ZipupdateController extends Controller
{
    /**
     * If Taktwerk BP code is exist in vendor (execut shell command offlineUpdateProject.sh)
     * @throws Exception
     */

    public function actionIndexShell(int $generalSettingId = null, string $zipFilePath = null)
    {
        if (!$generalSettingId && !$zipFilePath) {
            throw new \Exception('GenerallSetting model id or zip file path should be setted');
        }
        $isExecuted = false;
        $rootPath = Yii::$app->basePath . '/..';
        $commandName = $rootPath . '/d/offlineUpdateProject.sh';

        if ($generalSettingId) {
            $dbPassword = Yii::$app->db->password;
            $dbUser = Yii::$app->db->username;
            $dbName = getenv('DB_ENV_MYSQL_DATABASE');
            $command = "sh $commandName --destination $rootPath --dbpassword $dbPassword --dbuser $dbUser --dbname $dbName --dbRowId $generalSettingId";
            $isExecuted = shell_exec($command);
        } 
        if (!$isExecuted && $zipFilePath) {
            $command = "sudo sh $commandName --destination $rootPath --filePath $zipFilePath";
            $isExecuted = shell_exec("unzip -o $zipFilePath -d $rootPath");
        }
        if (!$isExecuted) {
            
            throw new \Exception('Something went wrong');
        }
    }

    /**
     * If Taktwerk BP code is exist in vendor
     * @throws Exception
     */
    public function actionIndex(int $generalSettingId = null, string $zipFilePath = null)
    {
        if (!$generalSettingId && !$zipFilePath) {
            throw new \Exception('GenerallSetting model id or zip file path should be setted');
        }
        $isExecuted = false;
        if ($generalSettingId) {
            $isExecuted = $this->actionExtractFileFromGeneralSettingModel($generalSettingId);
        } 
        if (!$isExecuted && $zipFilePath) {
            $isExecuted = $this->actionUpdateFromZipFile($zipFilePath);
        }
        if (!$isExecuted) {
            throw new \Exception('Something went wrong');
        }
    }

     /**
     * Execute zip file from General Setting
     * @throws Exception
     */
    public function actionExtractFileFromGeneralSettingModel($generalSettingId)
    {
        $generalSettingId = (int) $generalSettingId;
        $generalSettingClassName = '\taktwerk\yiiboilerplate\modules\setting\models\GeneralSetting';

        if (!class_exists($generalSettingClassName)) {
            throw new \Exception('taktwerk\yiiboilerplate\modules\setting\models\GeneralSetting class is not exists');
        }

        $generalSetting = $generalSettingClassName::findOne($generalSettingId);

        if (!$generalSetting) {
            return false;
        }

        $filePath = Yii::$app->basePath . '/../storage' . $generalSetting->getFilePath('attached_file');

        if (!$filePath) {
            throw new \Exception('File not founded');
        }
        if ($this->actionUpdateFromZipFile($filePath)) {
            return true;
        }
        
        throw new \Exception('Something went wrong');
    }

    /**
     * Execute zip file
     * @throws Exception
     */
    public function actionUpdateFromZipFile(string $zipFilePath)
    {
        if (!file_exists($zipFilePath)) {
            throw new \Exception('File is not exists');
        }
        if (mime_content_type($zipFilePath) !== 'application/zip') {
            throw new \Exception('File is not zip file');
        }
        if (!`which unzip`) {
            throw new \Exception('Command `unzip` is not exist. Please, install: "sudo apt install unzip"');
        }
        $rootPath = Yii::$app->basePath . '/..';
        $zipFilePath = escapeshellarg($zipFilePath);
        shell_exec("unzip -o $zipFilePath -d $rootPath");
        if (!shell_exec("unzip -o $zipFilePath -d $rootPath")) {
            throw new \Exception('File not founded');
        }

        return true;
    }
}
