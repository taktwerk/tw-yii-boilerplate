<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 6/27/2017
 * Time: 3:56 PM
 */

namespace taktwerk\yiiboilerplate\commands;

use taktwerk\yiiboilerplate\components\Crawler;
use yii\base\Exception;
use yii\console\Controller;
use yii\console\ExitCode;
use taktwerk\yiiboilerplate\components\ClassDispenser;

class CrawlerController extends Controller
{
    
    /**
     * @var
     */
    public $url;
    /**
     * @var bool
     */
    public $verbose = false;
    /**
     * @var
     */
    public $username;
    /**
     * @var
     */
    public $password;

    /**
     * @param string $actionID
     * @return array
     */
    public function options($actionID)
    {
        return ['url', 'verbose', 'username', 'password'];
    }

    /**
     * @return array
     */
    public function optionAliases()
    {
        return [
            'v' => 'verbose',
            'u' => 'username',
            'p' => 'password',
        ];
    }

    /**
     * @return int
     * @throws Exception
     */
    public function actionIndex()
    {
        if ($this->url == null || empty($this->url)) {
            $this->stderr("Parameter --url missing");
            exit;
        }
        if (!filter_var($this->url, FILTER_VALIDATE_URL, ['flags' => [FILTER_FLAG_SCHEME_REQUIRED, FILTER_FLAG_HOST_REQUIRED]])) {
            $this->stderr("Url \"{$this->url}\" is not a valid url.");
            exit;
        }
        $start = microtime();
        $crawler = ClassDispenser::makeObject(Crawler::class, [[
            'url' => $this->url,
            'verbose' => $this->verbose,
            'username' => $this->username,
            'password' => $this->password
        ]]);
        try {
            $crawler->run();
            $this->stdout("Total links checked: {$crawler::$total}. Success: {$crawler::$totalSuccess}. Failed: {$crawler::$totalErrors}\n");
            if (!empty($crawler->errors)) {
                $this->stdout("Links with errors [URL - Error code]\n");
                foreach ($crawler->errors as $url => $code) {
                    $this->stdout("$url - $code\n");
                }
                $this->stdout("\nFinished in: " . $this->microtimeDiff($start) . " seconds\n");
                return ExitCode::UNSPECIFIED_ERROR;
            }
            $this->stdout("\nFinished in: " . $this->microtimeDiff($start) . " seconds\n");
            return ExitCode::UNSPECIFIED_ERROR;
        } catch (\Exception $e) {
            $this->stderr($e->getMessage());
            $this->stdout("\nFinished in: " . $this->microtimeDiff($start) . " seconds\n");
            return ExitCode::UNSPECIFIED_ERROR;
        }
    }

    protected function microtimeDiff($start, $end = null)
    {
        if (!$end) {
            $end = microtime();
        }
        list($start_usec, $start_sec) = explode(" ", $start);
        list($end_usec, $end_sec) = explode(" ", $end);
        $diff_sec = intval($end_sec) - intval($start_sec);
        $diff_usec = floatval($end_usec) - floatval($start_usec);
        return floatval($diff_sec) + $diff_usec;
    }

}