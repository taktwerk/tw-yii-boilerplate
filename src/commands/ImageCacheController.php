<?php

namespace taktwerk\yiiboilerplate\commands;

use taktwerk\yiiboilerplate\components\Helper;
use yii\base\InvalidRouteException;
use yii\console\Controller;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use taktwerk\yiiboilerplate\helpers\CrudHelper;
use yii\helpers\Inflector;
use taktwerk\yiiboilerplate\helpers\FileHelper;
use taktwerk\yiiboilerplate\components\ImageHelper;

/**
 * Image Cache commands
 */
class ImageCacheController extends Controller
{

    public $defaultAction = 'generate-all';

    /**
     * Generate Cache of the all the images uploaded in models
     */
    public function actionGenerateAll()
    {
        $tables = \Yii::$app->db->getSchema()->getTableSchemas();
        foreach ($tables as $tbl) {
            foreach ($tbl->columns as $col) {
                if (CrudHelper::checkIfUploaded($col) || CrudHelper::checkIfCanvas($col)) {
                    $modelClass = (CrudHelper::findModelClass(Inflector::classify($tbl->name), $tbl->fullName));
                    if ($modelClass) {
                        $crudHelper = ClassDispenser::getMappedClass(CrudHelper::class);
                        $fields = array_merge($crudHelper::getCanvasFields($modelClass), $crudHelper::getUploadFields($modelClass));
                        foreach ($modelClass::find()->batch() as $items) {
                            foreach ($items as $item) {
                                foreach ($fields as $f) {
                                    $path = $item->getUploadPath() . $item->{$f};
                                    if ($item->{$f} && $path) {
                                        $fileType = $item->getFileType($f);
                                        if ($fileType == 'image') {
                                            try {
                                                // creates a 100x100 and 1920x1080 size of the images
                                                ImageHelper::processImage($path, [
                                                    'height' => 100,
                                                    'width' => 100
                                                ]);
                                                ImageHelper::processImage($path, [
                                                    'height' => 1080,
                                                    'width' => 1920
                                                ]);
                                            } catch (\Exception $e) {
                                                $pkString = '';
                                                foreach ($item->primaryKey() as $a) {
                                                    $pkString .= $a . ':' . $item->{$a} . ',';
                                                }
                                                $pkString = rtrim($pkString, ',');
                                                $this->stdout("\tCould not create cache image for table {$tbl->fullName} having $pkString. Error: {$e->getMessage()}\n");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
}
