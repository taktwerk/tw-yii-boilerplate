<?php

namespace taktwerk\yiiboilerplate\commands;

use taktwerk\yiiboilerplate\components\Helper;
use yii\base\InvalidRouteException;
use yii\console\Controller;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * Task runner command for development.
 *
 * @author Sharaddha>
 */
class ZipController extends Controller
{
    public $defaultAction = 'generate';

    /**
     * @var bool add job or update cache directly
     * - 1: add job to `queue_job` table
     * - 0: directly update cache
     */
    public $job = true;

    /**
     * @param string $actionID
     * @return array|string[]
     */
    public function options($actionID)
    {
        $actionParams = [];
        $paramsByAction = [
            'generate' => ['job'],
        ];

        if (isset($paramsByAction[$actionID])) {
            $actionParams = $paramsByAction[$actionID];
        }

        return array_merge(parent::options($actionID), $actionParams);
    }

    /**
     * Generate Zip for all documents of a model
     * php yii zip 1 NAMESPACE ADDITIONAL_FILES
     *
     * Generate job for creating zip
     * php yii zip 1 NAMESPACE --job=1
     *
     * @param string $name_space namespace of the model
     * @param int $id
     * @param array $additional_files array of all the additional files that needs to be included in the zip
     * @param array $exclude_relations array of all the excluded relations that does not need to be included in the zip
     * @throws InvalidRouteException
     * @throws \yii\base\Exception
     */
    public function actionGenerate($id, $modelClass, $type='processed', $exclude_relations=[], $additional_files=[]){

        $model = $modelClass::findOne($id);

        if(!$model){
            throw new InvalidRouteException("Cannot find model",404);
        }
        $this->addModelZipJob($model,$type);
    }
    public function actionGenerateAll($modelClass, $type='processed', $exclude_relations=[], $additional_files=[]){
        $query = $modelClass::find();
        foreach ($query->batch() as $models){
            foreach($models as $model){
                $this->addModelZipJob($model,$type);
            }
        }
    }
    protected function addModelZipJob($model,$type='processed'){
        if($this->job){
            try {
                $queueJob = $model->findZipGenerateQueueJobModel($type);
                if ($queueJob) {
                    $this->stdout("\tJob #{$queueJob->id} is already on queue for model {$model->toString()}.\n");
                } else {
                    if ($queueJob = $model->createZipGenerateQueueJobModel($type)) {
                        $this->stdout("\tJob for model {$model->toString()} added.\n");
                    } else {
                        $this->stdout("\tCould not add job for model {$model->toString()}.\n");
                    }
                }
            } catch (\Exception $exception) {
                $this->stdout("\tCould not add job for model {$model->toString()}. Error: {$exception->getMessage()}\n");
            }
        }else{
            $zip_name = method_exists($model,'getZipName')?$model->getZipName():$model->toString();
            $destination_path = 'zip/'.$model->client_id.'/'.$zip_name;
            $additional_files =  $model->additionalFilesForZip();
            if($additional_files['cmd']){
                foreach ($additional_files['cmd'] as $file){
                    shell_exec($file);
                }
            }
            unset($additional_files['cmd']);
            $exclude_relations = $model->excludedRelationsForZip();
            ClassDispenser::getMappedClass(Helper::class)::createZip($model->allZipFiles($type, $exclude_relations, $additional_files), $destination_path, $zip_name);
        }
    }
}
