<?php

namespace taktwerk\yiiboilerplate\commands;

use lajax\translatemanager\commands\TranslatemanagerController;
use taktwerk\yiiboilerplate\services\DynamicTranslation;

/**
 *
 */
class TranslateController extends TranslatemanagerController
{
    /**
     * Generate the views/_generated file for models that need translations based off of a key
     */
    public function actionExportModels()
    {
        $translator = new DynamicTranslation();
        $count = $translator->run();

        $this->stdout('Generated ' . $count . " keys.\n");
    }
}
