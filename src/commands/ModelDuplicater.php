<?php
namespace taktwerk\yiiboilerplate\commands;

use taktwerk\yiiboilerplate\modules\queue\commands\BackgroundCommandInterface;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use Exception;
use yii\db\ActiveRecord;
use yii\db\Expression;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use taktwerk\yiiboilerplate\models\TwActiveRecord;

class ModelDuplicater implements BackgroundCommandInterface
{

    /**
     * Don't notify on success.
     *
     * @var bool
     */
    public $send_mail = false;

    public $log = [];

    /**
     *
     * @param QueueJob $queue
     * @return mixed|void
     * @throws Exception
     */
    public function run(QueueJob $queue)
    {
        $params = json_decode($queue->parameters, true);
        
        $mClass = $params['model'];

        if (! (is_subclass_of($mClass, TwActiveRecord::class))) {
            return false;
        }
        try {
            if(empty($params['ids'])){
                $queue->status = QueueJob::STATUS_FINISHED;
            }else{
                foreach($params['ids'] as $id){
                    $model = $mClass::findOne($id);
                    try{
                        if($model && $model->duplicatable()){
                            $model->duplicateRecord();
                        }
                    }catch (\Exception $e){
                        throw $e;
                        $queue->error .= 'Error duplicating model with primary key '.$id.' : '.  $e->getMessage().' | '; 
                    }
                }
                $queue->status = QueueJob::STATUS_FINISHED;
                $queue->save();
            }
            $queue->save();
        } catch (Exception $e) {
            $queue->status = QueueJob::STATUS_FAILED;
            $queue->error = $e->getMessage();
            throw $e;
        }
    }
}
