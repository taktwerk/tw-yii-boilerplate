<?php
namespace taktwerk\yiiboilerplate\commands;

use taktwerk\yiiboilerplate\modules\queue\commands\BackgroundCommandInterface;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use Exception;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\FileHelper;
use taktwerk\yiiboilerplate\components\ClassDispenser;

class FileCompressor implements BackgroundCommandInterface
{

    /**
     * Don't notify on success.
     *
     * @var bool
     */
    public $send_mail = false;

    public $log = [];

    public const COMPRESSED_FILE_POSTFIX = '_min';

    public const COMPRESSED_THUMB_POSTFIX = '_thumb.jpeg';
    public const COMPRESSED_PDF_THUMB_POSTFIX = '_thumb.png';

    /**
     *
     * @param QueueJob $queue
     * @return mixed|void
     * @throws Exception
     */
    public function run(QueueJob $queue)
    {
        $params = json_decode($queue->parameters, true);
        $mClass = $params['model'];

        if (! (is_subclass_of($mClass, ActiveRecord::class))) {
            return false;
        }
        try {
            $model = $mClass::findOne([
                $params['id']
            ]);
            $atr = $params['attribute'];
            
            if ($model == null) {
                $queue->error = 'Model not found';
            } else if ($atr == null) {
                $queue->error = 'Model\'s provided attribute cannot be empty';
            } else if (!$model->hasAttribute($atr)) {
                $queue->error = 'Model does not have any property named "' . $atr . '"';
            } else {
                $response = ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\FileHelper::class)::compressFileByModel($model, $atr);
                if ($response['success'] === true) {
                    $queue->status = QueueJob::STATUS_FINISHED;
                    $model->updated_at = new Expression('NOW()');
                    $model->save();
                } else if ($response['success'] === false) {
                    $queue->error = $response['error'];
                }
            }
            $queue->save();
        } catch (Exception $e) {
            $queue->status = QueueJob::STATUS_FAILED;
            $queue->error = $e->getMessage();
            throw $e;
        }
    }
}
