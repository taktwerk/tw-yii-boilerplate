<?php

namespace taktwerk\yiiboilerplate\commands;

/*
 * @link http://www.diemeisterei.de/
 *
 * @copyright Copyright (c) 2014 diemeisterei GmbH, Stuttgart
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use mikehaertl\shellcommand\Command;
use yii\console\Controller;

/**
 * Task runner command for development.
 *
 * @author Tobias Munk <tobias@diemeisterei.de>
 */
class AppController extends Controller
{
    public $defaultAction = 'version';

    /**
     * Displays application version from ENV variable (read from version file).
     */
    public function actionVersion()
    {
        $this->stdout(\Yii::$app->id.' version ' . APP_VERSION . ' hash ' . APP_HASH);
        $this->stdout("\n");
    }

    /**
     * Renew the hash and version
     */
    public function actionRenewVersion()
    {
        exec('git rev-parse --short HEAD > src/hash', $output);
        exec('git describe --tags > src/version', $output);
        $this->stdout("> Hash and Version renewed.\n");
    }

    /**
     * Clear [application]/web/assets folder.
     */
    public function actionClearAssets()
    {
        $assets = \Yii::getAlias('@web/assets');

        // Matches from 7-8 char folder names, the 8. char is optional
        $matchRegex = '"^[a-z0-9][a-z0-9][a-z0-9][a-z0-9][a-z0-9][a-z0-9][a-z0-9]\?[a-z0-9]$"';

        // create $cmd command
        $cmd = 'cd "'.$assets.'" && ls | grep -e '.$matchRegex.' | xargs rm -rf ';

        // Set command
        $command = new Command($cmd);

        // Prompt user
        $delete = $this->confirm("\nDo you really want to delete web assets?", true);

        if ($delete) {
            // Try to execute $command
            if ($command->execute()) {
                echo "Web assets have been deleted.\n\n";
            } else {
                echo "\n".$command->getError()."\n";
                echo $command->getStdErr();
            }
        }
    }

    /**
     * Generate application and required vendor documentation.
     */
    public function actionGenerateDocs()
    {
        if ($this->confirm('Regenerate documentation files into ./docs-html', true)) {

            // array with commands
            $commands = [];
            $commands[] = 'vendor/bin/apidoc guide --interactive=0 docs web/apidocs';
            $commands[] = 'vendor/bin/apidoc api --interactive=0 --exclude=runtime/,tests/ src,vendor/schmunk42 web/apidocs';
            $commands[] = 'vendor/bin/apidoc guide --interactive=0 docs web/apidocs';

            foreach ($commands as $command) {
                $cmd = new Command($command);
                if ($cmd->execute()) {
                    echo $cmd->getOutput();
                } else {
                    echo $cmd->getOutput();
                    echo $cmd->getStdErr();
                    echo $cmd->getError();
                }
            }
        }
    }

    /**
     * @param string $command
     */
    protected function action($command, $params = [])
    {
        echo "\nRunning action '$command'...\n";
        \Yii::$app->runAction($command, $params);
    }

    /**
     * Action to set a manual datetime in projectroot/time.txt
     * This needs a sudo cron to run a bash script with the following content:
     * #!/bin/bash
     * if [ -f /path/to/project/time.txt ]; then
     *      date --set="$(cat /path/to/project/time.txt)" >> /path/to/project/settime.log;
     *      rm /path/to/project/time.txt
     * fi
     * @param $datetime_string
     */
    public function actionSetServerTime($datetime_string) {
        $datetime = strtotime($datetime_string);
        if($datetime) {
            $setdate = date('Y-m-d H:i:s', $datetime);
            echo "set server time: " . $setdate . "\n";
            $timefile = \Yii::getAlias('@root/time.txt');
            file_put_contents($timefile, $setdate);
        }
        else {
            echo "no correct date time format, for example 2020-05-30 13:55:00\n";
        }
    }
}
