<?php

namespace taktwerk\yiiboilerplate\commands;

use schmunk42\giiant\commands\BatchController as SBatchController;
use schmunk42\giiant\generators\crud\Generator;
use schmunk42\giiant\generators\model\Generator as ModelGenerator;
use taktwerk\yiiboilerplate\helpers\TableHelper;
use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

/**
 * Task runner command for development.
 *
 * @author Samir Dixit <samirdixit0@gmail.com>
 */
class BatchController extends SBatchController
{
    public $generateExtendedController = true;
    public $generateSearchModel = true;
    public $generateExtendedViews = true;
    public $generateRestController = true;
    /**
     * @desc if set to true, regenrates extended files ignoring $generateSearchModel,$generateExtendedViews,$generateRestController ,$generateExtendedController 
     * values
     */
    public $regenerateNonBaseFiles = false;
    
    /**
     * {@inheritdoc}
     */
    public function options($id)
    {
        return array_merge(
            parent::options($id),
            [
                'regenerateNonBaseFiles',
            ]
            );
    }
    /**
     * Run batch process to generate models all given tables.
     *
     * @throws \yii\console\Exception
     */
    public function actionModels()
    {
        ini_set('memory_limit', '-1');
        if($this->regenerateNonBaseFiles){
            $this->generateExtendedController =true;
            $this->generateSearchModel =true;
            $this->generateExtendedViews =true;
            $this->generateRestController =true;
            $this->extendedModels = true;
        }
        // create models
        foreach ($this->tables as $table) {
            $cmnt = TableHelper::getTableComment($table);
            $table = str_replace($this->tablePrefix, '', $table);
            $name = isset($this->tableNameMap[$table]) ? $this->tableNameMap[$table] : $this->modelGenerator->generateClassName($table);
            
            $crudControllerNamespace = $this->crudControllerNamespace;
            $modelNamespace = $this->modelNamespace;
            if ($cmnt && is_array($cmnt)) {
                if (isset($cmnt['base_namespace'])) {
                    $baseNamespace = rtrim($cmnt['base_namespace'], '\\');
                    $modelNamespace = rtrim($cmnt['base_namespace'], '\\') . '\models';
                    $crudControllerNamespace = $baseNamespace . '\controllers';
                }
            }
            $expectedControllerClass = $crudControllerNamespace . '\\' . $name . 'Controller';
            $params = [
                'interactive' => $this->interactive,
                'overwrite' => $this->overwrite,
                'expectedControllerClass'=>$expectedControllerClass,
                'useTimestampBehavior' => $this->useTimestampBehavior,
                'createdAtColumn' => $this->createdAtColumn,
                'updatedAtColumn' => $this->updatedAtColumn,
                'useTranslatableBehavior' => $this->useTranslatableBehavior,
                'languageTableName' => $this->languageTableName,
                'languageCodeColumn' => $this->languageCodeColumn,
                'useBlameableBehavior' => $this->useBlameableBehavior,
                'createdByColumn' => $this->createdByColumn,
                'updatedByColumn' => $this->updatedByColumn,
                'template' => $this->template,
                'ns' => $modelNamespace,
                'db' => $this->modelDb,
                'tableName' => $table,
                'tablePrefix' => $this->tablePrefix,
                'enableI18N' => $this->enableI18N,
                'singularEntities' => $this->singularEntities,
                'messageCategory' => $this->modelMessageCategory,
                'generateModelClass' => $this->extendedModels,
                'baseClassPrefix' => $this->modelBaseClassPrefix,
                'baseClassSuffix' => $this->modelBaseClassSuffix,
                'modelClass' => isset($this->tableNameMap[$table]) ? $this->tableNameMap[$table] : Inflector::camelize($table),
                'baseClass' => $this->modelBaseClass,
                'baseTraits' => $this->modelBaseTraits,
                'removeDuplicateRelations' => $this->modelRemoveDuplicateRelations,
                'generateRelations' => $this->modelGenerateRelations,
                'tableNameMap' => $this->tableNameMap,
                'generateQuery' => $this->modelGenerateQuery,
                'queryNs' => $this->modelQueryNamespace,
                'queryBaseClass' => $this->modelQueryBaseClass,
                'generateLabelsFromComments' => $this->modelGenerateLabelsFromComments,
                'generateHintsFromComments' => $this->modelGenerateHintsFromComments
            ];
            $route = 'gii/giiant-model';
            
            $app = \Yii::$app;
            $temp = new \yii\console\Application($this->appConfig);
            $temp->runAction(ltrim($route, '/'), $params);
            $temp->get($this->modelDb)->close();
            unset($temp);
            \Yii::$app = $app;
            \Yii::$app->log->logger->flush(true);
        }
    }

    /**
     * Run batch process to generate CRUDs all given tables.
     *
     * @throws \yii\console\Exception
     */
    public function actionCruds()
    {
        ini_set('memory_limit', '-1');
        if($this->regenerateNonBaseFiles){
            $this->generateExtendedController =true;
            $this->generateSearchModel =true;
            $this->generateExtendedViews =true;
            $this->generateRestController =true;
            $this->extendedModels = true;
        }
        // create CRUDs
        $providers = ArrayHelper::merge($this->crudProviders, Generator::getCoreProviders());
        
        foreach ($this->tables as $table) {
            $table = str_replace($this->tablePrefix, '', $table);
            $name = isset($this->tableNameMap[$table]) ? $this->tableNameMap[$table] : $this->modelGenerator->generateClassName($table);

            $cmnt = TableHelper::getTableComment($table);
            $crudControllerNamespace = $this->crudControllerNamespace;
            $modelNamespace = $this->modelNamespace;
            $crudSearchModelNamespace = $this->crudSearchModelNamespace;
            $crudViewPath = $this->crudViewPath;
            if ($cmnt) {
                if (isset($cmnt['base_namespace'])) {
                    $baseNamespace = rtrim($cmnt['base_namespace'], '\\');
                    $crudViewPath = $this->getDirectoryFromNamespace($baseNamespace.'\views');
                    $modelNamespace = $baseNamespace . '\models';
                    $crudControllerNamespace = $baseNamespace . '\controllers';
                    $crudSearchModelNamespace = $modelNamespace.'\search';
                }
            }
            // create folders
            $this->createDirectoryFromNamespace($crudControllerNamespace);
            $this->createDirectoryFromNamespace($crudSearchModelNamespace);
            $params = [
                'interactive' => $this->interactive,
                'overwrite' => $this->overwrite,
                'template' => $this->template,
                'modelClass' => $modelNamespace . '\\' . $name,
                'searchModelClass' => $crudSearchModelNamespace . '\\' . $name . $this->crudSearchModelSuffix,
                'controllerNs' => $crudControllerNamespace,
                'controllerClass' => $crudControllerNamespace . '\\' . $name . 'Controller',
                'viewPath' => $crudViewPath,
                'pathPrefix' => $this->crudPathPrefix,
                'tablePrefix' => $this->tablePrefix,
                'enableI18N' => $this->enableI18N,
                'singularEntities' => $this->singularEntities,
                'messageCategory' => $this->crudMessageCategory,
                'modelMessageCategory' => $this->modelMessageCategory,
                'actionButtonClass' => 'taktwerk\\yiiboilerplate\\grid\\ActionColumn',
                'baseControllerClass' => $this->crudBaseControllerClass,
                'providerList' => $providers,
                'skipRelations' => $this->crudSkipRelations,
                'accessFilter' => $this->crudAccessFilter,
                'baseTraits' => $this->crudBaseTraits,
                'tidyOutput' => $this->crudTidyOutput,
                'fixOutput' => $this->crudFixOutput,
                'template' => $this->crudTemplate,
                'indexWidgetType' => $this->crudIndexWidgetType,
                'indexGridClass' => $this->crudIndexGridClass,
                'formLayout' => $this->crudFormLayout,
                'overwriteControllerClass'=>$this->generateExtendedController,
                'overwriteSearchModelClass'=>$this->generateSearchModel,
                'overwriteExtendedViews'=>$this->generateExtendedViews,
                'overwriteRestControllerClass'=>$this->generateRestController
            ];
            $route = 'gii/giiant-crud';
            $app = \Yii::$app;
            $temp = new \yii\console\Application($this->appConfig);
            $temp->runAction(ltrim($route, '/'), $params);
            unset($temp);
            \Yii::$app = $app;
            \Yii::$app->log->logger->flush(true);
        }
    }

    public function setConfigByTableCmnt(){
        $cmnt = TableHelper::getTableComment($table);
        if ($cmnt) {
            if (isset($cmnt['base_namespace'])) {
                $this->modelNamespace = rtrim($cmnt['base_namespace'], '\\') . '\models';
            }
        }
    }
    /**
     * Helper function to create.
     *
     * @param $ns Namespace
     */
    private function createDirectoryFromNamespace($ns)
    {
        echo \Yii::getRootAlias($ns);
        $dir = \Yii::getAlias('@' . str_replace('\\', '/', ltrim($ns, '\\')));
        @mkdir($dir);
    }
    /**
     * Helper function to get directory path from namespace.
     *
     * @param $ns Namespace
     */
    private function getDirectoryFromNamespace($ns){
        echo \Yii::getRootAlias($ns);
        $dir = \Yii::getAlias('@' . str_replace('\\', '/', ltrim($ns, '\\')));
        return $dir;
    }
}