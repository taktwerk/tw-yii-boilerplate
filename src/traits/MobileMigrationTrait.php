<?php


namespace taktwerk\yiiboilerplate\traits;


use yii\db\Schema;

trait MobileMigrationTrait
{
    public function addOfflineChangeDateColumns($table)
    {
        if (!$this->db) {
            return;
        }
        $this->db->createCommand()->addColumn($table, "local_created_at", Schema::TYPE_DATETIME)->execute();
        $this->db->createCommand()->addColumn($table, "local_updated_at", Schema::TYPE_DATETIME)->execute();
        $this->db->createCommand()->addColumn($table, "local_deleted_at", Schema::TYPE_DATETIME)->execute();
    }

    public function dropOfflineChangeDateColumns($table)
    {
        if (!$this->db) {
            return;
        }
        $this->db->createCommand()->dropColumn($table, "local_created_at", Schema::TYPE_DATETIME)->execute();
        $this->db->createCommand()->dropColumn($table, "local_updated_at", Schema::TYPE_DATETIME)->execute();
        $this->db->createCommand()->dropColumn($table, "local_deleted_at", Schema::TYPE_DATETIME)->execute();
    }
}
