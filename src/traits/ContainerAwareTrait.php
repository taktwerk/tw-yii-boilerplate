<?php

namespace taktwerk\yiiboilerplate\traits;

use \taktwerk\yiiboilerplate\helpers\ClassMapHelper;
use Yii;
use yii\base\InvalidConfigException;
use yii\di\Container;

/**
 * @property-read Container $di
 * @property-ready \taktwerk\yiiboilerplate\helpers\ClassMapHelper $classMap
 */
trait ContainerAwareTrait
{
    /**
     * @return Container
     */
    public static function getDiContainer()
    {
        return Yii::$container;
    }

    /**
     * Gets a class from the container.
     *
     * @param string $class  he class name or an alias name (e.g. `foo`) that was previously registered via [[set()]]
     *                       or [[setSingleton()]]
     * @param array  $params constructor parameters
     * @param array  $config attributes
     *
     * @throws InvalidConfigException
     * @return object
     */
    public static function makeObject($class, $params = [], $config = [])
    {
        return self::getDiContainer()->get($class, $params, $config);
    }


    /**
     * @throws InvalidConfigException
     * @return \Da\User\Helper\ClassMapHelper|object
     *
     */
    public static function getClassMap2()
    {
        return self::getDiContainer()->get(ClassMapHelper::class);
    }
    public static function getMappedClass($className){
        return self::getClassMap2()->get($className);
    }
}