<?php


namespace taktwerk\yiiboilerplate\traits;
use Yii;
 
trait MobileDateTrait
{
    public function getIntCreatedAt()
    {
        return $this->getIntegerUtcTimeByAttribute($this->created_at);
    }

    public function getIntUpdatedAt()
    {
        return $this->getIntegerUtcTimeByAttribute($this->updated_at);
    }

    public function getIntDeletedAt()
    {
        return $this->getIntegerUtcTimeByAttribute($this->deleted_at);
    }

    public function getIntLocalCreatedAt()
    {
        $date = $this->local_created_at ? $this->local_created_at : $this->created_at;

        return $this->getIntegerUtcTimeByAttribute($date);
    }

    public function getIntLocalUpdatedAt()
    {
        $date = $this->local_updated_at ? $this->local_updated_at : $this->updated_at;

        return $this->getIntegerUtcTimeByAttribute($date);
    }

    public function getIntLocalDeletedAt()
    {
        $date = $this->local_deleted_at ? $this->local_deleted_at : $this->deleted_at;

        return $this->getIntegerUtcTimeByAttribute($date);
    }

    public function changeDateAttributeToUtcAndSqlTimeZone($dateAttribute)
    {
        $currentDate = Yii::$app->getRequest()->getHeaders()->get('X-CURRENT-DATETIME');
        $localTimeZoneDateAttribute = $dateAttribute;
        $differenceBeetweenSqlAndUtcInSeconds = $this->differenceBeetweenSqlAndUtc();
        if ($differenceBeetweenSqlAndUtcInSeconds) {
            $localCreatedAt = new \DateTime($localTimeZoneDateAttribute);
            $localCreatedAt->add(new \DateInterval('PT' . $differenceBeetweenSqlAndUtcInSeconds . 'S'));
            $localTimeZoneDateAttribute = $localCreatedAt->format('Y-m-d H:i:s');
        }
        if (!empty($currentDate) && is_string($currentDate)) {
            $clientTime = new \DateTime($currentDate);
            $differenceInSeconds = $clientTime->getOffset();
            if ($differenceInSeconds) {
                $localCreatedAt = new \DateTime($localTimeZoneDateAttribute);
                $localCreatedAt->sub(new \DateInterval('PT' . $differenceInSeconds . 'S'));
                $localTimeZoneDateAttribute = $localCreatedAt->format('Y-m-d H:i:s');
            }
        }

        return $localTimeZoneDateAttribute;
    }

    public function getIntegerUtcTimeByAttribute($attributeDate)
    {
        try {
            $date = $this->getUtcTimeByAttribute($attributeDate);

            return $this->getStrToTimeByUtc($date);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getUtcTimeByAttribute($date)
    {
        if (empty($date)) {
            return null;
        }

        // Replace a space with a plus. Yii does this.\
        if ($date instanceof yii\db\Expression) {
            if (empty($date->expression) || $date->expression !== 'NOW()') {
                return null;
            }
            $executedQuery = Yii::$app->getDb()->createCommand('SELECT ' . $date->expression . ' as date')
                ->queryAll();
            if (!$executedQuery || !isset($executedQuery[0]) || empty($executedQuery[0]['date'])) {
                return null;
            }
            $date = $executedQuery[0]['date'];
        }
        $date = new \DateTime($date);
        $sqlCurrentTime = Yii::$app->getDb()->createCommand('SELECT NOW() as currentTime')->queryAll();
        $sqlCurrentTimeInSeconds = 0;
        if (!empty($sqlCurrentTime[0]) && !empty($sqlCurrentTime[0]['currentTime'])) {
            $sqlCurrentTimeInSeconds =  $this->getStrToTimeByUtc($sqlCurrentTime[0]['currentTime']);
        }
        $serverTime = new \DateTime();
        $differenceBeetweenSqlTimeAndPhp = $sqlCurrentTimeInSeconds - $serverTime->getTimestamp();
        if ($differenceBeetweenSqlTimeAndPhp) {
            $date->sub(new \DateInterval('PT' . $differenceBeetweenSqlTimeAndPhp . 'S'));
        }

        return $date->format('Y-m-d H:i:s');
    }

    public function differenceBeetweenSqlAndUtc()
    {
        $sqlCurrentTime = Yii::$app->getDb()->createCommand(
            'SELECT NOW() as currentTime'
        )->queryAll();
        $sqlCurrentTimeInSeconds = 0;
        if (!empty($sqlCurrentTime[0]) && !empty($sqlCurrentTime[0]['currentTime'])) {
            $sqlCurrentTimeInSeconds =  $this->getStrToTimeByUtc($sqlCurrentTime[0]['currentTime']);
        }
        if (!$sqlCurrentTimeInSeconds) {
            return 0;
        }
        $serverTime = new \DateTime();

        return $sqlCurrentTimeInSeconds - $serverTime->getTimestamp();
    }

    public function getStrToTimeByUtc($date)
    {
        $tz = date_default_timezone_get();
        date_default_timezone_set('UTC');
        $dateInInt = strtotime($date);
        date_default_timezone_set($tz);

        return $dateInInt;
    }

    public function beforeSaveForLocalDateAttributes($insert)
    {
        if ($insert) {
            if ($this->local_created_at) {
                $this->local_created_at = $this->changeDateAttributeToUtcAndSqlTimeZone($this->local_created_at);
            }
            if ($this->local_updated_at) {
                $this->local_updated_at = $this->changeDateAttributeToUtcAndSqlTimeZone($this->local_updated_at);
            }
            if ($this->local_deleted_at) {
                $this->local_deleted_at = $this->changeDateAttributeToUtcAndSqlTimeZone($this->local_deleted_at);
            }
        } else {
            $newAttributes = yii\db\ActiveRecord::getDirtyAttributes();
            if ($newAttributes) {
                if (!empty($newAttributes['local_created_at'])) {
                    $this->local_created_at = $this->changeDateAttributeToUtcAndSqlTimeZone($this->local_created_at);
                }
                if (!empty($newAttributes['local_updated_at'])) {
                    $this->local_updated_at = $this->changeDateAttributeToUtcAndSqlTimeZone($this->local_updated_at);
                }
                if (!empty($newAttributes['local_deleted_at'])) {
                    $this->local_deleted_at = $this->changeDateAttributeToUtcAndSqlTimeZone($this->local_deleted_at);
                }
            }
        }
    }
}
