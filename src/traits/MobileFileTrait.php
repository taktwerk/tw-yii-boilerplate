<?php


namespace taktwerk\yiiboilerplate\traits;
use Yii;

trait MobileFileTrait
{
    public function getFilePathByAttribute($attribute, $options = [], $isPublish = true,$forceCacheUrl = false)
    {
        if (empty($this->$attribute)) {
            return null;
        }
        Yii::$app->urlManager->setBaseUrl('/');
        if($forceCacheUrl){
            $siteUri = $this->getFileUrl($attribute, $options, $isPublish);
        }else{
            $siteUri = !empty($this->getMinFilePath($attribute)) ? 
                    $this->getMinFilePath($attribute) :
                    $this->getFileUrl($attribute, $options, $isPublish);
        }
        Yii::$app->urlManager->setBaseUrl(null);

        return $this->getAbsoluteUrl($siteUri);
    }

    public function getAbsoluteUrl($uri)
    {
        return $uri ?
            Yii::$app->getUrlManager()->createAbsoluteUrl($uri):
            null;
    }

    public function getPreviewFile($field, $options=[])
    {
        return $this->showFilePreview($field, $options, true);
    }
}
