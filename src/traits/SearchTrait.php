<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 5/23/2017
 * Time: 9:01 AM
 */

namespace taktwerk\yiiboilerplate\traits;

use taktwerk\yiiboilerplate\components\Helper;
use taktwerk\yiiboilerplate\modules\userParameter\models\UserParameter;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use Yii;
use taktwerk\yiiboilerplate\components\ClassDispenser;

trait SearchTrait
{
    /**
     * @param $qryString
     * @return array|string
     */
    public function getOperator($qryString)
    {
        $operator = [];
        if (is_array($qryString)) {
            $operator['operator'] = 'OR';
            foreach ($qryString as $operand) {
                $operator['operand'][] = $operand;
            }
        } elseif (strpos(strtolower($qryString), '=0') !== false && strpos(strtolower($qryString), '!=0') === false) {
            $operator = [
                'operator' => 'Empty',
                'operand' => str_replace('=0', '', $qryString),
            ];
        }  elseif (strpos(strtolower($qryString), '!=0') !== false) {
            $operator = [
                'operator' => 'NotEmpty',
                'operand' => str_replace('!=0', '', $qryString),
            ];
        } elseif (strpos($qryString, '<>') === 0) {
            $operator = [
                'operator' => '!=',
                'operand' => str_replace('<>', '', $qryString),
            ];
        } elseif (strpos($qryString, '!=') === 0) {
            $operator = [
                'operator' => '!=',
                'operand' => str_replace('!=', '', $qryString),
            ];
        } elseif (strpos($qryString, '!') === 0) {
            $operator = [
                'operator' => '!=',
                'operand' => str_replace('!', '', $qryString),
            ];
        } elseif (strpos($qryString, '=>') === 0) {
            $operator = [
                'operator' => '>=',
                'operand' => str_replace('=>', '', $qryString),
            ];
        } elseif (strpos($qryString, '>=') === 0) {
            $operator = [
                'operator' => '>=',
                'operand' => str_replace('>=', '', $qryString),
            ];
        } elseif (strpos($qryString, '>') === 0) {
            $operator = [
                'operator' => '>',
                'operand' => str_replace('>', '', $qryString),
            ];
        } elseif (strpos($qryString, '=<') === 0) {
            $operator = [
                'operator' => '<=',
                'operand' => str_replace('=<', '', $qryString),
            ];
        }elseif (strpos($qryString, '==') === 0) {
            $operator = [
                'operator' => '=',
                'operand' => str_replace('==', '', $qryString),
            ];
        }
        elseif (strpos($qryString, '<=') === 0) {
            $operator = [
                'operator' => '<=',
                'operand' => str_replace('<=', '', $qryString),
            ];
        } elseif (strpos($qryString, '<') === 0) {
            $operator = [
                'operator' => '<',
                'operand' => str_replace('<', '', $qryString),
            ];
        } elseif (strpos(strtolower($qryString), ' to ') !== false) {
            $ranges = explode(' to ', $qryString);
            $operator = [
                'operator' => 'between',
                'start' => trim($ranges[0]),
                'end' => trim($ranges[1]),
            ];
        } elseif (strpos(strtolower($qryString), ' between ') !== false) {
            $ranges = explode(' between ', $qryString);
            $operator = [
                'operator' => 'between',
                'start' => trim($ranges[0]),
                'end' => trim($ranges[1]),
            ];
        } elseif (strpos($qryString, '...') !== false) {
            $ranges = explode('...', $qryString);
            $operator = [
                'operator' => 'between',
                'start' => trim($ranges[0]),
                'end' => trim($ranges[1]),
            ];
        } elseif (strpos(strtolower($qryString), ' and ') !== false) {
            $operands = explode(' and ', str_replace([' AND ', ' And '], ' and ', $qryString));
            $operator = [
                'operator' => 'AndLike',
                'first' => trim($operands[0]),
                'second' => trim($operands[1]),
            ];
        } elseif (strpos(strtolower($qryString), ' or ') !== false) {
            $operands = explode(' or ', str_replace([' OR ', ' Or '], ' or ', $qryString));
            $operator = [
                'operator' => 'OrLike',
                'first' => trim($operands[0]),
                'second' => trim($operands[1]),
            ];
        }
        elseif (strpos(strtolower($qryString), 'in= ') !== false) {
            $operands = explode('in= ', str_replace(['IN= ', 'In= ','iN= '], 'in= ', $qryString));
            $operator = [
                'operator' => 'OR',
                'operand' => array_map('trim', explode(',', $operands[1]))
            ];
        }
        else {
            $operator = '';
        }
        return $operator;
    }

    /**
     * @param $attribute
     * @param $query ActiveQuery
     */
    public function applyHashOperator($attribute, &$query, $tableName = null)
    {
        $tableName = $tableName ? $tableName : $this::tableName();
        $operator = $this->getOperator($this->$attribute);
        if (!is_array($operator)) {
            $query->andFilterWhere(
                [
                    $tableName . '.' . $attribute => $this->$attribute
                ]
            );
        } elseif (($operator['operator'] == 'OR')) {
            $query->andFilterWhere(
                [
                    $tableName . '.' . $attribute => $operator['operand']
                ]
            );
        }elseif (($operator['operator'] == 'AndLike')) {
            $query->andFilterWhere(
                [
                    'AND',
                    [
                        'like',
                        $tableName . '.' . $attribute,
                        $operator['first']
                    ],
                    [
                        'like',
                        $tableName . '.' . $attribute,
                        $operator['second']
                    ]
                ]
                );
        } elseif (($operator['operator'] == 'OrLike')) {
            $query->andFilterWhere(
                [
                    'OR',
                    [
                        'like',
                        $tableName . '.' . $attribute,
                        $operator['first']
                    ],
                    [
                        'like',
                        $tableName . '.' . $attribute,
                        $operator['second']
                    ]
                ]
                );
        } elseif (($operator['operator'] !== 'between')) {
            $query->andFilterWhere(
                [
                    $operator['operator'],
                    $tableName . '.' . $attribute,
                    $operator['operand']
                ]
                );
        }
          elseif (($operator['operator'] == 'Empty')) {
            $query->andFilterWhere([
                    'OR',
                    [
                        'is',
                        $tableName . '.' . $attribute,
                        new \yii\db\Expression('null')
                    ],
                    ['=', "LENGTH(".$tableName . '.' . $attribute.")", 0]
                ]
            );
        }  elseif (($operator['operator'] == 'NotEmpty')) {
            $attribute_with_table = $tableName . '.' . $attribute;
            $query->andWhere("$attribute_with_table IS NOT NULL AND TRIM($attribute_with_table) <> ''");
        } else {
            $query->andFilterWhere(
                [
                    $operator['operator'],
                    $tableName . '.' . $attribute,
                    $operator['start'],
                    $operator['end']
                ]
            );
        }
    }

    /**
     * @param $attribute
     * @param $query
     * @param bool $datetime
     * @param bool $isUnixTimestamp
     */
    public function applyDateOperator($attribute, &$query, $datetime = false, $isUnixTimestamp = false)
    {
        if (isset($this->$attribute) && $this->$attribute != '') {
            $format = 'Y-m-d';
            $format .= $datetime ? ' H:i:s' : '';
            $fromFormat = $datetime ?  Yii::$app->formatter->momentJsDateTimeFormat :  Yii::$app->formatter->momentJsDateFormat;
            $date_explode = explode(" TO ", $this->$attribute);
            $date1 = trim($date_explode[0]);
            $convert = \DateTime::createFromFormat(ClassDispenser::getMappedClass(Helper::class)::convertMomentToPHFormat($fromFormat), $date1);
            $date2 = trim($date_explode[1]);
            $convert1 = \DateTime::createFromFormat(ClassDispenser::getMappedClass(Helper::class)::convertMomentToPHFormat($fromFormat), $date2);
            if($convert && $convert1){
            $date1 = $convert->format($format);
            $date2 = $convert1->format($format);

            if($isUnixTimestamp){
                $str = '%Y-%m-%d';
                if($format!='Y-m-d'){
                    $str= '%Y-%m-%d %H:%i:%s';
                }
            $query->andFilterWhere([
                'between',
                "DATE_FORMAT(FROM_UNIXTIME({$attribute}),'$str')",
                $date1,
                $date2
            ]);
          }
          else{
            $query->andFilterWhere(
                [
                    'between',
                    $this::tableName() . '.' . $attribute,
                    $date1,
                    $date2
                ]
                );
          }
            }
        }
    }

    /**
     * @param $attribute
     * @param $query ActiveQuery
     */
    public function applyLikeOperator($attribute, &$query)
    {
        $operator = $this->getOperator($this->$attribute);
        if (!is_array($operator)) {
            $query->andFilterWhere(
                [
                    'like',
                    $this::tableName() . '.' . $attribute,
                    $this->$attribute
                ]
            );
        }elseif (($operator['operator'] == 'OR')) {
            $query->andFilterWhere(
                [
                    $this::tableName() . '.' . $attribute => $operator['operand']
                ]
                );
        }elseif (($operator['operator'] == 'AndLike')) {
            $query->andFilterWhere(
                [
                    'AND',
                    [
                        'like',
                        $this::tableName() . '.' . $attribute,
                        $operator['first']
                    ],
                    [
                        'like',
                        $this::tableName() . '.' . $attribute,
                        $operator['second']
                    ]
                ]
            );
        } elseif (($operator['operator'] == 'OrLike')) {
            $query->andFilterWhere(
                [
                    'OR',
                    [
                        'like',
                        $this::tableName() . '.' . $attribute,
                        $operator['first']
                    ],
                    [
                        'like',
                        $this::tableName() . '.' . $attribute,
                        $operator['second']
                    ]
                ]
            );
        } elseif (in_array($operator['operator'], ['!=','='])) {
            $query->andFilterWhere(
                [
                    $operator['operator'],
                    $this::tableName() . '.' . $attribute,
                    $operator['operand']
                ]
            );
        }  elseif (($operator['operator'] == 'Empty')) {
            $query->andFilterWhere(
                [
                    'OR',
                    [
                        'is',
                        $this::tableName() . '.' . $attribute,
                        new \yii\db\Expression('null')
                    ],
                    ['=', "LENGTH(".$this::tableName() . '.' . $attribute.")", 0]
                ]
            );
        }  elseif (($operator['operator'] == 'NotEmpty')) {
            $attribute_with_table = $this::tableName() . '.' . $attribute;
            $query->andWhere("$attribute_with_table IS NOT NULL AND TRIM($attribute_with_table) <> ''");
        }
    }

    /**
     * @param $model
     * @param $params
     */
    public function parseSearchParams($model, &$params,$formName = null)
    {
        // var_dump($params);
        if (Yii::$app->get('userUi')->get('reset', $model)=="true" || $params['reset'] == 1) {
            Yii::$app->get('userUi')->set('Grid', $model, $params);
            Yii::$app->get('userUi')->set('show_deleted', $model, "0");
            Yii::$app->get('userUi')->set('reset', $model, "false");
        } else {
            $mergeRecursive = false;
            if($formName===null){
                $formName = \Yii::createObject($model)->formName();
            }
            if(isset($params[$formName]) && count($params[$formName])>0){
                    $temp = \Yii::$app->get('userUi')->get('Grid', $model)[$formName];
                    if(is_array($temp) && count($temp)>0){
                        foreach($params[$formName] as $k=>$l){
                            if(strlen($l)>0){
                                $mergeRecursive = true;
                            }
                        }
                    }
            }
            if($mergeRecursive){
                $params = ArrayHelper::merge(Yii::$app->get('userUi')->get('Grid', $model),$params);
            }else{
                $params = array_merge((Yii::$app->get('userUi')->get('Grid', $model) ?: []), $params);
            }
            // unset($params['reset']);
            Yii::$app->get('userUi')->set(
                'Grid',
                $model,
                $params
            );
        }

        $params =  Yii::$app->get('userUi')->enableSave ?  Yii::$app->get('userUi')->get('Grid', $model) : $params;
        
    }

    /**
     * Load sort params from user interface or default order from model
     * @param $model
     * @return array
     */
    public function parseSortParams($model)
    {
//        Yii::$app->get('userUi')->set('Grid', $model, [
//            "sort" => "id",
//            "_pjax"=> "#newsletter-recipient-pjax-container",
//            "pageSize" => "20",
//            "NewsletterRecipient"=> [
//                "email"=> "",
//                "first_name"=> "",
//                "last_name"=> "",
//                "gender"=> "",
//                "is_verified"=> "",
//                "is_subscribed" => "",
//                "user_id"=> "",
//            ],
//            "page"=> "1",
//        ]);
        $value = Yii::$app->get('userUi')->get('Grid', $model)['sort'];
//        echo '<pre>';
//        var_dump(Yii::$app->get('userUi')->get('Grid', $model));die;
        
        if (isset($value)) {
            $attribute =  Yii::$app->get('userUi')->get('Grid', $model)['sort'];
            if (strncmp($attribute, '-', 1) === 0) {
                $order = SORT_DESC;
                $attribute = substr($attribute, 1);
            }
        }
        else {
            
            if(array_key_exists('pk', $this->defaultOrder)) {
                $this->defaultOrder[self::primaryKey()[0]] = $this->defaultOrder['pk'];
                unset($this->defaultOrder['pk']);
            }
            
            return $this->defaultOrder;
        }

        return [$attribute => $order];
    }

    /**
     * @param $model
     * @return int
     */
    public function parsePageParams($model)
    {
        $value = Yii::$app->get('userUi')->get('Grid', $model)['page'];

        if (empty($value)) {
            $request = Yii::$app->request;

            $value = $request->get('page');
        }

        return isset($value) ? $value : 1;
    }

    /**
     * Determine the page size for the grid based on the model
     * @param $model
     * @return int
     */
    public function parsePageSize($model)
    {
        // First try using the user's userUi model
        $value = Yii::$app->get('userUi')->get('Grid', $model)['pageSize'];
        if (Yii::$app->get('userUi')->enableSave && isset($value)) {
            return  Yii::$app->get('userUi')->get('Grid', $model)['pageSize'];
        } elseif (Yii::$app->hasModule('user-parameter') && !empty(UserParameter::get('pageSize'))) {
            // Using the UserParameter module
            return  Yii::$app->request->get('pageSize', UserParameter::get('pageSize'));
        }

        // Default value of 20 if no user or app defined default.
        return 20;
    }
}
