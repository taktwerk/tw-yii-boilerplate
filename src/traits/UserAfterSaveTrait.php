<?php


namespace taktwerk\yiiboilerplate\traits;

use Yii;
use yii\helpers\ArrayHelper;

trait UserAfterSaveTrait
{
    public function newsletterUserAfterSave($insert, $changedAttributes)
    {
        if (!Yii::$app->hasModule('newsletter')) {
            return;
        }
        $newsletterRecipientClass = 'taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient';
        if (!class_exists($newsletterRecipientClass)) {
            return;
        }
        $newsletterRecipientEmail = null;
        if (!$insert && isset($changedAttributes['email'])) {
            $newsletterRecipientEmail = $changedAttributes['email'];
        }
        if ($insert && $this->email) {
            $newsletterRecipientEmail = $this->email;
        }
        if (!$newsletterRecipientEmail) {
            return;
        }
        $newsletterRecipientByEmail = $newsletterRecipientClass::find()
            ->where(['email' => $newsletterRecipientEmail])
            ->one();

        $isNewNewsletterRecipient = !$newsletterRecipientByEmail || !$newsletterRecipientByEmail->id;
        if ($isNewNewsletterRecipient) {
            $newsletterRecipientByEmail = new $newsletterRecipientClass();
            $newsletterRecipientByEmail->is_verified = 0;
            $newsletterRecipientByEmail->is_subscribed = 0;
            $newsletterRecipientByEmail->token = $newsletterRecipientByEmail->generateToken();
        }
        $newsletterRecipientByEmail->email = $this->email;
        $newsletterRecipientByEmail->user_id = $this->id;
        $newsletterRecipientByEmail->save();

        if (!$isNewNewsletterRecipient) {
            return;
        }
        $newsletterTopicClass = 'taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterTopic';
        if (!class_exists($newsletterTopicClass)) {
            return;
        }
        $allNewsletterTopics = $newsletterTopicClass::find()
            ->where(['is_default' => 1])
            ->all();
        $allNewsletterTopicsIds = ArrayHelper::map($allNewsletterTopics, 'id', 'id');
        $newsletterRecipientByEmail->addAllNewsletterTopics($allNewsletterTopicsIds, false);
    }
}
