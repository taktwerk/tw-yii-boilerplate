<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 5/19/2017
 * Time: 12:23 PM
 */
namespace taktwerk\yiiboilerplate\traits;

trait ArHistoryTrait
{
    public static function getPK()
    {
        if (is_array(static::primaryKey())) {
            return static::primaryKey()[0];
        }
        return static::primaryKey();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistory()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\models\ArHistory::class,
            ['row_id' => static::getPK()]
        )
            ->where([
                \taktwerk\yiiboilerplate\models\ArHistory::tableName() . '.table_name' => static::tableName()
            ])
            ->orderBy('created_at DESC');
    }
    public function attributeHistory($attribute,$type='first',$oldValue = true){
        $q = (new \yii\db\Query())
        ->from(\taktwerk\yiiboilerplate\models\ArHistory::tableName())
        ->where([
            'row_id' => $this->getPrimaryKey(),
            'table_name'=> $this->tableName(),
            'field_name' => $attribute
        ]);
        $column = ($oldValue==true)?'old_value':'new_value';
        if($type ==='first'|| $type==='last'){
            $order = $type=='first'?'ASC':'DESC';
            return  $q->select($column)->orderBy('created_at '.$order)->scalar();
        }else{
            return $q->select([$column .' as `value`','created_at'])->orderBy('created_at DESC')->all();
        }
    }
}