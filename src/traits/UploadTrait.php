<?php
namespace taktwerk\yiiboilerplate\traits;

use taktwerk\yiiboilerplate\components\Helper;
use taktwerk\yiiboilerplate\helpers\DebugHelper;
use taktwerk\yiiboilerplate\helpers\FileHelper;
use Yii;
use Exception;
use ReflectionClass;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use yii\db\ActiveRecord;
use yii\helpers\StringHelper;
use yii\db\ColumnSchema;
use yii\helpers\Html;
use taktwerk\yiiboilerplate\components\ImageHelper;
use taktwerk\yiiboilerplate\commands\FileCompressor;
use taktwerk\yiiboilerplate\models\TwActiveRecord;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use yii\helpers\Json;

trait UploadTrait
{
    /**
     *
     * @var string
     */
    protected $uploadPath = '/uploads/';
    public static $compressibleFileTypes = [
        'audio','video','pdf', '3d'
    ];
    //private $
    /**
     * Get the uploaded file url from the cloud storage (for images it gets the processed url)
     *
     * @param
     *            $attribute
     * @param null $options
     * @param bool $publish
     * @return mixed
     * @throws Exception
     */
    public function getFileUrl($attribute, $options = null, $publish = false, $compressFile = true, $ext = 'mp4')
    {
        
        $comments = $this->extractComments($attribute);
        $fileType = $this->getFileType($attribute);
        if($comments && property_exists($comments,'loadImageFromField') && $comments->isAdvanceMode == true && empty($this->$attribute))
        {
            
            $attribute = $comments->loadImageFromField;
            $fileType = $this->getFileType($attribute);
            $isVideo = ($fileType == 'video');
            $isPdf = ($fileType == 'pdf');
            if (($isVideo || $isPdf) && $compressFile) {
                
                $path = $this->getThumbFilePath($attribute);
                
                if(empty($path))
                {
                    if($isVideo)
                    {return '/img/videooverlay.png';}
                    if($isPdf)
                    {
                        return '/img/pdf.png';
                    }
                }
                
                return ClassDispenser::getMappedClass(Helper::class)::getFile("elfinder", "fs", $path);
            }
            
        }

        if (empty($attribute) || ! $this->hasProperty($attribute)) {
            throw new Exception("getFileUrl: Unknown attribute " . $attribute . " on model " . get_class($this));
        }

        if (empty($this->$attribute)) {
            return null;
        }

        $path = $this->getUploadPath() . $this->$attribute;

        $dirPath = $this->getUploadPath();
        $pathInfo = pathinfo($path);
        $fileName = $pathInfo['filename'];
        $isImage = ($fileType == 'image');
        $isVideo = ($fileType == 'video');
        $isAudio = ($fileType == 'audio');
        
         
        $fileMetaAttr = $this->getFileMetaAttribute($attribute);
        if($fileMetaAttr && $this->{$fileMetaAttr}){
            try{
                $meta = @Json::decode($this->{$fileMetaAttr});
                if($meta['etag']){
                    $options['etag'] = $meta['etag'];
                }
                if($meta['lastModified']){
                    $options['lastModified'] = $meta['lastModified'];
                }
            }catch(\Exception $e){
                
            }
        } 
        
        if ($publish && $isImage) {
            return ClassDispenser::getMappedClass(Helper::class)::getImageUrl($path, $options);
        } else if (($isVideo||$isAudio) && $compressFile) {
            $paths = Yii::$app->fs->listContents($dirPath);
            $newFilesArray = [];
            if (! empty($paths)) {
                foreach ($paths as $file) {
                    if(preg_match("/{$fileName}/i", $file['filename']) ){
                        $ext =($isAudio)?(strlen(getenv('compressed_audio_format')>0)?getenv('compressed_audio_format'):'mp3'):$ext;
                        if ($file['extension'] == $ext) {
                            if (strpos($file['basename'], ClassDispenser::getMappedClass(FileCompressor::class)::COMPRESSED_FILE_POSTFIX . "." . $ext) > 0) {
                                $path = $file['path'];
                                break;
                            }
                        }
                    }
                }
            }
        }
        
        return ClassDispenser::getMappedClass(Helper::class)::getFile("elfinder", "fs", $path,$options);
    }

    /**
     * Get the uploaded file path from the cloud storage
     *
     * @param $attribute
     * @param null $options
     * @param bool $publish
     * @return mixed
     * @throws Exception
     */
    public function getFilePath($attribute, $options = null, $compressFile = true, $ext = 'mp4')
    {
        // not directly possible as it doesn't use the compressed yet
        // return $this->getPath($attribute, false);
        $isVideo = ($this->getFileType($attribute) == 'video');

        /* if ($isVideo && $compressFile) {
            $path = $this->getThumbFilePath($attribute);
            if(empty($path))
            {
                return '/img/videooverlay.png';
            }
            return $path;
        } */

        if (empty($attribute) || ! $this->hasProperty($attribute)) {
            throw new Exception("getFileUrl: Unknown attribute " . $attribute . " on model " . get_class($this));
        }

        if (empty($this->$attribute)) {
            return null;
        }

        $path = $this->getUploadPath() . $this->$attribute;

        $dirPath = $this->getUploadPath();
        $pathInfo = pathinfo($path);
        $fileName = $pathInfo['filename'];
        $isImage = ($this->getFileType($attribute) == 'image');
        $isVideo = ($this->getFileType($attribute) == 'video');
        $isAudio = ($this->getFileType($attribute) == 'audio');
        if ($isImage) {
            if ($this->getFileType($attribute) == 'image') {
                return ClassDispenser::getMappedClass(Helper::class)::processImage($path, $options);
            }
        } else if (($isVideo||$isAudio) && $compressFile) {
            $paths = Yii::$app->fs->listContents($dirPath);
            $newFilesArray = [];
            if (! empty($paths)) {
                foreach ($paths as $file) {
                    if(preg_match("/{$fileName}/i", $file['filename']) ){
                        $ext =($isAudio)?(strlen(getenv('compressed_audio_format')>0)?getenv('compressed_audio_format'):'mp3'):$ext;
                        if ($file['extension'] == $ext) {
                            if (strpos($file['basename'], ClassDispenser::getMappedClass(FileCompressor::class)::COMPRESSED_FILE_POSTFIX . "." . $ext) > 0) {
                                $path = $file['path'];
                                break;
                            }
                        }
                    }
                }
            }
        }
        return $path;
    }
    /**
     * Creates a temprary file of the Image and returns the path
     *
     * @param string $path
     *            Path of file in the Flysystem
     * @param array $options
     *            Image options like size
     * @return boolean $getCompressImgPathIfAvailable If true return the compressed Image Path of the original image
     */
    public function getImageTmpPath($attribute, $options = [], $getCompressImgPath = true)
    {
        // get image path of attribute. can also be thumb image or canvas image from video/audio file
        $path = $this->getPath($attribute);

        if($path)
        {
            return ClassDispenser::getMappedClass(ImageHelper::class)::getImageTmpPath($path,$options,$getCompressImgPath);
        }
        return null;
    }

    /**
     * @param $attribute
     * @param bool $getThumbnailforVideo forces return to be image
     * @return mixed|string|null
     * @throws Exception
     */
    public function getPath($attribute,$getThumbnailforVideo=true)
    {
        $canvasAttribute = $this->getCanvasAttribute($attribute);
        if($this->$canvasAttribute==null){
            return null;
        }
        $filePath = $this->getUploadPath() . $this->$canvasAttribute;
        $fileType = $this->getFileType($canvasAttribute);
        if(($fileType == 'video' || $fileType == 'pdf') && $getThumbnailforVideo)
        {
            $filePath = $this->getThumbFilePath($canvasAttribute);
        }
        else if($fileType != 'image' && $getThumbnailforVideo) {
            return null;
        }
        return $filePath;
    }
    public function getCanvasAttribute($column)
    {
        $comment = $this->extractComments($column);
        if ($this->checkIfCanvas($column)) {
            if(empty($this->$column))
            {
                if($comment && !empty($comment->loadImageFromField))
                {
                    $column = $comment->loadImageFromField;
                }
            }
        }else{
            $columnList = \Yii::$app->db->getTableSchema($this->tableName())->columns;
            foreach ($columnList as $col)
            {
                $columnName = $col->name;
                $comment = $this->extractComments($columnName);
                if ($this->checkIfCanvas($columnName))
                {
                    if(!empty($this->$columnName)){
                        $column = $columnName;
                        break;
                    }
                    else if($comment && !empty($comment->loadImageFromField) && $comment->loadImageFromField == $column)
                    {
                        $column = $comment->loadImageFromField;
                        break;
                    }
                }
            }
        }
        return $column;
    }

    /**
     * Get the uploaded video thumbnail url from the cloud storage (for images it gets the processed url)
     *
     * @param
     *            $attribute
     * @param null $options
     */
    public function getThumbFileUrl($attribute,$publish=true,$options=[])
    {
        $path = $this->getThumbFilePath($attribute);

        return $this->getThumbFileUriByPath($path,$publish,$options);
    }

    public function getMinFilePath($attribute)
    {
        $path = $this->getUploadPath() . $this->$attribute;
        $dirPath = $this->getUploadPath();
        $paths = Yii::$app->fs->listContents($dirPath);
        $newFilesArray = [];
        $ext = "mp4";
        $pathInfo = pathinfo($path);
        $fileName = $pathInfo['filename'];
        if (! empty($paths)) {
            if ($this->getFileType($attribute) == '3d') {
                $ext = 'glb';
            } else {
                $ext = ($this->getFileType($attribute) == 'audio') ?
                    (strlen(getenv('compressed_audio_format')>0) ? getenv('compressed_audio_format') : 'mp3') :
                    $ext;
            }
            foreach ($paths as $file) {
                if (preg_match("/{$fileName}/i", $file['filename'])) {
                    if ($file['extension'] == $ext) {
                        if (strpos($file['basename'], ClassDispenser::getMappedClass(FileCompressor::class)::COMPRESSED_FILE_POSTFIX . "." . $ext) > 0) {
                            $path = $file['path'];
                            break;
                        }
                    }
                }
            }
        }
        return ClassDispenser::getMappedClass(Helper::class)::getFile("elfinder", "fs", $path);
    }
    
    public function getThumbFileName($attribute)
    {
        $fileInfo = $this->getThumbFileInfo($attribute);

        return !(empty($fileInfo['basename'])) ? $fileInfo['basename'] : '';
    }

    public function getThumbFilePath($attribute)
    {
        $fileInfo = $this->getThumbFileInfo($attribute);

        return !(empty($fileInfo['path'])) ? $fileInfo['path'] : '';
    }

    public function getThumbFileInfo($attribute)
    {
        if (empty($attribute) || ! $this->hasProperty($attribute)) {
            throw new Exception("getThumbFilePath: Unknown attribute " . $attribute . " on model " . get_class($this));
        }

        if (empty($this->$attribute)) {
            return null;
        }

        $fileInfo = [];
        $path = $this->getUploadPath() . $this->$attribute;
        $dirPath = $this->getUploadPath();
        $pathInfo = pathinfo($path);
        $fileName = $pathInfo['filename'];

        $paths = Yii::$app->fs->listContents($dirPath);
        $newFilesArray = [];
        if (!empty($paths)) {
            foreach ($paths as $file) {
                if (!$this->isCorrectThumbFileInfo($file)) {
                    continue;
                }
                if(preg_match("/{$fileName}/i", $file['filename']) ){
                    if ($file['extension'] == 'jpeg' || $file['extension'] == 'png') {
                        if ((strpos($file['basename'], ClassDispenser::getMappedClass(FileCompressor::class)::COMPRESSED_THUMB_POSTFIX) > 0) || (strpos($file['basename'], ClassDispenser::getMappedClass(FileCompressor::class)::COMPRESSED_PDF_THUMB_POSTFIX) > 0)) {
                            $fileInfo = $file;
                            break;
                        }
                    }
                }
            }
        }

        return $fileInfo;
    }

    protected function isCorrectThumbFileInfo($fileInfo)
    {
        return !(
            empty($fileInfo) ||
            empty($fileInfo['extension']) ||
            empty($fileInfo['basename']) ||
            empty($fileInfo['path'])
        );
    }

    public function getThumbFileUriByPath($path, $publish=true,$options=[])
    {
        if($publish){
            return $path ?ClassDispenser::getMappedClass(Helper::class)::getImageUrl($path,$options):'';
        }
        return $path ? ClassDispenser::getMappedClass(Helper::class)::getFile("elfinder", "fs", $path) : '';
    }


    /**
     * Get file type for sending to upload preview widget
     *
     * @param
     *            $attribute
     * @return string
     * @throws Exception
     */
    public function getFileType($attribute)
    {
        if (empty($attribute) || ! $this->hasProperty($attribute)) {
            throw new Exception("getFileUrl: Unknown attribute " . $attribute . " on model " . get_class($this));
        }
        $path = $this->getUploadPath() . $this->$attribute;
        
        // Check cache
        $cacheKey = md5($path . '_mime');
        $mimeType = Yii::$app->fs_cache->get($cacheKey);
        $mimeType = false;
        if ($mimeType === false) {
            $mimeType = '';
            if (Yii::$app->get('fs')->has($path)) {
                switch (Yii::$app->get('fs')->getMimetype($path)) {
                    case 'application/pdf':
                        $mimeType = 'pdf';
                        break;
                    case 'image/png':
                    case 'image/jpg':
                    case 'image/jpeg':
                    case 'image/webp':
                        $mimeType = 'image';
                        break;
                    case 'video/mp4':
                    case 'video/x-m4v':
                    case 'video/quicktime':
                    case 'video/x-ms-asf':
                    case 'video/x-ms-wmv':
                    case 'video/mpeg':
                    case 'video/ogg':
                    case 'video/3gpp2':
                    case 'video/3gpp':
                    case 'video/webm':
                    case 'video/avi':
                    case 'video/msvideo':
                    case 'video/x-msvideo':
                    case 'video/x-matroska':
                        $mimeType = 'video';
                        break;
                    case 'audio/mpeg':
                    case 'audio/aac':
                    case 'audio/x-m4a':
                    case 'audio/m4a':
                    case 'audio/ogg':
                    case 'audio/wav':
                    case 'audio/x-wav':
                    case 'audio/webm':
                    case 'audio/3gpp':
                    case 'audio/3gpp2':
                    case 'audio/mp4':
                    case 'audio/x-flac':
                    case 'audio/flac':
                        $mimeType = 'audio';
                        break;
                    case 'application/zip':
                        $extension = substr(strrchr($this->$attribute, "."), 1);
                        $fileStream = Yii::$app->get('fs')->readStream($path);
                        $zipFilePath = '';
                        if ($fileStream) {
                            $file = tmpfile();
                            if ($file) {
                                $zipFilePath = stream_get_meta_data($file)['uri'];
                                file_put_contents($zipFilePath, $fileStream);
                            }
                        }
                        if ($zipFilePath && FileHelper::isConsist3DModelZipFile($zipFilePath)) {
                            $mimeType = '3d';
                            break;
                        }
                    default:
                        $extension = substr(strrchr($this->$attribute, "."), 1);

                        if (in_array($extension, ['gltf', 'stp', 'glb'])) {
                            $mimeType = '3d';
                            break;
                        }

                        $mimeType = 'other';
                }
            }

            if ($mimeType != '') {
                // Build dependency key (changed on upload of new file)
                $this->setFileTypeCache($cacheKey, $mimeType, $attribute);
            }
        }
        
        return $mimeType;
    }

    public function getTempFilePath($attribute)
    {
        if (empty($attribute) || ! $this->hasProperty($attribute)) {
            throw new Exception("getFileUrl: Unknown attribute " . $attribute . " on model " . get_class($this));
        }
        $path = $this->getUploadPath() . $this->$attribute;
        $fileStream = Yii::$app->get('fs')->readStream($path);
        if ($fileStream) {
            $file = tmpfile();
            if ($file) {
                $path = stream_get_meta_data($file)['uri'];
                file_put_contents($path, $fileStream);
                // fclose($file);

                return $path;
            }
        }

        return null;
    }

    public function is3dModelFile($attribute)
    {
        return $this->getFileType($attribute) === '3d';
    }

    /**
     * Get file type for sending to upload preview widget
     *
     * @param
     *            $attribute
     * @return string
     * @throws Exception
     */
    public function getFileMimeType($attribute)
    {
        if (empty($attribute) || ! $this->hasProperty($attribute)) {
            throw new Exception("getFileMimeType: Unknown attribute " . $attribute . " on model " . get_class($this));
        }
        $path = $this->getUploadPath() . $this->$attribute;
        $mimeType = '';
        if (Yii::$app->get('fs')->has($path)) {
            $mimeType = Yii::$app->get('fs')->getMimetype($path);
        }
        return $mimeType;
    }

    /**
     * Set the file type cache
     *
     * @param
     *            $cacheKey
     * @param
     *            $mimeType
     * @param
     *            $attribute
     */
    protected function setFileTypeCache($cacheKey, $mimeType, $attribute)
    {
        $path = $this->getUploadPath() . $this->$attribute;
        $dependencyKey = md5($path . '_timestamp');

        Yii::$app->fs_cache->set($cacheKey, $mimeType, 0, new TagDependency([
            'tags' => $dependencyKey
        ]));
    }

    /**
     * Upload an uploaded file to the cloud storage.
     *
     * @param
     *            $attribute
     * @param null $filePath
     * @param string $language
     *            Language to where to lookup in form scope for -1 for deleting
     * @throws Exception
     */
    public function uploadFile($attribute, $filePath, $fileName, $language = null, $isSelfCreatedFile = false,$skipCompression = false)
    {
        $fileName[$attribute] = ClassDispenser::getMappedClass(Helper::class)::cleanFileName($fileName[$attribute]);
        if (empty($attribute) || ! $this->hasAttribute($attribute)) {
            throw new Exception("uploadFile: Unknown attribute " . $attribute . " on model " . get_class($this));
        }
        // No file uploaded
        if (empty($filePath[$attribute])) {
            // Get form scope based on language if passed as argument, so we know where to lookup if file is removed
            $formScope = $language === null ?
                \Yii::$app->request->post($this->formName()) :
                \Yii::$app->request->post($this->formName())[$language][$this->formName()];
            if ($formScope[$attribute] == - 1) {
                $this->deleteFile($attribute);
                $this->$attribute = null;
                $fileMetaAttr = $this->getFileMetaAttribute($attribute);
                if($fileMetaAttr){
                    $this->{$fileMetaAttr}='';
                }
                $this->save(false);
            }
            return false;
        }

        // Make sure the file was uploaded.
        if ($isSelfCreatedFile == false && ! is_uploaded_file($filePath[$attribute])) {

            DebugHelper::dd($filePath);
            return false; // nothing to change.
        }

        // Add the time() to the file for properly refreshing the cache in case the same file is
        // uploaded again but with some minor corrections in it. This also handles the mobile app sync to trigger a new
        // download of the file.
        $filePrefix = time() . '_';
        
        $ext = (new \SplFileInfo($fileName[$attribute]))->getExtension();
        $baseFileName = $fileName[$attribute];
        
        /*Checking if filename contains timestamp already if yes then the timestamp is removed*/
        if($baseFileName){
            $checkNumeric = explode('_', $baseFileName, 2)[0];
            if(is_numeric($checkNumeric)){
                $tmp =  explode('_', $baseFileName, 2)[1];
                if($tmp && $tmp!='.'.$ext){
                    $baseFileName = $tmp;
                }
            }
        }
        /*Limiting the file name to be around 100 characters in length*/
        if(strlen($baseFileName)>100){
            $baseFileName = substr($baseFileName, 0, 100).'.'.$ext;
        }
        $newFileName = $filePrefix. $baseFileName;
        //$newFileName = $filePrefix.\Yii::$app->getSecurity()->generateRandomString(12).'.'.$ext; //To upload a file with random name 
        
        // Get the local path to move the file from tmp status
        @mkdir(Yii::getAlias('@runtime') . '/upload');
        $newPath = Yii::getAlias('@runtime') . '/upload/' . $filePrefix . $newFileName;
        if ($isSelfCreatedFile == false) {
            move_uploaded_file($filePath[$attribute], $newPath);
        } else {
            // prd($filePath[$attribute]);
            copy($filePath[$attribute], $newPath);
        }
        /* prd($filePath[$attribute]); */
        // Write the file to the filesystem.

        $stream = fopen($newPath, 'r+');

        Yii::$app->fs->putStream($this->getUploadPath() . $newFileName, $stream);

        // If the new file doesn't equal the old one, delete the old one.
        if (! empty($this->$attribute) && $this->$attribute != $fileName[$attribute]) {
            $this->deleteFile($attribute);
            if($this->checkIfCanvas($attribute) == false)
            {
                $this->deleteCanvasFile();
            }
        }
        $fileMetaAttr = $this->getFileMetaAttribute($attribute);
        if($fileMetaAttr){
            $this->setFileMeta($fileMetaAttr,$this->getUploadPath() . $newFileName,$newPath);
        }
        \yii\helpers\FileHelper::unlink($newPath);
        // Invalidate related tags
        $path = $this->getUploadPath() .$newFileName;
        $cacheKey = md5($path . '_timestamp');
        TagDependency::invalidate(Yii::$app->getCache(), $cacheKey);
        Yii::$app->fs_cache->delete($cacheKey);
        
        // Precache mimetype and timestamp
        ClassDispenser::getMappedClass(Helper::class)::getFileCachedModificationTime($cacheKey, $path);

        // Save the changes.
        $this->$attribute = $newFileName;
        $this->save(false);
        if($skipCompression==false && getenv('ENABLE_FILE_COMPRESSION')) {
            $type = $this->getFileType($attribute);
        
            if (in_array($type, self::$compressibleFileTypes)) {
                $checkQueue = $this->findCompressQueueJobModel($this, $attribute);
                if (empty($checkQueue)) {
                    $this->compressFile($this, $attribute);
                }
            } else {
                $checkQueue = $this->findCompressQueueJobModel($this, $attribute);
                if (!empty($checkQueue)) {
                    $this->deleteCompressQueueJobModel($this, $attribute);
                }
            }
        }
        return true;
    }
    public function setFileMeta($fileMetaAttr,$filePathOnFS,$filePathOnLocal){
        $lastMTime = Yii::$app->get('fs')->getTimestamp($filePathOnFS);
        $etag = '';
        $algo = 'md5';
        if (!in_array($algo, hash_algos())) {
            throw new \InvalidArgumentException('Hash algorithm ' . $algo . ' is not supported');
        }
        $exifData = @exif_read_data($filePathOnLocal);
        
        $stream = \Yii::$app->fs->readStream($filePathOnFS);
        if ($stream !== false) {
            $hc = hash_init($algo);
            hash_update_stream($hc, $stream);
            $etag = hash_final($hc);
        }
        
        $data = [
           'mimetype' => Yii::$app->fs->getMimetype($filePathOnFS),
           'etag' => $etag,
           'lastModified' => gmdate("D, d M Y H:i:s", $lastMTime)." GMT",
        ];
        $isSameEtag = false;
        try{
            if($this->{$fileMetaAttr}){
                $oldJson = Json::decode($this->{$fileMetaAttr});
                if(isset($oldJson['etag']) && $etag && $oldJson['etag']==$etag){
                    $isSameEtag = true;
                }
            }
        }catch(\Exception $e){
        }
        if($exifData && $isSameEtag==false){
            try{
            //Making sure that exifdata is json encodable otherwise not saving that detail in the json.
                Json::encode($exifData);
                $data['exif'] = $exifData;
            }catch(\Exception $e){
                
            }
        }
        if($isSameEtag==false){
            $this->{$fileMetaAttr} = Json::encode($data);
        }
    }
    public function getFileMetaAttribute($fileAttr){
        $columns = \Yii::$app->db->getTableSchema($this->tableName())->getColumnNames();
        foreach ($columns as $column)
        {
            $comment = $this->extractComments($column);
            if(($fileAttr.'_filemeta'==$column) || ($comment && (@$comment->inputtype === 'filemeta') && (@$comment->related_attribute === $fileAttr))) 
            {
                return $column;
            }
        }
        return null;
    }
    public function uploadByUrl($attribute, $url, $fileName)
    {
        $prictureStream = fopen($url, 'r');
        if ($prictureStream) {
            $file = tmpfile();
            if ($file) {
                $path = stream_get_meta_data($file)['uri'];
                file_put_contents($path, $prictureStream);
                $this->uploadFile(
                    $attribute,
                    [$attribute => $path],
                    [$attribute => $fileName],
                    null,
                    true
                );
                fclose($file);
            }
        }
    }

    /**
     * Delete previous file of model from server
     */
    public function deleteCanvasFile()
    {
        $columns = \Yii::$app->db->getTableSchema($this->tableName())->getColumnNames();
        foreach ($columns as $column)
        {
            $comment = $this->extractComments($column);
            if ((preg_match('/(_canvas)$/i', $column) || ($comment && (@$comment->inputtype === 'canvas')))  && ($comment && (@$comment->isAdvanceMode == true))) {
                $this->deleteFile($column);
                $this->$column = '';
                $metaColumn = $column."_meta";
                if(isset($this->$metaColumn))
                {
                    $this->$metaColumn = '';
                }
            }
        }
    }


    public function compressFile($model, $attribute)
    {
        return $this->createCompressQueueJobModel($model, $attribute);
    }

    /**
     *
     * @param
     *            $model
     * @return QueueJob|null
     */
    public function findCompressQueueJobModel(ActiveRecord $model, $attribute)
    {
        $query = QueueJob::findOne([
            'command' => ClassDispenser::getMappedClass(FileCompressor::class),
            'parameters' => json_encode([
                'model' => get_class($model),
                'id' => $model->getPrimaryKey(),
                'attribute' => $attribute
            ]),
            'status' => QueueJob::STATUS_QUEUED
        ]);

        if (($model = $query) !== null) {
            return $model;
        }

        return null;
    }

    public function deleteCompressQueueJobModel(TwActiveRecord $model, $attribute)
    {
        try {
            $queueJobModel = QueueJob::findOne([
                'command' => ClassDispenser::getMappedClass(FileCompressor::class),
                'parameters' => json_encode([
                    'model' => get_class($model),
                    'id' => $model->getPrimaryKey(),
                    'attribute' => $attribute
                ]),
                'status' => QueueJob::STATUS_QUEUED
            ]);
            
            if ($queueJobModel !== null) {
                $queueJobModel->delete();
            }
        } catch (Exception $e) {
            // If the file couldn't be remove, silence the error.
        }
    }

    /**
     *
     * @param $model TwActiveRecord
     * @return mixed
     */
    public function createCompressQueueJobModel(TwActiveRecord $model, $attribute)
    {
        $job = new QueueJob();
        $modelTitle =\yii\helpers\StringHelper::basename(get_class($model));
        return $job->queue("Convert File for ".$modelTitle." ".$model->id, ClassDispenser::getMappedClass(FileCompressor::class), [
            'model' => get_class($model),
            'id' => $model->getPrimaryKey(),
            'attribute' => $attribute
        ], true);
    }

    /**
     * Get unique upload path
     *
     * @return string
     */
    public function getUploadPath()
    {
        $reflect = new ReflectionClass($this);
        return $this->uploadPath . $reflect->getShortName() . '/' . $this->id . '/';
    }

    /**
     * Delete previous file of model from server
     */
    protected function deleteFile($attribute)
    {
        try {

            // If it has a assetsprod cache file, delete that too.
            $originalFilePath = $this->getUploadPath() . $this->$attribute;
            if (! empty(pathinfo($originalFilePath))) {
               if(\Yii::$app->fs->getMetadata($originalFilePath)['type']=='dir'){
                    return;
               }
                
                $originalFileName = pathinfo($originalFilePath)['filename'];
                $dirPath = $this->getUploadPath();
                $paths = Yii::$app->fs->listContents($dirPath);
                $newFilesArray = [];
                if (! empty($paths)) {
                    foreach ($paths as $file) {
                        if (StringHelper::startsWith($file['basename'], $originalFileName)) {
                            Yii::$app->fs->delete($file['path']);
                        }
                    }
                }
            }

            $cache_path = ClassDispenser::getMappedClass(FileHelper::class)::fileCachePath($originalFilePath);
            if (Yii::$app->fs_assetsprod->has($cache_path)) {
                Yii::$app->fs_assetsprod->delete($cache_path);
            }

        } catch (Exception $e) {
            // If the file couldn't be remove, silence the error.
        }
    }

    /**
     * @param bool $is_canvas
     * @return array
     */
    public function getUploadFields($is_canvas=false)
    {
        $crudGenerator = new \taktwerk\yiiboilerplate\templates\crud\Generator();
        $safeAttributes = Yii::$app->db->getTableSchema($this->tableName())->columnNames;
        $upload_fields = [];
        foreach ($safeAttributes as $attribute) {
            $column = ArrayHelper::getValue(Yii::$app->db->getTableSchema($this->tableName())->columns, $attribute);
            if($is_canvas){
                if (($crudGenerator->checkIfCanvas($column)) && $column->allowNull) {
                    $upload_fields[] = $attribute;
                }
            }
            if ($crudGenerator->checkIfUploaded($column) && $column->allowNull) {
                $upload_fields[] = $attribute;
            }
        }
        return array_unique($upload_fields);
    }
}
