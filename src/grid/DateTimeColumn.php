<?php
namespace taktwerk\yiiboilerplate\grid;

use yii\base\InvalidConfigException;
use Yii;
use yii\helpers\ArrayHelper;

class DateTimeColumn extends \kartik\grid\DataColumn
{

    public $filterType;

    public $format = 'datetime';

    public $filterWidgetPluginEvents = [];

    public $filterWidgetPluginOptions = [];

    public function init()
    {
        $this->filterType = GridView::FILTER_DATE_RANGE;
        
        if (! in_array($this->format, [
            'date',
            'datetime'
        ])) {
            throw new InvalidConfigException(Yii::t('twbp', 'format can be only date or datetime'));
        }
        

        $format = $this->format == 'datetime' ? Yii::$app->formatter->momentJsDateTimeFormat : Yii::$app->formatter->momentJsDateFormat;
        
        $filterWidgetPluginEvents =  [
            'apply.daterangepicker' => 'function(ev, picker) {
                    if($(this).val() == "") {
                        $(this).val(picker.startDate.format(picker.locale.format) + picker.locale.separator +
                        picker.endDate.format(picker.locale.format)).trigger("change");
                    }
                }',
            'show.daterangepicker' => 'function(ev, picker) {
                    picker.container.find(".ranges").off("mouseenter.daterangepicker", "li");
                    if($(this).val() == "") {
                        picker.container.find(".ranges .active").removeClass("active");
                    }
                }',
            'cancel.daterangepicker' => 'function(ev, picker) {
                    if($(this).val() != "") {
                        $(this).val("").trigger("change");
                    }
                }'
        ];
        $this->filterWidgetPluginEvents = ArrayHelper::merge($filterWidgetPluginEvents, $this->filterWidgetPluginEvents);
        $filterWidgetPluginOptions = [
            'opens'=>'left',
            'locale' => [
                'format' => $format,
                'separator' => ' TO ',
                'cancelLabel'=>'Clear'
            ]
        ];
        $this->filterWidgetPluginOptions = ArrayHelper::merge($filterWidgetPluginOptions, $this->filterWidgetPluginOptions);
        $filterWidgetOptions = [
            'pluginEvents' => $this->filterWidgetPluginEvents,
            'pluginOptions' => $this->filterWidgetPluginOptions
        ];
        $this->filterWidgetOptions = ArrayHelper::merge($filterWidgetOptions, $this->filterWidgetOptions);
        parent::init();
    }
}
