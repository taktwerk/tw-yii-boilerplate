<?php
namespace taktwerk\yiiboilerplate\grid;

use Closure;
use Yii;
use taktwerk\yiiboilerplate\assets\TwAsset;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use yii\grid\GridViewAsset;
use yii\helpers\Html;

class SortableGridView extends GridView
{

    /**
     * (required) The URL of related SortableAction
     *
     * @see \richardfan1126\sortable\SortableAction
     * @var string
     */
    public $sortUrl;

    /**
     * (optional) The text shown in the model while the server is reordering model
     * You can use HTML tag in this attribute.
     *
     * @var string
     */
    public $sortingPromptText = 'Loading...';

    /**
     * (optional) The text shown in alert box when sorting failed.
     *
     * @var string
     */
    public $failText = 'Fail to sort';

    public function init()
    {
        parent::init();
        if (! isset($this->sortUrl)) {
            throw new InvalidConfigException("You must specify the sortUrl");
        }
        $this->setDefaults();
        GridViewAsset::register($this->view);
        TwAsset::register($this->view);
        $this->tableOptions['class'] .= ' sortable-grid-view';
    }

    public function setDefaults()
    {
        $options = [
            'sortingPromptText' => 'Loading...',
            'failText' => 'Fail to sort'
        ];
        $this->options = array_merge($options, $this->options);
    }

    /**
     *
     * {@inheritdoc}
     * @see \yii\grid\GridView::renderTableRow()
     */
    public function renderTableRow($model, $key, $index)
    {
        $cells = [];
        /* @var $column Column */
        foreach ($this->columns as $column) {
            $cells[] = $column->renderDataCell($model, $key, $index);
        }
        if ($this->rowOptions instanceof Closure) {
            $options = call_user_func($this->rowOptions, $model, $key, $index, $this);
        } else {
            $options = $this->rowOptions;
        }
        // $options['id'] = "items[]_{$model->primaryKey}";
        $options['data-key'] = is_array($key) ? json_encode($key) : (string) $key;
        return Html::tag('tr', implode('', $cells), $options);
    }

    public function run()
    {
        if (! empty($this->columns)) {
            foreach ($this->columns as $column) {
                if (property_exists($column, 'enableSorting')) {
                    $column->enableSorting = false;
                }
            }
        }
        parent::run();
        $pjaxContainerId = '';
        if ($this->pjax == true) {
            $pjaxContainerId = $this->pjaxSettings['options']['id'];
        }
        $options = [
            'id' => ! empty($this->options['id']) ? $this->options['id'] : $this->id,
            'action' => $this->sortUrl,
            'sortingPromptText' => Yii::t('twbp', $this->sortingPromptText),
            'sortingFailText' => Yii::t('twbp', $this->failText),
            'csrfTokenName' => Yii::$app->request->csrfParam,
            'csrfToken' => Yii::$app->request->csrfToken,
            'reloadOnSort' => $this->pjax,
            'pjaxContainerId' => $pjaxContainerId,
            'handler' => '.sortable-row'
        ];
        $options = Json::encode($options);
        $js = '';
        if ($this->pjax == true && $pjaxContainerId) {
            $js = <<<JS
        $("#{$pjaxContainerId}").on("pjax:end", function() {
            jQuery('{$pjaxContainerId} .sortable-grid-view tbody').sortable('destroy');
            jQuery.SortableGridView({$options});});
JS;
        }
        $this->view->registerJs("jQuery.SortableGridView($options);{$js}");
    }
}
