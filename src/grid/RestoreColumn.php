<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace taktwerk\yiiboilerplate\grid;

use taktwerk\yiiboilerplate\models\TwActiveRecord;
use taktwerk\yiiboilerplate\widget\Select2;
use Yii;
use yii\grid\DataColumn as DataColumn;
use yii\helpers\Html;

class RestoreColumn extends DataColumn
{

    public $pjaxContainer = '';

    public $filter;

    public $model;

    public function init()
    {
        /**@var $model TwActiveRecord*/
        $model = $this->model;
        $get_param = Yii::$app->get('userUi')->get('show_deleted', $model::className());

        $this->visible = $get_param==="1";
        $this->attribute = 'is_deleted';
        $this->header = Yii::t('twbp','Deleted');

        $this->value = function($model_grid){
            return $model_grid->restorable()?Yii::t('twbp','Yes'):Yii::t('twbp','No');
        };

        $reflection = new \ReflectionClass($model);
        $shortModelName = $reflection->getShortName();

        if((Yii::$app->get('userUi')->get('Grid', $model))["$shortModelName"]['is_deleted']){
            $value = (Yii::$app->get('userUi')->get('Grid', $model))["$shortModelName"]['is_deleted'];
        }else{
            $value = Yii::$app->request->get("{$shortModelName}")['is_deleted'];
        }

        $this->filter = Select2::widget([
                'pjaxContainerId' => $this->pjaxContainer,
                'data' => [ 'yes' => Yii::t('twbp','Yes'), 'no' =>Yii::t('twbp','No')],
                'value'=> $value,
                'name' => "{$shortModelName}[is_deleted]",
                'id'=> "{$shortModelName}-is_deleted-grid",
                'options' => [
                    'placeholder' => Yii::t('twbp', 'Select'),
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'multiple' => false,
                ],
            ]);

        parent::init();
    }

}
