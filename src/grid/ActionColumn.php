<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace taktwerk\yiiboilerplate\grid;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn as AC;
use yii\base\BaseObject;

/**
 * ActionColumn is a column for the [[GridView]] widget that displays buttons for viewing and manipulating the items.
 *
 * To add an ActionColumn to the gridview, add it to the [[GridView::columns|columns]] configuration as follows:
 *
 * ```php
 * 'columns' => [
 * // ...
 * [
 * 'class' => ActionColumn::className(),
 * // you may configure additional properties here
 * ],
 * ]
 * ```
 *
 * For more details and usage information on ActionColumn, see the [guide article on data widgets](guide:output-data-widgets).
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ActionColumn extends AC
{

    public $useModal = false;

    public $modalId = 'modalForm';

    public $contentOptions = [
        'nowrap' => 'nowrap',
        'align' => 'center'
    ];

    public $headerOptions = [
        'class' => 'action-column'
    ];

    public $buttonAsDropDown = true;

    public function __construct($config = [])
    {
        $this->setDefaults();
        parent::__construct($config);
    }

    public function init()
    {
        $this->setTemplate();
        parent::init();

    }
    /**
     * Initializes the default button rendering callbacks.
     */
    protected function initDefaultButtons()
    {
        parent::initDefaultButtons();
        $this->initDefaultButton('recompress', 'compressed', [
            'data-method' => 'post',
            'title' => Yii::t('yii', 'Adds a compression job for this item\'s files'),
        ]);
    }
    public function setTemplate()
    {
        if ($this->buttonAsDropDown) {
            preg_match_all('/\\{([\w\-\/]+)\\}/', $this->template, $matches);
            $arrHtml = [];
            
            foreach($matches[1] as $key=>$action){
                $cls = '';
                if($action == 'delete'){
                    $arrHtml[$key-1]['cls'] = ' class="grid-act-divide" ';
                    if(count($matches[1]) > 3) { // hide divider after delete, if only view, edit, delete (no customActions)
                        $cls = ' class="grid-act-divide" ';
                    }
                }
                $arrHtml[] = ['action' => $action, 'cls' => $cls];
            }
            $listHtml = '';
            foreach ($arrHtml as $node) {
                $cls = $node['cls'];
                $action = $node['action'];
                $listHtml = $listHtml . "<li$cls>{{$action}}</li>";
            }
            $this->template = '<div class="dropdown-action">
            <button class="btn-act-dot btn btn-default dropdown-toggle" type="button" href="javascript:" data-toggle="dropdown"><i class="glyphicon glyphicon-option-vertical"></i>
            </button>
            <ul class="dropdown-action-menu dropdown-menu cus_drop">' . $listHtml . '</ul>
  </div>';

        }
    }

    protected function setDefaults()
    {
        $this->template = '<div class="dropdown-action">
        <button class="btn-act-dot btn btn-default dropdown-toggle" type="button" href="javascript:" data-toggle="dropdown"><i class="glyphicon glyphicon-option-vertical"></i>
        </button>
        <ul class="dropdown-action-menu dropdown-menu cus_drop">' . (Yii::$app->getUser()
                ->can(Yii::$app->controller->module->id . '_' . Yii::$app->controller->id . '_view') ? '<li>{view}</li>' : '')
                . ' ' .
                (Yii::$app->getUser()->can(Yii::$app->controller->module->id .
                    '_' . Yii::$app->controller->id . '_update') ? '<li class="grid-act-divide">{update}</li>' : '')
                    . ' ' .
                    (Yii::$app->getUser()
                        ->can(Yii::$app->controller->module->id . '_' . Yii::$app->controller->id . '_recompress') ? '<li>{recompress}</li>' : '')
                    . ' ' . (Yii::$app->getUser()->can(Yii::$app->controller->module->id . '_'
                        . Yii::$app->controller->id . '_delete') ? '<li>{delete}</li>' : '')
                        . ' ' . (! empty(Yii::$app->controller->customActions) ? '{'
                            . implode('} {', array_keys(Yii::$app->controller->customActions))
                            . '}' : null) . '</ul>
  </div>';
        $this->buttons = \Yii::$app->controller->customActions;

        $this->urlCreator = function ($action, $model, $key, $index) {
            /**
             *
             * @var \yii\db\ActiveRecord $model
             */
            // using the column name as key, not mapping to 'id' like the standard generator
            $params = is_array($key) ? $key : [
                $model->primaryKey()[0] => (string) $key
            ];
            $params[0] = Yii::$app->controller->id ? Yii::$app->controller->id . '/' . $action : $action;
            if($model->hasMethod('restorable') && $model->restorable() && property_exists(Yii::$app->controller, "model") && Yii::$app->get('userUi')->get('show_deleted', Yii::$app->controller->model) === "1"){
                $params['show_deleted'] = "1";
            }
            return Url::toRoute($params);
        };
        $this->visibleButtons = [
            'view' => function ($model, $key, $index) {
                return ($model->hasMethod('readable')) ? $model->readable() : false;
            },
            'update' => function ($model, $key, $index) {
                return ($model->hasMethod('editable')) ? $model->editable() : false;
            },
            'delete' => function ($model, $key, $index) {
                return ($model->hasMethod('deletable')) ? $model->deletable() : false;
            },
            'recompress' =>function ($model, $key, $index) {
                if($model->hasMethod('getUploadFields')){
                    $fields = $model->getUploadFields();
                    $isCompressible = false;
                    $compressibleFileTypes = $model::$compressibleFileTypes;
                    foreach($fields as $field){
                        if(in_array($model->getFileType($field),$compressibleFileTypes)){
                            $isCompressible = true;
                            break;
                        }
                    }
                    
                    return $isCompressible;
                }
            }
        ];

    }

    protected function initDefaultButton($name, $iconName, $additionalOptions = [])
    {
        if (! isset($this->buttons[$name]) && strpos($this->template, '{' . $name . '}') !== false) {
            $this->buttons[$name] = function ($url, $model, $key) use ($name, $iconName, $additionalOptions) {
                switch ($name) {
                    case 'view':
                        $title = Yii::t('yii', 'View');
                        if ($this->useModal) {
                            $this->buttonOptions = array_merge([
                                'data-toggle' => 'modal',
                                'data-url' => $url,
                                'data-pjax' => 1
                            ], $additionalOptions, $this->buttonOptions);
                            $url = '#' . $this->modalId;
                        }
                        break;
                    case 'update':
                        $title = Yii::t('yii', 'Update');
                        if ($this->useModal) {
                            $this->buttonOptions = array_merge([
                                'data-toggle' => 'modal',
                                'data-url' => $url,
                                'data-pjax' => 1
                            ], $additionalOptions, $this->buttonOptions);
                            $url = '#' . $this->modalId;
                        }
                        break;
                    case 'recompress':
                        $title = Yii::t('yii', 'Recompress');
                        if ($this->useModal) {
                            $this->buttonOptions = array_merge([
                                'data-toggle' => 'modal',
                                'data-url' => $url,
                                'data-pjax' => 1
                            ], $additionalOptions, $this->buttonOptions);
                            $url = '#' . $this->modalId;
                        }
                        break;
                    case 'delete':
                        if($model->hasMethod('restorable') && $model->restorable()){
                            $title = Yii::t('twbp', 'Restore');
                            $additionalOptions['data-confirm'] = Yii::t('twbp','Are you sure you want to restore this item?');
                        }else{
                            $title = Yii::t('yii', 'Delete');
                        }
                        if ($this->useModal) {
                            $this->buttonOptions = array_merge([
                                'data-url' => $url,
                                'data-pjax' => 1,
                                'class' => 'ajaxDelete'
                            ], $additionalOptions, $this->buttonOptions);
                        }
                        break;
                    default:
                        $title = ucfirst($name);
                }
                $options = array_merge([
                    'title' => $title,
                    'aria-label' => $title,
                    'data-pjax' => '0'
                ], $additionalOptions, $this->buttonOptions);
                $icon = Html::tag('span', '', [
                        'class' => "glyphicon glyphicon-$iconName"
                    ]) . ' ' . $title;
                return Html::a($icon, $url, $options);
            };
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $actions = [];
        preg_match_all('/\\{([\w\-\/]+)\\}/', $this->template, $matches);
        foreach ($matches[1] as $name){
            if (isset($this->visibleButtons[$name])) {
                $isVisible = $this->visibleButtons[$name] instanceof \Closure
                    ? call_user_func($this->visibleButtons[$name], $model, $key, $index)
                    : $this->visibleButtons[$name];
            } else {
                $isVisible = true;
            }

            if ($isVisible && isset($this->buttons[$name])) {
                $url = $this->createUrl($name, $model, $key, $index);
                if(call_user_func($this->buttons[$name], $url, $model, $key)){
                   $actions[] = $name;
                }
            }
        }

        if(count($actions)==1){
            $name = $actions[0];
            $url = $this->createUrl($name, $model, $key, $index);
            $buttons = explode('</span',call_user_func($this->buttons[$name], $url, $model, $key))[0];
            return '<span class="btn btn-default btn-single-action">'.$buttons.'</span>';
        }else{
            return preg_replace_callback('/\\{([\w\-\/]+)\\}/', function ($matches) use ($model, $key, $index) {
                $name = $matches[1];
                if($model->hasAttribute('deleted_at') && $model->deleted_at!=null && !in_array($name,['view','delete'])){
                    return '';
                }
                if (isset($this->visibleButtons[$name])) {
                    $isVisible = $this->visibleButtons[$name] instanceof \Closure
                        ? call_user_func($this->visibleButtons[$name], $model, $key, $index)
                        : $this->visibleButtons[$name];
                } else {
                    $isVisible = true;
                }

                if ($isVisible && isset($this->buttons[$name])) {
                    $url = $this->createUrl($name, $model, $key, $index);
                    return call_user_func($this->buttons[$name], $url, $model, $key);
                }

                return '';
            }, $this->template);
        }
    }
}
