<?php

namespace taktwerk\yiiboilerplate\enums;

class AppConfigurationModeEnum
{
    const ONLY_CONFIGURE = 0;
    const CONFIGURE_AND_DEVICE_LOGIN = 1;
    const CONFIGURE_AND_USER_LOGIN = 2;
    const CONFIGURE_AND_LOGIN_BY_CLIENT = 3;
}
