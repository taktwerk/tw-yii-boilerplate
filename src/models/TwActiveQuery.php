<?php
/**
 * Created by PhpStorm.
 * User: Jeremy
 * Date: 25/01/2017
 * Time: 08:40
 */

namespace taktwerk\yiiboilerplate\models;

use Illuminate\Support\Facades\Date;
use taktwerk\yiiboilerplate\helpers\DebugHelper;
use yii\db\ActiveQuery;
use DateTime;
use Yii;
use yii\db\Expression;

/**
 * Class TwActiveQuery
 * Use to set default scopes for all models
 *
 * @package taktwerk\yiiboilerplate\models
 */
class TwActiveQuery extends ActiveQuery
{
    /**
     * Get all models who's timestamp has been updated since a date.
     * Models who don't have the timestamp behavior will need to override this method in the Query class.
     * @param $updatedAt
     * @return mixed
     */
    public function updated(DateTime $updatedAt = null, DateTime $toDate = null)
    {
        if (!empty($updatedAt)) {
            $dateQuery = $this->andWhere(
                ['>=', $this->getPrimaryTableName() . '.updated_at', $updatedAt->format('Y-m-d H:i:s')]
            );
            if ($toDate) {
                $dateQuery = $dateQuery->andWhere(
                    ['<', $this->getPrimaryTableName() . '.updated_at', $toDate->format('Y-m-d H:i:s')]
                );
            }

            return $dateQuery;
        }
        // If we don't have a datetime, send everything, it's probably the first sync but don't send deleted items
        $this->andWhere([$this->getPrimaryTableName() . '.deleted_at' => null]);

        return $this;
    }

    /**
     * Override this method in the Query class to define special filters for the sync process.
     * For example, limiting the results to the current logged in user
     * @return $this
     */
    public function api()
    {
        return $this;
    }

    /**
     * Scope on the user Id.
     * @param $userId
     * @return mixed
     */
    public function user($userId)
    {
        return $this->andWhere([$this->getPrimaryTableName() . '.user_id' => $userId]);
    }

    /**
     * Scope on the user ids
     * @param $userIds
     * @return mixed
     */
    public function users($userIds)
    {
        return $this->andWhere([$this->getPrimaryTableName() . '.user_id' => $userIds]);
    }

    /**
     * Ask for elements edited during the last two days.
     * @return $this
     */
    public function recent()
    {
        $date = new DateTime();
        $date->modify('2 days ago');
        return $this->andWhere(['>=', $this->getPrimaryTableName() . '.updated_at', $date->format('Y-m-d 00:00:00')]);
    }

    /**
     * Sets the WHERE part of the query.
     *
     * The method requires a `$condition` parameter, and optionally a `$params` parameter
     * specifying the values to be bound to the query.
     *
     * The `$condition` parameter should be either a string (e.g. `'id=1'`) or an array.
     *
     * @inheritdoc
     *
     * @param string|array|Expression $condition the conditions that should be put in the WHERE part.
     * @param array $params the parameters (name => value) to be bound to the query.
     * @return $this the query object itself
     * @see andWhere()
     * @see orWhere()
     * @see QueryInterface::where()
     */
    public function where($condition, $params = [])
    {
        return $this->andWhere($condition, $params);
    }

    /**
     * Get all entries related to client including system client
     * @param bool $removedDeleted
     * @param string $tableName
     * @return \taktwerk\yiiboilerplate\models\TwActiveQuery|\yii\db\ActiveQuery
     */
    public function findWithSystemEntry($tableName, $removedDeleted = true){

        $model = $this->findWithOutSystemEntry($tableName, $removedDeleted);

        if (array_key_exists('REQUEST_METHOD', $_SERVER) && !Yii::$app->user->can('Authority') && \Yii::$app->hasModule('customer')) {
            //var $used_with_other_query is used to add a "OR" condition to get all the system entries additionally
            $model->findSystemEntry($tableName, $used_with_other_query=true, $removedDeleted);
        }
        return $model;
    }

    /**
     * Get all entries of the model related to system client
     * @param bool $removedDeleted
     * @param bool $used_with_other_query is used to add a "OR" condition to get all the system entries additionally
     * @param string $tableName
     * @return \taktwerk\yiiboilerplate\models\TwActiveQuery|\yii\db\ActiveQuery
     */
    public function findSystemEntry($tableName, $used_with_other_query=false, $removedDeleted = true){

        if (\Yii::$app->hasModule('customer')) {
            $clients = [];
            $systemUsers = \taktwerk\yiiboilerplate\modules\customer\models\Client::findWithSystem()->where(["is_system_entry" => 1])->all();

            foreach ($systemUsers as $system_client) {
                /**@var $system_client \taktwerk\yiiboilerplate\modules\customer\models\Client*/
                $clients[] = $system_client->id;
            }

            if (!empty($clients)) {
                if($used_with_other_query){
                    $this->orFilterWhere(['in', $tableName.'.client_id',$clients]);
                }else{
                    $this->andFilterWhere(['in', $tableName.'.client_id',$clients]);
                }
            }
        }
        return $this;
    }


    /**
     * To get all entry of any model but not the ones that belong to system clients
     * @param $removedDeleted bool To remove soft-deleted entries from results, false to include them in result
     * @param string $tableName
     * @inheritdoc
     */
    public function findWithOutSystemEntry($tableName, $removedDeleted = true)
    {
        $model = $this;
        // If not authority, only show tickets of our customers
        if (array_key_exists('REQUEST_METHOD', $_SERVER) && !Yii::$app->user->can('Authority') && \Yii::$app->hasModule('customer')) {
            $clients = [];
            $clientUsers = \taktwerk\yiiboilerplate\modules\customer\models\ClientUser::findAll(["user_id" => Yii::$app->user->id]);
            foreach ($clientUsers as $client) {
                $clients[] = $client->client_id;
            }

            if (!empty($clients)) {
                $model->andFilterWhere(['in', $tableName.'.client_id',$clients]);
            }

        }
        return $model;
    }

    /**
     * @param $is_deleted string
     * @param $tableName string
     */
    public function deleted($is_deleted, $tableName)
    {
        $model = Yii::createObject($this->modelClass);
        
        $showDeleted = (
            property_exists(\Yii::$app->controller, 'model') 
            && 
                (
                    
                    is_subclass_of($this->modelClass, \Yii::$app->controller->model) || is_subclass_of(\Yii::$app->controller->model, $this->modelClass)
                )
            && 
                (
                    Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model) 
                    ||
                    Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model))
                )
            );
        
        if(
            array_key_exists('REQUEST_METHOD', $_SERVER)
            && 
                (
                    Yii::$app->get('userUi')->get('show_deleted', get_parent_class($model)) === "1" 
                    || 
                    Yii::$app->get('userUi')->get('show_deleted', get_class($model)) === "1" || $showDeleted
                )
         )
        {
            
            if($is_deleted==='yes'){
                $this->andWhere([
                    'is not',$tableName . '.deleted_at', new Expression('null')
                ]);
            }elseif($is_deleted==='no'){
                $this->andWhere([
                    'is', $tableName . '.deleted_at', new Expression('null')
                ]);
            }
            return $this;
        }
    }
}
