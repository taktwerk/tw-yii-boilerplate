<?php
/**
 * Created by taktwerk
 */

namespace taktwerk\yiiboilerplate\models;

use taktwerk\yiiboilerplate\assets\TwAsset;
use taktwerk\yiiboilerplate\behaviors\BlameableBehavior;
use taktwerk\yiiboilerplate\behaviors\History;
use taktwerk\yiiboilerplate\behaviors\History as HistoryBehavior;
use taktwerk\yiiboilerplate\behaviors\SoftDelete;
use taktwerk\yiiboilerplate\components\Helper;
use taktwerk\yiiboilerplate\helpers\FileHelper;
use taktwerk\yiiboilerplate\modules\media\behaviors\MediaBehavior;
use taktwerk\yiiboilerplate\modules\user\models\UserGroup;
use taktwerk\yiiboilerplate\traits\ArHistoryTrait;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\UnknownMethodException;
use yii\base\UnknownPropertyException;
use yii\behaviors\TimestampBehavior;
use yii\caching\DbDependency;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\validators\RequiredValidator;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use yii\helpers\Json;
use taktwerk\yiiboilerplate\modules\user\models\base\Group;
use taktwerk\yiiboilerplate\modules\sync\behaviors\SyncIndexBehavior;


/**
 * This is the taktwerk base-model class for table "TwActiveRecord".
 *
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 *
 */

class TwActiveRecord extends ActiveRecord
{
    /**
     * Trait to save all edits to the history table
     */
    use ArHistoryTrait;

    /**
     * Trait to be able to easily filter on the columns of the table
     */
    use SearchTrait;

    protected $uiFieldList = [];

    /**
     * Default ordering in data provider
     * @var int
     */
    protected $defaultOrder = ['pk' => SORT_ASC];

    public $oldRecord;
    public $isMobileSync = false;

    public $is_deleted;
    
    public static $cacheStorage;
    public static $skipGroupFilter = false;
    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge([['is_deleted'], 'safe'], parent::rules());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = [
            'softdelete'=>SoftDelete::class,

            // Track who created or updated a model
            BlameableBehavior::class,
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],

            // Arhistory tracks changes to a model in a simple way
            'arhistory' => [
                'class' => HistoryBehavior::class,
                'skipAttributes' => [
                    'created_at',
                    'updated_at',
                    'created_by',
                    'updated_by'
                ],
                'allowEvents' => [
                    HistoryBehavior::EVENT_UPDATE,
                    HistoryBehavior::EVENT_DELETE,
                    HistoryBehavior::EVENT_INSERT,
                ],
                'skippedTables' => [
                    'user_ui_data',
                    'analytics_pageview',
                    'user_auth_log',
                ]
            ],
        ];

        $syncMod = \Yii::$app->getModule('sync');
        if($syncMod && is_array($syncMod->models)){
            foreach($syncMod->models as $m){
                if($m){
                if($this instanceof $m || is_subclass_of($this, $m) || is_subclass_of($m, static::class)){
                    $behaviors['syncindex'] = SyncIndexBehavior::class;
                }
                }
            }
        }
        return $behaviors;
    }
    /**
     * Generate element for dependent field rule and switched visible-hidden condition
     * Docs at yii-boilerplate/docs/models.md
     * @param string $sourceAttribute
     * @param string $sourceId
     * @param string $targetAttribute
     * @param string $targetId
     * @param $sourceShowValue
     * @param bool $requiredValidation does field must be required when it shows
     * @return array
     * For example:
     * ```php
     * [
     *     'type_id',
     *     'show',
     *     'study_id'
     *     'when' => function ($model) use ($sourceAttribute, $sourceShowValue) {
     *         return $model->$sourceAttribute == $sourceShowValue;
     *     },
     *     'whenClient' => "function (attribute, value) {
     *         return $('#type_id').val() == '2';
     *     }",
     *     'jsData' => [
     *         'sourceId' => 'type_id',
     *         'targetId' => 'study_id',
     *         'sourceShowValue' => '2',
     *     ]
     * ]
     * ```
     */
    public function generateDependentFieldRule(string $sourceAttribute,string $sourceId,string $targetAttribute,string $targetId,$sourceShowValue, $requiredValidation = false,$showIfNotMatched= false)
    {
        $sourceShowValues = (is_array($sourceShowValue) ? $sourceShowValue : [$sourceShowValue]);

        $sourceName = $this->formName().'['.$sourceAttribute.']';

        $targetName = $this->formName().'['.$targetAttribute.']';

        $fieldRule = [
            $sourceAttribute,
            'show',
            $targetAttribute,
            'jsData' => [
                'sourceId' => $sourceId,
                'targetId' => $targetId,
                'sourceShowValue' => $sourceShowValues,
                'showIfNotMatched'=>$showIfNotMatched
            ]
        ];
        if ($requiredValidation) {
            $fieldRule['when'] = function ($model) use ($sourceAttribute, $sourceShowValues) {
                if(in_array($model->$sourceAttribute, $sourceShowValues)==(!$showIfNotMatched)) {
                    return true;
                }
                return false;
            };

            $sourceShowValue = "[";
            foreach($sourceShowValues as $value) {
                $sourceShowValue .= "'" . $value . "',";
            }
            $sourceShowValue .= "]";


            $fieldRule['whenClient'] = "function (attribute, value) {
                var arr = $sourceShowValue;
                var el = $('#$sourceId');
                var val = el.val();
                var name = el.attr('name');
                var form = el.parents('form:first');
                if(form.length>0){
                    var inputList = form.serializeArray();
                    inputList.forEach(function(u,i){
                        if(u.name==name){
                            val = u.value;
                        }
                    });
                }
                return (arr.includes(val) == ".((!$showIfNotMatched)?'true':'false').");
            }";


        }
        return $fieldRule;
    }




   public function generateDependentFieldRules($targetAtrName, $rules=[],$required = false)
   {
       if(!isset($rules['field']) ){
       $rules['field'] = $this->formName().'['.$targetAtrName.']';
       }
       if(!isset($rules['container']) ){
           $rules['container'] =  ".form-group-block";
       }
       if(!isset($rules['logic']) ){
           $rules['logic'] =  "and";
       }

       if($rules['rules']){
           if(isset($rules['rules']['name'])){
               $rules['rules'] = [$rules['rules']];
           }
           foreach($rules['rules'] as $key=>$rule){
               if(isset($rule['value'])){
                   if(is_array($rule['value'])){
                       $rules['rules'][$key]['value'] = array_map('strval', array_values($rule['value']));
                   }else if(is_int($rule['value'])){
                       $rules['rules'][$key]['value'] = strval($rule['value']);
                   }
               }
           }
       }

       $fieldRule = [
            $targetAtrName,
            'show',
            'jsData' => $rules
        ];

        //Trigger not working on select2 changes - fix it on monday
        if($required){
            $fieldRule['when'] = function($model) use($targetAtrName,$rules){
                    $rule = new \taktwerk\yiiboilerplate\helpers\MFConditionalField($model, $targetAtrName,$rules);
                    return $rule->isRequired();
            };

            $fieldRule['on'] = $rules['on'];

//If the field block is visible then it's required 
             $fieldRule['whenClient'] = <<<EOT
             function(attribute,value){
                return !$(attribute.input).closest('{$rules['container']}').prop('hidden');    
            } 
            EOT;
             if(isset($rules['message'])){
                 $fieldRule['message'] = $rules['message'];
             }
        }

        return $fieldRule;
    }



    /**
     * Extract `required` rule from dependent fields array
     * To correctly overwrite `required` rule, parent rule must be set with the key equal attribute name
     * For example, if you want overwrite next `required` rule for 'status_id', then parent's element on rules() must be:
     * [
     *     'status_id' => ['status_id', 'required'],
     * ]
     *
     * @return array
     */
    public function buildRequiredRulesFromFormDependentShowFields()
    {
        $formDependentShowFields = $this->formDependentShowFields();

        $rules = [];

        foreach ($formDependentShowFields as $key => $dependentField){

            if(isset($dependentField['when'])){
            $rule = [
                $dependentField[0],
                'required',
                'when' => $dependentField['when'],
                'on' => $dependentField['on'],
                'whenClient' => isset($dependentField['whenClient']) ? $dependentField['whenClient'] : null
            ];
            if(isset($dependentField['message'])){
                $rule['message'] = $dependentField['message'];
            }
            $rules[] = $rule;
            }
        }


        return $rules;
    }
    /**
     * Define fields who visibilty depends upon other fields and their value
     * For Example:
     *  $dependentFields = [
            'range_id' => $this->generateDependentFieldRule(
                'type_id',
                Html::getInputId($this,'type_id'), //Id of the input/select/textarea tag
                'range_id',
                Html::getInputId($this,'range_id'),//Id of the input/select/textarea tag
                '1', // The value on which the field should be visible
                true),
        ];
     */
    public function formDependentShowFields()
    {
        // dependence project_type
        $dependentFields = [];
        /* $dependentFields = [
            'childrenIds' => $this->generateDependentFieldRule(
                'is_collection',
                Html::getInputId($this,'is_collection'),
                'childrenIds',
                'childrenIds',
                '1',true),
        ]; */
        return $dependentFields;
    }
    /**
     * Get the placeholder of a field
     * @param $field
     * @return mixed
     */
    public function getAttributePlaceholder($field)
    {
            // Try the obvious palceholders
        $placeholders = $this->attributePlaceholders();
        if (! empty($placeholders[$field])) {
            return $placeholders[$field];
        }
        /*
         * Doesn't make sense to show labels/field names as placeholder as long as the label is shown
         * // Try with labels
         * $labels = $this->attributeLabels();
         * if (!empty($labels[$field])) {
         * return $labels[$field];
         * }
         * // Default is to inflect the field name
         * return $this->generateAttributeLabel($field);
         */
    }

    /**
     * List of placeholders for fields
     *
     * @return array
     */
    public function attributePlaceholders()
    {
        return [];
    }
    public static function tableFullName(){
        $schema = static::getTableSchema();
        return $schema?$schema->fullName:NULL;
    }
    /**
     * SoftDeleteBehavior to find only not deleted entries
     *
     * @param $removedDeleted bool
     *            To remove soft-deleted entries from results, false to include them in re
     * @inheritdoc
     */
    public static function find($removedDeleted = true)
    {
        if(is_array($removedDeleted)){
            $findParams = $removedDeleted;
            $defaultFindParams = [
                'removeDeleted'=>true
            ];
            $findParams = array_merge($defaultFindParams,$findParams);
            $removedDeleted = $findParams['removeDeleted'];
        }
        $model = new \taktwerk\yiiboilerplate\models\TwActiveQuery(get_called_class());
        if(array_key_exists('REQUEST_METHOD', $_SERVER)) {
            $webUser = Yii::$app->user;
        }
        if(array_key_exists('REQUEST_METHOD', $_SERVER) && Yii::$app->request->get('show_deleted')==='1'){
            $removedDeleted = false;
        }
        $schema = static::getTableSchema();
        $tbl = $schema->fullName;
        $usrGrpSchma = Yii::$app->db->schema->getTableSchema(\Yii::$app->db->tablePrefix . 'user_group');
        $userGroupTbl = $usrGrpSchma ? $usrGrpSchma->fullName : NULL;
        if ($removedDeleted === true) {
            $model->andWhere([
                $tbl . '.deleted_at' => null
            ]);
        }
        try {
            if (array_key_exists('REQUEST_METHOD', $_SERVER) && ! $webUser->can('Authority')) {
                if(is_array(self::$cacheStorage)){
                    foreach(self::$cacheStorage as $n=>$i){
                        if($webUser->id!=$n){
                            unset(self::$cacheStorage[$n]);
                        }
                    }
                }
                if ($userGroupTbl != null) {
                    $moduleId = \Yii::$app->controller->module->id;
                    if($moduleId=='v1' || (php_sapi_name() === 'cli' && array_key_exists('REQUEST_METHOD', $_SERVER))){
                        //detecting models modules when in api and then check appropriate group permissions 
                        $u = explode('models',$model->modelClass);
                        $moduleId = \yii\helpers\StringHelper::basename($u[0]);
                        if($moduleId==''){
                            $moduleId = \Yii::$app->controller->module->id;
                        }
                    }
                    $basePerm = $moduleId . '_' . Inflector::camel2id(StringHelper::basename($model->modelClass), "-");

                    $seePermName = 'x_' . $basePerm . '_see';
                    $editPermName = 'x_' . $basePerm . '_edit';
                    $groupPermName =  $basePerm . '_group';
                    $ownPermName = $basePerm . '_own';
                    $permList = [$seePermName,$editPermName,$groupPermName,$ownPermName];

                    foreach($permList as $pName){
                        if( !isset(self::$cacheStorage[$webUser->id]['permissions']) || !key_exists($pName, self::$cacheStorage[$webUser->id]['permissions']) )
                        {
                            self::$cacheStorage[$webUser->id]['permissions'][$pName] =  $webUser->can($pName);
                        }
                    }
                    $perms = self::$cacheStorage[$webUser->id]['permissions'];
                    $id = (isset($webUser)) ? $webUser->id : "";

                    $skipGroupFilter = false;
                    if(isset($findParams) && isset($findParams['skipGroupFilter']) && $findParams['skipGroupFilter']) {
                        $skipGroupFilter = true;
                    }

                    if (!empty($id) && $perms[$groupPermName] && !$skipGroupFilter && !self::$skipGroupFilter) {
                        /* If the user has see permission but not the edit one then only related group's models are displayed */
                        if ($perms[$seePermName] && ($perms[$editPermName] == false || getenv('ALLOW_MODEL_GROUP_LISTING_WITH_EDIT_PERMISSION'))) {
                            $hasGroupId = false;
                            $hasGroupRelation = false;
                            if (array_search('group_id', $schema->columnNames) !== false) {
                                $hasGroupId = true;
                            }
                            if(!key_exists('user_group_ids',self::$cacheStorage[$webUser->id]) || !is_array(self::$cacheStorage[$webUser->id]['user_group_ids'])){
                                $groupIds = UserGroup::find()
                                    ->select('group_id')
                                    ->where(['user_id' => $id])
                                    ->indexBy('group_id')->column();
                                    self::$cacheStorage[$webUser->id]['user_group_ids'] = $groupIds;
                            }else{
                                $groupIds = self::$cacheStorage[$webUser->id]['user_group_ids'];
                            }

                            if ($hasGroupId == false) {
                                /* Checks if the current model has junction relation to the Group */
                                $temp = Yii::createObject($model->modelClass);
                                $relMethodName = 'get' . StringHelper::basename($model->modelClass) . 'Groups';
                                if ($temp->hasMethod($relMethodName)) {
                                    $groupRel = $temp->{$relMethodName}();
                                    if ($groupRel instanceof ActiveQuery) {
                                        $relGrpModel = Yii::createObject($groupRel->modelClass);
                                        $gpRel = $relGrpModel->getGroup();

                                        if (is_subclass_of($gpRel->modelClass, Group::class)) {
                                            $model->joinWith(lcfirst(StringHelper::basename($model->modelClass) . 'Groups'));
                                            $model->andWhere([$relGrpModel->tableName().'.group_id'=>$groupIds]);
                                            $hasGroupRelation = true;
                                        }
                                    }
                                }
                            }

                            if ($hasGroupId === false && $hasGroupRelation == false) {
                                $model
                                    //->join('LEFT JOIN', $userTbl, $userTbl . '.`id`=' . $tbl . '.`created_by`')
                                    ->join('LEFT JOIN', $userGroupTbl, $userGroupTbl . '.`user_id`=' . $tbl . '.`created_by`');
                                $model->andWhere([
                                    $userGroupTbl . '.`group_id`' => $groupIds
                                ]);
                                //prd($model->createCommand()->rawSql);
                            } else if($hasGroupRelation == false){
                                $model->andWhere([
                                    $tbl . '.group_id' => $groupIds
                                ]);
                            }
                        }
                    } else if (! empty($id) && $perms[$ownPermName]) {
                        $model->andWhere([
                            $tbl . '.created_by' => $id
                        ]);
                    }
                }
            }
        } catch (InvalidConfigException $e) {}
        if ((getenv('ALL_CACHE_DISABLED')!=='1') 
            && (getenv('ALL_CACHE_ENABLED') || getenv('YII_QUERY_CACHE_ENABLED'))) {
                      
            $db = self::getDb();
            if ($db->driverName == 'mysql') {
                // Caching results - START
                try {
                    $table = str_replace("`", '', static::getDb()->getSchema()->getRawTableName(static::tableName()));
                    preg_match('/dbname=([^;]*)/', $db->dsn, $match);
                    $dbName = @$match[1];
                    if($dbName==''){
                        $dbName = $db->createCommand("SELECT DATABASE()")->queryScalar();
                    }
                    $sql = "SELECT CREATE_TIME,UPDATE_TIME
                  FROM information_schema.TABLES
                  WHERE TABLE_SCHEMA='".addslashes($dbName)."'
                  AND information_schema.TABLES.TABLE_NAME = '" . $table . "'";
                    $model->cache(0,new DbDependency([
                        'sql'=>$sql,
                        'reusable'=>false
                    ]));

                }catch(\Exception $e){
                    \Yii::error(VarDumper::dumpAsString($e));
                }
            }
        }
        //Caching results - END
        return $model;
    }
    /**
     * Find API is used in the SyncController process. Default behaviour is to use the standard find function,
     * which usually filters on the .deleted_at. However, this can be overwritten to included deleted items
     * in the apps.
     * @param $removedDeleted bool To remove soft-deleted entries from results, false to include them in re
     * @return mixed
     */
    public static function findApi($removedDeleted = true)
    {
        return static::find($removedDeleted);
    }

    /**
     * Find records that are SoftDeleted also
     * @return TwActiveQuery
     */
    public static function findDeleted()
    {
        return new \taktwerk\yiiboilerplate\models\TwActiveQuery(get_called_class());
    }

    /**
     * Export model attributes to ENV
     * @param array $attributes
     * @throws \Exception
     */
    public function toEnv(array $attributes = [])
    {
        $reflection = new \ReflectionClass($this);
        $modelName = $reflection->getShortName();
        if (empty($attribute)) {
            foreach ($this->attributes as $key => $val) {
                $attributes[] = $key;
            }
        }
        foreach ($attributes as $attribute) {
            if (!$this->hasAttribute($attribute)) {
                throw new \Exception("Model $modelName don't have attribute: $attribute");
            }
            $var = strtoupper($modelName . '_' . $attribute);
            $value = $this->{$attribute};
            // If PHP is running as an Apache module and an existing
            // Apache environment variable exists, overwrite it
            if (function_exists('apache_getenv') && function_exists('apache_setenv') && apache_getenv($var)) {
                apache_setenv($var, $value);
            }

            if (function_exists('putenv')) {
                putenv("$var=$value");
            }
            $_ENV[$var] = $value;
            $_SERVER[$var] = $value;
        }
    }

    /**
     * B(R)EAD - Determine if the model can be Read by the user.
     * To be overwritten in the model.
     * @return bool
     */
    public function readable()
    {
        return true;
    }


    /**
     * BR(E)AD - Determine if the model can be Edited by the user.
     * To be overwritten in the model.
     * @return bool
     */
    public function editable()
    {
        return $this->hasAttribute('deleted_at')?($this->deleted_at==null):true;
    }


    /**
     * BREA(D) - Determine if the model can be Deleted by the user.
     * To be overwritten in the model.
     * @return bool
     */
    public function deletable()
    {
        return true;
    }

    public function restorable()
    {
        return $this->deleted_at && $this->deleted_by ? true : false;
    }

    /**
     * BREAD - Determine if the model can be Duplicated by the user.
     * To be overwritten in the model.
     * @return bool
     */
    public function duplicatable()
    {
       return $this->hasAttribute('deleted_at')?($this->deleted_at==null):true;
    }

    /**
     * Create a copy of the model
     * @return TwActiveRecord
     * @throws \Exception
     */
    public function duplicateRecord()
    {
        $oldSwitchVal = History::$globalSwitch;
        History::$globalSwitch = false;
        $tran = \Yii::$app->db->beginTransaction();
        try {
            $originalModel = $this;

            $refClass = $rClass = new \ReflectionClass(get_class($originalModel));

            /**@var $model TwActiveRecord*/
            $model = Yii::createObject(get_class($originalModel));
            $model->setAttributes($originalModel->attributes);

            /* Check for unique validation */
            $all_validations = $model->getValidators();
            $fKeys = $model->getTableSchema()->foreignKeys;
            foreach ($all_validations as $validation) {
                if ($validation instanceof yii\validators\UniqueValidator) {
                    foreach ($validation->attributes as $attribute) {
                        $all_validation_on_attribute = $model->getActiveValidators($attribute);
                        /*Skip the Unique attribute if it's also a foreign key - START */
                        $skipUniAttrib = false;
                        foreach($fKeys as $fk){
                            if(key_exists($attribute,$fk)){
                                $skipUniAttrib = true;
                                break;
                            }
                        }
                        if($skipUniAttrib){
                            continue;
                        }
                        /*Skip the Unique attribute if it's also a foreign key - END */
                        foreach ($all_validation_on_attribute as $attribute_validation) {
                            if ($attribute_validation instanceof yii\validators\StringValidator) {
                                if ($attribute_validation->max == strlen($model->$attribute)) {
                                    $random_int = random_int(1, 9999);
                                    $model->$attribute = substr_replace($model->$attribute, $random_int, strlen($model->$attribute) - strlen($random_int));
                                }
                            }
                        }
                        if ($originalModel->$attribute == $model->$attribute) {
                            $model->$attribute = $model->$attribute . random_int(1, 9999);
                        }
                    }
                }
            }

            // set attributes base on model with id = $id
            $model->save(false);

            $baseModel = \yii\helpers\StringHelper::basename(get_class($model));
            /* COPYING TRANSLATION RECORDS - START */
            /* if (method_exists($originalModel, 'get' . $baseModel . 'Translations')) {
             $tModels = $originalModel->{lcfirst($baseModel . 'Translations')};
             $tQ = $originalModel->{'get' . $baseModel . 'Translations'}();
             $tClass = $tQ->modelClass;
             foreach ($tModels as $t) {
             $nT = Yii::createObject($tClass);
             $nT->setAttributes($t->attributes);
             foreach ($tQ->link as $k => $l) {
             $nT->{$k} = $model->{$l};
             }
             $nT->save();
             }
             } */
            /* COPYING TRANSLATION RECORDS - END */

            /* COPYING MEDIA FILES - START */

            $behaviors = $originalModel->behaviors;
            foreach ($behaviors as $behavior) {
                if ($behavior instanceof MediaBehavior) {
                    $mediaFiles = $originalModel->getMediaFiles($behavior->model_type, null);
                    /**
                     *
                     * @var $mediaFiles \taktwerk\yiiboilerplate\modules\media\models\MediaFile[]
                     */
                    foreach ($mediaFiles as $mFile) {
                        if(!Yii::$app->fs->has($mFile->getUploadPath() . $mFile->file_name)){
                            continue;
                        }
                        $originalFile = clone $mFile;
                        $pathInfo = [];
                        $tmpFile =Helper::getLocalTempFile($mFile->getUploadPath() . $mFile->file_name);
                        $pathInfo['tmp_name'] = [
                            'file_name' => $tmpFile
                        ];
                        $pathInfo['name'] = [
                            'file_name' => $mFile->file_name
                        ];
                        $mFile->isNewRecord = true;
                        $mFile->id = null;
                        $mFile->model_id = $model->id;
                        $mFile->save();
                        $skipQueueProcessing = false;
                        $mimeType = \yii\helpers\FileHelper::getMimeType($mFile->file_name);
                        $fileType = $mFile->getFileType('file_name');
                        if (in_array($fileType, static::$compressibleFileTypes)){
                            $skipQueueProcessing = ClassDispenser::getMappedClass(FileHelper::class)::hasAllCompressedFiles($originalFile,'file_name');
                        }

                        $mFile->uploadFile('file_name', $pathInfo['tmp_name'], $pathInfo['name'], null, true,$skipQueueProcessing);
                        if (in_array($fileType, static::$compressibleFileTypes)){
                            /* Compressed file copying - start */
                            $filesCopied = ClassDispenser::getMappedClass(FileHelper::class)::copyCompressedFilesFromModelToModel($originalFile, $mFile, 'file_name');
                            /* Compressed file copying - end */
                            if($filesCopied==false && $skipQueueProcessing==true){
                                $mFile->compressFile($mFile,'file_name');
                            }
                        }

                        \yii\helpers\FileHelper::unlink($tmpFile);
                        unset($tmpFile);
                    }
                }
            }
            /* COPYING MEDIA FILES -END */

            $traits = $rClass->getTraits();
            while ($parent = $rClass->getParentClass()) {
                $traits += $rClass->getTraits();
                $rClass = $parent;
            }

            $traits = array_combine(array_keys($traits), array_keys($traits));

            /* COPYING SINGLE UPLOAD FILES - START */

            if (in_array(\taktwerk\yiiboilerplate\traits\UploadTrait::class, $traits)) {
                $upload_fields = $originalModel->getUploadFields(true);
                if (count($upload_fields) >= 1) {
                    foreach ($model->attributes as $field => $val) {
                        if(!in_array($field,$upload_fields)){
                            continue;
                        }
                        try {
                            if(!Yii::$app->fs->has($originalModel->getUploadPath() . $originalModel->{$field})){
                                continue;
                            }
                            $fPath = ClassDispenser::getMappedClass(Helper::class)::getLocalTempFile($originalModel->getUploadPath() . $originalModel->{$field});

                            if ($fPath && is_file($fPath)) {
                                $pathInfo = [];
                                $pathInfo['tmp_name'] = [
                                    $field => $fPath
                                ];
                                $pathInfo['name'] = [
                                    $field => $originalModel->$field
                                ];
                                $skipQueueProcessing = false;
                                $mimeType = \yii\helpers\FileHelper::getMimeType($originalModel->$field);
                                $fileType = $originalModel->getFileType($field);
                                if (in_array($fileType, static::$compressibleFileTypes)){
                                    $skipQueueProcessing = ClassDispenser::getMappedClass(FileHelper::class)::hasAllCompressedFiles($originalModel,$field);
                                }

                                $model->uploadFile($field, $pathInfo['tmp_name'], $pathInfo['name'], null, true,$skipQueueProcessing);
                                if (in_array($fileType, static::$compressibleFileTypes)){
                                    $filesCopied = ClassDispenser::getMappedClass(FileHelper::class)::copyCompressedFilesFromModelToModel($originalModel, $model,$field);
                                    if($filesCopied==false && $skipQueueProcessing==true){
                                        $model->compressFile($model,$field);
                                    }
                                    /* compressed video copying - end */
                                }
                                \yii\helpers\FileHelper::unlink($fPath);
                                unset($fPath);
                            }
                        } catch (\Exception $e) {
                            throw $e;
                        }
                    }
                }
            }
            /* COPYING SINGLE UPLOAD FILES - END */

            /* COPYING RELATED MODELS - START */
            $trackRelTables = [];

            foreach ($refClass->getMethods() as $method) {
                if (substr($method->name, 0, 3) !== 'get') {
                    continue;
                }
                $reflectionMethod = new \ReflectionMethod($originalModel, $method->name);
                $checkNumArgs = $reflectionMethod->getNumberOfParameters();
                if ($checkNumArgs > 0) {
                    continue;
                } else {
                    try{
                        $getMethod = $originalModel->{$method->name}();
                    }catch(\Exception $e){
                    }catch(\ArgumentCountError $e){
                    }
                    if ($getMethod instanceof ActiveQuery) {
                        if ($getMethod->multiple) {
                            if (! isset($getMethod->via) || empty($getMethod->via)) {
                                $checkRepeat = true;
                                foreach ($getMethod->batch() as $items) {
                                    /*
                                     * Checking if a relational table is being repeated again,
                                     * if yes then skip the table to avoid re-copying the same model
                                     */
                                    if ($checkRepeat) {
                                        $checkItem = @$items[0];
                                        if ($checkItem && isset($trackRelTables[$checkItem->tableName()])) {
                                            break;
                                        }
                                        $trackRelTables[$checkItem->tableName()] = $checkItem->tableName();
                                        $checkRepeat = false;
                                        if ($checkItem instanceof ArHistory) {
                                            break;
                                        }
                                    }
                                    foreach ($items as $item) {

                                        if (method_exists($item, 'duplicateRecord')) {
                                            if($item->duplicatable()==false){
                                                continue;
                                            }
                                            $dupItem = $item->duplicateRecord();
                                            foreach ($getMethod->link as $k => $l) {
                                                $dupItem->{$k} = $model->{$l};
                                            }
                                            $dupItem->save(false);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            /* COPYING RELATED MODELS - END */
        } catch (\Exception $e) {
            History::$globalSwitch = $oldSwitchVal;
            $tran->rollBack();
            throw $e;
        }
        History::$globalSwitch = $oldSwitchVal;
        $tran->commit();
        return $model;
    }

    /**
     *
     * @param
     *            $field
     * @return null
     */
    /*
     * public function showFilePreview($field)
     * {
     * $function = 'showPreviewFor' . Inflector::camelize($field);
     * if (method_exists($this, $function)) {
     * return $this->$function();
     * }
     * return null;
     * }
     */
    /**
     *
     * @param
     *            $field
     * @return null
     */
    public function showFilePreview($field, $options = [], $returnOnlyPath = false)
    {
        $comments = $this->extractComments($field);

        $defaultOptions = ['height' => 100]; // not html options
        $loadFromOtherField = false;
        $options = array_merge($defaultOptions, $options);
        if ($comments && property_exists($comments, 'loadImageFromField') && $comments->isAdvanceMode == true && empty($this->$field)) {
            $field = $comments->loadImageFromField;
            $loadFromOtherField = true;
        }
        $fileType = $this->getFileType($field);

        if(!getenv('ENABLE_FILE_PREVIEW'))
            return strtoupper($fileType);

        $isVideo = ($fileType == 'video');
        $isPdf = ($fileType == 'pdf');
        if ($isVideo || $isPdf) {
            $thumbUrl = $this->getThumbFileUrl($field,true,$options);
            if($thumbUrl) {
                return !$returnOnlyPath ? Html::img($thumbUrl):$thumbUrl;
            }else{
                if($loadFromOtherField){
                    $baseUrl =null;
                    if(\Yii::$app->has('view')){
                        $bundle = \Yii::$app->view->registerAssetBundle(TwAsset::class);
                        $baseUrl = $bundle->baseUrl;
                    }
                    if($isVideo){
                        return !$returnOnlyPath ?
                        Html::img(($baseUrl)?$baseUrl.'/images/videooverlay.png':'/img/videooverlay.png', ['height' => '100']) :
                        null;
                    }
                    if($isPdf){
                        return !$returnOnlyPath ?
                        Html::img(($baseUrl)?$baseUrl.'/images/pdf.png':'/img/pdf.png', ['height' => '100']) :
                        null;
                    }
                }
            }
        }

        if ($fileType=='image') {
            $src = !empty($this->$field) ?$this->getFileUrl($field, $options, true) :null;
            return !$returnOnlyPath ? Html::img($src) : $src;
        }

        if ($this->$field==null) {
            return null;
        }

        return !$returnOnlyPath ? strtoupper($fileType) : null;
    }
    public function showFilePreviewWithLink($attribute){
        $fileType = $this->getFileType($attribute);
        if (in_array($fileType, ['video','image','pdf'])) {
            return Html::a($this->showFilePreview($attribute),['update', $this->primaryKey()[0] => $this->primaryKey]);
        }else
        {
            return Html::a($this->{$attribute}, $this->getFileUrl($attribute));
        }
    }
    /**
     * @param string $type
     * @return mixed|null
     */
    public function displayFieldList($type = 'index')
    {
        if (!empty($this->uiFieldList[$type])) {
            return $this->uiFieldList[$type];
        }
        return null;
    }

    /**
     * Set `required` class name for field $attribute which has RequiredValidator (even with `when` attribute)
     *
     * @param
     *            $attribute
     * @return bool
     */
    public function isAttributeNeededMarkedByAsterisk($attribute)
    {
        foreach ($this->getActiveValidators($attribute) as $validator) {
            if ($validator instanceof RequiredValidator) {
                return true;
            }
        }
        return false;
    }

    /**
     * To get system entry of any model
     * @param $removedDeleted
     * @return TwActiveQuery|\yii\db\ActiveQuery
     */
    public static function findSystemEntry($removedDeleted=true){

        $model = self::find($removedDeleted);
        if(array_key_exists('REQUEST_METHOD', $_SERVER) && Yii::$app->get('userUi')->get('show_deleted', $model->modelClass) === "1") {
                $removedDeleted = false;
                $model = self::find($removedDeleted);
        }

       return $model->findSystemEntry(self::tableName());
    }


    /**
     * Get all entries related to client and no system client entries
     * @param bool $removedDeleted
     * @return \taktwerk\yiiboilerplate\models\TwActiveQuery|\yii\db\ActiveQuery
     */
    public static function findWithOutSystemEntry($removedDeleted = true){
        $model = static::find($removedDeleted);

        if(array_key_exists('REQUEST_METHOD', $_SERVER) && Yii::$app->get('userUi')->get('show_deleted', $model->modelClass) === "1") {
            $removedDeleted = false;
            $model = static::find($removedDeleted);
        }

        $model->findWithOutSystemEntry(self::tableName(),$removedDeleted);

        return $model;
    }


    /**
     * To find all client models associated with this active record with a fallback to get system entry
     * @param $client_id
     * @param $one, to get only single entry
     * @return array|ActiveRecord[]
     */
    public static function clientEntries($client_id, $one = false){

        $models = self::findWithOutSystemEntry()
            ->andWhere([self::tableName().".client_id"=>$client_id])
            ->all();

        if(empty($models)){
            $models = self::findSystemEntry()->all();
        }

        if($one && is_array($models))
            return $models[0];

        return $models;
    }


    /**
     * Extract comments from database
     *
     * @param
     *            $column
     * @return bool|mixed
     */
    public function extractComments($column)
    {
        $column = \Yii::$app->db->getTableSchema($this->tableName())->getColumn($column);
        $output = json_decode($column->comment);
        if (is_object($output)) {
            return $output;
        }
        return false;
    }

    public function checkIfHidden($column)
    {
        $comment = $this->extractComments($column);
        
        if (preg_match('/(_hidden)$/i', $column) ||
            ($comment && (@$comment->inputtype === 'hidden')) || 
            preg_match('/(_meta)$/i', $column) ||
            preg_match('/(_filemeta)$/i', $column) ||
            ($comment && (@$comment->inputtype === 'meta'||@$comment->inputtype === 'filemeta'))) 
        {
            return true;
        }
        return false;
    }

    public function checkIfCanvas($column)
    {
        $comment = $this->extractComments($column);
        if (preg_match('/(_canvas)$/i', $column) || ($comment && (@$comment->inputtype === 'canvas'))) {
            return true;
        }
        return false;
    }

    public function afterFind()
    {
        $this->oldRecord = clone $this;

        parent::afterFind();
    }
    public function hasMethod($name, $checkBehaviors = true)
    {

        $hasMethod = parent::hasMethod($name, $checkBehaviors);
        if ($hasMethod === false && \Yii::$app->has('modelNewRelationMapper')) {

            $relationMap = \Yii::$app->modelNewRelationMapper->newRelationsMapping;
            $attribute = lcfirst(preg_replace('/^get(.*)/isU', '', $name));

            foreach ($relationMap as $modelClass => $relation) {
                if (is_a($this, $modelClass)) {
                    if (isset($relation[$attribute])) {
                        if (is_subclass_of($relation[$attribute][0], ActiveRecord::class)) {
                            return true;
                        }
                    }
                }
            }
        }
        return $hasMethod;
    }

    public function __call($name, $params)
    {
        try {
            return parent::__call($name, $params);
        } catch (UnknownMethodException $e) {
            if(\Yii::$app->has('modelNewRelationMapper')){
            $relationMap = \Yii::$app->modelNewRelationMapper->newRelationsMapping;
            $attribute = lcfirst(preg_replace('/^get(.*)/isU', '', $name));
            $rel = '';
            if(StringHelper::endsWith($name, 'List')){
                $rel = preg_replace( "/List+$/", "", $name);
            }

            foreach ($relationMap as $modelClass => $relations) {
                if (is_a($this, $modelClass)) {
                    if (isset($relations[$attribute])) {
                        $relation = $relations[$attribute];
                        $isValid = (isset($relation[0]) && isset($relation[1]) && is_array($relation[1]) && is_bool($relation[2]) && is_subclass_of($relation[0],ActiveRecord::class));

                    if ($isValid){
                        if($relation[2]===true){
                            return $this->hasMany($relation[0], $relation[1]);
                        }
                        if($relation[2]===false){
                            return $this->hasOne($relation[0], $relation[1]);
                        }

                    }
                    }else if(isset($relations[$rel])){
                        $relation = $relations[$rel];
                            $isValid = (isset($relation[0]) && isset($relation[1]) && is_array($relation[1]) && is_bool($relation[2]) && is_subclass_of($relation[0],ActiveRecord::class));
                            return $relation[0]::filter(($params)?$params[0]:null);
                    }
            }
            }
            }
        }
        throw new UnknownMethodException('Calling unknown method: ' . get_class($this) . "::$name()");
    }
    public function canGetProperty($name, $checkVars = true, $checkBehaviors = true)
    {
        $check = parent::canGetProperty($name, $checkVars = true, $checkBehaviors);
        if ($check === false && \Yii::$app->has('modelNewRelationMapper')) {
            $relationMap = \Yii::$app->modelNewRelationMapper->newRelationsMapping;
            foreach ($relationMap as $modelClass => $relations) {
                if (is_a($this, $modelClass)) {
                    $relation = $relations[$name];
                    $isValid = (isset($relation[0]) && isset($relation[1]) && is_array($relation[1]) && is_bool($relation[2]) && is_subclass_of($relation[0], ActiveRecord::class));
                    return $isValid;
                }
            }
        }
        return $check;
    }

    public function __get($name)
    {
        try {
            return parent::__get($name);
        } catch (UnknownPropertyException $e) {
            if (\Yii::$app->has('modelNewRelationMapper')) {
                $relationMap = \Yii::$app->modelNewRelationMapper->newRelationsMapping;
                foreach ($relationMap as $modelClass => $relations) {
                    if (is_a($this, $modelClass)) {
                        if (isset($relations[$name])) {
                            $relation = $relations[$name];
                            $isValid = (isset($relation[0]) && isset($relation[1]) && is_array($relation[1]) && is_bool($relation[2]) && is_subclass_of($relation[0],ActiveRecord::class));

                        if ($isValid){
                            if($relation[2]===true){
                                return $this->hasMany($relation[0], $relation[1])->all();
                            }
                            if($relation[2]===false){
                                return $this->hasOne($relation[0], $relation[1])->one();
                            }
                        }
                    }
                }
            }
            }
        }
        throw new UnknownPropertyException('Getting unknown property: ' . get_class($this) . '::' . $name);
    }

    public static function ckeditorFields(){
        $ck_columns = [];
        $columns = self::getTableSchema()->columns;
        foreach ($columns as $column) {
            $comment = json_decode($column->comment);
            if(($column->type === 'text' && ($comment && @$comment->inputtype === 'editor')) || $column->name == 'html' || (strpos($column->name, '_html') !== false)){
                $ck_columns[]=$column->name;
            }
        }
        return $ck_columns;

    }

    /**
     * @param $model
     * @param null $pjaxContainer
     * @param bool $is_search_form
     */
    public function getCustomFilters($model, $pjaxContainer=null, $is_search_form=false){

    }
    /**
     * Enable/Disables the extended search on the Grid Views
    */
    public static function extendedSearchEnabled():bool{
        return true;
    }

    /**
     * @param $field
     * @param $language_id
     * @return string|int|null
     */
    public function translateByLanguage($field, $language_id)
    {
        if (method_exists($this, 'getLanguages')) {
            $translation_model = lcfirst(Inflector::camelize(StringHelper::basename($this::className()) . 'Translations'));
            foreach ($this->$translation_model as $translation) {

                // If the language_id fits and the value isn't empty
                if ($translation->language_id == $language_id && !empty($translation->$field)) {
                    $languageModel = $translation;
                }

            }

            $model = (!empty($languageModel) ? $languageModel : null);

            // If the model is not empty, return the field value or full model.
            if(!empty($model)) {
                return $model->$field;
            }

        }
        return null;
    }

    public function fillLanguage(){
        $model_attributes = array_keys($this->attributes);
        if(in_array('language_id', $model_attributes) && !isset($this->language_id)){
            $this->language_id = Yii::$app->language;
        }
    }
}
