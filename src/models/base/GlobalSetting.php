<?php

namespace taktwerk\yiiboilerplate\models\base;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the base-model class for table "global_setting".
 * - - - - - - - - -
 * Generated by the modified Giiant CRUD Generator by taktwerk.com
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 */
class GlobalSetting extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'global_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['key'],
                'required'
            ],
            [
                ['key'],
                'string',
                'max' => 255
            ],
            [
                'value',
                'string'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('twbp', 'ID'),
            'key' => Yii::t('twbp', 'Key'),
            'value' => Yii::t('twbp', 'Value'),
        ];
    }
}
