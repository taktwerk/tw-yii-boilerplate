<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 5/19/2017
 * Time: 12:30 PM
 */

namespace taktwerk\yiiboilerplate\models;

use Yii;
use yii\db\ActiveRecord;
use taktwerk\yiiboilerplate\behaviors\History;

class ArHistory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'arhistory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['table_name', 'row_id', 'event', 'created_at'], 'required'],
            [['row_id', 'event', 'created_at', 'created_by'], 'integer'],
            [['old_value', 'new_value'], 'string'],
            [['table_name', 'field_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('twbp', 'ID'),
            'table_name' => Yii::t('twbp', 'Table Name'),
            'row_id' => Yii::t('twbp', 'Row ID'),
            'event' => Yii::t('twbp', 'Event'),
            'created_at' => Yii::t('twbp', 'Created At'),
            'created_by' => Yii::t('twbp', 'Created By'),
            'field_name' => Yii::t('twbp', 'Field Name'),
            'old_value' => Yii::t('twbp', 'Old Value'),
            'new_value' => Yii::t('twbp', 'New Value'),
        ];
    }

    /**
     * @return string
     */
    public function getEventName()
    {
        switch ($this->event) {
            case History::EVENT_INSERT:
                return Yii::t('twbp', "Create");
                break;
            case History::EVENT_UPDATE:
                return Yii::t('twbp', "Update");
            case History::EVENT_DELETE:
                return Yii::t('twbp', "Delete");
            default:
                return "";
                break;
        }
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        $user = User::findOne($this->created_by);
        if ($user) {
            return $user->toString;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(
            \app\models\User::class,
            ['id' => 'created_by']
        );
    }
}
