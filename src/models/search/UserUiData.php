<?php
//Generation Date: 11-Sep-2020 02:34:55pm
namespace taktwerk\yiiboilerplate\models\search;

use taktwerk\yiiboilerplate\modules\system\models\search\UserUiData as UserUiDataSearchModel;

/**
* UserUiData represents the model behind the search form about `taktwerk\yiiboilerplate\models\UserUiData`.
*/
class UserUiData extends UserUiDataSearchModel{

}