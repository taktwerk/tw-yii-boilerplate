<?php
//Generation Date: 17-Sep-2020 10:23:47pm
namespace taktwerk\yiiboilerplate\modules\feedback\models\search;

use taktwerk\yiiboilerplate\modules\feedback\models\search\base\FeedbackSetting as FeedbackSettingSearchModel;

/**
* FeedbackSetting represents the model behind the search form about `taktwerk\yiiboilerplate\modules\feedback\models\FeedbackSetting`.
*/
class FeedbackSetting extends FeedbackSettingSearchModel{

}