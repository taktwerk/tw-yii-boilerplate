<?php
//Generation Date: 28-Sep-2020 09:01:18am
namespace taktwerk\yiiboilerplate\modules\feedback\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\feedback\models\Feedback as FeedbackModel;

/**
 * Feedback represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\feedback\models\Feedback`.
 */
class Feedback extends FeedbackModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'client_id',
                    'attached_file',
                    'attached_file_filemeta',
                    'title',
                    'description',
                    'feedback_url',
                    'reference_model',
                    'reference',
                    'status',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'local_created_at',
                    'local_updated_at',
                    'local_deleted_at',
                    'name',
                    'email',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FeedbackModel::findWithOutSystemEntry();

        $this->parseSearchParams(FeedbackModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(FeedbackModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(FeedbackModel::class),
                'params' => [
                    'page' => $this->parsePageParams(FeedbackModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('client_id', $query);
        $this->applyHashOperator('reference', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('attached_file', $query);
        $this->applyLikeOperator('attached_file_filemeta', $query);
        $this->applyLikeOperator('title', $query);
        $this->applyLikeOperator('description', $query);
        $this->applyLikeOperator('feedback_url', $query);
        $this->applyLikeOperator('reference_model', $query);
        $this->applyLikeOperator('status', $query);
        $this->applyLikeOperator('name', $query);
        $this->applyLikeOperator('email', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        $this->applyDateOperator('local_created_at', $query, true);
        $this->applyDateOperator('local_updated_at', $query, true);
        $this->applyDateOperator('local_deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(FeedbackModel::class);
    }
}
