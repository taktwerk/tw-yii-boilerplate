<?php
//Generation Date: 17-Sep-2020 10:23:47pm
namespace taktwerk\yiiboilerplate\modules\feedback\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\feedback\models\base\FeedbackSetting as BaseFeedbackSetting;

/**
 * This is the model class for table "feedback_setting".
 */
class FeedbackSetting extends BaseFeedbackSetting
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

}
