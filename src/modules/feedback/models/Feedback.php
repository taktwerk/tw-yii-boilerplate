<?php

namespace taktwerk\yiiboilerplate\modules\feedback\models;

use himiklab\yii2\recaptcha\ReCaptchaValidator2;
use taktwerk\yiiboilerplate\modules\sync\traits\UserAppDataVersionTrait;
use taktwerk\yiiboilerplate\modules\user\models\User;
use Yii;
use \taktwerk\yiiboilerplate\modules\feedback\models\base\Feedback as BaseFeedback;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "feedback".
 */
class Feedback extends BaseFeedback
{
    const SCENARIO_CAPTCHA = 'captcha';

    public $reCaptcha;

    use UserAppDataVersionTrait;

    public function rules()
    {
        $rules = parent::rules();
        if($this->scenario==self::SCENARIO_CAPTCHA){
            $rules = ArrayHelper::merge($rules,
                [
                    [['reCaptcha'], ReCaptchaValidator2::className(), 'on'=>self::SCENARIO_CAPTCHA ]
                ]);
        }
        return $rules;
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), ['reCaptcha' => Yii::t('twbp', 'Re Captcha')]);
    }

    public static function find($removedDeleted = true)
    {
        $model = parent::find($removedDeleted);
        if (php_sapi_name() != "cli"){
            $user = Yii::$app->getUser();
            $userIdentity = $user->getIdentity();

            if (!$user->can('Authority')) {
                if ($user->can('FeedbackAdmin') && $userIdentity->client_id) {
                    $model->andWhere(['client_id' => $userIdentity->client_id]);
                } else if ($user->can('FeedbackViewer')) {
                    $model->andWhere(['created_by' => $userIdentity->id]);
                }
            }
        }
        return $model;
    }

    public static function findWithOutSystemEntry($removedDeleted = true)
    {
        $model = self::find($removedDeleted);

        $model->findWithOutSystemEntry(self::tableName(),$removedDeleted);

        return $model;
    }

    public function sendFeedbackEmail(){
        $feedback = FeedbackSetting::find()->where(['is_active'=>1])->one();
        if($feedback){
            $emails = $feedback->receivers;
            $emails = explode(",",$emails);
            if(!empty($emails)){
                $this->sendEmail($emails, Yii::t('twbp','New feedback received'));
            }
        }
    }

    public function sendFeedbackConfirmationEmail(){

        $feedback = FeedbackSetting::find()->where(['is_active'=>1])->one();
        if($feedback){
            $send_confirmation = $feedback->sender_confirmation;
        }

        if($this->email){
            $email = $this->email;
        }
        if(empty($email) && $this->created_by && Yii::$app->hasModule('user')){
            $user = User::findOne($this->created_by);
            if($user){
                $email = $user->email;
            }
        }
        if($send_confirmation && $email){
            $this->sendEmail($email, Yii::t('twbp','We received your feedback'));
        }

    }

    public function sendEmail($emails, $subject){

        Yii::$app->mailer->htmlLayout = '@taktwerk-boilerplate/modules/feedback/mail/layouts/feedback-layout';
        Yii::$app->mailer->view->params['feedbackRecipient'] = $this;
        Yii::$app->mailer->view->params['feedbackRecipientSubject'] = $subject;
        $temp =  null;
        $mailer =  Yii::$app->mailer->compose(
            '@taktwerk-boilerplate/modules/feedback/mail/feedback-body',
            [
                'feedback' => $this,
            ]
        )
            ->setTo($emails)
            ->setSubject($subject);
        if($this->attached_file && Yii::$app->fs->has($this->getUploadPath().$this->attached_file)){
            $temp = Yii::getAlias('@runtime/'.$this->attached_file);
            file_put_contents($temp,Yii::$app->fs->read($this->getUploadPath().$this->attached_file));
            $mailer->attach($temp);
        }
        $result = $mailer->send();
        if($temp){
            unlink($temp);
        }
        return $result;
    }
}
