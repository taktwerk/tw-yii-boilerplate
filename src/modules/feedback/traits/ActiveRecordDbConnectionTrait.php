<?php

namespace taktwerk\yiiboilerplate\modules\feedback\traits;

trait ActiveRecordDbConnectionTrait
{
    public static function getDb()
    {
        return \Yii::$app->db;
    }
}
