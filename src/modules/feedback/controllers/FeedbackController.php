<?php

namespace taktwerk\yiiboilerplate\modules\feedback\controllers;

use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\modules\feedback\models\Feedback;
use taktwerk\yiiboilerplate\modules\user\models\User;
use taktwerk\yiiboilerplate\widget\Select2;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

/**
 * This is the class for controller "FeedbackController".
 */
class FeedbackController extends \taktwerk\yiiboilerplate\modules\feedback\controllers\base\FeedbackController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\feedback\models\Feedback';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\feedback\models\search\Feedback';


    /**
     * Additional actions for controllers, uncomment to use them
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'new-feedback',
                        ]
                    ]
                ]
            ]
        ]);
    }


    /**
     * Init controller
     * @throws \Exception
     */
    public function init()
    {
        $this->crudColumnsOverwrite = [
            'index' => [
                // index
                'reference' => false,
                'feedback_url' => false,
            ],
            'crud' => [
                'after#status'=>[
                    'attribute' => 'created_by',
                    'format' => 'html',
                    'value' => function ($model) {
                        if($model->created_by && Yii::$app->hasModule('user')){
                            $user = User::findOne($model->created_by);
                            return $user->toString()?:$user->id;
                        }
                        return "";
                    }
                ],
                'after#created_by' => [
                    'attribute' => 'created_at',
                    'class' => '\taktwerk\yiiboilerplate\grid\DateTimeColumn',
                    'content' => function ($model) {
                        return \Yii::$app->formatter->asDatetime($model->created_at);
                    }
                ],
                'reference_model' =>  [
                    'attribute' => 'reference_model',
                    'format' => 'html',
                    'value' => function ($model) {
                        if($model->reference){
                            $model_name = explode('models\\',$model->reference_model)[1];
                            if(class_exists(strtolower(explode('models\\',$model->reference_model)[0])).$model_name){
                                $controller_name = strtolower(Inflector::slug(Inflector::camel2words($model_name), '-'));
                                $module_name = explode("modules\\",substr($model->reference_model, 0, strrpos($model->reference_model, '\\models')))[1];
                                $module_name = strtolower(str_replace(" ", "",ucwords(str_replace("-"," ",$module_name))));
                                $url = \Yii::$app->urlManager->createAbsoluteUrl("$module_name/$controller_name/update?id=$model->reference");

                                return Html::a($model_name, $url, ['data-pjax' => 0]);
                            }else{
                                $url = str_replace("/view?","/update?",$model->feedback_url);
                                return  Html::a($model->reference_model, $url, ['data-pjax' => 0]);
                            }
                        }elseif(strpos($model->reference_model,'models')!==false){
                            $model_name = explode('models\\',$model->reference_model)[1];
                            $controller_name = strtolower(Inflector::slug(Inflector::camel2words($model_name), '-'));
                            $module_name = explode("modules\\",substr($model->reference_model, 0, strrpos($model->reference_model, '\\models')))[1];
                            $module_name = strtolower(str_replace(" ", "",ucwords(str_replace("-"," ",$module_name))));
                            $url = \Yii::$app->urlManager->createAbsoluteUrl("$module_name\\$controller_name\\index");
                            return Html::a($model_name, $url, ['data-pjax' => 0]);
                        }else{
                            return $model->reference_model;
                        }
                    },
                ],
            ]
        ];

        return parent::init();
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionNewFeedback()
    {
        $new_feedback = (new Feedback());
        $new_feedback->title = "Title";
        $post = Yii::$app->request->post();
        if(isset($post['Feedback']['reCaptcha'])){
            $new_feedback->scenario = $new_feedback::SCENARIO_CAPTCHA;
        }
        if (Yii::$app->request->isAjax && $new_feedback->load(Yii::$app->request->post()) && !empty(ActiveForm::validate($new_feedback))) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($new_feedback);
        }

        if(\Yii::$app->request->isPost){
            $success = false;
            $new_feedback->status = Feedback::STATUS_OPEN;
            if (php_sapi_name() != "cli" && !Yii::$app->getUser()->can('Authority') && Yii::$app->hasModule('customer')) {
                $clients = [];
                $clientUsers = \taktwerk\yiiboilerplate\modules\customer\models\ClientUser::findAll(['user_id' => Yii::$app->getUser()->id]);
                foreach ($clientUsers as $client) {
                    $new_feedback->client_id = $client->client_id;
                    continue;
                }
            }
            $new_feedback->title = "Title";
            if($new_feedback->load(\Yii::$app->request->post()) && $new_feedback->save()){
                if(\Yii::$app->request->post('screenshot')!==""){
                    $screenshot_tmp = \Yii::getAlias('@runtime/upload/'.date('YmdHis').".png");
                    $screenshot_name = 'screenshot_'.date('YmdHis').".png";
                    $img = str_replace('data:image/png;base64,', '',  \Yii::$app->request->post('screenshot'));
                    $img = str_replace(' ', '+', $img);
                    $data = base64_decode($img);
                    file_put_contents($screenshot_tmp, $data);
                    try {
                        $stream = fopen($screenshot_tmp, 'r+');
                        \Yii::$app->fs->putStream($new_feedback->getUploadPath() . $screenshot_name, $stream);
                    } catch (\Exception $e) {
                    }
                    $new_feedback->attached_file = $screenshot_name;
                }else{
                    if(isset($_FILES['Feedback']['name'])){
                        try {
                            $new_feedback->uploadFile('attached_file', $_FILES['Feedback']['tmp_name'], $_FILES['Feedback']['name']);
                        } catch (\Exception $e) {
//                            throw $e;
                        }
                    }
                }
                if($new_feedback->save()){
                    $new_feedback->sendFeedbackEmail();
                    $new_feedback->sendFeedbackConfirmationEmail();
                    $success = true;
                }else{
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($new_feedback);
                }
            }
            return $success;
        }elseif(Yii::$app->request->isGet){

            $new_feedback->reference_model = \Yii::$app->request->get('reference_model');
            $new_feedback->feedback_url = \Yii::$app->request->get('feedback_url');
            $new_feedback->reference = \Yii::$app->request->get('reference');

            \Yii::$app->view->getAssetManager()->bundles = [
                'yii\bootstrap\BootstrapAsset' => false,
                'yii\web\JqueryAsset' => false,
            ];

            return $this->renderAjax('@taktwerk/yiiboilerplate/modules/feedback/widgets/views/_form', [
                'model' => $new_feedback,
            ]);

        }else{
            return false;
        }
    }

    public function formFieldsOverwrite($model, $form)
    {
        $client_field = '';
        if(\Yii::$app->hasModule('customer')){
            if(php_sapi_name() != "cli" && \Yii::$app->getUser()->can('Authority')) {
                $client_field =$form->field(
                        $model,
                        'client_id'
                    )
                        ->widget(
                            Select2::class,
                            [
                                'data' => \taktwerk\yiiboilerplate\modules\customer\models\Client::find()->count() > 50 ? null : ArrayHelper::map(\taktwerk\yiiboilerplate\modules\customer\models\Client::find()->all(), 'id', 'toString'),
                                'initValueText' => \taktwerk\yiiboilerplate\modules\customer\models\Client::find()->count() > 50 ? \yii\helpers\ArrayHelper::map(\taktwerk\yiiboilerplate\modules\customer\models\Client::find()->andWhere(['id' => $model->client_id])->all(), 'id', 'toString') : '',
                                'options' => [
                                    'placeholder' => Yii::t('twbp', 'Select a value...'),
                                    'id' => 'client_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    (\taktwerk\yiiboilerplate\modules\customer\models\Client::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                                    (\taktwerk\yiiboilerplate\modules\customer\models\Client::find()->count() > 50 ? 'ajax' : '') => [
                                        'url' => \yii\helpers\Url::to(['list']),
                                        'dataType' => 'json',
                                        'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'Client\'
                                        };
                                    }')
                                    ],
                                ],
                                'pluginEvents' => [
                                    "change" => "function() {
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
                                        Url::toRoute('/customer/client/entry-details?id=', true) .
                                        "' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#client_id_well').html(json.data);
                                                //$('#client_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }",
                                ],
                                'addon' => (Yii::$app->getUser()->can('customer_client_create') && (!$relatedForm || ($relatedType != RelatedForms::TYPE_MODAL && !$useModal))) ? [
                                    'append' => [
                                        'content' => [
                                            RelatedForms::widget([
                                                'relatedController' => '/customer/client',
                                                'type' => $relatedTypeForm,
                                                'selector' => 'client_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                                'primaryKey' => 'id',
                                            ]),
                                        ],
                                        'asButton' => true
                                    ],
                                ] : []
                            ]
                        ) . '
                    
                
                <div id="client_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' .
                    (\taktwerk\yiiboilerplate\modules\customer\models\Client::findOne(['id' => $model->client_id])
                        ->entryDetails == '' ?
                        ' display:none;' :
                        '') . '
                    ">' .
                    (\taktwerk\yiiboilerplate\modules\customer\models\Client::findOne(['id' => $model->client_id])->entryDetails != '' ?
                        \taktwerk\yiiboilerplate\modules\customer\models\Client::findOne(['id' => $model->client_id])->entryDetails :
                        '') . ' 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>';
            }else{
                $client_field = HTML::activeHiddenInput(
                    $model,
                    'client_id',
                    [
                        'id' => Html::getInputId($model, 'client_id') . $owner
                    ]
                );
            }
        }else{
            $client_field = $form->field($model,'client_id');
        }
        $this->formFieldsOverwrite = [
            'client_id' => $client_field,

        ];

        return $this->formFieldsOverwrite;
    }

}
