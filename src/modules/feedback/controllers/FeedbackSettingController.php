<?php
//Generation Date: 17-Sep-2020 10:23:47pm
namespace taktwerk\yiiboilerplate\modules\feedback\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "FeedbackSettingController".
 */
class FeedbackSettingController extends \taktwerk\yiiboilerplate\modules\feedback\controllers\base\FeedbackSettingController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\feedback\models\FeedbackSetting';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\feedback\models\search\FeedbackSetting';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
}
