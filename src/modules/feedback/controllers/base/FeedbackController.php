<?php
//Generation Date: 11-Sep-2020 03:58:02pm
namespace taktwerk\yiiboilerplate\modules\feedback\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * FeedbackController implements the CRUD actions for Feedback model.
 */
class FeedbackController extends TwCrudController
{
}
