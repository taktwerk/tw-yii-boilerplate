<?php
//Generation Date: 17-Sep-2020 10:23:47pm
namespace taktwerk\yiiboilerplate\modules\feedback\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * FeedbackSettingController implements the CRUD actions for FeedbackSetting model.
 */
class FeedbackSettingController extends TwCrudController
{
}
