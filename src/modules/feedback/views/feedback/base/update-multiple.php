<?php
//Generation Date: 11-Sep-2020 03:58:03pm
use yii\helpers\Html;
use Yii;
/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\feedback\models\Feedback $model
 * @var array $pk
 * @var array $show
 */

$this->title = 'Feedback ' . $model->title . ', ' . Yii::t('twbp', 'Edit Multiple');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('twbp','Feedbacks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('twbp', 'Edit Multiple');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body feedback-update">

        <h1>
            <?= \Yii::t('twbp','Feedback'); ?>
        </h1>

        <div class="crud-navigation">
        </div>

        <?php echo $this->render('_form', [
            'model' => $model,
            'pk' => $pk,
            'show' => $show,
            'multiple' => true,
            'useModal' => $useModal,
            'action' => $action,
        ]); ?>

    </div>
</div>