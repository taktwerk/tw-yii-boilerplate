<?php
//Generation Date: 28-Sep-2020 09:01:18am
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\feedback\models\search\Feedback $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="feedback-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'client_id') ?>

        <?= $form->field($model, 'attached_file') ?>

        <?= $form->field($model, 'attached_file_filemeta') ?>

        <?= $form->field($model, 'title') ?>

        <?php // echo $form->field($model, 'description') ?>

        <?php // echo $form->field($model, 'feedback_url') ?>

        <?php // echo $form->field($model, 'reference_model') ?>

        <?php // echo $form->field($model, 'reference') ?>

        <?php // echo $form->field($model, 'status') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

        <?php // echo $form->field($model, 'local_created_at') ?>

        <?php // echo $form->field($model, 'local_updated_at') ?>

        <?php // echo $form->field($model, 'local_deleted_at') ?>

        <?php // echo $form->field($model, 'name') ?>

        <?php // echo $form->field($model, 'email') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
