<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200131_052949_make_reference_model_feedback_url_not_required extends TwMigration
{

    /**
     * @return bool|void
     */
    public function up()
    {
        $query = <<<EOF
        ALTER TABLE `feedback` 
        CHANGE `reference_model` `reference_model` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'Reference should be a hidden attribute with current model as value', 
        CHANGE `feedback_url` `feedback_url` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'Url should be a hidden attribute with current url as value';

EOF;
        $this->execute($query);
    }

    public function down()
    {
        echo "m200131_052949_make_reference_model_feedback_url_not_required cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
