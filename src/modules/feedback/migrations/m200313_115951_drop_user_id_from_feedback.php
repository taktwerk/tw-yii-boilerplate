<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200313_115951_drop_user_id_from_feedback extends TwMigration
{
    public function up()
    {
        $this->dropColumn('{{%feedback}}', 'user_id');
    }

    public function down()
    {
        echo "m200313_115951_drop_user_id_from_feedback cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
