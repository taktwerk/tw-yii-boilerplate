<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200220_104400_add_feedback_permission_to_administrator_fix extends TwMigration
{
    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        // Init the permission cascade
        $this->createCrudControllerPermissions('feedback_feedback');
        // Administration permission assignment
        $this->addAdminControllerPermission('feedback_feedback','Administrator');
    }

    /**
     * @return bool|void
     */
    public function down()
    {
        echo "m200220_104400_add_feedback_permission_to_administrator_fix cannot be reverted.\n";

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
