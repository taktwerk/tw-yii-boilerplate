<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200826_200614_rename_reference_id_to_reference extends TwMigration
{
    public function up()
    {
        $query = <<<EOF
ALTER TABLE `feedback` CHANGE `reference_id` `reference` INT(11) NULL DEFAULT NULL;
EOF;
        $this->execute($query);

    }

    public function down()
    {
        echo "m200826_200614_rename_reference_id_to_reference cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
