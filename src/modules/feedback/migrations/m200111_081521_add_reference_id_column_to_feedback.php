<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200111_081521_add_reference_id_column_to_feedback extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%feedback}}', 'reference_id', $this->integer()->null());
    }

    public function down()
    {
        echo "m200111_081521_add_reference_id_column_to_feedback cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
