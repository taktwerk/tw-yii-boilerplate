<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200124_053822_add_feedback_permission_to_viewer extends TwMigration
{
    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->createPermission('feedback_feedback_new-feedback', 'Add New Feedback', ['Viewer']);

    }

    /**
     * @return bool|void
     */
    public function down()
    {
        $this->removePermission('feedback_feedback_new-feedback',  ['Viewer']);

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
