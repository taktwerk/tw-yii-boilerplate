<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200421_121818_add_client_key_to_feedback extends TwMigration
{
    public function up()
    {
        $query = <<<EOF
ALTER TABLE `feedback` ADD CONSTRAINT `fk_client_feedback` FOREIGN KEY (`client_id`) REFERENCES `client`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
EOF;

        $this->execute($query);
        
    }

    public function down()
    {
        echo "m200421_121818_add_client_key_to_feedback cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
