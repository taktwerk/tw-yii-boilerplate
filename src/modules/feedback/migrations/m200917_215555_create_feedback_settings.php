<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200917_215555_create_feedback_settings extends TwMigration
{
    public function safeUp()
    {
        if ($this->db->getTableSchema('{{%feedback_setting}}', true) === null) {
            $tableOptions = 'ENGINE=InnoDB';

            $this->createTable(
                '{{%feedback_setting}}',
                [
                    'id' => Schema::TYPE_PK . "",
                    'receivers' => Schema::TYPE_TEXT ,
                    'sender_confirmation' => Schema::TYPE_TINYINT."(1) NOT NULL DEFAULT '0'",
                ],
                $tableOptions
            );
        }
        $tbl = "{{%feedback_setting}}";
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\feedback"}';
        $this->addCommentOnTable($tbl, $comment);

    }

    public function safeDown()
    {
        $this->dropTable('{{%feedback_setting}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
