<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200928_065548_feedback_file_meta_columns extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%feedback}}', 'attached_file_filemeta', $this->text()->after('attached_file'));
    }

    public function down()
    {
        echo "m200928_065548_feedback_file_meta_columns cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
