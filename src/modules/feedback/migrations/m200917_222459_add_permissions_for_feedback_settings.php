<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200917_222459_add_permissions_for_feedback_settings extends TwMigration
{
    public function up()
    {
        $this->createCrudControllerPermissions('feedback_feedback-setting');
    }

    public function down()
    {
        echo "m200917_222459_add_permissions_for_feedback_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
