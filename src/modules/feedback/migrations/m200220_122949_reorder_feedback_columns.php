<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200220_122949_reorder_feedback_columns extends TwMigration
{

    /**
     * @return bool|void
     */
    public function up()
    {
        $query = <<<EOF
        ALTER TABLE `feedback`
	CHANGE COLUMN `client_id` `client_id` INT(11) NULL DEFAULT NULL AFTER `id`,
	CHANGE COLUMN `description` `description` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `attached_file`,
	CHANGE COLUMN `status` `status` ENUM('Open','Done') NOT NULL DEFAULT 'Open' COLLATE 'utf8_unicode_ci' AFTER `description`,
	CHANGE COLUMN `feedback_url` `feedback_url` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Url should be a hidden attribute with current url as value' COLLATE 'latin1_swedish_ci' AFTER `description`,
	CHANGE COLUMN `reference_model` `reference_model` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Reference should be a hidden attribute with current model as value' COLLATE 'latin1_swedish_ci' AFTER `feedback_url`,
	CHANGE COLUMN `reference_id` `reference_id` INT(11) NULL DEFAULT NULL AFTER `reference_model`;
EOF;
        $this->execute($query);
    }

    public function down()
    {
        echo "m200220_122949_reorder_feedback_columns cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
