<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200826_195523_add_name_email extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%feedback}}', 'name', $this->string(255)->null());
        $this->addColumn('{{%feedback}}', 'email', $this->string(255)->null());

    }

    public function down()
    {
        echo "m200826_195523_add_name_email cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
