<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200302_070936_change_namespace_for_feedback extends TwMigration
{
    public function up()
    {
        $query = <<<EOF
        
            ALTER TABLE `feedback`
                COMMENT='';
                
            ALTER TABLE `feedback`
                COMMENT='{"base_namespace":"taktwerk\\\\\\\\yiiboilerplate\\\\\\\\modules\\\\\\\\feedback"}';
       
EOF;

        $this->execute($query);
    }

    public function down()
    {
        echo "m200302_070936_change_namespace_for_feedback cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
