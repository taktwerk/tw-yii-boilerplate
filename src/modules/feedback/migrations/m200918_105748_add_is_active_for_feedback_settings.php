<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200918_105748_add_is_active_for_feedback_settings extends TwMigration
{
    public function up()
    {

        $this->addColumn(
            '{{%feedback_setting}}',
            'is_active',
            $this->tinyInteger(1)->defaultValue(0) . ' AFTER sender_confirmation'
        );

    }

    public function down()
    {
        echo "m200918_105748_add_is_active_for_feedback_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
