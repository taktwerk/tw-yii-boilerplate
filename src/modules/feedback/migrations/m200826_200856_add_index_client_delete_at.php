<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200826_200856_add_index_client_delete_at extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_feedback_deleted_at_client_id', '{{%feedback}}', ['deleted_at','client_id']);
    }

    public function down()
    {
        echo "m200826_200856_add_index_client_delete_at cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
