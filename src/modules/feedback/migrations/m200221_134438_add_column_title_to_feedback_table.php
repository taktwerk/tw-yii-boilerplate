<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200221_134438_add_column_title_to_feedback_table extends TwMigration
{
    public function up()
    {
        $this->addColumn(
            '{{%feedback}}',
            'title',
            $this->string(255)->notNull() . ' AFTER attached_file'
        );
    }

    public function down()
    {
        $this->dropColumn('{{%feedback}}', 'title');
    }
}
