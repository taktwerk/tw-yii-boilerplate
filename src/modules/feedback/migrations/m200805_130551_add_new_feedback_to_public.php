<?php

use taktwerk\yiiboilerplate\TwMigration;
use yii\db\Schema;

class m200805_130551_add_new_feedback_to_public extends TwMigration
{
    public function up()
    {
        $this->removePermission('feedback_feedback_new-feedback');
        $this->createPermission('feedback_feedback_new-feedback', 'Add new feedback', ['Public']);
    }

    public function down()
    {
        echo "m200805_130551_add_new_feedback_to_public cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
