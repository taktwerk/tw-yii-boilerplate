<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200124_054400_add_feedback_permission_to_administrator extends TwMigration
{
    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->createPermission('feedback_feedback', 'Feedback Permission', ['Administrator']);

    }

    /**
     * @return bool|void
     */
    public function down()
    {
        $this->removePermission('feedback_feedback', ['Administrator']);

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
