<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200220_060220_add_comment_to_feedback_table_fix extends TwMigration
{
    /**
     * @return bool|void
     */
    public function up()
    {
        $tbl = "{{%feedback}}";
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\feedback"}';
        $this->addCommentOnTable($tbl, $comment);
    }

    public function down()
    {
        echo "m200220_060220_add_comment_to_feedback_table_fix cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
