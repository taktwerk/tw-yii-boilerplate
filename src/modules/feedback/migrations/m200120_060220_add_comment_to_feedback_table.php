<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200120_060220_add_comment_to_feedback_table extends TwMigration
{
    /**
     * @return bool|void
     */
    public function up()
    {
        $query = <<<EOF
            ALTER TABLE `feedback`
                COMMENT='{"base_namespace":"app\\\\modules\\\\feedback"}';
           
EOF;
        $this->execute($query);
    }

    public function down()
    {
        echo "m200120_060220_add_comment_to_feedback_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
