<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200302_065744_add_user_id_to_feedback extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%feedback}}', 'user_id', $this->integer()->null());
    }

    public function down()
    {
        echo "m200302_065744_add_user_id_to_feedback cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
