<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191223_114511_feedback extends TwMigration
{
    public function safeUp()
    {
        if ($this->db->getTableSchema('{{%feedback}}', true) === null) {
            $tableOptions = 'ENGINE=InnoDB';

            $this->createTable(
                '{{%feedback}}',
                [
                    'id' => Schema::TYPE_PK . "",
                    'attached_file' => Schema::TYPE_STRING . "(255)",
                    'description' => Schema::TYPE_TEXT . "",
                    'reference_model' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Reference should be a hidden attribute with current model as value'",
                    'feedback_url' => Schema::TYPE_STRING . "(255) NOT NULL COMMENT 'Url should be a hidden attribute with current url as value'",
                    'status' => "enum('Open','Done')" . " NOT NULL DEFAULT 'Open'"
                ],
                $tableOptions
            );
        }

    }

    public function safeDown()
    {
        $this->dropTable('{{%feedback}}');
    }
}
