<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200419_184907_add_feedback_roles extends TwMigration
{
    public function up()
    {
        // Create roles
        $this->createRole('FeedbackViewer');
        $this->createRole('FeedbackAdmin', 'FeedbackViewer');

        // Init the permission cascade for see
        $this->addSeeControllerPermission('feedback_feedback', 'FeedbackViewer');
        // Administration permission assignment
        $this->addAdminControllerPermission('feedback_feedback','FeedbackAdmin');

    }

    public function down()
    {
        echo "m200419_184907_add_feedback_roles cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
