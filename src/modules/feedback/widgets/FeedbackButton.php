<?php

namespace taktwerk\yiiboilerplate\modules\feedback\widgets;

use yii\base\Widget;

class FeedbackButton extends Widget
{
    /**
     * @var string
     */
    public $btn_class;
    /**
     * @var string
     */
    public $icon;
    /**
     * @return string|void
     */
    public function run()
    {
        return $this->render('@taktwerk/yiiboilerplate/modules/feedback/widgets/views/_button',[
            'btn_class'=>$this->btn_class,
            'icon'=>$this->icon." ",
        ]);
    }
}
