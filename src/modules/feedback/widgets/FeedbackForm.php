<?php

namespace taktwerk\yiiboilerplate\modules\feedback\widgets;

use taktwerk\yiiboilerplate\modules\feedback\models\Feedback;
use yii\base\Widget;

class FeedbackForm extends Widget
{
    /**
     * @var
     */
    public $is_screenshot = false;
    
    /**
     * @var
     */
    public $is_popup = false;
    
    /**
     * @var
     */
    public $message = '';
    /**
     * @var
     */
    public $model = '';

    /**
     * @var
     */
    public $captcha = false;

    /**
     * @return string|void
     */
    public function run()
    {
        $feedback = new Feedback();
        if($this->captcha){
            $feedback->scenario = $feedback::SCENARIO_CAPTCHA;
        }
        return $this->render('@taktwerk-boilerplate/modules/feedback/widgets/views/_form',[
            'model'=> $feedback,
            'captcha'=> $this->captcha,
            'is_screenshot'=> $this->is_screenshot,
            'is_popup'=> $this->is_popup,
            'message'=>$this->message
        ]);
    }
}
