<?php

namespace taktwerk\yiiboilerplate\modules\feedback\widgets;

use yii\base\Widget;

class FeedbackFormAssets extends Widget
{
    /**
     * @return string|void
     */
    public function run()
    {
        return $this->render('@taktwerk/yiiboilerplate/modules/feedback/widgets/views/_assets');
    }
}
