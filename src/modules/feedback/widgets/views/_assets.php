<?php
/** @var $reference_model string*/
/** @var $feedback_url string*/

use yii\helpers\Url;
use yii\bootstrap\Modal;

$feedback_create_url = Url::toRoute(['/feedback/feedback/new-feedback']);
$feedback_url = Yii::$app->urlManager->hostInfo.Yii::$app->request->url;
$reference_module_path = "app\\modules\\".str_replace(" ", "",ucwords(str_replace("-"," ",Yii::$app->controller->module->id)));
$reference_module = str_replace(" ", "",ucwords(str_replace("-"," ",Yii::$app->controller->module->id)));
$reference_controller = Yii::$app->controller->id!="default"?str_replace(" ", "",ucwords(str_replace("-"," ",Yii::$app->controller->id))):$reference_module;
$reference_action = str_replace(" ", "",ucwords(str_replace("-"," ",Yii::$app->controller->action->id)));
if(class_exists($reference_module_path."\\models\\".$reference_module)){
    $reference_model = $reference_module_path."\\models\\".$reference_module;
}elseif(class_exists("app\\models\\".$reference_module)){
    $reference_model = "app\\models\\".$reference_module;
}if(class_exists($reference_module_path."\\models\\".$reference_controller)){
    $reference_model = $reference_module_path."\\models\\".$reference_controller;
}elseif(class_exists("app\\models\\".$reference_controller)){
    $reference_model = "app\\models\\".$reference_controller;
}elseif(class_exists($reference_module_path."\\models\\".$reference_action)){
    $reference_model = $reference_module_path."\\models\\".$reference_action;
}elseif(class_exists("app\\models\\".$reference_action)){
    $reference_model = "app\\models\\".$reference_action;
}else{
    $reference_model = $reference_controller;
}
$reference_model_id = Yii::$app->request->get('id')?:null;
$reference_model= str_replace("\\","\\\\",$reference_model);
$js = <<<JS
$(document).ready(function () {
    $('.feedback-button').on('click', function(e){
        e.preventDefault();
        $('#modalFormFeedback').modal('show');
       $.ajax({
         type: 'GET',
         url: "$feedback_create_url",
         data: {
             reference_model : '$reference_model',
             reference : '$reference_model_id',
             feedback_url : '$feedback_url',
             form_submit : false,
         },
         success: function(data) {
           $('body').addClass('kv-grid-loading');
           $('#modalFormFeedback').find('.modal-body').html(data);
           $('body').removeClass('kv-grid-loading');
         }
       });
    });
});

JS;
$this->registerJs($js,\yii\web\View::POS_END);
Modal::begin([
    'size' => Modal::SIZE_LARGE,
    'id' => 'modalFormFeedback',
    'header' => Yii::t('twbp','Feedback Form'),
    'clientOptions' => [
        'backdrop' => 'static',
    ],
]);
Modal::end();
