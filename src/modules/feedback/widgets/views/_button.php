<?php
use yii\helpers\Html;

/**
 * @var $btn_class string
 */
echo Html::a($icon.'Feedback','#',[
    'class'=>$btn_class?'feedback-button '.$btn_class:'feedback-button btn btn-primary',
]);

