<?php
/**@var $model \taktwerk\yiiboilerplate\modules\feedback\models\Feedback*/

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
$css = <<<CSS
.hidden, .kv-hidden{
    display: none !important;
}
CSS;
$this->registerCss($css);
if(!isset($is_screenshot)){
    $is_screenshot = true;
}
if(!isset($is_popup)){
    $is_popup = true;
}
if(!isset($message)){
    $message = Yii::t('twbp','If you found a software error or a data issue please send in your feedback.');
}
$feedback_create_url = Url::toRoute(['/feedback/feedback/new-feedback']);
$redirect_uri = Url::toRoute([Yii::$app->request->getPathInfo()]);
\taktwerk\yiiboilerplate\modules\feedback\assets\CanvasAsset::register($this);
?>
<div id="modalFormFeedbackSuccess" class="alert alert-success alert-dismissible <?=Yii::$app->request->get('success')==="1"?"":"hidden"?>">
    <i class="icon fa fa-check"></i> <?=Yii::t('twbp',"Feedback submitted successfully.")?>
</div>
<?php if($message) : ?>
<p><?=$message?></p>
<?php endif; ?>
<div class="feedback-form">
    <?php $form = ActiveForm::begin([
        'id' => 'Feedback-Modal',
        'layout' => 'default',
        'enableClientValidation' => true,
        'action' => $feedback_create_url,
        'method' => 'post',
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'options' => [
            'name' => 'Feedback',
            'enctype' => 'multipart/form-data'
        ],
    ]);
    ?>
    <?php if(Yii::$app->user->isGuest){?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field(
                    $model,
                    'name',
                    [
                        'selectors' => [
                            'input' => '#' .
                                Html::getInputId($model, 'name') . $owner
                        ]
                    ]
                )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('name'),
                            'id' => Html::getInputId($model, 'name') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('name'));?>
            </div>
            <div class="col-md-6">
                <?= $form->field(
                    $model,
                    'email',
                    [
                        'selectors' => [
                            'input' => '#' .
                                Html::getInputId($model, 'email') . $owner
                        ]
                    ]
                )
                    ->textInput(
                        [
                            'type' => 'email',
                            'id' => Html::getInputId($model, 'email') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('email'));?>
            </div>
        </div>
    <?php }?>

    <?php
    echo $form->field(
        $model,
        'description'
    )
        ->textarea(
            [
                'rows' => 6,
                'placeholder' => $model->getAttributePlaceholder('description'),
            ]
        )
        ->hint($model->getAttributeHint('description'));
    ?>
    <div class="row">
        <div class="col-md-<?=!$is_screenshot?'12':'6'?>">
            <?php
            echo $form->field(
                $model,
                'attached_file'
            )->widget(
                \taktwerk\yiiboilerplate\widget\FileInput::class,
                [
                    'options'       => ['id' => 'feedback-file-upload-by-modal'],
                    'pluginOptions' => [
                        'showUpload' => false,
                        'showRemove' => false,
                        'initialPreview' => (!empty($model->attached_file) ? [
                            $model->getFileUrl('attached_file')
                        ] : ''),
                        'initialCaption' => $model->attached_file,
                        'initialPreviewAsData' => true,
                        'initialPreviewFileType' => $model->getFileType('attached_file'),
                        'fileActionSettings' => [
                            'indicatorNew' => $model->attached_file === null ? '' : '<a href=" ' . $model->getFileUrl('attached_file') . '" target="_blank"><i class="glyphicon glyphicon-hand-down text-warning"></i></a>',
                            'indicatorNewTitle' => \Yii::t('twbp','Download'),
                        ],
                        'overwriteInitial' => true,
                        'browseClass' => 'btn btn-primary btn-file btn-feedback',
                        'browseIcon' => '<i class="fa fa-folder-open"></i> ',
                    ],
                    'pluginEvents' => [
                        'fileclear' => 'function() { var prev = $("input[name=\'" + $(this).attr("name") + "\']")[0]; $(prev).val(-1); }',

                    ],
                ]
            )
                ->hint($model->getAttributeHint('attached_file'));
            ?>
        </div>
        <div class="col-md-6 <?=!$is_screenshot?'hidden':''?>">
            <?=Html::label(Yii::t('twbp','Screenshot'),'canvas',['class'=>'control-label '.(!$is_screenshot?'hidden':'')])?>
            <img id="canvas" width="100%" height="100%" src="" alt="" style="margin-bottom: 5px" class="hidden">
            <div class="canvas-spin text-center vertical-center hidden"><i class="fa fa-3x fa-spinner fa-spin" aria-hidden="true"></i></div>
            <input name="screenshot" id="screenshot" type="hidden" value="">
        </div>
    </div>
    <?php
    echo $form->field($model,'reference_model')->hiddenInput()->label(false);
    echo $form->field($model,'feedback_url')->hiddenInput()->label(false);
    echo $form->field($model,'reference')->hiddenInput()->label(false);
    ?>
    <?php
    if($captcha){
        echo $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha2::class);
    }
    ?>
    <div class="submitted-form">
        <?php
        echo Html::button(
            '<i class="fa fa-camera" aria-hidden="true"></i> ' .
            Yii::t('twbp', 'Screenshot'),[
                'class'=>'btn btn-default '. (!$is_screenshot?'hidden':''),
                'id'=>'take-screenshot',
                'style'=>"margin-right: 10px"
            ]
        );
        echo Html::button(
            '<i class="fa fa-check-square" aria-hidden="true"></i> ' .
            Yii::t('twbp', 'Submit'),[
                'class'=>'btn btn-primary',
                'id' => 'send-feedback',
                'type'=>"submit"
            ]
        );
        ?>
    </div>
    <div class="close-form hidden">
        <?php
        if($is_popup){
            echo Html::button(Yii::t('twbp', 'Close'),[
                    'class'=>'btn btn-default btn-secondary',
                    'data-dismiss'=>'modal'
                ]
            );
        }else{
            echo Html::button(
                '<i class="fa fa-check-square" aria-hidden="true"></i> ' .
                Yii::t('twbp', 'Submit'),[
                    'class'=>'btn btn-primary',
                    'id' => 'send-feedback',
                    'type'=>"submit"
                ]
            );
        }?>
    </div>

    <?php
    ActiveForm::end();
    $js = <<<JS
$(document).ready(function(){
    $('#take-screenshot').on('click',function(){
        $('.canvas-spin').addClass('show');
        $('.canvas-spin').removeClass('hidden');
        html2canvas($('.wrapper').get(0)).then(function(canvas) {
          $('#canvas').removeClass('hidden');
          $('#canvas').addClass('show');
          $('.canvas-spin').removeClass('show');
          $('.canvas-spin').addClass('hidden');
          $('#canvas').attr('src',canvas.toDataURL('image/png'));
          $('#screenshot').attr('value',canvas.toDataURL('image/png'));
        });
    });
    
    $(document).on("submit",'#Feedback-Modal',function (e){
        e.preventDefault();
            $('#send-feedback').html('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>');
            var form = $(this)[0];
            var formData = new FormData(form);
            $.ajax({
                   type: "POST",
                   url: "$feedback_create_url",
                   processData: false,
                   contentType: false,
                   data: formData,
                   success: function(data)
                   {
                       if(data==true){
                        $('#modalFormFeedbackSuccess').addClass('show');
                        $('#modalFormFeedbackSuccess').removeClass('hidden');
                        $('.close-form').addClass('show');
                        $('.close-form').removeClass('hidden');
                        $('.submitted-form').remove();
                        if("$captcha"==1 && "$is_popup"!==1){
                            window.location.href = "$redirect_uri" + "?success=1";
                        }
                       }else{
                            window.location.href = "$redirect_uri";
                       }
                   }
                 });
    });
});
JS;
    $this->registerJs($js,\yii\web\View::POS_END);
    ?>
</div>