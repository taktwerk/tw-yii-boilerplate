<?php

namespace taktwerk\yiiboilerplate\modules\feedback\assets;

use yii\helpers\FileHelper;
use yii\web\AssetBundle;

class CanvasAsset extends AssetBundle
{
    public $sourcePath = '@taktwerk-boilerplate/modules/feedback/assets/web';

    public $js = [
        'js/html2canvas.js'
    ];
    public $jsOptions = [
        ['position' => \yii\web\View::POS_HEAD]
    ];

    public function init()
    {
        parent::init();

        // /!\ CSS/LESS development only setting /!\
        // Touch the asset folder with the highest mtime of all contained files
        // This will create a new folder in web/assets for every change and request
        // made to the app assets.
        if (getenv('APP_ASSET_FORCE_PUBLISH')) {
            $files = FileHelper::findFiles(\Yii::getAlias($this->sourcePath));
            $mtimes = [];
            foreach ($files as $file) {
                $mtimes[] = filemtime($file);
            }
            touch(\Yii::getAlias($this->sourcePath), max($mtimes));
        }
    }
}
