<?php

namespace taktwerk\yiiboilerplate\modules\feedback;

use taktwerk\yiiboilerplate\components\MenuItemInterface;

class Module extends \taktwerk\yiiboilerplate\components\TwModule implements MenuItemInterface
{
    
    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\feedback\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function getMenuItems()
    {
        $items = [
            [
                'label' => \Yii::t('twbp', 'Feedback'),
                'url' => ['/feedback/feedback-setting'],
                'icon' => 'fa fa-caret-right',
                'visible' => \Yii::$app->user->can('x_feedback_feedback-setting_see')
            ],
        ];
        return $items;
    }
}
