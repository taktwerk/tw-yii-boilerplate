<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%digi_screen_layout}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%digi_screen}}`
 */
class m210603_110115_create_digi_screen_layout_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%digi_screen_layout}}', [
            'id' => $this->primaryKey(),
            'digi_screen_id' => $this->integer(11)->notNull(),
            'layout' => $this->string(255),
            'keycode' => $this->string(255),
        ]);
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\digisign"}';
        $this->addCommentOnTable('{{%digi_screen_layout}}', $comment);
        // creates index for column `digi_screen_id`
        $this->createIndex(
            'idx-digi_screen_layout-digi_screen_id',
            '{{%digi_screen_layout}}',
            'digi_screen_id'
        );

        // add foreign key for table `{{%digi_screen}}`
        $this->addForeignKey(
            'fk-digi_screen_layout-digi_screen_id',
            '{{%digi_screen_layout}}',
            'digi_screen_id',
            '{{%digi_screen}}',
            'id',
            'CASCADE'
        );
        $this->createIndex('idx_digi_screen_layout_deleted_at_digi_screen_id', '{{%digi_screen_layout}}', ['deleted_at','digi_screen_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    $this->dropIndex('idx_digi_screen_layout_deleted_at_digi_screen_id', '{{%digi_screen_layout}}');
            // drops foreign key for table `{{%digi_screen}}`
        $this->dropForeignKey(
            'fk-digi_screen_layout-digi_screen_id',
            '{{%digi_screen_layout}}'
        );

        // drops index for column `digi_screen_id`
        $this->dropIndex(
            'idx-digi_screen_layout-digi_screen_id',
            '{{%digi_screen_layout}}'
        );

        $this->dropTable('{{%digi_screen_layout}}');
    }
}
