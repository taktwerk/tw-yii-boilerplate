<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;
use taktwerk\yiiboilerplate\modules\digisign\models\DigiScreen;
use taktwerk\yiiboilerplate\modules\digisign\models\DigiScreenLayout;

class m210603_120054_move_layout extends TwMigration
{
    public function up()
    {
        $q = DigiScreen::find();
        foreach($q->batch() as $screens){
            foreach($screens as $screen){
                $layout = new DigiScreenLayout();
                $layout->digi_screen_id = $screen->id;
                $layout->layout = $screen->layout;
                $layout->created_by = $screen->created_by;
                $layout->updated_by = $screen->updated_by;
                if($layout->save()){
                    $screen->digi_screen_layout_id = $layout->id;
                    $screen->save();
                }
            }
        }
        $this->dropColumn('{{%digi_screen}}','layout');
    }

    public function down()
    {
        $this->addColumn( '{{%digi_screen}}','layout',$this->string(255)->notNull()->comment('key of the layout in digisign module\'s layout class mapping'));
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
