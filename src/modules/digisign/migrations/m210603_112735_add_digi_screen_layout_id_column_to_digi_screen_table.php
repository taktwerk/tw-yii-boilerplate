<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;
use taktwerk\yiiboilerplate\modules\digisign\models\DigiScreen;

/**
 * Handles adding columns to table `{{%digi_screen}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%digi_screen_layout}}`
 */
class m210603_112735_add_digi_screen_layout_id_column_to_digi_screen_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%digi_screen}}', 'digi_screen_layout_id', $this->integer(11)
            ->null()->after('scan_uri')->defaultValue(null)->comment('The layout which is currently displayed'));

        // creates index for column `digi_screen_layout_id`
        $this->createIndex(
            'idx-digi_screen-digi_screen_layout_id',
            '{{%digi_screen}}',
            'digi_screen_layout_id'
        );
        $this->createIndex('idx_digi_screen_deleted_at_digi_screen_layout_id', '{{%digi_screen}}', ['deleted_at','digi_screen_layout_id']);
        // add foreign key for table `{{%digi_screen_layout}}`
        $this->addForeignKey(
            'fk-digi_screen-digi_screen_layout_id',
            '{{%digi_screen}}',
            'digi_screen_layout_id',
            '{{%digi_screen_layout}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            // drops foreign key for table `{{%digi_screen_layout}}`
        $this->dropForeignKey(
            'fk-digi_screen-digi_screen_layout_id',
            '{{%digi_screen}}'
        );

        // drops index for column `digi_screen_layout_id`
        $this->dropIndex(
            'idx-digi_screen-digi_screen_layout_id',
            '{{%digi_screen}}'
        );
        $this->dropIndex(
            'idx_digi_screen_deleted_at_digi_screen_layout_id',
            '{{%digi_screen}}'
            );
        $this->dropColumn('{{%digi_screen}}', 'digi_screen_layout_id');
    }
}
