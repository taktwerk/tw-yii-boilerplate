<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%digi_screen}}`.
 */
class m201106_112327_add_scan_uri_column_to_digi_screen_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%digi_screen}}', 'scan_uri', $this->string(255)->null()->after('enabled'));
        $this->addColumn('{{%digi_screen}}', 'enable_barcode_scan', $this->boolean()->null()->after('enabled'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%digi_screen}}', 'scan_uri');
        $this->dropColumn('{{%digi_screen}}', 'enable_barcode_scan');
    }
}
