<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200907_063111_signage_public_permission2 extends TwMigration
{
    public function up()
    {
      //$this->createPermission('digisign_digi-sign_index', 'Digital Signage Screen', ['Public']);
      $this->removePermission('digisign_digi-screen_view',['Public']);
      $this->createPermission('digisign_digi-sign_view', 'Digital Signage Screen', ['Public']);
      
    }

    public function down()
    {
        echo "m200907_063101_signage_public_permission cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
