<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%digi_screen}}`.
 */
class m200907_054007_create_digi_screen_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tbl = "{{%digi_screen}}";
        
        $this->createTable($tbl, [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'description' => $this->text(),
            'layout' => $this->string(255)->notNull()->comment('key of the layout in digisign module\'s layout class mapping'),
            'passcode' =>$this->string(255),
            'enabled' => $this->boolean()
        ]);
        
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\digisign"}';
        $this->addCommentOnTable($tbl, $comment);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            $this->dropTable('{{%display_device}}');
    }
}
