<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m210604_113701_signage_layout_change_public_permission extends TwMigration
{
    public function up()
    {
        $this->createPermission('digisign_digi-sign_layout-change', 'Digital Signage Layout change', ['Public']);
    }

    public function down()
    {
        echo "m210604_113701_signage_layout_change_public_permission cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
