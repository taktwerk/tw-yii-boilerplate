<?php
use yii\helpers\Url;
$layoutUrl = Url::toRoute(['/digisign/digi-sign/layout-check','id'=>$screenId]);
$js = <<<EOT
setInterval(function(){
    $.ajax({
        url:"$layoutUrl",
        success:function(data){
            if(data==true){
            window.location.reload();
            }
        }
    });
},7000)
EOT;
$this->registerJs($js);
?>
<div class="centered">
<div class="gigantic text-center">
    <p>THIS SCREEN HAS NOT YET BEEN ENABLED</p>
    <p>PLEASE GO TO</p>
    
    <p><a href="<?= $url ?>"><?= $url ?></a></p>
    <p>AND ACTIVATE THIS SCREEN</p>
</div>

<div class="huge text-center">
    <p>The browser will automatically refresh shortly after</p>
</div>
</div>