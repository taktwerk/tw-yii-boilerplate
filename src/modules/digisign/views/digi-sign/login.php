<?php
use taktwerk\yiiboilerplate\widget\form\ActiveForm;
$js = <<<EOT
$('#passcode-in').on('keyup', function() {
     if (this.value.length == {$passLength}) {
          $('#passcode-form')[0].submit();
     }
});
$(document).keypress(function(event){
var el =  $('#passcode-in');
    if(el.is(":focus")){
        return;
    }
    var key = (event.keyCode ? event.keyCode : event.which); 
    var ch=String.fromCharCode(key); 
    if(ch){
        var newVal = el.val()+ch;
        el.focus(); 
    }     
});
EOT;
$this->registerJs($js);
?>
<?php
$form = ActiveForm::begin([
    'id' => 'passcode-form',
    'options' => [
        'class' => 'centered'
    ]
]);
?>
<?=$form->field($model, 'password', [ 'template'=>'{input}','inputOptions' => ['autofocus' => true]])->passwordInput(['id' => 'passcode-in','placeholder' => \Yii::t('twbp','Screen Passcode')])->label(false)?>
<?php ActiveForm::end(); ?>