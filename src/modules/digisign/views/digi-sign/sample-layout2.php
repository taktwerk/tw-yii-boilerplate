<?php 
$css = <<<EOT
.trafficlight {
  width: 60px;
  margin: auto;
}

.box {
  height: 132px;
    width: 70px;
    background: black;
    border: 6px solid lightgrey;
}

.toppole {
  height: 60px;
  width: 30px;
  background: black;
  margin-left: 21px;
}

.smallbox {
    height: 72px;
    width: 61px;
    background: black;
    border: 6px solid lightgrey;
    margin-left: 5px;
}

.word {
  font-family: sans-serif;
  margin: 0;
  padding: 0;
  text-align: center;
}

.walk {
  color: green;
  margin-top: 7px;
}

.stay {
  color: red;
  margin-top: 10px;
  opacity: 0.4;
}

.bottompole {
  height: 200px;
  width: 30px;
  background: black;
  margin-left: 21px;
}

.light {
  height: 30px;
  width: 30px;
  border-radius: 50%;
  margin-left: 15px;
  margin-top: 8px;
}

.red {
  background-color:red;
}

.orange {
  background-color:orange;
  opacity: 0.4;
}

.green {
  background-color:green;
  opacity: 0.4;
}
EOT;
$this->registerCss($css);

$js = <<<EOT
//lights

function redorange() {
  $('#orangelight').css('opacity', 1)
};

function green() {
  $('#redlight').css('opacity', 0.4)
  $('#orangelight').css('opacity', 0.4)
  $('#greenlight').css('opacity', 1)
  clearInterval(first);
};

function orange() {
  $('#orangelight').css('opacity', 1)
  $('#greenlight').css('opacity', 0.4)
  clearInterval(second);
};

function red() {
  $('#redlight').css('opacity', 1)
  $('#orangelight').css('opacity', 0.4)
  clearInterval(third);
  first = setInterval(redorange, 4000);
  second = setInterval(green, 5000);
  third = setInterval(orange, 8000);
}; 
  
var first = setInterval(redorange, 4000);

var second = setInterval(green, 5000);

var third = setInterval(orange, 8000);

var fourth = setInterval(red, 10000);

//words

function stay() {
	$('#walkword').css('opacity', 0.4)
	$('#stayword').css('opacity', 1)
};

function walk() {
	$('#walkword').css('opacity', 1)
	$('#stayword').css('opacity', 0.4)
	clearInterval(wfirst);
	wfirst = setInterval(stay, 2000);
	clearInterval(wsecond);
	wsecond = setInterval(walk, 10000);
};

var wfirst = setInterval(stay, 3000);
var wsecond = setInterval (walk, 11000);
EOT;
$this->registerJs($js);
?>
<div class="trafficlight centered">
<div class="box">
  <div class="red light" id="redlight">
  </div>
  <div class="orange light" id="orangelight">
  </div>
  <div class="green light" id="greenlight">
  </div>
</div>
<div class="toppole">
</div>
<div class="smallbox">
  <p class="walk word" id="walkword">WALK</p>
  <p class="stay word" id="stayword">STAY</p>
</div>
<div class="bottompole">
</div>
</div>