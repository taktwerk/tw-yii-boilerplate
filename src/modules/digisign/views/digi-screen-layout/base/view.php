<?php
//Generation Date: 04-Jun-2021 05:27:51am
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\digisign\models\DigiScreenLayout $model
 * @var boolean $useModal
 */

$this->title = Yii::t('twbp', 'Digi Screen Layout') . ' #' . (is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey) . ', ' . Yii::t('twbp', 'View') . ' - ' . $model->toString();
if(!$fromRelation) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('twbp', 'Digi Screen Layouts'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => (string)is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('twbp', 'View');
}
$basePermission = Yii::$app->controller->module->id . '_' . Yii::$app->controller->id;

Yii::$app->controller->renderCustomBlocks(Yii::$app->controller::POS_MAIN);

?>
<div class="box box-default">
	<div class="giiant-crud box-body"
		id="digi-screen-layout-view">

		<!-- flash message -->
        <?php if (Yii::$app->session->getFlash('deleteError') !== null) : ?>
            <span class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= Yii::$app->session->getFlash('deleteError') ?>
            </span>
        <?php endif; ?>
        <?php if (!$useModal) : ?>
            <div class="clearfix crud-navigation">
			<!-- menu buttons -->
			<div class='pull-left'>
			<?php if (!$model->deleted_at!=null) : ?>
                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_update'
                    ) && $model->editable() ?
                        Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span> ' . Yii::t('twbp', 'Edit'),
                            [
                                'update',
                                'id' => $model->id,                                 'fromRelation' => $fromRelation,
                                'show_deleted'=>Yii::$app->request->get('show_deleted')
                            ],
                            ['class' => 'btn btn-info']
                        )
                    :
                        null
                    ?>
                     <?php if(method_exists($model,'getUploadFields')  && Yii::$app->getUser()->can(
                        $basePermission . '_recompress'
                    )){
                    	$fields = $model->getUploadFields();
                    	$isCompressible = false;
                    	$compressibleFileTypes = $model::$compressibleFileTypes;
                    	foreach($fields as $field){
                        	if(in_array($model->getFileType($field),$compressibleFileTypes)){
                            	$isCompressible = true;
                            	break;
                        	}
                    	}
                    if($isCompressible){	
                       echo Html::a(
                            '<span class="glyphicon glyphicon-compressed"></span> ' . Yii::t('twbp', 'Recompress'),
                            ['recompress', 'id' => $model->id, 'fromRelation' => $fromRelation],
                            ['class' => 'btn btn-warning','title'=>Yii::t('twbp', 'Adds a compression job for this item\'s files')]
                        );}
                   
                    }?>

                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_create'
                    ) ?
                        Html::a(
                            '<span class="glyphicon glyphicon-copy"></span> ' . Yii::t('twbp', 'Copy'),
                            ['create', 'id' => $model->id, 'fromRelation' => $fromRelation],
                            ['class' => 'btn btn-success']
                        )
                    :
                        null
                    ?>
                    <?php endif; ?>
                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_create'
                    ) ?
                        Html::a(
                            '<span class="glyphicon glyphicon-plus"></span>
				' . Yii::t('twbp', 'New'), ['create', 'fromRelation' =>
				$fromRelation], ['class' => 'btn btn-success create-new'] ) : null
				?>
			</div>
			<div class="pull-right">
			<?php if (!$model->deleted_at!=null) : ?>
                    <?= \taktwerk\yiiboilerplate\widget\CustomActionDropdownWidget::widget(['model'=>$model]) ?>
                    <?php if(Yii::$app->controller->crudMainButtons['listall']): ?>
                    <?= Html::a(
                        '<span class="glyphicon glyphicon-list"></span> '. Yii::t('twbp', 'List {model}',
                            ['model' => \Yii::t('twbp','Digi Screen Layouts')]                        ),
                        ['index'],
                        ['class' => 'btn btn-default']
                    ) ?>
                    <?php endif; ?>
            <?php endif; ?>                </div>
		</div>
        <?php endif; ?>
        <?php $this->beginBlock('taktwerk\yiiboilerplate\modules\digisign\models\DigiScreenLayout'); ?>

        <?php $viewColumns = [
                   [
            'attribute'=> 'id'
           ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::attributeFormat
                [
                    'format' => 'html',
                    'attribute' => 'digi_screen_id',
                    'value' => function ($model) {
                        $foreign = $model->getDigiScreen()->one();
                        if ($foreign) {
                            if (Yii::$app->getUser()->can('app_digi-screen_view') && $foreign->readable()) {
                                return Html::a($foreign->toString, [
                                    'digi-screen/view',
                                    'id' => $model->getDigiScreen()->one()->id,
                                ], ['data-pjax' => 0]);
                            }
                            return $foreign->toString;
                        }
                        return '<span class="label label-warning">?</span>';
                    },
                ],
[
        'attribute' => 'layout',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->layout != strip_tags($model->layout)){
                return \yii\helpers\StringHelper::truncate($model->layout,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'layout'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'layout'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->layout,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'layout'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'layout'])]])));
            }
        },
    ],
[
        'attribute' => 'keycode',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->keycode != strip_tags($model->keycode)){
                return \yii\helpers\StringHelper::truncate($model->keycode,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'keycode'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'keycode'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->keycode,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'keycode'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'keycode'])]])));
            }
        },
    ],
        ];

        // view columns overwrite
        $viewColumns = Yii::$app->controller->crudColumnsOverwrite($viewColumns, 'view');

        echo DetailView::widget([
        'model' => $model,
        'attributes' => $viewColumns
        ]); ?>

        
        <hr />

        <?=  ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::deleteButton($model, "twbp", ['id' => $model->id], 'view')?>

        <?php $this->endBlock(); ?>


        
        <?php $this->beginBlock('Digi Screens'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsDigiScreens = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('digisign_digi-screen_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('digisign_digi-screen_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('digisign_digi-screen_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/digisign/digi-screen' . '/' . $action;
                        $params['DigiScreen'] = [
                            'digi_screen_layout_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'digi-screen'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'name',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'name'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'name'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'name'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'name'])]])));
            }
        },
    ],
[
        'attribute' => 'description',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->description != strip_tags($model->description)){
                return \yii\helpers\StringHelper::truncate($model->description,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'description'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'description'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->description,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'description'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'description'])]])));
            }
        },
    ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'production_line_id',
                    'value' => function ($model) {
                        if ($rel = $model->getProductionLine()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'production-line/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'passcode',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->passcode != strip_tags($model->passcode)){
                return \yii\helpers\StringHelper::truncate($model->passcode,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'passcode'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'passcode'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->passcode,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'passcode'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'passcode'])]])));
            }
        },
    ],
[
        'attribute' => 'enabled',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->enabled != strip_tags($model->enabled)){
                return \yii\helpers\StringHelper::truncate($model->enabled,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'enabled'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'enabled'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->enabled,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'enabled'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'enabled'])]])));
            }
        },
    ],
[
        'attribute' => 'enable_barcode_scan',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->enable_barcode_scan != strip_tags($model->enable_barcode_scan)){
                return \yii\helpers\StringHelper::truncate($model->enable_barcode_scan,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'enable_barcode_scan'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'enable_barcode_scan'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->enable_barcode_scan,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'enable_barcode_scan'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'enable_barcode_scan'])]])));
            }
        },
    ],
[
        'attribute' => 'scan_uri',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->scan_uri != strip_tags($model->scan_uri)){
                return \yii\helpers\StringHelper::truncate($model->scan_uri,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'scan_uri'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'scan_uri'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->scan_uri,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'scan_uri'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'scan_uri'])]])));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\digisign\models\DigiScreen::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsDigiScreens = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsDigiScreens, 'tab'):$columnsDigiScreens;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('digisign_digi-screen_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Digi Screen'),
                [
                    '/digisign/digi-screen/create',
                    'DigiScreen' => [
                        'digi_screen_layout_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getDigiScreens(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-digiscreens',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsDigiScreens
        ])?>
        </div>
                <?php $this->endBlock() ?>

<?php /* MAPPED MODELS RELATIONS - START*/?>
<?php 
$extraTabItems = [];
foreach(\Yii::$app->modelNewRelationMapper->newRelationsMapping as $modelClass=>$relations){
    if(is_a($model,$modelClass)){
        foreach($relations as $relName => $relation){
            $relationKey = array_key_first($relation[1]);
            $foreignKey= $relation[1][$relationKey];
            $relClass = $relation[0];
            $baseName = \yii\helpers\StringHelper::basename($relClass);
            $cntrler = Inflector::camel2id($baseName, '-', true);
            $pluralName= Inflector::camel2words(Inflector::pluralize($baseName));
            $singularName = Inflector::camel2words(Inflector::singularize($baseName));
            $basePerm =  \Yii::$app->controller->module->id.'_'.$cntrler;
            $this->beginBlock($pluralName);
?>
<?php 
$relatedModelObject = Yii::createObject($relClass::className());
$relationController = $relatedModelObject->getControllerInstance();
//TODO CrudHelper::getColumnFormat('\app\modules\guide\models\Guide', 'id');
?>
                    <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
       	<div class="clearfix"></div>
        <div>
        <?php $relMapCols = [[
                    'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can($basePerm.'_view') ? '{view} ' : '') . (Yii::$app->getUser()->can($basePerm.'_update') ? '{update} ' : '') . (Yii::$app->getUser()->can($basePerm.'_delete') ? '{delete} ' : ''),
            'urlCreator' => function ($action, $relation_model, $key, $index) use($model,$relationKey,$foreignKey,$baseName,$relatedBaseUrl) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = $relatedBaseUrl . $action;
                        
                        $params[$baseName] = [
                            $relationKey => $model->{$foreignKey}
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) { 
                             return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => $cntrler
                ],
                
        ];

        $relMapCols =array_merge($relMapCols,array_slice(array_keys($relatedModelObject->attributes),0,5));
        
        $relMapCols = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($relMapCols, 'tab'):$relMapCols;
        
        echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can($basePerm.'_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp',$singularName),
                [
                    '/'.$relationController->module->id.'/'.$cntrler.'/create',
                    $baseName => [
                        $relationKey => $model->{$foreignKey}
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[
                'class'=>'grid-outer-div'
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->{'get'.$relName}(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-'.$relName,
                    ]
                ]
                ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            'columns' =>$relMapCols
        ])?>
        </div>
                <?php $this->endBlock();
                $extraTabItems[] = [
                    'content' => $this->blocks[$pluralName],
                    'label' => '<small>' .
                    \Yii::t('twbp', $pluralName) .
                    '&nbsp;<span class="badge badge-default">' .
                    $model->{'get'.$relName}()->count() .
                    '</span></small>',
                    'active' => false,
                    'visible' =>
                    Yii::$app->user->can('x_'.$basePerm.'_see') && Yii::$app->controller->crudRelations(ucfirst($relName)),
                    ];
?>

<?php 
        }
    }
}
?>

                
<?php /* MAPPED MODELS RELATIONS - END*/ ?>
        <?= Tabs::widget(
            [
                'id' => 'relation-tabs',
                'encodeLabels' => false,
                'items' =>  array_filter(array_merge([
                    [
                        'label' => '<b class=""># ' . $model->id . '</b>',
                        'content' => $this->blocks['taktwerk\yiiboilerplate\modules\digisign\models\DigiScreenLayout'],
                        'active' => true,
                    ],
        (\Yii::$app->hasModule('digisign'))?[
                        'content' => $this->blocks['Digi Screens'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Digi Screens') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getDigiScreens()->groupBy('digi_screen.id')->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_digisign_digi-screen_see') && Yii::$app->controller->crudRelations('DigiScreens'),
                    ]:null,
                    [
                        'content' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\HistoryTab::class)::widget(['model' => $model]),
                        'label' => '<small>' .
                            \Yii::t('twbp', 'History') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getHistory()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('Administrator'),
                    ],
                ]
                ,$extraTabItems))
            ]
        );
        ?>
        <?= ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\RecordHistory::class)::widget(['model' => $model]) ?>
    </div>
</div>

<?php ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
$js = <<<JS
$(document).ready(function() {
    var hash=document.location.hash;
	var prefix="tab_" ;
    if (hash) {
        $('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
    } 
    
    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash=e.target.hash.replace(	"#", "#" + prefix);
    });
});
JS;

$this->registerJs($js);

Yii::$app->controller->renderCustomBlocks(Yii::$app->controller::POS_END);
