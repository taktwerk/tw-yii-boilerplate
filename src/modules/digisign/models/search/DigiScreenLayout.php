<?php
//Generation Date: 03-Jun-2021 11:52:33am
namespace taktwerk\yiiboilerplate\modules\digisign\models\search;

use taktwerk\yiiboilerplate\modules\digisign\models\search\base\DigiScreenLayout as DigiScreenLayoutSearchModel;

/**
* DigiScreenLayout represents the model behind the search form about `taktwerk\yiiboilerplate\modules\digisign\models\DigiScreenLayout`.
*/
class DigiScreenLayout extends DigiScreenLayoutSearchModel{

}