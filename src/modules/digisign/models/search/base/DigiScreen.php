<?php
//Generation Date: 04-Jun-2021 05:27:50am
namespace taktwerk\yiiboilerplate\modules\digisign\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\digisign\models\DigiScreen as DigiScreenModel;

/**
 * DigiScreen represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\digisign\models\DigiScreen`.
 */
class DigiScreen extends DigiScreenModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'name',
                    'description',
                    'production_line_id',
                    'passcode',
                    'enabled',
                    'enable_barcode_scan',
                    'scan_uri',
                    'digi_screen_layout_id',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$showDeleted = (property_exists(\Yii::$app->controller, 'model')
            &&
            (is_subclass_of(DigiScreenModel::class, \Yii::$app->controller->model) 
            	||
            	is_subclass_of(\Yii::$app->controller->model, DigiScreenModel::class))
            &&
              (Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model)
                ||
                Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model)))
            );
        $query = DigiScreenModel::find((Yii::$app->get('userUi')->get('show_deleted', DigiScreenModel::class) === '1'|| $showDeleted)?false:true);

        $this->parseSearchParams(DigiScreenModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(DigiScreenModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(DigiScreenModel::class),
                'params' => [
                    'page' => $this->parsePageParams(DigiScreenModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('production_line_id', $query);
        $this->applyHashOperator('enabled', $query);
        $this->applyHashOperator('enable_barcode_scan', $query);
        $this->applyHashOperator('digi_screen_layout_id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('name', $query);
        $this->applyLikeOperator('description', $query);
        $this->applyLikeOperator('passcode', $query);
        $this->applyLikeOperator('scan_uri', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(DigiScreenModel::class);
    }
}
