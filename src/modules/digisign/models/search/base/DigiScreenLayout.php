<?php
//Generation Date: 03-Jun-2021 11:52:33am
namespace taktwerk\yiiboilerplate\modules\digisign\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\digisign\models\DigiScreenLayout as DigiScreenLayoutModel;

/**
 * DigiScreenLayout represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\digisign\models\DigiScreenLayout`.
 */
class DigiScreenLayout extends DigiScreenLayoutModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'digi_screen_id',
                    'layout',
                    'keycode',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$showDeleted = (property_exists(\Yii::$app->controller, 'model')
            &&
            (is_subclass_of(DigiScreenLayoutModel::class, \Yii::$app->controller->model) 
            	||
            	is_subclass_of(\Yii::$app->controller->model, DigiScreenLayoutModel::class))
            &&
              (Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model)
                ||
                Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model)))
            );
        $query = DigiScreenLayoutModel::find((Yii::$app->get('userUi')->get('show_deleted', DigiScreenLayoutModel::class) === '1'|| $showDeleted)?false:true);

        $this->parseSearchParams(DigiScreenLayoutModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(DigiScreenLayoutModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(DigiScreenLayoutModel::class),
                'params' => [
                    'page' => $this->parsePageParams(DigiScreenLayoutModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('digi_screen_id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('layout', $query);
        $this->applyLikeOperator('keycode', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(DigiScreenLayoutModel::class);
    }
}
