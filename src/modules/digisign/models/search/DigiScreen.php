<?php
//Generation Date: 07-Sep-2020 02:11:50pm
namespace taktwerk\yiiboilerplate\modules\digisign\models\search;

use taktwerk\yiiboilerplate\modules\digisign\models\search\base\DigiScreen as DigiScreenSearchModel;
use yii\helpers\ArrayHelper;

/**
* DigiScreen represents the model behind the search form about `taktwerk\yiiboilerplate\modules\digisign\models\DigiScreen`.
*/
class DigiScreen extends DigiScreenSearchModel{

    public $layout;
    public function rules()
    {
        return array_merge(parent::rules(),[
            [['layout'],'safe']
        ]);
    }
    public function search($params){
        $dataProvider = parent::search($params);
        $query = $dataProvider->query;
        if(strlen($this->layout)>0){
            $query->joinWith('digiScreenLayout');
            $this->applyHashOperator('layout', $query,'digi_screen_layout');
        }
        return $dataProvider;
    }
}