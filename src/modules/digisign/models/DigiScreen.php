<?php
//Generation Date: 07-Sep-2020 02:11:50pm
namespace taktwerk\yiiboilerplate\modules\digisign\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\digisign\models\base\DigiScreen as BaseDigiScreen;
use yii\helpers\StringHelper;

/**
 * This is the model class for table "digi_screen".
 */
class DigiScreen extends BaseDigiScreen
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['scan_uri','trim'],
            ['layout', 'in', 'range' => array_keys(\Yii::$app->getModule('digisign')?\Yii::$app->getModule('digisign')->getDigiSignsList():[])],
            [['passcode'], 'string','length'=>4],
        ]);
    }
    /**
     * @inheritdoc
     */
    public function attributePlaceholders()
    {
        $placeholders = parent::attributePlaceholders();
        $placeholders['scan_uri']=getenv('DEFAULT_DIGISCREEN_BARCODE_SCAN_URI');
        return $placeholders;
    }
    
    
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $hints = parent::attributeHints();
        $hints['scan_uri'] = \Yii::t('twbp',"URL is required, if no global URL is set (see placeholder)");
        return $hints;
    }
    /**
     *
     * @return array
     */
    public function formDependentShowFields()
    {
        $fName = $this->formName();
        $dependentFields = [];
        
        $dependentFields['scan_uri'] = $this->generateDependentFieldRules('scan_uri', [
            "field" => $fName . "[scan_uri]",
            "container" => ".form-group-block",
            "rules" => [
                [
                    "name" => $fName . "[enable_barcode_scan]",
                    "operator"=>"is",
                    "value" =>  "1" //Show uri field if barcode scan is enabled
                ]
            ]
        ]);
        
        return $dependentFields;
    }
    /**
     * @return \taktwerk\yiiboilerplate\modules\digisign\components\DigiSignAbstract|NULL
    */
    public function getTemplate(){
        $layoutList = \Yii::$app->getModule('digisign')?\Yii::$app->getModule('digisign')->getDigiSignsList():[];
        if($this->layout==null){
            return null;
        }
        $template = isset($layoutList[$this->layout])?$layoutList[$this->layout]:null;
        if($template){
            $template->setDigiScreenModel($this);
        }
        return $template;
    }
    
    /**
     * 
     * @return NULL|string
     * @desc Returns the scan url if set in the model or globally 
    */
    public function getScanUrl(){
        $scanUrl = ($this->scan_uri!='')?$this->scan_uri:getenv('DEFAULT_DIGISCREEN_BARCODE_SCAN_URI');
        if($scanUrl==null){
            return null;
        }
        
        if(!StringHelper::startsWith($scanUrl,'http') && strpos($scanUrl, '://') === false){
            $scanUrl = \Yii::$app->urlManager->createAbsoluteUrl($scanUrl);
        }
        return $scanUrl;
    }
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['digi_screen_layout_id'] = Yii::t('twbp','Active Layout');
        $labels['layout'] = Yii::t('twbp','Active Layout');
        return $labels;
    }
    public function getActiveLayout(){
        return $this->digiScreenLayout?$this->digiScreenLayout:$this->getDigiScreenLayouts()->orderBy('created_at ASC')->one();
    }
    public function getLayout(){
       $l = $this->getActiveLayout();
       return $l?$l->layout:null;
    }
    public function changeLayoutByCode($code){
        $layout = $this->getDigiScreenLayouts()->where(['keycode'=>$code])->one();
        if($layout){
            $this->digi_screen_layout_id = $layout->id;
            return $this->save();
        }
        return false;
    }
}
