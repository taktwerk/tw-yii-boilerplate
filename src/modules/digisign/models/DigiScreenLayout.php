<?php
//Generation Date: 03-Jun-2021 11:48:55am
namespace taktwerk\yiiboilerplate\modules\digisign\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\digisign\models\base\DigiScreenLayout as BaseDigiScreenLayout;
use taktwerk\yiiboilerplate\modules\digisign\models\base\DigiScreen;

/**
 * This is the model class for table "digi_screen_layout".
 */
class DigiScreenLayout extends BaseDigiScreenLayout
{
    public function toString()
    {
        self::getLayoutTitle($this->layout);
        //return $this->keycode; // this attribute can be modified
    }
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['layout', 'in', 'range' => array_keys(\Yii::$app->getModule('digisign')?\Yii::$app->getModule('digisign')->getDigiSignsList():[])],
           [
               'layout',
               'unique',
               'targetAttribute' => ['digi_screen_id', 'layout'],
               'message'=>\Yii::t('twbp','This layout is already assigned to this screen')
           ],
            [
                'digi_screen_id',
                'checkDigiScreen'
            ],
            ['keycode','string','length' => [4, 10]]
           ]);
    }
    public function checkDigiScreen($attribute, $params, $validator)
    {
        if(!$this->isNewRecord){
           $digiScreen = DigiScreen::find()->where(['digi_screen_layout_id'=>$this->id])->one();
           if($digiScreen){
               if($digiScreen->id!= $this->digi_screen_id){
                   $this->addError('digi_screen_id',\Yii::t('twbp','This layout is currently active in the screen, Please change/remove the active layout of that digi screen to assign this to a different digi screen'));
               }
           }
        }
    }
    public static function getLayoutInstance($layoutCode){
        $list = \Yii::$app->getModule('digisign')->getDigiSignsList();
        return (strlen($layoutCode)>0 && isset($list[$layoutCode]))?$list[$layoutCode]:null;
    }
    public static function getLayoutTitle($layoutCode){
        $layout = self::getLayoutInstance($layoutCode);
        return $layout?$layout->getDigiSignTitle():'';
    }
}
