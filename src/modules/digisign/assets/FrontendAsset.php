<?php

namespace taktwerk\yiiboilerplate\modules\digisign\assets;

use yii\web\AssetBundle;

class FrontendAsset extends AssetBundle
{
    public $sourcePath = '@vendor/taktwerk/yii-boilerplate/src/modules/digisign/assets/web';
    public $css = [
        'css/frontend.css',
    ];
    public $js = [
        'js/frontend.js',
    ];
    public $depends = [
         'taktwerk\yiiboilerplate\modules\digisign\assets\AppAsset',
    ];
}
