<?php

namespace taktwerk\yiiboilerplate\modules\digisign\assets;

use yii\web\AssetBundle;

class OfflineAsset extends AssetBundle
{
    public $sourcePath = '@vendor/taktwerk/yii-boilerplate/src/modules/digisign/assets/web';
    public $css = [
        'css/offline.css',
    ];
    public $js = [
        'js/offline.js',
    ];
    public $depends = [
    'yii\web\JqueryAsset',
    ];
}
