<?php

namespace taktwerk\yiiboilerplate\modules\digisign;

use taktwerk\yiiboilerplate\components\MenuItemInterface;
use yii\base\InvalidConfigException;
use taktwerk\yiiboilerplate\modules\digisign\components\DigiSignInterface;
use taktwerk\yiiboilerplate\modules\digisign\components\DigiSignAbstract;

/**
 * env module definition class
 */
class Module extends \taktwerk\yiiboilerplate\components\TwModule implements MenuItemInterface
{
    public $defaultRoute = 'digi-sign';
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\digisign\controllers';
    
    public $digiSignList = [];
    
    private $defaultDigiSignList = [
        'layout1' => '\taktwerk\yiiboilerplate\modules\digisign\components\Layout1',
        'layout2' => '\taktwerk\yiiboilerplate\modules\digisign\components\Layout2',
        'production_line'=>'\taktwerk\yiiboilerplate\modules\guide\components\ProductionLineDigiSign'
    ];
    
    public function getMenuItems()
    {
        // example for settings entry
         $items = [
            [
                'label' => \Yii::t('twbp', 'Digital Signage'),
                'url' => ['/digisign/digi-screen'],
                'icon' => 'fa fa-paint-brush ',
                'visible' => \Yii::$app->user->can('Authority')
            ],
             [
                 'label' => \Yii::t('twbp', 'Digital Signage Layouts'),
                 'url' => ['/digisign/digi-screen-layout'],
                 'icon' => 'fa fa-square-o ',
                 'visible' => \Yii::$app->user->can('Authority')
             ],
        ];
        return $items; 
    }
    /**
    @return static|null \taktwerk\yiiboilerplate\modules\digisign\components\DigiSignAbstract[]
    */
    public function getDigiSignsList()
    {
        $list = array_merge($this->defaultDigiSignList, $this->digiSignList);
        
        $checkers = [];
        foreach ($list as $k => $item) {
            $ob = null;
            if (is_string($item)) {
                $ob = new $item();
            } elseif (is_array($item)) {
                if (! isset($item['class'])) {
                    throw new InvalidConfigException('Missing "class" config in the layout ' . $k);
                }
                $option = $item;
                unset($option['class']);
                $ob = new $item['class']($option);
            }
            if (! $ob instanceof DigiSignAbstract) {
                throw new InvalidConfigException('The layout class ' . get_class($ob) . ' must inherit the \taktwerk\yiiboilerplate\modules\digisign\components\DigiSignAbstract class');
            }
            $checkers[$k] = $ob;
        }
        return $checkers;
    }
}
