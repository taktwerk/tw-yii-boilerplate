<?php
// Generation Date: 07-Sep-2020 07:58:05am
namespace taktwerk\yiiboilerplate\modules\digisign\components;

use taktwerk\yiiboilerplate\modules\digisign\models\DigiScreen;
use yii\base\Component;

abstract class DigiSignAbstract extends Component implements DigiSignInterface
{

    private $_digiScreenModel = null;
    
    abstract public function getDigiSignHtml(): string;

    abstract public function getDigiSignTitle(): string;
    public function init(){
        parent::init();
        $this->attachEvents();
    }
    public function autoRefreshEnabled(): bool
    {
        return true;
    }

    public function setDigiScreenModel(DigiScreen $screen)
    {
        $this->_digiScreenModel = $screen;
    }

    public function getDigiScreenModel()
    {
        return $this->_digiScreenModel;
    }
    public function attachEvents(){
        
    }
    public function getDigiSignDataModels():array{
        return [];
    }
}