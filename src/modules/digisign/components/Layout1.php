<?php
//Generation Date: 07-Sep-2020 07:58:05am
namespace taktwerk\yiiboilerplate\modules\digisign\components;

class Layout1 extends DigiSignAbstract
{
    public function getDigiSignHtml():string{
        return \Yii::$app->controller->render('@taktwerk-boilerplate/modules/digisign/views/digi-sign/sample-layout');
    }
    public function getDigiSignTitle():string{
        return 'Analogue Clock';
    }
    public function autoRefreshEnabled():bool{
        return true;
    }
}