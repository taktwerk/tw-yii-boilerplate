<?php

namespace taktwerk\yiiboilerplate\modules\digisign\components;

use taktwerk\yiiboilerplate\modules\digisign\models\DigiScreen;

interface DigiSignInterface{
    
    /**
     * @desc Title of the Layout
     */
    public function getDigiSignTitle():string;
    /**
     * @desc Html content of the layout
     */
    public function getDigiSignHtml():string;
    /**
     * @desc Whether the digitalsignage should auto-refresh
     */
    public function autoRefreshEnabled():bool;
    /**
     * @desc Set the digiscreen model if it needs to be accessed in the template
     */
    public function setDigiScreenModel(DigiScreen $screen);
    /**
     * @desc Get the digiscreen model if it's set
     */
    public function getDigiScreenModel();
    //class names of the modesl e.g ['\app\models\Abc','\app\models\Xyz'];
    public function getDigiSignDataModels():array;
}