<?php
//Generation Date: 03-Jun-2021 11:52:33am
namespace taktwerk\yiiboilerplate\modules\digisign\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\modules\digisign\models\DigiScreenLayout;
use taktwerk\yiiboilerplate\widget\Select2;

/**
 * This is the class for controller "DigiScreenLayoutController".
 */
class DigiScreenLayoutController extends \taktwerk\yiiboilerplate\modules\digisign\controllers\base\DigiScreenLayoutController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\digisign\models\DigiScreenLayout';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\digisign\models\search\DigiScreenLayout';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
    public function init()
    {
        if ($this->module) {
            $list = $this->module->getDigiSignsList();
            foreach ($list as $key => $val) {
                $list[$key] = $val->getDigiSignTitle();
            }
            $this->crudColumnsOverwrite = [
                'crud' => [
                    'layout' => [
                        'attribute' => 'layout',
                        'format' => 'html',
                        'value' => function ($model) {
                        if ($model->layout && $this->module->getDigiSignsList()[$model->layout]) {
                            return $this->module->getDigiSignsList()[$model->layout]->getDigiSignTitle();
                        }
                        },
                    ],
                    
                            ],
                            ];
        }
        return parent::init();
    }
    public function formFieldsOverwrite($model, $form,$owner=null)
    {
        $list = $this->module->getDigiSignsList();
        foreach($list as $key => $val){
            $list[$key] = $val->getDigiSignTitle();
        }
        //$list = $dropDownList;
        $this->formFieldsOverwrite = [
            'layout'=>false,
            'last_auth'=>false,
            'digi_screen_layout_id'=>false
            /* 'after#description' =>
             $form->field(
             $model,
             'layout',
             )
             ->widget(
             Select2::class,
             [
             'data' => $list,
             'options' => [
             'placeholder' => Yii::t('twbp', 'Select a layout...'),
             'id' => 'tags' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
             'multiple' => false
             ],
             'maintainOrder' => true,
             'pluginOptions' => [
             'placeholder' => Yii::t('twbp', 'Select a layout...'),
             'allowClear' => true,
             'tags' => false,
             'maximumInputLength' => 45
             ]
             ]
             )
             ->hint($model->getAttributeHint('layout')), */
        ];
        $list = [];
        $digSigns =  \Yii::$app->getModule('digisign')->getDigiSignsList();
        foreach($digSigns as $key=>$digSigns){
            $list[$key] = DigiScreenLayout::getLayoutTitle($key);
        }
        $this->formFieldsOverwrite['layout'] = $form
        ->field($model,'layout')
        ->widget(
            Select2::class,
            [
                'data' =>$list,
                'options' => [
                    'placeholder' => \Yii::t('twbp', 'Select a layout...'),
                    'id' => 'tags' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                    'multiple' => false
                ],
                'maintainOrder' => true,
                'pluginOptions' => [
                    'placeholder' => \Yii::t('twbp', 'Select a layout...'),
                    'allowClear' => true,
                    'tags' => false,
                    'maximumInputLength' => 45
                ]
            ]
            )
            ->hint($model->getAttributeHint('layout'));
            return $this->formFieldsOverwrite;
    }
    
}
