<?php
// Generation Date: 07-Sep-2020 07:58:05am
namespace taktwerk\yiiboilerplate\modules\digisign\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;
use taktwerk\yiiboilerplate\modules\digisign\models\DigiScreen;
use yii\web\HttpException;
use taktwerk\yiiboilerplate\modules\digisign\assets\OfflineAsset;
use yii\web\JsExpression;
use yii\helpers\StringHelper;
use yii\web\View;
use yii\base\Event;
use yii\helpers\Url;


/**
 * This is the class for controller "DigiSignController".
 * @property \taktwerk\yiiboilerplate\modules\digisign\Module $module
 */
class DigiSignController extends Controller
{

    public $layout = 'frontend';

    /**
     * Additional actions for controllers, uncomment to use them
     *
     * @inheritdoc
     */
    /*
     * public function behaviors()
     * {
     * return ArrayHelper::merge(parent::behaviors(), [
     * 'access' => [
     * 'class' => AccessControl::class,
     * 'rules' => [
     * [
     * 'allow' => true,
     * 'actions' => [
     * 'index',
     * ],
     * 'roles' => ['@','*','?']
     * ]
     * // http://twapp.local/digisign/digi-sign
     * ]
     * ]
     * ]);
     * }
     */
    public function actionLayoutCheck($id)
    {
        \Yii::$app->response->format = 'json';
        $model = DigiScreen::findOne($id);
        $layoutList = $this->module->getDigiSignsList();

        if ($model == null || !$model->layout || ! isset($layoutList[$model->layout]) || $model->enabled==0) {
            return false;
        }
        return true;
    }
    public function actionLayoutChange($id,$code){
        \Yii::$app->response->format = 'json';
        $data = [
         'success'=>false   
        ];
        if($id & $code){
            $model = DigiScreen::findOne($id);
            if($model){
                $data['success'] = $model->changeLayoutByCode($code);
            }
        }
        return $data;
    }
    private function setUpOfflineHandling(){
        OfflineAsset::register(\Yii::$app->view);
        $reconnectMsg = \Yii::t('twbp','Server is offline. Trying to connect...');
        $js = <<<EOT
        $("body").append('<div class="offline-section" style="display:none"><i class="fa fa-exclamation-triangle"></i>&nbsp;{$reconnectMsg}</div>');

        Offline.options['checkOnLoad'] = false;
        Offline.options['interceptRequests'] = true;
        Offline.options['requests'] = false;
        
        Offline.on('down', function() {
           $(".offline-section").show();
        });
        Offline.on('up', function(){
           $(".offline-section").hide();
        });
        
        Offline.on('confirmed-down', function(){
           $(".offline-section").show();
        });
        
        var run = function(){
        if(Offline.state=="up")
            Offline.check();
        };
        setInterval(run, 7000);
EOT;
            \Yii::$app->view->registerJs($js);
    }
    /**
     * 
     * @param DigiScreen $digiScreenModel
     * @desc sets up bar code scanning widget and action for the digi-screen page if scan url is available
    */
    private function setUpBarCodeScanning(DigiScreen $digiScreenModel){
    $scanUrl = $digiScreenModel->getScanUrl();
    
    if($scanUrl==null){
        return;
    }
    $widget = \taktwerk\yiiboilerplate\widget\barcodescanner\BarcodeScanner::widget([
        'hiddenScanHtml'=>'',
        'hidden'=>true,
        'pluginOptions' => [
        "scanButtonKeyCode" => false,
        "scanButtonLongPressTime" => 500,
        "timeBeforeScanTest" => 100,
        "avgTimeByChar" => 30,
        "minLength" => 5,
        "suffixKeyCodes" => [
            9,
            13
        ],
        "prefixKeyCodes" => [],
        "ignoreIfFocusOn" => 'input',
        "stopPropagation" => false,
        "preventDefault" => false,
        "reactToKeydown" => true,
        "reactToPaste" => true,
        "singleScanQty" => 1,
        "reactToKeyDown" => true,
        "onScan"=>new JsExpression(
<<<EOT
function(sScanned, iQty){
    
    setTimeout(function(){
        //console.log('scanned:',sScanned);
        if(sScanned && sScanned[1] == 'X' && sScanned[2] == 'X') {
            if(sScanned[0] == 'L') {
                changeLayout(sScanned.slice(3));
            }
        }
        else {
            var htmlEl =$('<div style="display:none;" class="scan-alert alert alert-info"><span class="closebtn" onclick="removeFadeOut(this.parentElement,1000)">&times;</span></div>');
            //$("#scan-alert").remove();
            $("#extra-section").prepend(htmlEl);
            setTimeout(function(){
                $.ajax({
                url:"$scanUrl",
                method:'post',
                data:{q:sScanned,production_line_id:"{$digiScreenModel->production_line_id}"},
                success:function(q){
                        if(typeof q === 'object' && q !== null){
                            var el = htmlEl;
                            if(q.success==true){
                                el.removeClass('alert-danger alert-info').addClass('alert-success');
                            } else if(q.success==false){
                                el.removeClass('alert-success alert-info').addClass('alert-danger');
                            }
                            if(q.message){
                                var msgs = null;
                                if(typeof q.message === 'string'){
                                    var msgs = [q.message];
                                }else if(Array.isArray(q.message)){
                                    var msgs = q.message;
                                }
                                if(msgs){
                                    var liHtml='';
                                    var text ='';
                                    msgs.forEach(function(f){
                                           var errText = $("<span></span>").text(f);
                                           liHtml += $("<li class='list-group-item text-dots'></li>").text(errText.text())[0].outerHTML;
                                    });
                                    var errorUl = $("<ul class='list-group'></ul>").html(liHtml);
                                    el.append(errorUl[0].outerHTML);
                                    el.show();
                               }
                            }
                            setTimeout(function(){ removeFadeOut(el[0]);},4900);
                        }
                    }
               });
            },400);
        }
    },200);
}
EOT
)
    ],
]);
    
    Event::on(View::class, View::EVENT_END_PAGE, function($event) use($widget){
      echo $widget;
    }); 
$js = <<<EOT
function removeFadeOut(el, speed=1000) {
    if(el!=undefined){
        var seconds = speed/1000;
        el.style.transition = "opacity "+seconds+"s ease";
    
        el.style.opacity = 0;
        setTimeout(function() {
            if(el.parentNode){
                el.parentNode.removeChild(el);
            }
        }, speed);
    }
}
EOT;
\Yii::$app->view->registerJs($js,View::POS_BEGIN);
$jsEnd = <<<ET
setTimeout(function(){ $("body").append('<div id="extra-section" style="font-size:16px"></div>'); }, 100);
ET;
\Yii::$app->view->registerJs($jsEnd,View::POS_READY);
    }
    private function setUpKeyCodeDetection(\taktwerk\yiiboilerplate\modules\digisign\models\base\DigiScreen $digiScreen){
        $url = Url::toRoute(['/digisign/digi-sign/layout-change','id'=>$digiScreen->id]);
        $js = <<<EOT
        $(function(){
            var typedWord = '';
            var typeTimeout;
            var reqTimeout;
            window.addEventListener('keypress', function(e){
                var c = String.fromCharCode(e.keyCode);
                typedWord += c.toLowerCase();
                clearTimeout(typeTimeout);
                typeTimeout = setTimeout(function(){
                            typedWord = '';
                }, 5000);
                clearTimeout(reqTimeout);
                reqTimeout = setTimeout(function(){
                            if(typedWord.length >= 4){
                                changeLayout(typedWord);
                            }
                    },1200);
            });
        });
        
        function changeLayout(codeLookup) {
            $.ajax({
                url:'{$url}',
                data:{code:codeLookup},
                success:function(res){
                    if(res.success==true){
                        window.location.reload();
                    }
                }
            });
        };

EOT;
        \Yii::$app->view->registerJs($js,View::POS_END);
    }
    /**
     *
     * @param integer $id
     * @desc Action to view the digital-signage screen
     */
    public function actionView($id)
    {
        $model = DigiScreen::findOne($id);
        if ($model == null) {
            throw new HttpException(404, Yii::t('twbp', 'The requested page does not exist.'));
        }
        $this->setUpOfflineHandling();
        $this->setUpKeyCodeDetection($model);
        $updateUrl = \Yii::$app->urlManager->createAbsoluteUrl([
            '/digisign/digi-screen/update',
            'id' => $model->id
        ]);
        if (! \Yii::$app->user->can('Authority') && ! $model->enabled){
            return $this->render('authorize', [
                'url' => $updateUrl,
                'screenId' => $id
            ]);
        }
        $template = $model->getTemplate();
        if ($template==null) {
            // Render add screen view
            return $this->render('missing-screen', [
                'url' => $updateUrl,
                'screenId' => $id
            ]);
        }
        $screenIds = \Yii::$app->session->get('screen_ids');
        if (\Yii::$app->user->can('Authority') || (is_array($screenIds) && in_array($id, $screenIds)) || $model->passcode == '') {
            if($template->autoRefreshEnabled()){
                \Yii::$app->view->registerMetaTag([
                    'content' => '30',
                    'http-equiv'=>"refresh"
                ]);
            }
            if($model->enable_barcode_scan){
                $this->setUpBarCodeScanning($model);
            }
            return $template->getDigiSignHtml();
            
            //return $html;
            //return $this->renderAjax('signage',['html'=>$html]);
        }
        $dModel = new \yii\base\DynamicModel([
            'password'
        ]);
        $passLength = strlen($model->passcode);
        $dModel->addRule('password', 'required')->addRule('password', function ($attribute, $params, $validator) use ($model) {
            if ($this->$attribute != $model->passcode) {
                $this->addError($attribute, \Yii::t('twbp', 'Incorrect passcode'));
            }
        });
        if ($dModel->load(\Yii::$app->request->post())) {
            if ($dModel->validate()) {
                $screenIds = \Yii::$app->session->get('screen_ids');
                if (! is_array($screenIds)) {
                    $screenIds = [];
                }
                $screenIds[] = $model->id;
                \Yii::$app->session->set('screen_ids', array_unique($screenIds));
                return $this->refresh();
            }
        }
        $dModel->password = '';
        return $this->render('login', [
            'model' => $dModel,
            'passLength' => $passLength
        ]);
    }
}
