<?php
//Generation Date: 07-Sep-2020 02:11:50pm
namespace taktwerk\yiiboilerplate\modules\digisign\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * DigiScreenController implements the CRUD actions for DigiScreen model.
 */
class DigiScreenController extends TwCrudController
{
}
