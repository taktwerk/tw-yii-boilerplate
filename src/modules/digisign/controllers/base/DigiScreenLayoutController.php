<?php
//Generation Date: 03-Jun-2021 11:52:33am
namespace taktwerk\yiiboilerplate\modules\digisign\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * DigiScreenLayoutController implements the CRUD actions for DigiScreenLayout model.
 */
class DigiScreenLayoutController extends TwCrudController
{
}
