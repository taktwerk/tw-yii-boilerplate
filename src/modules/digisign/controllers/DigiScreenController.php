<?php
//Generation Date: 07-Sep-2020 02:11:50pm
namespace taktwerk\yiiboilerplate\modules\digisign\controllers;

use taktwerk\yiiboilerplate\grid\GridView;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use taktwerk\yiiboilerplate\modules\digisign\models\DigiScreenLayout;
/**
 * This is the class for controller "DigiScreenController".
 */
class DigiScreenController extends \taktwerk\yiiboilerplate\modules\digisign\controllers\base\DigiScreenController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\digisign\models\DigiScreen';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\digisign\models\search\DigiScreen';

    // /**
    // * Additional actions for controllers, uncomment to use them
    // * @inheritdoc
    // */
    // public function behaviors()
    // {
    // return ArrayHelper::merge(parent::behaviors(), [
    // 'access' => [
    // 'class' => AccessControl::class,
    // 'rules' => [
    // [
    // 'allow' => true,
    // 'actions' => [
    // 'list-of-additional-actions',
    // ],
    // 'roles' => ['@']
    // ]
    // ]
    // ]
    // ]);
    // }
    public function init()
    {
        if ($this->module) {
            $list = $this->module->getDigiSignsList();
            foreach ($list as $key => $val) {
                $list[$key] = $val->getDigiSignTitle();
            }
            $this->crudColumnsOverwrite = [
                'crud' => [
                    'layout' => false,
                    'enabled' => false,
                    'digi_screen_layout_id' => false,
                    'after#passcode' => [
                        'attribute' => 'enabled',
                        'format' => 'boolean'
                    ],
                    'after#description' => [
                        'attribute' => 'layout',
                        'format' => 'html',
                        'value' => function ($model) {
                            if ($model->digiScreenLayout && $this->module->getDigiSignsList()[$model->digiScreenLayout->layout]) {
                                return Html::a($this->module->getDigiSignsList()[$model->digiScreenLayout->layout]->getDigiSignTitle(), [
                                    'digi-screen-layout/view',
                                    'id' => 1
                                ]);
                            }
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'data' => $list,
                            'options' => [
                                'placeholder' => '',
                                'multiple' => false
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ]
                        ]
                    ],
                /* 'after#description' => [
                    'attribute' => 'layout',
                    'format' => 'html',
                    'content' => function ($model) {
                        if($this->module->getDigiSignsList()[$model->layout]){
                            return $this->module->getDigiSignsList()[$model->layout]->getDigiSignTitle();
                        }
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'data' => $list,
                        'options' => [
                            'placeholder' => '',
                            'multiple' => false,
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ],
                    ], */
                'after#name' => [
                        'label' => Yii::t('twbp', 'Preview Link'),
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a('Screen Link', [
                                '/digisign/digi-sign/view',
                                'id' => $model->id
                            ], [
                                'target' => '_blank',
                                'data'=>['pjax'=>0]]);
                    }
                    ]
                    ],
                     ];
        }
        return parent::init();
    }
    public function formFieldsOverwrite($model, $form,$owner=null)
    {
        $list = $this->module->getDigiSignsList();
        foreach($list as $key => $val){
            $list[$key] = $val->getDigiSignTitle();
        }
        //$list = $dropDownList;
        $this->formFieldsOverwrite = [
            'layout'=>false,
            'last_auth'=>false,
            'digi_screen_layout_id'=>false
            /* 'after#description' =>
            $form->field(
                $model,
                'layout',
                )
            ->widget(
                Select2::class,
                [
                    'data' => $list,
                    'options' => [
                        'placeholder' => Yii::t('twbp', 'Select a layout...'),
                        'id' => 'tags' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                        'multiple' => false
                    ],
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'placeholder' => Yii::t('twbp', 'Select a layout...'),
                        'allowClear' => true,
                        'tags' => false,
                        'maximumInputLength' => 45
                    ]
                ]
                )
            ->hint($model->getAttributeHint('layout')), */
        ];
        $list = [];
        foreach($model->digiScreenLayouts as $layout){
           $list[$layout->id] = DigiScreenLayout::getLayoutTitle($layout->layout).' - '.$layout->id;
        }
        $this->formFieldsOverwrite['digi_screen_layout_id'] = $form
        ->field($model,'digi_screen_layout_id')
            ->widget(
                Select2::class,
                [
                    'data' =>$list,
                    'options' => [
                        'placeholder' => Yii::t('twbp', 'Select a layout...'),
                        'id' => 'tags' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                        'multiple' => false
                    ],
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'placeholder' => Yii::t('twbp', 'Select a layout...'),
                        'allowClear' => true,
                        'tags' => false,
                        'maximumInputLength' => 45
                    ]
                ]
                )
                ->hint($model->getAttributeHint('layout'));
        return $this->formFieldsOverwrite;
    }
}
