<?php

namespace taktwerk\yiiboilerplate\modules\protocol\traits;

trait ActiveRecordDbConnectionTrait
{
    public static function getDb()
    {
        return \Yii::$app->db;
    }
}
