<?php

namespace taktwerk\yiiboilerplate\modules\protocol;

use taktwerk\yiiboilerplate\modules\protocol\models\ProtocolDefault;

class Module extends \taktwerk\yiiboilerplate\components\TwModule
{
    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\protocol\controllers';

    /**
     * @var array Form mappings
     */
    public $formMapping = [];
    /**
     * @var array default Form mappings
     */
    public $defaultFormMapping = [
        'protocol_default' => ProtocolDefault::class,
    ];

    public function init()
    {
        parent::init();
        $this->formMapping = array_merge($this->defaultFormMapping,$this->formMapping);
        // custom initialization code goes here

        // When in console mode, switch controller namespace to commands
        if (\Yii::$app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'taktwerk\yiiboilerplate\modules\protocol\commands';
        }
    }

    /**
     * @param $formTableName
     * @return mixed
     * @throws \Exception
     */
    public function getFormClass($formTableName)
    {
        if (isset($this->formMapping[$formTableName])) {
            return new $this->formMapping[$formTableName];
        }
        throw new \Exception('Invalid protocol_form_table \'' . $formTableName . '\' missing from mapping.');
    }
}
