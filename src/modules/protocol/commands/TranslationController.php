<?php

namespace taktwerk\yiiboilerplate\modules\protocol\commands;

use taktwerk\yiiboilerplate\modules\workflow\components\TranslationComponent;
use yii\console\Controller;

/**
 * Class TranslationController
 * @package taktwerk\yiiboilerplate\modules\protocol\commands
 */
class TranslationController extends Controller
{
    /**
     * The command being executed
     * @var
     */
    protected $command;

    /**
     * Create the translation file
     */
    public function actionIndex()
    {
        $translator = new TranslationComponent();
        $count = $translator->generate();
        echo "Generated " . $count . " translations.";
    }
}
