<?php
//Generation Date: 11-Sep-2020 02:56:23pm
namespace taktwerk\yiiboilerplate\modules\protocol\models\base;

use Yii;
use \taktwerk\yiiboilerplate\models\TwActiveRecord;
use yii\db\Query;


/**
 * This is the base-model class for table "protocol_comment".
 * - - - - - - - - -
 * Generated by the modified Giiant CRUD Generator by taktwerk.com
 *
 * @property integer $id
 * @property integer $protocol_id
 * @property string $comment
 * @property string $event
 * @property string $name
 * @property integer $old_workflow_step_id
 * @property integer $new_workflow_step_id
 * @property integer $deleted_by
 * @property string $deleted_at
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property string $toString
 * @property string $entryDetails
 *
 * @property \taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep $newWorkflowStep
 * @property \taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep $oldWorkflowStep
 * @property \taktwerk\yiiboilerplate\modules\protocol\models\Protocol $protocol
 */
class ProtocolComment extends TwActiveRecord implements \taktwerk\yiiboilerplate\modules\backend\models\RelationModelInterface{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [
                ['protocol_id', 'event', 'name'],
                'required'
            ],
            [
                ['protocol_id', 'old_workflow_step_id', 'new_workflow_step_id', 'deleted_by'],
                'integer'
            ],
            [
                ['comment'],
                'string'
            ],
            [
                ['deleted_at'],
                'safe'
            ],
            [
                ['event', 'name'],
                'string',
                'max' => 45
            ],
            [
                ['new_workflow_step_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => \taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::class,
                'targetAttribute' => ['new_workflow_step_id' => 'id']
            ],
            [
                ['old_workflow_step_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => \taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::class,
                'targetAttribute' => ['old_workflow_step_id' => 'id']
            ],
            [
                ['protocol_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => \taktwerk\yiiboilerplate\modules\protocol\models\Protocol::class,
                'targetAttribute' => ['protocol_id' => 'id']
            ]
        ];
        
        return  array_merge($rules, $this->buildRequiredRulesFromFormDependentShowFields());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('twbp', 'ID'),
            'protocol_id' => Yii::t('twbp', 'Protocol'),
            'comment' => Yii::t('twbp', 'Comment'),
            'event' => Yii::t('twbp', 'Event'),
            'name' => Yii::t('twbp', 'Name'),
            'old_workflow_step_id' => Yii::t('twbp', 'Old Workflow Step'),
            'new_workflow_step_id' => Yii::t('twbp', 'New Workflow Step'),
            'created_by' => \Yii::t('twbp', 'Created By'),
            'created_at' => \Yii::t('twbp', 'Created At'),
            'updated_by' => \Yii::t('twbp', 'Updated By'),
            'updated_at' => \Yii::t('twbp', 'Updated At'),
            'deleted_by' => \Yii::t('twbp', 'Deleted By'),
            'deleted_at' => \Yii::t('twbp', 'Deleted At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributePlaceholders()
    {
        return [
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
        ];
    }

    /**
     * Auto generated method, that returns a human-readable name as string
     * for this model. This string can be called in foreign dropdown-fields or
     * foreign index-views as a representative value for the current instance.
     *
     * @author: taktwerk
     * This method is auto generated with a modified Model-Generator by taktwerk.com
     * @return String
     */
    public function toString()
    {
        return $this->name; // this attribute can be modified
    }


    /**
     * Getter for toString() function
     * @return String
     */
    public function getToString()
    {
        return $this->toString();
    }

    /**
     * Description for the table/model to show on grid an form view
     * @return String
     */
    public static function tableHint()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return [
            'newWorkflowStep',
            'oldWorkflowStep',
            'protocol',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewWorkflowStep()
    {
        return $this->hasOne(
            \taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::class,
            ['id' => 'new_workflow_step_id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function newWorkflowStepList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getOldWorkflowStep()
    {
        return $this->hasOne(
            \taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::class,
            ['id' => 'old_workflow_step_id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function oldWorkflowStepList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getProtocol()
    {
        return $this->hasOne(
            \taktwerk\yiiboilerplate\modules\protocol\models\Protocol::class,
            ['id' => 'protocol_id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function protocolList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\protocol\models\Protocol::filter($q);
    }
    

    /**
     * Return details for dropdown field
     * @return string
     */
    public function getEntryDetails()
    {
        return $this->toString;
    }

    /**
     * @param $string
     */
    public static function fetchCustomImportLookup($string)
    {
            return self::find()->andWhere([self::tableName() . '.name' => $string]);
    }

    /**
     * User for filtering results for Select2 element
     * @param null $q
     * @return array
     */
    public static function filter($q = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, name AS text')
            ->from(self::tableName())
            ->andWhere([self::tableName() . '.deleted_at' => null])
            ->andWhere(['like', 'name', $q])
            ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }

        /**
     * Get related CRUD controller's class
     * @return \taktwerk\yiiboilerplate\modules\protocol\controllers\ProtocolCommentController|\yii\web\Controller
     */
    public function getControllerInstance()
    {
        if(class_exists('\taktwerk\yiiboilerplate\modules\protocol\controllers\ProtocolCommentController')){
            return new \taktwerk\yiiboilerplate\modules\protocol\controllers\ProtocolCommentController("protocol-comment", \taktwerk\yiiboilerplate\modules\protocol\Module::getInstance());
        }
    }
}