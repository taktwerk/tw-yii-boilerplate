<?php
//Generation Date: 06-Nov-2020 06:35:33am
namespace taktwerk\yiiboilerplate\modules\protocol\models\base;

use Yii;
use \taktwerk\yiiboilerplate\models\TwActiveRecord;
use yii\db\Query;
use taktwerk\yiiboilerplate\traits\UploadTrait;

/**
 * This is the base-model class for table "protocol_default".
 * - - - - - - - - -
 * Generated by the modified Giiant CRUD Generator by taktwerk.com
 *
 * @property integer $id
 * @property integer $protocol_id
 * @property string $protocol_file
 * @property string $protocol_file_filemeta
 * @property integer $deleted_by
 * @property string $deleted_at
 * @property string $local_created_at
 * @property string $local_updated_at
 * @property string $local_deleted_at
 * @property string $pdf_image
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property string $toString
 * @property string $entryDetails
 *
 * @property \taktwerk\yiiboilerplate\modules\protocol\models\Protocol $protocol
 */
class ProtocolDefault extends TwActiveRecord implements \taktwerk\yiiboilerplate\modules\backend\models\RelationModelInterface{

    /**
     * We can upload images and files to this model, so we need our helper trait.
     */
    use UploadTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol_default';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [
                ['protocol_id'],
                'required'
            ],
            [
                ['protocol_id', 'deleted_by'],
                'integer'
            ],
            [
                ['protocol_file_filemeta'],
                'string'
            ],
            [
                ['deleted_at', 'local_created_at', 'local_updated_at', 'local_deleted_at'],
                'safe'
            ],
            [
                ['protocol_file', 'pdf_image'],
                'string',
                'max' => 255
            ],
            [
                ['protocol_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => \taktwerk\yiiboilerplate\modules\protocol\models\Protocol::class,
                'targetAttribute' => ['protocol_id' => 'id']
            ]
        ];
        
        return  array_merge($rules, $this->buildRequiredRulesFromFormDependentShowFields());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('twbp', 'ID'),
            'protocol_id' => Yii::t('twbp', 'Protocol'),
            'protocol_file' => Yii::t('twbp', 'Protocol File'),
            'protocol_file_filemeta' => Yii::t('twbp', 'Protocol File Filemeta'),
            'created_by' => \Yii::t('twbp', 'Created By'),
            'created_at' => \Yii::t('twbp', 'Created At'),
            'updated_by' => \Yii::t('twbp', 'Updated By'),
            'updated_at' => \Yii::t('twbp', 'Updated At'),
            'deleted_by' => \Yii::t('twbp', 'Deleted By'),
            'deleted_at' => \Yii::t('twbp', 'Deleted At'),
            'local_created_at' => \Yii::t('twbp', 'Local Created At'),
            'local_updated_at' => \Yii::t('twbp', 'Local Updated At'),
            'local_deleted_at' => \Yii::t('twbp', 'Local Deleted At'),
            'pdf_image' => Yii::t('twbp', 'Pdf Image'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributePlaceholders()
    {
        return [
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
        ];
    }

    /**
     * Auto generated method, that returns a human-readable name as string
     * for this model. This string can be called in foreign dropdown-fields or
     * foreign index-views as a representative value for the current instance.
     *
     * @author: taktwerk
     * This method is auto generated with a modified Model-Generator by taktwerk.com
     * @return String
     */
    public function toString()
    {
        return $this->protocol_file; // this attribute can be modified
    }


    /**
     * Getter for toString() function
     * @return String
     */
    public function getToString()
    {
        return $this->toString();
    }

    /**
     * Description for the table/model to show on grid an form view
     * @return String
     */
    public static function tableHint()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return [
            'protocol',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProtocol()
    {
        return $this->hasOne(
            \taktwerk\yiiboilerplate\modules\protocol\models\Protocol::class,
            ['id' => 'protocol_id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function protocolList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\protocol\models\Protocol::filter($q);
    }
    

    /**
     * Return details for dropdown field
     * @return string
     */
    public function getEntryDetails()
    {
        return $this->toString;
    }

    /**
     * @param $string
     */
    public static function fetchCustomImportLookup($string)
    {
            return self::find()->andWhere([self::tableName() . '.protocol_file' => $string]);
    }

    /**
     * Load data into object
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;

        // Don't update these fields as they are handled separately.
        unset($data[$scope]['protocol_file']);
        unset($data[$scope]['pdf_image']);

        return parent::load($data,$formName);
    }

    /**
     * User for filtering results for Select2 element
     * @param null $q
     * @return array
     */
    public static function filter($q = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, protocol_file AS text')
            ->from(self::tableName())
            ->andWhere([self::tableName() . '.deleted_at' => null])
            ->andWhere(['like', 'protocol_file', $q])
            ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }

        /**
     * Get related CRUD controller's class
     * @return \taktwerk\yiiboilerplate\modules\protocol\controllers\ProtocolDefaultController|\yii\web\Controller
     */
    public function getControllerInstance()
    {
        if(class_exists('\taktwerk\yiiboilerplate\modules\protocol\controllers\ProtocolDefaultController')){
            return new \taktwerk\yiiboilerplate\modules\protocol\controllers\ProtocolDefaultController("protocol-default", \taktwerk\yiiboilerplate\modules\protocol\Module::getInstance());
        }
    }
}