<?php
//Generation Date: 17-Dec-2020 01:01:25pm
namespace taktwerk\yiiboilerplate\modules\protocol\models\base;

use Yii;
use \taktwerk\yiiboilerplate\models\TwActiveRecord;
use yii\db\Query;


/**
 * This is the base-model class for table "protocol_field_comment".
 * - - - - - - - - -
 * Generated by the modified Giiant CRUD Generator by taktwerk.com
 *
 * @property integer $id
 * @property string $name
 * @property string $comment
 * @property integer $validation
 * @property integer $protocol_id
 * @property integer $workflow_step_id
 * @property integer $deleted_by
 * @property string $deleted_at
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property string $toString
 * @property string $entryDetails
 *
 * @property \taktwerk\yiiboilerplate\modules\protocol\models\Protocol $protocol
 * @property \taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep $workflowStep
 */
class ProtocolFieldComment extends TwActiveRecord implements \taktwerk\yiiboilerplate\modules\backend\models\RelationModelInterface{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol_field_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [
                ['comment'],
                'string'
            ],
            [
                ['validation', 'protocol_id', 'workflow_step_id', 'deleted_by'],
                'integer'
            ],
            [
                ['deleted_at'],
                'safe'
            ],
            [
                ['name'],
                'string',
                'max' => 255
            ],
            [
                ['protocol_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => \taktwerk\yiiboilerplate\modules\protocol\models\Protocol::class,
                'targetAttribute' => ['protocol_id' => 'id']
            ],
            [
                ['workflow_step_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => \taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::class,
                'targetAttribute' => ['workflow_step_id' => 'id']
            ]
        ];
        
        return  array_merge($rules, $this->buildRequiredRulesFromFormDependentShowFields());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('twbp', 'ID'),
            'name' => Yii::t('twbp', 'Name'),
            'comment' => Yii::t('twbp', 'Comment'),
            'validation' => Yii::t('twbp', 'Validation'),
            'protocol_id' => Yii::t('twbp', 'Protocol'),
            'workflow_step_id' => Yii::t('twbp', 'Workflow Step'),
            'created_by' => \Yii::t('twbp', 'Created By'),
            'created_at' => \Yii::t('twbp', 'Created At'),
            'updated_by' => \Yii::t('twbp', 'Updated By'),
            'updated_at' => \Yii::t('twbp', 'Updated At'),
            'deleted_by' => \Yii::t('twbp', 'Deleted By'),
            'deleted_at' => \Yii::t('twbp', 'Deleted At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributePlaceholders()
    {
        return [
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
        ];
    }

    /**
     * Auto generated method, that returns a human-readable name as string
     * for this model. This string can be called in foreign dropdown-fields or
     * foreign index-views as a representative value for the current instance.
     *
     * @author: taktwerk
     * This method is auto generated with a modified Model-Generator by taktwerk.com
     * @return String
     */
    public function toString()
    {
        return $this->name; // this attribute can be modified
    }


    /**
     * Getter for toString() function
     * @return String
     */
    public function getToString()
    {
        return $this->toString();
    }

    /**
     * Description for the table/model to show on grid an form view
     * @return String
     */
    public static function tableHint()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return [
            'protocol',
            'workflowStep',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProtocol()
    {
        return $this->hasOne(
            \taktwerk\yiiboilerplate\modules\protocol\models\Protocol::class,
            ['id' => 'protocol_id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function protocolList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\protocol\models\Protocol::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflowStep()
    {
        return $this->hasOne(
            \taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::class,
            ['id' => 'workflow_step_id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function workflowStepList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::filter($q);
    }
    

    /**
     * Return details for dropdown field
     * @return string
     */
    public function getEntryDetails()
    {
        return $this->toString;
    }

    /**
     * @param $string
     */
    public static function fetchCustomImportLookup($string)
    {
            return self::find()->andWhere([self::tableName() . '.name' => $string]);
    }

    /**
     * User for filtering results for Select2 element
     * @param null $q
     * @return array
     */
    public static function filter($q = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, name AS text')
            ->from(self::tableName())
            ->andWhere([self::tableName() . '.deleted_at' => null])
            ->andWhere(['like', 'name', $q])
            ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }

        /**
     * Get related CRUD controller's class
     * @return \taktwerk\yiiboilerplate\modules\protocol\controllers\ProtocolFieldCommentController|\yii\web\Controller
     */
    public function getControllerInstance()
    {
        if(class_exists('\taktwerk\yiiboilerplate\modules\protocol\controllers\ProtocolFieldCommentController')){
            return new \taktwerk\yiiboilerplate\modules\protocol\controllers\ProtocolFieldCommentController("protocol-field-comment", \taktwerk\yiiboilerplate\modules\protocol\Module::getInstance());
        }
    }
}