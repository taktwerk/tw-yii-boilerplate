<?php

namespace taktwerk\yiiboilerplate\modules\protocol\models;

use taktwerk\yiiboilerplate\modules\sync\traits\UserAppDataVersionTrait;
use taktwerk\yiiboilerplate\traits\UploadTrait;
use \taktwerk\yiiboilerplate\modules\protocol\models\base\ProtocolDefault as BaseProtocolDefault;
use yii\helpers\StringHelper;
use Yii;

/**
 * This is the model class for table "protocol_default".
 */
class ProtocolDefault extends BaseProtocolDefault
{
    use UploadTrait, UserAppDataVersionTrait;

    public function formLocation()
    {
        return '@taktwerk/yiiboilerplate/modules/protocol/views/protocol-default/form.php';
    }

    public function tabLocation()
    {
        return '@taktwerk/yiiboilerplate/modules/protocol/views/protocol-default/tab.php';
    }

    public function load($data, $formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;

        // Don't update these fields as they are handled separately.
        unset($data[$scope]['protocol_file']);

        return parent::load($data,$formName);
    }

    public function beforeSave($insert)
    {
        if ($insert && !$this->isMobileSync) {
            if ($this->protocol && $this->protocol->protocolTemplate) {
                $protocolTemplate = $this->protocol->protocolTemplate;
                if ($protocolTemplate->protocol_file) {
                    $this->protocol_file = $protocolTemplate->protocol_file;
                }
            }
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->refresh();
        if ($insert && !$this->isMobileSync) {
            if ($this->protocol_file) {
                $protocolDefaults = ProtocolDefault::find()->where(['protocol_id' => $this->protocol_id])->all();
                $isFirstProtcolDefault = !$protocolDefaults || ($protocolDefaults[0]->id === $this->id);
                $protocolModel = null;
                if ($isFirstProtcolDefault && $this->protocol && $this->protocol->protocolTemplate) {
                    $protocolModel = $this->protocol->protocolTemplate;
                } else {
                    $protocolModel = $protocolDefaults[count($protocolDefaults) - 2];
                }
                if ($protocolModel) {
                    $this->copyProtocolFile($protocolModel);
                }
            }
        }

        // if (array_key_exists('protocol_file', $changedAttributes) &&
        //     $this->isPdf() &&
        //     class_exists('Imagick')
        // ) {
        //     $this->refresh();

        //     $pdfImg = FileHelper::PdfToImageByModel($this,'protocol_file');
        //     if($pdfImg && $pdfImg['success']){
        //         $this->pdf_image = $pdfImg['name'];
        //         $this->save();
        //     }
        // }

        parent::afterSave($insert, $changedAttributes);
        $this->changeUserAppDataVersion($insert, $changedAttributes);
    }

    protected function copyProtocolFile($protocolModel)
    {
        if (!$protocolModel->protocol_file) {
            return;
        }
        $originalProtocolFilePath = $protocolModel->getUploadPath() . $protocolModel->protocol_file;
        if (empty(pathinfo($originalProtocolFilePath))) {
            return;
        }
        $filesForCopy = [
            [
                'fileName' => $protocolModel->protocol_file,
                'uploadPath' => $originalProtocolFilePath,
                'attribute' => 'protocol_file',
            ],
            [
                'fileName' => $protocolModel->pdf_image,
                'uploadPath' => $protocolModel->getUploadPath() . $protocolModel->pdf_image,
                'attribute' => 'pdf_image',
            ],
        ];

        foreach ($filesForCopy as $fileForCopy) {
            $originalFileName = pathinfo($fileForCopy['uploadPath'])['filename'];
            $dirPath = $protocolModel->getUploadPath();
            $paths = Yii::$app->fs->listContents($dirPath);

            if (empty($paths)) {
                continue;
            }

            foreach ($paths as $file) {
                if (StringHelper::startsWith($file['basename'], $originalFileName)) {
                    $newFile = $this->getUploadPath() . $fileForCopy['fileName'];
                    Yii::$app->fs->copy($file['path'], $newFile);
                    $this->updateAttributes(
                        [
                            $fileForCopy['attribute'] => $fileForCopy['fileName']
                        ]
                    );
                    break;
                }
            }
        }
    }

    public function clientsForChangeAppDataVersion()
    {
        $clientIds = [$this->protocol->client_id];

        return $clientIds;
    }

    public function getAdditionalUserIdsForChangeAppDataVersion($insert, $changedAttributes)
    {
        $protocolTemplate = $this->protocol->protocolTemplate;
        if (!$protocolTemplate) {
            return [];
        }
        $workflow = $protocolTemplate->workflow;
        if (!$workflow) {
            return [];
        }
        $workflowSteps = $workflow->workflowSteps;
        if (!$workflowSteps) {
            return [];
        }
        $specificUsersFromColumn = [];
        $userIdsByRoles = [];
        foreach ($workflowSteps as $workflowStep) {
            if ($workflowStep->user_id) {
                $specificUsersFromColumn[] = $workflowStep->user_id;
            }
            if ($workflowStep->role) {
                $userIdsByRole = Yii::$app->authManager->getUserIdsByRole($workflowStep->role);
                $userIdsByRoles = array_merge($userIdsByRoles, $userIdsByRole);
            }
        }

        return array_merge($specificUsersFromColumn, $userIdsByRoles);
    }
}
