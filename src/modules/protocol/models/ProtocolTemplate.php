<?php

namespace taktwerk\yiiboilerplate\modules\protocol\models;

use Illuminate\Support\Str;
use taktwerk\yiiboilerplate\modules\sync\traits\UserAppDataVersionTrait;
use taktwerk\yiiboilerplate\traits\UploadTrait;
use Yii;
use \taktwerk\yiiboilerplate\modules\protocol\models\base\ProtocolTemplate as BaseProtocolTemplate;
use taktwerk\yiiboilerplate\helpers\FileHelper;

/**
 * This is the model class for table "protocol_template".
 */
class ProtocolTemplate extends BaseProtocolTemplate
{
    use UserAppDataVersionTrait, UploadTrait;

    public function rules()
    {
        $parentRules = parent::rules();

        $newRules = [
            [['protocol_file', 'thumbnail'], 'string', 'max' => 255]
        ];

        $rules = array_merge($parentRules, $newRules);

        return $rules;
    }

    /**
     * List of mappings to the forms
     * @var array
     */
    protected $formMapping = [
        'protocol_default' => ProtocolDefault::class,
    ];

    public function load($data, $formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;

        // Don't update these fields as they are handled separately.
        unset($data[$scope]['protocol_file']);

        return parent::load($data,$formName);
    }

    public function imagePath()
    {
        return $this->getUploadPath() . $this->protocol_file;
    }

    /**
     * Check if the file a pdf
     * @return bool
     */
    public function isPdf()
    {
        return !empty($this->protocol_file) && substr($this->protocol_file, -4) == '.pdf';
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if (array_key_exists('protocol_file', $changedAttributes) &&
            $this->isPdf() &&
            class_exists('Imagick')
        ) {
            $this->refresh();
            if (!isset($changedAttributes['thumbnail'])) {
                $pdf = $this->getUploadPath() . $this->protocol_file;
                $pngFileName = Str::replaceLast('.pdf', '.png', strtolower($this->protocol_file));
                $png = $this->getUploadPath() . $pngFileName;

                $tmpFile = Yii::getAlias('@runtime/convert/' . $this->protocol_file);
                $convertedFile = Yii::getAlias('@runtime/convert/' . $pngFileName);
                $contents = Yii::$app->fs->read($pdf);

                @mkdir(Yii::getAlias('@runtime/convert/'));
                file_put_contents($tmpFile, $contents);

                $imagick = new \Imagick();
                $imagick->setResolution(300, 300);
                $imagick->readImage($tmpFile . "[0]");
                $imagick->setImageFormat("png");
                $imagick->setCompressionQuality(90);
                $imagick->writeImage($convertedFile);

                // Save thumb
                $stream = fopen($convertedFile, 'r+');
                if (Yii::$app->fs->has($png)) {
                    Yii::$app->fs->delete($png);
                }
                Yii::$app->fs->writeStream($png, $stream);

                $this->thumbnail = $pngFileName;
                $this->save();

                // delete tmp files
                unlink($tmpFile);
                unlink($convertedFile);
            }

            $pdfImg = FileHelper::PdfToImageByModel($this,'protocol_file');
            if($pdfImg && $pdfImg['success']){
                $this->pdf_image = $pdfImg['name'];
                $this->save();
            }
        }

        $this->changeUserAppDataVersion($insert, $changedAttributes);
    }
}
