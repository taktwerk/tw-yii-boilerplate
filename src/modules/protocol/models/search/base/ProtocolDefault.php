<?php
//Generation Date: 28-Sep-2020 08:02:15am
namespace taktwerk\yiiboilerplate\modules\protocol\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\protocol\models\ProtocolDefault as ProtocolDefaultModel;

/**
 * ProtocolDefault represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\protocol\models\ProtocolDefault`.
 */
class ProtocolDefault extends ProtocolDefaultModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'protocol_id',
                    'protocol_file',
                    'protocol_file_filemeta',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'local_created_at',
                    'local_updated_at',
                    'local_deleted_at',
                    'pdf_image',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProtocolDefaultModel::find();

        $this->parseSearchParams(ProtocolDefaultModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(ProtocolDefaultModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(ProtocolDefaultModel::class),
                'params' => [
                    'page' => $this->parsePageParams(ProtocolDefaultModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('protocol_id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('protocol_file', $query);
        $this->applyLikeOperator('protocol_file_filemeta', $query);
        $this->applyLikeOperator('pdf_image', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        $this->applyDateOperator('local_created_at', $query, true);
        $this->applyDateOperator('local_updated_at', $query, true);
        $this->applyDateOperator('local_deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(ProtocolDefaultModel::class);
    }
}
