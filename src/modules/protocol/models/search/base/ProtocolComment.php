<?php
//Generation Date: 11-Sep-2020 02:56:41pm
namespace taktwerk\yiiboilerplate\modules\protocol\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\protocol\models\ProtocolComment as ProtocolCommentModel;

/**
 * ProtocolComment represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\protocol\models\ProtocolComment`.
 */
class ProtocolComment extends ProtocolCommentModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'protocol_id',
                    'comment',
                    'event',
                    'name',
                    'old_workflow_step_id',
                    'new_workflow_step_id',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProtocolCommentModel::find();

        $this->parseSearchParams(ProtocolCommentModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(ProtocolCommentModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(ProtocolCommentModel::class),
                'params' => [
                    'page' => $this->parsePageParams(ProtocolCommentModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('protocol_id', $query);
        $this->applyHashOperator('old_workflow_step_id', $query);
        $this->applyHashOperator('new_workflow_step_id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('comment', $query);
        $this->applyLikeOperator('event', $query);
        $this->applyLikeOperator('name', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(ProtocolCommentModel::class);
    }
}
