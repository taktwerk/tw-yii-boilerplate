<?php
//Generation Date: 17-Dec-2020 01:01:35pm
namespace taktwerk\yiiboilerplate\modules\protocol\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\protocol\models\ProtocolFieldComment as ProtocolFieldCommentModel;

/**
 * ProtocolFieldComment represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\protocol\models\ProtocolFieldComment`.
 */
class ProtocolFieldComment extends ProtocolFieldCommentModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'name',
                    'comment',
                    'validation',
                    'protocol_id',
                    'workflow_step_id',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$showDeleted = (property_exists(\Yii::$app->controller, 'model')
            &&
            (is_subclass_of(ProtocolFieldCommentModel::class, \Yii::$app->controller->model) 
            	||
            	is_subclass_of(\Yii::$app->controller->model, ProtocolFieldCommentModel::class))
            &&
              (Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model)
                ||
                Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model)))
            );
        $query = ProtocolFieldCommentModel::find((Yii::$app->get('userUi')->get('show_deleted', ProtocolFieldCommentModel::class) === '1'|| $showDeleted)?false:true);

        $this->parseSearchParams(ProtocolFieldCommentModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(ProtocolFieldCommentModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(ProtocolFieldCommentModel::class),
                'params' => [
                    'page' => $this->parsePageParams(ProtocolFieldCommentModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('validation', $query);
        $this->applyHashOperator('protocol_id', $query);
        $this->applyHashOperator('workflow_step_id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('name', $query);
        $this->applyLikeOperator('comment', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(ProtocolFieldCommentModel::class);
    }
}
