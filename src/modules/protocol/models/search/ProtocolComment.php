<?php

namespace taktwerk\yiiboilerplate\modules\protocol\models\search;

use taktwerk\yiiboilerplate\modules\protocol\models\search\base\ProtocolComment as ProtocolCommentSearchModel;

/**
* ProtocolComment represents the model behind the search form about `taktwerk\yiiboilerplate\modules\protocol\models\ProtocolComment`.
*/
class ProtocolComment extends ProtocolCommentSearchModel{

}