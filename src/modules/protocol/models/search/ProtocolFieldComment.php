<?php
//Generation Date: 17-Dec-2020 01:01:35pm
namespace taktwerk\yiiboilerplate\modules\protocol\models\search;

use taktwerk\yiiboilerplate\modules\protocol\models\search\base\ProtocolFieldComment as ProtocolFieldCommentSearchModel;

/**
* ProtocolFieldComment represents the model behind the search form about `taktwerk\yiiboilerplate\modules\protocol\models\ProtocolFieldComment`.
*/
class ProtocolFieldComment extends ProtocolFieldCommentSearchModel{

}