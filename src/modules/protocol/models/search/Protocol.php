<?php

namespace taktwerk\yiiboilerplate\modules\protocol\models\search;

use Yii;
use taktwerk\yiiboilerplate\modules\protocol\models\search\base\Protocol as ProtocolSearchModel;
use taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep;

/**
 * Protocol represents the model behind the search form about `taktwerk\yiiboilerplate\modules\protocol\models\Protocol`.
 */
class Protocol extends ProtocolSearchModel{

    public function search($params)
    {
        $dataProvider = parent::search($params);

        $query = $dataProvider->query;
        // Limit the results for people who can't create. We want to show any protocol that they were
        // a "part of", meaning that they had the possibility to fill out the protocol in one of
        // the steps or in a future step.
        if (!Yii::$app->getUser()->can('protocol_protocol-template_create')) {
            // Find steps the user is assigned to and get the workflow ids
            $workflowIds = [];
            $workflowSteps = WorkflowStep::find()
                ->where(['user_id' => Yii::$app->getUser()->id])
                ->orWhere(['in', 'role', Yii::$app->getUser()->getRoles(true)])
                ->all();
            foreach ($workflowSteps as $step) {
                if (!in_array($step->workflow_id, $workflowIds)) {
                    $workflowIds[] = $step->workflow_id;
                }
            }
            // From the steps, find protocol templates
            $protocolIds = [];
            foreach (\taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::find()->where(['in', 'workflow_id', $workflowIds])->all() as $pro) {
                $protocolIds[] = $pro->id;
            }

            $query->where(['in', 'protocol_template_id', $protocolIds]);
        }
        return $dataProvider;
    }
}