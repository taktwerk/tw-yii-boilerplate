<?php

namespace taktwerk\yiiboilerplate\modules\protocol\models\search;

use taktwerk\yiiboilerplate\modules\protocol\models\search\base\ProtocolTemplate as ProtocolTemplateSearchModel;

/**
* ProtocolTemplate represents the model behind the search form about `taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate`.
*/
class ProtocolTemplate extends ProtocolTemplateSearchModel{

}