<?php

namespace taktwerk\yiiboilerplate\modules\protocol\models\search;

use taktwerk\yiiboilerplate\modules\protocol\models\search\base\ProtocolDefault as ProtocolDefaultSearchModel;

/**
* ProtocolDefault represents the model behind the search form about `taktwerk\yiiboilerplate\modules\protocol\models\ProtocolDefault`.
*/
class ProtocolDefault extends ProtocolDefaultSearchModel{

}