<?php

namespace taktwerk\yiiboilerplate\modules\protocol\models;

use app\models\User;
use taktwerk\yiiboilerplate\modules\protocol\models\ProtocolComment;
use taktwerk\yiiboilerplate\modules\workflow\models\WorkflowTransition;
use taktwerk\yiiboilerplate\modules\sync\traits\UserAppDataVersionTrait;

use Yii;
use \taktwerk\yiiboilerplate\modules\protocol\models\base\Protocol as BaseProtocol;
use yii\db\ActiveRecord;
use yii\helpers\StringHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;
use taktwerk\yiiboilerplate\modules\protocol\models\base\ProtocolFieldComment;
use taktwerk\yiiboilerplate\modules\protocol\components\WorkFlowTransitionInterface;
use taktwerk\yiiboilerplate\modules\workflow\models\Workflow;
use taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep;
use yii\db\AfterSaveEvent;
use yii\helpers\VarDumper;
use taktwerk\yiiboilerplate\components\Helper;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use taktwerk\yiiboilerplate\helpers\CrudHelper;

/**
 * This is the model class for table "protocol".
 */
class Protocol extends BaseProtocol
{
    use UserAppDataVersionTrait;
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

    /**
     * @return mixed
     */
    public function protocolForm()
    {
        $model = $this->protocolFormClass();
        return $model->findOne($this->protocol_form_number);
    }
    public function logFieldChangesEnabled(){
        $models = \Yii::$app->params['protocol_field_change_log_models'];
        if($models && is_array($models)){
            $form = $this->protocolForm();
            foreach($models as $model){
                if(ltrim($model,'\\') == get_class($form) ||  is_subclass_of($model, $form) || is_subclass_of($form, $model)){
                    return true;
                }
            }
        }
        return false;
    }
    /**
     * @return mixed
     */
    public function protocolFormClass()
    {
        return Yii::$app->getModule('protocol')->getFormClass($this->protocol_form_table);
    }

    public function getReferenceModel()
    {
        if ($this->reference_model && $this->reference_id && is_subclass_of($this->reference_model, ActiveRecord::class)) {
            $m = $this->reference_model;
            return $m::findOne($this->reference_id);
        }
        return null;
    }

    public function getReferenceModelName($withUrl = true)
    {
        $refModel = $this->getReferenceModel();
        $name = null;
        $translateCategory='app';
        if ($this->reference_model) {
            $name = $this->reference_model;
            if(strpos($this->reference_model, 'taktwerk\yiiboilerplate') !== false)
            {
                $translateCategory = 'twbp';
            }
        }

        if ($refModel) {
            $translate = true;
            if ($refModel->hasMethod('toString')) {
                $name = $refModel->toString();
                $translate = false;
            }
            if ($withUrl) {
                if ($refModel->hasMethod('getModelUrlWithName')) {
                    $data = $refModel->getModelUrlWithName();

                    if ($data && isset($data['name']) && isset($data['url'])) {
                        return Html::a($data['name'], $data['url'], [
                            'target' => '_blank',
                            'data' => [
                                'pjax' => 0
                            ]
                        ]);
                    }
                    if ($data && isset($data['name'])) {
                        return $data['name'];
                    }
                }

                $model_name = explode('models\\', $this->reference_model)[1];
                if (class_exists(strtolower(explode('models\\', $this->reference_model)[0])) . $model_name) {
                    $controller_name = strtolower(Inflector::slug(Inflector::camel2words($model_name), '-'));
                    $module_name = explode("modules\\", substr($this->reference_model, 0, strrpos($this->reference_model, '\\models')))[1];
                    $module_name = strtolower(str_replace(" ", "", ucwords(str_replace("-", " ", $module_name))));
                    $url = \Yii::$app->urlManager->createAbsoluteUrl("$module_name/$controller_name/update?id=$this->reference_id");
                    
                    return Html::a($translate?$name:\Yii::t($translateCategory,$name), $url, [
                        'data-pjax' => 0
                    ]);
                } else {
                    $url = str_replace("/view?", "/update?", $this->feedback_url);
                    return Html::a($this->reference_model, $url, [
                        'data-pjax' => 0,
                        'target' => '_blank',
                    ]);
                }
            }
            
        }else
        if (strpos($this->reference_model, 'models') !== false && is_subclass_of($this->reference_model, ActiveRecord::class)) {
            $model_name = explode('models\\', $this->reference_model)[1];
            $controller_name = strtolower(Inflector::slug(Inflector::camel2words($model_name), '-'));
            $module_name = explode("modules\\", substr($this->reference_model, 0, strrpos($this->reference_model, '\\models')))[1];
            $module_name = strtolower(str_replace(" ", "",ucwords(str_replace("-"," ",$module_name))));
            $url = \Yii::$app->urlManager->createAbsoluteUrl("$module_name\\$controller_name\\index");
            $name = Inflector::camel2words($model_name);
            return $withUrl?Html::a(\Yii::t($translateCategory,$name), $url, ['data-pjax' => 0]):\Yii::t($translateCategory,$name);
        }
        return \Yii::t($translateCategory,$name);
    }

    /**
     * @param WorkflowTransition $transition
     */
    public function transition(WorkflowTransition $transition)
    {
        $formModel = $this->protocolForm();
        $workflow_step_id = $transition->workflow_step_id;
        $changeStep = true;
        if($isWFTInterface = $formModel instanceof WorkflowTransitionInterface){
            $formModel->setCurrentWFTransition($transition,$this);
            if($formModel->beforeWFTransition()===false){
                $changeStep = false;
            }
        }
        if($changeStep){
            $this->changeWFStep($workflow_step_id,
                $transition->next_workflow_step_id,
                $transition->workflowStep->getType());
        }
        if($isWFTInterface){
            $formModel->afterWFTransition();
        }
        
    }
    public function changeWFStepByOrderNumber($currentWFStepId, $newStepOrderNumber,$type){
        $step = WorkflowStep::findOne(['id'=>$currentWFStepId]);
        if($step){
            $newStep = $step->workflow->getWorkflowSteps()->andWhere(['order_number'=>$newStepOrderNumber])->one();
            $this->changeWFStep($currentWFStepId,$newStep->id,$type);
        }
    }
    public function changeWFStep($fromWorkflowStepId,$toWorkflowStepId,$type){
        // Add comment
        $comment = new ProtocolComment();
        $comment->protocol_id = $this->id;
        $comment->event = $type;
        $comment->old_workflow_step_id = $fromWorkflowStepId;
        $comment->new_workflow_step_id = $toWorkflowStepId;
        $comment->name = 'b';
        $comment->save();
        // Change current workflow step
        $this->workflow_step_id = $toWorkflowStepId;
        $this->save();
        $this->addFormNumberToProtocol();
    }
    public function switchToNextTransition($comment='',$workFlowStepId=null){
        if($workFlowStepId){
            $nextTransition = $this->workflowStep->getWorkflowTransitions0()->andWhere(['workflow_step_id'=>$workFlowStepId])->one();
        }else{
            $nextTransition = $this->workflowStep->getWorkflowTransitions0()->andWhere(['workflow_step_id'=>$this->workflow_step_id])->one();
        }
        if($nextTransition){
            $this->addComment($comment);
            $this->transition($nextTransition);
        }
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub

        if ($insert && !$this->isMobileSync) {
            // Create the related protocol form
            $this->addFormNumberToProtocol();
        }
        $this->changeUserAppDataVersion($insert, $changedAttributes);
    }

    public function addFormNumberToProtocol()
    {
        $model = Yii::$app->getModule('protocol')->getFormClass($this->protocol_form_table);
        $form = new $model;
        $form->protocol_id = $this->id;
        if ($form->save()) {
            $this->protocol_form_number = $form->id;
            $this->save();
        }
    }
    /**
     * @desc Checks if the step order was ever visited in the current protocol
     * @param integer $stepNumber The order_number of the workflowStep
     */
    public function isWorkflowStepVisited($stepNumber,$checkOnlyLastVisitedStep = false){
        if($checkOnlyLastVisitedStep==true){
            $comment = $this->getProtocolComments()
            ->orderBy('protocol_comment.created_at DESC')->one();
            if($comment && $comment->oldWorkflowStep){
                return $comment->oldWorkflowStep->order_number == $stepNumber;
            }
            return false;
        }

        $exist = $this
        ->getProtocolComments()
        ->joinWith('oldWorkflowStep')
        ->andWhere(['workflow_step.order_number'=>$stepNumber])
        ->exists();
        if($exist==false){
            $exist = $this
            ->getProtocolComments()
            ->joinWith('newWorkflowStep')
            ->andWhere(['workflow_step.order_number'=>$stepNumber])
            ->exists();
        }
        return $exist;
    }
    /**
     * @param $comment
     * @param string $event
     */
    public function addComment($comment, $event = ProtocolComment::EVENT_COMMENT)
    {
        if (empty($comment)) {
            return;
        }
        $object = new ProtocolComment();
        $object->protocol_id = $this->id;
        $object->comment = $comment;
        $object->event = $event;
        $object->name = 'a';
        if (!$object->save()) {
            dd($object->getErrors());
        }
    }
    /**
     * @param $comment
     * @param string $event
     */
    public function addFieldComment($comment, $fieldValidation,$attributeName)
    {
        if (empty($attributeName) || $this->workflow_step_id==null) {
            return;
        }
        
        $object = $this->getProtocolFieldComments()
        ->andWhere(['name'=>$attributeName])
        ->one();
        $validation = null;
        if($fieldValidation===null || $fieldValidation===''){
            $validation = null;
        }else{
            $validation = ($fieldValidation==1)?1:0;
        }
        if($object==null){
            $object = new ProtocolFieldComment();
        }
        $object->protocol_id = $this->id;
        $object->comment = $comment;
        $object->validation = $validation;
        $object->name = $attributeName;
        $object->workflow_step_id = $this->workflow_step_id;
        if (!$object->save()) {
            dd($object->getErrors());
        }
    }
    /**
     * Before we delete, make sure to clean up data.
     * @return bool
     */
    public function beforeDelete()
    {
        // Todo: handled by mysql foreign keys?
//        $form = $this->protocolForm();
//        if ($form) {
//            $form->delete();
//        }

        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

    /**
     * @return mixed|null|string
     */
    public function assignee()
    {
        return $this->workflowStep ? $this->workflowStep->assignee() : null;
    }

    /**
     * Determine if a user can fill out a protocol.
     * @return bool
     */
    public function canFill()
    {
        if (Yii::$app->user->can('Authority')) {
            return true;
        }
        return $this->canAccessWorkflowStep($this->workflowStep);
    }
    public function canAccessWorkflowStep(WorkflowStep $step){
        $previousStepAccessible = true;
        if($step->getType()===WorkflowStep::TYPE_FINAL){
            //If the step is final type then we check 
            //if the current user was able to access the 
            //previous steps with type INPUT or VALIDATION 
            $previousStepAccessible = false;
            $cmnts = $this->protocolComments;
            foreach($cmnts as $cmnt){
                if($cmnt->oldWorkflowStep && in_array($cmnt->oldWorkflowStep->getType(),[WorkflowStep::TYPE_INPUT,WorkflowStep::TYPE_VALIDATION]))
                {
                    $previousStepAccessible = $this->canAccessWorkflowStep($cmnt->oldWorkflowStep);
                    if($previousStepAccessible===true){
                        break;
                    }
                }
            }
        }
        
        if ($step->getUserId()) {
            return $previousStepAccessible && $step->getUserId() == Yii::$app->user->id;
        }
        return $previousStepAccessible && in_array($step->getRole(), Yii::$app->getUser()->getRoles(true));
    }
    /**
     * Determine if a user can fill out a protocol.
     * @return bool
     */
    public function canSee()
    {
        if($this->canFill()===true){
            return true;
        }
        $cmnts = $this->protocolComments;
        $allowed = false;
        foreach($cmnts as $cmnt){
            if($cmnt->oldWorkflowStep){
                $allowed = $this->canAccessWorkflowStep($cmnt->oldWorkflowStep);
                if($allowed===true){
                    break;
                }
            }
        }
        return $allowed;
    }

    public function getAdditionalUserIdsForChangeAppDataVersion($insert, $changedAttributes)
    {
        $protocolTemplate = $this->protocolTemplate;
        if (!$protocolTemplate) {
            return [];
        }
        $workflow = $protocolTemplate->workflow;
        if (!$workflow) {
            return [];
        }
        $workflowSteps = $workflow->workflowSteps;
        if (!$workflowSteps) {
            return [];
        }
        $specificUsersFromColumn = [];
        $userIdsByRoles = [];
        foreach ($workflowSteps as $workflowStep) {
            if ($workflowStep->getUserId()) {
                $specificUsersFromColumn[] = $workflowStep->getUserId();
            }
            if ($workflowStep->getRole()) {
                $userIdsByRole = Yii::$app->authManager->getUserIdsByRole($workflowStep->getRole());
                $userIdsByRoles = array_merge($userIdsByRoles, $userIdsByRole);
            }
        }

        return array_merge($specificUsersFromColumn, $userIdsByRoles);
    }

    public static function find($removedDeleted = true)
    {
        $model = parent::find($removedDeleted);
        if(array_key_exists('REQUEST_METHOD', $_SERVER)){
            $user = Yii::$app->user;
            $userIdentity = $user->getIdentity();
    
            if (!$user->can('Authority')) {
                if ($user->can('ProtocolAdmin') && $userIdentity->client_id) {
                    $model->andWhere(['client_id' => $userIdentity->client_id]);
                } else if ($user->can('ProtocolViewer')) {
                    $model->andWhere(['created_by' => $userIdentity->id]);
                }
            }
        }
        return $model;
    }

    public static function findWithOutSystemEntry($removedDeleted = true)
    {
        $model = self::find($removedDeleted);
        
        $model->findWithOutSystemEntry(self::tableName(),$removedDeleted);
        
        return $model;
    }


    public static function quickstart($templateId, $referenceModel = null, $referenceId = null)
    {
        $template = ProtocolTemplate::findOne($templateId);
        if ($template) {
            $protocol = new Protocol();
            $protocol->name = strtoupper(substr(md5(\Yii::$app->security->generateRandomString(15)), 0, 5));
            $protocol->protocol_template_id = $template->id;
            $protocol->protocol_form_table = $template->protocol_form_table ?: 'protocol_default';
            $protocol->workflow_step_id = $template->workflow->startingStep()->id;
            // why was this 0 before?
            $protocol->protocol_form_number = $referenceId;
            $protocol->client_id = $template->client_id;
            $protocol->reference_model = $referenceModel;
            $protocol->reference_id = $referenceId;
            if ($protocol->save()) {
                return $protocol->id;
            }
        }
        else {
          //  echo 'no template found';
        }

        return false;
    }
    public function afterFieldsChanged(AfterSaveEvent $event){
        $this->saveFieldsChangedComment($event->changedAttributes);
    }
    public function saveFieldsChangedComment($changedFieldsValue=[]){
        /*if(\Yii::$app->has('session') && !Yii::$app->user->can('Authority')) {
            return;
        }*/
        $model = $this->protocolForm();
        $skipAttributes = ['updated_at','updated_by'];
        $changedAttributes = [];
        foreach($changedFieldsValue as $k=>$j){
            if(strlen($model->{$k})!=strlen($j) || $j != $model->{$k}){
                $changedAttributes[$k] = $j;
            }
        }
        $changed = [];
        foreach($changedAttributes as $atr => $value) {
            if(in_array($atr, $skipAttributes) || CrudHelper::checkIfMultiUploaded($model->getTableSchema()->getColumn($atr))){
                continue;
            }
            $oldFormattedValue = $value;
            $newFormattedValue = $model->$atr;
            if($model->{$atr} != $value){
                $changed[$atr] = [
                    'old' => $oldFormattedValue,
                    'new' => $newFormattedValue
                ];
            }
        }
        if($changed) {
            $this->addComment(serialize($changed), ProtocolComment::EVENT_ATTRIBUTE_CHANGE);
        }
    }
}
