<?php
//Generation Date: 17-Dec-2020 01:01:25pm
namespace taktwerk\yiiboilerplate\modules\protocol\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\protocol\models\base\ProtocolFieldComment as BaseProtocolFieldComment;
use taktwerk\yiiboilerplate\modules\user\models\User;

/**
 * This is the model class for table "protocol_field_comment".
 */
class ProtocolFieldComment extends BaseProtocolFieldComment
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }
    /**
     * Get the body
     * @return string
     */
    public function body()
    {
        if (!empty($this->comment)) {
            $connectModel  = $this->protocol->protocolForm();
            $lbl = $connectModel->getAttributeLabel($this->name);
            return Yii::t('twbp', 'Commented on {fieldLabel}: {comment}', [
                'comment' => (strpos($this->comment, "\n") !== false ? "<br>" : null) . nl2br($this->comment),
                'fieldLabel'=>$lbl
            ]);
        }
        
        // If the  new step is final
        if ($this->workflowStep->isFinal()) {
            return Yii::t('twbp', 'Final');
        }
        return Yii::t(
            'twbp',
            'Workflow Step changed from \'{old}\' to \'{new}\'.',
            [
                'new' => $this->workflowStep->toString()
            ]);
    }
    /**
     * @return mixed|string
     */
    public function author()
    {
        if (!empty($this->created_by)) {
            $user = User::findOne($this->created_by);
            if (!empty($user)) {
                return $user->toString();
            }
        }
        return Yii::t('twbp', 'Unknown');
    }
    /**
     * @return string
     */
    public function icon()
    {
        if (!empty($this->comment)) {
            return 'fa-comment';
        }
        
        if ($this->workflowStep && $this->workflowStep->isFinal()) {
            return 'fa-check';
        }
        
        return 'fa-arrow-circle-right';
    }
    /**
     * @return string
     */
    public function colour()
    {
        if ($this->workflowStep && $this->workflowStep->isFinal()) {
            return 'blue';
        }
        
        return !empty($this->comment) ? 'yellow' : 'green';
    }
}
