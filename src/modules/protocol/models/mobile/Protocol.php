<?php

namespace taktwerk\yiiboilerplate\modules\protocol\models\mobile;

use taktwerk\yiiboilerplate\modules\protocol\models\Protocol as BaseProtocol;
use taktwerk\yiiboilerplate\traits\MobileDateTrait;

use taktwerk\yiiboilerplate\modules\sync\traits\UserAppDataVersionTrait;
use taktwerk\yiiboilerplate\traits\MobileFileTrait;

/**
 * This is the model class for table "protocol".
 */
class Protocol extends BaseProtocol
{
    use MobileDateTrait, MobileFileTrait;

    public function beforeSave($insert)
    {
        $this->beforeSaveForLocalDateAttributes($insert);

        return parent::beforeSave($insert);
    }

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $creator = \app\models\User::findOne($this->created_by);

        return [
            'id' => $this->id,
            'client_id' => $this->client_id,
            'creator' => $creator ? $creator->toString() : null,
            'name' => $this->name,
            'workflow_step_id' => $this->workflow_step_id,
            'protocol_template_id' => $this->protocol_template_id,
            'protocol_form_table' => $this->protocol_form_table,
            'protocol_form_number' => $this->protocol_form_number,
            'reference_model' => $this->reference_model,
            'reference_id' => $this->reference_id,
            'deleted_at' => $this->getIntDeletedAt(),
            'updated_at' => $this->getIntUpdatedAt(),
            'created_at' => $this->getIntCreatedAt(),
            'local_deleted_at' => $this->getIntLocalDeletedAt(),
            'local_updated_at' => $this->getIntLocalUpdatedAt(),
            'local_created_at' => $this->getIntLocalCreatedAt(),
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ];
    }
}
