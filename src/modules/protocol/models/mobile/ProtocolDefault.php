<?php

namespace taktwerk\yiiboilerplate\modules\protocol\models\mobile;

use taktwerk\yiiboilerplate\modules\protocol\models\ProtocolDefault as BaseProtocolDefault;
use taktwerk\yiiboilerplate\traits\MobileDateTrait;
use taktwerk\yiiboilerplate\modules\sync\traits\UserAppDataVersionTrait;
use Yii;
use taktwerk\yiiboilerplate\traits\MobileFileTrait;

/**
 * This is the model class for table "protocol_template".
 */
class ProtocolDefault extends BaseProtocolDefault
{
    use MobileDateTrait, MobileFileTrait;

    public function load($data, $formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;

//        $data['protocol_id'] = 100;
        if ($scope === '' && !empty($data)) {
            $this->setAttributes($data);

            return true;
        } elseif (isset($data[$scope])) {
            $this->setAttributes($data[$scope]);

            return true;
        }

        return false;
    }

    public function beforeSave($insert)
    {
        $this->beforeSaveForLocalDateAttributes($insert);

        return parent::beforeSave($insert);
    }

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        return [
            'id' => $this->id,
            'protocol_id' => $this->protocol_id,
            'protocol_file' => $this->protocol_file ?: null,
            'protocol_file_path' => $this->getFilePathByAttribute('protocol_file'),
            'pdf_image' => $this->pdf_image ? : null,
            'pdf_image_path' => $this->getFilePathByAttribute('pdf_image'),
            'thumb_protocol_file' => $this->getThumbFileName('protocol_file'),
            'thumb_protocol_file_path' => $this->getAbsoluteUrl($this->getThumbFileUrl('protocol_file')),
            'deleted_at' => $this->getIntDeletedAt(),
            'updated_at' => $this->getIntUpdatedAt(),
            'created_at' => $this->getIntCreatedAt(),
            'local_deleted_at' => $this->getIntLocalDeletedAt(),
            'local_updated_at' => $this->getIntLocalUpdatedAt(),
            'local_created_at' => $this->getIntLocalCreatedAt(),
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ];
    }
}
