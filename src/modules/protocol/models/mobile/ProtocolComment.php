<?php

namespace taktwerk\yiiboilerplate\modules\protocol\models\mobile;

use taktwerk\yiiboilerplate\traits\MobileDateTrait;
use \taktwerk\yiiboilerplate\modules\protocol\models\ProtocolComment as BaseProtocolComment;

/**
 * This is the model class for table "protocol_comment".
 */
class ProtocolComment extends BaseProtocolComment
{
    use MobileDateTrait;

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $arrayData = parent::toArray($fields, $expand, $recursive);
        $additionalData = [
            'creator' => $this->author(),
            'deleted_at' => $this->getIntDeletedAt(),
            'updated_at' => $this->getIntUpdatedAt(),
            'created_at' => $this->getIntCreatedAt(),
        ];

        return array_merge($arrayData, $additionalData);
    }
}
