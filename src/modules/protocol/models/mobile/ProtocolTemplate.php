<?php

namespace taktwerk\yiiboilerplate\modules\protocol\models\mobile;

use taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate as BaseProtocolTemplate;
use taktwerk\yiiboilerplate\traits\MobileDateTrait;
use taktwerk\yiiboilerplate\traits\MobileFileTrait;

/**
 * This is the model class for table "protocol_template".
 */
class ProtocolTemplate extends BaseProtocolTemplate
{
    use MobileDateTrait, MobileFileTrait;

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        return [
            'id' => $this->id,
            'client_id' => $this->client_id,
            'name' => $this->name,
            'workflow_id' => $this->workflow_id,
            'protocol_file' => $this->protocol_file ?: null,
            'protocol_file_path' => $this->getFilePathByAttribute('protocol_file'),
            'pdf_image' => $this->pdf_image ? : null,
            'pdf_image_path' => $this->getFilePathByAttribute('pdf_image'),
            'thumb_protocol_file' => $this->getThumbFileName('protocol_file'),
            'thumb_protocol_file_path' => $this->getAbsoluteUrl($this->getThumbFileUrl('protocol_file')),
            'deleted_at' => $this->getIntDeletedAt(),
            'updated_at' => $this->getIntUpdatedAt(),
            'created_at' => $this->getIntCreatedAt(),
            'local_deleted_at' => $this->getIntLocalDeletedAt(),
            'local_updated_at' => $this->getIntLocalUpdatedAt(),
            'local_created_at' => $this->getIntLocalCreatedAt(),
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ];
    }
}
