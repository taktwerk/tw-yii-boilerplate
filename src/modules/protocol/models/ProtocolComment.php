<?php

namespace taktwerk\yiiboilerplate\modules\protocol\models;

use app\models\User;
use taktwerk\yiiboilerplate\modules\sync\traits\UserAppDataVersionTrait;
use Yii;
use \taktwerk\yiiboilerplate\modules\protocol\models\base\ProtocolComment as BaseProtocolComment;
use yii\helpers\StringHelper;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use taktwerk\yiiboilerplate\components\Helper;
use yii\helpers\Inflector;

/**
 * This is the model class for table "protocol_comment".
 */
class ProtocolComment extends BaseProtocolComment
{
    use UserAppDataVersionTrait;
    /**
     * Some events
     */
    const EVENT_COMMENT = 'comment';
    const EVENT_START = 'start';
    const EVENT_ATTRIBUTE_CHANGE = 'change';

//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

    /**
     * Get the body
     * @return string
     */
    public function body()
    {
        if (!empty($this->comment)) {
            if($this->event === self::EVENT_ATTRIBUTE_CHANGE) {
                try{
                    $changed = unserialize($this->comment);
                    $model = $this->protocol->protocolForm();
                    if($model==null){
                        return '';
                    }
                    $transCat = StringHelper::startsWith(get_class($model),'app\\')?'app':'twbp';
                    $comment = Yii::t('twbp', 'Changed') . '<br />';
                    $hasOneRelations = ClassDispenser::getMappedClass(Helper::class)::getModelHasOneRelations($model);
                    foreach ($changed as $key => $values) {
                        $oldValue = $values['old'];
                        $newValue = $values['new'];
                        $oldFormattedValue = $oldValue;
                        $newFormattedValue = $newValue;
                        $col = $model->getTableSchema()->getColumn($key);
                        if($col && StringHelper::startsWith($col->dbType,'enum(',false)){
                            $oldFormattedValue = \Yii::t($transCat,Inflector::humanize($oldValue));
                            $newFormattedValue = \Yii::t($transCat,Inflector::humanize($newValue));
                        }
                        try{
                            foreach($hasOneRelations as $rel){
                                if(in_array($key,$rel['link'])){
                                    $relClass = $rel['modelClass'];
                                    $condition1 = [];
                                    $condition1[] = 'AND';
                                    $condition2 =  $condition1;
                                    foreach($rel['link'] as $j => $k){
                                        $condition1[] = [$j=>$oldValue];
                                        $condition2[] = [$j=>$newValue];
                                    }
                                    $oldRecord = ($oldValue)?$relClass::find()->where($condition1)->one():null;
                                    $newRecord = ($newValue)?$relClass::find()->where($condition2)->one():null;
                                    $oldFormattedValue = ($oldRecord && method_exists($oldRecord, 'getToString'))?$oldRecord->getToString():$oldValue;
                                    $newFormattedValue = ($newRecord && method_exists($newRecord, 'getToString'))?$newRecord->getToString():$newValue;
                                    break;
                                }
                            }
                        }catch (\Exception $e){}
                        //$comment .= $this->getAttributeLabel($key) . ': ' . $values['old'] . ' => ' . $values['new'] . "\r\n";
                        $comment .= $model->getAttributeLabel($key) . ': ' . $oldFormattedValue . ' => ' . $newFormattedValue . '<br />';
                    }
                }catch(\Exception $e){
                    return $comment = Yii::t('twbp', 'Changed') . ':<br />';$this->comment;
                }
                return $comment;
            }
            return Yii::t('twbp', 'Commented: {comment}', [
                'comment' => (strpos($this->comment, "\n") !== false ? "<br>" : null) . nl2br($this->comment)
            ]);
        }

        // If the  new step is final
        if ($this->newWorkflowStep->isFinal()) {
            return Yii::t('twbp', 'Final');
        }
        return Yii::t(
            'twbp',
            'Workflow Step changed from \'{old}\' to \'{new}\'.',
            [
                'old' => $this->oldWorkflowStep->toString(),
                'new' => $this->newWorkflowStep->toString()
            ]);
    }

    /**
     * @return mixed|string
     */
    public function author()
    {
        if (!empty($this->created_by)) {
            $user = User::findOne($this->created_by);
            if (!empty($user)) {
                return $user->toString();
            }
        }
        return Yii::t('twbp', 'Unknown');
    }

    /**
     * @return string
     */
    public function icon()
    {
        if (!empty($this->comment)) {
            return 'fa-comment';
        }

        if ($this->newWorkflowStep && $this->newWorkflowStep->isFinal()) {
            return 'fa-check';
        }

        return 'fa-arrow-circle-right';
    }

    /**
     * @return string
     */
    public function colour()
    {
        if ($this->newWorkflowStep && $this->newWorkflowStep->isFinal()) {
            return 'blue';
        }

        return !empty($this->comment) ? 'yellow' : 'green';
    }

    public function clientsForChangeAppDataVersion()
    {
        $clientIds = [$this->protocol->client_id];

        return $clientIds;
    }

    public function getAdditionalUserIdsForChangeAppDataVersion($insert, $changedAttributes)
    {
        $protocolTemplate = $this->protocol->protocolTemplate;
        if (!$protocolTemplate) {
            return [];
        }
        $workflow = $protocolTemplate->workflow;
        if (!$workflow) {
            return [];
        }
        $workflowSteps = $workflow->workflowSteps;
        if (!$workflowSteps) {
            return [];
        }
        $specificUsersFromColumn = [];
        $userIdsByRoles = [];
        foreach ($workflowSteps as $workflowStep) {
            if ($workflowStep->user_id) {
                $specificUsersFromColumn[] = $workflowStep->user_id;
            }
            if ($workflowStep->role) {
                $userIdsByRole = Yii::$app->authManager->getUserIdsByRole($workflowStep->role);
                $userIdsByRoles = array_merge($userIdsByRoles, $userIdsByRole);
            }
        }

        return array_merge($specificUsersFromColumn, $userIdsByRoles);
    }
}
