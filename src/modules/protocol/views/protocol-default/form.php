<?php

use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;

/** @var $model \taktwerk\yiiboilerplate\modules\protocol\models\Protocol */
$formModel = $model->protocolForm();
$disabled = !$model->workflowStep->isFormEnabled();
?>
<div class="row">
    <?= $form->field($formModel, 'protocol_id')->hiddenInput(['value'=> $formModel->protocol_id])->label(false) ?>
    <div class="col-md-6">
        <?= $form->field(
            $formModel,
            'protocol_file',
            [
                'selectors' => [
                    'input' => '#' .
                        Html::getInputId($formModel, 'protocol_file') . $owner
                ]
            ]
        )->widget(
            \taktwerk\yiiboilerplate\widget\FileInput::class,
            [
                'pluginOptions' => [
                    'showUpload' => false,
                    'showRemove' => false,


                    'initialPreview' => (!empty($formModel->protocol_file) ? [
                        $formModel->getFileUrl('protocol_file')
                    ] : ''),
                    'initialCaption' => $formModel->protocol_file,
                    'initialPreviewAsData' => true,
                    'initialPreviewFileType' => $formModel->getFileType('protocol_file'),
                    'fileActionSettings' => [
                        'indicatorNew' => $formModel->protocol_file === null ? '' : '<a href=" ' . $formModel->getFileUrl('protocol_file') . '" target="_blank"><i class="glyphicon glyphicon-hand-down text-warning"></i></a>',
                        'indicatorNewTitle' => \Yii::t('twbp','Download'),
                    ],
                    'overwriteInitial' => true,
                    'initialPreviewConfig'=> [
                        [
                            'downloadUrl'=> $formModel->getFileUrl('protocol_file')
                        ],
                    ]
                ],
                'pluginEvents' => [
                    'fileclear' => 'function() { var prev = $("input[name=\'" + $(this).attr("name") + "\']")[0]; $(prev).val(-1); }',

                ],
                'options' => [
                    'id' => Html::getInputId($formModel, 'protocol_file') . $owner,
                    'accept' => 'image/*;capture=camera',
                ]
            ]
        )
            ->hint($formModel->getAttributeHint('protocol_file'))
        ?>
    </div>
    <div class="col-md-6">
        <?= $form->field(
            $formModel,
            'pdf_image',
            [
                'selectors' => [
                    'input' => '#' .
                        Html::getInputId($formModel, 'pdf_image') . $owner
                ]
            ]
        )->widget(
            \taktwerk\yiiboilerplate\widget\FileInput::class,
            [
                'pluginOptions' => [
                    'showUpload' => false,
                    'showRemove' => false,


                    'initialPreview' => (!empty($formModel->pdf_image) ? [
                        $formModel->getFileUrl('pdf_image')
                    ] : ''),
                    'initialCaption' => $formModel->pdf_image,
                    'initialPreviewAsData' => true,
                    'initialPreviewFileType' => $formModel->getFileType('pdf_image'),
                    'fileActionSettings' => [
                        'indicatorNew' => $formModel->pdf_image === null ? '' : '<a href=" ' . $formModel->getFileUrl('pdf_image') . '" target="_blank"><i class="glyphicon glyphicon-hand-down text-warning"></i></a>',
                        'indicatorNewTitle' => \Yii::t('twbp','Download'),
                    ],
                    'overwriteInitial' => true,
                    'initialPreviewConfig'=> [
                     [
                         // 'url' => Url::toRoute(['delete-file','id'=>$model->getPrimaryKey(), 'attribute' => 'attached_file']),
                         'downloadUrl'=> $formModel->getFileUrl('pdf_image'),
                         // 'filetype' => !empty($model->getMinFilePath('attached_file') && $model->getFileType('attached_file') == 'video') ? 'video/mp4':'',
                     ],
                    ],
                ],
                'pluginEvents' => [
                    'fileclear' => 'function() { var prev = $("input[name=\'" + $(this).attr("name") + "\']")[0]; $(prev).val(-1); }',

                ],
                'options' => [
                    'id' => Html::getInputId($formModel, 'pdf_image') . $owner,
                    'accept' => 'image/*;capture=camera',
                ]
            ]
        )
            ->hint($formModel->getAttributeHint('pdf_image'))
        ?>
    </div>
</div>
