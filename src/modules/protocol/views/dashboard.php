<?php if (!Yii::$app->getUser()->can('protocol_protocol_fill')) {
    return;
}

// Limit to finding protocols whose steps are assigned to us
$stepIds = [];
$roles = Yii::$app->getUser()->getRoles(true);
$steps = \taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()
    ->where(['user_id' => Yii::$app->getUser()->id])
    ->orWhere(['in', 'role', $roles])
    ->all();
foreach ($steps as $step) {
    $stepIds[] = $step->id;
}

$protocols = \taktwerk\yiiboilerplate\modules\protocol\models\Protocol::find()->where(['in', 'workflow_step_id', $stepIds])->all();

?>
<div class="col-md-6 col-lg-4">
    <div class="box box-widget widget-user-2 kpi">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-yellow">
            <div class="widget-user-image">
                <i class="fa fa-list fa-3x pull-left"></i>
            </div>
            <!-- /.widget-user-image -->
            <h3 class="widget-user-username">
                <?= Yii::t('twbp','Protocols') ?>
            </h3>
        </div>
        <div class="box-body">

            <ul class="products-list product-list-in-box">
                <?php foreach($protocols as $protocol): ?>
                <li class="item">
                    <div class="product-info">
                        <a href="<?=\yii\helpers\Url::to(['/protocol/protocol/fill', 'id' => $protocol->id])?>">
                            <?=$protocol->name?>
                            <span class="label label-default pull-right"><?=$protocol->workflowStep->name?></span>
                        </a>
                    </div>
                </li>
                <?php endforeach; ?>
                <?php if (count($protocols) == 0): ?>
                <li class="item">
                    <div class="product-info">
                        <?=Yii::t('twbp', 'All done!')?>
                    </div>
                </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>
