<?php
//Generation Date: 11-Sep-2020 02:56:41pm
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\protocol\models\ProtocolComment $model
 * @var string $relatedTypeForm
 */

$this->title = Yii::t('twbp', 'Protocol Comment') . ' #' . (is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey) . ', ' . Yii::t('twbp', 'Edit') . ' - ' . $model->toString();
if(!$fromRelation) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('twbp', 'Protocol Comments'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => (string)is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('twbp', 'Edit');
}
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body protocol-comment-update">

        <div class="crud-navigation">

            <?= Html::a(
                Yii::t('twbp', 'Cancel'),
                $fromRelation ? urldecode($fromRelation) : \yii\helpers\Url::previous(),
                ['class' => 'btn btn-default cancel-form-btn']
            ) ?>
            <?= (!$fromRelation ? Html::a(
                '<span class="glyphicon glyphicon-eye-open"></span> ' . Yii::t('twbp', 'View'),
                [
                    'view',
                    'id' => $model->id,
                    'show_deleted'=>Yii::$app->request->get('show_deleted')
                ],
                ['class' => 'btn btn-default']
            ) : '') ?>
            <?=  \taktwerk\yiiboilerplate\widget\CustomActionDropdownWidget::widget(['model'=>$model]) ?>
            <div class="pull-right">
                <?php                 \yii\bootstrap\Modal::begin([
                'header' => '<h2>' . Yii::t('twbp', 'Information') . '</h2>',
                'toggleButton' => [
                'tag' => 'btn',
                'label' => '?',
                'class' => 'btn btn-default',
                'title' => Yii::t('twbp', 'Help')
                ],
                ]);?>

                <?= $this->render('@taktwerk-boilerplate/views/_shortcut_info_modal') ?>
                <?php  \yii\bootstrap\Modal::end();
                ?>
            </div>
        </div>

        <?php echo $this->render('_form', [
            'model' => $model,
            'inlineForm' => $inlineForm,
            'relatedTypeForm' => $relatedTypeForm,
            'fromRelation' => $fromRelation,
        ]); ?>

    </div>
</div>