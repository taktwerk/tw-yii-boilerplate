<?php
//Generation Date: 11-Sep-2020 02:56:41pm
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\widget\form\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\components\ClassDispenser;
/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\protocol\models\ProtocolComment $model
 * @var taktwerk\yiiboilerplate\widget\form\ActiveForm $form
 * @var boolean $useModal
 * @var boolean $multiple
 * @var array $pk
 * @var array $show
 * @var string $action
 * @var string $owner
 * @var array $languageCodes
 * @var boolean $relatedForm to know if this view is called from ajax related-form action. Since this is passing from
 * select2 (owner) it is always inherit from main form
 * @var string $relatedType type of related form, like above always passing from main form
 * @var string $owner string that representing id of select2 from which related-form action been called. Since we in
 * each new form appending "_related" for next opened form, it is always unique. In main form it is always ID without
 * anything appended
 */

$owner = $relatedForm ? '_' . $owner . '_related' : '';
$relatedTypeForm = Yii::$app->request->get('relatedType')?:$relatedTypeForm;

if (!isset($multiple)) {
$multiple = false;
}

if (!isset($relatedForm)) {
$relatedForm = false;
}

$tableHint = (!empty(taktwerk\yiiboilerplate\modules\protocol\models\ProtocolComment::tableHint()) ? '<div class="table-hint">' . taktwerk\yiiboilerplate\modules\protocol\models\ProtocolComment::tableHint() . '</div><hr />' : '<br />');

?>
<div class="protocol-comment-form">
        <?php  $formId = 'ProtocolComment' . ($ajax || $useModal ? '_ajax_' . $owner : ''); ?>    
    <?php $form = ActiveForm::begin([
        'id' => $formId,
        'layout' => 'default',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'action' => $useModal ? $action : '',
        'options' => [
        'name' => 'ProtocolComment',
            ],
    ]); ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>
        <?php echo $tableHint; ?>

        <?php if ($multiple) : ?>
            <?=Html::hiddenInput('update-multiple', true)?>
            <?php foreach ($pk as $id) :?>
                <?=Html::hiddenInput('pk[]', $id)?>
            <?php endforeach;?>
        <?php endif;?>

        <?php $fieldColumns = [
                
            'protocol_id' => 
            $form->field(
                $model,
                'protocol_id'
            )
                    ->widget(
                        Select2::class,
                        [
                            'data' => taktwerk\yiiboilerplate\modules\protocol\models\Protocol::find()->count() > 50 ? null : ArrayHelper::map(taktwerk\yiiboilerplate\modules\protocol\models\Protocol::find()->all(), 'id', 'toString'),
                            'initValueText' => taktwerk\yiiboilerplate\modules\protocol\models\Protocol::find()->count() > 50 ? \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\modules\protocol\models\Protocol::find()->andWhere(['id' => $model->protocol_id])->all(), 'id', 'toString') : '',
                            'options' => [
                                'placeholder' => Yii::t('twbp', 'Select a value...'),
                                'id' => 'protocol_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                            ],
                            'pluginOptions' => [
                                'allowClear' => false,
                                (taktwerk\yiiboilerplate\modules\protocol\models\Protocol::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                                (taktwerk\yiiboilerplate\modules\protocol\models\Protocol::find()->count() > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'Protocol\'
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                                    var data_id = $(this);
                                $.post('" .
            Url::toRoute('/protocol/protocol/system-entry?id=', true) .
            "' + data_id.val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if(!json.success){
                                                if ((data_id.val() != null) && (data_id.val() != '')) {
                                        // Enable edit icon
                                        $(data_id.next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
            Url::toRoute('/protocol/protocol/entry-details?id=', true) .
            "' + data_id.val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#protocol_id_well').html(json.data);
                                                //$('#protocol_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $(data_id.next().next().children('button')[0]).prop('disabled', true);
                                    }
                                            }else{
                                        // Disable edit icon and remove sub-form
                                        $(data_id.next().next().children('button')[0]).prop('disabled', true);
                                            }
                                        });
                                }",
                            ],
                            'addon' => (Yii::$app->getUser()->can('modules_protocol_create') && (!$relatedForm || ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL && !$useModal))) ? [
                                'append' => [
                                    'content' => [
                                        ClassDispenser::getMappedClass(RelatedForms::class)::widget([
                                            'relatedController' => '/protocol/protocol',
                                            'type' => $relatedTypeForm,
                                            'selector' => 'protocol_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'primaryKey' => 'id',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    ) . '
                    
                
                <div id="protocol_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' . 
                    (taktwerk\yiiboilerplate\modules\protocol\models\Protocol::findOne(['id' => $model->protocol_id])
                        ->entryDetails == '' ?
                    ' display:none;' :
                    '') . '
                    ">' . 
                    (taktwerk\yiiboilerplate\modules\protocol\models\Protocol::findOne(['id' => $model->protocol_id])->entryDetails != '' ?
                    taktwerk\yiiboilerplate\modules\protocol\models\Protocol::findOne(['id' => $model->protocol_id])->entryDetails :
                    '') . ' 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>',
            'comment' => 
            $form->field(
                $model,
                'comment',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'comment') . $owner
                    ]
                ]
            )
                    ->textarea(
                        [
                            'rows' => 6,
                            'placeholder' => $model->getAttributePlaceholder('comment'),
                            'id' => Html::getInputId($model, 'comment') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('comment')),
            'event' => 
            $form->field(
                $model,
                'event',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'event') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('event'),
                            'id' => Html::getInputId($model, 'event') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('event')),
            'name' => 
            $form->field(
                $model,
                'name',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'name') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('name'),
                            'id' => Html::getInputId($model, 'name') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('name')),
            'old_workflow_step_id' => 
            $form->field(
                $model,
                'old_workflow_step_id'
            )
                    ->widget(
                        Select2::class,
                        [
                            'data' => taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()->count() > 50 ? null : ArrayHelper::map(taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()->all(), 'id', 'toString'),
                            'initValueText' => taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()->count() > 50 ? \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()->andWhere(['id' => $model->old_workflow_step_id])->all(), 'id', 'toString') : '',
                            'options' => [
                                'placeholder' => Yii::t('twbp', 'Select a value...'),
                                'id' => 'old_workflow_step_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                (taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                                (taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()->count() > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'WorkflowStep\'
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
                                        Url::toRoute('/workflow/workflow-step/entry-details?id=', true) .
                                        "' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#old_workflow_step_id_well').html(json.data);
                                                //$('#old_workflow_step_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }",
                            ],
                            'addon' => (Yii::$app->getUser()->can('modules_workflow-step_create') && (!$relatedForm || ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL && !$useModal))) ? [
                                'append' => [
                                    'content' => [
                                        ClassDispenser::getMappedClass(RelatedForms::class)::widget([
                                            'relatedController' => '/workflow/workflow-step',
                                            'type' => $relatedTypeForm,
                                            'selector' => 'old_workflow_step_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'primaryKey' => 'id',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    ) . '
                    
                
                <div id="old_workflow_step_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' . 
                    (taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::findOne(['id' => $model->old_workflow_step_id])
                        ->entryDetails == '' ?
                    ' display:none;' :
                    '') . '
                    ">' . 
                    (taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::findOne(['id' => $model->old_workflow_step_id])->entryDetails != '' ?
                    taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::findOne(['id' => $model->old_workflow_step_id])->entryDetails :
                    '') . ' 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>',
            'new_workflow_step_id' => 
            $form->field(
                $model,
                'new_workflow_step_id'
            )
                    ->widget(
                        Select2::class,
                        [
                            'data' => taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()->count() > 50 ? null : ArrayHelper::map(taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()->all(), 'id', 'toString'),
                            'initValueText' => taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()->count() > 50 ? \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()->andWhere(['id' => $model->new_workflow_step_id])->all(), 'id', 'toString') : '',
                            'options' => [
                                'placeholder' => Yii::t('twbp', 'Select a value...'),
                                'id' => 'new_workflow_step_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                (taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                                (taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()->count() > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'WorkflowStep\'
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
                                        Url::toRoute('/workflow/workflow-step/entry-details?id=', true) .
                                        "' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#new_workflow_step_id_well').html(json.data);
                                                //$('#new_workflow_step_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }",
                            ],
                            'addon' => (Yii::$app->getUser()->can('modules_workflow-step_create') && (!$relatedForm || ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL && !$useModal))) ? [
                                'append' => [
                                    'content' => [
                                        ClassDispenser::getMappedClass(RelatedForms::class)::widget([
                                            'relatedController' => '/workflow/workflow-step',
                                            'type' => $relatedTypeForm,
                                            'selector' => 'new_workflow_step_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'primaryKey' => 'id',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    ) . '
                    
                
                <div id="new_workflow_step_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' . 
                    (taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::findOne(['id' => $model->new_workflow_step_id])
                        ->entryDetails == '' ?
                    ' display:none;' :
                    '') . '
                    ">' . 
                    (taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::findOne(['id' => $model->new_workflow_step_id])->entryDetails != '' ?
                    taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::findOne(['id' => $model->new_workflow_step_id])->entryDetails :
                    '') . ' 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>',]; ?>        <div class='clearfix'></div>
<?php $twoColumnsForm = true; ?>
<?php 
// form fields overwrite
$fieldColumns = Yii::$app->controller->crudColumnsOverwrite($fieldColumns, 'form', $model, $form, $owner);

foreach($fieldColumns as $attribute => $fieldColumn) {
            if (count($model->primaryKey())>1 && in_array($attribute, $model->primaryKey()) && $model->checkIfHidden($attribute)) {
            echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
            echo $fieldColumn;
            echo $twoColumnsForm ? '</div>' : '';
        } elseif ($model->checkIfHidden($attribute)) {
            echo $fieldColumn;
        } elseif (!$multiple || ($multiple && isset($show[$attribute]))) {
            if ((isset($show[$attribute]) && $show[$attribute]) || !isset($show[$attribute])) {
                echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
                echo $fieldColumn;
                echo $twoColumnsForm ? '</div>' : '';
            } else {
                echo $fieldColumn;
            }
        }
                
} ?><div class='clearfix'></div>

                <?php $this->endBlock(); ?>
        
        <?= ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_TAB) ?
            Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                    'label' => Yii::t('twbp', 'Protocol Comment'),
                    'content' => $this->blocks['main'],
                    'active' => true,
                ],
                ]
            ])
            : $this->blocks['main']
        ?>        
        <div class="col-md-12">
        <hr/>
        
        </div>
        
        <div class="clearfix"></div>
        <?php echo $form->errorSummary($model); ?>
        <?php echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::formButtons($model, $useModal, $relatedForm, $multiple, "twbp", ['id' => $model->id]);?>
        <?php ClassDispenser::getMappedClass(ActiveForm::class)::end(); ?>
        <?= ($relatedForm && $relatedType == ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL) ?
        '<div class="clearfix"></div>' :
        ''
        ?>
        <div class="clearfix"></div>
    </div>
</div>

<?php
if ($useModal) {
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::class)::register($this);
}
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
if(!isset($useDependentFieldsJs) || $useDependentFieldsJs==true){
$this->render('@taktwerk-views/_dependent-fields', ['model' => $model,'formId'=>$formId]);
}
?>