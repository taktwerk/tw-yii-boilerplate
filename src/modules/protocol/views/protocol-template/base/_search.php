<?php
//Generation Date: 28-Sep-2020 08:02:17am
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\protocol\models\search\ProtocolTemplate $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="protocol-template-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'client_id') ?>

        <?= $form->field($model, 'name') ?>

        <?= $form->field($model, 'workflow_id') ?>

        <?= $form->field($model, 'protocol_form_table') ?>

        <?php // echo $form->field($model, 'protocol_file') ?>

        <?php // echo $form->field($model, 'protocol_file_filemeta') ?>

        <?php // echo $form->field($model, 'thumbnail') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

        <?php // echo $form->field($model, 'local_created_at') ?>

        <?php // echo $form->field($model, 'local_updated_at') ?>

        <?php // echo $form->field($model, 'local_deleted_at') ?>

        <?php // echo $form->field($model, 'pdf_image') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
