<?php use yii\helpers\ArrayHelper;

/** @var $comment \taktwerk\yiiboilerplate\modules\protocol\models\ProtocolComment */ ?>
<!--<div class="box box-default">-->
<!--    <div class="box-header">-->
<!--        <h3 class="box-title">--><?//=Yii::t('twbp', 'History')?><!--</h3>-->
<!--    </div>-->
<!--    <div class="box-body">-->
<!---->
<!--    </div>-->
<!--</div>-->

<?php
$user = Yii::t('twbp', 'Unknown');
$u = \app\models\User::findOne($model->created_by);
if ($u) {
    $user = $u->toString();
}
?>
<div class="box box-default collapsed-box">
    <div class="box-header with-border" data-widget="collapse">
        <h3 class="box-title"><i class="fa fa-file-text"></i> <?= Yii::t('twbp', 'Protocol Comments'); ?></h3>
        <div class="box-tools pull-right">
            <span ><?= $model->getProtocolComments()->count(); ?></span>
            <!-- Collapse Button -->
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-plus"></i>
            </button>
        </div>
        <!-- /.box-tools -->
    </div>
    <div class="box-body" style="display: none">
        <ul class="timeline">
            <?php foreach ($model->getProtocolComments()->orderBy(['created_at' => SORT_DESC,'id'=>SORT_DESC])->all() as $comment): ?>
                <li>
                    <i class="fa <?=$comment->icon() ?> bg-<?=$comment->colour() ?>"></i>
                    <div class="timeline-item">
                    <span class="time" title="<?=Yii::$app->formatter->asDatetime($comment->created_at) ?>">
                        <i class="fa fa-clock-o"></i> <?=Yii::$app->formatter->format($comment->created_at, 'relativeTime') ?>
                    </span>

                        <h3 class="timeline-header no-border">
                            <a href="#"><?=$comment->author()?></a> <?=$comment->body() ?>
                        </h3>
                    </div>
                </li>
            <?php endforeach; ?>
            <li>
                <i class="fa fa-user bg-aqua"></i>
                <div class="timeline-item">
                    <span class="time" title="<?=Yii::$app->formatter->asDatetime($model->created_at) ?>">
                        <i class="fa fa-clock-o"></i> <?=Yii::$app->formatter->format($model->created_at, 'relativeTime') ?>
                    </span>

                    <h3 class="timeline-header no-border"><a href="#"><?=$user?></a> <?=Yii::t('twbp', 'Created')?></h3>
                </div>
            </li>
        </ul>
    </div>
</div>
