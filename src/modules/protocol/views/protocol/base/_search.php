<?php
//Generation Date: 11-Sep-2020 02:56:40pm
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\protocol\models\search\Protocol $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="protocol-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'client_id') ?>

        <?= $form->field($model, 'protocol_template_id') ?>

        <?= $form->field($model, 'workflow_step_id') ?>

        <?= $form->field($model, 'name') ?>

        <?php // echo $form->field($model, 'protocol_form_table') ?>

        <?php // echo $form->field($model, 'protocol_form_number') ?>

        <?php // echo $form->field($model, 'reference_model') ?>

        <?php // echo $form->field($model, 'reference_id') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

        <?php // echo $form->field($model, 'local_created_at') ?>

        <?php // echo $form->field($model, 'local_updated_at') ?>

        <?php // echo $form->field($model, 'local_deleted_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
