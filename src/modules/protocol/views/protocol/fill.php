<?php

use yii\helpers\Html;

use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use yii\web\View;
use yii\helpers\Json;
use yii\bootstrap\Modal;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\protocol\models\Protocol $model
 * @var string $relatedTypeForm
 */

$this->title = Yii::t('twbp', 'Protocol Fill');
$this->params['breadcrumbs'][] = ['label' => Yii::t('twbp', 'Protocols'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$css = <<<EOT
.parent-field-cmnt-div {
    background-color: #ebf1f3;
    width: calc(100% - 30px);
    margin-left: 15px;
    padding-top: 5px;
    margin-bottom: 5px;
    float: left;
}
.comment-block {
    display: flex;
    justify-content: space-between;
}
.comment-group {
    width: calc(100% - 115px);
}
.comment-block .control-label {
    color: #737373;
    font-size: 12px;
}
EOT;
$this->registerCss($css,[],'field-comment-css');
$protoForm = $model->protocolForm();
$formId = 'Protocol';
?>

<div class="protocol-form">
<?php $form = ActiveForm::begin([
    'fieldClass' => '\taktwerk\yiiboilerplate\widget\ActiveField',
    'id' => $formId,
    'layout' => 'default',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
    'options' => [
        'name' => 'Protocol',
    ],
]); ?>
<div class="box box-default">
    <div class="giiant-crud box-body protocol-create">
        <?php /*
 // back button does not work
        <div class="clearfix crud-navigation">
            <div class="pull-left">
                <?= Html::a(
                    Yii::t('twbp', 'Cancel'),
                    \yii\helpers\Url::previous(),
                    [
                        'class' => 'btn btn-default'
                    ]
                ) ?>
            </div>
        </div>
 */ ?>
            <?php 
                if ($protoForm) {
                echo $this->render($protoForm->formLocation(), compact('form', 'model'));
                }
            ?>

            <?php if(!$model->workflowStep->isFinal()): ?>
            <div class="row">
                <div class="col-md-12">
                    <?= Html::beginTag('div', ['class' => 'form-group']) ?>
                    <?= Html::label(Yii::t('twbp', 'Notes', ['class' => 'control-label'])); ?>
                    <?= Html::textarea('comment', '', ['class' => 'form-control', 'rows' => 5, 
                        'placeholder' => Yii::t('twbp', 'Comments are visible for all protocol editors of this workflow. You can leave multiple comments, one with every save. Each comment will be logged and cannot be updated/deleted.')]) ?>
                    <?= Html::endTag('div') ?>
                </div>
            </div>
<?php echo Html::hiddenInput('is_transitioning','0',['id'=>'is-trans-fld']);?>
            <?= Html::submitButton(
                '<span class="glyphicon glyphicon-check"></span> ' . Yii::t('twbp', 'Save'),
                [
                    'id' => 'save-' . $model->formName(),
                    'class' => 'btn btn-success',
                    'name' => 'submit-default',
                    'onClick'=>'$("#is-trans-fld").val(0);$("input[type=\'file\']").each(function( index,v ) { if($(v).data("fileinput")){$(v).data("fileinput").minFileCount=0;}}); return true;',
                ]
            ); ?>
            <?php endif; ?>
        </div>
    </div>

<?php if(!$model->workflowStep->isFinal()): ?>
    <div id="transition-buttons" class="box box-info">
        <div class="giiant-crud box-body">
            <?php $tempi=1; foreach ($model->workflowStep->getWorkflowTransitions0()->orderBy(['default_order' => SORT_ASC])->all() as $transition): ?>
                <?php if($transition->default_order || \Yii::$app->getUser()->can('Authority')) : // hide if default_order is 0 ?>
                    <?php  $btnId = $model->formName().'tran_'.$transition->id;
                    
                    echo Html::button(
                        $transition->favicon() . Yii::t('twbp', $transition->action_key) . (!$transition->default_order && \Yii::$app->getUser()->can('Authority') ? ' (Authority only)' : ''),
                        [
                            'id' => $btnId,
                        //    'id' => $model->formName().'tran_ '.$transition->id,
                            'class' => 'btn btn-secondary',
                            'onClick'=>'$("#is-trans-fld").val(1);',
                            'data-skip-validation'=>($transition->skip_step_validation==1)?'true':'false'
                         ]);
                    $extraJs = '';
                    if($transition->skip_step_validation==1){
                        $extraJs = <<<EOT
$("input[type=\'file\']").each(function( index,v ) { 
if($(v).data("fileinput")){ $(v).data("fileinput").minFileCount=0;
}
});
data.settings.validateOnSubmit = false;
var atrs = data.attributes;
var atrIds = [];
atrs.forEach(function(item){
    atrIds.push(item.id);
});
for(var i = 0;i < atrIds.length;i++){
            $('#{$formId}').yiiActiveForm('remove', atrIds[i]);
}

EOT;
                    }
                    $this->registerJs(<<<EOT
    $('#{$btnId}').click(function(){
    var data = $('#{$formId}').data('yiiActiveForm');
    {$extraJs}
    $.each(data.attributes, function() {this.status = 3;}); 
    $('#{$formId}').one( "afterValidate", function(event, attribute, messages) {
      if(messages.length==0){
            \$('#confirmTrans{$tempi}').modal('show');
        }else{
            $("#is-trans-fld").val(0);
            setTimeout(function(){ $('html, body').animate({
                        scrollTop: $(".has-error:first").offset().top
                    },3);
            },100)
            
        }
    });

    $('#{$formId}').yiiActiveForm('validate');

    return false;
    });
    EOT
    ,View::POS_END);
                    ?>
                        <?php

                            Modal::begin([
                                        'header' =>'<h4>'.\Yii::t('twbp', 'Change Workflow Step').'</h4>',
                                        'toggleButton' => false,
                                        'id' => "confirmTrans{$tempi}",
                                        'footer' => Html::button(\Yii::t('twbp', 'No'), [
                                            'class' => "btn btn-default",
                                            'data' => [
                                                'dismiss' => 'modal'
                                            ]
                                        ]) . ' ' . Html::submitButton(\Yii::t('twbp','Yes'),[
                                                            'class'=>"btn btn-success",
                                                            'name' => $transition->submitActionName(),
                                                            'onClick'=>"$('#confirmTrans{$tempi}').modal('hide')"
                                                    ])
                                                ]);
                ?>
                            <p><?= \Yii::t('twbp','Do you really want to run this workflow transition?')?>
                            <?php if($transition->skip_step_validation==1){?>
                            	<br/><br/><?= \Yii::t('twbp','Please note: This step will only save the Notes section comment and not the other changes made in the form.')?>
                            <?php }?>
                            </p>
                        <?php Modal::end();?>
                <?php endif; ?>
			<?php $tempi++; endforeach; ?>
        </div>
    </div>
    
<?php endif; ?>

<?php ActiveForm::end()?>
</div>
<?= $this->render('comments', compact('model')) ?>
<?php 
if($protoForm){
$protoFormName = ($protoForm)?$protoForm->formName():false;
$isFinalJson = 'false';
if($model->workflowStep->isFinal()){
   $isFinalJson = 'true';
}
    $existingComments = Json::encode($model->getProtocolFieldComments()
    ->select(['name','comment','validation'])->indexBy('name')->asArray()->all());
    $commentTranslation = Yii::t('twbp', 'Comment');
    $validationTranslation = Yii::t('twbp', 'Validation');
    $yesTranslation = Yii::t('twbp', 'Yes');
    $noTranslation = Yii::t('twbp', 'No');

    $js = <<<EOT
    var i = 0;
    var inputs = $('.protocol-form:first form:first').find('input[type="file"],input[type="text"],input[type="checkbox"],input[type="radio"],textarea,select');
    var fieldsDone = [];
    var isFinalStep = $isFinalJson;
    var existingComments = $existingComments;
    
    inputs.each(function(index,el){
        var jel =$(el);
        var elName = jel.prop('name');
        if(elName =='' || elName=='comment' || fieldsDone.includes(elName)){
            return;
        }
    fieldsDone.push(elName);
    var cmnt = '';
    var validationVal='';
    if(typeof existingComments[elName]=='object'){
        var cmnt = $('<div>').text(existingComments[elName].comment).html();//Escaping html content in the comment
        var validationVal = existingComments[elName].validation;
    }
    var fieldTemplate = `<div class="comment-block">
                <input type="hidden" name="ProtocolFieldComment[name][\${i}]" value="\${elName}">
    			<div class="form-group comment-group">
    				<label class="control-label">$commentTranslation</label>
    				<textarea \${(isFinalStep==true)?'disabled':''} rows="1" type="text" class="form-control" name="ProtocolFieldComment[comment][\${i}]">\${cmnt}</textarea>
    			</div>
    			<div class="form-group validation-group">
    				<label class="show control-label">$validationTranslation</label> <input
    					type="hidden" name="ProtocolFieldComment[validation][\${i}]" value="">
    				<div class="btn-group btn-group-toggle" data-toggle="buttons" role="radiogroup">
    					<label class="btn btn-info \${validationVal=='1'?'active':''} \${(isFinalStep==true)?'disabled':''}">
                            <input \${(isFinalStep==true)?'disabled':''} type="radio" name="ProtocolFieldComment[validation][\${i}]" \${validationVal=='1'?'checked':''} value="1">$yesTranslation</label>
                        <label class="btn btn-info \${validationVal=='0'?'active':''} \${(isFinalStep==true)?'disabled':''}">
                            <input \${(isFinalStep==true)?'disabled':''} type="radio" name="ProtocolFieldComment[validation][\${i}]" \${validationVal=='0'?'checked':''} value="0">$noTranslation</label>
    				</div>
    			</div>
    		</div>`;
        var parentDiv = $(jel).parents('.form-group-block:first,.form-group:first').parent();
        var appendAfter = jel;
        if(parentDiv.length){
            appendAfter = $(parentDiv);
            //appendAfter.addClass('parent-field-cmnt-div');
            var newDiv = appendAfter.clone().empty().html(fieldTemplate).insertAfter(appendAfter);
            
            var parentCmntDiv = $('<div class="parent-field-cmnt-div"></div>').insertAfter(newDiv);
            parentCmntDiv.append(appendAfter.detach()).append(newDiv.detach());
            
        }else{
        appendAfter.after(fieldTemplate);} i++;
    });
    EOT;

    $this->registerJs($js,View::POS_END);
}
?>