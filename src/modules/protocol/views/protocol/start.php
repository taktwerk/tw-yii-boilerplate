<?php

use yii\helpers\Html;

use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\protocol\models\Protocol $model
 * @var string $relatedTypeForm
 */

$this->title = Yii::t('twbp', 'Start');
$this->params['breadcrumbs'][] = ['label' => Yii::t('twbp', 'Protocols'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-default">
    <div class="giiant-crud box-body protocol-create">

        <div class="clearfix crud-navigation">
            <div class="pull-left">
                <?= Html::a(
                    Yii::t('twbp', 'Cancel'),
                    \yii\helpers\Url::previous(),
                    [
                        'class' => 'btn btn-default'
                    ]
                ) ?>
            </div>
        </div>


        <div class="protocol-form">
            <?php $form = ActiveForm::begin([
                'fieldClass' => '\taktwerk\yiiboilerplate\widget\ActiveField',
                'id' => 'Protocol',
                'layout' => 'default',
                'enableClientValidation' => true,
                'errorSummaryCssClass' => 'error-summary alert alert-error',
                'options' => [
                    'name' => 'Protocol',
                ],
            ]);
            ?>

            <div class="row">
                <div class="col-md-6">
                        <?= $form->field(
                            $model,
                            'protocol_template_id'
                        )
                            ->widget(
                                Select2::class,
                                [
                                    'data' => taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::find()->count() > 50 ? null : ArrayHelper::map(taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::find()->all(), 'id', 'toString'),
                                    'initValueText' => taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::find()->count() > 50 ? \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::find()->andWhere(['id' => $model->protocol_template_id])->all(), 'id', 'toString') : '',
                                    'options' => [
                                        'placeholder' => Yii::t('twbp', 'Select a value...'),
                                        'id' => 'protocol_template_id' . null,
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false,
                                        (taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                                        (taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::find()->count() > 50 ? 'ajax' : '') => [
                                            'url' => \yii\helpers\Url::to(['list']),
                                            'dataType' => 'json',
                                            'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'ProtocolTemplate\'
                                        };
                                    }')
                                        ],
                                    ],
                                    'pluginEvents' => [
                                        "change" => "function() {
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
                                            Url::toRoute('/protocol/protocol-template/entry-details?id=', true) .
                                            "' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#cprotocol_template_id_well').html(json.data);
                                                //$('#protocol_template_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }",
                                    ],
                                    'addon' => (Yii::$app->getUser()->can('protocol_protocol-template_create') && (!$relatedForm || ($relatedType != RelatedForms::TYPE_MODAL && !$useModal))) ? [
                                        'append' => [
                                            'content' => [
                                                RelatedForms::widget([
                                                    'relatedController' => '/protocol/protocol-template',
                                                    'type' => $relatedTypeForm,
                                                    'selector' => 'protocol_template_id' . null,
                                                    'primaryKey' => 'id',
                                                ]),
                                            ],
                                            'asButton' => true
                                        ],
                                    ] : []
                                ]
                            )
                        ?>
                </div>

                <div class="col-md-6">
                <?= $form->field(
                    $model,
                    'name',
                    [
                        'selectors' => [
                            'input' => '#' .
                                Html::getInputId($model, 'name') . $owner
                        ]
                    ]
                )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('name'),
                            'id' => Html::getInputId($model, 'name') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('name'))
                ?>
                </div>
            </div>

            <hr/>

        </div>

        <div class="clearfix"></div>
        <?php echo $form->errorSummary($model); ?>
        <div class="col-md-12"<?=!$relatedForm ? ' id="main-submit-buttons"' : ''?>>
            <?= Html::submitButton(
                '<span class="glyphicon glyphicon-check"></span> ' .
                ($model->isNewRecord && !$multiple ?
                    Yii::t('twbp', 'Create') :
                    Yii::t('twbp', 'Save')),
                [
                    'id' => 'save-' . $model->formName(),
                    'class' => 'btn btn-success',
                    'name' => 'submit-default'
                ]
            );
            ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
