<?php
//Generation Date: 17-Dec-2020 01:01:35pm
namespace taktwerk\yiiboilerplate\modules\protocol\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "ProtocolFieldCommentController".
 */
class ProtocolFieldCommentController extends \taktwerk\yiiboilerplate\modules\protocol\controllers\base\ProtocolFieldCommentController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\protocol\models\ProtocolFieldComment';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\protocol\models\search\ProtocolFieldComment';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
}
