<?php

namespace taktwerk\yiiboilerplate\modules\protocol\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "ProtocolTemplateController".
 */
class ProtocolTemplateController extends \taktwerk\yiiboilerplate\modules\protocol\controllers\base\ProtocolTemplateController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\protocol\models\search\ProtocolTemplate';

    public function init()
    {
        $this->crudColumnsOverwrite = [
            'crud' => [
                // index
                'thumbnail' => false,
            ]
        ];

        return parent::init();
    }

    public function formFieldsOverwrite($model, $form)
    {
        $this->formFieldsOverwrite = [
            'thumbnail' => false
        ];

        return $this->formFieldsOverwrite;
    }
}
