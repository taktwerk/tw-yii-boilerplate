<?php

namespace taktwerk\yiiboilerplate\modules\protocol\controllers;

use taktwerk\yiiboilerplate\modules\protocol\models\Protocol;
use taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate;
use taktwerk\yiiboilerplate\helpers\CrudHelper;
use yii\filters\AccessControl;
use taktwerk\yiiboilerplate\grid\GridView;
use yii\helpers\ArrayHelper;
use Yii;
use yii\helpers\Html;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use yii\helpers\StringHelper;
use taktwerk\yiiboilerplate\models\User;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\db\ActiveRecord;

/**
 * This is the class for controller "ProtocolController".
 */
class ProtocolController extends base\ProtocolController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\protocol\models\Protocol';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\protocol\models\search\Protocol';

    /**
     * Additional actions for controllers, uncomment to use them
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'start', 'fill', 'quickstart'
                        ],
                        'roles' => ['@']
                    ]
                ]
            ]
        ]);
    }

    /**
     * Init override
     */
    public function init()
    {
        if (php_sapi_name() != "cli") {
            $this->gridButtonActions = [
                'button' => [
                    'label' => '<i class="glyphicon glyphicon-star"></i> ' . Yii::t('twbp', 'Start'),
                    'url' => ['/protocol/protocol/start'],
                    'class' => 'btn btn-success',
                    'visible' => Yii::$app->getUser()->can('protocol_protocol_start')
                ]
            ];
        }

        $this->customActions = [
            'fill' => function ($url, $model, $key) {
                if (! Yii::$app->getUser()->can('protocol_protocol_fill') || ! $model->canFill()) {
                    return false;
                }
                return Html::a('<span class="glyphicon glyphicon-star"></span> ' . Yii::t('twbp', 'Protocol'), $url, [
                    'title' => Yii::t('twbp', 'Protocol'),
                    'url' => $url,
                    'data-pjax' => 0
                ]);
            }
        ];

        $this->crudColumnsOverwrite = [
             'index' => [
                // 'protocol_template_id'=>false,
                'protocol_form_table' => false,
                'protocol_form_number' => false,
                'workflow_step_id' => false,
                'reference_id' => false,
                //'created_by' => true,
                'reference_model' => [
                    // 'attribute'=>'reference_model',
                    'label' => Yii::t('twbp', 'Reference Model'),
                    'format' => 'raw',
                    'content' => function ($model) {
                        return $model->getReferenceModelName();
                    }
                ],
                'after#name' => [
                    'label' => Yii::t('twbp', 'Assignee'),
                    'content' => function ($model) {
                        return $model->assignee();
                    }
                ],

                'before#name' => [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'workflow_step_id',
                    'format' => 'html',
                    'content' => function ($model) {
                        if ($model->workflowStep) {
                            if (Yii::$app->getUser()->can('app_workflow-step_view') && $model->workflowStep->readable()) {
                                return Html::a($model->workflowStep->toString, [
                                    '/workflow/workflow-step/view',
                                    'id' => $model->workflow_step_id
                                ], [
                                    'data-pjax' => 0,
                                    'class' => 'label label-default'
                                ]);
                            } else {
                                return $model->workflowStep->toString;
                            }
                        } else {
                            return null;
                        }
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'data' => \taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()->count() > 50 ? null : \yii\helpers\ArrayHelper::map(\taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()->all(), 'id', 'toString'),
                        'initValueText' => \taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()->count() > 50 ? \yii\helpers\ArrayHelper::map(\taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()->andWhere([
                            'id' => $searchModel->workflow_step_id
                        ])->all(), 'id', 'toString') : '',
                        'options' => [
                            'placeholder' => '',
                            'multiple' => true
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            (\taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                            (\taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::find()->count() > 50 ? 'ajax' : '') => [
                                'url' => \yii\helpers\Url::to([
                                    '/workflow/workflow-step/list'
                                ]),
                                'dataType' => 'json',
                                'data' => new \yii\web\JsExpression('function(params) {
                        return {
                            q:params.term, m: \'WorkflowStep\'
                        };
                    }')
                            ]
                        ]
                    ]
                ]
            ],
            'crud' => [
                'after#reference_model' => [
                    'attribute' => 'created_by',
                    'format' => 'html',
                    'value' => function ($model) {
                    if ($model->created_by && Yii::$app->hasModule('user')) {
                        $user = User::findOne($model->created_by);
                        return $user->toString() ?: $user->id;
                    }
                    return "";
                    }
                    ],
                'after#created_by' => [
                    'attribute' => 'created_at',
                    'class' => '\taktwerk\yiiboilerplate\grid\DateTimeColumn',
                    'content' => function ($model) {
                    return \Yii::$app->formatter->asDatetime($model->created_at);
                    }
                    ]
             ],
        ];

    }
    /**
     * Start a Protocol.
     * @return string
     */
    public function actionStart()
    {
        $model = new Protocol();

        // Create the thing
        if (Yii::$app->request->isPost) {
            $model->load($_POST);

            $data = Yii::$app->request->post('Protocol');

            $template = $model->protocolTemplate;
            $protocol = new Protocol();
            $protocol->name = $data['name'];
            $protocol->protocol_template_id = $template->id;
            $protocol->protocol_form_table = $template->protocol_form_table;
            $protocol->workflow_step_id = $template->workflow->startingStep()->id;
            $protocol->protocol_form_number = 0;
            $protocol->client_id = $template->client_id;
            if ($protocol->save()) {
                return $this->redirect(['index']);
            } else {
                dd($protocol->getErrors());
            }
        }

        return $this->render('start', [
            'model' => $model,
        ]);
    }

    /**
     * @param $templateId
     * @return \yii\web\Response
     */
    public function actionQuickstart($templateId, $referenceModel = null, $referenceId = null)
    {
        if(Protocol::quickstart($templateId, $referenceModel, $referenceId)) {
            return $this->redirect(['/protocol/protocol/fill', 'id' => $protocol->id]);
        }

        // If it didn't work out, go back home.
        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\HttpException
     */
    public function actionFill($id)
    {
        /** @var $model Protocol */
        $model = $this->findModel($id);
        $form = $model->protocolForm();
        if(\Yii::$app->request->referrer){
            if(strpos(\Yii::$app->request->referrer, 'protocol/fill') === false){
                //Saving the referrer url so that user can be redirected back to the page in case user is not allowed for the next workflow step
                \Yii::$app->session->set('protocol_not_allowed_redirect_url',
                    \Yii::$app->request->referrer);
            }
        }
        if (!$model->canFill()){
            if(\Yii::$app->request->referrer && Yii::$app->request->isGet){
                \Yii::$app->session->setFlash('error',\Yii::t('app','You cannot access this protocol'));
                return $this->redirect(\Yii::$app->request->referrer);
            }
            return $this->redirect(['index']);
        }
        if (Yii::$app->request->isPost){
            $dbTransaction = \Yii::$app->db->beginTransaction();
            $form = $model->protocolForm();
            //Detecting the field changes on update and saving them in the comment history
            if($model->logFieldChangesEnabled()){
                $form->on(ActiveRecord::EVENT_AFTER_UPDATE,[$model, 'afterFieldsChanged']);
            }
            $formData = Yii::$app->request->post();
            if($form->hasMethod('beforeSavedByProtocol')){
                $form->beforeSavedByProtocol($model);
            }
            $transition = null;
            // Is a transition taking place?
            if (!isset($_POST['submit-default'])) {
                // Look for the matching step
                foreach ($model->workflowStep->workflowTransitions0 as $trans) {
                    if (isset($_POST[$trans->submitActionName()])) {
                        $transition = $trans;
                        break;
                    }
                }
            }
            if($transition && $transition->skip_step_validation==1){
                //If skip step validation is true then we dont save the formModel attached to the protocol
                $model->addComment(Yii::$app->request->post('comment', null));
                $model->transition($transition);
                $dbTransaction->commit();
                $model->refresh();
                if(!$model->canFill()){
                    //Return to the formModel Grid
                    $redirectUrl = \Yii::$app->session->get('protocol_not_allowed_redirect_url');
                    if($redirectUrl==''){
                        $redirectUrl = Url::home();
                    }
                    return $this->redirect($redirectUrl);
                }
                return $this->redirect(['fill', 'id' => $model->id]);
            }

            if (!$formData || $form->load($formData)==false || ($form->load($formData) && $form->save(false))) {
                
                // Handle uploaded files
                if (!empty($uploadFields = ClassDispenser::getMappedClass(CrudHelper::class)::getUploadFields($model->protocolFormClass()))) {
                    foreach ($uploadFields as $uploadField) {
                        $form->uploadFile(
                            $uploadField,
                            $_FILES[$form->formName()]['tmp_name'],
                            $_FILES[$form->formName()]['name']
                        );
                    }
                }
                if($model->workflowStep->isFinal()==false){
                    $fieldComments = \Yii::$app->request->post('ProtocolFieldComment');
                    if($fieldComments && is_array($fieldComments)&& key_exists('comment',$fieldComments) && key_exists('validation',$fieldComments) && key_exists('name',$fieldComments))
                    {
                        foreach($fieldComments['name'] as $k=>$v){
                            if(key_exists($k,$fieldComments['comment'])){
                                $fieldcmnt = $fieldComments['comment'][$k];
                                $fieldValidation = @$fieldComments['validation'][$k];
                                $fieldName = @$fieldComments['name'][$k];
                                /*
                                   preg_match('#\[(.*?)\]#', $fieldName, $match);
                                   $attributeName =  $match[1]?$match[1]: $fieldName;
                                */
                                $attributeName = $fieldName;
                                $model->addFieldComment($fieldcmnt, $fieldValidation,$attributeName);
                            }
                        }
                    }
                }
                // Is a transition taking place?
                if ($transition) {
                    // Save a potential comment before the transition change
                    $model->addComment(Yii::$app->request->post('comment', null));
                    $model->transition($transition);
                    $dbTransaction->commit();
                    $model->refresh();
                    if(!$model->canFill()){
                        //Return to the formModel Grid
                        $redirectUrl = \Yii::$app->session->get('protocol_not_allowed_redirect_url');
                        if($redirectUrl==''){
                            $redirectUrl = Url::home();
                        }
                        return $this->redirect($redirectUrl);
                    }
                    return $this->redirect(['fill', 'id' => $model->id]);
                }
                // Save a potential comment if no transition change was there.
                $model->addComment(Yii::$app->request->post('comment', null));
                $dbTransaction->commit();
                return $this->redirect(['fill', 'id' => $model->id]);
            }
        }

        return $this->render('fill', [
            'model' => $model,
        ]);
    }

}
