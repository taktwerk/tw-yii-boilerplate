<?php

namespace taktwerk\yiiboilerplate\modules\protocol\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "ProtocolCommentController".
 */
class ProtocolCommentController extends \taktwerk\yiiboilerplate\modules\protocol\controllers\base\ProtocolCommentController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\protocol\models\ProtocolComment';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\protocol\models\search\ProtocolComment';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
}
