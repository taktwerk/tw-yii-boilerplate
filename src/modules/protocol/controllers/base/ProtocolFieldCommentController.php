<?php
//Generation Date: 17-Dec-2020 01:01:35pm
namespace taktwerk\yiiboilerplate\modules\protocol\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * ProtocolFieldCommentController implements the CRUD actions for ProtocolFieldComment model.
 */
class ProtocolFieldCommentController extends TwCrudController
{
}
