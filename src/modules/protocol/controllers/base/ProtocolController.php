<?php
//Generation Date: 11-Sep-2020 02:56:40pm
namespace taktwerk\yiiboilerplate\modules\protocol\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * ProtocolController implements the CRUD actions for Protocol model.
 */
class ProtocolController extends TwCrudController
{
}
