<?php
//Generation Date: 11-Sep-2020 02:56:41pm
namespace taktwerk\yiiboilerplate\modules\protocol\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * ProtocolDefaultController implements the CRUD actions for ProtocolDefault model.
 */
class ProtocolDefaultController extends TwCrudController
{
}
