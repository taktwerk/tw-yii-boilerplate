<?php
//Generation Date: 11-Sep-2020 02:56:43pm
namespace taktwerk\yiiboilerplate\modules\protocol\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * ProtocolTemplateController implements the CRUD actions for ProtocolTemplate model.
 */
class ProtocolTemplateController extends TwCrudController
{
}
