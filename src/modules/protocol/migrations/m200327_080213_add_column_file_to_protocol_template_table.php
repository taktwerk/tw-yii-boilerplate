<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200327_080213_add_column_file_to_protocol_template_table extends TwMigration
{
    public function up()
    {
        $this->addColumn(
            '{{%protocol_template}}',
            'protocol_file',
            'VARCHAR(255) AFTER protocol_form_table'
        );
        $this->addColumn('{{%protocol_template}}', 'thumbnail', 'VARCHAR(255) AFTER protocol_file');
    }

    public function down()
    {
        $this->dropColumn('{{%protocol_template}}', 'protocol_file');
        $this->dropColumn('{{%protocol_template}}', 'thumbnail');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
