<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 */
class m201231_100045_drop_form_studer_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = $this->db->tablePrefix . 'protocol_form_studer';
        if ($this->db->getTableSchema($tableName, true) !== null) {
            $this->dropTable('{{%protocol_form_studer}}');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
