<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191229_074226_add_system_entry_permission_to_protocol extends TwMigration
{
    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->createPermission('protocol_protocol_system-entry', 'Check Protocol System Entry', ['ProtocolAdmin']);
        $this->createPermission('protocol_protocol-comment_system-entry', 'Check Protocol Comment System Entry', ['ProtocolAdmin']);
        $this->createPermission('protocol_protocol-template_system-entry', 'Check Protocol Template System Entry', ['ProtocolAdmin']);
    }

    public function down()
    {
        $this->removePermission('protocol_protocol_system-entry', ['ProtocolAdmin']);
        $this->removePermission('protocol_protocol-comment_system-entry', ['ProtocolAdmin']);
        $this->removePermission('protocol_protocol-template_system-entry', ['ProtocolAdmin']);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
