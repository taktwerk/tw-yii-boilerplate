<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%protocol_field_comment}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%workflow_step}}`
 * - `{{%protocol_id}}`
 */
class m201216_100045_create_protocol_field_comment_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%protocol_field_comment}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Name of the field'),
            'comment' => $this->text(),
            'validation' => $this->boolean()->null()->defaultValue(null),
            'protocol_id' => $this->integer(11),
            'workflow_step_id' => $this->integer(11),
        ]);
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\protocol"}';
        $this->addCommentOnTable('protocol_field_comment', $comment);
        // creates index for column `workflow_step_id`
        $this->createIndex(
            'idx-protocol_field_comment-workflow_step_id',
            '{{%protocol_field_comment}}',
            'workflow_step_id'
        );

        // add foreign key for table `{{%workflow_step}}`
        $this->addForeignKey(
            'fk-protocol_field_comment-workflow_step_id',
            '{{%protocol_field_comment}}',
            'workflow_step_id',
            '{{%workflow_step}}',
            'id',
            'CASCADE'
        );

        // creates index for column `protocol_id`
        $this->createIndex(
            'idx-protocol_field_comment-protocol_id',
            '{{%protocol_field_comment}}',
            'protocol_id'
        );

        // add foreign key for table `{{%protocol_id}}`
        $this->addForeignKey(
            'fk-protocol_field_comment-protocol_id',
            '{{%protocol_field_comment}}',
            'protocol_id',
            '{{%protocol}}',
            'id',
            'CASCADE'
        );
        $this->createIndex('idx_protocol_field_comment_deleted_at_workflow_step_id', '{{%protocol_field_comment}}', ['deleted_at','workflow_step_id']);
        $this->createIndex('idx_protocol_field_comment_deleted_at_protocol_id', '{{%protocol_field_comment}}', ['deleted_at','protocol_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    $this->dropIndex('idx_protocol_field_comment_deleted_at_workflow_step_id', '{{%protocol_field_comment}}');
    $this->dropIndex('idx_protocol_field_comment_deleted_at_protocol_id', '{{%protocol_field_comment}}');
            // drops foreign key for table `{{%workflow_step}}`
        $this->dropForeignKey(
            'fk-protocol_field_comment-workflow_step_id',
            '{{%protocol_field_comment}}'
        );

        // drops index for column `workflow_step_id`
        $this->dropIndex(
            'idx-protocol_field_comment-workflow_step_id',
            '{{%protocol_field_comment}}'
        );

        // drops foreign key for table `{{%protocol_id}}`
        $this->dropForeignKey(
            'fk-protocol_field_comment-protocol_id',
            '{{%protocol_field_comment}}'
        );

        // drops index for column `protocol_id`
        $this->dropIndex(
            'idx-protocol_field_comment-protocol_id',
            '{{%protocol_field_comment}}'
        );

        $this->dropTable('{{%protocol_field_comment}}');
    }
}
