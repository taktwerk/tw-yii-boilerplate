<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200731_163118_add_to_protocol_template_table_pdf_image_column extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%protocol_default}}', 'pdf_image', $this->string(255));
    }

    public function down()
    {
        echo "m200731_163118_add_to_protocol_template_table_pdf_image_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
