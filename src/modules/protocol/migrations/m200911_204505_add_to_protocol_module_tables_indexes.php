<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200911_204505_add_to_protocol_module_tables_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_protocol_comment_deleted_at_new_workflow_step_id', '{{%protocol_comment}}', ['deleted_at','new_workflow_step_id']);
        $this->createIndex('idx_protocol_comment_deleted_at_old_workflow_step_id', '{{%protocol_comment}}', ['deleted_at','old_workflow_step_id']);
        $this->createIndex('idx_protocol_comment_deleted_at_protocol_id', '{{%protocol_comment}}', ['deleted_at','protocol_id']);

        $this->createIndex('idx_protocol_default_deleted_at_protocol_id', '{{%protocol_default}}', ['deleted_at','protocol_id']);
    }

    public function down()
    {
        echo "m200911_204505_add_to_protocol_module_tables_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
