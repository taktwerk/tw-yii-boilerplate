<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200429_052017_add_customer_role_to_protocol_admin extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $protocol_admin = $auth->getRole('ProtocolAdmin');
        $customer = $auth->getRole('Customer');
        if(!$auth->hasChild($protocol_admin, $customer)){
            $auth->addChild($protocol_admin, $customer);
        }

    }

    public function down()
    {
        echo "m200429_052017_add_customer_role_to_protocol_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
