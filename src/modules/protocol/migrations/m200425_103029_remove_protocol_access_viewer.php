<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200425_103029_remove_protocol_access_viewer extends TwMigration
{
    public function up()
    {
        $this->removePermission('x_protocol_protocol-template_see','ProtocolViewer');
    }

    public function down()
    {
        echo "m200425_103029_remove_protocol_access_viewer cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
