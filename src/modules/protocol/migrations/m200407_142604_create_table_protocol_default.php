<?php

use taktwerk\yiiboilerplate\TwMigration;
use taktwerk\yiiboilerplate\traits\MobileMigrationTrait;

class m200407_142604_create_table_protocol_default extends TwMigration
{
    use MobileMigrationTrait;

    public function up()
    {
        $this->createTable('{{%protocol_default}}', [
            'id' => $this->primaryKey(),
            'protocol_id' => $this->integer()->notNull(),
            'protocol_file' => $this->string(255),
        ]);
        $this->addForeignKey(
            'protocol_default_fk_protocol_id',
            '{{%protocol_default}}',
            'protocol_id',
            '{{%protocol}}',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable('{{%protocol_default}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
