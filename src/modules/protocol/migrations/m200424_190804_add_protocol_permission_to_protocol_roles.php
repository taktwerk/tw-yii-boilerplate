<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200424_190804_add_protocol_permission_to_protocol_roles extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $this->createRole('ProtocolViewer');
        $this->createRole('ProtocolAdmin');
        $backend = $auth->getRole('Backend');
        $protocol_viewer = $auth->getRole('ProtocolViewer');
        $protocol_admin = $auth->getRole('ProtocolAdmin');

        $auth->addChild($protocol_viewer, $backend);
        $auth->addChild($protocol_admin, $protocol_viewer);

        $this->createCrudControllerPermissions('protocol_protocol');
        $this->addAdminControllerPermission('protocol_protocol','ProtocolAdmin');
        $this->addSeeControllerPermission('protocol_protocol','ProtocolViewer');

        $this->createCrudControllerPermissions('protocol_protocol-template');
        $this->addAdminControllerPermission('protocol_protocol-template','ProtocolAdmin');
        $this->addSeeControllerPermission('protocol_protocol-template','ProtocolViewer');


        $this->createPermission('protocol_protocol_quickstart', 'Protocol QuickStart', ['ProtocolViewer']);
        $this->createPermission('protocol_protocol_fill', 'Protocol Fill', ['ProtocolViewer']);
        $this->createPermission('protocol_protocol_start', 'Protocol Start', ['ProtocolAdmin']);
    }

    public function down()
    {
        echo "m200424_190804_add_ProtocolAdmin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
