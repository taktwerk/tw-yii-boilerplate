<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191224_133159_add_comment_to_protocol_tables extends TwMigration
{

    public function up()
    {
        $query = <<<EOF
            
            ALTER TABLE `protocol`
                COMMENT='{"base_namespace":"app\\\\modules\\\\protocol"}';
            ALTER TABLE `protocol_comment`
                COMMENT='{"base_namespace":"app\\\\modules\\\\protocol"}';
            ALTER TABLE `protocol_template`
                COMMENT='{"base_namespace":"app\\\\modules\\\\protocol"}';
           
EOF;
        $this->execute($query);
    }

    public function down()
    {
        echo "m191224_133159_add_comment_to_protocol_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
