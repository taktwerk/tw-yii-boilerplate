<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200928_054806_protocol_file_meta_columns extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%protocol_template}}', 'protocol_file_filemeta', $this->text()->after('protocol_file'));
        
        $this->addColumn('{{%protocol_default}}', 'protocol_file_filemeta', $this->text()->after('protocol_file'));
    }

    public function down()
    {
        $this->dropColumn('{{%protocol_template}}', 'protocol_file_filemeta');
        
        $this->dropColumn('{{%protocol_default}}', 'protocol_file_filemeta');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
