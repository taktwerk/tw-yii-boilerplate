<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200813_101631_protocol_template_index extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_protocol_template_deleted_at_client_id', '{{%protocol_template}}', ['deleted_at','client_id']);
        $this->createIndex('idx_protocol_template_deleted_at_workflow_id', '{{%protocol_template}}', ['deleted_at','workflow_id']);
    }

    public function down()
    {
        echo "m200813_101631_protocol_template_index cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
