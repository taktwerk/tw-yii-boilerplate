<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200814_123926_protocol_default_pdf_image_comment extends TwMigration
{
    public function up()
    {
        $comment = <<<EOT
{"inputtype":"file","allowedExtensions":["image"]}
EOT;
        $this->addCommentOnColumn('{{%protocol_default}}', 'pdf_image', $comment);
    }

    public function down()
    {
        echo "m200814_123926_protocol_default_pdf_image_comment cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
