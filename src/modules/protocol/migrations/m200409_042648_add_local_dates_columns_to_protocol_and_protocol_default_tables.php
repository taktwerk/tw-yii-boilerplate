<?php

use taktwerk\yiiboilerplate\TwMigration;
use taktwerk\yiiboilerplate\traits\MobileMigrationTrait;

class m200409_042648_add_local_dates_columns_to_protocol_and_protocol_default_tables extends TwMigration
{
    use MobileMigrationTrait;

    public function up()
    {
        $this->addOfflineChangeDateColumns('protocol');
        $this->addOfflineChangeDateColumns('protocol_default');
    }

    public function down()
    {
        $this->dropOfflineChangeDateColumns('protocol');
        $this->dropOfflineChangeDateColumns('protocol_default');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
