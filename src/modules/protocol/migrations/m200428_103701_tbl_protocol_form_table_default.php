<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200428_103701_tbl_protocol_form_table_default extends TwMigration
{
    public function up()
    {
        $this->alterColumn('{{%protocol_template}}', 'protocol_form_table', 
            $this->string(255)->defaultValue('protocol_default'));
    }

    public function down()
    {
        echo "m200428_103701_tbl_protocol_form_table_default cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
