<?php

use taktwerk\yiiboilerplate\TwMigration;
use taktwerk\yiiboilerplate\traits\MobileMigrationTrait;

class m200327_133358_add_loclal_timestamp_columns_for_protocol extends TwMigration
{
    use MobileMigrationTrait;

    public function up()
    {
        $this->addOfflineChangeDateColumns('protocol_template');
    }

    public function down()
    {
        $this->dropOfflineChangeDateColumns('protocol_template');
    }
}
