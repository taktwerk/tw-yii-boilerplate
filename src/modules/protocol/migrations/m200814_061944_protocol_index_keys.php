<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200814_061944_protocol_index_keys extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_protocol_deleted_at_client_id', '{{%protocol}}', ['deleted_at','client_id']);
        $this->createIndex('idx_protocol_deleted_at_protocol_template_id', '{{%protocol}}', ['deleted_at','protocol_template_id']);
        $this->createIndex('idx_protocol_deleted_at_workflow_step_id', '{{%protocol}}', ['deleted_at','workflow_step_id']);
        /* $this->addForeignKey('fk_protocol_reference_id', '{{%protocol}}', 'reference_id', '{{%reference}}', 'id');
        $this->createIndex('idx_protocol_deleted_at_reference_id', '{{%protocol}}', ['deleted_at','reference_id']); */
    }

    public function down()
    {
        echo "m200814_061944_protocol_index_keys cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
