<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%protocol}}`.
 */
class m200409_235853_add_reference_columns_to_protocol_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            '{{%protocol}}',
            'reference_model',
            'VARCHAR(255) AFTER protocol_form_number'
        );
        $this->addColumn(
            '{{%protocol}}',
            'reference_id',
            'INT(11) AFTER reference_model'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%protocol}}', 'reference_model');
        $this->dropColumn('{{%protocol}}', 'reference_id');
    }
}
