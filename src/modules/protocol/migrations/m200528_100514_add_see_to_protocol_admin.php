<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200528_100514_add_see_to_protocol_admin extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $this->removeRole('ProtocolAdmin');
        $this->removeRole('ProtocolViewer');
        $this->createRole('ProtocolAdmin');
        $this->createRole('ProtocolViewer');
        $backend = $auth->getRole('Backend');
        $protocol_viewer = $auth->getRole('ProtocolViewer');
        $protocol_admin = $auth->getRole('ProtocolAdmin');

        if($auth->canAddChild($protocol_viewer, $backend) && !$auth->hasChild($protocol_viewer, $backend)){
            $auth->addChild($protocol_viewer, $backend);
        }
        if($auth->canAddChild($protocol_admin, $backend) && !$auth->hasChild($protocol_admin, $backend)){
            $auth->addChild($protocol_admin, $backend);
        }

        $this->createCrudControllerPermissions('protocol_protocol');
        $this->addAdminControllerPermission('protocol_protocol','ProtocolAdmin');
        $this->addSeeControllerPermission('protocol_protocol','ProtocolViewer');

        $this->createCrudControllerPermissions('protocol_protocol-template');
        $this->addAdminControllerPermission('protocol_protocol-template','ProtocolAdmin');
        $this->addSeeControllerPermission('protocol_protocol-template','ProtocolAdmin');


        $this->createPermission('protocol_protocol_quickstart', 'Protocol QuickStart', ['ProtocolViewer']);
        $this->createPermission('protocol_protocol_fill', 'Protocol Fill', ['ProtocolViewer']);
        $this->createPermission('protocol_protocol_start', 'Protocol Start', ['ProtocolAdmin']);

        $protocol_admin = $auth->getRole('ProtocolAdmin');
        $customer = $auth->getRole('Customer');
        if(!$auth->hasChild($protocol_admin, $customer)){
            $auth->addChild($protocol_admin, $customer);
        }

    }

    public function down()
    {
        echo "m200528_100514_add_see_to_protocol_admin cannot be reverted.\n";

//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
