<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m181010_000000_create_protocol extends TwMigration
{
    public function safeUp()
    {
        $this->createTable('{{%protocol_template}}', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer()->notNull(),
            'name' => $this->string(45)->notNull(),
            'workflow_id' => $this->integer()->notNull(),
            'protocol_form_table' => $this->string(191),
        ]);
        $this->addForeignKey('protocol_template_fk_client_id', '{{%protocol_template}}', 'client_id', '{{%client}}', 'id');
        $this->addForeignKey('protocol_template_fk_workflow_id', '{{%protocol_template}}', 'workflow_id', '{{%workflow}}', 'id');

        $this->createTable('{{%protocol}}', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer()->notNull(),
            'protocol_template_id' => $this->integer()->notNull(),
            'workflow_step_id' => $this->integer()->notNull(),
            'name' => $this->string(45)->notNull(),
            'protocol_form_table' => $this->string(191),
            'protocol_form_number' => $this->integer()->unsigned()->notNull(),
        ]);
        $this->addForeignKey('protocol_fk_client_id', '{{%protocol}}', 'client_id', '{{%client}}', 'id');
        $this->addForeignKey('protocol_fk_protocol_template_id', '{{%protocol}}', 'protocol_template_id', '{{%protocol_template}}', 'id');
        $this->addForeignKey('protocol_fk_workflow_step_id', '{{%protocol}}', 'workflow_step_id', '{{%workflow_step}}', 'id');


        $this->createTable('{{%protocol_comment}}', [
            'id' => $this->primaryKey(),
            'protocol_id' => $this->integer()->notNull(),
            'comment' => $this->text(),
            'event' => $this->string(45)->notNull(),
            'name' => $this->string(45)->notNull(),

            'old_workflow_step_id' => $this->integer()->null(),
            'new_workflow_step_id' => $this->integer()->null(),

        ]);
        $this->addForeignKey('protocol_comment_fk_protocol_id', '{{%protocol_comment}}', 'protocol_id', '{{%protocol}}', 'id');
        $this->addForeignKey('protocol_comment_fk_old_workflow_id', '{{%protocol_comment}}', 'old_workflow_step_id', '{{%workflow_step}}', 'id');
        $this->addForeignKey('protocol_comment_fk_new_workflow_id', '{{%protocol_comment}}', 'new_workflow_step_id', '{{%workflow_step}}', 'id');
        $this->createIndex('protocol_comment_event_idx', '{{%protocol_comment}}', ['event']);
    }

    public function safeDown()
    {
        $this->dropTable('{{%protocol_comment}}');
        $this->dropTable('{{%protocol}}');
        $this->dropTable('{{%protocol_template}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
