<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200415_045042_add_namespace_protocol extends TwMigration
{
    public function up()
    {
        $comment= '{"base_namespace":"app\\\\modules\\\\protocol"}';
        $this->addCommentOnTable('protocol', $comment);
        $this->addCommentOnTable('protocol_comment', $comment);
        $this->addCommentOnTable('protocol_template', $comment);
    }

    public function down()
    {
        echo "m200415_045042_add_namespace_protocol cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
