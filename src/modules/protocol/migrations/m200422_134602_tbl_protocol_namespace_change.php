<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200422_134602_tbl_protocol_namespace_change extends TwMigration
{
    public function up()
    {
        $comment= '{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\protocol"}';
        $this->addCommentOnTable('{{%protocol}}', $comment);
        $this->addCommentOnTable('{{%protocol_comment}}', $comment);
        $this->addCommentOnTable('{{%protocol_template}}', $comment);
        $this->addCommentOnTable('{{%protocol_default}}', $comment);
    }

    public function down()
    {
        echo "m200422_134602_tbl_namespace_change cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
