<?php
namespace taktwerk\yiiboilerplate\modules\protocol\components;
use taktwerk\yiiboilerplate\modules\workflow\models\WorkflowTransition;
use taktwerk\yiiboilerplate\modules\protocol\models\Protocol;

interface WorkflowTransitionInterface {
    /**
     *@desc sets the current workflow transition 
    **/
    public function setCurrentWFTransition(WorkflowTransition $transition,Protocol $protocol);
    /**
     *@desc excutes before the Worflow transition
     **/
    public function beforeWFTransition();
    /**
     *@desc excutes after the Worflow transition
     **/
    public function afterWFTransition();
}