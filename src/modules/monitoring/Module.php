<?php

namespace taktwerk\yiiboilerplate\modules\monitoring;

class Module extends \taktwerk\yiiboilerplate\components\TwModule
{
    /**
     * @var string The controller namespace to use
     */
    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\monitoring\controllers';

    /**
     * Init module
     */
    public function init()
    {
        parent::init();

        // When in console mode, switch controller namespace to commands
        if (\Yii::$app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'taktwerk\yiiboilerplate\modules\monitoring\commands';
        }
    }
}
