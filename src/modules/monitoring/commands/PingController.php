<?php

namespace taktwerk\yiiboilerplate\modules\monitoring\commands;

use yii\console\Controller;

class PingController extends Controller
{
    /**
     * @throws \Exception
     */
    public function actionIndex()
    {
        $endpoint = getenv('APP_PUSHBEAT_PING_URL');

        if (!empty($endpoint)) {
            $this->ping($endpoint);
        }
    }

    /**
     * @param $endpoint
     * @throws \Exception
     */
    protected function ping($endpoint)
    {
        $url = 'https://pushbeat.io/ping/' . trim($endpoint, '/');

        $client = new \GuzzleHttp\Client();
        $options = [
            'verify' => false, // we don't usually want to ignore certificates, but this is just a push and a pain on windows
        ];

        $proxy = getenv('APP_CURL_PROXY', null);
        if (!empty($proxy)) {
            $options['proxy'] = $proxy;
        }

        $res = $client->request('GET', $url, $options);

        if ($res->getStatusCode() != 200) {
            throw new \Exception('Invalid Pushbeat endpoint `' . $url . '`. Check your .env\'s `APP_PUSHBEAT_PING_URL` config.');
        }
    }
}
