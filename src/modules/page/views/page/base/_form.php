<?php
//Generation Date: 03-Nov-2020 10:29:11am
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\widget\form\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\components\ClassDispenser;
/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\page\models\Page $model
 * @var taktwerk\yiiboilerplate\widget\form\ActiveForm $form
 * @var boolean $useModal
 * @var boolean $multiple
 * @var array $pk
 * @var array $show
 * @var string $action
 * @var string $owner
 * @var array $languageCodes
 * @var boolean $relatedForm to know if this view is called from ajax related-form action. Since this is passing from
 * select2 (owner) it is always inherit from main form
 * @var string $relatedType type of related form, like above always passing from main form
 * @var string $owner string that representing id of select2 from which related-form action been called. Since we in
 * each new form appending "_related" for next opened form, it is always unique. In main form it is always ID without
 * anything appended
 */

$owner = $relatedForm ? '_' . $owner . '_related' : '';
$relatedTypeForm = Yii::$app->request->get('relatedType')?:$relatedTypeForm;

if (!isset($multiple)) {
$multiple = false;
}

if (!isset($relatedForm)) {
$relatedForm = false;
}

$tableHint = (!empty(taktwerk\yiiboilerplate\modules\page\models\Page::tableHint()) ? '<div class="table-hint">' . taktwerk\yiiboilerplate\modules\page\models\Page::tableHint() . '</div><hr />' : '<br />');

?>
<div class="page-form">
        <?php  $formId = 'Page' . ($ajax || $useModal ? '_ajax_' . $owner : ''); ?>    
    <?php $form = ActiveForm::begin([
        'id' => $formId,
        'layout' => 'default',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'action' => $useModal ? $action : '',
        'options' => [
        'name' => 'Page',
            ],
    ]); ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>
        <?php echo $tableHint; ?>

        <?php if ($multiple) : ?>
            <?=Html::hiddenInput('update-multiple', true)?>
            <?php foreach ($pk as $id) :?>
                <?=Html::hiddenInput('pk[]', $id)?>
            <?php endforeach;?>
        <?php endif;?>

        <?php $fieldColumns = [
                
            'code' => 
            $form->field(
                $model,
                'code',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'code') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('code'),
                            'id' => Html::getInputId($model, 'code') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('code')),
            'language_id' => 
            $form->field(
                $model,
                'language_id'
            )
                    ->widget(
                        Select2::class,
                        [
                            'data' => taktwerk\yiiboilerplate\models\Language::find()->count() > 50 ? null : ArrayHelper::map(taktwerk\yiiboilerplate\models\Language::find()->all(), 'language_id', 'toString'),
                                    'initValueText' => taktwerk\yiiboilerplate\models\Language::find()->count() > 50 ? \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\models\Language::find()->andWhere(['language_id' => ($model->isNewRecord && $model->language_id==null) ?\Yii::$app->language:$model->language_id])->all(), 'language_id', 'toString') : '',
                            'options' => [
                                'placeholder' => Yii::t('twbp', 'Select a value...'),
                                'id' => 'language_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'value' => ($model->isNewRecord && $model->language_id==null)?\Yii::$app->language:$model->language_id
                            ],
                            'pluginOptions' => [
                                'allowClear' => false,
                                (taktwerk\yiiboilerplate\models\Language::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                                (taktwerk\yiiboilerplate\models\Language::find()->count() > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'Language\'
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
                                        Url::toRoute('/translatemanager/language/entry-details?id=', true) .
                                        "' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#language_id_well').html(json.data);
                                                //$('#language_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }",
                            ],
                            'addon' => (Yii::$app->getUser()->can('models_language_create') && (!$relatedForm || ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL && !$useModal))) ? [
                                'append' => [
                                    'content' => [
                                        ClassDispenser::getMappedClass(RelatedForms::class)::widget([
                                            'relatedController' => '/translatemanager/language',
                                            'type' => $relatedTypeForm,
                                            'selector' => 'language_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'primaryKey' => 'language_id',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    ) . '
                    
                
                <div id="language_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' . 
                    (taktwerk\yiiboilerplate\models\Language::findOne(['language_id' => $model->language_id])
                        ->entryDetails == '' ?
                    ' display:none;' :
                    '') . '
                    ">' . 
                    (taktwerk\yiiboilerplate\models\Language::findOne(['language_id' => $model->language_id])->entryDetails != '' ?
                    taktwerk\yiiboilerplate\models\Language::findOne(['language_id' => $model->language_id])->entryDetails :
                    '') . ' 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>',
            'title' => 
            $form->field(
                $model,
                'title',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'title') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('title'),
                            'id' => Html::getInputId($model, 'title') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('title')),
            'slug' => 
            $form->field(
                $model,
                'slug',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'slug') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('slug'),
                            'id' => Html::getInputId($model, 'slug') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('slug')),
            'excerpt' => 
            $form->field(
                $model,
                'excerpt',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'excerpt') . $owner
                    ]
                ]
            )
                    ->textarea(
                        [
                            'rows' => 6,
                            'placeholder' => $model->getAttributePlaceholder('excerpt'),
                            'id' => Html::getInputId($model, 'excerpt') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('excerpt')),
            'body_html' => 
            $form->field(
                $model,
                'body_html',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'body_html') . $owner
                    ]
                ]
            )
                     ->widget(
                         ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\CKEditor5::class),
                         [
                             'options' => [
                                 'id' => Html::getInputId($model, 'body_html') . $owner,
                              ],
                         ]
                     )
                    ->hint($model->getAttributeHint('body_html')),
            'author_id' => 
            $form->field(
                $model,
                'author_id'
            )
                    ->widget(
                        Select2::class,
                        [
                            'data' => taktwerk\yiiboilerplate\models\User::find()->count() > 50 ? null : ArrayHelper::map(taktwerk\yiiboilerplate\models\User::find()->all(), 'id', 'toString'),
                                    'initValueText' => taktwerk\yiiboilerplate\models\User::find()->count() > 50 ? \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\models\User::find()->andWhere(['id' => $model->author_id])->all(), 'id', 'toString') : '',
                            'options' => [
                                'placeholder' => Yii::t('twbp', 'Select a value...'),
                                'id' => 'author_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                (taktwerk\yiiboilerplate\models\User::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                                (taktwerk\yiiboilerplate\models\User::find()->count() > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'User\'
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
                                        Url::toRoute('/usermanager/user/entry-details?id=', true) .
                                        "' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#author_id_well').html(json.data);
                                                //$('#author_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }",
                            ],
                            'addon' => (Yii::$app->getUser()->can('models_user_create') && (!$relatedForm || ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL && !$useModal))) ? [
                                'append' => [
                                    'content' => [
                                        ClassDispenser::getMappedClass(RelatedForms::class)::widget([
                                            'relatedController' => '/usermanager/user',
                                            'type' => $relatedTypeForm,
                                            'selector' => 'author_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'primaryKey' => 'id',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    ) . '
                    
                
                <div id="author_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' . 
                    (taktwerk\yiiboilerplate\models\User::findOne(['id' => $model->author_id])
                        ->entryDetails == '' ?
                    ' display:none;' :
                    '') . '
                    ">' . 
                    (taktwerk\yiiboilerplate\models\User::findOne(['id' => $model->author_id])->entryDetails != '' ?
                    taktwerk\yiiboilerplate\models\User::findOne(['id' => $model->author_id])->entryDetails :
                    '') . ' 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>',
            'meta_description' => 
            $form->field(
                $model,
                'meta_description',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'meta_description') . $owner
                    ]
                ]
            )
                    ->textarea(
                        [
                            'rows' => 6,
                            'placeholder' => $model->getAttributePlaceholder('meta_description'),
                            'id' => Html::getInputId($model, 'meta_description') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('meta_description')),
            'meta_keywords' => 
            $form->field(
                $model,
                'meta_keywords',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'meta_keywords') . $owner
                    ]
                ]
            )
                    ->textarea(
                        [
                            'rows' => 6,
                            'placeholder' => $model->getAttributePlaceholder('meta_keywords'),
                            'id' => Html::getInputId($model, 'meta_keywords') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('meta_keywords')),
            'is_active' => 
            $form->field($model, 'is_active')->checkbox(),]; ?>        <div class='clearfix'></div>
<?php $twoColumnsForm = true; ?>
<?php 
// form fields overwrite
$fieldColumns = Yii::$app->controller->crudColumnsOverwrite($fieldColumns, 'form', $model, $form, $owner);

foreach($fieldColumns as $attribute => $fieldColumn) {
            if (count($model->primaryKey())>1 && in_array($attribute, $model->primaryKey()) && $model->checkIfHidden($attribute)) {
            echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
            echo $fieldColumn;
            echo $twoColumnsForm ? '</div>' : '';
        } elseif ($model->checkIfHidden($attribute)) {
            echo $fieldColumn;
        } elseif (!$multiple || ($multiple && isset($show[$attribute]))) {
            if ((isset($show[$attribute]) && $show[$attribute]) || !isset($show[$attribute])) {
                echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
                echo $fieldColumn;
                echo $twoColumnsForm ? '</div>' : '';
            } else {
                echo $fieldColumn;
            }
        }
                
} ?><div class='clearfix'></div>

                <?php $this->endBlock(); ?>
        
        <?= ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_TAB) ?
            Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                    'label' => Yii::t('twbp', 'Page'),
                    'content' => $this->blocks['main'],
                    'active' => true,
                ],
                ]
            ])
            : $this->blocks['main']
        ?>        
        <div class="col-md-12">
        <hr/>
        
        </div>
        
        <div class="clearfix"></div>
        <?php echo $form->errorSummary($model); ?>
        <?php echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::formButtons($model, $useModal, $relatedForm, $multiple, "twbp", ['id' => $model->id]);?>
        <?php ClassDispenser::getMappedClass(ActiveForm::class)::end(); ?>
        <?= ($relatedForm && $relatedType == ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL) ?
        '<div class="clearfix"></div>' :
        ''
        ?>
        <div class="clearfix"></div>
    </div>
</div>

<?php
if ($useModal) {
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::class)::register($this);
}
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
if(!isset($useDependentFieldsJs) || $useDependentFieldsJs==true){
$this->render('@taktwerk-views/_dependent-fields', ['model' => $model,'formId'=>$formId]);
}
?>