<?php
//Generation Date: 11-Sep-2020 02:55:45pm
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\page\models\search\Page $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="page-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'code') ?>

        <?= $form->field($model, 'language_id') ?>

        <?= $form->field($model, 'title') ?>

        <?= $form->field($model, 'slug') ?>

        <?php // echo $form->field($model, 'excerpt') ?>

        <?php // echo $form->field($model, 'body_html') ?>

        <?php // echo $form->field($model, 'author_id') ?>

        <?php // echo $form->field($model, 'meta_description') ?>

        <?php // echo $form->field($model, 'meta_keywords') ?>

        <?php // echo $form->field($model, 'is_active') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
