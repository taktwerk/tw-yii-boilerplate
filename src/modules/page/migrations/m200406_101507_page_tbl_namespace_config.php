<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200406_101507_page_tbl_namespace_config extends TwMigration
{
    public function up()
    {
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\page"}';
        $this->addCommentOnTable('{{%page}}', $comment);
    }

    public function down()
    {
        echo "m200406_101507_page_tbl_namespace_config cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
