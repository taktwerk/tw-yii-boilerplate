<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200717_082431_page_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_page_deleted_at_author_id', '{{%page}}', ['deleted_at','author_id']);
        $this->createIndex('idx_page_deleted_at_language_id', '{{%page}}', ['deleted_at','language_id']);
        $this->alterColumn('{{%page}}','is_active', $this->tinyInteger(1)->defaultValue(null));
    }

    public function down()
    {
        echo "m200717_082431_page_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
