<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m190730_000000_create_page_acl extends TwMigration
{
    public function safeUp()
    {
        $this->createCrudControllerPermissions('page_page', 'PagePage');
    }

    public function safeDown()
    {
        echo "m190730_000000_create_page_acl cannot be reverted.\n";
    }
}
