<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m201208_120246_page_admin extends TwMigration
{
    public function safeUp()
    {
        $auth = $this->getAuth();
        $this->createRole('PageViewer');
        $this->createRole('PageAdmin');
        $backend = $auth->getRole('Backend');
        $pageViewer = $auth->getRole('PageViewer');
        $pageAdmin = $auth->getRole('PageAdmin');
        $auth->addChild($pageViewer, $backend);
        $auth->addChild($pageAdmin, $pageViewer);
        
        $this->createCrudControllerPermissions('page_page');
        $this->addAdminControllerPermission('page_page','PageAdmin');
        $this->addSeeControllerPermission('page_page','PageViewer');
        
    }

    public function safeDown()
    {
        echo "m201208_120246_page_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
