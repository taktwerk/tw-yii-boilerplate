<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m190404_000000_page extends TwMigration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable(
            '{{%page}}',
            [
                'id' => $this->primaryKey()->unsigned(),
                'code' => $this->string(45)->notNull(),
                'author_id' => $this->integer()->null(),
                'language_id' => $this->string(5)->notNull(),
                'title' => $this->string(255)->notNull(),
                'excerpt' => $this->text(),
                'body_html' => $this->text(),
                'slug' => $this->string(255)->null(),
                'meta_description' => $this->text(),
                'meta_keywords' => $this->text(),
                'is_active' => $this->tinyInteger()->notNull(),
            ],
            $tableOptions
        );
        $this->addForeignKey('page_fk_language_id', '{{%page}}', 'language_id', '{{%language}}', 'language_id');
        $this->addForeignKey('page_fk_author_id', '{{%page}}', 'author_id', '{{%user}}', 'id');
        $this->createIndex('page_active', '{{%page}}', ['code', 'is_active']);
    }

    public function safeDown()
    {
        $this->dropTable('{{%page}}');
    }
}
