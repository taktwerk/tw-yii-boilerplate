<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m190919_000000_alter_page_table extends TwMigration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE `page`
            CHANGE COLUMN `slug` `slug` VARCHAR(255) NULL DEFAULT NULL COLLATE \'utf8_unicode_ci\' AFTER `title`,
            CHANGE COLUMN `author_id` `author_id` INT(11) NULL DEFAULT NULL AFTER `body_html`;
        ');
    }

    public function safeDown()
    {
        echo "m190919_000000_alter_page_table cannot be reverted.\n";
    }
}
