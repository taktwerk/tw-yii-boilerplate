<?php

namespace taktwerk\yiiboilerplate\modules\page\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "PageController".
 */
class PageController extends \taktwerk\yiiboilerplate\modules\page\controllers\base\PageController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\page\models\Page';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\page\models\search\Page';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
    public function init(){
        $this->crudColumnsOverwrite = [
            'index'=>[
                'body_html'=>false
            ]
        ];
        return parent::init();
    }
}
