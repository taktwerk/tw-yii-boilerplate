<?php
//Generation Date: 11-Sep-2020 02:55:45pm
namespace taktwerk\yiiboilerplate\modules\page\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends TwCrudController
{
}
