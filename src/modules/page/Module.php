<?php


namespace taktwerk\yiiboilerplate\modules\page;

class Module extends \taktwerk\yiiboilerplate\components\TwModule
{
    /**
     * @var string
     */
    public $defaultRoute = 'page';

    /**
     * Init module
     */
    public function init()
    {
        parent::init();

        // When in console mode, switch controller namespace to commands
        if (\Yii::$app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'taktwerk\yiiboilerplate\modules\page\commands';
        }
    }
}
