<?php

namespace taktwerk\yiiboilerplate\modules\page\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\page\models\Page as PageModel;

/**
 * Page represents the model behind the search form about `\taktwerk\yiiboilerplate\modules\page\models\Page`.
 */
class Page extends PageModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'code',
                    'author_id',
                    'language_id',
                    'title',
                    'excerpt',
                    'body_html',
                    'slug',
                    'meta_description',
                    'meta_keywords',
                    'is_active',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PageModel::find();

        $this->parseSearchParams(PageModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => $this->parseSortParams(PageModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(PageModel::class),
                'params' => [
                    'page' => $this->parsePageParams(PageModel::class),
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('author_id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('code', $query);
        $this->applyLikeOperator('language_id', $query);
        $this->applyLikeOperator('title', $query);
        $this->applyLikeOperator('excerpt', $query);
        $this->applyLikeOperator('body_html', $query);
        $this->applyLikeOperator('slug', $query);
        $this->applyLikeOperator('meta_description', $query);
        $this->applyLikeOperator('meta_keywords', $query);
        $this->applyLikeOperator('is_active', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(PageModel::class);
    }
}
