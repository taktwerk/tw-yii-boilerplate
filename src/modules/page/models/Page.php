<?php

namespace taktwerk\yiiboilerplate\modules\page\models;

use Yii;
use taktwerk\yiiboilerplate\modules\page\models\base\Page as BasePage;
use taktwerk\yiiboilerplate\models\Language;


/**
 * This is the model class for table "page".
 */
class Page extends BasePage
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }
    /**
     * @param null $q
     * @return array
     */
    public static function languageList($q = null)
    {
        return Language::filter($q);
    }
}
