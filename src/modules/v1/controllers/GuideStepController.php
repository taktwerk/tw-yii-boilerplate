<?php

namespace taktwerk\yiiboilerplate\modules\v1\controllers;

/**
 * This is the class for REST controller "GuideStepController".
 */

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\rest\TwActiveController;
use taktwerk\yiiboilerplate\TwXAuth;
use Yii;

class GuideStepController extends TwActiveController
{
    public $modelClass = 'taktwerk\yiiboilerplate\modules\guide\models\mobile\GuideStep';
    public $updateScenario = 'update';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => TwXAuth::class,
            'except' => ['options'],
        ];
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: authorization');
        header('Access-Control-Allow-Credentials: true');
        $behaviors['corsFilter']  = [
            'class' => \yii\filters\Cors::className(),
        ];
        if (Yii::$app->request->isOptions) {
            unset($behaviors['authenticator']);
        }
        $behaviors['contentNegotiator']['only'][] = 'update';
        return $behaviors;
    }

    /**
     * Override the verbs to add the Batch function.
     * @return array
     */
    protected function verbs()
    {
        return ArrayHelper::merge(parent::verbs(), [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::class,
                'actions' => [
                    '*' => ['OPTIONS'],
                ],
            ],
            'index' => ['OPTIONS'],
            'upload' => ['POST'],
            'update' => ['POST']
        ]);
    }

        /**
     * Override the Actions to add the Search capability
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'index',
            'upload' => [
                'class' => \taktwerk\yiiboilerplate\rest\actions\UploadAction::className(),
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            // Need to support options for Ionic apps
            'options' => ['class' => 'yii\rest\OptionsAction']
        ]);
    }
}