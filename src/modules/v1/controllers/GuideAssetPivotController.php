<?php

namespace taktwerk\yiiboilerplate\modules\v1\controllers;

/**
 * This is the class for REST controller "GuideAssetPivotController".
 */

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\rest\TwActiveController;

class GuideAssetPivotController extends TwActiveController
{
    public $modelClass = 'taktwerk\yiiboilerplate\modules\guide\models\GuideAssetPivot';
}
