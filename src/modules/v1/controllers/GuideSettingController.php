<?php

namespace taktwerk\yiiboilerplate\modules\v1\controllers;

/**
 * This is the class for REST controller "GuideSettingController".
 */

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\rest\TwActiveController;

class GuideSettingController extends TwActiveController
{
    public $modelClass = 'taktwerk\yiiboilerplate\modules\guide\models\GuideSetting';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'allow' => true,
                            'matchCallback' => function ($rule, $action) {
                                return \Yii::$app->user->can(
                                    $this->module->id . '_' . $this->id . '_' . $action->id,
                                    ['route' => true]
                                );
                            },
                        ]
                    ]
                ]
            ]
        );
    }
}
