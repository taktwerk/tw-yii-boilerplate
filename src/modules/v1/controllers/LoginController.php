<?php
/**
 * Created by PhpStorm.
 * User: ben-g
 * Date: 05.02.2016
 * Time: 09:57
 */

namespace taktwerk\yiiboilerplate\modules\v1\controllers;


/**
 * This is the class for REST controller "LoginController".
 */

use taktwerk\yiiboilerplate\rest\Token;
use taktwerk\yiiboilerplate\rest\TwActiveController;
use taktwerk\yiiboilerplate\TwXAuth;
use yii\web\Response;

class LoginController extends TwActiveController
{
    public $modelClass = 'app\models\User';

    public $enableCsrfValidation = false;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => TwXAuth::class,
            'only' => ['check'],
        ];
        return array_merge($behaviors, [
            'corsFilter'  => [
                'class' => \yii\filters\Cors::className(),
            ],
        ]);
    }

    /**
     * Override the Actions
     */
    public function actions()
    {
        return [
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
            'index' => [
                'class' => 'taktwerk\yiiboilerplate\modules\user\actions\rest\ApiLoginAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess']
            ],
            'by-client-identifier' => [
                'class' => 'taktwerk\yiiboilerplate\modules\user\actions\rest\LoginByClientIdentifierAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess']
            ],
            'by-user-identifier' => [
                'class' => 'taktwerk\yiiboilerplate\modules\user\actions\rest\LoginByUserIdentifierAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess']
            ]
        ];
    }
    
    protected function verbs()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::class,
                'actions' => [
                    '*' => ['OPTIONS'],
                ],
            ],
        ];
    }
    public function actionCheck()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $user = \Yii::$app->getUser();
        if (!$user) {
            \Yii::$app->getResponse()->setStatusCode(400);
            return ['error' => 'User is not found'];
        }
        $token = Token::find()->where(['user_id'=>$user->id])->one();
        if ($token !== null && !$token->isExpired) {
            return ['access_token' => $token->code, 'user_id' => $user->id];
        }
        $token = new Token();
        $token->user_id = $user->id;
        $token->type = Token::TYPE_RECOVERY;
        if (!$token->save()) {
            die("ERROR: " . print_r($token->getErrors(),true));
        }
        \Yii::$app->getResponse()->setStatusCode(200);

        return ['access_token' => $token->code, 'user_id' => $user->id];
    }
}
