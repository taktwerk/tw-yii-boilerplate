<?php

namespace taktwerk\yiiboilerplate\modules\v1\controllers;

use taktwerk\yiiboilerplate\modules\guide\models\mobile\GuideViewHistory;
use taktwerk\yiiboilerplate\rest\TwActiveController;
use taktwerk\yiiboilerplate\TwXAuth;
use yii\helpers\ArrayHelper;
use Yii;

class GuideViewHistoryController extends TwActiveController
{
    public $modelClass = GuideViewHistory::class;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => TwXAuth::class,
            'except' => ['options'],
        ];
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: authorization');
        header('Access-Control-Allow-Credentials: true');
        $behaviors['corsFilter']  = [
            'class' => \yii\filters\Cors::className(),
        ];
        if (Yii::$app->request->isOptions) {
            unset($behaviors['authenticator']);
        }
        return $behaviors;
    }

    /**
     * Override the verbs to add the Batch function.
     * @return array
     */
    protected function verbs()
    {
        return ArrayHelper::merge(parent::verbs(), [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::class,
                'actions' => [
                    '*' => ['OPTIONS'],
                ],
            ],
            'index' => ['OPTIONS'],
            'upload' => ['POST'],
            'update' => ['POST']
        ]);
    }

    /**
     * Override the Actions to add the Search capability
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'index',
            'upload' => [
                'class' => \taktwerk\yiiboilerplate\rest\actions\UploadAction::className(),
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            // Need to support options for Ionic apps
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ]
        ]);
    }
}
