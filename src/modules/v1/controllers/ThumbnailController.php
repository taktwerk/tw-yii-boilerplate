<?php

namespace taktwerk\yiiboilerplate\modules\v1\controllers;

/**
 * This is the class for REST controller "GuideAssetPivotController".
 */

use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\rest\TwActiveController;

class ThumbnailController extends \yii\rest\Controller
{
    public function behaviors()
    {
        $beh = parent::behaviors(); // TODO: Change the autogenerated stub
        unset($beh['authenticator']);
        return $beh;
    }


    /**
     * Override the verbs to add the Batch function.
     * @return array
     */
    protected function verbs()
    {
        return ArrayHelper::merge(parent::verbs(), [
            'index' => ['GET', 'POST']
        ]);
    }

    public function actionIndex()
    {
        die('yo');
    }

    public function actionThumb()
    {
        var_dump($_FILES);
        die("?");


        $source = 'tmp_' . uniqid();
        @mkdir(Yii::getAlias('@runtime/convert/'));
        $tmpFile = Yii::getAlias('@runtime/convert/' . $file);
        $convertedFile = Yii::getAlias('@runtime/convert/' . $pngFileName);
        $contents = Yii::$app->fs->read($pdf);

        @mkdir(Yii::getAlias('@runtime/convert/'));
        file_put_contents($tmpFile, $contents);

        $imagick = new \Imagick();
        $imagick->setResolution(300, 300);
        $imagick->readImage($tmpFile. "[0]");
        $imagick->setImageFormat( "png" );
        $imagick->setCompressionQuality(90);
        $imagick->writeImage($convertedFile);

        $stream = fopen($convertedFile, 'r+');
        Yii::$app->fs->writeStream($png, $stream);
    }
}
