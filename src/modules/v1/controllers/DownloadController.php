<?php


namespace taktwerk\yiiboilerplate\modules\v1\controllers;

use taktwerk\yiiboilerplate\controllers\ElFinderController;
use taktwerk\yiiboilerplate\TwXAuth;

class DownloadController extends ElFinderController
{
    public $enableCsrfValidation = false;
    public $urlWithLanguage = false;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => TwXAuth::class,
            'except' => ['options'],
        ];
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: authorization');
        header('Access-Control-Allow-Credentials: true');
        $behaviors['corsFilter']  = [
            'class' => \yii\filters\Cors::className(),
        ];

        return $behaviors;
    }

    /**
     * Override the Actions
     */
    public function actions()
    {
        return [
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::class,
                'actions' => [
                    '*' => ['OPTIONS'],
                ],
            ],
        ];
    }
}
