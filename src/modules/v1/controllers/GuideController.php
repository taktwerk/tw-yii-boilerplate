<?php

namespace taktwerk\yiiboilerplate\modules\v1\controllers;

/**
 * This is the class for REST controller "GuideController".
 */

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\rest\TwActiveController;
use taktwerk\yiiboilerplate\modules\guide\models\Guide;
use yii\web\NotFoundHttpException;
use yii\helpers\Json;

class GuideController extends TwActiveController
{
    public $modelClass = 'taktwerk\yiiboilerplate\modules\guide\models\Guide';
    /**
     *@desc Fetches Guide View History
     */
    public function actionViewHistory($id){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $guide = Guide::findOne($id);
        if($guide==null){
            throw new NotFoundHttpException("Guide not found: $id");
        }
        $viewHistory = $guide->getGuideViewHistoryOfUser();
        $viewHistoryData = [];
        if($viewHistory){
            try{
                $vData = Json::decode($viewHistory->data);
                $viewHistoryData['guide_id'] = $viewHistory->guide_id;
                $viewHistoryData['step'] = $vData['step_order_number'];
                if(isset($vData['child_guide_id'])){
                    $viewHistoryData['parent_guide_id'] = $viewHistory->guide_id;
                    $viewHistoryData['guide_id'] = $vData['child_guide_id'];
                }
            } catch (\Exception $e) {}
        }
        return $viewHistoryData;
    }

    /**
     * Saves Guide View History
     */
    public function actionSaveHistory($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $step = \Yii::$app->request->post('step');
        $parent_guide_id = \Yii::$app->request->post('parent_guide_id');
        $guide = Guide::findOne($id);
        if($guide==null){
            throw new NotFoundHttpException("Guide not found: $id");
        }
        if($parent_guide_id!=null){
            $parentGuide = Guide::findOne($parent_guide_id);
            if($parentGuide==null)
            throw new NotFoundHttpException("Parent Guide not found: $parent_guide_id");
        }
        $guide->saveGuideViewHistory($step,$parent_guide_id);
        return true;
    }
}
