<?php

namespace taktwerk\yiiboilerplate\modules\v1\controllers;

/**
 * This is the class for REST controller "ClientController".
 */

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\rest\TwActiveController;

class ClientController extends TwActiveController
{
    public $modelClass = 'taktwerk\yiiboilerplate\modules\customer\models\Client';
}
