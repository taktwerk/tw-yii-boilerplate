<?php

namespace taktwerk\yiiboilerplate\modules\v1\controllers;

/**
 * This is the class for REST controller "PostController".
 */

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\rest\TwActiveController;

class PostController extends TwActiveController
{
    public $modelClass = 'taktwerk\yiiboilerplate\modules\post\models\Post';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'allow' => true,
                            'matchCallback' => function ($rule, $action) {
                                return \Yii::$app->user->can(
                                    $this->module->id . '_' . $this->id . '_' . $action->id,
                                    ['route' => true]
                                );
                            },
                        ]
                    ]
                ]
            ]
        );
    }
}
