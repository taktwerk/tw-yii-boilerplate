<?php

namespace taktwerk\yiiboilerplate\modules\v1\controllers;

/**
 * This is the class for REST controller "ClientDeviceController".
 */

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\rest\TwActiveController;

class ClientDeviceController extends TwActiveController
{
    public $modelClass = 'taktwerk\yiiboilerplate\modules\customer\models\ClientDevice';
}
