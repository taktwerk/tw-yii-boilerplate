<?php

namespace taktwerk\yiiboilerplate\modules\v1\controllers;

use taktwerk\yiiboilerplate\helpers\CorsFilterHelper;
use taktwerk\yiiboilerplate\modules\sync\controllers\SyncController as BaseSyncController;
use taktwerk\yiiboilerplate\rest\filters\CheckApplicationVersionMethod;

/**
 * Class SyncController
 * @package app\modules\v1\controllers
 */
class SyncController extends BaseSyncController
{
    /**
     * Add our behaviors for the API
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $auth = $behaviors['authenticator'] = ['class' => \taktwerk\yiiboilerplate\rest\auth\TwXAuth::class];
        unset($behaviors['authenticator']);
        $behaviors['corsFilter'] = [
            'class' => CorsFilterHelper::class
        ];
        $behaviors['authenticator'] = $auth;
        $behaviors['authenticator']['except'] = ['options'];
        $behaviors['versionChecker'] = [
            'class' => CheckApplicationVersionMethod::class,
            'only' => ['index', 'save-progress'],
            'except' => ['options'],
        ];

        return $behaviors;
    }
}
