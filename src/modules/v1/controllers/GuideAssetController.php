<?php

namespace taktwerk\yiiboilerplate\modules\v1\controllers;

/**
 * This is the class for REST controller "GuideAssetController".
 */

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\rest\TwActiveController;

class GuideAssetController extends TwActiveController
{
    public $modelClass = 'taktwerk\yiiboilerplate\modules\guide\models\GuideAsset';
}