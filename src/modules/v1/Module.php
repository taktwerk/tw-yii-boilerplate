<?php

namespace taktwerk\yiiboilerplate\modules\v1;


class Module extends \taktwerk\yiiboilerplate\components\TwModule
{
    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\v1\controllers';

    // Token Expire-Time for Confirm in Seconds
    public $confirmWithin = 60*60*24;
    // Token Expire-Time for Recover in Seconds
    public $recoverWithin = 60*60*24;

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}