<?php
namespace taktwerk\yiiboilerplate\modules\gii;

use yii\gii\GiiAsset;

/**
 * Class Module
 * 
 * @package taktwerk\yiiboilerplate\modules\gii
 */
class Module extends \yii\gii\Module
{

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->viewPath = '@vendor/yiisoft/yii2-gii/src/views';
            
            if (\Yii::$app instanceof \yii\web\Application) {
                \Yii::$app->view->registerAssetBundle(GiiAsset::className());
            }
            return true;
        }
        return false;
    }

    /**
     *
     * @return mixed
     */
    public function checkAccess()
    {
        return parent::checkAccess();
    }
}
