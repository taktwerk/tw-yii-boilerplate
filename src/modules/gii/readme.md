# Taktwerk Gii

[![N|Solid](https://www.taktwerk.ch/images/taktwerk_Logo.svg)](https://www.taktwerk.ch/)

This Gii module is an extended version of the Gii-ant module. 


### Multiple file upload field configuration in a table sql

  - The field/column should end with **_media** e.g marksheet_media , and should be **varchar(255)** with **null** allowed.
  - The configuration for generating the file upload field can be set in column comment as json below is an example of the json configuration:
  ``` { "allowedExtensions": ["jpg","pdf","png"], "minFileCount":"2", "maxFileCount":"4", "maxFileSize":"1024" } ```
- Below is an example of the table sql 
```
CREATE TABLE `guide_step` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `marksheet_media` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '{ "allowedExtensions": ["jpg","pdf","png"], "minFileCount":"2", "maxFileCount":"4" }',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
```





### Enabling Ordering/Sorting in a table crud sql

  - There should be column named `order_number` with int(11) in the table.
  - The configuration for generating CRUD with Sortable Grid View in relational tabs instead of the basic CRUD of the table to implement sorting based on a particular column/s value like Guide Steps order should be according to the `guide_id` column in the guide_step table, to support that the Json config for the order_number column would be:
  ```  {"sortRestrictAttributes":["guide_id"] }	 ```
  
  "***sortRestrictAttributes***" key's value as array in json config should have the name of the column/s on which the sorting will be based. 
  
- Below is an example of the table sql 
```
CREATE TABLE `guide_step` (
  `id` int(11) NOT NULL,
  `guide_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `picture_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `certificate_media` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '{ "allowedExtensions": ["jpg","pdf","png"], "minFileCount":"2", "maxFileCount":"4" }',
  `marksheet_media` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `order_number` int(11) NOT NULL COMMENT '{ "sortRestrictAttributes":["guide_id"] }',
  `description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
```


### Configuration to generate a Canvas Field(Drawing/Signature pad) in CRUD of the table

  - The name of the column which you want to be canvas based should end with "**_canvas**" e.g design_canvas, sign_canvas.
  - There are two modes in the canvas drawing field which is display on the CRUD form, one is **basic** mode which work as a signature pad and is limited in features, and the other one is **advance** mode which opens up a full blown drawing editor and with many tools which can change the look of the canvas drawing.
  By default the canvas is set to *basic* mode but by adding the below coniguration in the column comment you can force Gii to generate the field in *advance* mode.
`{"isAdvanceMode":true}`
 To save meta-data of the advance mode canvas(for allowing edit later functionality)  we'll need another field(column) named `column_name_canvas_meta` with datatype `longtext`,  and `keepMetaData` will be set to `true` in canvas column comment configuration like below

`{"isAdvanceMode":true,"keepMetaData":true}`

- Below is an example of the table sql with canvas field in advance mode
```
CREATE TABLE `test_file` (
  `id` int(11) NOT NULL,
  `design_canvas` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '{"isAdvanceMode":true,"keepMetaData":true}',
  `design_canvas_meta` longtext COLLATE utf8_unicode_ci,
  `sign_canvas` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
```