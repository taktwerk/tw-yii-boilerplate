$(function(){
	$('body').append('<div id="report-overlay"><div id="report-text"></div></div>');
	var spinOpts = {
            lines: 13, // The number of lines to draw
            length: 6, // The length of each line
            width: 10, // The line thickness
            radius: 30, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            direction: 1, // 1: clockwise, -1: counterclockwise
            color: '#333333', // #rgb or #rrggbb or array of colors
            speed: 1, // Rounds per second
            trail: 60, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 29, // The z-index (defaults to 2000000000)
            top: '50%', // Top position relative to parent
            left: '50%', // Left position relative to parent
            position:'fixed'
        };
	var spinner = new Spinner(spinOpts);

	var onTerminate = function(status){
		spinner.stop();$('#report-overlay').hide();
	};
	var options = {
	        cookiePrefix: 'report-gen-monitor',
			onRequest: function(cookieName){
				$('#report-overlay').show();
	            $('a.download-rprt-btn').addClass('disabled');
			},
	        onResponse: function(status){
	            $('#report-overlay').hide();
	            $('a.download-rprt-btn').removeClass('disabled');
			},
			onTimeout: function(){
	            $('#report-overlay').hide();
	            $('a.download-rprt-btn').removeClass('disabled');
			},
			onRequest: function(){
				spinner.spin($('#report-overlay')[0]);
				document.body.appendChild(spinner.el);
				$('#report-overlay').show();
			},
			onResponse:onTerminate,
			onTimeout: function(){onTerminate(); alert('Timeout error');},
		};
	$('a.download-rprt-btn').ResponseMonitor(options);	
});
