<?php

namespace taktwerk\yiiboilerplate\modules\report\assets;

/*
 * @link http://www.yiiframework.com/
 *
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

use yii\helpers\FileHelper;
use yii\web\AssetBundle;

/**
 * Configuration for `backend` client script files.
 *
 * @since 4.0
 */
class ReportAsset extends AssetBundle
{
    public $sourcePath = '@taktwerk-boilerplate/modules/report/assets/web';
    //public $basePath ='@vendor/taktwerk/yii-boilerplate/src/widget/canvasinput/web';
    public $js = [
        'js/spin.min.js',
        'js/response-monitor.js',
        'js/response-monitor.jquery.js',
        'js/report-custom.js'
    ];

    public $css = [
        'css/report.css'
    ];

    public $depends = [
        'yii\jui\JuiAsset'
    ];

    public function init()
    {
        parent::init();
        // /!\ CSS/LESS development only setting /!\
        // Touch the asset folder with the highest mtime of all contained files
        // This will create a new folder in web/assets for every change and request
        // made to the app assets.
        if (getenv('APP_ASSET_FORCE_PUBLISH')) {
            $path = \Yii::getAlias($this->sourcePath);
            $files = FileHelper::findFiles($path);
            $mtimes = [];
            foreach ($files as $file) {
                $mtimes[] = filemtime($file);
            }
            touch($path, max($mtimes));
        }
        
    }
}
