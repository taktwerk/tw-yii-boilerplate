<?php
//Generation Date: 04-May-2021 05:44:32am
namespace taktwerk\yiiboilerplate\modules\report\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Cookie;

/**
 * This is the class for controller "ReportController".
 */
class ReportController extends \taktwerk\yiiboilerplate\modules\report\controllers\base\ReportController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\report\models\Report';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\report\models\search\Report';

    /**
     * Additional actions for controllers, uncomment to use them
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'download',
                        ],
                        'roles' => ['@']
                    ]
                ]
            ],
            /* 'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'download'  => ['POST'],
                ],
            ] */
        ]);
    }
    public function init(){
        parent::init();
        if (php_sapi_name() != "cli") {
            $this->crudColumnsOverwrite =[
                'function'=>false
            ];
            $this->crudColumnsOverwrite = [
                'crud' => [
                    'function' => false,
                    'params' => false,
                    'class'=>\Yii::$app->user->can('ReportAdmin'),
                    'before#title' => [
                        'label' => \Yii::t('twbp', 'Download report'),
                        'format' => 'raw',
                        'headerOptions'=>['style'=>'max-width:70px', 'class'=>'text-center'],
                        'contentOptions'=>['class'=>'text-center'],
                        'value' => function ($m) {
                            $outputTypes = $m->getAllowedOutputTypes();
                            $types = [
                               'xls'=>[
                                    'xls',
                                    'fa fa-file-excel-o',
                                    \Yii::t('twbp', 'Excel File'),'success'
                                ],
                               'csv'=> [
                                    'csv',
                                    'fa fa-file-o',
                                    \Yii::t('twbp', 'CSV File'),'primary'
                                ],
                                // UNCOMMENT TO ENABLE PDF,HTML
                                'pdf' => [
                                    'pdf',
                                    'fa fa-file-pdf-o',
                                    \Yii::t('twbp', 'Pdf File'),
                                    'danger'
                                ],
                                'html' =>[
                                        'html',
                                        'fa fa-html5',
                                        \Yii::t('twbp', 'Html File'),
                                        'warning'
                                ]
                            ];
                        $html = [];
                        foreach($outputTypes as $type){
                            if(!isset($types[$type])){
                                continue;
                            }
                            $t = $types[$type];
                            $html[] = Html::a('<i class="fa '.$t[1].'"></i>',
                                ['download','id' => $m->id,'type' => $t[0]],
                                ['data'=>['pjax'=>0,'method'=>'post'],
                                'class' => 'download-rprt-btn btn btn-sm btn-'.$t[3],
                                'title' => $t[2]
                            ]);
                        }
                        return implode(' ', $html);
                        }
                    ],
                    'id'=>false,
                ]
            ];
        }
    }
    public function formFieldsOverwrite($model, $form)
    {
        $this->formFieldsOverwrite = [
            'function'=>false,
        ];
        return $this->formFieldsOverwrite;
    }
    public function actionDownload($id,$type='xls'){
        if(\Yii::$app->request->referrer==null){
            throw new \Exception(\Yii::t('twbp','You cannot access this link directly'));
        }
        $allowedTypes = ['xls','csv','pdf','html'];
        if(!in_array($type,$allowedTypes)){
            throw new \Exception(\Yii::t('twbp','Wrong report format requested'));
        }
        $model = $this->findModel($id);
        if(!in_array($type,$model->getAllowedOutputTypes())){
            throw new \Exception(\Yii::t('twbp','Wrong report format requested'));
        }
        $reportInfo = $model->generate($type,$this->module->saveReportHistory);
        $cookiePrefix = 'report-gen-monitor';
        $tokenValue = \Yii::$app->request->get($cookiePrefix);
        if($tokenValue){
            $cookieName = $cookiePrefix.'_'.$tokenValue;
            $cookieValue = 1;
            $cookie = new Cookie();
            $cookie->name = $cookieName;
            $cookie->value = $cookieValue;
            $cookie->httpOnly = false; //To Allow browser to read the cookie
            \Yii::$app->response->cookies->add($cookie);
        }
        if($reportInfo){
            return $this->response->sendFile($reportInfo['report_path'], $reportInfo['report_name'])->send();
        }
        return $this->redirect(\Yii::$app->request->referrer);
    }
}
