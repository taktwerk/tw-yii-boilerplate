<?php
//Generation Date: 21-Jun-2021 11:08:17am
namespace taktwerk\yiiboilerplate\modules\report\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * ReportHistoryController implements the CRUD actions for ReportHistory model.
 */
class ReportHistoryController extends TwCrudController
{
}
