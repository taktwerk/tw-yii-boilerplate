<?php
//Generation Date: 04-May-2021 05:44:32am
namespace taktwerk\yiiboilerplate\modules\report\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * ReportController implements the CRUD actions for Report model.
 */
class ReportController extends TwCrudController
{
}
