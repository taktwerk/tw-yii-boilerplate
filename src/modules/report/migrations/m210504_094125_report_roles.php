<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m210504_094125_report_roles extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $this->createRole('ReportViewer');
        $this->createRole('ReportAdmin');
        $reportViewer = $auth->getRole('ReportViewer');
        $reportAdmin = $auth->getRole('ReportAdmin');
        $auth->addChild($reportAdmin, $reportViewer);
        $this->createCrudControllerPermissions('report_report');
        $this->addSeeControllerPermission('report_report','ReportViewer');
        $this->createPermission('report_report_download', 'Report Download', ['ReportViewer']);
        $this->addAdminControllerPermission('report_report','ReportAdmin');
    }

    public function down()
    {
        echo "m210504_094125_report_roles cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
