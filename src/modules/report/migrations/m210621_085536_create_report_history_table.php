<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%report_history}}`.
 */
class m210621_085536_create_report_history_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%report_history}}', [
            'id' => $this->primaryKey(),
            'report_file' => $this->string(255),
            'report_file_filemeta' => $this->text(),
            'report_data_file' => $this->string(255),
            'report_data_file_filemeta' => $this->text()
        ]);
        
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\report"}';
        $this->addCommentOnTable('{{%report_history}}', $comment);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            $this->dropTable('{{%report_history}}');
    }
}
