<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%report_history}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%report}}`
 */
class m210622_043010_add_report_column_to_report_history_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%report_history}}', 'report_id', $this->integer()->null()->after('id'));

        // creates index for column `report_id`
        $this->createIndex(
            'idx-report_history-report_id',
            '{{%report_history}}',
            'report_id'
        );

        // add foreign key for table `{{%report}}`
        $this->addForeignKey(
            'fk-report_history-report_id',
            '{{%report_history}}',
            'report_id',
            '{{%report}}',
            'id',
            'CASCADE'
        );
        $this->createIndex('idx_report_history_deleted_at_report_id', '{{%report_history}}', ['deleted_at','report_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx_report_history_deleted_at_report_id',
            '{{%report_history}}'
            );
            // drops foreign key for table `{{%report}}`
        $this->dropForeignKey(
            'fk-report_history-report_id',
            '{{%report_history}}'
        );

        // drops index for column `report_id`
        $this->dropIndex(
            'idx-report_history-report_id',
            '{{%report_history}}'
        );

        $this->dropColumn('{{%report_history}}', 'report_id');
    }
}
