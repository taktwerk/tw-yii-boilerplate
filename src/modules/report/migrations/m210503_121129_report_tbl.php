<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m210503_121129_report_tbl extends TwMigration
{
    public function up()
    {
        $this->createTable('{{%report}}', [
            'id'=>$this->primaryKey(),
            'title'=>$this->string(255)->comment('name of the report'),
            'class'=> $this->string(255)->comment('class having the report function'), 
            'function'=> $this->string(255)->comment('Function which creates the report'),
            'params' => $this->string(255)->comment('Function which creates the report'),
        ]);
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\report"}';
        $this->addCommentOnTable('{{%report}}', $comment);
    }

    public function down()
    {
        echo "m210503_121129_report_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
