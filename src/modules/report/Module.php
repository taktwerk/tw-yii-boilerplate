<?php


namespace taktwerk\yiiboilerplate\modules\report;


use taktwerk\yiiboilerplate\modules\report\assets\ReportAsset;

class Module extends \taktwerk\yiiboilerplate\components\TwModule
{
    /**
     * @var string
     */
    public $defaultRoute = 'report/index';

    /**
     * @var array Models that implement the searchable interface
     */
    public $models = [];
    /**
     * @var boolean To save generated report in ReportHistory model
     */
    public $saveReportHistory = true;

    /**
     * Init module
     */
    public function init()
    {
        parent::init();

        // When in console mode, switch controller namespace to commands
        if (\Yii::$app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'taktwerk\yiiboilerplate\modules\report\commands';
        }else{
            ReportAsset::register(\Yii::$app->view);
        }
        
    }
}
