<?php
//Generation Date: 22-Jun-2021 06:37:00am
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\report\models\search\ReportHistory $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="report-history-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'report_id') ?>

        <?= $form->field($model, 'report_file') ?>

        <?= $form->field($model, 'report_file_filemeta') ?>

        <?= $form->field($model, 'report_data_file') ?>

        <?php // echo $form->field($model, 'report_data_file_filemeta') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
