<?php
//Generation Date: 22-Jun-2021 06:37:00am
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\widget\form\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\components\ClassDispenser;
/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\report\models\ReportHistory $model
 * @var taktwerk\yiiboilerplate\widget\form\ActiveForm $form
 * @var boolean $useModal
 * @var boolean $multiple
 * @var array $pk
 * @var array $show
 * @var string $action
 * @var string $owner
 * @var array $languageCodes
 * @var boolean $relatedForm to know if this view is called from ajax related-form action. Since this is passing from
 * select2 (owner) it is always inherit from main form
 * @var string $relatedType type of related form, like above always passing from main form
 * @var string $owner string that representing id of select2 from which related-form action been called. Since we in
 * each new form appending "_related" for next opened form, it is always unique. In main form it is always ID without
 * anything appended
 */

$owner = $relatedForm ? '_' . $owner . '_related' : '';
$relatedTypeForm = Yii::$app->request->get('relatedType')?:$relatedTypeForm;

if (!isset($multiple)) {
$multiple = false;
}

if (!isset($relatedForm)) {
$relatedForm = false;
}

$tableHint = (!empty(taktwerk\yiiboilerplate\modules\report\models\ReportHistory::tableHint()) ? '<div class="table-hint">' . taktwerk\yiiboilerplate\modules\report\models\ReportHistory::tableHint() . '</div><hr />' : '<br />');

?>
<div class="report-history-form">
        <?php  $formId = 'ReportHistory' . ($ajax || $useModal ? '_ajax_' . $owner : ''); ?>    
    <?php $form = ActiveForm::begin([
        'id' => $formId,
        'layout' => 'default',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'action' => $useModal ? $action : '',
        'options' => [
        'name' => 'ReportHistory',
    'enctype' => 'multipart/form-data'
        
        ],
    ]); ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>
        <?php echo $tableHint; ?>

        <?php if ($multiple) : ?>
            <?=Html::hiddenInput('update-multiple', true)?>
            <?php foreach ($pk as $id) :?>
                <?=Html::hiddenInput('pk[]', $id)?>
            <?php endforeach;?>
        <?php endif;?>

        <?php $fieldColumns = [
                
            'report_id' => 
            $form->field(
                $model,
                'report_id'
            )
                    ->widget(
                        Select2::class,
                        [
                            'data' => taktwerk\yiiboilerplate\modules\report\models\Report::find()->count() > 50 ? null : ArrayHelper::map(taktwerk\yiiboilerplate\modules\report\models\Report::find()->groupBy('report.id')->all(), 'id', 'toString'),
                                    'initValueText' => taktwerk\yiiboilerplate\modules\report\models\Report::find()->groupBy('report.id')->count() > 50 ? \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\modules\report\models\Report::find()->andWhere(['report.id' => $model->report_id])->groupBy('report.id')->all(), 'id', 'toString') : '',
                            'options' => [
                                'placeholder' => Yii::t('twbp', 'Select a value...'),
                                'id' => 'report_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                (taktwerk\yiiboilerplate\modules\report\models\Report::find()->groupBy('report.id')->count() > 50 ? 'minimumInputLength' : '') => 3,
                                (taktwerk\yiiboilerplate\modules\report\models\Report::find()->groupBy('report.id')->count() > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'Report\', page:params.page || 1
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
                                        Url::toRoute('/report/report/entry-details?id=', true) .
                                        "' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#report_id_well').html(json.data);
                                                //$('#report_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }",
                            ],
                            'addon' => (Yii::$app->getUser()->can('modules_report_create') && (!$relatedForm || ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL && !$useModal))) ? [
                                'append' => [
                                    'content' => [
                                        ClassDispenser::getMappedClass(RelatedForms::class)::widget([
                                            'relatedController' => '/report/report',
                                            'type' => $relatedTypeForm,
                                            'selector' => 'report_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'primaryKey' => 'id',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    ) . '
                    
                
                <div id="report_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' . 
                    (taktwerk\yiiboilerplate\modules\report\models\Report::find()->where(['report.id' => $model->report_id])->one()
                        ->entryDetails == '' ?
                    ' display:none;' :
                    '') . '
                    ">' . 
                    (taktwerk\yiiboilerplate\modules\report\models\Report::find()->where(['report.id' => $model->report_id])->one()->entryDetails != '' ?
                    taktwerk\yiiboilerplate\modules\report\models\Report::find()->where(['report.id' => $model->report_id])->one()->entryDetails :
                    '') . ' 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>',
            'report_file' => 
            $form->field(
                $model,
                'report_file',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'report_file') . $owner
                    ]
                ]
            )->widget(
                ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\FileInput::class),
                [
                    'pluginOptions' => [
                        'showUpload' => false,
                        'showRemove' => false,
                        
                        
                                   'initialPreview' => (!empty($model->report_file) ? [
                            !empty($model->getMinFilePath('report_file')  && $model->getFileType('report_file') == 'video') ? $model->getMinFilePath('report_file') : $model->getFileUrl('report_file',[],true)
                        ] : ''),
                        'initialCaption' => $model->report_file,
                        'initialPreviewAsData' => true,
                        'initialPreviewFileType' => $model->getFileType('report_file'),
                        'fileActionSettings' => [
                            'indicatorNew' => $model->report_file === null ? '' : '<a href=" ' . (!empty($model->getMinFilePath('report_file') && $model->getFileType('report_file') == 'video') ? $model->getMinFilePath('report_file') : $model->getFileUrl('report_file')) . '" target="_blank"><i class="glyphicon glyphicon-hand-down text-warning"></i></a>',
                            'indicatorNewTitle' => \Yii::t('twbp','Download'),
                            'showDrag'=>false,
                        ],
                        'overwriteInitial' => true,
                         'initialPreviewConfig'=> [
                                                     [
                                                         'url' => Url::toRoute(['delete-file','id'=>$model->getPrimaryKey(), 'attribute' => 'report_file']),
                                                         'downloadUrl'=> !empty($model->getMinFilePath('report_file')  && $model->getFileType('report_file') == 'video') ? $model->getMinFilePath('report_file') : $model->getFileUrl('report_file'),
                                                         'filetype' => !empty($model->getMinFilePath('report_file') && $model->getFileType('report_file') == 'video') ? 'video/mp4':'',
                                                     ],
                                                 ],
                    ],
                     'pluginEvents' => [
                         'fileclear' => 'function() { var prev = $("input[name=\'" + $(this).attr("name") + "\']")[0]; $(prev).val(-1); }',
                        
                        'filepredelete'=>'function(xhr){ 
                               var abort = true; 
                               if (confirm("Are you sure you want to remove this file? This action will delete the file even without pressing the save button")) { 
                                   abort = false; 
                               } 
                               return abort; 
                        }', 
                     ],
                    'options' => [
                        'id' => Html::getInputId($model, 'report_file') . $owner
                    ]
                ]
            )
            ->hint($model->getAttributeHint('report_file')),
            'report_data_file' => 
            $form->field(
                $model,
                'report_data_file',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'report_data_file') . $owner
                    ]
                ]
            )->widget(
                ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\FileInput::class),
                [
                    'pluginOptions' => [
                        'showUpload' => false,
                        'showRemove' => false,
                        
                        
                                   'initialPreview' => (!empty($model->report_data_file) ? [
                            !empty($model->getMinFilePath('report_data_file')  && $model->getFileType('report_data_file') == 'video') ? $model->getMinFilePath('report_data_file') : $model->getFileUrl('report_data_file',[],true)
                        ] : ''),
                        'initialCaption' => $model->report_data_file,
                        'initialPreviewAsData' => true,
                        'initialPreviewFileType' => $model->getFileType('report_data_file'),
                        'fileActionSettings' => [
                            'indicatorNew' => $model->report_data_file === null ? '' : '<a href=" ' . (!empty($model->getMinFilePath('report_data_file') && $model->getFileType('report_data_file') == 'video') ? $model->getMinFilePath('report_data_file') : $model->getFileUrl('report_data_file')) . '" target="_blank"><i class="glyphicon glyphicon-hand-down text-warning"></i></a>',
                            'indicatorNewTitle' => \Yii::t('twbp','Download'),
                            'showDrag'=>false,
                        ],
                        'overwriteInitial' => true,
                         'initialPreviewConfig'=> [
                                                     [
                                                         'url' => Url::toRoute(['delete-file','id'=>$model->getPrimaryKey(), 'attribute' => 'report_data_file']),
                                                         'downloadUrl'=> !empty($model->getMinFilePath('report_data_file')  && $model->getFileType('report_data_file') == 'video') ? $model->getMinFilePath('report_data_file') : $model->getFileUrl('report_data_file'),
                                                         'filetype' => !empty($model->getMinFilePath('report_data_file') && $model->getFileType('report_data_file') == 'video') ? 'video/mp4':'',
                                                     ],
                                                 ],
                    ],
                     'pluginEvents' => [
                         'fileclear' => 'function() { var prev = $("input[name=\'" + $(this).attr("name") + "\']")[0]; $(prev).val(-1); }',
                        
                        'filepredelete'=>'function(xhr){ 
                               var abort = true; 
                               if (confirm("Are you sure you want to remove this file? This action will delete the file even without pressing the save button")) { 
                                   abort = false; 
                               } 
                               return abort; 
                        }', 
                     ],
                    'options' => [
                        'id' => Html::getInputId($model, 'report_data_file') . $owner
                    ]
                ]
            )
            ->hint($model->getAttributeHint('report_data_file')),]; ?>        <div class='clearfix'></div>
<?php $twoColumnsForm = true; ?>
<?php 
// form fields overwrite
$fieldColumns = Yii::$app->controller->crudColumnsOverwrite($fieldColumns, 'form', $model, $form, $owner);

foreach($fieldColumns as $attribute => $fieldColumn) {
             if($model->checkIfHidden($attribute))
            {
                echo $fieldColumn;
            }
            else if(!$model->isNewRecord || !$model->checkIfCanvas($attribute))
            {
                if (!$multiple || ($multiple && isset($show[$attribute]))) {
                    if ((isset($show[$attribute]) && $show[$attribute]) || !isset($show[$attribute])) {
                        echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
                        echo $fieldColumn;
                        echo $twoColumnsForm ? '</div>' : '';
                    }else{
                        echo $fieldColumn;
                    }
                }
            }
                
} ?><div class='clearfix'></div>

                <?php $this->endBlock(); ?>
        
        <?= ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_TAB) ?
            Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                    'label' => Yii::t('twbp', 'Report History'),
                    'content' => $this->blocks['main'],
                    'active' => true,
                ],
                ]
            ])
            : $this->blocks['main']
        ?>        
        <div class="col-md-12">
        <hr/>
        
        </div>
        
        <div class="clearfix"></div>
        <?php echo $form->errorSummary($model); ?>
        <?php echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::formButtons($model, $useModal, $relatedForm, $multiple, "twbp", ['id' => $model->id]);?>
        <?php ClassDispenser::getMappedClass(ActiveForm::class)::end(); ?>
        <?= ($relatedForm && $relatedType == ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL) ?
        '<div class="clearfix"></div>' :
        ''
        ?>
        <div class="clearfix"></div>
    </div>
</div>

<?php
if(count($hiddenFieldsName)>0){
    $fName = $model->formName();
    $hideJs='';
    foreach($hiddenFieldsName as $name => $op){
        $pClass = $op['parent_class']?$op['parent_class']:'form-group-block';
        $hideJs .= <<<EOT
$('[name="{$fName}[{$name}]"]').parents('.{$pClass}:first').hide();

EOT;
    }
    if($hideJs){
        $this->registerJs($hideJs,\yii\web\View::POS_END);
    }
}
if ($useModal) {
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::class)::register($this);
}
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
if(!isset($useDependentFieldsJs) || $useDependentFieldsJs==true){
$this->render('@taktwerk-views/_dependent-fields', ['model' => $model,'formId'=>$formId]);
}
?>