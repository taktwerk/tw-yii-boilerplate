<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\widget\form\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;

use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\modules\setting\models\GeneralSetting;

/**
 * @var yii\web\View $this
 * @var app\models\GeneralSetting $model
 * @var string $relatedTypeForm
 */

$this->title = Yii::t('twbp', 'Reports');
$this->params['breadcrumbs'][] =  Yii::t('twbp', 'Reports');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body global-setting-create">

        <div class="clearfix crud-navigation">
            <div class="pull-left">
                <?= Html::a(
                    Yii::t('twbp', 'Cancel'),
                    \yii\helpers\Url::previous(),
                    ['class' => 'btn btn-default']
                ) ?>
            </div>

        </div>

    </div>
</div>