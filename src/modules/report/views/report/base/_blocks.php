<?php
 
use yii\helpers\Html;
use yii\widgets\DetailView;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use yii\helpers\Url;
use yii\helpers\Inflector;
if(!isset($relatedBlock)){
    $relatedBlock = 'taktwerk\yiiboilerplate\modules\report\models\Report';
}
$blockFound = false;
?>
<?php if($relatedBlock=='taktwerk\yiiboilerplate\modules\report\models\Report'){
    $blockFound = true;
?>
 <?php $this->beginBlock('taktwerk\yiiboilerplate\modules\report\models\Report'); ?>

        <?php $viewColumns = [
                   [
            'attribute'=> 'id'
           ],
[
        'attribute' => 'title',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->title != strip_tags($model->title)){
                return \yii\helpers\StringHelper::truncate($model->title,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'title'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'title'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->title,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'title'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'title'])]])));
            }
        },
    ],
[
        'attribute' => 'class',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->class != strip_tags($model->class)){
                return \yii\helpers\StringHelper::truncate($model->class,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'class'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'class'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->class,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'class'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'class'])]])));
            }
        },
    ],
[
        'attribute' => 'function',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->function != strip_tags($model->function)){
                return \yii\helpers\StringHelper::truncate($model->function,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'function'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'function'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->function,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'function'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'function'])]])));
            }
        },
    ],
[
        'attribute' => 'params',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->params != strip_tags($model->params)){
                return \yii\helpers\StringHelper::truncate($model->params,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'params'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'params'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->params,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'params'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'params'])]])));
            }
        },
    ],
        ];

        // view columns overwrite
        $viewColumns = Yii::$app->controller->crudColumnsOverwrite($viewColumns, 'view');

        echo DetailView::widget([
        'model' => $model,
        'attributes' => $viewColumns
        ]); ?>

        
        <hr />

        <?=  ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::deleteButton($model, "twbp", ['id' => $model->id], 'view')?>

        <?php $this->endBlock(); ?>

		<?php } ?>
        <?php if($relatedBlock=='Report Histories' && Yii::$app->user->can('x_report_report-history_see') && Yii::$app->controller->crudRelations('ReportHistories')){
 $blockFound = true; ?>
        <?php $this->beginBlock('Report Histories'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsReportHistories = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('report_report-history_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('report_report-history_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('report_report-history_recompress') ? '{recompress} ' : '') . (Yii::$app->getUser()->can('report_report-history_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/report/report-history' . '/' . $action;
                        $params['ReportHistory'] = [
                            'report_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'report-history'
                ],
[
        'attribute' => 'report_file',
        'format' => 'html',
        'value' => function ($model) {
                return $model->showFilePreviewWithLink('report_file');
                },
    ],
[
        'attribute' => 'report_data_file',
        'format' => 'html',
        'value' => function ($model) {
                return $model->showFilePreviewWithLink('report_data_file');
                },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\report\models\ReportHistory::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsReportHistories = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsReportHistories, 'tab'):$columnsReportHistories;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('report_report-history_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Report History'),
                [
                    '/report/report-history/create',
                    'ReportHistory' => [
                        'report_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getReportHistories(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-reporthistories',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsReportHistories
        ])?>
        </div>
                <?php $this->endBlock() ?>

<?php }?>
<?php if($relatedBlock=='History'){?>
<?php $this->beginBlock('History'); ?><?php echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\HistoryTab::class)::widget(['model' => $model]);?><?php $this->endBlock() ?><?php }?>
<?php /* MAPPED MODELS RELATIONS - START*/?>
<?php 
if($blockFound == false){
foreach(\Yii::$app->modelNewRelationMapper->newRelationsMapping as $modelClass=>$relations){
    if(is_a($model,$modelClass)){
        foreach($relations as $relName => $relation){
            $relationKey = array_key_first($relation[1]);
            $foreignKey= $relation[1][$relationKey];
            $relClass = $relation[0];
            $baseName = \yii\helpers\StringHelper::basename($relClass);
            $cntrler = Inflector::camel2id($baseName, '-', true);
            $pluralName = Inflector::camel2words(Inflector::pluralize($baseName));
            $singularName = Inflector::camel2words(Inflector::singularize($baseName));
            $basePerm =  \Yii::$app->controller->module->id.'_'.$cntrler;
            if($relatedBlock != $pluralName){
                continue;
            }
            $blockFound = true;
            $this->beginBlock($pluralName);
?>
<?php 
$relatedModelObject = Yii::createObject($relClass::className());
$relationController = $relatedModelObject->getControllerInstance();
//TODO CrudHelper::getColumnFormat('\app\modules\guide\models\Guide', 'id');
?>
                    <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
       	<div class="clearfix"></div>
        <div>
        <?php $relMapCols = [[
                    'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can($basePerm.'_view') ? '{view} ' : '') . (Yii::$app->getUser()->can($basePerm.'_update') ? '{update} ' : '') . (Yii::$app->getUser()->can($basePerm.'_delete') ? '{delete} ' : ''),
            'urlCreator' => function ($action, $relation_model, $key, $index) use($model,$relationKey,$foreignKey,$baseName,$relatedBaseUrl) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = $relatedBaseUrl . $action;
                        
                        $params[$baseName] = [
                            $relationKey => $model->{$foreignKey}
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) { 
                             return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => $cntrler
                ],
                
        ];

        $relMapCols =array_merge($relMapCols,array_slice(array_keys($relatedModelObject->attributes),0,5));
        
        $relMapCols = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($relMapCols, 'tab'):$relMapCols;
        
        echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can($basePerm.'_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp',$singularName),
                [
                    '/'.$relationController->module->id.'/'.$cntrler.'/create',
                    $baseName => [
                        $relationKey => $model->{$foreignKey}
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[
                'class'=>'grid-outer-div'
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->{'get'.$relName}(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-'.$relName,
                    ]
                ]
                ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            'columns' =>$relMapCols
        ])?>
        </div>
                <?php $this->endBlock();
?>

<?php }
    }
}
?>
<?php }?>               
<?php /* MAPPED MODELS RELATIONS - END*/ ?>
<?php 
echo $this->blocks[$relatedBlock];
?>