<?php
use yii\helpers\Html;
use Yii;
use yii\bootstrap\Nav;
use kartik\dialog\Dialog;
$tabAction = Yii::$app->request->get('parent_action',isset($tabAction)?$tabAction:'view');
$onClick = $model->isNewRecord?"krajeeDialogRelatedTab.alert('Is available after saving this form');return false;":'';
?>
<div class="col-md-3">
	<div class="box box-solid">
		<div class="box-body no-padding">
		<?php 
		echo Nav::widget([
		    'encodeLabels'=>false,
		    'options' => ['class' =>'nav nav-pills nav-stacked'], // set this to nav-tab to get tab-styled navigation
		    'items' => array_filter([                    [
                        'label'=>'<i class="fa fa-star"></i>'.\Yii::t('twbp','Report'),
                        'options' => ['class'=>'bg-info', 'data'=>['new-rec-alert'=>($model->isNewRecord)?1:0]],
                        'active' => false,
                        'url' => ($model->isNewRecord)?'#':['report/'.$tabAction,'id'=>$model->id],
                    ],
        (\Yii::$app->hasModule('report'))?[

		            'label' => '<i class="fa fa-caret-right"></i>' .
		            \Yii::t('twbp', 'Report Histories').
		            Html::tag('span', $model->getReportHistories()->groupBy('report_history.id')->count(),
		                ['class'=>"label label-primary pull-right"]),
		            'url'=>($model->isNewRecord)?'#':[
		                      'report/'.'view',
		                      'id'=>$model->id,
		                      'parent_action'=>$tabAction,
		                      'relatedBlock'=>'Report Histories'
		                  ],
		            'options'=>['data'=>['new-rec-alert'=>($model->isNewRecord)?1:0]],
		            'visible' => Yii::$app->user->can('x_report_report-history_see') && Yii::$app->controller->crudRelations('ReportHistories'),
                    ]:null,
                    [
                        'label'=>'<i class="fa fa-history"></i>' .
		            \Yii::t('twbp', 'History').
		            Html::tag('span', $model->getHistory()->count(),
		                ['class'=>"label label-primary pull-right"]),
                    'url'=>($model->isNewRecord)?'#':[
	                      'report/'.'view',
	                      'id'=>$model->id,
	                      'parent_action'=>$tabAction,
	                      'relatedBlock'=>'History'
	                ],
		            'options'=> ['data'=>['new-rec-alert'=>($model->isNewRecord)?1:0]],
                    'visible' => Yii::$app->user->can('Administrator'),
                   ]]),
		]);
		?>
		</div>
	</div>
</div>
<?php
echo $this->render('@taktwerk-views/_new_rec_dialog', ['model' => $model]);
?>