<?php 
namespace taktwerk\yiiboilerplate\modules\report\components;

use yii\helpers\Json;
use yii\data\DataProviderInterface;
use yii\helpers\StringHelper;

abstract class ReportAbstract implements ReportInterface{

    public $columns = [];

    public $dataProvider;

    public $params;

    public function setParams($params){
        $params = Json::decode($params);
        $this->params = $params;
    }

    public function getReportDataProvider():DataProviderInterface{
        return $this->dataProvider;
    }

    public function getReportColumns():array{
        return $this->columns;
    }
    public function getReportFileName():string{
        $class = static::class;
        return 'export_' . StringHelper::basename($class) . '_' . date("Y-m-d_H-i-s");
    }
    /**
     * @inheritdoc
     */
    public function getRowLimitPerFile():int{
        return 0;
    }
    abstract public function buildReportDataProviderAndColumns();
}