<?php
namespace taktwerk\yiiboilerplate\modules\report\components;
use yii\data\DataProviderInterface;

interface ReportInterface{
    public function setParams($params);
    public function buildReportDataProviderAndColumns();
    public function getReportDataProvider():DataProviderInterface;
    public function getReportColumns():array;
    public function getReportFileName():string;
    /**
     * @desc returns row limit per file to split the reports in multiple files
     * IF row limit is 0 file is NOT split
    */
    public function getRowLimitPerFile():int;
}