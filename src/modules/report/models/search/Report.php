<?php
//Generation Date: 04-May-2021 05:44:32am
namespace taktwerk\yiiboilerplate\modules\report\models\search;

use taktwerk\yiiboilerplate\modules\report\models\search\base\Report as ReportSearchModel;

/**
* Report represents the model behind the search form about `taktwerk\yiiboilerplate\modules\report\models\Report`.
*/
class Report extends ReportSearchModel{

}