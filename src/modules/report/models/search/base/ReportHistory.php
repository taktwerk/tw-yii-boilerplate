<?php
//Generation Date: 22-Jun-2021 06:37:00am
namespace taktwerk\yiiboilerplate\modules\report\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\report\models\ReportHistory as ReportHistoryModel;

/**
 * ReportHistory represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\report\models\ReportHistory`.
 */
class ReportHistory extends ReportHistoryModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'report_id',
                    'report_file',
                    'report_file_filemeta',
                    'report_data_file',
                    'report_data_file_filemeta',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$showDeleted = (property_exists(\Yii::$app->controller, 'model')
            &&
            (is_subclass_of(ReportHistoryModel::class, \Yii::$app->controller->model) 
            	||
            	is_subclass_of(\Yii::$app->controller->model, ReportHistoryModel::class))
            &&
              (Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model)
                ||
                Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model)))
            );
        $query = ReportHistoryModel::find((Yii::$app->get('userUi')->get('show_deleted', ReportHistoryModel::class) === '1'|| $showDeleted)?false:true);

        $this->parseSearchParams(ReportHistoryModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(ReportHistoryModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(ReportHistoryModel::class),
                'params' => [
                    'page' => $this->parsePageParams(ReportHistoryModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('report_id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('report_file', $query);
        $this->applyLikeOperator('report_file_filemeta', $query);
        $this->applyLikeOperator('report_data_file', $query);
        $this->applyLikeOperator('report_data_file_filemeta', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(ReportHistoryModel::class);
    }
}
