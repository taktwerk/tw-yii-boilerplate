<?php
//Generation Date: 04-May-2021 05:44:32am
namespace taktwerk\yiiboilerplate\modules\report\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\report\models\Report as ReportModel;

/**
 * Report represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\report\models\Report`.
 */
class Report extends ReportModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'title',
                    'class',
                    'function',
                    'params',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$showDeleted = (property_exists(\Yii::$app->controller, 'model')
            &&
            (is_subclass_of(ReportModel::class, \Yii::$app->controller->model) 
            	||
            	is_subclass_of(\Yii::$app->controller->model, ReportModel::class))
            &&
              (Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model)
                ||
                Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model)))
            );
        $query = ReportModel::find((Yii::$app->get('userUi')->get('show_deleted', ReportModel::class) === '1'|| $showDeleted)?false:true);

        $this->parseSearchParams(ReportModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(ReportModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(ReportModel::class),
                'params' => [
                    'page' => $this->parsePageParams(ReportModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('title', $query);
        $this->applyLikeOperator('class', $query);
        $this->applyLikeOperator('function', $query);
        $this->applyLikeOperator('params', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(ReportModel::class);
    }
}
