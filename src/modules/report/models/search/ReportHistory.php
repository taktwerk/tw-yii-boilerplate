<?php
//Generation Date: 21-Jun-2021 11:08:17am
namespace taktwerk\yiiboilerplate\modules\report\models\search;

use taktwerk\yiiboilerplate\modules\report\models\search\base\ReportHistory as ReportHistorySearchModel;

/**
* ReportHistory represents the model behind the search form about `taktwerk\yiiboilerplate\modules\report\models\ReportHistory`.
*/
class ReportHistory extends ReportHistorySearchModel{

}