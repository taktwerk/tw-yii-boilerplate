<?php
//Generation Date: 21-Jun-2021 11:06:27am
namespace taktwerk\yiiboilerplate\modules\report\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\report\models\base\ReportHistory as BaseReportHistory;

/**
 * This is the model class for table "report_history".
 */
class ReportHistory extends BaseReportHistory
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

}
