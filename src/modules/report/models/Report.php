<?php
//Generation Date: 04-May-2021 05:44:25am
namespace taktwerk\yiiboilerplate\modules\report\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\report\models\base\Report as BaseReport;
use taktwerk\yiiboilerplate\modules\report\components\ReportInterface;
use yii\helpers\Json;
use PhpOffice\PhpSpreadsheet\IOFactory;
use yii2tech\spreadsheet\Spreadsheet;
/**
 * This is the model class for table "report".
 */
class Report extends BaseReport
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['title','class'],'required'],
            [['class'], 'validateClass'],
            /* [['function'], 'validateFunction'], */
        ]);
    }
    public function validateClass($attribute, $params, $validator)
    {
        if (!is_subclass_of($this->$attribute, ReportInterface::class)){
            $this->addError($attribute, \Yii::t('twbp','Please enter a valid class name and it must implement the ReportInterface'));
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $hints = parent::attributeHints();
        $hints['params'] = \Yii::t('twbp', 'Json format configuration');
        return $hints;  
    }
    public function getAllowedOutputTypes(){
        $outputTypes = ['csv'];
        if($this->params){
            $params = Json::decode($this->params);
            if(isset($params['output_types'])){
                $outputTypes = [];
                if(is_string($params['output_types'])){
                    $outputTypes[] = $params['output_types'];
                }else{
                    $outputTypes = $params['output_types'];
                }
            }
        }
        return $outputTypes;
    }
    /* public function validateFunction($attribute, $params, $validator)
    {
        if (!method_exists($this->class, $this->{$attribute})){
            $this->addError($attribute, \Yii::t('twbp','Function doesn\'t exists in the class'));
        }
    } */
    public function generate($type = 'csv',$saveHistory = true){
        if($this->isNewRecord==false && $this->class){
            $class = $this->class;
            /**
             * @var $obj ReportAbstract
             */
            $obj = new $class();
            if(!is_subclass_of($class, ReportInterface::class)){
                throw new \Exception('The Report class must be an instance of ReportInterface');
            }
            $obj->setParams($this->params);
            $obj->buildReportDataProviderAndColumns();
            $dataProvider = $obj->getReportDataProvider();

            $columns = $obj->getReportColumns();

            $limit = $obj->getRowLimitPerFile();

            $cloneQuery = clone $dataProvider->query;

            $totolRowCount = $cloneQuery->count();

            $chunkCount = 1;

            if($limit>0){
                $dataProvider->query->offset(0)->limit($limit);
                $chunkCount = ceil($totolRowCount/$limit);
                if($chunkCount==0){
                    $chunkCount = 1; //To generate atleast one empty file
                }
            }else{
                $limit = $totolRowCount;
                $dataProvider->query->offset(0)->limit($totolRowCount);
            }
            $offset = 0;
            $downloadFileName = $obj->getReportFileName().'.zip';
            $tempDir = sys_get_temp_dir();
            $downloadFilePath = tempnam($tempDir, $downloadFileName);
            $csvFiles = [];
            $zip = new \ZipArchive();
            $zip->open($downloadFilePath, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
            for($i = 1; $i <= $chunkCount; $i++){
                $fileName = $obj->getReportFileName();
                if($i>1){
                    $fileName = $fileName.'_'.$i;
                    $dataProvider->query->offset($offset)->limit($limit);
                }
                $exporter = new Spreadsheet([
                    'query' => $dataProvider->query,
                    'batchSize' => $limit,
                    'columns' => $columns
                ]);
                if($type==='xls'){
                    $exporter->writerType = 'Xlsx';
                    $xlFileName = $fileName.'.xlsx';
                }
                if($type==='pdf'){
                    $exporter->writerType = 'Mpdf';
                    $xlFileName = $fileName.'.pdf';
                }
                if($type==='html'){
                    $exporter->writerType = 'Html';
                    $xlFileName = $fileName.'.html';
                }
                if($type==='csv'){
                    $xlFileName = $fileName.'.csv';
                    $exporter->writerType = 'Csv';
                    $exporter->writerCreator = function($spreadsheet, $writerType){
                        $writer = IOFactory::createWriter($spreadsheet, $writerType);
                        $writer->setDelimiter(';');
                        return $writer;
                    };
                }
                $tmpFile = tempnam($tempDir, $fileName);
                $exporter->save($tmpFile);
                if($saveHistory){
                    if($type==='csv'){
                        $csvFiles[$xlFileName] = $tmpFile;
                    }else{
                        $exporter->writerType = 'Csv';
                        $exporter->writerCreator = function($spreadsheet, $writerType){
                            $writer = IOFactory::createWriter($spreadsheet, $writerType);
                            $writer->setDelimiter(';');
                            return $writer;
                        };
                        $csvTmpFile = tempnam($tempDir, $fileName.'.csv');
                        $exporter->save($csvTmpFile);
                        $csvFileName = $fileName.'.csv';
                        $csvFiles[$csvFileName] = $csvTmpFile;
                    }
                }
                $zip->addFile($tmpFile, $xlFileName);
                $offset = $offset+$limit;
            }
            $zip->close();
            if($saveHistory===true){
                $reportHistory = new ReportHistory();
                $reportHistory->report_id = $this->id;
                $reportHistory->save();
                $reportHistory->uploadFile('report_file', ['report_file' => $downloadFilePath],['report_file' => $downloadFileName], null, true);
                if($type!='csv'){
                    $csvZip = new \ZipArchive();
                    $csvZipFileName = $obj->getReportFileName().'_csv.zip';
                    $csvZipFilePath = tempnam($tempDir, $csvZipFileName);
                    $csvZip->open($csvZipFilePath, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
                    foreach($csvFiles as $fName => $fPath){
                        $csvZip->addFile($fPath,$fName);
                    }
                    $csvZip->close();
                    $csvPathInfo = [];
                    $csvPathInfo['tmp_name'] = [
                        'report_data_file' => $csvZipFilePath
                    ];
                    $csvPathInfo['name'] = [
                        'report_data_file' => $csvZipFileName
                    ];
                }else{
                    $csvPathInfo = [];
                    $csvPathInfo['tmp_name'] = [
                        'report_data_file' => $downloadFilePath
                    ];
                    $csvPathInfo['name'] = [
                        'report_data_file' => $obj->getReportFileName().'_csv.zip'
                    ];
                }
                $reportHistory->uploadFile('report_data_file', $csvPathInfo['tmp_name'], $csvPathInfo['name'], null, true);
                $reportHistory->save();
            }
            return [
                'report_path'=>$downloadFilePath,
                'report_name'=>$downloadFileName
            ];
        }
        return null;
    }
}
