<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170802_073623_webdav_acl extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $permission = $auth->createPermission('app_dav');
        $permission->description = 'WebDAV controller';
        $auth->add($permission);

        $public = $auth->getRole('Public');
        $auth->addChild($public, $permission);
    }

    public function down()
    {
        echo "m170802_073623_webdav_acl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
