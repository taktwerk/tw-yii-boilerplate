<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m180423_000001_share_attributes extends TwMigration
{
    public function up()
    {
        $this->createTable('{{%flysystem_attribute}}', [
            'id' => $this->primaryKey(),
            'flysystem_role_id' => $this->integer(),
            'flysystem_user_id' => $this->integer(),
            'target' => "ENUM('file', 'directory')",
            'pattern' => $this->string(255)->notNull(),
            'is_readable' => $this->boolean()->defaultValue(true),
            'is_writable' => $this->boolean()->defaultValue(true),
        ]);

        $this->addForeignKey('fk_flysystem_attribute_role_id', '{{%flysystem_attribute}}', 'flysystem_role_id', '{{%flysystem_role}}', 'id');
        $this->addForeignKey('fk_flysystem_user_id', '{{%flysystem_attribute}}', 'flysystem_user_id', '{{%flysystem_user}}', 'id');
    }

    public function down()
    {
        $this->dropTable('{{%flysystem_attribute}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
