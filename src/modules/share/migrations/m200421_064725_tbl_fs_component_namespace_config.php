<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200421_064725_tbl_fs_component_namespace_config extends TwMigration
{
    public function up()
    {
        $tbl = "{{%fs_component}}";
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\share"}';
        $this->addCommentOnTable($tbl, $comment);
    }

    public function down()
    {
        echo "m200421_064725_tbl_fs_component_namespace_config cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
