<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m180530_000001_share_attributes_new_permissions extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%flysystem_attribute}}', 'readable', "ENUM('skip', 'yes', 'no') DEFAULT 'yes'");
        $this->addColumn('{{%flysystem_attribute}}', 'writable', "ENUM('skip', 'yes', 'no') DEFAULT 'skip'");

        $this->dropColumn('{{%flysystem_attribute}}', 'is_readable');
        $this->dropColumn('{{%flysystem_attribute}}', 'is_writable');
    }

    public function down()
    {
        $this->dropColumn('{{%flysystem_attribute}}', 'readable');
        $this->dropColumn('{{%flysystem_attribute}}', 'writable');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
