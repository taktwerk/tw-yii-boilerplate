<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200911_190836_add_to_table_flysystem_role_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_flysystem_role_deleted_at_fs_id', '{{%flysystem_role}}', ['deleted_at','fs_id']);
    }

    public function down()
    {
        echo "m200911_190836_add_to_table_flysystem_role_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
