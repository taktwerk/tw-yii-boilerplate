<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m180518_000001_fs_component extends TwMigration
{
    public function up()
    {
        $this->createTable('{{%fs_component}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(45)->notNull(),
            'class' => "ENUM('local', 'aws', 's3', 'seafile')",
            'parameters' => $this->text(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%fs_component}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
