<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200911_190902_add_to_table_flysystem_user_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_flysystem_user_deleted_at_fs_id', '{{%flysystem_user}}', ['deleted_at','fs_id']);
        $this->createIndex('idx_flysystem_user_deleted_at_user_id', '{{%flysystem_user}}', ['deleted_at','user_id']);
    }

    public function down()
    {
        echo "m200911_190902_add_to_table_flysystem_user_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
