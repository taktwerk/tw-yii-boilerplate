<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200911_190801_add_to_table_flysystem_attribute_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_flysystem_attribute_deleted_at_flysystem_role_id', '{{%flysystem_attribute}}', ['deleted_at','flysystem_role_id']);
        $this->createIndex('idx_flysystem_attribute_deleted_at_flysystem_user_id', '{{%flysystem_attribute}}', ['deleted_at','flysystem_user_id']);
    }

    public function down()
    {
        echo "m200911_190801_add_to_table_flysystem_attribute_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
