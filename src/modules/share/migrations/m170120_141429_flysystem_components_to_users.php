<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170120_141429_flysystem_components_to_users extends TwMigration
{
    public function up()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%flysystem}}',
            [
                'id'=> Schema::TYPE_PK."",
                'fs_component' => Schema::TYPE_STRING . ' NOT NULL',
                'alias' => Schema::TYPE_STRING,
                'root_path' => Schema::TYPE_STRING,
                'read_only' => ' TINYINT(1) DEFAULT 0',
            ],
            $tableOptions
        );
        $this->createTable(
            '{{%flysystem_user}}',
            [
                'id'=> Schema::TYPE_PK."",
                'fs_id' => Schema::TYPE_INTEGER,
                'user_id' => Schema::TYPE_INTEGER,
                'root_path' => Schema::TYPE_STRING,
                'overwrite_path' => ' TINYINT(1) DEFAULT 0',
                'read_only' => ' TINYINT(1) DEFAULT 0',
                'overwrite_access' => ' TINYINT(1) DEFAULT 0',
            ],
            $tableOptions
        );

        $this->createIndex('fs_id_idx','{{%flysystem_user}}','fs_id');
        $this->createIndex('user_id_idx','{{%flysystem_user}}','user_id');
        $this->addForeignKey('flysystem_user_fk_user_id','{{%flysystem_user}}','user_id','{{%user}}','id');
        $this->addForeignKey('flysystem_user_fk_flysystem_id','{{%flysystem_user}}','fs_id','{{%flysystem}}','id');
    }

    public function down()
    {
        echo "m170120_141429_flysystem_components_to_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
