<?php

namespace taktwerk\yiiboilerplate\modules\share\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "FlysystemAttributeController".
 */
class FlysystemAttributeController extends \taktwerk\yiiboilerplate\modules\share\controllers\base\FlysystemAttributeController
{
//    /**
//     * Model class with namespace
//     */
//    public $model = 'taktwerk\yiiboilerplate\modules\share\models\FlysystemAttribute';
//
//    /**
//     * Search Model class
//     */
//    public $searchModel = 'taktwerk\yiiboilerplate\modules\share\models\search\FlysystemAttribute';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }

    public $model = \taktwerk\yiiboilerplate\modules\share\models\FlysystemAttribute::class;
    public $searchModel = \taktwerk\yiiboilerplate\modules\share\models\search\FlysystemAttribute::class;
}
