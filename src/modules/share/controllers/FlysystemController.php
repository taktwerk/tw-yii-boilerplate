<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

namespace taktwerk\yiiboilerplate\modules\share\controllers;

/**
 * This is the class for controller "FlysystemController".
 */
class FlysystemController extends \taktwerk\yiiboilerplate\modules\share\controllers\base\FlysystemController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\share\models\Flysystem';
    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\share\models\search\Flysystem';
}
