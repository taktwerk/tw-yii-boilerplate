<?php
//Generation Date: 11-Sep-2020 02:42:31pm
namespace taktwerk\yiiboilerplate\modules\share\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * FlysystemUserController implements the CRUD actions for FlysystemUser model.
 */
class FlysystemUserController extends TwCrudController
{
}
