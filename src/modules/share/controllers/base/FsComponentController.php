<?php
//Generation Date: 11-Sep-2020 02:42:32pm
namespace taktwerk\yiiboilerplate\modules\share\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * FsComponentController implements the CRUD actions for FsComponent model.
 */
class FsComponentController extends TwCrudController
{
}
