<?php
//Generation Date: 11-Sep-2020 02:42:31pm
namespace taktwerk\yiiboilerplate\modules\share\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * FlysystemRoleController implements the CRUD actions for FlysystemRole model.
 */
class FlysystemRoleController extends TwCrudController
{
}
