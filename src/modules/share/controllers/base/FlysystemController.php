<?php
//Generation Date: 11-Sep-2020 02:42:30pm
namespace taktwerk\yiiboilerplate\modules\share\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * FlysystemController implements the CRUD actions for Flysystem model.
 */
class FlysystemController extends TwCrudController
{
}
