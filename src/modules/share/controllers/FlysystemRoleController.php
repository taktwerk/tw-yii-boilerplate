<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

namespace taktwerk\yiiboilerplate\modules\share\controllers;

/**
 * This is the class for controller "FlysystemUserController".
 */
class FlysystemRoleController extends \taktwerk\yiiboilerplate\modules\share\controllers\base\FlysystemRoleController
{
    public $model = \taktwerk\yiiboilerplate\modules\share\models\FlysystemRole::class;
    public $searchModel = \taktwerk\yiiboilerplate\modules\share\models\FlysystemRoleSearch::class;
}
