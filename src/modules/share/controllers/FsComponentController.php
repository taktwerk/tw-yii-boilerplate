<?php

namespace taktwerk\yiiboilerplate\modules\share\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "FsComponentController".
 */
class FsComponentController extends \taktwerk\yiiboilerplate\modules\share\controllers\base\FsComponentController
{
//    /**
//     * Model class with namespace
//     */
    public $model = 'taktwerk\yiiboilerplate\modules\share\models\FsComponent';
//
//    /**
//     * Search Model class
//     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\share\models\search\FsComponent';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
}
