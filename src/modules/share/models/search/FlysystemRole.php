<?php
//Generation Date: 11-Sep-2020 02:42:31pm
namespace taktwerk\yiiboilerplate\modules\share\models\search;

use taktwerk\yiiboilerplate\modules\share\models\search\base\FlysystemRole as FlysystemRoleSearchModel;

/**
* FlysystemRole represents the model behind the search form about `taktwerk\yiiboilerplate\modules\share\models\FlysystemRole`.
*/
class FlysystemRole extends FlysystemRoleSearchModel{

}