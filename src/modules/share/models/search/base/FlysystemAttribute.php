<?php
//Generation Date: 11-Sep-2020 02:42:30pm
namespace taktwerk\yiiboilerplate\modules\share\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\share\models\FlysystemAttribute as FlysystemAttributeModel;

/**
 * FlysystemAttribute represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\share\models\FlysystemAttribute`.
 */
class FlysystemAttribute extends FlysystemAttributeModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'flysystem_role_id',
                    'flysystem_user_id',
                    'target',
                    'pattern',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'readable',
                    'writable',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FlysystemAttributeModel::find();

        $this->parseSearchParams(FlysystemAttributeModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(FlysystemAttributeModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(FlysystemAttributeModel::class),
                'params' => [
                    'page' => $this->parsePageParams(FlysystemAttributeModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('flysystem_role_id', $query);
        $this->applyHashOperator('flysystem_user_id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('target', $query);
        $this->applyLikeOperator('pattern', $query);
        $this->applyLikeOperator('readable', $query);
        $this->applyLikeOperator('writable', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(FlysystemAttributeModel::class);
    }
}
