<?php
//Generation Date: 11-Sep-2020 02:42:31pm
namespace taktwerk\yiiboilerplate\modules\share\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\share\models\FlysystemUser as FlysystemUserModel;

/**
 * FlysystemUser represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\share\models\FlysystemUser`.
 */
class FlysystemUser extends FlysystemUserModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'fs_id',
                    'user_id',
                    'root_path',
                    'overwrite_path',
                    'read_only',
                    'overwrite_access',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FlysystemUserModel::find();

        $this->parseSearchParams(FlysystemUserModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(FlysystemUserModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(FlysystemUserModel::class),
                'params' => [
                    'page' => $this->parsePageParams(FlysystemUserModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('fs_id', $query);
        $this->applyHashOperator('user_id', $query);
        $this->applyHashOperator('overwrite_path', $query);
        $this->applyHashOperator('read_only', $query);
        $this->applyHashOperator('overwrite_access', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('root_path', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(FlysystemUserModel::class);
    }
}
