<?php
//Generation Date: 11-Sep-2020 02:42:31pm
namespace taktwerk\yiiboilerplate\modules\share\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\share\models\FlysystemRole as FlysystemRoleModel;

/**
 * FlysystemRole represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\share\models\FlysystemRole`.
 */
class FlysystemRole extends FlysystemRoleModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'fs_id',
                    'role',
                    'root_path',
                    'overwrite_path',
                    'read_only',
                    'overwrite_access',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FlysystemRoleModel::find();

        $this->parseSearchParams(FlysystemRoleModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(FlysystemRoleModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(FlysystemRoleModel::class),
                'params' => [
                    'page' => $this->parsePageParams(FlysystemRoleModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('fs_id', $query);
        $this->applyHashOperator('overwrite_path', $query);
        $this->applyHashOperator('read_only', $query);
        $this->applyHashOperator('overwrite_access', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('role', $query);
        $this->applyLikeOperator('root_path', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(FlysystemRoleModel::class);
    }
}
