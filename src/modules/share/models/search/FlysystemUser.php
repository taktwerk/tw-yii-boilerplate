<?php
//Generation Date: 11-Sep-2020 02:42:31pm
namespace taktwerk\yiiboilerplate\modules\share\models\search;

use taktwerk\yiiboilerplate\modules\share\models\search\base\FlysystemUser as FlysystemUserSearchModel;

/**
* FlysystemUser represents the model behind the search form about `taktwerk\yiiboilerplate\modules\share\models\FlysystemUser`.
*/
class FlysystemUser extends FlysystemUserSearchModel{

}