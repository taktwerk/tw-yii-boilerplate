<?php
//Generation Date: 11-Sep-2020 02:42:30pm
namespace taktwerk\yiiboilerplate\modules\share\models\search;

use taktwerk\yiiboilerplate\modules\share\models\search\base\Flysystem as FlysystemSearchModel;

/**
* Flysystem represents the model behind the search form about `taktwerk\yiiboilerplate\modules\share\models\Flysystem`.
*/
class Flysystem extends FlysystemSearchModel{

}