<?php

namespace taktwerk\yiiboilerplate\modules\share\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\share\models\base\FsComponent as BaseFsComponent;

/**
 * This is the model class for table "fs_component".
 */
class FsComponent extends BaseFsComponent
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }
    /**
     * @return string
     */
    public function fsName()
    {
        if (substr($this->name, 0, 3) !== 'fs_') {
            return 'fs_' . $this->name;
        }
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function parsedParameters()
    {
        return json_decode($this->parameters, true);
    }

    /**
     * @return mixed
     */
    public function params()
    {
        $parameters = $this->parsedParameters();

        // Class
        $parameters['class'] = $this->componentClass();

        // An empty path should never happen (it brakes everything)
        if (empty($parameters['path']) && $this->class === self::CLASS_LOCAL) {
            $parameters['path'] = '@app/../storage/';
        }

        return $parameters;
    }

    /**
     * @return string
     */
    protected function componentClass()
    {
        if ($this->class == self::CLASS_AWS) {
            return 'creocoder\flysystem\AwsS3Filesystem';
        } elseif ($this->class == self::CLASS_S3) {
            return 'creocoder\flysystem\AwsS3Filesystem';
        } elseif ($this->class == self::CLASS_SEAFILE) {
            return 'taktwerk\yiiboilerplate\components\storage\seafile\SeafileFilesystem';
        }
        return 'creocoder\flysystem\LocalFilesystem';
    }
}
