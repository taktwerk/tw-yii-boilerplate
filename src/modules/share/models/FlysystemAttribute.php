<?php

namespace taktwerk\yiiboilerplate\modules\share\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\share\models\base\FlysystemAttribute as BaseFlysystemAttribute;

/**
 * This is the model class for table "flysystem_attribute".
 */
class FlysystemAttribute extends BaseFlysystemAttribute
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

}
