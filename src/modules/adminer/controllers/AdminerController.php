<?php

namespace taktwerk\yiiboilerplate\modules\adminer\controllers;

use yii\filters\AccessControl;
use yii\web\Controller;

class AdminerController extends Controller
{
    public $enableCsrfValidation = false;
    public $layout = false;
    public $defaultAction = 'adminer';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'adminer',
                        ],
                        'roles' => ['Authority','Superadmin']
                    ]
                ]
            ]
        ];
    }

    public function actionAdminer()
    {
        $this->layout=false;
        return $this->render('index');
    }
}
