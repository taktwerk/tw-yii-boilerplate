<?php

namespace taktwerk\yiiboilerplate\modules\adminer;

class Module extends \taktwerk\yiiboilerplate\components\TwModule
{
    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\adminer\controllers';

    public function init()
    {
        parent::init();

        $this->defaultRoute = 'adminer';
    }
}
