<?php

namespace taktwerk\yiiboilerplate\modules\search\models\interfaces;

/**
 * Interface SearchableInterface
 * @package taktwerk\yiiboilerplate\modules\search\models\interfaces
 */
interface SearchableInterface
{
    /**
     * @return string translation key
     */
    public function searchableKey();

    /**
     * @return array list of fields to search on
     */
    public function searchableFields();
}
