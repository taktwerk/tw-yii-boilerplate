<?php

namespace taktwerk\yiiboilerplate\modules\search\components;

use taktwerk\yiiboilerplate\modules\search\models\interfaces\SearchableInterface;
use Exception;
use Yii;
use yii\helpers\ArrayHelper;

class SearchBuilder
{
    /**
     * @var null|\yii\base\Module
     */
    protected $module;

    /**
     * @var string The search term
     */
    protected $query;

    /**
     * @var bool If the results are flat, for example for ajax results
     */
    protected $flat = false;

    /**
     * SearchBuilder constructor.
     */
    public function __construct()
    {
        $this->module = Yii::$app->getModule('search');
    }

    /**
     * @param $query
     * @param array $classes
     * @param bool $flat
     * @return array
     * @throws Exception
     */
    public function search($query, $classes = [], $flat = false)
    {
        $results = [];
        $this->query = trim($query);
        $this->flat = $flat;

        if (!is_array($classes)) {
            $classes = [$classes];
        }

        foreach ($this->module->models as $model) {
            // If we are limiting the results to one model and it's not in the list, skip
            if (!empty($classes) && !in_array($model, $classes)) {
                continue;
            }

            $modelObject = new $model();

            // Make sure the model implements the interface
            if (($modelObject instanceof SearchableInterface) === false) {
                throw new Exception(Yii::t('twbp', 'Searchable: Model {model} doesn\'t implement the SearchableInterface interface.', ['model' => $model]));
            }

            // Start building those results
            $result = $this->addModel($modelObject);
            $results[] = $result;
        }

        // For flat results (ajax), we want to merge everything into one key
        if ($this->flat) {
            $models = [];
            foreach ($results as $key => $data) {
                if (!empty($data['models'])) {
                    $models = ArrayHelper::merge($models, $data['models']);
                }
            }
            $results = [0 => ['key' => 'flat', 'models' => $models]];
        }
        return $results;
    }

    /**
     *
     */
    protected function addModel($model)
    {
        $results = [
            'key' => $model->searchableKey(),
            'models' => [],
        ];

        // What does the search look like?
        $fields = $model->searchableFields();
        $pk = $model->primaryKey()[0];

        foreach ($fields as $field) {
            list ($fieldName, $fieldConf) = $this->fieldConfig($field);

            $search = new $model();
            if (method_exists($search, 'searchableCustom')) {
                $search = $search->searchableCustom($this->query);
            } else {
                $search = $search->find()
                    ->andFilterWhere([
                        'LIKE',
                        $model->tableName() . '.' . $fieldName,
                        str_replace('*', $this->query, $fieldConf['pattern']),
                        false
                    ]);
            }
            $models = $search
                ->limit($this->module->limitPerQuery)
                ->all();

            foreach ($models as $result) {
                // Todo: Check if can view this model before adding id
                if (!isset($results['models'][$result->$pk])) {
                    $results['models'][$result->$pk] = [
                        'weight' => $fieldConf['weight'],
                        'data' => $result
                    ];
                } else {
                    $results['models'][$result->$pk]['weight'] += $fieldConf['weight'];
                }
            }
        }

        // Reorder results based on weight
        uasort($results['models'], function ($a, $b) {
            return ($b['weight'] - $a['weight']);
        });

        return $results;
    }

    /**
     *
     */
    protected function fieldConfig($field)
    {
        // Just a string? use defaults
        if (is_string($field)) {
            return [
                $field,
                [
                    'weight' => 1,
                    'exact' => false,
                    'pattern' => '%*%',
                ]
            ];
        }

        // We have an array, add missing party
        return [
            $field,
            [
                'weight' => ArrayHelper::getValue($field, 'weight', 1),
                'exact' => ArrayHelper::getValue($field, 'exact', false),
                'pattern' => ArrayHelper::getValue($field, 'pattern', '%*%'),
            ]
        ];
    }
}
