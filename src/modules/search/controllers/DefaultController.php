<?php

namespace taktwerk\yiiboilerplate\modules\search\controllers;

use taktwerk\yiiboilerplate\modules\search\components\SearchBuilder;
use yii\web\Controller;
use Yii;

class DefaultController extends Controller
{
    /**
     * @var SearchBuilder
     */
    protected $search;

    /**
     * Init
     */
    public function init()
    {
        parent::init();
        $this->search = new SearchBuilder();
    }

    /**
     * @param $query
     */
    public function actionIndex()
    {
        $query = Yii::$app->request->get('query', false);
        $ajax = Yii::$app->request->isAjax;
        $type = Yii::$app->request->get('type', []);
        if (!empty($type)) {
            $type = base64_decode($type);
        }
        $results = !empty($query) ? $this->search->search($query, $type, $ajax) : [];

        if ($ajax) {
            return $this->asJson($results);
        }

        return $this->render('index', [
            'results' => $results
        ]);
    }
}
