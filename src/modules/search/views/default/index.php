<?php foreach ($results as $model) : ?>
    <h2 class="title"><?=Yii::t('twbp', $model['key'])?></h2>

    <div class="row">
        <?php foreach ($model['models'] as $result) : ?>
            <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="box box-flat">
                    <div class="box-header">
                        <h3>
                            <a href="#">
                                <?=$result['data']->toString()?>
                            </a>
                        </h3>
                    </div>
                    <div class="box-body">
                        <?=$result['data']->getEntryDetails()?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endforeach; ?>