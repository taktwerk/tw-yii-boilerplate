<?php

namespace taktwerk\yiiboilerplate\modules\search\transformers;

/**
 * Interface TransformerInterface
 * @package taktwerk\yiiboilerplate\modules\search\transformers
 */
class SearchTransformer
{
    public $model;

    public function toArray()
    {
        return $this->model->toArray();
    }
}
