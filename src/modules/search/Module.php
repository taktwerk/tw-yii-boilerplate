<?php


namespace taktwerk\yiiboilerplate\modules\search;

class Module extends \taktwerk\yiiboilerplate\components\TwModule
{
    /**
     * @var string
     */
    public $defaultRoute = 'index';

    /**
     * @var array Models that implement the searchable interface
     */
    public $models = [];

    /**
     * @var int MySQL Limit for the search on each model field.
     */
    public $limitPerQuery = 10;

    /**
     * Init module
     */
    public function init()
    {
        parent::init();

        // When in console mode, switch controller namespace to commands
        if (\Yii::$app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'taktwerk\yiiboilerplate\modules\search\commands';
        }
    }
}
