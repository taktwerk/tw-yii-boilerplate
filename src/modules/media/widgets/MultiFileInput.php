<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 6/19/2017
 * Time: 11:01 AM
 */
namespace taktwerk\yiiboilerplate\modules\media\widgets;

use taktwerk\yiiboilerplate\widget\FileInput;
use yii\helpers\Url;
use yii\helpers\Html;
use taktwerk\yiiboilerplate\modules\media\behaviors\MediaBehavior;
use yii\helpers\Json;
use yii\web\View;

class MultiFileInput extends FileInput
{

    /**
     * *
     * This field stores the ids of the media model comma seprated
     */
    public $fileIdAttr;
    
    public $languageKey=null;
    public $deleteUrl = null;
    /**
     * Model Type
     */
    public $modelType;
    protected static $widgetCount = 0;
    protected static $requiredJsAdded = false;
    /**
     * *
     * Media category in the model
     */
    public $mediaCategory;
    
    protected $fileIdAttrName;
    
    public function init()
    {
        self::$widgetCount =self::$widgetCount++;
        parent::init();
        if($this->languageKey){
            $modelName =$this->model->formName();
            $name = $modelName . '['.$this->languageKey.'][' . $modelName . '][' . $this->fileIdAttr . ']';
            $this->fileIdAttrName = $name;
        }
        $this->setDefaults();
    }

    protected function initWidget()
    {
        $css = <<<EOT
.file-preview-image {font-size: 15px;word-break: break-all;}
EOT;
        \Yii::$app->view->registerCss($css);
        $input = parent::initWidget();
        
        $hidden = $this->field->form->field($this->model, $this->fileIdAttr,['options'=>['tag'=>false],'template'=>'{input}'])
        ->hiddenInput()->label(false);
        
        if($this->languageKey){
            $hidden = $this->field->form->field($this->model, $this->fileIdAttr,['options'=>['tag'=>false],'template'=>'{input}'])
            ->hiddenInput([
                'name'=>$this->fileIdAttrName
            ])->label(false);
        }

        return $input . $hidden;
    }

    public function setDefaults()
    {

        if (strlen($this->fileIdAttr) == 0 || strlen($this->modelType) == 0 || strlen($this->mediaCategory) == 0) {
            throw new \Exception(\Yii::t('twbp', 'Please configure fileIdAttr,modelType and mediaCategory to use the widget'));
        }

        $behaviors = $this->model->getBehaviors();
        $hasMediaBehavior = false;
        foreach ($behaviors as $b) {
            if ($b instanceof MediaBehavior) {
                $hasMediaBehavior = true;
                break;
            }
        }
        if (! $hasMediaBehavior) {
            throw new \Exception('The model must use MediaBehavior (taktwerk\yiiboilerplate\modules\media\behaviors)');
        }

        $files = $this->model->getMediaFiles($this->modelType, $this->mediaCategory);

        $initialPreview = [];
        $initialPreviewConfig = [];

        $multi_file_ids = [];

        if (count($files) > 0) {
            foreach ($files as $f) {
                $initialPreview[] = $f->getUrl();
                $initialPreviewConfig[] = [
                    'caption' => $f->alt_name,
                    'url' =>($this->deleteUrl)?$this->deleteUrl: Url::toRoute([
                        '/media/default/delete-file'
                    ]),
                    'downloadUrl' => $f->getUrl(),
                    'key' => $f->id,
                    'type' => $f->getType(),
                    'filetype' => $f->getMimeType(),
                    'size' => $f->size
                ];
                $multi_file_ids[] = $f->id;
            }
        }
        $this->model->{$this->fileIdAttr} = implode(',', $multi_file_ids);

        $pluginOptions = [
            'showUpload' => false,
            'showRemove' => false,
            'showCaption' => false,
           // 'minFileCount'=>1,
            //'maxFileCount' => 2,
            'uploadAsync'=> true,
            'validateInitialCount' => true,
            "browseOnZoneClick" => true,
            'uploadUrl' => Url::toRoute([
                "/media/default/upload-file",
                'atr' =>$this->attribute,
                'fname' => $this->model->formName(),
                'key'=>$this->languageKey
            ]),
            'initialPreview' => $initialPreview,
            'initialPreviewConfig' => $initialPreviewConfig,
            'initialCaption' => '',
            'deleteUrl' => ($this->deleteUrl)?$this->deleteUrl: Url::toRoute([
                '/media/default/delete-file'
            ]),
            'initialPreviewAsData' => true,
            'initialCaption' => '',
            'initialPreviewFileType' => 'image',
            'fileActionSettings' => [],
            'overwriteInitial' => false,
        ];

        $this->pluginOptions = array_merge($pluginOptions, $this->pluginOptions);

        // \taktwerk\yiiboilerplate\modules\media\Module
        $module = \Yii::$app->getModule('media');
        if (! $module instanceof \taktwerk\yiiboilerplate\modules\media\Module) {
            $module = \Yii::$container->get(\taktwerk\yiiboilerplate\modules\media\Module::class, [
                'mediaModule'
            ]);
        }
        $allowedExtensions = \Yii::$app->security->hashData(Json::encode($this->pluginOptions['allowedFileExtensions']), $module->fileRuleSecretKey);
        $allowedFileTypes = \Yii::$app->security->hashData(Json::encode($this->pluginOptions['allowedFileTypes']), $module->fileRuleSecretKey);
        $currentUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $encryptedUrl = \Yii::$app->security->hashData($currentUrl, $module->fileRuleSecretKey);

        $uploadExtraData = [
            'fe' => $allowedExtensions,
            'ft' => $allowedFileTypes,
            'sender' => $encryptedUrl
        ];

        $this->pluginOptions['uploadExtraData'] = is_array($this->pluginOptions['uploadExtraData']) ? array_merge($uploadExtraData, $this->pluginOptions['uploadExtraData']) : $uploadExtraData;

        $inputName = Html::getInputName($this->model, $this->fileIdAttr);

        if($this->fileIdAttrName){
            $inputName = $this->fileIdAttrName;
        }

        $pluginEvents = [
            'filedeleted' => 'function(event, key, jqXHR, data){
                                var el = $("[name=\'' . $inputName . '\']");
                                var items = [];
                                if(el.val()){
                                    items = el.val().split(",");
                                }
                                var index = items.indexOf(key.toString());
                                if (index !== -1) items.splice(index, 1);
                                el.val(items.join(","));
                            }',
            /* 'filepredelete' => 'function(xhr){
                               var abort = true;
                               if (confirm("Are you sure you want to remove this file? This action will delete the file even without pressing the save button"))
                               {
                                   abort = false;
                               }
                               return abort;
                        }', */
            'filebatchselected' => 'function(event, files){
                                        var filestack = $(this).fileinput("getFileStack"), fstack = [];
                                        $.each(filestack, function(fileId, fileObj) {
                                            if (fileObj !== undefined) {
                                                fstack.push(fileObj);
                                            }
                                        });
                                        if(fstack.length>0) {
                                            $(this).fileinput("upload");
                                        }
                                    }',
            'fileuploaded' => 'function(event, data, previewId, index){
                                var el = $("[name=\'' . $inputName . '\']");
                                var items = [];
                                if(el.val()){
                                    items = el.val().split(",");
                                }
                                items.push(data.response.initialPreviewConfig[0].key.toString());
                                el.val(items.join(","));
                                $(window).unbind("beforeunload");
                                $(window).bind("beforeunload", function () {
                                        return confirm();
                                });
                            }'
        ];
        $this->pluginEvents = array_merge($pluginEvents, $this->pluginEvents);
        $atrLbl = $this->model->getAttributeLabel($this->attribute);
        $options = [
            'multiple' => true
        ];

        if($this->pluginOptions['minFileCount'] && $this->pluginOptions['uploadUrl'])
        {
            $minFileCount = $this->pluginOptions['minFileCount'];
            $formId = $this->field->form->getId();
            $atr = $this->attribute; 
            $fieldId = $this->options['id'];
            
            $tClass = "field-$fieldId";
            $erMsg = \Yii::t('twbp',"{attributeLabel} must have at least {minimumFileCount} file",[
                'attributeLabel'=> $atrLbl,
                'minimumFileCount' => $minFileCount
            ]);
            $oneTimeJs = '';

            if(self::$requiredJsAdded==false){
                //This js code is needed only once in the web page
               self::$requiredJsAdded = true;
               $oneTimeJs = <<<EOT
$('#{$formId}').find('[type="submit"]').on('click', function(){
            $(window).unbind("beforeunload");
});
if(yii.validation){
    yii.validation.kartikFileInput = function (value, messages, options){ 
               var valid = false;
               if (jQuery('#'+options.fieldId).fileinput('_isFileSelectionValid')) {
                    valid = true;
               }
               if (valid==false){
                    yii.validation.addMessage(messages, options.message, value);
                }
            };
}
EOT;
            }
           $validateOnChange = ($this->field->validateOnChange === false)?'false':'true';
           $validateOnBlur = ($this->field->validateOnBlur === false)?'false':'true';

           $js = <<<EOT
                   jQuery('.{$tClass}').addClass('required');
jQuery(document).ready(function(){

$oneTimeJs

setTimeout(function(){
$('#{$formId}').yiiActiveForm('remove', '$fieldId');

jQuery('#{$formId}').yiiActiveForm('add',{
"id":"{$fieldId}",
"name":"{$atr}",
"container":".{$tClass}",
"input":"#{$fieldId}",
"validateOnBlur":{$validateOnBlur},
"validateOnChange":{$validateOnChange},
"error":".help-block.help-block-error",
"validate": function(attribute, value, messages, deferred, \$form){
            yii.validation.kartikFileInput(value, messages, {
            "message": "{$erMsg}",
            "fieldId" :"$fieldId"
            });
        }
}); }, 250);
});

EOT;
            \Yii::$app->view->registerJs($js,View::POS_END);
        }
        
        $this->options = array_merge($options, $this->options);
        
    }
}