<?php
namespace taktwerk\yiiboilerplate\modules\media\controllers;

use yii\web\Controller;
use taktwerk\yiiboilerplate\modules\media\models\MediaFile;
use yii\helpers\Json;
use \yii\web\UploadedFile;
use yii\web\ForbiddenHttpException;
use taktwerk\yiiboilerplate\modules\media\components\EFileValidator;

/**
 * Default controller for the `media` module
 */
class DefaultController extends Controller
{

    /**
     * Renders the index view for the module
     *
     * @return string
     */
    public function actionIndex()
    {
     //   return $this->render('index');
    }

    public function actionUploadFile($atr, $fname,$key=null)
    {
        $security = \Yii::$app->getSecurity();
        $secret = $this->module->fileRuleSecretKey;
        $request = \Yii::$app->request;
        try {
            $fileExtensionsEncrypted = $request->post('fe');
            $fileTypesEncrypted = $request->post('ft');
            if ($fileExtensionsEncrypted == null || $fileTypesEncrypted == null) {
                throw new \yii\web\UnprocessableEntityHttpException(\Yii::t('twbp', 'Invalid data submitted during file upload'));
            }
            if(($fe = $security->validateData($request->post('fe'), $secret)) && ($ft = $security->validateData($request->post('ft'), $secret)) && ($sender = $security->validateData($request->post('sender'), $secret))){
                $allowedFileExtension = Json::decode($fe);
                $allowedFileTypes = Json::decode($ft);
            }else{
                throw new \yii\web\UnprocessableEntityHttpException(\Yii::t('twbp', 'Upload failed, Please reload the page and try again'));
            }
            
        } catch (\Exception $e) {
            throw new \yii\web\UnprocessableEntityHttpException($e->getMessage());
        }
        $referrer = $request->referrer;
        if ($referrer != $sender) {
            throw new \yii\web\UnprocessableEntityHttpException(\Yii::t('twbp', 'Data sent is not in correct format'));
        }
        if ($request->isPost) {
            $uploadedInstance = UploadedFile::getInstanceByName($fname . "[" . $atr . "]");
            if ($uploadedInstance == null) {
                $uploadedInstance = @UploadedFile::getInstancesByName($fname . "[" . $atr . "]")[0];
            }
            if($key){
                $uploadedInstance= UploadedFile::getInstanceByName($fname ."[{$key}][{$fname}][{$atr}]");
                if($uploadedInstance==null){
                    $uploadedInstance = UploadedFile::getInstancesByName($fname ."[{$key}][{$fname}][{$atr}]")[0];
                }
            }
            if ($uploadedInstance == null) {
                throw new \yii\web\UnprocessableEntityHttpException(\Yii::t('twbp', 'Please upload a file'));
            }

            $validator = new EFileValidator();
            $validator->extensions = $allowedFileExtension;
            $mimeTypes = [];

            if (is_array($allowedFileTypes)) {
                foreach ($allowedFileTypes as $type) {
                    if ($type == 'object' || $type == 'other') {
                        $mimeTypes = [];
                        break;
                    }elseif($type == 'document'){
                        $mimeTypes = array_merge($mimeTypes, [
                            'application/pdf',
                            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                            'application/msword',
                            'application/vnd.ms-excel',
                            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                        ]);
                    }
                    else {
                        $mimeTypes[] = '*' . $type . '*';
                    }
                }
            }
            $validator->mimeTypes = $mimeTypes;
            $error = '';
            $validator->validate($uploadedInstance, $error);
            if ($error) {
                throw new \yii\web\UnprocessableEntityHttpException($error);
            }

            $model = new MediaFile();
            $pathInfo = [];
            $file = $_FILES[$fname];
            
            if ($file && is_array($file)) {
                $pathInfo = [];
                if($key){
                    $pathInfo['tmp_name'] = [
                        'file_name' => is_array($file['tmp_name'][$key][$fname][$atr]) ? $file['tmp_name'][$key][$fname][$atr][0] : $file['tmp_name'][$key][$fname][$atr]
                    ];
                    
                    $pathInfo['name'] = [
                        'file_name' => is_array($file['name'][$key][$fname][$atr]) ? $file['name'][$key][$fname][$atr][0] : $file['name'][$key][$fname][$atr]
                    ];
                    
                }else{
                    $pathInfo['tmp_name'] = [
                        'file_name' => is_array($file['tmp_name'][$atr]) ? $file['tmp_name'][$atr][0] : $file['tmp_name'][$atr]
                    ];
                    $pathInfo['name'] = [
                        'file_name' => is_array($file['name'][$atr]) ? $file['name'][$atr][0] : $file['name'][$atr]
                    ];
                    
                }
                
                $model->alt_name = $pathInfo['name']['file_name'];
                $model->size = $uploadedInstance->size;
                $model->save(false);
                if (! $model->uploadFile('file_name', $pathInfo['tmp_name'], $pathInfo['name'])) {
                    return false;
                }
                
                if ($request->isAjax) {
                    \Yii::$app->response->format = 'json';
                    $response = [];
                    $response['initialPreview'] = [
                        $model->getUrl()
                    ];
                    $response['initialPreviewConfig'][] = [
                        'key' => $model->id,
                        'caption' => $model->alt_name,
                        'type' => $model->getType(),
                        'size' => $model->size
                    ];
                    return $response;
                }
            } else {
                if ($request->isAjax) {
                    \Yii::$app->response->format = 'json';
                    throw new \yii\web\UnprocessableEntityHttpException(\Yii::t('twbp', 'Please upload the file correctly'));
                }
            }
        }
        $this->redirect($request->referrer);
    }

    public function actionDeleteFile()
    {
        $id = \Yii::$app->request->post('key');
        $model = MediaFile::findOne([
            'id' => $id
        ]);
        if ($model) {
            if (!$model->deletable()) {
                throw new ForbiddenHttpException(\Yii::t('twbp', 'You are not allowed to perform this action'));
            }
            if(\Yii::$app->request->isAjax){
                if($model->model_type ==null && $model->model_id==null){
                    $model->delete();
                }else{
                $deleteList = \Yii::$app->session->get('deletemedia');
                if(!is_array($deleteList)){
                    $deleteList = [];
                }
                $deleteList[$model->id] = '1';
                \Yii::$app->session->set('deletemedia',$deleteList);
                }
            }else{
                $model->delete();}
        }
        return true;
    }
}
