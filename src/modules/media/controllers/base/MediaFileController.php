<?php
//Generation Date: 11-Sep-2020 02:54:31pm
namespace taktwerk\yiiboilerplate\modules\media\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * MediaFileController implements the CRUD actions for MediaFile model.
 */
class MediaFileController extends TwCrudController
{
}
