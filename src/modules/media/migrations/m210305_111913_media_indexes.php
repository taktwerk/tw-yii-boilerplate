<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m210305_111913_media_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex(
            'idx_hfr_case_del_at_model_id_type',
            '{{%media_file}}',
            ['deleted_at','model_id','model_type']
            );
        $this->createIndex(
            'idx_hfr_case_del_at_model_id_type_category',
            '{{%media_file}}',
            ['deleted_at','model_id','model_type','media_category']
            );
    }

    public function down()
    {
        echo "m210305_111913_media_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
