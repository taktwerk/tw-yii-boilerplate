<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200108_110755_comment_config_media_file extends TwMigration
{
    public function up()
    {
        $tbl = "{{%media_file}}";
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\media"}';
        $this->addCommentOnTable($tbl, $comment);
    }

    public function down()
    {
        echo "m200108_110755_comment_config_media_file cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
