<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%media_file}}`.
 */
class m200922_041433_add_file_meta_column_to_media_file_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $comment ='{"inputtype":"filemeta","related_attribute":"file_name"}';
        $this->addColumn('{{%media_file}}', 'file_meta', $this->text()->comment($comment)->after('file_name'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%media_file}}', 'file_meta');
    }
}
