<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191202_073508_add_media_table extends TwMigration
{

    public function up()
    {
        $tbl = "{{%media_file}}";
        if ($this->db->getTableSchema($tbl, true) === null) {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            $this->createTable($tbl, [
                'id' => Schema::TYPE_PK . "",
                'file_name' => $this->string(255)
                    ->null(),
                'alt_name' => $this->string(255)
                    ->null(),
                'model_type' => $this->string(255)
                    ->null(),
                'model_id' => $this->integer(11)
                    ->null(),
                'size' => $this->string(255)
                    ->null()
                    ->comment("Size in bytes"),
                'media_category' => $this->integer(11)
                    ->null()
            ], $tableOptions);
        }
    }

    public function down()
    {
        $this->dropTable("{{%media_file}}");
        return false;
    }
    
    /*
     * // Use safeUp/safeDown to run migration code within a transaction
     * public function safeUp()
     * {
     * }
     *
     * public function safeDown()
     * {
     * }
     */
}
