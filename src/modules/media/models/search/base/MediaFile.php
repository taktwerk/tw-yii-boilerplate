<?php
//Generation Date: 11-Sep-2020 02:54:31pm
namespace taktwerk\yiiboilerplate\modules\media\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\media\models\MediaFile as MediaFileModel;

/**
 * MediaFile represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\media\models\MediaFile`.
 */
class MediaFile extends MediaFileModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'file_name',
                    'alt_name',
                    'model_type',
                    'model_id',
                    'size',
                    'media_category',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MediaFileModel::find();

        $this->parseSearchParams(MediaFileModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(MediaFileModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(MediaFileModel::class),
                'params' => [
                    'page' => $this->parsePageParams(MediaFileModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('model_id', $query);
        $this->applyHashOperator('media_category', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('file_name', $query);
        $this->applyLikeOperator('alt_name', $query);
        $this->applyLikeOperator('model_type', $query);
        $this->applyLikeOperator('size', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(MediaFileModel::class);
    }
}
