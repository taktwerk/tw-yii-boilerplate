<?php

namespace taktwerk\yiiboilerplate\modules\media\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\media\models\base\MediaFile as BaseMediaFile;
use taktwerk\yiiboilerplate\traits\UploadTrait;
use taktwerk\yiiboilerplate\components\Helper;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use taktwerk\yiiboilerplate\helpers\FileHelper;
use taktwerk\yiiboilerplate\models\TwActiveRecord;
/**
 * This is the model class for table "media_file".
 */
class MediaFile extends BaseMediaFile
{
    use UploadTrait;
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

    // /**
    // * List of additional rules to be applied to model, uncomment to use them
    // * @return array
    // */
    // public function rules()
    // {
    // return array_merge(parent::rules(), [
    // [['something'], 'safe'],
    // ]);
    // }
    public function getUrl()
    {
        return $this->getFileUrl('file_name', null, true);
    }

    public function getType()
    {
        return $this->getFileType('file_name');
    }

    public function getMimeType()
    {
        return $this->getFileMimeType('file_name');
    }
    public function deletable()
    { //By default media file is deletable by user who created/update this model or who created/updated the connected model from the model_type,model_id 
        if(false == (\Yii::$app->user->id == $this->created_by || \Yii::$app->user->id == $this->updated_by)){
            $class = $this->model_type;
            if($class && $this->model_id && $model = $class::findOne($this->model_id)){
                if($model->hasMethod('deletable') && $model->deletable()){
                    return true;
                }
                if($model->hasMethod('updatable') && $model->updatable()){
                    return true;
                }
                if($model->hasAttribute('created_by')){
                    if($model->created_by == \Yii::$app->user->id){
                        return true;
                    }
                }
                if($model->hasAttribute('updated_by')){
                    if($model->updated_by == \Yii::$app->user->id){
                        return true;
                    }
                }
            }
            return false;
        }else{
            return true;
        }
    }

    public function delete()
    {
       parent::delete();
       $this->deleteFile('file_name');
    }

    public static function copyToMediaFromModel(TwActiveRecord $model, $attribute, $modelType, $mediaCategory, $suffix = ''){
        if($model->{$attribute}){
            $pathInfo = [];
            $mFile = new static();
            $tmpFile = ClassDispenser::getMappedClass(Helper::class)::getLocalTempFile($model->getUploadPath() . $model->{$attribute});
            $ext = pathinfo($model->{$attribute}, PATHINFO_EXTENSION);
            $file = basename($model->{$attribute}, '.' . $ext);
            $filename = $file . $suffix . '.' . $ext;
            $pathInfo['tmp_name'] = [
                'file_name' => $tmpFile
            ];
            $pathInfo['name'] = [
                'file_name' => $filename
            ];
            $mFile->model_id = $model->id;
            $mFile->media_category = $mediaCategory;
            $mFile->model_type = $modelType;
            $mFile->size = (string)\Yii::$app->fs->getSize($model->getUploadPath() . $model->{$attribute});
            $mFile->alt_name = ClassDispenser::getMappedClass(Helper::class)::removeTimestampFromFileName($filename);
            if($mFile->alt_name==''){
                $mFile->alt_name = $model->{$attribute};
            }
            $transcation = \Yii::$app->db->beginTransaction();
            $mFile->save();
            $uploaded = $mFile->uploadFile('file_name', $pathInfo['tmp_name'], $pathInfo['name'], null, true,false);
            $uploaded?$transcation->commit():$transcation->rollBack();
            return $uploaded;
        }
    }
    public function createACopy($relatedModel=null,$modelType=null){
        $mFile = $this;
        if(!Yii::$app->fs->has($mFile->getUploadPath() . $mFile->file_name)){
            return false;
        }
        $originalFile = clone $mFile;
        $pathInfo = [];
        $tmpFile = Helper::getLocalTempFile($mFile->getUploadPath() . $mFile->file_name);
        $pathInfo['tmp_name'] = [
            'file_name' => $tmpFile
        ];
        $pathInfo['name'] = [
            'file_name' => $mFile->file_name
        ];
        $mFile->isNewRecord = true;
        $mFile->id = null;
        if($relatedModel){
            $mFile->model_id = $relatedModel->id;
        }
        if($modelType){
            $mFile->model_type = $modelType;
        }
        $mFile->save();
        $skipQueueProcessing = false;
        $mimeType = \yii\helpers\FileHelper::getMimeType($mFile->file_name);
        $fileType = $mFile->getFileType('file_name');
        if (in_array($fileType, static::$compressibleFileTypes)){
            $skipQueueProcessing = ClassDispenser::getMappedClass(FileHelper::class)::hasAllCompressedFiles($originalFile,'file_name');
        }

        $mFile->uploadFile('file_name', $pathInfo['tmp_name'], $pathInfo['name'], null, true,$skipQueueProcessing);
        if (in_array($fileType, static::$compressibleFileTypes)){
            /* Compressed file copying - start */
            $filesCopied = ClassDispenser::getMappedClass(FileHelper::class)::copyCompressedFilesFromModelToModel($originalFile, $mFile, 'file_name');
            /* Compressed file copying - end */
            if($filesCopied==false && $skipQueueProcessing==true){
                $mFile->compressFile($mFile,'file_name');
            }
        }
        \yii\helpers\FileHelper::unlink($tmpFile);
        unset($tmpFile);
        return $mFile;
    }
}
