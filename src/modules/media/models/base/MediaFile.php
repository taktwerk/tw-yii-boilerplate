<?php
//Generation Date: 11-Sep-2020 02:54:10pm
namespace taktwerk\yiiboilerplate\modules\media\models\base;

use Yii;
use \taktwerk\yiiboilerplate\models\TwActiveRecord;
use yii\db\Query;


/**
 * This is the base-model class for table "media_file".
 * - - - - - - - - -
 * Generated by the modified Giiant CRUD Generator by taktwerk.com
 *
 * @property integer $id
 * @property string $file_name
 * @property string $alt_name
 * @property string $model_type
 * @property integer $model_id
 * @property string $size
 * @property integer $media_category
 * @property integer $deleted_by
 * @property string $deleted_at
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property string $toString
 * @property string $entryDetails
 */
class MediaFile extends TwActiveRecord implements \taktwerk\yiiboilerplate\modules\backend\models\RelationModelInterface{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'media_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [
                ['model_id', 'media_category', 'deleted_by'],
                'integer'
            ],
            [
                ['deleted_at','file_meta'],
                'safe'
            ],
            [
                ['file_name', 'alt_name', 'model_type', 'size'],
                'string',
                'max' => 255
            ]
        ];
        
        return  array_merge($rules, $this->buildRequiredRulesFromFormDependentShowFields());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('twbp', 'ID'),
            'file_name' => Yii::t('twbp', 'File Name'),
            'alt_name' => Yii::t('twbp', 'Alt Name'),
            'model_type' => Yii::t('twbp', 'Model Type'),
            'model_id' => Yii::t('twbp', 'Model'),
            'size' => Yii::t('twbp', 'Size'),
            'media_category' => Yii::t('twbp', 'Media Category'),
            'created_by' => \Yii::t('twbp', 'Created By'),
            'created_at' => \Yii::t('twbp', 'Created At'),
            'updated_by' => \Yii::t('twbp', 'Updated By'),
            'updated_at' => \Yii::t('twbp', 'Updated At'),
            'deleted_by' => \Yii::t('twbp', 'Deleted By'),
            'deleted_at' => \Yii::t('twbp', 'Deleted At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributePlaceholders()
    {
        return [
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
        ];
    }

    /**
     * Auto generated method, that returns a human-readable name as string
     * for this model. This string can be called in foreign dropdown-fields or
     * foreign index-views as a representative value for the current instance.
     *
     * @author: taktwerk
     * This method is auto generated with a modified Model-Generator by taktwerk.com
     * @return String
     */
    public function toString()
    {
        return $this->alt_name; // this attribute can be modified
    }


    /**
     * Getter for toString() function
     * @return String
     */
    public function getToString()
    {
        return $this->toString();
    }

    /**
     * Description for the table/model to show on grid an form view
     * @return String
     */
    public static function tableHint()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return [
        ];
    }



    /**
     * Return details for dropdown field
     * @return string
     */
    public function getEntryDetails()
    {
        return $this->toString;
    }

    /**
     * @param $string
     */
    public static function fetchCustomImportLookup($string)
    {
            return self::find()->andWhere([self::tableName() . '.alt_name' => $string]);
    }

    /**
     * User for filtering results for Select2 element
     * @param null $q
     * @return array
     */
    public static function filter($q = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, alt_name AS text')
            ->from(self::tableName())
            ->andWhere([self::tableName() . '.deleted_at' => null])
            ->andWhere(['like', 'alt_name', $q])
            ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }

        /**
     * Get related CRUD controller's class
     * @return \taktwerk\yiiboilerplate\modules\media\controllers\MediaFileController|\yii\web\Controller
     */
    public function getControllerInstance()
    {
        if(class_exists('\taktwerk\yiiboilerplate\modules\media\controllers\MediaFileController')){
            return new \taktwerk\yiiboilerplate\modules\media\controllers\MediaFileController("media-file", \taktwerk\yiiboilerplate\modules\media\Module::getInstance());
        }
    }
}