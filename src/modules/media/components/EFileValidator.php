<?php
namespace taktwerk\yiiboilerplate\modules\media\components;

use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\web\JsExpression;
use yii\web\UploadedFile;
use yii\validators\FileValidator as FV;

/**
 * FileValidator verifies if an attribute is receiving a valid uploaded file.
 *
 * Note that you should enable `fileinfo` PHP extension.
 *
 * @property-read int $sizeLimit The size limit for uploaded files. This property is read-only.
 *
 */
class EFileValidator extends FV
{   
  /**
    * @var boolean whether to strictly check `allowedFileExtensions` if `allowedFileTypes` is mentioned
    */
    public $strictExtensionTypeCheck = false;
    
    /**
     * {@inheritdoc}
     */
    protected function validateValue($value)
    {
        if($this->strictExtensionTypeCheck==true){
            return parent::validateValue($value);
        }
        if (!$value instanceof UploadedFile || $value->error == UPLOAD_ERR_NO_FILE) {
            return [$this->uploadRequired, []];
        }
        
        switch ($value->error) {
            case UPLOAD_ERR_OK:
                if ($this->maxSize !== null && $value->size > $this->getSizeLimit()) {
                    return [
                        $this->tooBig,
                        [
                            'file' => $value->name,
                            'limit' => $this->getSizeLimit(),
                            'formattedLimit' => Yii::$app->formatter->asShortSize($this->getSizeLimit()),
                        ],
                    ];
                } elseif ($this->minSize !== null && $value->size < $this->minSize) {
                    return [
                        $this->tooSmall,
                        [
                            'file' => $value->name,
                            'limit' => $this->minSize,
                            'formattedLimit' => Yii::$app->formatter->asShortSize($this->minSize),
                        ],
                    ];
                } elseif (!empty($this->extensions) && !empty($this->mimeTypes) && $isExtInvalid = !$this->validateExtension($value) && $isMimeInvalid = !$this->validateMimeType($value)) {
                    if($isExtInvalid)
                    return [$this->wrongExtension, ['file' => $value->name, 'extensions' => implode(', ', $this->extensions)]];
                    if($isMimeInvalid)
                    return [$this->wrongMimeType, ['file' => $value->name, 'mimeTypes' => implode(', ', $this->mimeTypes)]];
                }
                
                return null;
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                return [$this->tooBig, [
                'file' => $value->name,
                'limit' => $this->getSizeLimit(),
                'formattedLimit' => Yii::$app->formatter->asShortSize($this->getSizeLimit()),
                ]];
            case UPLOAD_ERR_PARTIAL:
                Yii::warning('File was only partially uploaded: ' . $value->name, __METHOD__);
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                Yii::warning('Missing the temporary folder to store the uploaded file: ' . $value->name, __METHOD__);
                break;
            case UPLOAD_ERR_CANT_WRITE:
                Yii::warning('Failed to write the uploaded file to disk: ' . $value->name, __METHOD__);
                break;
            case UPLOAD_ERR_EXTENSION:
                Yii::warning('File upload was stopped by some PHP extension: ' . $value->name, __METHOD__);
                break;
            default:
                break;
        }
        
        return [$this->message, []];
    }
}