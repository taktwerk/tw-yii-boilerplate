<?php
namespace taktwerk\yiiboilerplate\modules\media\behaviors;

use taktwerk\yiiboilerplate\modules\media\models\MediaFile;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\StringHelper;

class MediaBehavior extends Behavior
{

    /**
     *
     * @var string a unique identifier for each model class
     */
    public $model_type = '';

    /**
     *
     * @var array owner mediafiles attributes names
     */
    public $attributes = [];

    /**
     *
     * @var array owner mediafiles attributes names
     */
    public $attributesConfig = [];
    protected $changedMediaFields = [];
    /**
     * @inheritdoc
     *
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'addModelDetails',
            ActiveRecord::EVENT_AFTER_UPDATE => 'addModelDetails',
            ActiveRecord::EVENT_BEFORE_UPDATE=>'setChangedMediaAttributes',
            ActiveRecord::EVENT_AFTER_FIND=>'populateMediaIds'
            // ActiveRecord::EVENT_BEFORE_DELETE => 'deleteOwners',
        ];
    }
    public function populateMediaIds(){
        $mediaFields = $this->attributesConfig;
        foreach($mediaFields as $field){
            $fName = $field['attribute'];
            $ids = $this->owner->mediaFilesQuery($this->model_type, $field['media_category'])
            ->select('id')->column();
            $this->owner->{$fName} = implode(',',$ids);
        }
    }
    public function setChangedMediaAttributes($event){
        $mediaFields = $this->attributesConfig;
        foreach($mediaFields as $field){
            $fName = $field['attribute'];
            $ids = $this->owner
            ->mediaFilesQuery($this->model_type, $field['media_category'])
            ->select('id')->column();
            $newIds = array_filter(explode(',', $this->owner->{$fName}));
            sort($ids);sort($newIds);
            if(array_diff($ids,$newIds) != array_diff($newIds,$ids)){
                $this->changedMediaFields[$fName] = $ids;
            }
            //Setting media ids in the related media column of the model 
            if (StringHelper::endsWith($fName, '_ids')) {
                $atr = rtrim($fName,'_ids');
                if($this->owner->hasAttribute($atr)){
                    $this->owner->{$atr} = is_array($this->owner->$fName)?implode(',', $this->owner->$fName):$this->owner->$fName;
                }
            }
        }
    }
    public function getChangedMediaAttributes(){
        return $this->changedMediaFields;
    }
    /**
     * Add model details to the Media
     */
    public function addModelDetails()
    {
        foreach ($this->attributesConfig as $config) {
            if (isset($config['attribute']) && isset($config['media_category'])) {
                $atr = $config['attribute'];
                if (is_array($this->owner->$atr)) {
                    foreach ($this->owner->$atr as $id) {
                        $model = $this->loadModel([
                            'AND',
                            ['id' => $id],
                            ['OR',
                                'created_by' => $this->owner->created_by,
                                'created_by' => $this->owner->updated_by,
                                'updated_by' => $this->owner->created_by,
                                'updated_by' => $this->owner->updated_by
                            ]
                        ]);
                        if ($model) {
                            $model->model_id = $this->owner->primaryKey;
                            $model->model_type = $this->model_type;
                            $model->media_category = $config['media_category'];
                            $model->save(false);
                        }
                    }
                } elseif (is_string($this->owner->$atr)) {
                    $ids = explode(',', $this->owner->$atr);
                    foreach ($ids as $id) {
                        $model = $this->loadModel(['AND',
                            ['id' => $id],
                            ['OR','created_by' => $this->owner->created_by,'updated_by' => $this->owner->updated_by]
                        ]);
                        if ($model) {
                            $model->model_id = $this->owner->primaryKey;
                            $model->model_type = $this->model_type;
                            $model->media_category = $config['media_category'];
                            $model->save(false);
                        }
                    }
                }
            } else {
                throw new \Exception('attribute and category must be configured in order to use Media Behaviour');
            }
        }
       $this->deleteSessionMedia();
    }
    public function deleteSessionMedia()
    {
        if (empty(\Yii::$app->session)) {
            return;
        }
        $deleteList = \Yii::$app->session->get('deletemedia');
        if (is_array($deleteList)) {
            foreach ($deleteList as $mediaId => $i) {
                $item = $this->loadModel([
                    'AND',
                    [
                        'id' => $mediaId,
                        'model_id' => $this->owner->primaryKey,
                        'model_type' => $this->model_type
                    ],
                    [
                        'OR',
                        'created_by' => $this->owner->created_by,
                        'updated_by' => $this->owner->created_by,
                        'created_by' => $this->owner->updated_by,
                        'updated_by' => $this->owner->updated_by]
                    ]);
                if ($item) {
                    $item->delete();
                    unset($deleteList[$mediaId]);
                }
            }
            if(count($deleteList)>1)
            {
                \Yii::$app->session->set('deletemedia',$deleteList);
            }else{
                \Yii::$app->session->remove('deletemedia');
            }
        }
    }
    public function mediaFilesQuery($model_type, $media_category=null){
        $q = Mediafile::find()->where([
            'model_id' => $this->owner->primaryKey,
            'model_type' => $model_type,
        ]);
        if($media_category!==null){
            $q->andWhere(['media_category' => $media_category]);
        }
        return $q;
    }
    /**
     *
     * @return \taktwerk\yiiboilerplate\modules\media\models\MediaFile[]
     */
    public function getMediaFiles($model_type, $media_category=null)
    {
        $q = $this->mediaFilesQuery($model_type,$media_category);
        return $q->all();
    }

    public function showMediaPreview($model_type, $media_category, $previewOptions = [], $fileCount = 1)
    {
        // prd($model_type,$media_category);
        $models = $this->getMediaFiles($model_type, $media_category);
        if (count($models) > 0) {
            if ($fileCount < 2) {
                return $models[0]->showFilePreview('file_name', $previewOptions);
            }
            $html = '';
            foreach ($models as $m) {
                $html .= $m->showFilePreview('file_name', $previewOptions);
            }
            return $html;
        }
        return null;
    }

    /**
     * Load model by id
     *
     * @param int $id
     * @return Mediafile
     */
    private function loadModel($id)
    {
        return Mediafile::findOne($id);
    }
}
