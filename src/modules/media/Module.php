<?php

namespace taktwerk\yiiboilerplate\modules\media;

/**
 * media module definition class
 */
class Module extends \taktwerk\yiiboilerplate\components\TwModule
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\media\controllers';
    
    /**
     * @desc contains secret key to encypt "allowedFileExtensions" in file input widget and then  decrypt in upload-file action in DefaultController of this module.
     */
    public $fileRuleSecretKey = 'rssdasdulaFoola';
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
