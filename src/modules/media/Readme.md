# BP Media module 

#### Installation

```bash
php yii migrate --migrationPath=@vendor/taktwerk/yii-boilerplate/src/modules/media/migrations
```

In common.php file 
```php
'modules' => [
...
'media' => [
            'class' => 'taktwerk\yiiboilerplate\modules\media\Module',
        ],
        ...
]
```
## Usage
##### Adding multi file upload to a model

Steps to start using the multi file upload in a model class. Let say you have *Student* model which should have multi-upload for student's marksheets and certificates.

1) Declare two public properties for each file/document type you'd like to add, in our case there are two type of files i.e Marksheet and Certificate so we'll need to declare four properties like below:

```php
public $multi_file_marksheet;
public $multi_file_marksheet_ids;

public $multi_file_certificate;
public $multi_file_certificate_ids;
```

2) Add the properties declared to the model rules, you can add them to the `safe` rule
```php
...
[
                [
                    'multi_file_marksheet',
                    'multi_file_marksheet_ids'
                    'multi_file_certificate',
                    'multi_file_certificate_ids'
                ],
                'safe'
            ],
...
```

3) Attach the `taktwerk\yiiboilerplate\modules\media\behaviors\MediaBehavior` to the student model like below
```php
 public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['mediafile'] = [
            'class' => MediaBehavior::className(),
            'model_type' =>'student', //Should be a unqiue string per model class
            'attributesConfig' => [
                [
                    'attribute' => 'multi_file_certificate_ids',
                    'media_category' => 'ceritificate' 
                ],
                [
                    'attribute' => 'multi_file_marksheet_ids',
                    'media_category' => 'marksheet'
                ]
            ]
        ];
        return $behaviors;
    }
```

4) Use the MultiFileInput widget in the active form like 
```php
//File uploader for Marksheets
echo $form->field(
                $model,
                'multi_file_marksheet',
                [
                    'selectors' => [
                        'input' => '#' .
                        Html::getInputId($model, 'multi_file_marksheet') . $owner
                    ],
                    
                ]
                )->widget(
                    \taktwerk\yiiboilerplate\modules\media\widgets\MultiFileInput::class,
                    [
                        'fileIdAttr'=>'multi_file_marksheet_ids',
                        'modelType'=>'student',
                        'mediaCategory'=>'marksheet',
                    ]
                   );

//File uploader for Certificates
echo $form->field(
                $model,
                'multi_file_certificate',
                [
                    'selectors' => [
                        'input' => '#' .
                        Html::getInputId($model, 'multi_file_certificate') . $owner
                    ],
                    
                ]
                )->widget(
                    \taktwerk\yiiboilerplate\modules\media\widgets\MultiFileInput::class,
                    [
                        'fileIdAttr'=>'multi_file_certificate_ids',
                        'modelType'=>'student',
                        'mediaCategory'=>'ceritificate',
                    ]
                   );
```

##### Fetching the Media models/files of a model
Let say we want the Student's uploaded Marksheets
we can use `getMediaFiles()` method to fetch those files, the methods accepts two parameter 1) The model type 2) The Media Category , and returns an array of `taktwerk\yiiboilerplate\modules\media\models\MediaFile` models based on _media_file_ table
e.g
` $files = $studentModel->getMediaFiles('student', 'marksheet');`
The MediaFile model contains file specific methods 
1) `getUrl()`  Returns the File Url
2) `getType()` Return s the File type e.g image,video,pdf, other



