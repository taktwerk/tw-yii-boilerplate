<?php
//Generation Date: 25-Nov-2020 06:32:13am
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\usermanager\models\User $model
 * @var boolean $useModal
 */

$this->title = Yii::t('twbp', 'User') . ' #' . (is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey) . ', ' . Yii::t('twbp', 'View') . ' - ' . $model->toString();
if(!$fromRelation) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('twbp', 'Users'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => (string)is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('twbp', 'View');
}
$basePermission = Yii::$app->controller->module->id . '_' . Yii::$app->controller->id;

Yii::$app->controller->renderCustomBlocks(Yii::$app->controller::POS_MAIN);

?>
<div class="box box-default">
	<div class="giiant-crud box-body"
		id="user-view">

		<!-- flash message -->
        <?php if (Yii::$app->session->getFlash('deleteError') !== null) : ?>
            <span class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= Yii::$app->session->getFlash('deleteError') ?>
            </span>
        <?php endif; ?>
        <?php if (!$useModal) : ?>
            <div class="clearfix crud-navigation">
			<!-- menu buttons -->
			<div class='pull-left'>
                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_update'
                    ) && $model->editable() ?
                        Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span> ' . Yii::t('twbp', 'Edit'),
                            [
                                'update',
                                'id' => $model->id,                                 'fromRelation' => $fromRelation,
                                'show_deleted'=>Yii::$app->request->get('show_deleted')
                            ],
                            ['class' => 'btn btn-info']
                        )
                    :
                        null
                    ?>
                     <?php if(method_exists($model,'getUploadFields')  && Yii::$app->getUser()->can(
                        $basePermission . '_recompress'
                    )){
                    	$fields = $model->getUploadFields();
                    	$isCompressible = false;
                    	$compressibleFileTypes = $model::$compressibleFileTypes;
                    	foreach($fields as $field){
                        	if(in_array($model->getFileType($field),$compressibleFileTypes)){
                            	$isCompressible = true;
                            	break;
                        	}
                    	}
                    if($isCompressible){	
                       echo Html::a(
                            '<span class="glyphicon glyphicon-compressed"></span> ' . Yii::t('twbp', 'Recompress'),
                            ['recompress', 'id' => $model->id, 'fromRelation' => $fromRelation],
                            ['class' => 'btn btn-warning','title'=>Yii::t('twbp', 'Adds a compression job for this item\'s files')]
                        );}
                   
                    }?>
                    
                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_create'
                    ) ?
                        Html::a(
                            '<span class="glyphicon glyphicon-copy"></span> ' . Yii::t('twbp', 'Copy'),
                            ['create', 'id' => $model->id, 'fromRelation' => $fromRelation],
                            ['class' => 'btn btn-success']
                        )
                    :
                        null
                    ?>
                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_create'
                    ) ?
                        Html::a(
                            '<span class="glyphicon glyphicon-plus"></span>
				' . Yii::t('twbp', 'New'), ['create', 'fromRelation' =>
				$fromRelation], ['class' => 'btn btn-success create-new'] ) : null
				?>
			</div>
			<div class="pull-right">
                    <?= \taktwerk\yiiboilerplate\widget\CustomActionDropdownWidget::widget(['model'=>$model]) ?>
                    <?php if(Yii::$app->controller->crudMainButtons['listall']): ?>
                    <?= Html::a(
                        '<span class="glyphicon glyphicon-list"></span> '. Yii::t('twbp', 'List {model}',
                            ['model' => \Yii::t('twbp','Users')]                        ),
                        ['index'],
                        ['class' => 'btn btn-default']
                    ) ?>
                    <?php endif; ?>
                </div>
		</div>
        <?php endif; ?>
        <?php $this->beginBlock('taktwerk\yiiboilerplate\modules\usermanager\models\User'); ?>

        <?php $viewColumns = [
                   [
            'attribute'=> 'id'
           ],
[
        'attribute' => 'identifier',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->identifier != strip_tags($model->identifier)){
                return \yii\helpers\StringHelper::truncate($model->identifier,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->identifier,500));
            }
        },
    ],
[
        'attribute' => 'username',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->username != strip_tags($model->username)){
                return \yii\helpers\StringHelper::truncate($model->username,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->username,500));
            }
        },
    ],
                'email:email',
[
        'attribute' => 'password_hash',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->password_hash != strip_tags($model->password_hash)){
                return \yii\helpers\StringHelper::truncate($model->password_hash,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->password_hash,500));
            }
        },
    ],
[
        'attribute' => 'auth_key',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->auth_key != strip_tags($model->auth_key)){
                return \yii\helpers\StringHelper::truncate($model->auth_key,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->auth_key,500));
            }
        },
    ],
                'unconfirmed_email:email',
[
        'attribute' => 'registration_ip',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->registration_ip != strip_tags($model->registration_ip)){
                return \yii\helpers\StringHelper::truncate($model->registration_ip,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->registration_ip,500));
            }
        },
    ],
[
        'attribute' => 'flags',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->flags != strip_tags($model->flags)){
                return \yii\helpers\StringHelper::truncate($model->flags,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->flags,500));
            }
        },
    ],
[
        'attribute' => 'confirmed_at',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->confirmed_at != strip_tags($model->confirmed_at)){
                return \yii\helpers\StringHelper::truncate($model->confirmed_at,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->confirmed_at,500));
            }
        },
    ],
[
        'attribute' => 'blocked_at',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->blocked_at != strip_tags($model->blocked_at)){
                return \yii\helpers\StringHelper::truncate($model->blocked_at,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->blocked_at,500));
            }
        },
    ],
[
        'attribute' => 'last_login_at',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->last_login_at != strip_tags($model->last_login_at)){
                return \yii\helpers\StringHelper::truncate($model->last_login_at,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->last_login_at,500));
            }
        },
    ],
[
        'attribute' => 'last_login_ip',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->last_login_ip != strip_tags($model->last_login_ip)){
                return \yii\helpers\StringHelper::truncate($model->last_login_ip,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->last_login_ip,500));
            }
        },
    ],
[
        'attribute' => 'auth_tf_key',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->auth_tf_key != strip_tags($model->auth_tf_key)){
                return \yii\helpers\StringHelper::truncate($model->auth_tf_key,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->auth_tf_key,500));
            }
        },
    ],
[
        'attribute' => 'auth_tf_enabled',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->auth_tf_enabled != strip_tags($model->auth_tf_enabled)){
                return \yii\helpers\StringHelper::truncate($model->auth_tf_enabled,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->auth_tf_enabled,500));
            }
        },
    ],
[
        'attribute' => 'password_changed_at',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->password_changed_at != strip_tags($model->password_changed_at)){
                return \yii\helpers\StringHelper::truncate($model->password_changed_at,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->password_changed_at,500));
            }
        },
    ],
[
        'attribute' => 'role_changed_at',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->role_changed_at != strip_tags($model->role_changed_at)){
                return \yii\helpers\StringHelper::truncate($model->role_changed_at,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->role_changed_at,500));
            }
        },
    ],
[
        'attribute' => 'fullname',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->fullname != strip_tags($model->fullname)){
                return \yii\helpers\StringHelper::truncate($model->fullname,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->fullname,500));
            }
        },
    ],
[
        'attribute' => 'gdpr_consent',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->gdpr_consent != strip_tags($model->gdpr_consent)){
                return \yii\helpers\StringHelper::truncate($model->gdpr_consent,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->gdpr_consent,500));
            }
        },
    ],
[
        'attribute' => 'gdpr_consent_date',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->gdpr_consent_date != strip_tags($model->gdpr_consent_date)){
                return \yii\helpers\StringHelper::truncate($model->gdpr_consent_date,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->gdpr_consent_date,500));
            }
        },
    ],
[
        'attribute' => 'gdpr_deleted',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->gdpr_deleted != strip_tags($model->gdpr_deleted)){
                return \yii\helpers\StringHelper::truncate($model->gdpr_deleted,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->gdpr_deleted,500));
            }
        },
    ],
[
        'attribute' => 'app_data_version',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->app_data_version != strip_tags($model->app_data_version)){
                return \yii\helpers\StringHelper::truncate($model->app_data_version,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->app_data_version,500));
            }
        },
    ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::attributeFormat
                [
                    'format' => 'html',
                    'attribute' => 'client_id',
                    'visible' => Yii::$app->getUser()->can('Authority'),
        
                    'value' => function ($model) {
                        $foreign = $model->getClient()->one();
                        if ($foreign) {
                            if (Yii::$app->getUser()->can('app_client_view') && $foreign->readable()) {
                                return Html::a($foreign->toString, [
                                    'client/view',
                                    'id' => $model->getClient()->one()->id,
                                ], ['data-pjax' => 0]);
                            }
                            return $foreign->toString;
                        }
                        return '<span class="label label-warning">?</span>';
                    },
                ],
        ];

        // view columns overwrite
        $viewColumns = Yii::$app->controller->crudColumnsOverwrite($viewColumns, 'view');

        echo DetailView::widget([
        'model' => $model,
        'attributes' => $viewColumns
        ]); ?>

        
        <hr />

        <?=  ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::deleteButton($model, "twbp", ['id' => $model->id], 'view')?>

        <?php $this->endBlock(); ?>


        
        <?php $this->beginBlock('Client Users'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsClientUsers = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('usermanager_client-user_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('usermanager_client-user_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('usermanager_client-user_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/customer/client-user' . '/' . $action;
                        $params['ClientUser'] = [
                            'user_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'client-user'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'client_id',
                    'value' => function ($model) {
                        if ($rel = $model->getClient()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'client/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                'is_default_autologin:boolean',
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\customer\models\ClientUser::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsClientUsers = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsClientUsers, 'tab'):$columnsClientUsers;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('usermanager_client-user_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Client User'),
                [
                    '/customer/client-user/create',
                    'ClientUser' => [
                        'user_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getClientUsers(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-clientusers',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsClientUsers
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Flysystem Users'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsFlysystemUsers = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('usermanager_flysystem-user_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('usermanager_flysystem-user_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('usermanager_flysystem-user_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/share/flysystem-user' . '/' . $action;
                        $params['FlysystemUser'] = [
                            'user_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'flysystem-user'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'fs_id',
                    'value' => function ($model) {
                        if ($rel = $model->getFs()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'flysystem/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'root_path',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->root_path != strip_tags($model->root_path)){
                return \yii\helpers\StringHelper::truncate($model->root_path,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->root_path,500));
            }
        },
    ],
[
        'attribute' => 'overwrite_path',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->overwrite_path != strip_tags($model->overwrite_path)){
                return \yii\helpers\StringHelper::truncate($model->overwrite_path,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->overwrite_path,500));
            }
        },
    ],
[
        'attribute' => 'read_only',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->read_only != strip_tags($model->read_only)){
                return \yii\helpers\StringHelper::truncate($model->read_only,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->read_only,500));
            }
        },
    ],
[
        'attribute' => 'overwrite_access',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->overwrite_access != strip_tags($model->overwrite_access)){
                return \yii\helpers\StringHelper::truncate($model->overwrite_access,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->overwrite_access,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\share\models\FlysystemUser::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsFlysystemUsers = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsFlysystemUsers, 'tab'):$columnsFlysystemUsers;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('usermanager_flysystem-user_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Flysystem User'),
                [
                    '/share/flysystem-user/create',
                    'FlysystemUser' => [
                        'user_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getFlysystemUsers(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-flysystemusers',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsFlysystemUsers
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Google Spreadsheets'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsGoogleSpreadsheets = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('usermanager_google-spreadsheet_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('usermanager_google-spreadsheet_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('usermanager_google-spreadsheet_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/system/google-spreadsheet' . '/' . $action;
                        $params['GoogleSpreadsheet'] = [
                            'user_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'google-spreadsheet'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'title',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->title != strip_tags($model->title)){
                return \yii\helpers\StringHelper::truncate($model->title,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->title,500));
            }
        },
    ],
                'url:url',

                [
                    'attribute' => 'type',
                    'value' => function ($model) {
                        return \Yii::t('twbp', $model->type);
                    },
                ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\models\GoogleSpreadsheet::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsGoogleSpreadsheets = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGoogleSpreadsheets, 'tab'):$columnsGoogleSpreadsheets;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('usermanager_google-spreadsheet_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Google Spreadsheet'),
                [
                    '/system/google-spreadsheet/create',
                    'GoogleSpreadsheet' => [
                        'user_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getGoogleSpreadsheets(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-googlespreadsheets',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsGoogleSpreadsheets
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Import Progresses'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsImportProgresses = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('usermanager_import-progress_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('usermanager_import-progress_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('usermanager_import-progress_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/import/import-progress' . '/' . $action;
                        $params['ImportProgress'] = [
                            'user_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'import-progress'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'model',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->model != strip_tags($model->model)){
                return \yii\helpers\StringHelper::truncate($model->model,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->model,500));
            }
        },
    ],
[
        'attribute' => 'pid',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->pid != strip_tags($model->pid)){
                return \yii\helpers\StringHelper::truncate($model->pid,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->pid,500));
            }
        },
    ],
[
        'attribute' => 'total_rows',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->total_rows != strip_tags($model->total_rows)){
                return \yii\helpers\StringHelper::truncate($model->total_rows,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->total_rows,500));
            }
        },
    ],
[
        'attribute' => 'current_row',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->current_row != strip_tags($model->current_row)){
                return \yii\helpers\StringHelper::truncate($model->current_row,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->current_row,500));
            }
        },
    ],
[
        'attribute' => 'message',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->message != strip_tags($model->message)){
                return \yii\helpers\StringHelper::truncate($model->message,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->message,500));
            }
        },
    ],

                [
                    'attribute' => 'status',
                    'value' => function ($model) {
                        return \Yii::t('twbp', $model->status);
                    },
                ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\import\models\ImportProgress::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsImportProgresses = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsImportProgresses, 'tab'):$columnsImportProgresses;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('usermanager_import-progress_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Import Progress'),
                [
                    '/import/import-progress/create',
                    'ImportProgress' => [
                        'user_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getImportProgresses(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-importprogresses',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsImportProgresses
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Notifications'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsNotifications = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('usermanager_notification_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('usermanager_notification_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('usermanager_notification_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/notification/notification' . '/' . $action;
                        $params['Notification'] = [
                            'user_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'notification'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'message',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->message != strip_tags($model->message)){
                return \yii\helpers\StringHelper::truncate($model->message,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->message,500));
            }
        },
    ],
                'read_at:datetime',
[
        'attribute' => 'link',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->link != strip_tags($model->link)){
                return \yii\helpers\StringHelper::truncate($model->link,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->link,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\notification\models\Notification::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsNotifications = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsNotifications, 'tab'):$columnsNotifications;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('usermanager_notification_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Notification'),
                [
                    '/notification/notification/create',
                    'Notification' => [
                        'user_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getNotifications(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-notifications',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsNotifications
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Pages'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsPages = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('usermanager_page_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('usermanager_page_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('usermanager_page_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/page/page' . '/' . $action;
                        $params['Page'] = [
                            'author_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'page'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'code',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->code != strip_tags($model->code)){
                return \yii\helpers\StringHelper::truncate($model->code,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->code,500));
            }
        },
    ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'language_id',
                    'value' => function ($model) {
                        if ($rel = $model->getLanguage()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'language/view',
                                    'language_id' => $rel->language_id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'title',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->title != strip_tags($model->title)){
                return \yii\helpers\StringHelper::truncate($model->title,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->title,500));
            }
        },
    ],
[
        'attribute' => 'slug',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->slug != strip_tags($model->slug)){
                return \yii\helpers\StringHelper::truncate($model->slug,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->slug,500));
            }
        },
    ],
[
        'attribute' => 'excerpt',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->excerpt != strip_tags($model->excerpt)){
                return \yii\helpers\StringHelper::truncate($model->excerpt,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->excerpt,500));
            }
        },
    ],
[
        'attribute' => 'body_html',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->body_html != strip_tags($model->body_html)){
                return \yii\helpers\StringHelper::truncate($model->body_html,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->body_html,500));
            }
        },
    ],
[
        'attribute' => 'meta_description',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->meta_description != strip_tags($model->meta_description)){
                return \yii\helpers\StringHelper::truncate($model->meta_description,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->meta_description,500));
            }
        },
    ],
[
        'attribute' => 'meta_keywords',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->meta_keywords != strip_tags($model->meta_keywords)){
                return \yii\helpers\StringHelper::truncate($model->meta_keywords,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->meta_keywords,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\page\models\Page::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsPages = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsPages, 'tab'):$columnsPages;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('usermanager_page_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Page'),
                [
                    '/page/page/create',
                    'Page' => [
                        'author_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getPages(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-pages',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsPages
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Posts'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsPosts = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('usermanager_post_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('usermanager_post_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('usermanager_post_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/post/post' . '/' . $action;
                        $params['Post'] = [
                            'author_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'post'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'post_category_id',
                    'value' => function ($model) {
                        if ($rel = $model->getPostCategory()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'post-category/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'language_id',
                    'value' => function ($model) {
                        if ($rel = $model->getLanguage()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'language/view',
                                    'language_id' => $rel->language_id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'title',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->title != strip_tags($model->title)){
                return \yii\helpers\StringHelper::truncate($model->title,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->title,500));
            }
        },
    ],
[
        'attribute' => 'body_html',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->body_html != strip_tags($model->body_html)){
                return \yii\helpers\StringHelper::truncate($model->body_html,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->body_html,500));
            }
        },
    ],
[
        'attribute' => 'excerpt',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->excerpt != strip_tags($model->excerpt)){
                return \yii\helpers\StringHelper::truncate($model->excerpt,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->excerpt,500));
            }
        },
    ],
                'is_featured:boolean',
[
        'attribute' => 'slug',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->slug != strip_tags($model->slug)){
                return \yii\helpers\StringHelper::truncate($model->slug,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->slug,500));
            }
        },
    ],
[
        'attribute' => 'seo_title',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->seo_title != strip_tags($model->seo_title)){
                return \yii\helpers\StringHelper::truncate($model->seo_title,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->seo_title,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\post\models\Post::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsPosts = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsPosts, 'tab'):$columnsPosts;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('usermanager_post_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Post'),
                [
                    '/post/post/create',
                    'Post' => [
                        'author_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getPosts(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-posts',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsPosts
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Queue Messages'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsQueueMessages = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('usermanager_queue-message_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('usermanager_queue-message_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('usermanager_queue-message_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/queue/queue-message' . '/' . $action;
                        $params['QueueMessage'] = [
                            'user_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'queue-message'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'job_id',
                    'value' => function ($model) {
                        if ($rel = $model->getJob()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'queue-job/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'message',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->message != strip_tags($model->message)){
                return \yii\helpers\StringHelper::truncate($model->message,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->message,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\queue\models\QueueMessage::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsQueueMessages = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsQueueMessages, 'tab'):$columnsQueueMessages;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('usermanager_queue-message_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Queue Message'),
                [
                    '/queue/queue-message/create',
                    'QueueMessage' => [
                        'user_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getQueueMessages(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-queuemessages',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsQueueMessages
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Social Accounts'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsSocialAccounts = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('usermanager_social-account_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('usermanager_social-account_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('usermanager_social-account_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/usermanager/social-account' . '/' . $action;
                        $params['SocialAccount'] = [
                            'user_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'social-account'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'provider',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->provider != strip_tags($model->provider)){
                return \yii\helpers\StringHelper::truncate($model->provider,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->provider,500));
            }
        },
    ],
[
        'attribute' => 'client_id',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->client_id != strip_tags($model->client_id)){
                return \yii\helpers\StringHelper::truncate($model->client_id,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->client_id,500));
            }
        },
    ],
[
        'attribute' => 'code',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->code != strip_tags($model->code)){
                return \yii\helpers\StringHelper::truncate($model->code,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->code,500));
            }
        },
    ],
                'email:email',
[
        'attribute' => 'username',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->username != strip_tags($model->username)){
                return \yii\helpers\StringHelper::truncate($model->username,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->username,500));
            }
        },
    ],
[
        'attribute' => 'data',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->data != strip_tags($model->data)){
                return \yii\helpers\StringHelper::truncate($model->data,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->data,500));
            }
        },
    ],
];
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('usermanager_social-account_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Social Account'),
                [
                    '/usermanager/social-account/create',
                    'SocialAccount' => [
                        'user_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getSocialAccounts(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-socialaccounts',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsSocialAccounts
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Tokens'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsTokens = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('usermanager_token_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('usermanager_token_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('usermanager_token_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/usermanager/token' . '/' . $action;
                        $params['Token'] = [
                            'user_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'token'
                ],
[
        'attribute' => 'code',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->code != strip_tags($model->code)){
                return \yii\helpers\StringHelper::truncate($model->code,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->code,500));
            }
        },
    ],
[
        'attribute' => 'type',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->type != strip_tags($model->type)){
                return \yii\helpers\StringHelper::truncate($model->type,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->type,500));
            }
        },
    ],
];
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('usermanager_token_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Token'),
                [
                    '/usermanager/token/create',
                    'Token' => [
                        'user_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getTokens(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-tokens',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsTokens
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('User Auth Codes'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsUserAuthCodes = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('usermanager_user-auth-code_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('usermanager_user-auth-code_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('usermanager_user-auth-code_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/user/user-auth-code' . '/' . $action;
                        $params['UserAuthCode'] = [
                            'user_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'user-auth-code'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'code',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->code != strip_tags($model->code)){
                return \yii\helpers\StringHelper::truncate($model->code,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->code,500));
            }
        },
    ],
[
        'attribute' => 'status',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->status != strip_tags($model->status)){
                return \yii\helpers\StringHelper::truncate($model->status,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->status,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\user\models\UserAuthCode::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsUserAuthCodes = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsUserAuthCodes, 'tab'):$columnsUserAuthCodes;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('usermanager_user-auth-code_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','User Auth Code'),
                [
                    '/user/user-auth-code/create',
                    'UserAuthCode' => [
                        'user_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getUserAuthCodes(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-userauthcodes',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsUserAuthCodes
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('User Parameters'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsUserParameters = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('usermanager_user-parameter_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('usermanager_user-parameter_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('usermanager_user-parameter_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/userParameter/user-parameter' . '/' . $action;
                        $params['UserParameter'] = [
                            'user_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'user-parameter'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'key',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->key != strip_tags($model->key)){
                return \yii\helpers\StringHelper::truncate($model->key,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->key,500));
            }
        },
    ],
[
        'attribute' => 'value',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->value != strip_tags($model->value)){
                return \yii\helpers\StringHelper::truncate($model->value,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->value,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\userParameter\models\UserParameter::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsUserParameters = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsUserParameters, 'tab'):$columnsUserParameters;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('usermanager_user-parameter_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','User Parameter'),
                [
                    '/userParameter/user-parameter/create',
                    'UserParameter' => [
                        'user_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getUserParameters(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-userparameters',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsUserParameters
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('User Tw Datas'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsUserTwDatas = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('usermanager_user-tw-data_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('usermanager_user-tw-data_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('usermanager_user-tw-data_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/user/user-tw-data' . '/' . $action;
                        $params['UserTwData'] = [
                            'user_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'user-tw-data'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'profile_id',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
        if($model->hasAttribute('profile_id')){
            if($model->profile_id != strip_tags($model->profile_id)){
                return \yii\helpers\StringHelper::truncate($model->profile_id,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->profile_id,500));
            }
        }
        },
    ],
[
        'attribute' => 'login_redirect',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->login_redirect != strip_tags($model->login_redirect)){
                return \yii\helpers\StringHelper::truncate($model->login_redirect,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->login_redirect,500));
            }
        },
    ],
];
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('usermanager_user-tw-data_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','User Tw Data'),
                [
                    '/user/user-tw-data/create',
                    'UserTwData' => [
                        'user_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getUserTwDatas(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-usertwdatas',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsUserTwDatas
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('User Ui Datas'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsUserUiDatas = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('usermanager_user-ui-data_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('usermanager_user-ui-data_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('usermanager_user-ui-data_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/system/user-ui-data' . '/' . $action;
                        $params['UserUiData'] = [
                            'user_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'user-ui-data'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'key',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->key != strip_tags($model->key)){
                return \yii\helpers\StringHelper::truncate($model->key,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->key,500));
            }
        },
    ],
[
        'attribute' => 'param',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->param != strip_tags($model->param)){
                return \yii\helpers\StringHelper::truncate($model->param,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->param,500));
            }
        },
    ],
[
        'attribute' => 'value',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->value != strip_tags($model->value)){
                return \yii\helpers\StringHelper::truncate($model->value,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->value,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\models\UserUiData::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsUserUiDatas = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsUserUiDatas, 'tab'):$columnsUserUiDatas;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('usermanager_user-ui-data_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','User Ui Data'),
                [
                    '/system/user-ui-data/create',
                    'UserUiData' => [
                        'user_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getUserUiDatas(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-useruidatas',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsUserUiDatas
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Workflow Steps'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsWorkflowSteps = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('usermanager_workflow-step_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('usermanager_workflow-step_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('usermanager_workflow-step_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/workflow/workflow-step' . '/' . $action;
                        $params['WorkflowStep'] = [
                            'user_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'workflow-step'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'workflow_id',
                    'value' => function ($model) {
                        if ($rel = $model->getWorkflow()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'workflow/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],

                [
                    'attribute' => 'type',
                    'value' => function ($model) {
                        return \Yii::t('twbp', $model->type);
                    },
                ],
[
        'attribute' => 'role',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->role != strip_tags($model->role)){
                return \yii\helpers\StringHelper::truncate($model->role,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->role,500));
            }
        },
    ],
                'is_first:boolean',
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsWorkflowSteps = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsWorkflowSteps, 'tab'):$columnsWorkflowSteps;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('usermanager_workflow-step_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Workflow Step'),
                [
                    '/workflow/workflow-step/create',
                    'WorkflowStep' => [
                        'user_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getWorkflowSteps(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-workflowsteps',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsWorkflowSteps
        ])?>
        </div>
                <?php $this->endBlock() ?>

<?php /* MAPPED MODELS RELATIONS - START*/?>
<?php 
$extraTabItems = [];
foreach(\Yii::$app->modelNewRelationMapper->newRelationsMapping as $modelClass=>$relations){
    if(is_a($model,$modelClass)){
        foreach($relations as $relName => $relation){
            $relationKey = array_key_first($relation[1]);
            $foreignKey= $relation[1][$relationKey];
            $relClass = $relation[0];
            $baseName = \yii\helpers\StringHelper::basename($relClass);
            $cntrler = Inflector::camel2id($baseName, '-', true);
            $pluralName= Inflector::camel2words(Inflector::pluralize($baseName));
            $singularName = Inflector::camel2words(Inflector::singularize($baseName));
            $basePerm =  \Yii::$app->controller->module->id.'_'.$cntrler;
            $this->beginBlock($pluralName);
?>
<?php 
$relatedModelObject = Yii::createObject($relClass::className());
$relationController = $relatedModelObject->getControllerInstance();
//TODO CrudHelper::getColumnFormat('\app\modules\guide\models\Guide', 'id');
?>
                    <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
       	<div class="clearfix"></div>
        <div>
        <?php $relMapCols = [[
                    'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can($basePerm.'_view') ? '{view} ' : '') . (Yii::$app->getUser()->can($basePerm.'_update') ? '{update} ' : '') . (Yii::$app->getUser()->can($basePerm.'_delete') ? '{delete} ' : ''),
            'urlCreator' => function ($action, $relation_model, $key, $index) use($model,$relationKey,$foreignKey,$baseName,$relatedBaseUrl) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = $relatedBaseUrl . $action;
                        
                        $params[$baseName] = [
                            $relationKey => $model->{$foreignKey}
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) { 
                             return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => $cntrler
                ],
                
        ];

        $relMapCols =array_merge($relMapCols,array_slice(array_keys($relatedModelObject->attributes),0,5));
        
        $relMapCols = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($relMapCols, 'tab'):$relMapCols;
        
        echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can($basePerm.'_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp',$singularName),
                [
                    '/'.$relationController->module->id.'/'.$cntrler.'/create',
                    $baseName => [
                        $relationKey => $model->{$foreignKey}
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[
                'class'=>'grid-outer-div'
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->{'get'.$relName}(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-'.$relName,
                    ]
                ]
                ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            'columns' =>$relMapCols
        ])?>
        </div>
                <?php $this->endBlock();
                $extraTabItems[] = [
                    'content' => $this->blocks[$pluralName],
                    'label' => '<small>' .
                    \Yii::t('twbp', $pluralName) .
                    '&nbsp;<span class="badge badge-default">' .
                    $model->{'get'.$relName}()->count() .
                    '</span></small>',
                    'active' => false,
                    'visible' =>
                    Yii::$app->user->can('x_'.$basePerm.'_see') && Yii::$app->controller->crudRelations(ucfirst($relName)),
                    ];
?>

<?php 
        }
    }
}
?>

                
<?php /* MAPPED MODELS RELATIONS - END*/ ?>
        <?= Tabs::widget(
            [
                'id' => 'relation-tabs',
                'encodeLabels' => false,
                'items' =>  array_filter(array_merge([
                    [
                        'label' => '<b class=""># ' . $model->id . '</b>',
                        'content' => $this->blocks['taktwerk\yiiboilerplate\modules\usermanager\models\User'],
                        'active' => true,
                    ],
        (\Yii::$app->hasModule('customer'))?[
                        'content' => $this->blocks['Client Users'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Client Users') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getClientUsers()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_usermanager_client-user_see') && Yii::$app->controller->crudRelations('ClientUsers'),
                    ]:null,
        (\Yii::$app->hasModule('share'))?[
                        'content' => $this->blocks['Flysystem Users'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Flysystem Users') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getFlysystemUsers()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_usermanager_flysystem-user_see') && Yii::$app->controller->crudRelations('FlysystemUsers'),
                    ]:null,
        (\Yii::$app->hasModule('system'))?[
                        'content' => $this->blocks['Google Spreadsheets'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Google Spreadsheets') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getGoogleSpreadsheets()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_usermanager_google-spreadsheet_see') && Yii::$app->controller->crudRelations('GoogleSpreadsheets'),
                    ]:null,
        (\Yii::$app->hasModule('import'))?[
                        'content' => $this->blocks['Import Progresses'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Import Progresses') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getImportProgresses()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_usermanager_import-progress_see') && Yii::$app->controller->crudRelations('ImportProgresses'),
                    ]:null,
        (\Yii::$app->hasModule('notification'))?[
                        'content' => $this->blocks['Notifications'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Notifications') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getNotifications()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_usermanager_notification_see') && Yii::$app->controller->crudRelations('Notifications'),
                    ]:null,
        (\Yii::$app->hasModule('page'))?[
                        'content' => $this->blocks['Pages'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Pages') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getPages()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_usermanager_page_see') && Yii::$app->controller->crudRelations('Pages'),
                    ]:null,
        (\Yii::$app->hasModule('post'))?[
                        'content' => $this->blocks['Posts'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Posts') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getPosts()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_usermanager_post_see') && Yii::$app->controller->crudRelations('Posts'),
                    ]:null,
        (\Yii::$app->hasModule('queue'))?[
                        'content' => $this->blocks['Queue Messages'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Queue Messages') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getQueueMessages()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_usermanager_queue-message_see') && Yii::$app->controller->crudRelations('QueueMessages'),
                    ]:null,
        (\Yii::$app->hasModule('usermanager'))?[
                        'content' => $this->blocks['Social Accounts'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Social Accounts') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getSocialAccounts()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_usermanager_social-account_see') && Yii::$app->controller->crudRelations('SocialAccounts'),
                    ]:null,
        (\Yii::$app->hasModule('usermanager'))?[
                        'content' => $this->blocks['Tokens'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Tokens') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getTokens()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_usermanager_token_see') && Yii::$app->controller->crudRelations('Tokens'),
                    ]:null,
        (\Yii::$app->hasModule('user'))?[
                        'content' => $this->blocks['User Auth Codes'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'User Auth Codes') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getUserAuthCodes()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_usermanager_user-auth-code_see') && Yii::$app->controller->crudRelations('UserAuthCodes'),
                    ]:null,
        (\Yii::$app->hasModule('userParameter'))?[
                        'content' => $this->blocks['User Parameters'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'User Parameters') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getUserParameters()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_usermanager_user-parameter_see') && Yii::$app->controller->crudRelations('UserParameters'),
                    ]:null,
        (\Yii::$app->hasModule('user'))?[
                        'content' => $this->blocks['User Tw Datas'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'User Tw Datas') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getUserTwDatas()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_usermanager_user-tw-data_see') && Yii::$app->controller->crudRelations('UserTwDatas'),
                    ]:null,
        (\Yii::$app->hasModule('system'))?[
                        'content' => $this->blocks['User Ui Datas'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'User Ui Datas') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getUserUiDatas()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_usermanager_user-ui-data_see') && Yii::$app->controller->crudRelations('UserUiDatas'),
                    ]:null,
        (\Yii::$app->hasModule('workflow'))?[
                        'content' => $this->blocks['Workflow Steps'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Workflow Steps') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getWorkflowSteps()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_usermanager_workflow-step_see') && Yii::$app->controller->crudRelations('WorkflowSteps'),
                    ]:null,
                    [
                        'content' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\HistoryTab::class)::widget(['model' => $model]),
                        'label' => '<small>' .
                            \Yii::t('twbp', 'History') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getHistory()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('Administrator'),
                    ],
                ]
                ,$extraTabItems))
            ]
        );
        ?>
        <?= ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\RecordHistory::class)::widget(['model' => $model]) ?>
    </div>
</div>

<?php ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
$js = <<<JS
$(document).ready(function() {
    var hash=document.location.hash;
	var prefix="tab_" ;
    if (hash) {
        $('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
    } 
    
    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash=e.target.hash.replace(	"#", "#" + prefix);
    });
});
JS;

$this->registerJs($js);

Yii::$app->controller->renderCustomBlocks(Yii::$app->controller::POS_END);