<?php
//Generation Date: 25-Nov-2020 06:32:12am
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\usermanager\models\search\User $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'identifier') ?>

        <?= $form->field($model, 'username') ?>

        <?= $form->field($model, 'email') ?>

        <?= $form->field($model, 'password_hash') ?>

        <?php // echo $form->field($model, 'auth_key') ?>

        <?php // echo $form->field($model, 'unconfirmed_email') ?>

        <?php // echo $form->field($model, 'registration_ip') ?>

        <?php // echo $form->field($model, 'flags') ?>

        <?php // echo $form->field($model, 'confirmed_at') ?>

        <?php // echo $form->field($model, 'blocked_at') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'last_login_at') ?>

        <?php // echo $form->field($model, 'last_login_ip') ?>

        <?php // echo $form->field($model, 'auth_tf_key') ?>

        <?php // echo $form->field($model, 'auth_tf_enabled') ?>

        <?php // echo $form->field($model, 'password_changed_at') ?>

        <?php // echo $form->field($model, 'role_changed_at') ?>

        <?php // echo $form->field($model, 'fullname') ?>

        <?php // echo $form->field($model, 'gdpr_consent') ?>

        <?php // echo $form->field($model, 'gdpr_consent_date') ?>

        <?php // echo $form->field($model, 'gdpr_deleted') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'app_data_version') ?>

        <?php // echo $form->field($model, 'client_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
