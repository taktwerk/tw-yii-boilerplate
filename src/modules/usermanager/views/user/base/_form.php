<?php
//Generation Date: 25-Nov-2020 06:32:12am
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\widget\form\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\components\ClassDispenser;
/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\usermanager\models\User $model
 * @var taktwerk\yiiboilerplate\widget\form\ActiveForm $form
 * @var boolean $useModal
 * @var boolean $multiple
 * @var array $pk
 * @var array $show
 * @var string $action
 * @var string $owner
 * @var array $languageCodes
 * @var boolean $relatedForm to know if this view is called from ajax related-form action. Since this is passing from
 * select2 (owner) it is always inherit from main form
 * @var string $relatedType type of related form, like above always passing from main form
 * @var string $owner string that representing id of select2 from which related-form action been called. Since we in
 * each new form appending "_related" for next opened form, it is always unique. In main form it is always ID without
 * anything appended
 */

$owner = $relatedForm ? '_' . $owner . '_related' : '';
$relatedTypeForm = Yii::$app->request->get('relatedType')?:$relatedTypeForm;

if (!isset($multiple)) {
$multiple = false;
}

if (!isset($relatedForm)) {
$relatedForm = false;
}

$tableHint = (!empty(taktwerk\yiiboilerplate\modules\usermanager\models\User::tableHint()) ? '<div class="table-hint">' . taktwerk\yiiboilerplate\modules\usermanager\models\User::tableHint() . '</div><hr />' : '<br />');

if (!Yii::$app->getUser()->can('Authority')) {
    $clients = [];
    $clientUsers = \taktwerk\yiiboilerplate\modules\customer\models\ClientUser::findAll(['user_id' => Yii::$app->getUser()->id]);
    foreach ($clientUsers as $client) {
        $model->client_id = $client->client_id;
        continue;
    }
    $show['client_id'] = false;
}
?>
<div class="user-form">
        <?php  $formId = 'User' . ($ajax || $useModal ? '_ajax_' . $owner : ''); ?>    
    <?php $form = ActiveForm::begin([
        'id' => $formId,
        'layout' => 'default',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'action' => $useModal ? $action : '',
        'options' => [
        'name' => 'User',
            ],
    ]); ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>
        <?php echo $tableHint; ?>

        <?php if ($multiple) : ?>
            <?=Html::hiddenInput('update-multiple', true)?>
            <?php foreach ($pk as $id) :?>
                <?=Html::hiddenInput('pk[]', $id)?>
            <?php endforeach;?>
        <?php endif;?>

        <?php $fieldColumns = [
                
            'identifier' => 
            $form->field(
                $model,
                'identifier',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'identifier') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('identifier'),
                            'id' => Html::getInputId($model, 'identifier') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('identifier')),
            'username' => 
            $form->field(
                $model,
                'username',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'username') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('username'),
                            'id' => Html::getInputId($model, 'username') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('username')),
            'email' => 
            $form->field(
                $model,
                'email',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'email') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'type' => 'email',
                            'id' => Html::getInputId($model, 'email') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('email')),
            'password_hash' => 
            $form->field(
                $model,
                'password_hash',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'password_hash') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('password_hash'),
                            'id' => Html::getInputId($model, 'password_hash') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('password_hash')),
            'auth_key' => 
            $form->field(
                $model,
                'auth_key',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'auth_key') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('auth_key'),
                            'id' => Html::getInputId($model, 'auth_key') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('auth_key')),
            'unconfirmed_email' => 
            $form->field(
                $model,
                'unconfirmed_email',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'unconfirmed_email') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'type' => 'email',
                            'id' => Html::getInputId($model, 'unconfirmed_email') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('unconfirmed_email')),
            'registration_ip' => 
            $form->field(
                $model,
                'registration_ip',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'registration_ip') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('registration_ip'),
                            'id' => Html::getInputId($model, 'registration_ip') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('registration_ip')),
            'flags' => 
            $form->field(
                $model,
                'flags',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'flags') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'type' => 'number',
                            'step' => '1',
                            'placeholder' => $model->getAttributePlaceholder('flags'),
                            
                            'id' => Html::getInputId($model, 'flags') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('flags')),
            'confirmed_at' => 
            $form->field($model, 'confirmed_at')->widget(\kartik\datecontrol\DateControl::classname(), [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'ajaxConversion' => true,
                'options' => [
                    'type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_APPEND,
                    'pickerButton' => ['icon' => 'time'],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true,
                        'class' => 'form-control'
                    ]
                ],
            ]),
            'blocked_at' => 
            $form->field($model, 'blocked_at')->widget(\kartik\datecontrol\DateControl::classname(), [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'ajaxConversion' => true,
                'options' => [
                    'type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_APPEND,
                    'pickerButton' => ['icon' => 'time'],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true,
                        'class' => 'form-control'
                    ]
                ],
            ]),
            'last_login_at' => 
            $form->field($model, 'last_login_at')->widget(\kartik\datecontrol\DateControl::classname(), [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'ajaxConversion' => true,
                'options' => [
                    'type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_APPEND,
                    'pickerButton' => ['icon' => 'time'],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true,
                        'class' => 'form-control'
                    ]
                ],
            ]),
            'last_login_ip' => 
            $form->field(
                $model,
                'last_login_ip',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'last_login_ip') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('last_login_ip'),
                            'id' => Html::getInputId($model, 'last_login_ip') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('last_login_ip')),
            'auth_tf_key' => 
            $form->field(
                $model,
                'auth_tf_key',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'auth_tf_key') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('auth_tf_key'),
                            'id' => Html::getInputId($model, 'auth_tf_key') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('auth_tf_key')),
            'auth_tf_enabled' => 
            $form->field(
                $model,
                'auth_tf_enabled',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'auth_tf_enabled') . $owner
                    ]
                ]
            )
                     ->checkbox(
                         [
                             'id' => Html::getInputId($model, 'auth_tf_enabled') . $owner
                         ]
                     )
                    ->hint($model->getAttributeHint('auth_tf_enabled')),
            'password_changed_at' => 
            $form->field($model, 'password_changed_at')->widget(\kartik\datecontrol\DateControl::classname(), [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'ajaxConversion' => true,
                'options' => [
                    'type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_APPEND,
                    'pickerButton' => ['icon' => 'time'],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true,
                        'class' => 'form-control'
                    ]
                ],
            ]),
            'role_changed_at' => 
            $form->field($model, 'role_changed_at')->widget(\kartik\datecontrol\DateControl::classname(), [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'ajaxConversion' => true,
                'options' => [
                    'type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_APPEND,
                    'pickerButton' => ['icon' => 'time'],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true,
                        'class' => 'form-control'
                    ]
                ],
            ]),
            'fullname' => 
            $form->field(
                $model,
                'fullname',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'fullname') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('fullname'),
                            'id' => Html::getInputId($model, 'fullname') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('fullname')),
            'gdpr_consent' => 
            $form->field(
                $model,
                'gdpr_consent',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'gdpr_consent') . $owner
                    ]
                ]
            )
                     ->checkbox(
                         [
                             'id' => Html::getInputId($model, 'gdpr_consent') . $owner
                         ]
                     )
                    ->hint($model->getAttributeHint('gdpr_consent')),
            'gdpr_consent_date' => 
            $form->field($model, 'gdpr_consent_date')->widget(\kartik\datecontrol\DateControl::classname(), [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                'options' => [
                    'type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND,
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true,
                        'class' => 'form-control'
                    ]
                ],
            ]),
            'gdpr_deleted' => 
            $form->field(
                $model,
                'gdpr_deleted',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'gdpr_deleted') . $owner
                    ]
                ]
            )
                     ->checkbox(
                         [
                             'id' => Html::getInputId($model, 'gdpr_deleted') . $owner
                         ]
                     )
                    ->hint($model->getAttributeHint('gdpr_deleted')),
            'app_data_version' => 
            $form->field(
                $model,
                'app_data_version',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'app_data_version') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'type' => 'number',
                            'step' => '1',
                            'placeholder' => $model->getAttributePlaceholder('app_data_version'),
                            
                            'id' => Html::getInputId($model, 'app_data_version') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('app_data_version')),
            'client_id' => Yii::$app->getUser()->can('Authority')?
            $form->field(
                $model,
                'client_id'
            )
                    ->widget(
                        Select2::class,
                        [
                            'data' => taktwerk\yiiboilerplate\modules\customer\models\Client::find()->count() > 50 ? null : ArrayHelper::map(taktwerk\yiiboilerplate\modules\customer\models\Client::find()->all(), 'id', 'toString'),
                                    'initValueText' => taktwerk\yiiboilerplate\modules\customer\models\Client::find()->count() > 50 ? \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\modules\customer\models\Client::find()->andWhere(['id' => $model->client_id])->all(), 'id', 'toString') : '',
                            'options' => [
                                'placeholder' => Yii::t('twbp', 'Select a value...'),
                                'id' => 'client_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                (taktwerk\yiiboilerplate\modules\customer\models\Client::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                                (taktwerk\yiiboilerplate\modules\customer\models\Client::find()->count() > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'Client\'
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
                                        Url::toRoute('/customer/client/entry-details?id=', true) .
                                        "' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#client_id_well').html(json.data);
                                                //$('#client_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }",
                            ],
                            'addon' => (Yii::$app->getUser()->can('modules_client_create') && (!$relatedForm || ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL && !$useModal))) ? [
                                'append' => [
                                    'content' => [
                                        ClassDispenser::getMappedClass(RelatedForms::class)::widget([
                                            'relatedController' => '/customer/client',
                                            'type' => $relatedTypeForm,
                                            'selector' => 'client_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'primaryKey' => 'id',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    ) . '
                    
                
                <div id="client_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' . 
                    (taktwerk\yiiboilerplate\modules\customer\models\Client::findOne(['id' => $model->client_id])
                        ->entryDetails == '' ?
                    ' display:none;' :
                    '') . '
                    ">' . 
                    (taktwerk\yiiboilerplate\modules\customer\models\Client::findOne(['id' => $model->client_id])->entryDetails != '' ?
                    taktwerk\yiiboilerplate\modules\customer\models\Client::findOne(['id' => $model->client_id])->entryDetails :
                    '') . ' 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>':
HTML::activeHiddenInput(
                $model,
                'client_id',
                [
                    'id' => Html::getInputId($model, 'client_id') . $owner,
                    
                ]
            ),]; ?>        <div class='clearfix'></div>
<?php $twoColumnsForm = true; ?>
<?php 
// form fields overwrite
$fieldColumns = Yii::$app->controller->crudColumnsOverwrite($fieldColumns, 'form', $model, $form, $owner);

foreach($fieldColumns as $attribute => $fieldColumn) {
            if (count($model->primaryKey())>1 && in_array($attribute, $model->primaryKey()) && $model->checkIfHidden($attribute)) {
            echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
            echo $fieldColumn;
            echo $twoColumnsForm ? '</div>' : '';
        } elseif ($model->checkIfHidden($attribute)) {
            echo $fieldColumn;
        } elseif (!$multiple || ($multiple && isset($show[$attribute]))) {
            if ((isset($show[$attribute]) && $show[$attribute]) || !isset($show[$attribute])) {
                echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
                echo $fieldColumn;
                echo $twoColumnsForm ? '</div>' : '';
            } else {
                echo $fieldColumn;
            }
        }
                
} ?><div class='clearfix'></div>

                <?php $this->endBlock(); ?>
        
        <?= ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_TAB) ?
            Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                    'label' => Yii::t('twbp', 'User'),
                    'content' => $this->blocks['main'],
                    'active' => true,
                ],
                ]
            ])
            : $this->blocks['main']
        ?>        
        <div class="col-md-12">
        <hr/>
        
        </div>
        
        <div class="clearfix"></div>
        <?php echo $form->errorSummary($model); ?>
        <?php echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::formButtons($model, $useModal, $relatedForm, $multiple, "twbp", ['id' => $model->id]);?>
        <?php ClassDispenser::getMappedClass(ActiveForm::class)::end(); ?>
        <?= ($relatedForm && $relatedType == ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL) ?
        '<div class="clearfix"></div>' :
        ''
        ?>
        <div class="clearfix"></div>
    </div>
</div>

<?php
if ($useModal) {
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::class)::register($this);
}
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
if(!isset($useDependentFieldsJs) || $useDependentFieldsJs==true){
$this->render('@taktwerk-views/_dependent-fields', ['model' => $model,'formId'=>$formId]);
}
?>