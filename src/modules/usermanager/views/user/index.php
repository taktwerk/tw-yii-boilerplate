<?php

use yii\bootstrap\Modal;

/**@var $this \yii\web\View*/
require(__DIR__ . DIRECTORY_SEPARATOR . 'base' . DIRECTORY_SEPARATOR . 'index.php');

Modal::begin([
    'size' => Modal::SIZE_DEFAULT,
    'id' => 'bulkEmail',
    'header' => Yii::t('twbp','Bulk Email'),
    'clientOptions' => [
        'backdrop' => 'static',
    ],
]);
?>
    <div id="email-list" style="white-space: pre-line;word-wrap: break-word;"></div><br>
    <a class="btn btn-primary" id="email-list-btn" style="margin-top:10px;">
        <i class="fa fa-envelope" aria-hidden="true"></i> <?= Yii::t('twbp', 'Open mail application')?>
    </a>
    <button class="btn btn-primary" id="copy" style="margin-top:10px;">
        <i class="fa fa-clipboard copy-icon" aria-hidden="true"></i> <?= Yii::t('twbp', 'Copy')?>
    </button>
<?php
Modal::end();?>
    <script type="application/javascript">
        $("#copy").on('click', function (event) {
            var temp = $("<input>");
            $("#email-list").append(temp);
            temp.val($("#email-list-btn").attr('data-href')).select();
            temp.focus();
            document.execCommand("copy");
            temp.remove();
            $('.copy-icon').addClass('copied');
            setTimeout(function() {
                $('.copy-icon').removeClass('copied');
            }, 1500);

        });
    </script>
<?php
$js = <<<JS
function blockMultiple(elm){
    var element = $(elm),
        keys=$("#" + gridViewKey + "-grid").yiiGridView('getSelectedRows'),
        url = element.attr('data-url');
    $.post(url,{pk : keys},
        function (){
            if (useModal != true) {
                $.pjax.reload({container: "#" + gridViewKey + "-pjax-container"});
            }
        }
    );
}
function emailMultiple(elm){
    var element = $(elm),
        keys=$("#" + gridViewKey + "-grid").yiiGridView('getSelectedRows'),
        url = element.attr('data-url');
    if(keys.length>=1){
        $.post(url,{pk : keys},
            function (data){
                if (useModal != true) {
                    $('#bulkEmail').modal('show');
                    $('#bulkEmail').find('#email-list').html(data);
                    $('#bulkEmail').find('#email-list-btn').attr("href","mailto:"+data);
                    $('#bulkEmail').find('#email-list-btn').attr("data-href",data);
                }
            }
        );
    }
    
    $(document).on("pjax:success", function(event){
        $.each(keys, function (key, val) {
            var input = $('.kv-row-checkbox[value='+val+']');
            input.attr("checked", "true");
        });
        
        if($('.kv-row-checkbox[name="selection[]"]').length === keys.length){
            $('input[name="selection_all"]').attr("checked", "true");
        }
    });
}

JS;

$this->registerJs($js,\yii\web\View::POS_HEAD);
