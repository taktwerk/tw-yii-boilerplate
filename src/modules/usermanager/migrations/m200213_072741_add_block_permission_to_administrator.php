<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200213_072741_add_block_permission_to_administrator extends TwMigration
{
    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->createPermission('usermanager_user_block', 'User Manager Block Permission', ['Administrator']);
        $this->createPermission('usermanager_user_block_multiple', 'User Manager Block Multiple Permission', ['Administrator']);
    }

    public function down()
    {
        echo "m200213_072741_add_block_permission_to_administrator cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
