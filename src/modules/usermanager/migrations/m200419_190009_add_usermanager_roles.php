<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200419_190009_add_usermanager_roles extends TwMigration
{
    public function up()
    {

        // Create roles
        $this->createRole('UsermanagerViewer');
        $this->createRole('UsermanagerAdmin', 'UsermanagerViewer');

        // Init the permission cascade for see
        $this->addSeeControllerPermission('usermanager_user', 'UsermanagerViewer');
        // Administration permission assignment
        $this->addAdminControllerPermission('usermanager_user','UsermanagerAdmin');
    }

    public function down()
    {
        echo "m200419_190009_add_usermanager_roles cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
