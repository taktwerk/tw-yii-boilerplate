<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200207_120411_add_fields_to_user extends TwMigration
{
    /**
     * @return bool|void
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'deleted_at', $this->dateTime()->null());
        $this->addColumn('{{%user}}', 'deleted_by', $this->integer()->null());
        $this->addColumn('{{%user}}', 'created_by', $this->integer()->null());
        $this->addColumn('{{%user}}', 'updated_by', $this->integer()->null());
    }

    public function safeDown()
    {

    }
}
