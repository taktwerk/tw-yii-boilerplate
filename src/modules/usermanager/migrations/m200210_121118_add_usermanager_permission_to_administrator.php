<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200210_121118_add_usermanager_permission_to_administrator extends TwMigration
{
    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->createPermission('usermanager_user', 'User Manager Permission', ['Administrator']);
    }

    public function down()
    {
        $this->removePermission('usermanager_user', ['Administrator']);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
