<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200220_101118_add_usermanager_permission_to_administrator_fix extends TwMigration
{
    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        // Init the permission cascade
        $this->createCrudControllerPermissions('usermanager_user');
        // Administration permission assignment
        $this->addAdminControllerPermission('usermanager_user','Administrator');
    }

    public function down()
    {
        echo "m200220_101118_add_usermanager_permission_to_administrator_fix cannot be reverted.\n";
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
