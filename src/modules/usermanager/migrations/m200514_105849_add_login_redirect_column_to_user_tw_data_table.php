<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;
use Yii;

/**
 * Handles adding columns to table `{{%user_tw_data}}`.
 */
class m200514_105849_add_login_redirect_column_to_user_tw_data_table extends TwMigration
{
    public function up()
    {
        $isExistColumn = (bool) $this->getDb()
            ->getSchema()
            ->getTableSchema('user_tw_data')
            ->getColumn('login_redirect');
        if(!$isExistColumn) {
            $this->addColumn('{{%user_tw_data}}', 'login_redirect', $this->string(45)->null());
        }

    }

    public function down()
    {
        $this->dropColumn('{{%user_tw_data}}', 'login_redirect');
    }
}
