<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m210528_045251_remove_usermanager_from_administrator extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $admin = $auth->getRole('Administrator');
        $usermanagerermList = [
            $auth->getPermission('usermanager_user'),
            $auth->getPermission('usermanager_user_block'),
            $auth->getPermission('usermanager_user_index'),
            $auth->getPermission('x_usermanager_user_admin'),
            $auth->getPermission('usermanager_user_block_multiple'),
            $auth->getPermission('usermanager_user_view'),
            $auth->getPermission('usermanager_user_delete'),
            $auth->getPermission('usermanager_user_update'),
        ];
        if($admin){
            foreach($usermanagerermList as $perm){
                if($perm){
                    $auth->removeChild($admin, $perm);
                }
            }
        }
    }

    public function down()
    {
        echo "m210528_045251_remove_usermanager_from_administrator cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
