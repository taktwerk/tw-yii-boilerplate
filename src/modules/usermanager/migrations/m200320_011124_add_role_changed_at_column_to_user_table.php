<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%user}}`.
 */
class m200320_011124_add_role_changed_at_column_to_user_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            '{{%user}}',
            'role_changed_at',
            SCHEMA::TYPE_INTEGER . '(11) AFTER password_changed_at'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'role_changed_at');
    }
}
