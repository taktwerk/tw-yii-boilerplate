<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m210322_055859_user_block_perm_to_usermanageradmin extends TwMigration
{
    public function up()
    {
        $this->createPermission('usermanager_user_block', 'User Manager Block Permission', ['UsermanagerAdmin']);
        $this->createPermission('usermanager_user_block_multiple', 'User Manager Block Multiple Permission', ['UsermanagerAdmin']);
    }

    public function down()
    {
        echo "m210322_055859_user_block_perm_to_usermanageradmin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
