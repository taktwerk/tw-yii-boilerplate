<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200219_101315_add_client_id_to_user extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'client_id', $this->integer()->null());
    }

    public function down()
    {
        echo "m200219_101315_add_client_id_to_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
