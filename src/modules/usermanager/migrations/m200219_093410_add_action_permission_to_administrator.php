<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200219_093410_add_action_permission_to_administrator extends TwMigration
{
    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->createPermission('usermanager_user_view', 'User Manager Permission', ['Administrator']);
        $this->createPermission('usermanager_user_update', 'User Manager Permission', ['Administrator']);
        $this->createPermission('usermanager_user_delete', 'User Manager Permission', ['Administrator']);
    }

    public function down()
    {
        echo "m200219_093410_add_action_permission_to_administrator cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
