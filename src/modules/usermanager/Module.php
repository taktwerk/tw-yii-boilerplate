<?php

namespace taktwerk\yiiboilerplate\modules\usermanager;

/**
 * usermanager module definition class
 */
class Module extends \taktwerk\yiiboilerplate\components\TwModule
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\usermanager\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
