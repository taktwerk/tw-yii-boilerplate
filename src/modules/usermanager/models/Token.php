<?php

namespace taktwerk\yiiboilerplate\modules\usermanager\models;

use Da\User\Model\Token as BaseToken;

class Token extends BaseToken
{
    const TYPE_SET_PASSWORD = 4;
    protected $routes = [
        self::TYPE_CONFIRMATION => '/user/registration/confirm',
        self::TYPE_RECOVERY => '/user/recovery/reset',
        self::TYPE_CONFIRM_NEW_EMAIL => '/user/settings/confirm',
        self::TYPE_CONFIRM_OLD_EMAIL => '/user/settings/confirm',
        self::TYPE_SET_PASSWORD =>'/user/recovery/reset',
    ];
    public function getIsExpired()
    {
        try{
           return parent::getIsExpired();
        }catch (\Exception $e){
            if ($this->type === static::TYPE_SET_PASSWORD) {
                $expirationTime = $this->getModule()->tokenSetPasswordLifespan;
            }else {
                return false;
                throw new \RuntimeException('Unknown Token type.');
            }
        }
        return ($this->created_at + $expirationTime) < time();
    }
}