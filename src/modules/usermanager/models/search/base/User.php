<?php
//Generation Date: 25-Nov-2020 11:16:10am
namespace taktwerk\yiiboilerplate\modules\usermanager\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\usermanager\models\User as UserModel;

/**
 * User represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\usermanager\models\User`.
 */
class User extends UserModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'identifier',
                    'username',
                    'email',
                    'password_hash',
                    'auth_key',
                    'unconfirmed_email',
                    'registration_ip',
                    'flags',
                    'confirmed_at',
                    'blocked_at',
                    'updated_at',
                    'created_at',
                    'last_login_at',
                    'last_login_ip',
                    'auth_tf_key',
                    'auth_tf_enabled',
                    'password_changed_at',
                    'role_changed_at',
                    'fullname',
                    'gdpr_consent',
                    'gdpr_consent_date',
                    'gdpr_deleted',
                    'deleted_at',
                    'deleted_by',
                    'created_by',
                    'updated_by',
                    'app_data_version',
                    'client_id',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$showDeleted = (property_exists(\Yii::$app->controller, 'model')
            &&
            (is_subclass_of(UserModel::class, \Yii::$app->controller->model) 
            	||
            	is_subclass_of(\Yii::$app->controller->model, UserModel::class))
            &&
              (Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model)
                ||
                Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model)))
            );
        $query = UserModel::findWithOutSystemEntry((Yii::$app->get('userUi')->get('show_deleted', UserModel::class) === '1'|| $showDeleted)?false:true);

        $this->parseSearchParams(UserModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(UserModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(UserModel::class),
                'params' => [
                    'page' => $this->parsePageParams(UserModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('flags', $query);
        $this->applyHashOperator('confirmed_at', $query);
        $this->applyHashOperator('blocked_at', $query);
        $this->applyHashOperator('updated_at', $query);
        $this->applyHashOperator('created_at', $query);
        $this->applyHashOperator('last_login_at', $query);
        $this->applyHashOperator('auth_tf_enabled', $query);
        $this->applyHashOperator('password_changed_at', $query);
        $this->applyHashOperator('role_changed_at', $query);
        $this->applyHashOperator('gdpr_consent', $query);
        $this->applyHashOperator('gdpr_consent_date', $query);
        $this->applyHashOperator('gdpr_deleted', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('app_data_version', $query);
        $this->applyHashOperator('client_id', $query);
        $this->applyLikeOperator('identifier', $query);
        $this->applyLikeOperator('username', $query);
        $this->applyLikeOperator('email', $query);
        $this->applyLikeOperator('password_hash', $query);
        $this->applyLikeOperator('auth_key', $query);
        $this->applyLikeOperator('unconfirmed_email', $query);
        $this->applyLikeOperator('registration_ip', $query);
        $this->applyLikeOperator('last_login_ip', $query);
        $this->applyLikeOperator('auth_tf_key', $query);
        $this->applyLikeOperator('fullname', $query);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(UserModel::class);
    }
}
