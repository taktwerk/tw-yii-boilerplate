<?php

namespace taktwerk\yiiboilerplate\modules\usermanager\models\search;

use taktwerk\yiiboilerplate\modules\usermanager\models\search\base\User as UserSearchModel;
use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

/**
* User represents the model behind the search form about `taktwerk\yiiboilerplate\modules\usermanager\models\User`.
*/
class User extends UserSearchModel{

    public $status;
    public $client;
    public $blocked_status;
    public $selectedRoles;
    public $selectedGroups;
    /**
     * List of additional rules to be applied to model, uncomment to use them
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['status', 'client','blocked_status','selectedRoles','selectedGroups'], 'safe'],
        ]);
    }

    public function search($params)
    {
        $dataProvider = parent::search($params);

        $dataProvider->query->joinWith(['client']);

        $dataProvider->sort->attributes['client'] = [
            'asc' => ['client.name' => SORT_ASC],
            'desc' => ['client.name' => SORT_DESC],
            'default' => SORT_DESC,
        ];
        $query = $dataProvider->query;
        if($this->status){
            
            try {
                if($this->status=="on"){
                    $user_ids = ArrayHelper::getColumn(self::find()->andWhere('unix_timestamp() - user.last_login_at < :time', ['time' => 300])->all(),'id');
                }elseif($this->status=="off"){
                    $user_ids = ArrayHelper::getColumn(self::find()
                        ->andWhere('unix_timestamp() - user.last_login_at > :time', ['time' => 300])
                        ->orWhere(['user.last_login_at'=>null])
                        ->all(),'id');
                }
                
                $query->andFilterWhere(['in',self::tableName().'.id',$user_ids]);
            } catch (Exception $e) {
                //
            }
        }
        if($this->blocked_status || $this->blocked_status==='0'){
            
            if($this->blocked_status==0){
                $query->andWhere(['IS NOT', 'user.blocked_at',new Expression('null')]);
            }
            if($this->blocked_status==1){
                $query->andWhere(['IS', 'user.blocked_at',new Expression('null')]);
            }
        }
        
        $dataProvider->query;
        if ($this->client) {
            $dataProvider->query->andFilterWhere(['like', 'client.name', $this->client]);
        }

        if($this->selectedRoles && is_array($this->selectedRoles) && count($this->selectedRoles)){
            $tbl = self::tableFullName();
            $query->join('LEFT JOIN',
                'auth_assignment',
                "auth_assignment.user_id =$tbl.id"
                );
            $query->andWhere(['auth_assignment.item_name'=>$this->selectedRoles]);
            
        }
        if($this->selectedGroups && is_array($this->selectedGroups) && count($this->selectedGroups)){
            $query->joinWith('userGroups');
            $query->andWhere(['user_group.group_id' => $this->selectedGroups]);
        }
        return $dataProvider;
    }
}