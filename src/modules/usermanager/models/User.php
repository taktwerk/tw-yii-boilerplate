<?php

namespace taktwerk\yiiboilerplate\modules\usermanager\models;

use Da\User\Helper\SecurityHelper;
use Da\User\Service\PasswordRecoveryService;
use Endroid\QrCode\QrCode;
use kartik\password\StrengthValidator;
use taktwerk\yiiboilerplate\enums\AppConfigurationModeEnum;
use taktwerk\yiiboilerplate\modules\customer\models\ClientUser;
use taktwerk\yiiboilerplate\modules\user\widgets\AssignmentsWidget;
use taktwerk\yiiboilerplate\traits\UserAfterSaveTrait;
use Yii;
use \taktwerk\yiiboilerplate\modules\usermanager\models\base\User as BaseUser;
use Da\User\Model\Assignment;
use Da\User\Service\UpdateAuthAssignmentsService;
use Da\User\Traits\AuthManagerAwareTrait;
use Da\User\Traits\ContainerAwareTrait;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\web\Application;
use taktwerk\yiiboilerplate\modules\user\models\UserGroup;
use taktwerk\yiiboilerplate\modules\user\service\PasswordSetService;
use Endroid\QrCode\Writer\PngWriter;
use Endroid\QrCode\Builder\Builder;
use taktwerk\yiiboilerplate\modules\user\models\Group;

/**
 * This is the model class for table "user".
 * @property integer $client_id
 */
class User extends BaseUser
{
    use AuthManagerAwareTrait;
    use ContainerAwareTrait;
    use UserAfterSaveTrait;

    const IDENTIFIER_STRING_LENGTH = 16;

    /**
     * @var $role string
     */
    public $role;
    /**
     * @var $password string
     */
    public $password;
    /**
     * @var $group_ids
     */
    public $group_ids;
    /**
     * @var bool
     */
    public $password_email = false;
    /**
     * @var string
     */
    public $password_pattern = 'normal';
    /**
     * List of additional rules to be applied to model, uncomment to use them
     * @return array
     */
    public function rules()
    {
        $rules = array_merge(parent::rules(), [
            [['username', 'email'], 'required', 'on' => 'create'],
            [['password','role','client_id','password_email','group_ids'], 'safe'],
            [['username', 'email'], 'validateUnique'],
            ['client_id', 'default', 'value' => null],
        ]);

        if(getenv('ENABLE_PASSWORD_PATTERN')){
            if(!empty(getenv('PASSWORD_PATTERN'))){
                $this->password_pattern = getenv('PASSWORD_PATTERN');
            }
            $rules[] = [['password'], StrengthValidator::className(), 'preset'=>$this->password_pattern,
                'when' => function ($model) {
                    return $model->password_email != 1;
                }, 'whenClient' => "function (attribute, value) { 
                   return ($('#user-password_email').val()).trim()!='1'; 
            }"];
        }

        return array_merge($rules, $this->buildRequiredRulesFromFormDependentShowFields());
    }

    /**
     *
     * @return array
     */
    public function formDependentShowFields()
    {
        $fName = $this->formName();
        $dependentFields = [];
        $dependentFields['password'] = $this->generateDependentFieldRules('password', [
            "field" => $fName . "[password]",
            "container" => ".form-group-block",
            'on'=>'create',
            "rules" => [
                [
                    "name" => $fName . "[password_email]",
                    "operator" => "is",
                    "value" =>  '',
                ]
            ],
        ],true);

        return $dependentFields;
    }


    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(),[
            'password_email' => Yii::t('twbp','Send Email to user to set password'),
            'group_ids' => Yii::t('twbp','Assign Group')
        ]);
    }

    /**
     * List of additional rules to be applied to model, uncomment to use them
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            TimestampBehavior::class,
        ]);
    }
    public function getUserGroups(){
        return $this->hasMany(UserGroup::class, ['user_id' => 'id']);
    }
    public function getGroups(){
        return $this->hasMany(Group::className(), ['id' => 'group_id'])
        ->via('userGroups');
    }
    /**
     * @param bool $insert
     * @return bool
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function beforeSave($insert)
    {
        /** @var SecurityHelper $security */
        $security = $this->make(SecurityHelper::class);
        if (!$insert) {
            if ((!$this->role && $this->role_changed_at) || $this->role) {
                $model = $this->make(Assignment::class, [], ['user_id' => $this->id]);
                $oldAuthAssignmentItems = $model->items;
                $model->items = $this->role;

                $items = [];
                foreach ($oldAuthAssignmentItems as $assignmentItem){
                    if(!in_array($assignmentItem, $this->getItems())){
                        $items[] = $assignmentItem;
                    }
                }

                $model->items = ArrayHelper::merge((array)$model->items, $items);

                $model->items = array_filter($model->items);

                if ($model) {
                    $this->make(UpdateAuthAssignmentsService::class, [$model])->run();
                    if (array_diff($oldAuthAssignmentItems, $model->items) ||
                        array_diff($model->items, $oldAuthAssignmentItems)
                    ) {
                        $this->role_changed_at = time();
                    }
                }
            }
        } else {
            $this->identifier = Yii::$app->security->generateRandomString(self::IDENTIFIER_STRING_LENGTH);
            if (!$this->client_id && getenv('CLIENT_ID')) {
                $this->client_id = getenv('CLIENT_ID');
            }
            if (empty($this->auth_key)) {
                $this->setAttribute('auth_key', $security->generateRandomString());
            }
            if (Yii::$app instanceof Application) {
                $this->setAttribute('registration_ip', Yii::$app->request->getUserIP());
            }
        }
        if (!empty($this->password) || empty($this->password_hash)) {
            $this->setAttribute(
                'password_hash',
                $security->generatePasswordHash($this->password)
            );
            $this->password_changed_at = time();
        }


        if (!Yii::$app->getUser()->can('Authority')) {
            $client = ClientUser::find()->where(['user_id' => Yii::$app->getUser()->id])->one();
            $this->client_id = $client->client_id;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function beforeValidate()
    {
        if($this->scenario == 'create' && empty($this->password) && $this->isNewRecord && $this->password_email){
            $this->password = time();
        }
        if ($this->isNewRecord) {
            if ($this->scenario == 'create') {
                /** @var SecurityHelper $security */
                $security = $this->make(SecurityHelper::class);
                $this->setAttribute('auth_key', $security->generateRandomString());

                if (!empty($this->password)) {
                    $this->setAttribute(
                        'password_hash',
                        $security->generatePasswordHash($this->password)
                    );
                    $this->password_changed_at = time();
                }
                if (empty($this->created_at)) {
                    $this->created_at = time();
                }
                $this->updated_at = time();
                if(!$this->confirmed_at){
                    $this->confirmed_at = time();
                }
            }
        }

        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function getAssignedRoleItems()
    {
        return $this->id ? $this->make(Assignment::class, [], ['user_id' => $this->id])->items : "";
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function getItems()
    {
        $validRoleOptions = [];
        $defaultRoles = explode(",", getenv('APP_CLIENT_ROLES_DEFAULT'));
        $applicableRoles = explode(",", getenv('APP_CLIENT_ROLES'));
        $groupDefaultRoles = getenv('APP_GROUP_ROLES');
        $userRoles = array_keys(Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id));

        if(count($userRoles)==1)
            $userRoles = implode(':',$userRoles).':';
        else
            $userRoles = implode(':',$userRoles);

        $isGroupFound = false;
        if(isset($groupDefaultRoles) && $groupDefaultRoles){
            $allRoles = array_filter(explode(',',$groupDefaultRoles));
            foreach ($allRoles as $role){
                $subRoles = explode(':',$role);
                if(stripos($userRoles,$subRoles[0].':')!==false){
                    $rolesToBeShown = explode('+',$subRoles[1]);
                    $isGroupFound = true;
                }
            }
        }

        $rolesToBeShown = array_filter($rolesToBeShown);
        if(count($rolesToBeShown)>=1){
            $rolesToBeShown = array_combine($rolesToBeShown, $rolesToBeShown);
            return $rolesToBeShown;
        }elseif ($isGroupFound && count($rolesToBeShown)<1){
            return [];
        }

        if(count($applicableRoles)==count($defaultRoles)){
            return [];
        }

        $validRoles = array_keys($this->getAuthManager()->getItems(1));
        foreach ($applicableRoles as $role) {
            if (in_array($role, $validRoles)) {
                $validRoleOptions[$role] = $role;
            }
        }

        return $validRoleOptions;

    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function getDefaultItems()
    {
        $validRoleOptions = [];
        $applicableRoles = explode(",", getenv('APP_CLIENT_ROLES_DEFAULT'));
        $clientRoles = explode(",", getenv('APP_CLIENT_ROLES'));

        if(count($applicableRoles)<1 && count($clientRoles)==1){
            $applicableRoles = $clientRoles;
        }

        $validRoles = array_keys($this->getAuthManager()->getItems(1));
        foreach ($applicableRoles as $role) {
            if (in_array($role, $validRoles)) {
                $validRoleOptions[] = $role;
            }
        }
        $validRoleOptions = array_filter($validRoleOptions);
        return $validRoleOptions;

    }

    /**
     * @return bool whether is blocked or not
     */
    public function getIsBlocked()
    {
        return $this->blocked_at !== null;
    }


    /**
     * BREA(D) - Determine if the model can be Deleted by the user.
     * To be overwritten in the model.
     * @return bool
     */
    public function deletable()
    {
        if (Yii::$app->user->identity->getId() == $this->id) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param bool $removedDeleted
     * @return \taktwerk\yiiboilerplate\models\TwActiveQuery|\yii\db\ActiveQuery
     * @throws \yii\db\Exception
     */
    public static function find($removedDeleted = true)
    {

        $model = parent::find($removedDeleted = true);


        if (php_sapi_name() != "cli" && !Yii::$app->getUser()->can('Authority')) {

            $authority_users = ArrayHelper::getColumn(Yii::$app->db->createCommand("SELECT user_id FROM auth_assignment where item_name in ('Superadmin', 'Authority')")
                ->queryAll(), 'user_id');
            $tbl = self::tableFullName();
            $model->filterWhere(['not in', $tbl.'.id', $authority_users]);
        }
        $model->findWithSystemEntry(self::tableName(), $removedDeleted);

        return $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        if (Yii::$app->hasModule('customer')) {
            return $this->hasOne(
                \taktwerk\yiiboilerplate\modules\customer\models\Client::class,
                ['id' => 'client_id']
            );
        }
        return null;
    }

    public function afterSave($insert, $changedAttributes)
    {
        //Scenario if only one group exists then assign automatically and don't show
        if($insert && empty($this->group_ids)){
            $total_group = \taktwerk\yiiboilerplate\modules\user\models\Group::find()->count();
            if($total_group == 1){
                $Group = \taktwerk\yiiboilerplate\modules\user\models\Group::find()->One();
                if($Group){
                    $this->group_ids[] = $Group->id;
                }
            }
        }

        if(!empty($this->group_ids)){
            \taktwerk\yiiboilerplate\modules\user\models\UserGroup::deleteAll('user_id = :user_id', [':user_id' => $this->id]);
            foreach ($this->group_ids as $key => $group_id){
                $UserGroup = new \taktwerk\yiiboilerplate\modules\user\models\UserGroup();
                $UserGroup->user_id = $this->id;
                $UserGroup->group_id = $group_id;
                $UserGroup->save();
            }
        }
        if($insert && $this->password_email){
            $this->sendRegistrationEmail(true);
        }

        if($insert){
            $this->assignDefaultRoles();
        }

        if($this->client_id != $changedAttributes['client_id']){
            $old_client_user = ClientUser::find()->where(['user_id'=>$this->id,'client_id'=>$changedAttributes['client_id']])->one();
            $client_user = ClientUser::find()->where(['user_id'=>$this->id, 'client_id'=>$this->client_id])->one();
            if(!$client_user){
                if($old_client_user){
                    $old_client_user->client_id = $this->client_id;
                    $old_client_user->save();
                }else{
                    $new = new ClientUser();
                    $new->client_id = $this->client_id;
                    $new->user_id = $this->id;
                    $new->save();
                }
            }
        }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
        if (!$insert) {
            if (!empty($changedAttributes['password_changed_at']) ||
                !empty($changedAttributes['role_changed_at']) ||
                !empty($changedAttributes['blocked_at'])
            ) {
                Yii::$app->getUser()->logoutByUserId($this->id);
            }
        }
        $this->newsletterUserAfterSave($insert, $changedAttributes);
    }
    public function afterFind()
    {
        $UserGroup = \taktwerk\yiiboilerplate\modules\user\models\UserGroup::find()
            ->select('group_id')
            ->andWhere(['user_id'=>$this->id])
            ->all();
        foreach ($UserGroup as $key => $group){
            $this->group_ids[] = $group->group_id;
        }
        parent::afterFind();

    }

    public function getAppSettingsData($format = null)
    {
        if (!Yii::$app->params['appSettings']) {
            return null;
        }
        $config = Yii::$app->params['appSettings'];
        if (isset($config['mode']) && (int) $config['mode'] === AppConfigurationModeEnum::CONFIGURE_AND_USER_LOGIN) {
            $config['userIdentifier'] = $this->identifier;
        }

        if ($format === 'json') {
            return json_encode($config);
        }

        return $config;
    }

    public function qrcode()
    {
        $appSettingsJson = $this->getAppSettingsData('json');
        if (!$appSettingsJson) {
            return null;
        }
        return Builder::create()->data($appSettingsJson)->build()->getDataUri();
    }

    /**
     * Find query to check for unique username and email
     * @param bool $removedDeleted
     * @return \taktwerk\yiiboilerplate\models\TwActiveQuery|\yii\db\ActiveQuery
     */
    public static function findUniqueEntry($removedDeleted = true)
    {
        $model = parent::find($removedDeleted = true);
        return $model;
    }

    /**
     * Custom validation to check for unique username and email
     * @param $attribute.
     */
    public function validateUnique($attribute)
    {
        $exists = self::findUniqueEntry()->where(["$attribute" => $this->$attribute])->exists();
        if ($exists && $this->oldAttributes[$attribute]!=$this->$attribute) {
            $this->addError($attribute, "$attribute {$this->$attribute} has already been taken.");
        }
    }

    /**
     * Get online status of user
     * @return bool
     */
    public function isOnline()
    {
        return isset($this->last_login_at) ? time() - $this->last_login_at <= 300 : false;
    }

    /**
     * @param $user
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function sendRegistrationEmail($set_password=false){
        $user = $this->make(\Da\User\Model\User::class, []);
        $user->set_password = true;
        $user->email = $this->email;
        $user->id = $this->id;
        $mailService = \taktwerk\yiiboilerplate\modules\user\factory\MailFactory::makeWelcomeWithPasswordMailerService($user,$set_password);
        $this->make(PasswordSetService::class, [$this->email, $mailService])->run();
        return true;
    }

    /**
     * Assign default roles
     */
    public function assignDefaultRoles(){
        $groupDefaultRoles = getenv('APP_GROUP_ROLES_DEFAULT');
        $userRoles = array_keys(Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id));

        if(count($userRoles)==1)
            $userRoles = implode(':',$userRoles).':';
        else
            $userRoles = implode(':',$userRoles);

            if(isset($groupDefaultRoles) && $groupDefaultRoles){
            $allRoles = explode(',',$groupDefaultRoles);
            foreach ($allRoles as $role){
                $subRoles = explode(':',$role);
                if(stripos($userRoles,$subRoles[0].':')!==false){
                    $rolesToBeAssigned = explode('+',$subRoles[1]);
                }
            }
        }

        $rolesToBeAssigned = array_filter($rolesToBeAssigned);

        if(count($rolesToBeAssigned)<1){
            $rolesToBeAssigned = $this->getDefaultItems();
        }

        if(count($rolesToBeAssigned)>=1){
            $model = $this->make(Assignment::class, [], ['user_id' => $this->id]);
            $model->items = $rolesToBeAssigned;
            if ($model) {
                $this->make(UpdateAuthAssignmentsService::class, [$model])->run();
            }
        }
    }

}
