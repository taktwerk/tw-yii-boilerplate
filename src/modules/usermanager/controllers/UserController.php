<?php

namespace taktwerk\yiiboilerplate\modules\usermanager\controllers;

use taktwerk\yiiboilerplate\modules\usermanager\models\User;
use Da\User\Helper\SecurityHelper;
use taktwerk\yiiboilerplate\models\TwActiveRecord;
use Yii;
use yii\base\InvalidRouteException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\HttpException;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\grid\GridView;
use Da\User\Model\Role;
use Da\User\Search\RoleSearch;
use taktwerk\yiiboilerplate\modules\user\models\Group;
/**
 * This is the class for controller "UserController".
 */
class UserController extends \taktwerk\yiiboilerplate\modules\usermanager\controllers\base\UserController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\usermanager\models\User';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\usermanager\models\search\User';

    /**
     * Additional actions for controllers, uncomment to use them
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'block',
                            'block-multiple',
                            'email-multiple',
                        ],
                        'roles' => ['@']
                    ]
                ]
            ]
        ]);
    }

    public function init()
    {
        $this->customActions['resend-recovery'] = function ($url, $model) {
                if (Yii::$app->user->can('Authority')  && $model->id != Yii::$app->user->id) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-envelope"></span> ' . Yii::t('twbp', 'Resend registration email'),
                        ['/user/admin/resend-recovery', 'id' => $model->id],
                        [
                            'title' => Yii::t('twbp', 'Resend registration email'),
                            'data-confirm' => Yii::t(
                                'twbp',
                                'Are you sure you want to resend the email to this user for setting the password?'
                            ),
                            'data-method' => 'POST',
                        ]
                    );
                }
                return null;
            };

        $this->crudRelations = [
            'ClientUsers'=> false,
            'FlysystemUsers'=> false,
            'GoogleSpreadsheets'=> false,
            'ImportProgresses'=> false,
            'Notifications'=> false,
            'Pages'=> false,
            'Posts'=> false,
            'Profile'=> false,
            'QueueMessages'=> false,
            'SocialAccounts'=> false,
            'Tokens'=> false,
            'UserAuthCodes'=> false,
            'UserParameters'=> false,
            'UserTwDatas'=> false,
            'UserUiDatas'=> false,
            'WorkflowSteps'=> false,
        ];


        if(php_sapi_name() != "cli" && Yii::$app->getUser()->can('Authority') && Yii::$app->hasModule('customer')){
            $view_client_id  = [
                'before#fullname'=> [
                    'format' => 'html',
                    'attribute' => 'client_id',
                    'value' => function ($model) {
                        $foreign = $model->getClient()->one();
                        if ($foreign) {
                            if (Yii::$app->getUser()->can('app_client_view') && $foreign->readable()) {
                                return Html::a($foreign->toString, [
                                    'client/view',
                                    'id' => $model->getClient()->one()->id,
                                ], ['data-pjax' => 0]);
                            }
                            return $foreign->toString;
                        }
                        return '<span class="label label-warning">?</span>';
                    },
                ]
            ];
        }else{

            $view_client_id  = null;
        }

        $this->crudColumnsOverwrite = [
            'view'=>ArrayHelper::merge($view_client_id,[
                'auth_key'=>false,
                'password_hash'=>false,
                'unconfirmed_email'=>false,
                'registration_ip'=>false,
                'flags'=>false,
                'confirmed_at'=>false,
                'last_login_ip'=>false,
                'last_login_at'=>[
                    'attribute' => 'last_login_at',
                    'value' => function ($model) {
                        if (!$model->last_login_at || $model->last_login_at == 0) {
                            return \Yii::t('twbp', 'Never');
                        } elseif (extension_loaded('intl')) {
                            return \Yii::t('twbp', '{0, date, MMMM dd, YYYY HH:mm}', [$model->last_login_at]);
                        } else {
                            return date('Y-m-d G:i:s', $model->last_login_at);
                        }
                    },
                ],
                'auth_tf_key'=>false,
                'auth_tf_enabled'=>false,
                'password_changed_at'=>false,
                'gdpr_deleted'=>false,
                'gdpr_consent'=>false,
                'blocked_at'=>false,
                'gdpr_consent_date'=>[
                    'attribute'=>'roles',
                    'header' => Yii::t('twbp', 'Roles'),
                    'value'=>function($model){
                        return implode(", ",$model->getAssignedRoleItems());
                    }
                ],
                'fullname'=>[
                    'attribute'=>'identifier',
                    'format'=>'raw',
                    'header' => Yii::t('twbp', 'Identifier'),
                    'value'=>function($model){
                        if (empty(getenv('TAKTWERK_TYPE'))) {
                            return null;
                        }
                        $content = !$model->isNewrecord?
                            '<div style="text-align: start;">'. Html::img($model->qrcode()). '</div>' :
                            "";

                        if (Yii::$app->getUser()->can('Authority')) {
                            $config = $model->getAppSettingsData();
                            if (empty(getenv('TAKTWERK_TYPE')) && isset($config['taktwerk'])) {
                                unset($config['taktwerk']);
                            }
                            $content .= '<div>' . json_encode($config) . '</div>';
                        }

                        return $content;
                    },
                ],
                'before#username'=>'fullname',
            ]),
            'index'=>[
                'auth_key'=>false,
                'identifier'=>false,
                'role_changed_at'=>false,
                'app_data_version'=>false,
                'password_hash'=>false,
                'unconfirmed_email'=>false,
                'registration_ip'=>false,
                'flags'=>false,
                'confirmed_at'=>false,
                'last_login_ip'=>false,
                'auth_tf_key'=>false,
                'auth_tf_enabled'=>false,
                'password_changed_at'=>false,
                'gdpr_consent'=>false,
                'gdpr_deleted'=>false,
                'gdpr_consent_date'=>[
                    'header'=>'Status',
                    'format'=>'raw',
                    'filter' =>php_sapi_name() != "cli"? Html::dropDownList( 'User[status]',Yii::$app->request->get('User')['status'] ,['on'=>'Online', 'off'=>'Offline'],['class'=>'form-control','prompt'=>'Status']):[],
                    'value'=>function($model){
                        if($model->isOnline()){
                            return '<span class="label label-success">Online</span>';
                        }else{
                            return '<span class="label label-warning">Offline</span>';
                        }
                    }
                ],
                'last_login_at'=>[
                    'attribute' => 'last_login_at',
                    'value' => function ($model) {
                        if (!$model->last_login_at || $model->last_login_at == 0) {
                            return \Yii::t('twbp', 'Never');
                        } elseif (extension_loaded('intl')) {
                            return \Yii::t('twbp', '{0, date, MMMM dd, YYYY HH:mm}', [$model->last_login_at]);
                        } else {
                            return date('Y-m-d G:i:s', $model->last_login_at);
                        }
                    },
                ],
                'fullname'=>false,
                'blocked_at'=> [
                    'attribute'=>'blocked_status',  
                    'header' => Yii::t('twbp', 'Block status'),
                    'filter'=>[0=>Yii::t('twbp','Blocked'),1=>Yii::t('twbp','Active')],
                    'value' => function ($model) {
                        if ($model->id==Yii::$app->user->identity->getId()) {
                            return "";
                        } else if ($model->getIsBlocked()) {
                            return Html::a(Yii::t('twbp', 'Blocked')  , ['block', 'id' => $model->id], [
                                'class' => 'btn btn-xs btn-danger btn-block',
                                'title'=>\Yii::t('twbp','Toggle Status'),
                                'data-method' => 'post',
                                'data-confirm' => Yii::t('twbp', 'Are you sure you want to unblock this user?'),
                            ]);
                        } else {
                            return Html::a(Yii::t('twbp', 'Active'), ['block', 'id' => $model->id], [
                                'class' => 'btn btn-xs btn-success btn-block',
                                'title'=>\Yii::t('twbp','Toggle Status'),
                                'data-method' => 'post',
                                'data-confirm' => Yii::t('twbp', 'Are you sure you want to block this user?'),
                            ]);
                        }
                    },
                    'format' => 'raw',
                ],
                'before#username'=>'fullname',
                /* 'before#fullname' => php_sapi_name() != "cli" && Yii::$app->getUser()->can('Authority') && Yii::$app->hasModule('customer')?[
                    'attribute' => 'client',
                    'value' => 'client.name',
                    'format' => 'html',
                    'content' => function ($model){
                        return $model->client->toString;
                    }
                ]:false */
            ],
            'crud'=>[
                'after#email'=>[
                    'attribute'=>'selectedRoles',
                    'label' => Yii::t('twbp', 'Roles'),
                    'value'=>function($model){
                    return is_array($model->getAssignedRoleItems())?implode(", ",$model->getAssignedRoleItems()):"";
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'data' => ArrayHelper::map(\Yii::$app->getAuthManager()->getItems(\yii\rbac\Item::TYPE_ROLE),'name','name'),
                        'options' => [
                            'placeholder' => '',
                            'multiple' => true,
                        ],
                    ],
                    ],
                'after#username'=>[
                    'attribute'=>'selectedGroups',
                    'label'=>Yii::t('twbp', 'Groups'),
                    'value'=>function($model){
                    return implode(', ',ArrayHelper::map($model->groups,'id','name'));
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'data' => Group::find()->select('group.name,group.id')->indexBy('group.id')->column(),
                        'options' => [
                            'placeholder' => '',
                            'multiple' => true,
                        ],
                    ],
               ],
            ]
        ];
        if(php_sapi_name() != "cli"){

        $this->gridDropdownActions = [
            'block-multiple' => (Yii::$app->getUser()->can(
                Yii::$app->controller->module->id . '_' .
                Yii::$app->controller->id . '_block-multiple'
            ) ?
                [
                    'url' => [
                        false
                    ],
                    'options' => [
                        'onclick' => 'blockMultiple(this);',
                        'data-url' => Url::to(['/usermanager/user/block-multiple'])
                    ],
                    'label' => '<i class="fa fa-ban" aria-hidden="true"></i> ' . Yii::t('twbp', 'Block'),
                ] : ''),
            'email-multiple' => (Yii::$app->getUser()->can(
                Yii::$app->controller->module->id . '_' .
                Yii::$app->controller->id . '_email-multiple'
            ) ?
                [
                    'url' => [
                        false
                    ],
                    'options' => [
                        'onclick' => 'emailMultiple(this);',
                        'data-url' => Url::to(['/usermanager/user/email-multiple']),
                        'data-pjax' => false
                    ],
                    'label' => '<i class="fa fa-envelope" aria-hidden="true"></i> ' . Yii::t('twbp', 'Bulk Email'),
                ] : '')
        ];
        }
        return parent::init(); // TODO: Change the autogenerated stub
    }

    /**
     * @param $model
     * @param $form
     * @return array
     * @throws \Exception
     */
    public function formFieldsOverwrite($model, $form)
    {
        if(\Yii::$app->hasModule('customer')){
            if(php_sapi_name() != "cli" && \Yii::$app->getUser()->can('Authority')) {
                $client_field =$form->field(
                        $model,
                        'client_id'
                    )
                        ->widget(
                            Select2::class,
                            [
                                'data' => \taktwerk\yiiboilerplate\modules\customer\models\Client::find()->count() > 50 ? null : ArrayHelper::map(\taktwerk\yiiboilerplate\modules\customer\models\Client::find()->all(), 'id', 'toString'),
                                'initValueText' => \taktwerk\yiiboilerplate\modules\customer\models\Client::find()->count() > 50 ? \yii\helpers\ArrayHelper::map(\taktwerk\yiiboilerplate\modules\customer\models\Client::find()->andWhere(['id' => $model->client_id])->all(), 'id', 'toString') : '',
                                'options' => [
                                    'placeholder' => Yii::t('twbp', 'Select a value...'),
                                    'id' => 'client_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    (\taktwerk\yiiboilerplate\modules\customer\models\Client::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                                    (\taktwerk\yiiboilerplate\modules\customer\models\Client::find()->count() > 50 ? 'ajax' : '') => [
                                        'url' => \yii\helpers\Url::to(['list']),
                                        'dataType' => 'json',
                                        'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'Client\'
                                        };
                                    }')
                                    ],
                                ],
                                'pluginEvents' => [
                                    "change" => "function() {
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
                                        Url::toRoute('/customer/client/entry-details?id=', true) .
                                        "' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#client_id_well').html(json.data);
                                                //$('#client_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }",
                                ],
                                'addon' => (Yii::$app->getUser()->can('customer_client_create') && (!$relatedForm || ($relatedType != RelatedForms::TYPE_MODAL && !$useModal))) ? [
                                    'append' => [
                                        'content' => [
                                            RelatedForms::widget([
                                                'relatedController' => '/customer/client',
                                                'type' => $relatedTypeForm,
                                                'selector' => 'client_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                                'primaryKey' => 'id',
                                            ]),
                                        ],
                                        'asButton' => true
                                    ],
                                ] : []
                            ]
                        ) . '
                    
                
                <div id="client_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' .
                    (\taktwerk\yiiboilerplate\modules\customer\models\Client::findOne(['id' => $model->client_id])
                        ->entryDetails == '' ?
                        ' display:none;' :
                        '') . '
                    ">' .
                    (\taktwerk\yiiboilerplate\modules\customer\models\Client::findOne(['id' => $model->client_id])->entryDetails != '' ?
                        \taktwerk\yiiboilerplate\modules\customer\models\Client::findOne(['id' => $model->client_id])->entryDetails :
                        '') . ' 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>';
            }else{
                $client_field = Html::activeHiddenInput(
                    $model,
                    'client_id',
                    [
                        'id' => Html::getInputId($model, 'client_id') . $owner
                    ]
                );
            }
        }else{
            $client_field = $form->field($model,'client_id');
        }
        $total_group = \taktwerk\yiiboilerplate\modules\user\models\Group::find()->count();
        if($total_group > 1) {
            $group_field = $form->field(
                $model,
                'group_ids'
            )
                ->widget(
                    Select2::class,
                    [
                        'data' => ArrayHelper::map(\taktwerk\yiiboilerplate\modules\user\models\Group::find()->all(), 'id', 'toString'),
                        'options' => [
                            'placeholder' => Yii::t('twbp', 'Select group'),
                            'id' => 'group_ids' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'multiple' => true
                        ],
                        'addon' => []
                    ]
                );
        }else{
            $group_field = '';
        }
        $model->role = $model->isNewRecord?"":$model->getAssignedRoleItems();
        $this->formFieldsOverwrite = [
            'unconfirmed_email'=>false,
            'registration_ip'=>false,
            'flags'=>false,
            'confirmed_at'=>false,
            'last_login_ip'=>false,
            'last_login_at'=>false,
            'auth_tf_key'=>false,
            'auth_tf_enabled'=>false,
            'password_changed_at'=>false,
            'gdpr_consent'=>false,
            'gdpr_consent_date'=>false,
            'blocked_at'=>false,
            'identifier'=>false,
            'app_data_version'=>false,
            'role_changed_at'=>false,
            'gdpr_deleted'=>false,
            'before#username' => $client_field,
            'before#client_id' => $group_field,
            'client_id'=>false,
            'group_ids'=>false,
            'password_hash'=> $form->field(
                $model,
                'password',
                [
                    'selectors' => [
                        'input' => '#' .
                            \kartik\helpers\Html::getInputId($model, 'password') . $owner
                    ]
                ]
            )
                ->textInput(
                    [
                        'placeholder' => $model->getAttributePlaceholder('password'),
                        'autocomplete'=>"off",
                        'id' => Html::getInputId($model, 'password') . $owner,
                    ]
                )
                ->hint($model->getAttributeHint('password')),
            'after#fullname'=>!$model->isNewRecord?$form->field(
                $model,
                'role',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'role') . $owner
                    ],
                ]
            )
                ->inline(false)
                ->checkboxList(
                    $model->getItems()
                    , [
                        'id' => Html::getInputId($model, 'role') . $owner,
                    ]
                )
                ->label(count($model->getItems())>=1?Yii::t('twbp','Role'):false):"",
            'auth_key'=> $form->field(
                $model,
                'password_email',
                [
                    'selectors' => [
                        'input' => '#' .
                            \kartik\helpers\Html::getInputId($model, 'password_email') . $owner
                    ]
                ]
            )
                ->checkbox(['disabled'=>!$model->isNewRecord?true:false])
                ->hint($model->getAttributeHint('password_email')),

        ];
        return $this->formFieldsOverwrite;
    }

    public function beforeCreate(TwActiveRecord $model)
    {
        $model->scenario = "create";
        return parent::beforeCreate($model); // TODO: Change the autogenerated stub
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     * @throws InvalidRouteException
     * @throws \yii\base\Exception
     */
    public function actionBlock($id)
    {
        /**@var $model User*/
        $model = $this->findModel($id);
        if($model){
            /** @var SecurityHelper $security */
            $security = $model->make(SecurityHelper::class);
            if ($model->getIsBlocked()) {
                $model->updateAttributes(['blocked_at' => null]);
            } else {
                $model->updateAttributes(
                    ['blocked_at' => time(), 'auth_key' => $security->generateRandomString()]
                );
                Yii::$app->getUser()->logoutByUserId($model->id);
            }
        }else{
            throw new InvalidRouteException();
        }

        return $this->redirect("index");
    }

    /**
     * @return mixed
     * @throws HttpException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionBlockMultiple()
    {
        $pk = Yii::$app->request->post('pk'); // Array or selected records primary keys
        // Preventing extra unnecessary query
        if (!empty($pk)) {
            foreach ($pk as $id) {
                /**@var $model User*/
                $model = $this->findModel($id);
                if($model && Yii::$app->user->identity->getId()!=$model->id){
                    /** @var SecurityHelper $security */
                    $security = $model->make(SecurityHelper::class);
                    if (!$model->getIsBlocked()) {
                        $model->updateAttributes(
                            ['blocked_at' => time(), 'auth_key' => $security->generateRandomString()]
                        );
                        Yii::$app->getUser()->logoutByUserId($model->id);
                    }
                }
            }
        }
        if (!Yii::$app->request->isAjax) {
            return $this->actionIndex();
        }
    }

    /**
     * @return mixed
     * @throws HttpException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionEmailMultiple()
    {
        $pk = Yii::$app->request->post('pk'); // Array or selected records primary keys
        // Preventing extra unnecessary query
        if (!empty($pk)) {
            $email_array = ArrayHelper::getColumn(User::find()->where(['in', 'id', $pk])->all(), 'email');
            return implode(",", $email_array);
        }
        if (!Yii::$app->request->isAjax) {
            return $this->actionIndex();
        }
    }
}
