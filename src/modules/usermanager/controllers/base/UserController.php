<?php
//Generation Date: 25-Nov-2020 06:08:50am
namespace taktwerk\yiiboilerplate\modules\usermanager\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends TwCrudController
{
}
