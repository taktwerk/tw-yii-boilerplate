<?php
namespace taktwerk\yiiboilerplate\modules\backupmanager\controllers;

use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\web\Controller;
use taktwerk\yiiboilerplate\modules\backupmanager\models\Dump;
use taktwerk\yiiboilerplate\modules\backupmanager\models\Restore;
use Symfony\Component\Process\Process;
use taktwerk\yiiboilerplate\modules\backupmanager\Module;
use taktwerk\yiiboilerplate\modules\backupmanager\contracts\Mysqldump;
use yii\helpers\FileHelper;

/**
 * Default controller.
 */
class DefaultController extends Controller
{

    /**
     *
     * @return Module
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'create' => [
                        'post'
                    ],
                    'delete' => [
                        'post'
                    ],
                    'delete-all' => [
                        'post'
                    ],
                    'delete-all-oldbackup' => [
                        'post'
                    ],
                    'restore' => [
                        'get',
                        'post'
                    ],
                    '*' => [
                        'get'
                    ]
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actionIndex()
    {
        $dataArray = $this->prepareFileData();
        $dbList = $this->getModule()->dbList;
        $model = new Dump($dbList, $this->getModule()->customDumpOptions);
        $dataProvider = new ArrayDataProvider([
            'allModels' => $dataArray,
            'pagination' => [
                'pageSize' => 20
            ]
        ]);
        $activePids = $this->checkActivePids();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'dbList' => $dbList,
            'activePids' => $activePids
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actionCreate()
    {
        $model = new Dump($this->getModule()->dbList, $this->getModule()->customDumpOptions);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $dbInfo = $this->getModule()->getDbInfo($model->db);
            $dumpOptions = $model->makeDumpOptions();
            $manager = $this->getModule()->createManager($dbInfo);
            
            $dumpPath = $manager->makeName($dbInfo, $dumpOptions);
            FileHelper::createDirectory(\Yii::getAlias('@runtime/tmp'));
            $tempPath = \Yii::getAlias('@runtime/tmp/' . $dumpPath);
            $ignoreTables = getenv('DB_BACKUP_IGNORE_TABLES');
            if($ignoreTables){
                $dumpOptions['ignoreTables'] = $ignoreTables;
            }
            $dumpCommand = $manager->makeDumpCommand($tempPath, $dbInfo, $dumpOptions);
            if ($dumpCommand == false) {
                $dumpPath = $manager->makeName($dbInfo, $dumpOptions);
                $dump = new Mysqldump("{$dbInfo['dsn']}", "{$dbInfo['username']}", "{$dbInfo['password']}", [
                    'compress' => $dumpOptions['isArchive'] ? Mysqldump::GZIP : Mysqldump::NONE,
                    'no-data' => $dumpOptions['schemaOnly'] ? true : false,
                    'exclude-tables'=>($ignoreTables)?array_filter(explode(',',$ignoreTables)):[]
                ]);
                
                $dump->start($tempPath);
                $stream = fopen($tempPath, 'r+');
                Yii::$app->fs->putStream($this->getModule()->path . $dumpPath, $stream);
                fclose($stream);
                FileHelper::unlink($tempPath);
            } else {
                Yii::trace(compact('dumpCommand', 'dumpPath', 'dumpOptions'), get_called_class());
                if ($model->runInBackground) {
                    $asynOptions = [
                        'tempPath' => $tempPath,
                        'dumpPath' => $dumpPath
                    ];
                   
                    $this->runProcessAsync($dumpCommand, false, $asynOptions);
                } else {
                    if ($this->runProcess($dumpCommand)) {
                        $stream = fopen($tempPath, 'r+');
                        Yii::$app->fs->putStream($this->getModule()->path . $dumpPath, $stream);
                        fclose($stream);
                        FileHelper::unlink($tempPath);
                    }
                }
            }
        } else {
            Yii::$app->session->setFlash('error', Yii::t('twbp', 'Backup request invalid.') . '<br>' . Html::errorSummary($model));
        }
        
        return $this->redirect([
            'index'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actionDownload($id)
    {
        $dumpPath = $this->getModule()->path . StringHelper::basename(ArrayHelper::getValue($this->getModule()->getFileList(), $id));
        
        return Yii::$app->response->sendFile($dumpPath);
    }

    /**
     * @inheritdoc
     */
    public function actionRestore($id)
    {
        $dumpFile = $this->getModule()->path . StringHelper::basename(ArrayHelper::getValue($this->getModule()->getFileList(), $id));
        $model = new Restore($this->getModule()->dbList, $this->getModule()->customRestoreOptions);
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $dbInfo = $this->getModule()->getDbInfo($model->db);
                $restoreOptions = $model->makeRestoreOptions();
                $manager = $this->getModule()->createManager($dbInfo);
                $restoreCommand = $manager->makeRestoreCommand($dumpFile, $dbInfo, $restoreOptions);
                Yii::trace(compact('restoreCommand', 'dumpFile', 'restoreOptions'), get_called_class());
                if ($model->runInBackground) {
                    $this->runProcessAsync($restoreCommand, true);
                } else {
                    $this->runProcess($restoreCommand, true);
                }
                
                return $this->redirect([
                    'index'
                ]);
            }
        }
        
        return $this->render('restore', [
            'model' => $model,
            'file' => $dumpFile,
            'id' => $id
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actionStorage($id)
    {
        if (Yii::$app->has('fs')) {
            $dumpname = StringHelper::basename(ArrayHelper::getValue($this->getModule()->getFileList(), $id));
            $dumpPath = $this->getModule()->path . $dumpname;
            $exists = Yii::$app->fs->has($dumpname);
            if ($exists) {
                Yii::$app->fs->delete($dumpname);
                Yii::$app->session->setFlash('success', Yii::t('twbp', 'Backup deleted from storage.'));
            } else {
                $stream = fopen($dumpPath, 'r+');
                Yii::$app->fs->writeStream($dumpname, $stream);
                Yii::$app->session->setFlash('success', Yii::t('twbp', 'Backup uploaded to storage.'));
            }
        }
        
        return $this->redirect([
            'index'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actionDelete($name)
    {
        $dumpFile = $this->getModule()->path . $name;
        if (Yii::$app->fs->delete($this->getModule()->path . $name)) {
            Yii::$app->session->setFlash('success', Yii::t('twbp', 'Backup deleted successfully.'));
        } else {
            Yii::$app->session->setFlash('error', Yii::t('twbp', 'Error deleting Backup.'));
        }
        
        return $this->redirect([
            'index'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actionDeleteAll()
    {
        if (! empty($this->getModule()->getFileList())) {
            $fail = [];
            foreach ($this->getModule()->getFileList() as $file) {
                if (! unlink($file)) {
                    $fail[] = $file;
                }
            }
            if (empty($fail)) {
                Yii::$app->session->setFlash('success', Yii::t('twbp', 'All Backups successfully removed.'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('twbp', 'Error deleting Backups.'));
            }
        }
        
        return $this->redirect([
            'index'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actionDeleteAllOldbackup()
    {
        if (! empty($this->getModule()->getFileList())) {
            $fail = [];
            foreach ($this->getModule()->getFileList() as $file) {
                $fileCreatedTime = filectime($file);
                $numberDays = getenv('DB_BACKUP_MAX_RETAIN_DAYS');
                $dbBackupMaxRetainDays = ! empty($numberDays) ? $numberDays : '7';
                $daysAgo = time() - 60 * 60 * 24 * $dbBackupMaxRetainDays;
                
                if ($fileCreatedTime < $daysAgo) {
                    if (! unlink($file)) {
                        $fail[] = $file;
                    }
                }
            }
            if (empty($fail)) {
                Yii::$app->session->setFlash('success', Yii::t('twbp', 'All Backups successfully removed.'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('twbp', 'Error deleting Backups.'));
            }
        }
        
        return $this->redirect([
            'index'
        ]);
    }

    /**
     *
     * @param
     *            $command
     * @param bool $isRestore
     */
    protected function runProcess($command, $isRestore = false)
    {
        $process = new Process($command);
        $process->setTimeout($this->getModule()->timeout);
        $process->run();
        if ($process->isSuccessful()) {
            $msg = (! $isRestore) ? Yii::t('twbp', 'Backup successfully created.') : Yii::t('twbp', 'Backup successfully restored.');
            Yii::$app->session->addFlash('success', $msg);
            return true;
        } else {
            $msg = (! $isRestore) ? Yii::t('twbp', 'Backup failed.') : 
            Yii::t('twbp', 'Restore failed.');
            Yii::$app->session->addFlash('error', $msg . 'Command - ' .
                $command . '<br>' . $process->getOutput() . $process->getErrorOutput());
            Yii::error($msg . PHP_EOL . 'Command - ' . $command . PHP_EOL . $process->getOutput() . PHP_EOL . $process->getErrorOutput());
        }
        return false;
    }

    /**
     *
     * @param
     *            $command
     * @param bool $isRestore
     */
    protected function runProcessAsync($command, $isRestore = false, $data = [])
    {
        $process = new Process($command);
        $process->setTimeout($this->getModule()->timeout);
        $process->start();
        $pid = $process->getPid();
        $activePids = Yii::$app->session->get('backupPids', []);
        if (! $process->isRunning()) {
            if ($process->isSuccessful()) {
                $msg = (! $isRestore) ? Yii::t('twbp', 'Backup successfully created.') : Yii::t('twbp', 'Backup successfully restored.');
                Yii::$app->session->addFlash('success', $msg);
            } else {
                $msg = (! $isRestore) ? Yii::t('twbp', 'Backup failed.') : Yii::t('twbp', 'Restore failed.');
                Yii::$app->session->addFlash('error', $msg . '<br>' . 'Command - ' . $command . '<br>' . $process->getOutput() . $process->getErrorOutput());
                Yii::error($msg . PHP_EOL . 'Command - ' . $command . PHP_EOL . $process->getOutput() . PHP_EOL . $process->getErrorOutput());
            }
        } else {
            $activePids[$pid] = $command;
            Yii::$app->session->set('backupPids', $activePids);
            Yii::$app->session->set('tempPath' . $pid, $data['tempPath']);
            Yii::$app->session->set('dumpPath' . $pid, $data['dumpPath']);
            Yii::$app->session->addFlash('info', Yii::t('twbp', 'Process running with pid={pid}', [
                'pid' => $pid
            ]) . '<br>' . $command);
        }
    }

    /**
     *
     * @return array
     */
    protected function checkActivePids()
    {
        $activePids = Yii::$app->session->get('backupPids', []);
        $newActivePids = [];
        if (! empty($activePids)) {
            foreach ($activePids as $pid => $cmd) {
                $tempPath = Yii::$app->session->get('tempPath' . $pid);
                $dumpPath = Yii::$app->session->get('dumpPath' . $pid);
                $process = new Process('ps -p ' . $pid);
                $process->setTimeout($this->getModule()->timeout);
                $process->run();
                if (! $process->isSuccessful()) {
                    $stream = fopen($tempPath, 'r+');
                    Yii::$app->fs->putStream($this->getModule()->path . $dumpPath, $stream);
                    fclose($stream);
                    FileHelper::unlink($tempPath);
                    Yii::$app->session->addFlash('success', Yii::t('twbp', 'Process complete!') . '<br> PID=' . $pid . ' ' . $cmd);
                } else {
                    $newActivePids[$pid] = $cmd;
                }
            }
        }
        Yii::$app->session->set('backupPids', $newActivePids);
        
        return $newActivePids;
    }

    /**
     *
     * @return array
     */
    protected function prepareFileData()
    {
        $dataArray = [];
        foreach ($this->getModule()->getFileList() as $id => $file) {
            $dataArray[] = [
                'id' => $id,
                'type' => $file['extension'],
                'name' => $file['basename'],
                'size' => Yii::$app->formatter->asShortSize($file['size']),
                'create_at' => Yii::$app->formatter->asDatetime($file['timestamp'],'php:Y-m-d H:i:s'),
                'timestamp'=>$file['timestamp']
            ];
        }
        ArrayHelper::multisort($dataArray, [
            'timestamp'
        ], [
            SORT_DESC
        ]);
        
        return $dataArray ?: [];
    }
}
