# Yii2 Backup Manager

MySQL Database Backup functionality

## Configuration

Once the extension is installed, simply add it in your config by:

Basic ```config/web.php```

Advanced ```backend/config/common.php```

Add the below config in bootstrap section of the configuration 

```
 'bootstrap' => [
        'taktwerk\yiiboilerplate\modules\backupmanager\Bootstrap'
    ],
```

## Simple config

```php
    'backup-manager' => [
            'class' => 'taktwerk\yiiboilerplate\modules\backupmanager\Module',
            // path to directory for the backups
            'path' => 'dbbackups/',
            // list of registerd db-components
            'dbList' => [
                'db'
            ],
       ],
```

## Advanced config

```php
    'components' => [
        // https://github.com/creocoder/yii2-flysystem
        'fs' => [
            'class' => 'creocoder\flysystem\FtpFilesystem',
            'host' => 'ftp.example.com',
            //'port' => 21,
            //'username' => 'your-username',
            //'password' => 'your-password',
            //'ssl' => true,
            //'timeout' => 60,
            //'root' => '/path/to/root',
            //'permPrivate' => 0700,
            //'permPublic' => 0744,
            //'passive' => false,
            //'transferMode' => FTP_TEXT,
        ],
    ],
    'modules' => [
    ....
        'backup-manager' => [
            'class' => 'taktwerk\yiiboilerplate\modules\backupmanager\Module',
            // path to directory for the backups
            'path' => 'dbbackups/',
            // list of registerd db-components
            'dbList' => ['db', 'db1', 'db2'],
            // process timeout
            'timeout' => 3600,
            // additional mysqldump/pg_dump presets (available for choosing in backup forms)
            'customDumpOptions' => [
                'mysqlForce' => '--force',
                'somepreset' => '--triggers --single-transaction',
                'pgCompress' => '-Z2 -Fc',
            ],
           
            // options for full customizing default command generation
            'mysqlManagerClass' => 'CustomClass',
            'postgresManagerClass' => 'CustomClass',
            // option for add additional BackupManagers
            'createManagerCallback' => function($dbInfo) {
                if ($dbInfo['dbName'] == 'exclusive') {
                    return new MyExclusiveManager;
                } else {
                    return false;
                }
            }
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ],
    ],
```

## Console config

```php
    'backup-manager' => [
            'class' => 'taktwerk\yiiboilerplate\modules\backupmanager\Module',
            // path to directory for the backups
            'path' => 'dbbackups/',
            // list of registerd database-components
            'dbList' => [
                'db'
            ],
       ],
```


## Usage

Pretty url's ```/backup-manager```

No pretty url's ```index.php?r=backup-manager```

## Console usage

```
```-db``` - databse component, default value: `db`

```-gzip``` - gzip archive, value : 0 or 1, default value:1

```
Create backup

```bash
php yii backup/create --gzip=1
```


Deleting all backups

```bash
php yii backup/delete-all-oldbackup
```

Test database connection

```bash
php yii backup/test-connection -db=db
```