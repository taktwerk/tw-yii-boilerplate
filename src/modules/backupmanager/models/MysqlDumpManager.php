<?php
namespace taktwerk\yiiboilerplate\modules\backupmanager\models;

use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use taktwerk\yiiboilerplate\components\Helper;

/**
 * Class MysqlDumpManager.
 */
class MysqlDumpManager extends BaseDumpManager
{

    /**
     *
     * @param
     *            $path
     * @param array $dbInfo
     * @param array $dumpOptions
     * @return mixed
     */
    public function makeDumpCommand($path, array $dbInfo, array $dumpOptions)
    {
        // default port
        if (empty($dbInfo['port'])) {
            $dbInfo['port'] = '3306';
        }
        $program = 'mysqldump';
        if(!Helper::shellProgramExists($program)){
            return false;
        }
        if(!empty($dbInfo['password'])){
        $dbPassword = '"' . $dbInfo['password'] . '"';
        }
        else 
        {
            $dbPassword = "";
        }
        $arguments = [
            'mysqldump',
            '--host=' . $dbInfo['host'],
            '--port=' . $dbInfo['port'],
            '--user=' . $dbInfo['username'],
            '--password='. $dbPassword
        ];
        if ($dumpOptions['schemaOnly']) {
            $arguments[] = '--no-data';
        }
        if ($dumpOptions['ignoreTables']) {
            $ignoreTbls = $dumpOptions['ignoreTables'];
            if(is_string($ignoreTbls)){
                $ignoreTbls = array_filter(explode(',',$ignoreTbls));
            }
            if(is_array($ignoreTbls)){
                foreach($ignoreTbls as $tbl){
                    $arguments[] .= " --ignore-table=".$dbInfo['dbName'].'.'.$tbl;
                }
            }
        }
        if ($dumpOptions['preset']) {
            $arguments[] = trim($dumpOptions['presetData']);
        }
        $arguments[] = $dbInfo['dbName'];
        if ($dumpOptions['isArchive']) {
            $arguments[] = '|';
            $arguments[] = 'gzip';
        }
        $arguments[] = '>';
        $arguments[] = '"'.$path.'"';
        return implode(' ', $arguments);
    }

    /**
     *
     * @param
     *            $path
     * @param array $dbInfo
     * @param array $restoreOptions
     * @return mixed
     */
    public function makeRestoreCommand($path, array $dbInfo, array $restoreOptions)
    {
        $arguments = [];
        if (StringHelper::endsWith($path, '.gz', false)) {
            $arguments[] = 'gunzip -c';
            $arguments[] = $path;
            $arguments[] = '|';
        }
        // default port
        if (empty($dbInfo['port'])) {
            $dbInfo['port'] = '3306';
        }
        $arguments = ArrayHelper::merge($arguments, [
            'mysql',
            '--host=' . $dbInfo['host'],
            '--port=' . $dbInfo['port'],
            '--user=' . $dbInfo['username'],
            '--password=' . $dbInfo['password']
        ]);
        if ($restoreOptions['preset']) {
            $arguments[] = trim($restoreOptions['presetData']);
        }
        $arguments[] = $dbInfo['dbName'];
        if (! StringHelper::endsWith($path, '.gz', false)) {
            $arguments[] = '<';
            $arguments[] = $path;
        }

        return implode(' ', $arguments);
    }
}
