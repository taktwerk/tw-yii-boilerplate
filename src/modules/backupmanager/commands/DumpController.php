<?php
namespace taktwerk\yiiboilerplate\modules\backupmanager\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\FileHelper;
use taktwerk\yiiboilerplate\modules\backupmanager\models\Dump;
use PDO;
use PDOException;
use Symfony\Component\Process\Process;
use taktwerk\yiiboilerplate\modules\backupmanager\Module;
use taktwerk\yiiboilerplate\modules\backupmanager\contracts\Mysqldump;


/**
 * Database backup manager.
 */
class DumpController extends Controller
{

    public $db = 'db';

    public $gzip = true;

    public $storage = false;

    public $file = null;

    public $defaultAction = 'create';

    public function options($actionID)
    {
        return array_merge(
            parent::options($actionID),
            [
                'db',
                'gzip',
                'storage',
                'file',
                'interactive'
            ]);
    }

    public function optionAliases()
    {
        return array_merge(parent::optionAliases(), [
            'db' => 'db',
            'gz' => 'gzip',
            's' => 'storage',
            'f' => 'file'
        ]);
    }

    /**
     *
     * @return Module
     */
    public function getModule()
    {
        return Yii::$app->getModule('backup-manager');
    }

    /**
     * Create database backup.
     */
    public function actionCreate()
    {
        $model = new Dump($this->getModule()->dbList);

        if (ArrayHelper::isIn($this->db, $this->getModule()->dbList)) {
            $dbInfo = $this->getModule()->getDbInfo($this->db);
            $dumpOptions = $model->makeDumpOptions();
            if ($this->gzip) {
                $dumpOptions['isArchive'] = true;
            }

            $manager = $this->getModule()->createManager($dbInfo);
            $dumpPath = $manager->makeName($dbInfo, $dumpOptions);
            $tempPath = \Yii::getAlias('@runtime/tmp/' . $dumpPath);
            $dumpCommand = $manager->makeDumpCommand($tempPath, $dbInfo, $dumpOptions);

            if ($dumpCommand == false) {
                $dump = new Mysqldump("{$dbInfo['dsn']}", "{$dbInfo['username']}", "{$dbInfo['password']}", [
                    'compress' => $dumpOptions['isArchive'] ? Mysqldump::GZIP : Mysqldump::NONE,
                    'no-data' => $dumpOptions['schemaOnly'] ? true : false
                ]);

                $dump->start($tempPath);
                $stream = fopen($tempPath, 'r+');
                Yii::$app->fs->putStream($this->getModule()->path . $dumpPath, $stream);
                fclose($stream);
                FileHelper::unlink($tempPath);
            } else {
                Yii::trace(compact('dumpCommand', 'dumpPath', 'dumpOptions'), get_called_class());

                $process = new Process($dumpCommand);
                $process->setTimeout($this->getModule()->timeout);
                $process->run();
                if ($process->isSuccessful()) {
                    $stream = fopen($tempPath, 'r+');
                    Yii::$app->fs->putStream($this->getModule()->path . $dumpPath, $stream);
                    fclose($stream);
                    FileHelper::unlink($tempPath);
                    $this->stdout('Backup successfully created.'.PHP_EOL);
                } else {
                    $this->stdout('Backup failed create.'.PHP_EOL);
                }
            }
        } else {
            $this->stdout('Database configuration not found.'.PHP_EOL);
        }
    }


    /**
     * Deleting all backups.
     */
    public function actionDeleteAll()
    {
        if($this->confirm("Do you want to delete all backups?")){
        if (! empty($this->getModule()->getFileList())) {
                $fail = [];
                foreach ($this->getModule()->getFileList() as $file) {
                    
                    if (!Yii::$app->fs->delete($file['path'])) {
                        $fail[] = $file;
                    }
                }
                if (empty($fail)) {
                    $this->stdout('All backups successfully removed.'.PHP_EOL);
                } else {
                    $this->stdout('Error deleting backups.'.PHP_EOL);
                }
            }
        }
    }

    /**
     * Deleting all old backups.
     */
    public function actionDeleteAllOldbackup()
    {
        $numberDays = getenv('DB_BACKUP_MAX_RETAIN_DAYS');
        $dbBackupMaxRetainDays = ! empty($numberDays) ? $numberDays : '7';

        if($this->confirm("Do you want to delete backups older than {$numberDays} days?")){
            if (! empty($this->getModule()->getFileList())) {
                $fail = [];
                foreach ($this->getModule()->getFileList() as $file) {
                    $fileCreatedTime = $file['timestamp'];
                    $daysAgo = time() - 60 * 60 * 24 * $dbBackupMaxRetainDays;
                    if ($fileCreatedTime < $daysAgo) {
                        if (!Yii::$app->fs->delete($file['path'])) {
                            $fail[] = $file;
                        }
                    }
                }

                if (empty($fail)) {
                    $this->stdout('All backups successfully removed.'.PHP_EOL);

                } else {
                    $this->stdout('Error deleting backups.'.PHP_EOL);
                }
            }
      }
    }

    /**
     * Test connection to database.
     */
    public function actionTestConnection()
    {
        if (ArrayHelper::isIn($this->db, $this->getModule()->dbList)) {
            $info = $this->getModule()->getDbInfo($this->db);
            try {
                new PDO($info['dsn'], $info['username'], $info['password']);
                $this->stdout('Connection success.');
            } catch (PDOException $e) {
                $this->stdout('Connection failed: ' . $e->getMessage().PHP_EOL);
            }
        } else {
            $this->stdout('Database configuration not found.'.PHP_EOL);
        }
    }
}
