<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use taktwerk\yiiboilerplate\modules\backupmanager\models\BaseDumpManager;

/* @var $this yii\web\View */
/* @var \taktwerk\yiiboilerplate\modules\backupmanager\models\Restore $model */
/* @var string $file */
/* @var int $id */

$this->title = Yii::t('twbp', 'Restore');
$this->params['breadcrumbs'][] = ['label' => Yii::t('twbp', 'Backup manager'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="backupmanager-default-restore">

    <div class="well">
        <h4><?= Yii::t('twbp', 'Restore') . ': ' .  $file ?></h4>
        <?php $form = ActiveForm::begin([
            'action' => ['restore', 'id' => $id],
            'method' => 'post',
        ]) ?>

        <?= $form->errorSummary($model) ?>

        <?= $form->field($model, 'db')->dropDownList($model->getDBList(), ['prompt' => '']) ?>

        <?php if (!BaseDumpManager::isWindows()) {
            echo $form->field($model, 'runInBackground')->checkbox();
        } ?>

        <?php if ($model->hasPresets()): ?>
            <?= $form->field($model, 'preset')->dropDownList($model->getCustomOptions(), ['prompt' => '']) ?>
        <?php endif ?>

        <?= Html::submitButton(Yii::t('twbp', 'Restore'), ['class' => 'btn btn-success']) ?>

        <?php ActiveForm::end() ?>
    </div>

</div>
