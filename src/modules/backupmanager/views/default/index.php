<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\grid\GridView;
use taktwerk\yiiboilerplate\modules\backupmanager\models\BaseDumpManager;
use taktwerk\yiiboilerplate\components\Helper;

/* @var $this yii\web\View */
/* @var array $dbList */
/* @var array $activePids */
/* @var \taktwerk\yiiboilerplate\modules\backupmanager\models\Dump $model */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = Yii::t('twbp', 'Backup manager');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php if(Yii::$app->session->hasFlash('success')):?>
<div class="success-summary alert alert-success">
        <?php
    $message = Yii::$app->session->getFlash('success');
    if (is_array($message)) {
        echo "<ul>";
        foreach ($message as $success) {
            echo "<li>" . $success . "</li>";
        }
        echo "</ul>";
    } else {
        echo "<p>" . $message . "</p>";
    }
    ?>
       </div>
<?php endif; ?>
    <?php if(Yii::$app->session->hasFlash('error')):?>
<div class="error-summary alert alert-error">
        <?php
        $message = Yii::$app->session->getFlash('error');
        if (is_array($message)) {
            echo "<p>Please fix the following errors</p>";
            echo "<ul>";
            foreach ($message as $error) {
                echo "<li>" . $error . "</li>";
            }
            echo "</ul>";
        } else {
            echo "<p>" . $message . "</p>";
        }
        ?>
    </div>
<?php endif; ?>
<div class="box box-default">

	<div class="well">
        <?php
        
        $form = ActiveForm::begin([
            'action' => [
                'create'
            ],
            'method' => 'post',
            'layout' => 'inline',
            'class' => 'kv-panel-before'
        ])?>

        <?= $form->field($model, 'db')->dropDownList(array_combine($dbList, $dbList)) ?>

        <?= $form->field($model, 'isArchive')->checkbox(['class' => 'select-on-check-all', 'checked' => true]) ?>

        <?= $form->field($model, 'schemaOnly')->checkbox(['class' => 'select-on-check-all ']) ?>

        <?php
        
        // if (! BaseDumpManager::isWindows()) {
        // echo $form->field($model, 'runInBackground')->checkbox();
        // }
        ?>

        <?php if ($model->hasPresets()): ?>
            <?= $form->field($model, 'preset')->dropDownList($model->getCustomOptions(), ['prompt' => '']) ?>
        <?php endif ?>

        <?= Html::submitButton('<i class="glyphicon glyphicon-hdd"></i> ' . Yii::t('twbp', 'Create dump'), ['class' => 'btn btn-success create-new']) ?>

        <?php ActiveForm::end() ?>
    </div>

    <?php if (!empty($activePids)): ?>
        <div class="giiant-crud box-body guide-index">
		<h4><?= Yii::t('twbp', 'Active processes') ?></h4>
            <?php foreach ($activePids as $pid => $cmd): ?>
                <b><?= $pid ?></b>: <?= $cmd ?><br>
            <?php endforeach ?>
        </div>
    <?php endif ?>

    <!-- <p>
        <?
        /*
         * = Html::a(Yii::t('twbp', 'Delete all'),
         * ['delete-all'],
         * [
         * 'class' => 'btn btn-danger',
         * 'data-method' => 'post',
         * 'data-confirm' => Yii::t('twbp', 'Are you sure?'),
         * ]
         * )
         */
        ?>
    </p> -->
<?php 
//prd($dataProvider->models);
?>
    <?=\taktwerk\yiiboilerplate\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'kv-grid-table table table-hover table-bordered table-striped kv-table-wrap'],
        'rowOptions' => ['class' => 'guide-grid'],
        'containerOptions' => ['class' => 'giiant-crud box-body guide-index'],
        'summaryOptions' => ['class' => 'kv-panel-before'],
        'columns' => [
            [
                'attribute' => 'type',
                'label' => Yii::t('twbp', 'Type'),
                'contentOptions' => ['class' => 'guide-grid']
                
            ],
            [
                'attribute' => 'name',
                'label' => Yii::t('twbp', 'Name'),
                'contentOptions' => ['class' => 'guide-grid']
                
            ],
            [
                'attribute' => 'size',
                'label' => Yii::t('twbp', 'Size'),
                'contentOptions' => ['class' => 'guide-grid']
                
            ],
            [
                'attribute' => 'create_at',
                'label' => Yii::t('twbp', 'Create time'),
                'contentOptions' => ['class' => 'guide-grid']
                
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{download} {delete}',
                'headerOptions' => ['class' => 'text-center'],
                'buttons' => [
                    'download' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-download-alt"></span>', 
                            Helper::getFile("elfinder", "fs", Yii::$app->getModule('backup-manager')->path . "/" . $model['name']), 
                            [
                                'title' => Yii::t('twbp', 'Download'),
                                'class' => 'btn btn-sm btn-default'
                                
                            ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', 
                            [
                                'delete','name' => $model['name']
                                
                            ],
                            ['title' => Yii::t('twbp', 'Delete'),
                                'data-method' => 'post',
                                'data-confirm' => Yii::t('twbp', 'Are you sure?'),
                                'class' => 'btn btn-sm btn-danger'
                                
                            ]);
                    }
            ]
            ]
        ],
        'pager' => [
            'class' => yii\widgets\LinkPager::class,
            'firstPageLabel' => Yii::t('twbp', 'First'),
            'lastPageLabel' => Yii::t('twbp', 'Last'),
        ],
    ])?>

</div>
