<?php

namespace taktwerk\yiiboilerplate\modules\env\controllers;

use yii\web\Controller;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;
use yii\web\Response;

/**
 * Default controller for the `env` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {   $envList = $_ENV;
        ksort($envList);
        return $this->render('index',['model'=>$envList]);
    }
    public function actionAppConfig(){
        require(\Yii::getAlias('@app/config/env.php'));
        $appConfig = require(\Yii::getAlias('@app/config/main.php'));
        $loadedComponentsConfig = \Yii::$app->getComponents();
        $appConfig['components'] = $loadedComponentsConfig;
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $appConfig;
    }
}
