<?php

namespace taktwerk\yiiboilerplate\modules\env;

use taktwerk\yiiboilerplate\components\MenuItemInterface;

/**
 * env module definition class
 */
class Module extends \taktwerk\yiiboilerplate\components\TwModule implements MenuItemInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\env\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }

    public function getMenuItems()
    {
        // example for settings entry
        /* $items = [
            [
                'label' => \Yii::t('twbp', 'Env'),
                'url' => 'javascript:',
                'icon' => 'fa fa-cog',
                'visible' => \Yii::$app->user->can('Authority')
            ],
        ];
        return $items; */
    }
}
