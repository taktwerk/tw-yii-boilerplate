<?php
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\helpers\Json;

/**
 *
 * @var yii\web\View $this
 */

$this->title = 'Environment Config';
$this->params['breadcrumbs'][] = $this->title;
$css = <<<EOT
.jproperty{
    color: #204a87;
}
.jstring{
    color: #4e9a06;
}
.json-data{
    background-color: #fff;
}
.json-data ul,
.json-data ol,
.json-data ul li ul{
    list-style: none;
}
EOT;
$this->registerCss($css);
?>
<div class="box box-default">
	<div class="giiant-crud box-body" id="env-view">
		<div class="clearfix crud-navigation">
			<!-- menu buttons -->
			<div class='pull-left'></div>
			<div class="pull-right"></div>
		</div>
		<ul class="nav nav-tabs">
			<li class="active"><a href="#one" data-toggle="tab"><?php echo \Yii::t('twbp','Env Variables')?></a></li>
			<li><a href="#two" data-toggle="tab"><?php echo \Yii::t('twbp','App Config')?></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade active in" id="one">
       	<?php
        $attribs = [];
        foreach ($model as $k => $i) {
            
            if (is_array($i) || is_object($i)) {
                $attribs[] = [
                    'format' => 'html',
                    'attribute' => $k,
                    'value' => '<pre>' . print_R($i, true) . "</pre>"
                ];
            } else {
                $attribs[] = $k;
            }
        }        
        ?>
       		 <?=DetailView::widget(['model' => $model,'attributes' => $attribs]);?>
       	</div>
			<div class="tab-pane fade"  id="two">
				<pre class="json-data">
    	</pre>
			</div>
		</div>
	</div>
</div>
<?php
$url = Url::toRoute([
    '/env/default/app-config'
]);
$jhre = <<<EOT
    {
       var isArray = function(a) {
        return (!!a) && (a.constructor === Array);
        };
       var formatJson =  function formatJsonwithHtml(data){
            if(data==undefined){
            return;
            }
            var html = "";
            var itemCount = Object.keys(data).length;
            var i = 0;
            if(isArray(data)){
                html += "<span class='jarray'><span class='toggle'>[</span><ol>";
                data.forEach(function(item, index){
                    if(typeof item === 'object'){
                        html += formatJson(item);
                    }else{
                        html += '<li><span class="aa jstring">"'+item;
                        if(itemCount==(i+1)){
                            html += '"</span></li>';
                        }else{
                            html += '"</span>,</li>';
                        } 
                   }
                    i++;
                });
                html += '</ol><span class="toggle-end" card="8">]</span></span>';
             }
            else if(typeof data === "object"){
                html += "<span class='jobject'><span class='toggle'>{</span><ul>";
                for (var key in data) {
                  if (data.hasOwnProperty(key)) {
                    var val = data[key];
                    if(typeof val === 'object'){
                        html += '<li><span class="jproperty"><span class="p collapsible">"' + key+ '"</span></span>:';
                        var temp = formatJson(val); 
                        html += (temp===undefined)?'null':temp;
                        html +=',</li>';
                    }else{
                        html += '<li><span class="jproperty">"'+key+'"</span>:<span class="jstring">"'+val+'"</span>';
                        if(itemCount==(i+1))
                         {html += '</li>';
                        }else{
                        html += ',</li>';}          
                    }
                  }
                  i++;
                }
                html += '</ul><span class="toggle-end">}</span></span>';
            }
            html += "</span>";
            return html;
        }
        var loadConfig = function loadAppConfig(maxAttempt){
            $.ajax({
            url:'{$url}',
            success:function(data){
                    var json  =data;
                    $('.json-data').html(formatJson(json));//JSON.stringify(data, undefined, 2));
            },
            error:function(error){
                if(error.status==500 && maxAttempt>=1)
                {
                    loadConfig(maxAttempt-1);
                }
            }
            });
       }
       loadConfig(4);
    }

EOT;
$this->registerJs($jhre);
?>