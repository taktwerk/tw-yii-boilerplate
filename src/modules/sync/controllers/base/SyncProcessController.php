<?php
//Generation Date: 11-Sep-2020 02:57:24pm
namespace taktwerk\yiiboilerplate\modules\sync\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * SyncProcessController implements the CRUD actions for SyncProcess model.
 */
class SyncProcessController extends TwCrudController
{
}
