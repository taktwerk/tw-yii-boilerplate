<?php
//Generation Date: 14-May-2021 12:47:30pm
namespace taktwerk\yiiboilerplate\modules\sync\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * SyncIndexController implements the CRUD actions for SyncIndex model.
 */
class SyncIndexController extends TwCrudController
{
}
