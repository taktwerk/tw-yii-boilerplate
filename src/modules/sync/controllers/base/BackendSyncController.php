<?php

namespace taktwerk\yiiboilerplate\modules\sync\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;

/**
 * BackendSyncController implements the CRUD actions for  model.
 */
class BackendSyncController extends TwCrudController
{
}
