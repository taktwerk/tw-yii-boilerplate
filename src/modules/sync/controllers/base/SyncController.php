<?php

namespace taktwerk\yiiboilerplate\modules\sync\controllers\base;

use taktwerk\yiiboilerplate\modules\sync\models\SyncProcess;
use taktwerk\yiiboilerplate\modules\user\models\UserDevice;
use taktwerk\yiiboilerplate\rest\components\SyncHelper;
use taktwerk\yiiboilerplate\rest\TwMobileApplicationController;
use taktwerk\yiiboilerplate\traits\MobileDateTrait;
use yii\helpers\ArrayHelper;
use Yii;
use taktwerk\yiiboilerplate\rest\exceptions\MissingCurrentDatetimeException;
use yii\web\Response;
use yii\db\Transaction;
use DateTime;
use taktwerk\yiiboilerplate\rest\exceptions\WrongSyncProcessIdException;
use taktwerk\yiiboilerplate\rest\exceptions\MissingDeviceInfoHeaderException;

abstract class SyncController extends TwMobileApplicationController
{
    use MobileDateTrait;
    /**
     * @var array list of models to add to the sync data
     */
    protected $models = [];
    const MAX_SYNC_ITEMS = 200;

    public $enableCsrfValidation = false;

    /**
     * List of models to add to the sync data
     */
    protected function getModels()
    {
        $syncModule = Yii::$app->getModule('sync');
        if (!$syncModule || empty($syncModule->models) || !is_array($syncModule->models)) {
            return [];
        }

        return $syncModule->models;
    }

    /**
     * Override the Actions to add the Search capability
     */
    public function actions()
    {
        return [
            'index',
            'check-available-data',
            'save-progress',
            // Need to support options for Ionic apps
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ]
        ];
    }

    /**
     * Override the verbs to add the Batch function.
     * @return array
     */
    protected function verbs()
    {
        return ArrayHelper::merge(parent::verbs(), [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::class,
                'actions' => [
                    '*' => ['OPTIONS'],
                ],
            ],
            'batch-all' => ['POST'],
            'info' => ['GET'],
        ]);
    }

    /**
     * Add our behaviors for the API
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: authorization');
        header('Access-Control-Allow-Credentials: true');
        $behaviors['corsFilter']  = [
            'class' => \yii\filters\Cors::className(),
        ];

        return $behaviors;
    }

    /**
     * Get all new models since last sync
     * @param null $lastUpdatedAt
     * @return array sync data
     */
    public function actionIndex()
    {
        $lastUpdatedAt = Yii::$app->getRequest()->get('lastUpdatedAt');
        if($lastUpdatedAt) {
            $lastUpdatedAt = (new \DateTime($lastUpdatedAt, new \DateTimeZone("UTC")))->add(new \DateInterval('PT1S'))->format('c');
        }
        $syncProcessId = Yii::$app->getRequest()->get('syncProcessId');
        $lastUpdatedNumber = Yii::$app->getRequest()->get('lastUpdatedNumber');
        $appDataVersion = Yii::$app->user->identity->app_data_version;
        Yii::$app->response->format = Response::FORMAT_JSON;

        $serverTime = new \DateTime();
        $differenceInSeconds = ($serverTime->getTimestamp() - $this->getClientDateTime()->getTimestamp());
        $lastUpdatedAt = $this->getSqlLastUpdatedAt($lastUpdatedAt);
        /// currentDate to prohibit synchronization of data that is created in parallel.
        $currentDate = $this->getConvertDateToSqlTimezone(
            (new \DateTime("now", new \DateTimeZone("UTC")))->format('Y-m-d H:i:s')
        );
        if ($syncProcessId) {
            $syncProcess = SyncProcess::find()
                ->joinWith('device')
                ->where([
                        'sync_process.id' => $syncProcessId,
                        'user_device.user_id' => Yii::$app->user->identity->id,
                    ])
                ->one();
            if (!$syncProcess) {
                throw new WrongSyncProcessIdException('Sync process id is wrong.');
            }
        } else {
            $deviceInfo = $this->getDeviceInfo($syncProcessId);
            $syncProcess = new SyncProcess();
            $syncProcess->device_id = $deviceInfo->id;
            $syncProcess->all_items_count = 0;
            $syncProcess->progress = 0;
            $syncProcess->synced_items_count = 0;
            $syncProcess->save(false);
        }

        $models = $this->getModels();
        $modelsSyncData = [];
        $lastSyncModel = null;
        $countOfModels = 0;
        foreach ($models as $modelName) {
            $model = Yii::createObject($modelName);
            $tableName = $model->tableName();
            try {
                if (!empty($lastUpdatedNumber)) {
                    $countOfCurrentModel = $modelName::find(false)
                        ->updated($lastUpdatedAt)
                        ->api()
                        ->count();
                    if ($countOfModels + $countOfCurrentModel > $lastUpdatedNumber) {
                        $offset = $lastUpdatedNumber - $countOfModels;
                        $modelsSyncData[$tableName] = $modelName::find(false)
                            ->offset($offset)
                            ->updated($lastUpdatedAt, $currentDate)
                            ->api()
                            ->all();
                        $lastUpdatedNumber = null;
                        continue;
                    }
                    $countOfModels += $countOfCurrentModel;
                } else {
                    $modelsSyncData[$tableName] = $modelName::find(false)
                        ->updated($lastUpdatedAt, $currentDate)
                        ->api()
                        ->all();
                }
                $lastUpdatedAtModelDate = $modelName::find(false)
                    ->updated($lastUpdatedAt, $currentDate)
                    ->orderBy(['updated_at' => SORT_DESC])
                    ->one();
                if (empty($lastSyncModel) ||
                    strtotime($lastSyncModel->updated_at) < strtotime($lastUpdatedAtModelDate->updated_at)
                ) {
                    $lastSyncModel = $lastUpdatedAtModelDate;
                }
            } catch (\Exception $e) {
                //TODO add error
            }
        }
        $syncData = [
            'difference' => $differenceInSeconds,
            'syncProcessId' => $syncProcess->id,
            'models' => $modelsSyncData,
            'version' => $appDataVersion,
            'lastModelUpdatedAt' => $lastSyncModel ?
                $this->getUtcTimeByAttribute($lastSyncModel->updated_at) :
                null,
        ];

        return $syncData;
    }

    public function actionCheckAvailableData($appDataVersion = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $currentAppVersionData = Yii::$app->user->identity->getAttribute('app_data_version');

        if (!$appDataVersion && $currentAppVersionData) {
            return ['result' => true];
        }
        if ((int) $appDataVersion !== $currentAppVersionData) {
            return ['result' => true];
        }

        return ['result' => false];
    }

    public function actionSaveProgress()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $synchronizationInfo = Yii::$app->getRequest()->getBodyParams();
        if (empty($synchronizationInfo['id']) || empty($synchronizationInfo['uuid'])) {
            return ['result' => false];
        }
        $userDevice = $deviceInfoModel = UserDevice::findOne(
            ['user_id' => Yii::$app->user->identity->id, 'uuid' => $synchronizationInfo['uuid']]
        );
        if (!$userDevice || !$userDevice->id) {
            return ['result' => false];
        }
        unset($synchronizationInfo['uuid']);
        $synchronizationInfo['device_id'] = $userDevice->id;
        $syncProcessForm = SyncProcess::findOne($synchronizationInfo['id']);
        if ($syncProcessForm->load($synchronizationInfo, '') &&
            $syncProcessForm->save(true)
        ) {
            return ['result' => true];
        }

        return ['result' => false];
    }

    /**
     * Get device info model
     * @return UserDevice|null
     */
    protected function getDeviceInfo(int $syncProcessId = null)
    {
        $deviceInfoModel = null;
        $deviceInfoHeaderData = Yii::$app->getRequest()->getHeaders()->get('X-Device-Info');
        if (empty($deviceInfoHeaderData) || !is_string($deviceInfoHeaderData)) {
            throw new MissingDeviceInfoHeaderException('Missing X-Device-Info header in request.');
        }
        $deviceInfo = json_decode($deviceInfoHeaderData, true);
        if ($deviceInfo && $deviceInfo['uuid']) {
            $deviceInfoModel = UserDevice::findOne(
                ['user_id' => Yii::$app->user->identity->id, 'uuid' => $deviceInfo['uuid']]
            );
        }
        if (!$deviceInfoModel) {
            $deviceInfoModel = new UserDevice();
            $deviceInfoModel->user_id = Yii::$app->user->identity->id;
            $deviceInfoModel->uuid = $deviceInfo['uuid'];
            $deviceInfoModel->device_information = $deviceInfoHeaderData;
            $deviceInfoModel->save();
        }

        return $deviceInfoModel;
    }

    public function actionInfo($lastUpdatedAt = null)
    {
        $lastUpdatedAt = null;
        Yii::$app->response->format = Response::FORMAT_JSON;

        $lastUpdatedAt = $this->getSqlLastUpdatedAt($lastUpdatedAt);

        // Calculate difference from "now" of request with "now" of server
        $serverTime = new \DateTime();
        $models = $this->getModels();
        $countOfAllItems = 0;
        foreach ($models as $modelName) {
            $model = Yii::createObject($modelName);

            $modelItemsCount = $model::find(false)
                ->updated($lastUpdatedAt)
                ->api()
                ->count();

            $countOfAllItems += $modelItemsCount;
        }

        $syncInfoData['countOfAllItems'] = $countOfAllItems;
        $syncInfoData['countOfPages'] = ceil($countOfAllItems / self::MAX_SYNC_ITEMS);

        return $syncInfoData;
    }

    public function actionBatchAll()
    {
        $mappingId = Yii::$app->getRequest()->get('mapping_id', '_id');
        $lookupId = null;
        $lookup = Yii::$app->getRequest()->get('lookup_id', null);
        if (!empty($lookup)) {
            $lookupId = explode(',', $lookup);
        }
        $data = Yii::$app->getRequest()->getBodyParams();
        if (empty($data)) {
            $data = file_get_contents("php://input");
        }

        $transaction = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);

        $errors = [];
        $returnModels = [];
        $models = $this->getModels();
        foreach ($models as $model) {
            $tableName = $model::getTableSchema()->name;
            if (empty($data[$tableName])) {
                continue;
            }
            $dataForSaving = $data[$tableName];
            $syncComponent = Yii::createObject(['class' => SyncHelper::class], [$model, $mappingId, $lookupId]);

            $returnModels[$tableName] = $syncComponent->saveSyncData($dataForSaving);

            if ($syncComponent->errors) {
                $errors[$tableName] = $syncComponent->errors;

                continue;
            }
        }

        if ($errors) {
            $transaction->rollBack();

            return $errors;
        }

        $transaction->commit();

        return $returnModels;
    }

    /**
     * Get the last sync with the server
     * @param $lastUpdatedAt
     * @return DateTime|null
     */
    protected function getSqlLastUpdatedAt($lastUpdatedAt)
    {
        /// $lastUpdatedAt - UTC date
        if (empty($lastUpdatedAt)) {
            return null;
        }
        // Replace a space with a plus. Yii does this.
        return $this->getConvertDateToSqlTimezone(str_replace(' ', '+', $lastUpdatedAt));
    }

    protected function getConvertDateToSqlTimezone($date, $isAnti = false)
    {
        $date = new \DateTime($date);
        /// Search difference beetween current UTC date and SQL time
        $sqlCurrentTime = Yii::$app->getDb()->createCommand('SELECT NOW() as currentTime')->queryAll();
        $sqlCurrentTimeInSeconds = 0;
        if (!empty($sqlCurrentTime[0]) && !empty($sqlCurrentTime[0]['currentTime'])) {
            $tz = date_default_timezone_get();
            date_default_timezone_set('UTC');
            $sqlCurrentTimeInSeconds = strtotime($sqlCurrentTime[0]['currentTime']);
            date_default_timezone_set($tz);
        }
        $utcTimeInSeconds = (new \DateTime("now", new \DateTimeZone("UTC")))->getTimestamp();
        $differenceBeetweenSqlTimeAndPhp = $sqlCurrentTimeInSeconds - $utcTimeInSeconds;
        $sqlDate = $date;
        if ($differenceBeetweenSqlTimeAndPhp > 0) {
            if ($isAnti) {
//                $differenceBeetweenSqlTimeAndPhp = $differenceBeetweenSqlTimeAndPhp * -1;
                $sqlDate->sub(new \DateInterval('PT' . $differenceBeetweenSqlTimeAndPhp . 'S'));
            } else {
                $sqlDate->add(new \DateInterval('PT' . $differenceBeetweenSqlTimeAndPhp . 'S'));
            }

        }
        if ($differenceBeetweenSqlTimeAndPhp < 0) {
            if (!$isAnti) {
//                $differenceBeetweenSqlTimeAndPhp = $differenceBeetweenSqlTimeAndPhp * -1;
                $sqlDate->sub(new \DateInterval('PT' . $differenceBeetweenSqlTimeAndPhp . 'S'));
            } else {
                $sqlDate->add(new \DateInterval('PT' . $differenceBeetweenSqlTimeAndPhp . 'S'));
            }
        }

        return $sqlDate;
    }

    /**
     * Get the client's datetime as sent in the header.
     * @return \DateTime
     * @throws MissingCurrentDatetimeException
     */
    protected function getClientDateTime()
    {
        $currentDate = Yii::$app->getRequest()->getHeaders()->get('X-CURRENT-DATETIME');
        if (empty($currentDate) || !is_string($currentDate)) {
            throw new MissingCurrentDatetimeException('Missing X-CURRENT-DATETIME header in request.');
        }

        return new \DateTime($currentDate);
    }
}
