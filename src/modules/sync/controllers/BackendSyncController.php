<?php

namespace taktwerk\yiiboilerplate\modules\sync\controllers;

use Yii;
use taktwerk\yiiboilerplate\modules\feedback\models\Feedback;
use yii\helpers\Url;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\web\UploadedFile;

/**
 * This is the class for controller "BackendSyncController".
 */
class BackendSyncController extends \taktwerk\yiiboilerplate\modules\sync\controllers\base\BackendSyncController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\sync\models\SyncProcess';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\sync\models\search\SyncProcess';


    /**
     * Additional actions for controllers, uncomment to use them
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['update', 'view'],
            'rules' => [
                [
                    'allow' => true,
                    'actions' => [
                        'index',
                        'view',
                        'delete',
                        'delete-multiple',
                    ],
                    'roles' => ['@']
                ]
            ]
        ];

        return $behaviors;
    }
}
