<?php


namespace taktwerk\yiiboilerplate\modules\sync\controllers;


use taktwerk\yiiboilerplate\rest\filters\CheckApplicationVersionMethod;
use taktwerk\yiiboilerplate\helpers\CorsFilterHelper;

class SyncController extends \taktwerk\yiiboilerplate\modules\sync\controllers\base\SyncController
{
    /**
     * Add our behaviors for the API
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $auth = $behaviors['authenticator'] = ['class' => \taktwerk\yiiboilerplate\rest\auth\TwXAuth::class];
        unset($behaviors['authenticator']);
        $behaviors['corsFilter'] = [
            'class' => CorsFilterHelper::class
        ];
        $behaviors['authenticator'] = $auth;
        $behaviors['authenticator']['except'] = ['options'];
        $behaviors['versionChecker'] = [
            'class' => CheckApplicationVersionMethod::class,
            'only' => ['index', 'save-progress'],
            'except' => ['options'],
        ];
        
        return $behaviors;
    }
}
