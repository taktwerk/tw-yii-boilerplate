<?php


namespace taktwerk\yiiboilerplate\modules\sync\traits;


trait SyncInfoTrait
{
    public $inProgressSyncProcessesCount;
    public $pausedSyncProcessesCount;
    public $canceledSyncProcessesCount;
    public $successSyncProcessesCount;
    public $failedSyncProcessesCount;
    public $withoutStatusSyncProcessesCount;
}
