<?php
namespace taktwerk\yiiboilerplate\modules\sync\traits;

trait WebConfigTrait
{
    protected function setWebConfig()
    {
        \Yii::$app->set('user', [
            'class' => \taktwerk\yiiboilerplate\components\WebUser::class,
            'enableSession' => false,
            'enableAutoLogin' => false,
            'identityClass' => \taktwerk\yiiboilerplate\modules\user\models\User::class
        ]);
        \Yii::$app->set('request', [
            'class' => '\yii\web\Request',
            'enableCsrfCookie' => false
        ]);
        $_SERVER['REQUEST_METHOD'] = 'GET'; // To emulate a web request from cli
    }

   protected function unsetWebConfig()
    {
        \Yii::$app->clear('user');
        \Yii::$app->clear('request');
        unset($_SERVER['REQUEST_METHOD']);
    }
}