<?php


namespace taktwerk\yiiboilerplate\modules\sync\traits;

use taktwerk\yiiboilerplate\modules\customer\models\ClientUser;
use taktwerk\yiiboilerplate\modules\user\models\User;
use Yii;
use yii\helpers\ArrayHelper;

trait UserAppDataVersionTrait
{
    public function afterSave($insert, $changedAttributes)
    {
        // This will take care of the profile
        parent::afterSave($insert, $changedAttributes);

        $this->changeUserAppDataVersion($insert, $changedAttributes);
    }

    public function clientsForChangeAppDataVersion()
    {
        if (!empty($this->client_id)) {
            return [$this->client_id];
        }

        return [];
    }

    public function changeUserAppDataVersion($insert, $changedAttributes)
    {
        try {
            $clientUserIds = [];
            $clientIds = $this->clientsForChangeAppDataVersion();
            if ($clientIds) {
                $clientUserIds = ArrayHelper::getColumn(
                    ClientUser::find()
                        ->where(['client_id' => $clientIds])
                        ->all(),
                    'user_id'
                );
            }
            $authorityUserIds = Yii::$app->authManager->getUserIdsByRole('Authority');
            $additionalUserIds = $this->getAdditionalUserIdsForChangeAppDataVersion($insert, $changedAttributes);
            $userIds = array_merge($clientUserIds, $authorityUserIds);
            $userIds = array_merge($userIds, $additionalUserIds);
            $users = User::find()->where(['id' => $userIds])->all();
            foreach ($users as $user) {
                $user->app_data_version++;
                $user->save();
            }
        } catch (\Exception $e) {
            // If something went wrong, and it's not because the column app_data_version doesn't exist yet, we want to catch it.
        }
    }

    public function getAdditionalUserIdsForChangeAppDataVersion($insert, $changedAttributes)
    {
        return [];
    }
}
