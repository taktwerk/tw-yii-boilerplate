<?php

namespace taktwerk\yiiboilerplate\modules\sync\models\mobile;

use taktwerk\yiiboilerplate\modules\sync\models\SyncIndex as BaseSyncIndex;
use taktwerk\yiiboilerplate\traits\MobileDateTrait;

use taktwerk\yiiboilerplate\modules\sync\traits\UserAppDataVersionTrait;
use taktwerk\yiiboilerplate\traits\MobileFileTrait;

/**
 * This is the model class for table "protocol".
 */
class SyncIndex extends BaseSyncIndex
{
    use MobileDateTrait, MobileFileTrait;

    public function beforeSave($insert)
    {
        $this->beforeSaveForLocalDateAttributes($insert);
        return parent::beforeSave($insert);
    }
    public static function find($removeDeleted = true){
        $q = parent::find($removeDeleted);
        $q->andWhere(['user_id'=>\Yii::$app->user->id]);
        return $q;
    }
    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
       return [
            'id' => $this->id,
            'model'=>$this->model,
            'model_id'=>$this->model_id,
            'user_id' => $this->user_id,
            'deleted_at' => $this->getIntDeletedAt(),
            'updated_at' => $this->getIntUpdatedAt(),
            'created_at' => $this->getIntCreatedAt(),
            'local_deleted_at' => $this->getIntLocalDeletedAt(),
            'local_updated_at' => $this->getIntLocalUpdatedAt(),
            'local_created_at' => $this->getIntLocalCreatedAt(),
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ];
    }
}
