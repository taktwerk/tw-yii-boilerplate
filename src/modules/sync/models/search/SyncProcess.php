<?php

namespace taktwerk\yiiboilerplate\modules\sync\models\search;

use Yii;
use taktwerk\yiiboilerplate\modules\sync\models\SyncProcess as SyncProcessModel;
use taktwerk\yiiboilerplate\modules\sync\models\search\base\SyncProcess as SyncProcessSearchModel;
use yii\data\ActiveDataProvider;

class SyncProcess extends SyncProcessSearchModel
{

    public function search($params)
    {
        $query = SyncProcessModel::find()
            ->joinWith(['device']);

        if (!empty($params['SyncProcess']['device_id'])) {
            $query->andFilterWhere(['like', 'user_device.device_information', $params['SyncProcess']['device_id']]);
        }

        $this->parseSearchParams(SyncProcessModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => $this->parseSortParams(SyncProcessModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(SyncProcessModel::class),
                'params' => [
                    'page' => $this->parsePageParams(SyncProcessModel::class),
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
//        $this->applyHashOperator('device_id', $query);
        $this->applyLikeOperator('status', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);

        return $dataProvider;
    }
}
