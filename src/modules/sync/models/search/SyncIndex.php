<?php
//Generation Date: 14-May-2021 12:47:30pm
namespace taktwerk\yiiboilerplate\modules\sync\models\search;

use taktwerk\yiiboilerplate\modules\sync\models\search\base\SyncIndex as SyncIndexSearchModel;

/**
* SyncIndex represents the model behind the search form about `taktwerk\yiiboilerplate\modules\sync\models\SyncIndex`.
*/
class SyncIndex extends SyncIndexSearchModel{

}