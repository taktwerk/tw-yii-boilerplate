<?php
//Generation Date: 11-Sep-2020 02:57:24pm
namespace taktwerk\yiiboilerplate\modules\sync\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\sync\models\SyncProcess as SyncProcessModel;

/**
 * SyncProcess represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\sync\models\SyncProcess`.
 */
class SyncProcess extends SyncProcessModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'device_id',
                    'progress',
                    'all_items_count',
                    'synced_items_count',
                    'status',
                    'description',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SyncProcessModel::find();

        $this->parseSearchParams(SyncProcessModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(SyncProcessModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(SyncProcessModel::class),
                'params' => [
                    'page' => $this->parsePageParams(SyncProcessModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('device_id', $query);
        $this->applyHashOperator('progress', $query);
        $this->applyHashOperator('all_items_count', $query);
        $this->applyHashOperator('synced_items_count', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('status', $query);
        $this->applyLikeOperator('description', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(SyncProcessModel::class);
    }
}
