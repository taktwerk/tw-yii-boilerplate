<?php
//Generation Date: 17-May-2021 10:44:58am
namespace taktwerk\yiiboilerplate\modules\sync\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\sync\models\SyncIndex as SyncIndexModel;

/**
 * SyncIndex represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\sync\models\SyncIndex`.
 */
class SyncIndex extends SyncIndexModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'model',
                    'model_id',
                    'user_id',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'local_created_at',
                    'local_updated_at',
                    'local_deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$showDeleted = (property_exists(\Yii::$app->controller, 'model')
            &&
            (is_subclass_of(SyncIndexModel::class, \Yii::$app->controller->model) 
            	||
            	is_subclass_of(\Yii::$app->controller->model, SyncIndexModel::class))
            &&
              (Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model)
                ||
                Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model)))
            );
        $query = SyncIndexModel::find((Yii::$app->get('userUi')->get('show_deleted', SyncIndexModel::class) === '1'|| $showDeleted)?false:true);

        $this->parseSearchParams(SyncIndexModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(SyncIndexModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(SyncIndexModel::class),
                'params' => [
                    'page' => $this->parsePageParams(SyncIndexModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('model_id', $query);
        $this->applyHashOperator('user_id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('model', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        $this->applyDateOperator('local_created_at', $query, true);
        $this->applyDateOperator('local_updated_at', $query, true);
        $this->applyDateOperator('local_deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(SyncIndexModel::class);
    }
}
