<?php

namespace taktwerk\yiiboilerplate\modules\sync\models;
use taktwerk\yiiboilerplate\modules\user\models\UserDevice;

use \taktwerk\yiiboilerplate\modules\sync\models\base\SyncProcess as BaseSyncProcess;

/**
 * This is the model class for table "sync_process".
 */
class SyncProcess extends BaseSyncProcess
{
    public function isUserDevice($attribute, $params)
    {
        $userDevice = UserDevice::find(['id' => $this->$attribute, 'user_id' => Yii::$app->user->identity->id])->one();

        return (bool) $userDevice;
    }

    public function getUser()
    {
        return $this->device->user;
    }
}
