<?php
//Generation Date: 14-May-2021 12:47:18pm
namespace taktwerk\yiiboilerplate\modules\sync\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\sync\models\base\SyncIndex as BaseSyncIndex;

/**
 * This is the model class for table "sync_index".
 */
class SyncIndex extends BaseSyncIndex
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }
    public function behaviors(){
        $behaviors = parent::behaviors();
        unset($behaviors['syncindex']);//We dont need to add synindex behavior for this model
        return $behaviors;
    }
}
