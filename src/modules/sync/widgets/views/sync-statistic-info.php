<style>
    .hidden {
        display: none;
    }
    .client-name {
        cursor: pointer;
        padding: 5px 5px 5px 5px;
        font-size: 18px;
    }
    .sync-statistic-info {
        padding: 5px 5px 5px 20px;
        font-size: 16px;
    }
    .device-info {
        padding: 15px 5px 5px 15px;
    }
    .device-section {
        cursor: pointer;
    }
</style>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="small-box bg-aqua">
            <div class="inner">
                <p><?=Yii::t('twbp', 'Sync Processes');?></p>
            </div>
            <div class="inner text-left">
                <?php foreach ($clients as $client) { ?>
                    <div id="<?=$client->id?>-client-sync">
                        <div class="client-name">
                            <i class="fa fa-book"></i>
                            <span><?=$client->name?></span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </div>
                        <div class="sync-statistic-info hidden">
                            <div>Success: <?=$client->successSyncProcessesCount?></div>
                            <div>Failed: <?=$client->failedSyncProcessesCount?></div>
                            <div>In Progress: <?=$client->inProgressSyncProcessesCount?></div>
                            <div>Paused: <?=$client->pausedSyncProcessesCount?></div>
                            <div>Canceled: <?=$client->canceledSyncProcessesCount?></div>
                            <div>Unknown: <?=$client->withoutStatusSyncProcessesCount?></div>
                            <?php if ($client->syncedDevices) { ?>
                                <div id="<?=$client->id?>-device-section">
                                    <h5 class="device-section">
                                        <i class="fa fa-phone-square"></i>
                                        Devices (<?=count($client->syncedDevices)?>)
                                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                    </h5>
                                    <div class="device-info-list hidden">
                                        <?php foreach ($client->syncedDevices as $syncedDevice) { ?>
                                            <div class="device-info">
                                                Model: <?=$syncedDevice->deviceModel?>
                                                <br>
                                                User: <?=$syncedDevice->user->username?>
                                                <br>
                                                Last Synced At: <?=$syncedDevice->lastSyncProcess ? $syncedDevice->lastSyncProcess->created_at : '-'?>
                                                <br>
                                                Status: <?=$syncedDevice->lastSyncProcess ? $syncedDevice->lastSyncProcess->status : '-'?>
                                            </div>
                                        <?php }?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if (!empty($authorityDevicesInfo)) { ?>
                    <div id="authority-client-sync">
                        <div class="client-name">
                            <i class="fa fa-book"></i>
                            <span>Authority</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </div>
                        <div class="sync-statistic-info hidden">
                            <div>Success: <?=$authorityDevicesInfo['allSyncProcessesInfo']['successSyncProcessesCount']?></div>
                            <div>Failed: <?=$authorityDevicesInfo['allSyncProcessesInfo']['failedSyncProcessesCount']?></div>
                            <div>In Progress: <?=$authorityDevicesInfo['allSyncProcessesInfo']['inProgressSyncProcessesCount']?></div>
                            <div>Paused: <?=$authorityDevicesInfo['allSyncProcessesInfo']['pausedSyncProcessesCount']?></div>
                            <div>Canceled: <?=$authorityDevicesInfo['allSyncProcessesInfo']['canceledSyncProcessesCount']?></div>
                            <div>Unknown: <?=$authorityDevicesInfo['allSyncProcessesInfo']['withoutStatusSyncProcessesCount']?></div>
                            <?php $allDevices = $authorityDevicesInfo['allDevices']; ?>
                            <?php if ($allDevices) { ?>
                                <div id="<?=$client->id?>-device-section">
                                    <h5 class="device-section">
                                        <i class="fa fa-phone-square"></i>
                                        Devices (<?=count($allDevices)?>)
                                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                    </h5>
                                    <div class="device-info-list hidden">
                                        <?php foreach ($allDevices as $syncedDevice) { ?>
                                            <div class="device-info">
                                                Model: <?=$syncedDevice->deviceModel?>
                                                <br>
                                                User: <?=$syncedDevice->user->username?>
                                                <br>
                                                Last Synced At: <?=$syncedDevice->lastSyncProcess ? $syncedDevice->lastSyncProcess->created_at : '-'?>
                                                <br>
                                                Status: <?=$syncedDevice->lastSyncProcess ? $syncedDevice->lastSyncProcess->status : '-'?>
                                            </div>
                                        <?php }?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <a href="<?=\yii\helpers\Url::toRoute('/sync/backend-sync')?>" class="small-box-footer">
                <?=Yii::t('twbp', 'More Info');?> <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>


