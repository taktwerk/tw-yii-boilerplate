<?php

namespace taktwerk\yiiboilerplate\modules\sync\widgets;

use taktwerk\yiiboilerplate\modules\sync\enums\SyncProcessStatusEnum;
use taktwerk\yiiboilerplate\modules\customer\models\Client;
use taktwerk\yiiboilerplate\modules\user\models\UserDevice;
use yii\base\Widget;
use \yii\db\Query;
use Yii;
use yii\web\View;

class SyncStatisticInfoWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $this->registerJs();

        $clients = Client::find()
            ->select('client.*')
            ->addSelect(
                [
                    "SUM(IF(status IN ('" . SyncProcessStatusEnum::PROGRESS . "','" . SyncProcessStatusEnum::RESUME . "'), 1, 0)) as inProgressSyncProcessesCount",
                    "SUM(IF(status = '" . SyncProcessStatusEnum::PAUSE ."', 1, 0)) as pausedSyncProcessesCount",
                    "SUM(IF(status = '" . SyncProcessStatusEnum::CANCEL ."', 1, 0)) as canceledSyncProcessesCount",
                    "SUM(IF(status = '" . SyncProcessStatusEnum::SUCCESS ."', 1, 0)) as successSyncProcessesCount",
                    "SUM(IF(status = '" . SyncProcessStatusEnum::FAILED ."', 1, 0)) as failedSyncProcessesCount",
                    "SUM(IF(status IS NULL, 1, 0)) as withoutStatusSyncProcessesCount"
                ]
            )
            ->joinWith('syncProcesses')
            ->groupBy('client.id')
            ->all();

        $authorityDevicesInfo = [];
        if (Yii::$app->getUser()->can('Authority')) {
            $allSyncProcesses = (new Query)
                ->select(
                    [
                        "SUM(IF(status IN ('" . SyncProcessStatusEnum::PROGRESS . "','" . SyncProcessStatusEnum::RESUME . "'), 1, 0)) as inProgressSyncProcessesCount",
                        "SUM(IF(status = '" . SyncProcessStatusEnum::PAUSE ."', 1, 0)) as pausedSyncProcessesCount",
                        "SUM(IF(status = '" . SyncProcessStatusEnum::CANCEL ."', 1, 0)) as canceledSyncProcessesCount",
                        "SUM(IF(status = '" . SyncProcessStatusEnum::SUCCESS ."', 1, 0)) as successSyncProcessesCount",
                        "SUM(IF(status = '" . SyncProcessStatusEnum::FAILED ."', 1, 0)) as failedSyncProcessesCount",
                        "SUM(IF(status IS NULL, 1, 0)) as withoutStatusSyncProcessesCount"
                    ])
                ->from('sync_process')
                ->one();

            $allDevices = UserDevice::find()->joinWith('syncProcesses')
                ->orderBy(['sync_process.updated_at' => SORT_DESC])
                ->all();

            $authorityDevicesInfo = [
                'allSyncProcessesInfo' => $allSyncProcesses,
                'allDevices' => $allDevices,
            ];
        }

        return $this->render(
            'sync-statistic-info',
            ['clients' => $clients, 'authorityDevicesInfo' => $authorityDevicesInfo]
        );
    }

    protected function registerJs()
    {
        $modalJs = <<<JS
$('.client-name').on('click', function () {
        var syncStaticInfoElement = $(this).parent().find('.sync-statistic-info');
        if (syncStaticInfoElement) {
            if (syncStaticInfoElement.hasClass('hidden')) {
                syncStaticInfoElement.removeClass('hidden');
            } else {
                syncStaticInfoElement.addClass('hidden');
            }
        }
    });
$('.device-section').on('click', function () {
        var deviceInfoListElement = $(this).parent().find('.device-info-list');
        if (deviceInfoListElement) {
            if (deviceInfoListElement.hasClass('hidden')) {
                deviceInfoListElement.removeClass('hidden');
            } else {
                deviceInfoListElement.addClass('hidden');
            }
        }
    });
JS;
        $this->view->registerJs($modalJs, View::POS_END);
    }
}
