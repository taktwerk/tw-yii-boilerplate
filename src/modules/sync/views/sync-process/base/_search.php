<?php
//Generation Date: 11-Sep-2020 02:57:24pm
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\sync\models\search\SyncProcess $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="sync-process-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'device_id') ?>

        <?= $form->field($model, 'progress') ?>

        <?= $form->field($model, 'all_items_count') ?>

        <?= $form->field($model, 'synced_items_count') ?>

        <?php // echo $form->field($model, 'status') ?>

        <?php // echo $form->field($model, 'description') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
