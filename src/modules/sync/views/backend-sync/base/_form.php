<?php

use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\feedback\models\Feedback $model
 * @var yii\widgets\ActiveForm $form
 * @var boolean $useModal
 * @var boolean $multiple
 * @var array $pk
 * @var array $show
 * @var string $action
 * @var string $owner
 * @var array $languageCodes
 * @var boolean $relatedForm to know if this view is called from ajax related-form action. Since this is passing from
 * select2 (owner) it is always inherit from main form
 * @var string $relatedType type of related form, like above always passing from main form
 * @var string $owner string that representing id of select2 from which related-form action been called. Since we in
 * each new form appending "_related" for next opened form, it is always unique. In main form it is always ID without
 * anything appended
 */

$owner = $relatedForm ? '_' . $owner . '_related' : '';
$relatedTypeForm = Yii::$app->request->get('relatedType')?:$relatedTypeForm;

if (!isset($multiple)) {
$multiple = false;
}

if (!isset($relatedForm)) {
$relatedForm = false;
}

$tableHint = (!empty(taktwerk\yiiboilerplate\modules\feedback\models\Feedback::tableHint()) ? '<div class="table-hint">' . taktwerk\yiiboilerplate\modules\feedback\models\Feedback::tableHint() . '</div><hr />' : '<br />');

?>
<div class="feedback-form">
        <?php $form = ActiveForm::begin([
        'fieldClass' => '\taktwerk\yiiboilerplate\widget\ActiveField',
        'id' => 'Feedback' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
        'layout' => 'default',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'action' => $useModal ? $action : '',
        'options' => [
        'name' => 'Feedback',
    'enctype' => 'multipart/form-data'
        
        ],
    ]); ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>
        <?php echo $tableHint; ?>

        <?php if ($multiple) : ?>
            <?=Html::hiddenInput('update-multiple', true)?>
            <?php foreach ($pk as $id) :?>
                <?=Html::hiddenInput('pk[]', $id)?>
            <?php endforeach;?>
        <?php endif;?>

        <?php $fieldColumns = [
                
            'attached_file' => 
            $form->field(
                $model,
                'attached_file',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'attached_file') . $owner
                    ]
                ]
            )->widget(
                \taktwerk\yiiboilerplate\widget\FileInput::class,
                [
                    'pluginOptions' => [
                        'showUpload' => false,
                        'showRemove' => false,
                        
                        
                        'initialPreview' => (!empty($model->attached_file) ? [
                            $model->getFileUrl('attached_file')
                        ] : ''),
                        'initialCaption' => $model->attached_file,
                        'initialPreviewAsData' => true,
                        'initialPreviewFileType' => $model->getFileType('attached_file'),
                        'fileActionSettings' => [
                            'indicatorNew' => $model->attached_file === null ? '' : '<a href=" ' . $model->getFileUrl('attached_file') . '" target="_blank"><i class="glyphicon glyphicon-hand-down text-warning"></i></a>',
                            'indicatorNewTitle' => \Yii::t('twbp','Download'),
                        ],
                        'overwriteInitial' => true,
                         'initialPreviewConfig'=> [
                                                     [
                                                         'url' => Url::toRoute(['delete-file','id'=>$model->getPrimaryKey(), 'attribute' => 'attached_file']),
                                                     ],
                                                 ],
                    ],
                     'pluginEvents' => [
                         'fileclear' => 'function() { var prev = $("input[name=\'" + $(this).attr("name") + "\']")[0]; $(prev).val(-1); }',
                        
                        'filepredelete'=>'function(xhr){ 
                               var abort = true; 
                               if (confirm("Are you sure you want to remove this file? This action will delete the file even without pressing the save button")) { 
                                   abort = false; 
                               } 
                               return abort; 
                        }', 
                     ],
                    'options' => [
                        'id' => Html::getInputId($model, 'attached_file') . $owner
                    ]
                ]
            )
            ->hint($model->getAttributeHint('attached_file')),
            'description' => 
            $form->field(
                $model,
                'description',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'description') . $owner
                    ]
                ]
            )
                    ->textarea(
                        [
                            'rows' => 6,
                            'placeholder' => $model->getAttributePlaceholder('description'),
                            'id' => Html::getInputId($model, 'description') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('description')),
            'reference_model' => 
            $form->field(
                $model,
                'reference_model',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'reference_model') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('reference_model'),
                            'id' => Html::getInputId($model, 'reference_model') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('reference_model')),
            'feedback_url' => 
            $form->field(
                $model,
                    'feedback_url',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'feedback_url') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'type' => 'url',
                            'placeholder' => $model->getAttributePlaceholder('feedback_url'),
                            'id' => Html::getInputId($model, 'feedback_url') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('feedback_url')),
            'status' => 
            $form->field(
                $model,
                'status',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'status') . $owner
                    ]
                ]
            )
                    ->widget(
                        Select2::class,
                        [
                            'options' => [
                                'id' => Html::getInputId($model, 'status') . $owner,
                                'placeholder' => !$model->isAttributeRequired('status') ? Yii::t('twbp', 'Select a value...') : null,
                            ],
                            'data' => [
                                'Open' => Yii::t('twbp', 'Open'),
                                'Done' => Yii::t('twbp', 'Done'),
                            ],
                            'hideSearch' => 'true',
                            'pluginOptions' => [
                                'allowClear' => false
                            ],
                        ]
                    )
                    ->hint($model->getAttributeHint('status')),
            'reference_id' => 
            $form->field($model, 'reference_id'),
            'client_id' =>
            $form->field($model, 'client_id'),]; ?>        <div class='clearfix'></div>
<?php $twoColumnsForm = true; ?><?php 
// form fields overwrite
$fieldColumns = Yii::$app->controller->crudColumnsOverwrite($fieldColumns, 'form', $model, $form);

foreach($fieldColumns as $attribute => $fieldColumn) {
    if (!$multiple || ($multiple && isset($show[$attribute]))) {
        if ((isset($show[$attribute]) && $show[$attribute]) || !isset($show[$attribute])) {
            echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
            echo $fieldColumn;
            echo $twoColumnsForm ? '</div>' : '';
        }else{
            echo $fieldColumn;
        }
    }
}
?>
<div class='clearfix'></div>

                <?php $this->endBlock(); ?>
        
        <?= ($relatedType != RelatedForms::TYPE_TAB) ?
            Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                    'label' => Yii::t('twbp', 'Feedback'),
                    'content' => $this->blocks['main'],
                    'active' => true,
                ],
                ]
            ])
            : $this->blocks['main']
        ?>        
        <div class="col-md-12">
        <hr/>
        
        </div>
        
        <div class="clearfix"></div>
        <?php echo $form->errorSummary($model); ?>
        <div class="col-md-12"<?=!$relatedForm ? ' id="main-submit-buttons"' : ''?>>
        <?php if(Yii::$app->controller->crudMainButtons['createsave']): ?>
            <?= Html::submitButton(
            '<span class="glyphicon glyphicon-check"></span> ' .
            ($model->isNewRecord && !$multiple ?
                Yii::t('twbp', 'Create') :
                Yii::t('twbp', 'Save')),
            [
                'id' => 'save-' . $model->formName(),
                'class' => 'btn btn-success',
                'name' => 'submit-default'
            ]
            );
            ?>
        <?php endif; ?>

        <?php if ((!$relatedForm && !$useModal) && !$multiple) { ?>
            <?php if (Yii::$app->getUser()->can(Yii::$app->controller->module->id .
            '_' . Yii::$app->controller->id . '_create') ): ?>
                <?php if(Yii::$app->controller->crudMainButtons['createnew']): ?>
                    <?= Html::submitButton(
                    '<span class="glyphicon glyphicon-check"></span> ' .
                    ($model->isNewRecord && !$multiple ?
                        Yii::t('twbp', 'Create & New') :
                        Yii::t('twbp', 'Save & New')),
                    [
                        'id' => 'save-' . $model->formName(),
                        'class' => 'btn btn-default',
                        'name' => 'submit-new'
                    ]
                    );
                    ?>
                <?php endif; ?>
                <?php if(Yii::$app->controller->crudMainButtons['createclose']): ?>
                    <?= Html::submitButton(
                    '<span class="glyphicon glyphicon-check"></span> ' .
                    ($model->isNewRecord && !$multiple ?
                        Yii::t('twbp', 'Create & Close') :
                        Yii::t('twbp', 'Save & Close')),
                    [
                        'id' => 'save-' . $model->formName(),
                        'class' => 'btn btn-default',
                        'name' => 'submit-close'
                    ]
                    );
                    ?>
                <?php endif; ?>
            <?php endif; ?>
        <?php if (!$model->isNewRecord && Yii::$app->getUser()->can(Yii::$app->controller->module->id .
        '_' . Yii::$app->controller->id . '_delete') && $model->deletable() && Yii::$app->controller->crudMainButtons['delete']) { ?>
        <?= Html::a(
        '<span class="glyphicon glyphicon-trash"></span> ' .
        Yii::t('twbp', 'Delete'),
        ['delete', 'id' => $model->id, 'fromRelation' => $fromRelation],
        [
            'class' => 'btn btn-danger',
            'data-confirm' => '' . Yii::t('twbp', 'Are you sure to delete this item?') . '',
            'data-method' => 'post',
        ]
        );
        ?>
        <?php } ?>
        <?php } elseif ($multiple) { ?>
        <?= Html::a(
        '<span class="glyphicon glyphicon-exit"></span> ' .
        Yii::t('twbp', 'Close'),
        $useModal ? false : [],
        [
            'id' => 'save-' . $model->formName(),
            'class' => 'btn btn-danger' . ($useModal ? ' closeMultiple' : ''),
            'name' => 'close'
        ]
        );
        ?>
        <?php } else { ?>
        <?= Html::a(
        '<span class="glyphicon glyphicon-exit"></span> ' .
        Yii::t('twbp', 'Close'),
        ['#'],
        [
            'class' => 'btn btn-danger',
            'data-dismiss' => 'modal',
            'name' => 'close'
        ]
        );
        ?>
        <?php } ?>
        </div>
        <?php ActiveForm::end(); ?>
        <?= ($relatedForm && $relatedType == \taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms::TYPE_MODAL) ?
        '<div class="clearfix"></div>' :
        ''
        ?>
        <div class="clearfix"></div>
    </div>
</div>
<?php
if ($useModal) {
\taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::register($this);
}
\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::register($this);
?>