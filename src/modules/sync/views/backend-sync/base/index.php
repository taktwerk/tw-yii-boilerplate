<?php

use yii\helpers\Html;
use yii\helpers\Url;
use taktwerk\yiiboilerplate\grid\GridView;
use taktwerk\yiiboilerplate\widget\export\ExportMenu;
use taktwerk\yiiboilerplate\helpers\DropdownItemHelper;
use yii\web\View;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var boolean $useModal
 * @var boolean $importer
 * @var taktwerk\yiiboilerplate\modules\sync\models\search\SyncProcess $searchModel
 */

$this->title = Yii::t('twbp', 'Synchronization processes');
$this->params['breadcrumbs'][] = $this->title;
$basePermission = Yii::$app->controller->module->id . '_' . Yii::$app->controller->id;

$tableHint = (!empty(taktwerk\yiiboilerplate\modules\sync\models\SyncProcess::tableHint()) ?
    '<hr /><div class="table-hint">' . taktwerk\yiiboilerplate\modules\sync\models\SyncProcess::tableHint() . '</div>' :
    ''
);

/* ------- Multiple-Delete Batch Action ------ */
$inlineScript = 'var gridViewKey = "sync_process", useModal = ' . ($useModal ? 'true' : 'false') . ';';
$this->registerJs($inlineScript, View::POS_HEAD, 'my-inline-js');

$gridColumns = [
    [
        'attribute' => 'id',
        'filter' => false,
        'value' => function ($model) {
            return $model->id;
        },
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '<div class="dropdown">
                <button class="btn-act-dot btn btn-default dropdown-toggle" type="button" href="javascript:" data-toggle="dropdown"><i class="glyphicon glyphicon-option-vertical"></i>
                </button>
                <ul class="dropdown-menu cus_drop">' .
                    (Yii::$app->getUser()->can(Yii::$app->getUser()
                    ->can(Yii::$app->controller->module->id . '_' . Yii::$app->controller->id . '_view')) ? '<li>{view}</li>' : '') . ' ' . (Yii::$app->getUser()->can(Yii::$app->controller->module->id . '_' . Yii::$app->controller->id . '_delete') ? '<li>{delete}</li>' : '') . ' ' . (! empty(Yii::$app->controller->customActions) ? '{' . implode('} {', array_keys(Yii::$app->controller->customActions)) . '}' : null) . '</ul>
          </div>
        </div>',
    ],
    [
        'attribute' => 'status',
        'value' => function ($model) {
            return \Yii::t('twbp', $model->status);
        },
        'filter' => [
            'progress' => Yii::t('twbp', 'Progress'),
            'pause' => Yii::t('twbp', 'Pause'),
            'resume' => Yii::t('twbp', 'Resume'),
            'failed' => Yii::t('twbp', 'Failed'),
            'success' => Yii::t('twbp', 'Success'),
            'cancel' => Yii::t('twbp', 'Cancel'),
        ],
        'filterType' => GridView::FILTER_SELECT2,
        'class' => '\kartik\grid\DataColumn',
        'filterWidgetOptions' => [
            'options' => [
                'placeholder' => ''
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ]
        ],
    ],
    [
        'attribute' => 'progress',
        'filter' => false,
        'value' => function ($model) {
            return $model->progress;
        },
    ],
    [
        'attribute' => 'all_items_count',
        'filter' => false,
        'value' => function ($model) {
            return $model->all_items_count;
        },
    ],
    [
        'attribute' => 'synced_items_count',
        'filter' => false,
        'value' => function ($model) {
            return $model->synced_items_count;
        },
    ],
    [
        'attribute' => 'device_id',
        'format' => 'html',
        'value' => function ($model) {
            $deviceInfo = json_decode($model->device->device_information, true);
            if (!$deviceInfo) {
                return '';
            }

            $deviceInfoString = '';

            foreach ($deviceInfo as $key => $value) {
                $deviceInfoString .= '<b>' . ucfirst($key) . '</b> : ' . $value . '<br>';
            }

            return $deviceInfoString;
        },
    ],
    [
        'attribute' => 'user',
        'format' => 'html',
        'value' => function ($model) {
            if (!$model->user) {
                return '';
            }

            $user = $model->user;

            return $user->username;
        },
    ],
    [
        'attribute' => 'description',
        'format' => 'html',
        'value' => function ($model) {
            if($model->description != strip_tags($model->description)){
                return \yii\helpers\StringHelper::truncate($model->description,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->description,500));
            }
        },
    ],
    [
        'attribute' => 'created_at',
        'label' => Yii::t('twbp', 'Was started'),
        'filter' => false,
        'format' => 'html',
        'value' => function ($model) {
            return $model->created_at;
        },
    ],
];

// grid columns overwrite
$gridColumns = Yii::$app->controller->crudColumnsOverwrite($gridColumns, 'index');

// export widget
$exportWidget = false;
if(
    Yii::$app->user->can('app_export') ||
    Yii::$app->user->can('app_export-pdf') ||
    Yii::$app->user->can('app_export-xlsx') ||
    Yii::$app->user->can($basePermission . '_export-xlsx') ||
    Yii::$app->user->can($basePermission . '_export-pdf') ||
    Yii::$app->user->can($basePermission . '_export-csv') ||
    Yii::$app->user->can($basePermission . '_export-xls') ||
    Yii::$app->user->can($basePermission . '_export-html') ||
    Yii::$app->user->can($basePermission . '_export-text')
) {
    // overwrite export columns
    $exportColumns = Yii::$app->controller->exportColumnsOverwrite($gridColumns);

    if (!isset($exportColumns)) {
        $exportColumns = $gridColumns;
        foreach ($exportColumns as $column => $value) {
            // Remove checkbox and action columns from Excel export
            if (isset($value['class']) &&
                (strpos($value['class'], 'CheckboxColumn') !== false || strpos($value['class'], 'ActionColumn') !== false)
            ) {
                unset($exportColumns[$column]);
            }
        }
    }

    $exportWidget = ExportMenu::widget([
        'filename' => Yii::$app->controller->exportFilename,
        'asDropdown' => true,
        'dataProvider' => $dataProvider,
        'showColumnSelector' => false,
        'columns' => $exportColumns,
        'fontAwesome' => true,
        'exportContainer' => [
            'class' => 'btn-group mr-2'
        ],
        'dropdownOptions' => [
            'label' => 'Export',
        ],
        'exportConfig' => [
            ExportMenu::FORMAT_EXCEL_X => (
            Yii::$app->user->can('app_export') ||
            Yii::$app->user->can('app_export-xlsx') ||
            Yii::$app->user->can($basePermission . '_export-xlsx') ? [] : false),
            ExportMenu::FORMAT_PDF => (
            Yii::$app->user->can('app_export') ||
            Yii::$app->user->can('app_export-pdf') ||
            Yii::$app->user->can($basePermission . '_export-pdf') ? [] : false),
            ExportMenu::FORMAT_CSV => (Yii::$app->user->can($basePermission . '_export-csv') ? [] : false),
            ExportMenu::FORMAT_EXCEL => (Yii::$app->user->can($basePermission . '_export-xls') ? [] : false),
            ExportMenu::FORMAT_HTML => (Yii::$app->user->can($basePermission . '_export-html') ? [] : false),
            ExportMenu::FORMAT_TEXT => (Yii::$app->user->can($basePermission . '_export-text') ? [] : false),
        ],
    ]);
}

// action buttons
$gridButtonActions = '';
foreach (Yii::$app->controller->gridButtonActions as $buttonCode => $buttonAction) {
    if ($buttonAction['visible']) {
        $gridButtonActions .= ' ' . Html::a(
            $buttonAction['label'],
            $buttonAction['url'],
            [
                'class' => $buttonAction['class'],
                'data-pjax' => false,
                'data-toggle' => 'modal',
                'data-url' => $buttonAction['url']
            ]
        );
    }
}
?>

<?php $this->beginBlock('info');
\yii\bootstrap\Modal::begin([
    'header' => '<h2>' . Yii::t('twbp', 'Information') . '</h2>',
    'toggleButton' => [
        'tag' => 'btn',
        'label' => '?',
        'class' => 'btn btn-default',
        'title' => Yii::t('twbp', 'Information about possible search operators')
],
]);?><?= $this->render('@taktwerk-boilerplate/views/_info_modal') ?><?php \yii\bootstrap\Modal::end();
$this->endBlock(); ?>
<div class="box box-default">
    <div class="giiant-crud box-body feedback-index">
        <?php
        // echo $this->render('_search', ['model' =>$searchModel]);
        ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => 'select[name="pageSize"]',
                'options' => [
                    'id' => 'feedback-grid'
                ],
                'columns' => $gridColumns,
                'containerOptions' => [
                    'style' => 'overflow: auto'
                ], // only set when $responsive = false
                'headerRowOptions' => [
                    'class' => 'kartik-sheet-style'
                ],
                'filterRowOptions' => [
                    'class' => 'kartik-sheet-style'
                ],
                'toolbar' => [
                    [ 'content' =>
                        $this->blocks['info'].
                        \taktwerk\yiiboilerplate\widget\FilterWidget::widget(['model' => $searchModel, 'pjaxContainer' => 'feedback-pjax-container']) .
                        Html::a(
                            '<i class="glyphicon glyphicon-repeat"></i>',
                            ['index', 'reset' => 1],
                            ['class' => 'btn btn-default', 'title' => Yii::t('twbp', 'Reset Search')]
                        )
                    ],
                    $exportWidget,
                    ($importer && Yii::$app->user->can('import')) ?
                    \taktwerk\yiiboilerplate\modules\import\widget\Importer::widget([
                        'dataProvider' => $dataProvider,
                        'header' => Yii::t('twbp', 'Import'),
                        'pjaxContainerId' => 'feedback-pjax-container',
                    ]) : '',
                    [
                        'content' => \taktwerk\yiiboilerplate\widget\PageSize::widget([
                            'pjaxContainerId' => 'feedback-pjax-container',
                            'model' => $searchModel,
                        ])
                    ],
                ],
                'panel' => [
                    'heading' => "<h3 class=\"panel-title\"><i class=\"glyphicon glyphicon-list\"></i>  " .
                        Yii::t('twbp', 'Synchronization processes') .
                        "</h3>" . \taktwerk\yiiboilerplate\modules\import\widget\ImportResult::widget(),
                    'type' => 'default',
                    'before' => (Yii::$app->getUser()->can(
                        Yii::$app->controller->module->id .
                        '_' .
                        Yii::$app->controller->id .
                        '_create'
                    ) ?
                        Html::a(
                            '<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('twbp', 'New'),
                            [$useModal ? '#modalForm' : 'create'],
                            [
                                'class' => 'btn btn-success create-new',
                                'data-pjax' => $useModal,
                                'data-toggle' => 'modal',
                                'data-url' => Url::toRoute('create')
                            ]
                        ) : '') . $gridButtonActions . ' ' . (Yii::$app->getUser()->can('Authority') ?
                            \yii\bootstrap\ButtonDropdown::widget([
                                'id' => 'giiant-relations',
                                'encodeLabel' => false,
                                'label' => '<span class="glyphicon glyphicon-paperclip"></span> ' .
                                Yii::t('twbp', 'Relations'),
                                'dropdown' => [
                                    'options' => [
                                    ],
                                    'encodeLabels' => false,
                                    'items' => [
                                    ]
                                ],
                                'options' => [
                                    'class' => 'btn-default'
                                ]
                            ])
                            : '') . $tableHint
                    ,
                    'after' => '{pager}',
                    'footer' => false
                ],
                'striped' => true,
                'pjax' => true,
                'pjaxSettings' => [
                    'options' => [
                        'id' => 'feedback-pjax-container',
                    ],
                    'clientOptions' => [
                        'method' => 'POST'
                    ]
                ],
                'hover' => true,
                'pager' => [
                    'class' => yii\widgets\LinkPager::class,
                    'firstPageLabel' => Yii::t('twbp', 'First'),
                    'lastPageLabel' => Yii::t('twbp', 'Last')
                ],
            ])
            ?>
    </div>
</div>
<?php \yii\bootstrap\Modal::begin([
    'size' => \yii\bootstrap\Modal::SIZE_DEFAULT,
    'header' => '<h4>' . Yii::t('twbp', 'Choose fields to edit') . ':</h4>',
    'id' => 'edit-multiple',
    'clientOptions' => [
        'backdrop' => 'static',
    ],
]);
?>

<?php \yii\bootstrap\Modal::end();?>
<?php \yii\bootstrap\Modal::begin([
    'size' => \yii\bootstrap\Modal::SIZE_LARGE,
    'id' => 'modalFormMultiple',
    'clientOptions' => [
        'backdrop' => 'static',
    ],
]);
?>
<?php \yii\bootstrap\Modal::end();
?>
<?php \yii\bootstrap\Modal::begin([
    'size' => \yii\bootstrap\Modal::SIZE_LARGE,
    'id' => 'modalForm',
    'clientOptions' => [
        'backdrop' => 'static',
    ],
]);
?>
<?php \yii\bootstrap\Modal::end();
?>
<?php
if ($useModal) {
    \taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::register($this);
}
\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::register($this);
?>
