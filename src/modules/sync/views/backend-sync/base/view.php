<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\sync\models\SyncProcess $model
 * @var boolean $useModal
 */

$this->title = Yii::t('twbp', 'Synchronization process') . ' #' . $model->id . ', ' . Yii::t('twbp', 'View');
if(!$fromRelation) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('twbp', 'Synchronization processes'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('twbp', 'View');
}
$basePermission = Yii::$app->controller->module->id . '_' . Yii::$app->controller->id;
?>
<div class="box box-default">
    <div class="giiant-crud box-body" id="sync-process-view">

        <!-- flash message -->
        <?php if (Yii::$app->session->getFlash('deleteError') !== null) : ?>
            <span class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= Yii::$app->session->getFlash('deleteError') ?>
            </span>
        <?php endif; ?>
        <?php if (!$useModal) : ?>
            <div class="clearfix crud-navigation">
                <!-- menu buttons -->
                <div class='pull-left'>
                </div>
                <div class="pull-right">
                    <?php if(Yii::$app->controller->crudMainButtons['listall']): ?>
                    <?= Html::a(
                        '<span class="glyphicon glyphicon-list"></span> ' . Yii::t('twbp', 'List {model}',
                            ['model' => Yii::t('twbp', 'Synchronization processes')]                        ),
                        ['index'],
                        ['class' => 'btn btn-default']
                    ) ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>
        <?php $this->beginBlock('taktwerk\yiiboilerplate\modules\sync\models\SyncProcess'); ?>

        <?php $viewColumns = [
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return ucfirst(\Yii::t('twbp', $model->status));
                },
            ],
            [
                'attribute' => 'progress',
                'value' => function ($model) {
                    return $model->progress;
                },
            ],
            [
                'attribute' => 'all_items_count',
                'value' => function ($model) {
                    return $model->all_items_count;
                },
            ],
            [
                'attribute' => 'synced_items_count',
                'value' => function ($model) {
                    return $model->synced_items_count;
                },
            ],
            [
                'attribute' => 'device_id',
                'format' => 'html',
                'value' => function ($model) {
                    $deviceInfo = json_decode($model->device->device_information, true);
                    if (!$deviceInfo) {
                        return '';
                    }

                    $deviceInfoString = '';

                    foreach ($deviceInfo as $key => $value) {
                        $deviceInfoString .= '<b>' . ucfirst($key) . '</b> : ' . $value . '<br>';
                    }

                    return $deviceInfoString;
                },
            ],
            [
                'attribute' => 'user',
                'format' => 'html',
                'value' => function ($model) {
                    if (!$model->user) {
                        return '';
                    }

                    $user = $model->user;

                    return $user->username;
                },
            ],
            [
                'attribute' => 'description',
                'format' => 'html',

                'value' => function ($model) {
                    if($model->description != strip_tags($model->description)){
                        return \yii\helpers\StringHelper::truncate($model->description,500,'...',null,true);
                    }else{
                        return nl2br(\yii\helpers\StringHelper::truncate($model->description,500));
                    }
                },
            ],
        ];

        // view columns overwrite
        $viewColumns = Yii::$app->controller->crudColumnsOverwrite($viewColumns, 'view');

        echo DetailView::widget([
        'model' => $model,
        'attributes' => $viewColumns
        ]); ?>


        <hr/>

        <?= Yii::$app->getUser()->can(
            $basePermission . '_delete'
        ) && $model->deletable() && Yii::$app->controller->crudMainButtons['delete'] ?
            Html::a(
                '<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('twbp', 'Delete'),
                $useModal ? false : ['delete', 'id' => $model->id, 'fromRelation' => $fromRelation],
                [
                    'class' => 'btn btn-danger' . ($useModal ? ' ajaxDelete' : ''),
                    'data-url' => Url::toRoute(['delete', 'id' => $model->id, 'fromRelation' => $fromRelation]),
                    'data-confirm' => $useModal ? false : Yii::t('twbp', 'Are you sure to delete this item?'),
                    'data-method' => $useModal ? false : 'post',
                ]
            )
        :
            ''
        ?>

        <?php $this->endBlock(); ?>



        <?= Tabs::widget(
            [
                'id' => 'relation-tabs',
                'encodeLabels' => false,
                'items' => [
                    [
                        'label' => '<b class=""># ' . $model->id . '</b>',
                        'content' => $this->blocks['taktwerk\yiiboilerplate\modules\sync\models\SyncProcess'],
                        'active' => true,
                    ],
                    [
                        'content' => \taktwerk\yiiboilerplate\widget\HistoryTab::widget(['model' => $model]),
                        'label' => '<small>' .
                            Yii::t('twbp', 'History') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getHistory()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('Administrator'),
                    ],
                ]
            ]
        );
        ?>
        <?= taktwerk\yiiboilerplate\RecordHistory::widget(['model' => $model]) ?>
    </div>
</div>

<?php \taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::register($this);
$js = <<<JS
$(document).ready(function() {
    var hash = document.location.hash;
    
    var prefix = "tab_";
    if (hash) {
        $('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
    } 
    
    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash.replace("#", "#" + prefix);
    });
});
JS;

$this->registerJs($js);
