<?php


namespace taktwerk\yiiboilerplate\modules\sync\enums;


class SyncProcessStatusEnum
{
    const PROGRESS = 'progress';
    const PAUSE = 'pause';
    const RESUME = 'resume';
    const FAILED = 'failed';
    const SUCCESS = 'success';
    const CANCEL = 'cancel';

    public static function toArray()
    {
        return [
            self::PROGRESS,
            self::FAILED,
            self::PAUSE,
            self::RESUME,
            self::SUCCESS,
            self::CANCEL,
        ];
    }

    public static function getNumberByKey($key)
    {
        $keyValue = [
            'progress' => 0,
            'pause' => 1,
            'resume' => 2,
            'failed' => 3,
            'success' => 4,
        ];
        
        return !empty($keyValue[$key]) ? $keyValue[$key] : null;
    }

    public static function getProgressStatusesInArray()
    {
        return [
            self::PROGRESS,
            self::RESUME,
        ];
    }
}
