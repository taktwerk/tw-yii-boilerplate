<?php
namespace taktwerk\yiiboilerplate\modules\sync\commands;

use taktwerk\yiiboilerplate\modules\queue\commands\BackgroundCommandInterface;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use taktwerk\yiiboilerplate\modules\user\models\User;
use Exception;
use taktwerk\yiiboilerplate\modules\sync\models\SyncIndex;
use taktwerk\yiiboilerplate\modules\sync\traits\WebConfigTrait;

class SyncIndexUserJob implements BackgroundCommandInterface
{
    use WebConfigTrait;
    /**
     * Don't notify on success.
     *
     * @var bool
     */
    public $send_mail = false;

    /**
     *
     * @param QueueJob $queue
     * @return mixed|void
     * @throws Exception
     */
    public function run(QueueJob $queue)
    {
        $params = json_decode($queue->parameters, true);
        $id = $params['id'];
        $mClass = $params['model'];
        if($mClass::tableName() == 'sync_index'){
            $queue->status = QueueJob::STATUS_FINISHED;
            return;
        }
        try {
            if($mClass=='' || $id==''){
              $queue->status = QueueJob::STATUS_FAILED;
              $queue->error = 'Model not found';
            }else{
                $this->setWebConfig();
                $user = $mClass::findOne($id);
                if(\Yii::$app->user->login($user)){
                    $this->syncModelsIndex();
                }
                $this->unsetWebConfig();
                $queue->status = QueueJob::STATUS_FINISHED;
            }
            $queue->save();
        } catch (Exception $e) {
            $queue->status = QueueJob::STATUS_FAILED;
            $queue->error = $e->getMessage();
            throw new Exception($queue->error);
        }
    }

    private function syncModelsIndex()
    {
        $models = \Yii::$app->getModule('sync')->models;
        if(!is_array($models) || count($models)<1){
            return;
        }
        $userId = \Yii::$app->user->id;
        $modelUpdated = false;
        foreach($models as $mClass){
            $tbl = $mClass::tableName();
            if($tbl==SyncIndex::tableName()){
                continue;
            }
            $q = $mClass::find()->select($tbl.'.id')->asArray();
            $syncQ = SyncIndex::find()->where(['user_id'=>$userId,'model'=>$tbl]);

            foreach($syncQ->batch() as $syncs){
                foreach($syncs as $sync){
                    $exist = $mClass::find()
                    ->select($tbl.'.id')
                    ->andWhere([$tbl.'.id' => $sync['model_id']])->exists();
                    if($exist==false){
                        $sync->delete();
                        $modelUpdated = true;
                    }
                }
            }
            foreach($q->batch(1000) as $models){
                foreach($models as $model){
                    $exist = SyncIndex::find()
                    ->where([
                        'user_id'=>$userId,
                        'model_id'=>$model['id'],
                        'model'=>$tbl])
                        ->exists();
                        if($exist==false){
                            $sync = new SyncIndex();
                            $sync->model = $tbl;
                            $sync->model_id = $model['id'];
                            $sync->user_id = $userId;
                            $sync->save();
                            $modelUpdated = true;
                        }
                }
            }
       }
       if($modelUpdated){
           try{
               \Yii::$app->user->identity->app_data_version++;
               \Yii::$app->user->identity->save();
           }catch(\Exception $e){
           }catch(\Error $e){}
       }
    }
    

    
}