<?php
namespace taktwerk\yiiboilerplate\modules\sync\commands;

use yii\console\Controller;
use taktwerk\yiiboilerplate\modules\user\models\User;
use taktwerk\yiiboilerplate\modules\sync\models\SyncIndex;
use taktwerk\yiiboilerplate\modules\sync\traits\WebConfigTrait;
class SyncIndexController extends Controller
{
    use WebConfigTrait;
    public function actionModels($id = 'all'){
        $models = \Yii::$app->getModule('sync')->models;
        if(!is_array($models)){
            return;
        }
        $this->setWebConfig();
        if($id == 'all') {
            $userQ = User::find();
        }
        else {
            $userQ = User::find()->andWhere(['id' => $id]);
        }
        foreach($userQ->batch() as $users){
            foreach ($users as $user){
                if(\Yii::$app->user->login($user)){
                    foreach($models as $mClass){
                        if($mClass::tableName()!=SyncIndex::tableName()){
                            $this->addToSyncIndex($mClass);
                        }
                    }
                }
            }
        }
        $this->unsetWebConfig();
    }
    private function addToSyncIndex($modelClass){
        $tbl = $modelClass::tableName();
        $q = $modelClass::find()->select($tbl.'.id')->asArray();
        $userId = \Yii::$app->user->id;
        $syncQ = SyncIndex::find()
        ->where([
            'user_id'=>$userId,'model'=>$tbl
        ]);
        $modelUpdated = false;
        foreach($syncQ->batch() as $syncs){
            foreach($syncs as $sync){
                $exist = $modelClass::find()
                ->select($tbl.'.id')
                ->andWhere([$tbl.'.id'=>$sync['model_id']])->exists();
                if($exist==false){
                    $modelUpdated = true;
                    $sync->delete();
                }
            }
        } 

        foreach($q->batch(1000) as $models){
            foreach($models as $model){
               $exist = SyncIndex::find()
               ->where([
                   'user_id'=>$userId,
                   'model_id'=>$model['id'],
                   'model'=>$tbl])->exists();
               if($exist==false){
                   $sync = new SyncIndex();
                   $sync->model = $tbl;
                   $sync->model_id = $model['id'];
                   $sync->user_id = $userId;
                   $sync->save();
                   $modelUpdated = true;
               }
            }
        }
        if($modelUpdated){
            try{
                \Yii::$app->user->identity->app_data_version++;
                \Yii::$app->user->identity->save();
            }catch(\Exception $e){
            }catch(\Error $e){}
        }
    }
}
