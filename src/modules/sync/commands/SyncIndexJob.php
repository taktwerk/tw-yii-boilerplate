<?php
namespace taktwerk\yiiboilerplate\modules\sync\commands;

use taktwerk\yiiboilerplate\modules\queue\commands\BackgroundCommandInterface;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use taktwerk\yiiboilerplate\modules\user\models\User;
use Exception;
use taktwerk\yiiboilerplate\modules\sync\models\SyncIndex;
use taktwerk\yiiboilerplate\modules\sync\traits\WebConfigTrait;

class SyncIndexJob implements BackgroundCommandInterface
{
    use WebConfigTrait;
    /**
     * Don't notify on success.
     *
     * @var bool
     */
    public $send_mail = false;

    /**
     *
     * @param QueueJob $queue
     * @return mixed|void
     * @throws Exception
     */
    public function run(QueueJob $queue)
    {
        $params = json_decode($queue->parameters, true);
        $id = $params['id'];
        $mClass = $params['model'];
        if($mClass::tableName() == 'sync_index'){
            $queue->status = QueueJob::STATUS_FINISHED;
            return;
        }
        try {
            if($mClass=='' || $id==''){
              $queue->status = QueueJob::STATUS_FAILED;
              $queue->error = 'Model not found';
            }else{
                $this->setWebConfig();
                $userQ = User::find();
                foreach ($userQ->batch() as $users) {
                    foreach ($users as $user) {
                        if(\Yii::$app->user->login($user)){
                            $this->addToSyncIndex($mClass, $id);
                        }
                    }
                }
                $this->unsetWebConfig();
                $queue->status = QueueJob::STATUS_FINISHED;
            }
            $queue->save();
        } catch (Exception $e) {
            $queue->status = QueueJob::STATUS_FAILED;
            $queue->error = $e->getMessage();
            throw new Exception($queue->error);
        }
    }

    private function addToSyncIndex($mClass, $id)
    {
        $model = $mClass::findOne($id);
        $userId = \Yii::$app->user->id;
        if($userId){
            $tbl = $mClass::tableName();
            $modelUpdated = false;
            $syncModel = SyncIndex::find()->where([
                'user_id' => $userId,'model_id' => $id,'model' => $tbl
            ])->one();
            if ($model) {
                if ($syncModel == null) {
                    $sync = \Yii::createObject(SyncIndex::class);
                    $sync->model = $tbl;
                    $sync->model_id = $id;
                    $sync->user_id = $userId;
                    $sync->save();
                    $modelUpdated = true;
                }
            } else {
                if($syncModel){
                    $modelUpdated = true;
                    $syncModel->delete();
                }
            }
            if($modelUpdated){
                try{
                    \Yii::$app->user->identity->app_data_version++;
                    \Yii::$app->user->identity->save();
                }catch(\Exception $e){
                }catch(\Error $e){}
            }
        }
    }
}