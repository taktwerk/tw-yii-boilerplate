<?php
namespace taktwerk\yiiboilerplate\modules\sync\behaviors;

use yii\db\ActiveRecord;
use yii\base\Behavior;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use taktwerk\yiiboilerplate\modules\sync\commands\SyncIndexJob;

class SyncIndexBehavior extends Behavior
{
    // ...

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'insertSyncIndex',
            ActiveRecord::EVENT_AFTER_UPDATE => 'insertSyncIndex',
            ActiveRecord::EVENT_AFTER_DELETE => 'insertSyncIndex',
        ];
    }

    public function insertSyncIndex($event)
    {
        if($this->jobExists()==false){
            $job = new QueueJob();
            $modelTitle =\yii\helpers\StringHelper::basename(get_class($this->owner));
            return $job->queue("Sync Indexing for ".$modelTitle." ".$this->owner->id, 
                ClassDispenser::getMappedClass(SyncIndexJob::class), [
                'model' => get_class($this->owner),
                'id' => $this->owner->getPrimaryKey()
            ], true);
        }
    }
    protected function jobExists(){
        $query = QueueJob::find()->andWhere([
            'command' => ClassDispenser::getMappedClass(SyncIndexJob::class),
            'parameters' => json_encode([
                'model' => get_class($this->owner),
                'id' =>$this->owner->getPrimaryKey(),
            ]),
            'status' => QueueJob::STATUS_QUEUED
        ]);
        return $query->exists();
    }
}