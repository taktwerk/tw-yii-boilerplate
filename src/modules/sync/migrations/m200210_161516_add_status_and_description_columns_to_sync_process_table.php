<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%sync_process}}`.
 */
class m200210_161516_add_status_and_description_columns_to_sync_process_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            '{{%sync_process}}',
            'description',
            $this->text() . ' AFTER synced_items_count'
        );
        $sql = 'ALTER TABLE `sync_process` ADD `status`' .
            'enum(\'progress\',\'pause\',\'resume\',\'failed\',\'success\',\'cancel\') NULL AFTER `synced_items_count`;';
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%sync_process}}', 'status');
        $this->dropColumn('{{%sync_process}}', 'description');
    }
}
