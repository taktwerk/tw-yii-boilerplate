<?php

use taktwerk\yiiboilerplate\models\User;
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200219_171316_add_default_to_column_app_data_version_in_user_table extends TwMigration
{
    public function up()
    {
        $sql = 'ALTER TABLE `user`
                CHANGE `app_data_version` `app_data_version` int(11) NULL DEFAULT \'1\' AFTER `updated_by`';
        $this->db->createCommand($sql)->execute();

        $users = User::find()->where(['is', 'app_data_version', new \yii\db\Expression('null')])
            ->all();
        foreach ($users as $user) {
            $user->app_data_version = 1;
            $user->save();
        }
    }

    public function down()
    {
        echo "m200219_171316_add_default_to_column_app_data_version_in_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
