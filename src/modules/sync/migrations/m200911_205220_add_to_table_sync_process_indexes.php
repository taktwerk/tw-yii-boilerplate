<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200911_205220_add_to_table_sync_process_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_sync_process_deleted_at_device_id', '{{%sync_process}}', ['deleted_at','device_id']);
    }

    public function down()
    {
        echo "m200911_205220_add_to_table_queue_message_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
