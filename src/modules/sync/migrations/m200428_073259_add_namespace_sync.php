<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200428_073259_add_namespace_sync extends TwMigration
{
    public function up()
    {
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\sync"}';
        $this->addCommentOnTable('{{%sync_process}}', $comment);
    }

    public function down()
    {
        echo "m200428_073259_add_namespace_sync cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
