<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%index}}`.
 */
class m210514_100029_create_sync_index_table extends TwMigration
{
    use \taktwerk\yiiboilerplate\modules\guide\traits\MobileMigrationTrait;
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('{{%sync_index}}',[
            'id' => 'int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY',//   $this->primaryKey(),
            'model' => $this->string(),
            'model_id' => $this->integer(),
            'user_id' => $this->integer(),
        ]);

        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\sync"}';
        $this->addCommentOnTable('{{%sync_index}}', $comment);
        $this->addOfflineChangeDateColumns("{{%sync_index}}");
        $this->addForeignKey('fk_sync_index_user_id', '{{%sync_index}}', 'user_id', '{{%user}}', 'id');
        $this->createIndex('idx_sync_index_deleted_at_user_id', '{{%sync_index}}', ['deleted_at','user_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            $this->dropTable('{{%index}}');
    }
}
