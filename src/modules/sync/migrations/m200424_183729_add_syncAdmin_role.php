<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200424_183729_add_syncAdmin_role extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $this->createRole('SyncAdmin','','Sync Module Admin');
        $SyncAdmin = $auth->getRole('SyncAdmin');
        $backend = $auth->getRole('Backend');
        $auth->addChild($SyncAdmin, $backend);
        $this->createCrudControllerPermissions('sync_backend-sync');
        $this->addAdminControllerPermission('sync_backend-sync','SyncAdmin');
    }

    public function down()
    {
        echo "m200424_183729_add_syncAdmin_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
