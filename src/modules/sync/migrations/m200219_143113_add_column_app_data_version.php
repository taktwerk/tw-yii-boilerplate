<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200219_143113_add_column_app_data_version extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'app_data_version', $this->integer(11));
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'app_data_version');
    }
}
