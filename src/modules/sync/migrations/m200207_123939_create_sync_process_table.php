<?php

use taktwerk\yiiboilerplate\TwMigration;
use yii\db\Schema;

/**
 * Handles the creation of table `{{%sync_process}}`.
 */
class m200207_123939_create_sync_process_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%sync_process}}', [
            'id' => $this->primaryKey(),
            'device_id' => Schema::TYPE_INTEGER . "(11)",
            'progress' => Schema::TYPE_INTEGER . "(11)",
            'all_items_count' => Schema::TYPE_INTEGER . "(11)",
            'synced_items_count' => Schema::TYPE_INTEGER . "(11)",
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%sync_process}}');
    }
}
