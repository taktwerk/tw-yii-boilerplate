<?php

namespace taktwerk\yiiboilerplate\modules\sync;

class Module extends \taktwerk\yiiboilerplate\components\TwModule
{

    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\sync\controllers';
    public $models;

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
