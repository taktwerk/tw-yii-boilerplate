<?php

namespace taktwerk\yiiboilerplate\modules\notification\controllers;

use taktwerk\yiiboilerplate\modules\notification\models\Notification;
use taktwerk\yiiboilerplate\modules\notification\widgets\NotificationWidget;
use yii\db\Exception;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use Yii;

/**
 * This is the class for controller "NotificationController".
 */
class NotificationController extends \taktwerk\yiiboilerplate\modules\notification\controllers\base\NotificationController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\notification\models\Notification';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\notification\models\search\Notification';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'refresh',
                            'show',
                            'read',
                            'read-multiple',
                        ],
                        'roles' => ['@']
                    ]
                ]
            ]
        ]);
    }
    /**
     * @throws \Exception
     */
    public function init()
    {
        $this->crudColumnsOverwrite = [
            'crud' => [
                // index
                'after#read_at' => [
                    'attribute' => 'created_at',
                    'class' => '\taktwerk\yiiboilerplate\grid\DateTimeColumn',
                    'content' => function ($model) {
                        return \Yii::$app->formatter->asDatetime($model->created_at);
                    }

                ]
            ],
        ];


        $this->gridDropdownActions = [
            'read-multiple' => (Yii::$app->getUser()->can(
                Yii::$app->controller->module->id . '_' .
                Yii::$app->controller->id . '_read-multiple'
            ) ?
                [
                    'url' => [
                        false
                    ],
                    'options' => [
                        'onclick' => 'readMultiple(this);',
                        'data-url' => Url::to(['/notification/notification/read-multiple'])
                    ],
                    'label' => '<i class="fa fa-eye" aria-hidden="true"></i> ' . Yii::t('twbp', 'Mark as read'),
                ] : '')
        ];

        return parent::init();
    }

    /**
     * @return string
     */
    public function actionRefresh()
    {
        $notification = new NotificationWidget();
        return $notification->renderDropdown();
    }
    /**
     * @param $id
     * @return bool|string
     * @throws \yii\web\HttpException
     */
    public function actionRead($id)
    {
        if (\Yii::$app->request->isAjax) {
            $model = $this->findModel($id);
            if ($model->user_id != \Yii::$app->user->id) {
                $result = [
                    'message' => \Yii::t('twbp', 'No notification found'),
                ];
            } else {
                $result = [
                    'message' => $model->message,
                ];
                if ($model->read_at == null) {
                    $model->read_at = new Expression('NOW()');
                    $model->save(false);
                }
            }
            return json_encode($result);
        }
        return false;
    }
    /**
     * @param $id
     * @return string
     * @throws Exception
     * @throws \yii\web\HttpException
     */
    public function actionShow($id)
    {
        $model = $this->findModel($id);
        if ($model->user_id != \Yii::$app->user->id) {
            throw new Exception(\Yii::t('twbp', 'Not allowed'));
        } else {
            if ($model->read_at == null) {
                $model->read_at = new Expression('NOW()');
                $model->save(false);
            }
            return $this->redirect(Url::to($model->link));
        }
    }


    /**
     * @param int $id
     * @param bool $viewFull
     * @param null $fromRelation
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\console\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id, $viewFull = false, $fromRelation = null)
    {
        $modal = $this->findModel($id);
        if ($modal->read_at == null && $modal->user_id == \Yii::$app->user->id) {
            $modal->updateAttributes(['read_at' => new Expression('NOW()')]);
        }
        return parent::actionView($id, $viewFull, $fromRelation);
    }


    /**
     * @return mixed
     * @throws HttpException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionReadMultiple()
    {
        $pk = Yii::$app->request->post('pk'); // Array or selected records primary keys
        // Preventing extra unnecessary query
        if (!empty($pk)) {
            foreach ($pk as $id) {
                /**@var $model Notification*/
                $modal = $this->findModel($id);
                if ($modal->read_at == null && $modal->user_id == \Yii::$app->user->id) {
                    $modal->updateAttributes(['read_at' => new Expression('NOW()')]);
                }
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->actionIndex();
        }
    }
}
