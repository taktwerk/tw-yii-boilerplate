<?php
//Generation Date: 11-Sep-2020 02:54:35pm
namespace taktwerk\yiiboilerplate\modules\notification\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * NotificationController implements the CRUD actions for Notification model.
 */
class NotificationController extends TwCrudController
{
}
