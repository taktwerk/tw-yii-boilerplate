<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 * @var $notification \taktwerk\yiiboilerplate\modules\notification\models\Notification
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 5/31/2017
 * Time: 11:08 AM
 */
use yii\helpers\Url;
$css = <<<CSS
.navbar-nav > .notifications-menu > .dropdown-menu > li .menu > li > a, .navbar-nav > .messages-menu > .dropdown-menu > li .menu > li > a, .navbar-nav > .tasks-menu > .dropdown-menu > li .menu > li > a {
	white-space: normal;
}
.navbar-nav > .notifications-menu > .dropdown-menu, .navbar-nav > .messages-menu > .dropdown-menu, .navbar-nav > .tasks-menu > .dropdown-menu {
	width: 416px;
}
.button-glow {
    animation: glowing 1300ms infinite;
}
@keyframes glowing {
    0% {
        background-color: #8ee8ffd1;
        box-shadow: 0 0 5px #13cbf9b3;
    }
    50% {
        background-color: #65dbf9;
        box-shadow: 0 0 15px #2ad4ffba;
    }
    100% {
        background-color: #65dbf97a;
        box-shadow: 0 0 5px #2ad4ff87;
    }
}
CSS;

Yii::$app->view->registerCss($css);

?>
<li>
    <a title="<?=Yii::t('twbp',"Click to mark unread")?>" href="<?= $notification->link ?
        Url::toRoute(['/notification/notification/show', 'id' => $notification->id]) :
        '#' ?>"
        <?= !$notification->link ?
            'data-toggle="modal" data-target="#notificationModal" data-id="' . $notification->id . '"' :
            'target="_blank"'
        ?>
    >
        <?= $notification->read_at == null ? '<strong>' : '' ?>
        <?= strlen($notification->message)<100?$notification->message:substr($notification->message, 0, 100)."..."; ?>
        <?= $notification->read_at == null ? '</strong>' : '' ?>
        <br>
        <small class="text-info"><?= Yii::t('twbp', 'Created At').": ".date('d-m-Y',strtotime($notification->created_at));?></small>
    </a>
</li>

