<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 * @var $notifications      \taktwerk\yiiboilerplate\modules\notification\models\Notification[]
 * @var $countNotifications integer
 * @var $countTasks         integer
 * @var $tasks              \taktwerk\yiiboilerplate\modules\queue\models\QueueJob[]
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 5/31/2017
 * Time: 11:08 AM
 */

?>
<li class="dropdown notifications-menu tw-notifications">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-bell-o"></i>
        <span class="label label-warning"><?= $countNotifications ?></span>
    </a>
    <ul class="dropdown-menu">
        <li class="header"><?= Yii::t('twbp', 'You have {count} unread notifications', ['count' => $countNotifications]) ?></li>
        <li>
            <ul class="menu">
                <?php foreach ($notifications as $notification) : ?>
                    <?= $this->render('_notification_item', compact('notification')) ?>
                <?php endforeach; ?>
            </ul>
        </li>
        <?php if(\Yii::$app->hasModule('notification')){ ?>
        <li class="footer"><a href="<?= \yii\helpers\Url::toRoute(['/notification']) ?>">View all</a></li>
        <?php }?>
    </ul>
</li>

<li class="dropdown tasks-menu tw-tasks">
    <a href="#" class="dropdown-toggle <?=Yii::$app->request->cookies->has('task_alert')?"button-glow":""?>" data-toggle="dropdown">
        <i class="fa fa-flag-o"></i>
        <span class="label label-danger"><?= $countTasks ?></span>
    </a>
    <ul class="dropdown-menu">
        <li class="header"><?= Yii::t('twbp', 'There are {count} queue jobs', ['count' => $countTasks]) ?></li>
        <li>
            <ul class="menu">
                <?php foreach ($tasks as $task) : ?>
                    <?= $this->render('_task_item', compact('task')) ?>
                <?php endforeach; ?>
            </ul>
        </li>
        <?php if(Yii::$app->user->can('queue_queue-job')):?>
        <li class="footer">
            <a href="<?= \yii\helpers\Url::toRoute(['/queue/queue-job']) ?>"><?=Yii::t('twbp','View all')?></a>
        </li>
        <?php endif;?>
    </ul>
</li>