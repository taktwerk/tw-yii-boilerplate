<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200331_125100_notification_tbl_namespace extends TwMigration
{
    public function up()
    {
        $tbl = "{{%notification}}";
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\notification"}';
        $this->addCommentOnTable($tbl, $comment);
    }

    public function down()
    {
        echo "m200331_125100_notification_tbl_namespace cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
