<?php
namespace taktwerk\yiiboilerplate\modules\notification\commands;

use taktwerk\yiiboilerplate\components\Helper;
use taktwerk\yiiboilerplate\modules\notification\models\Notification;
use yii\console\Controller;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * Task runner command for development.
 *
 * @author Sharaddha>
 */
class NotificationController extends Controller
{
    
    public function actionFileSystem(){

        $used = 0;
        $total = 0;
        
        foreach (ClassDispenser::getMappedClass(Helper::class)::getDisks() as $key=>$disc){
            if($key>=1){
                $parts = explode("|",$disc);
                $used += $parts[0];
                $total = $total + $parts[0] + $parts[1];
            }
        }

        $used_percentage = round(($used*100)/$total,2);

        if($used_percentage>=80){
            $authority_users = ArrayHelper::getColumn(\Yii::$app->db->createCommand("SELECT user_id FROM auth_assignment where item_name in ('Superadmin', 'Authority')")
                ->queryAll(), 'user_id');

            foreach ($authority_users as $user){
                $message = \Yii::t("twbp","FileSystem usage is reached above 80%, it is $used_percentage%");
                $notification_exists = Notification::find()
                    ->where([
                        'message'=>$message,
                        'user_id'=>$user
                    ])
                    ->andWhere(
                        [
                            '>=',
                            'created_at',
                            new Expression('UNIX_TIMESTAMP(NOW() - INTERVAL 1 DAY)')
                        ])
                    ->exists();
                if(!$notification_exists){
                    Notification::send($user, $message);
                }
            }
        }
    }
}
