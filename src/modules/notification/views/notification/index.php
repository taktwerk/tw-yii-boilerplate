<?php
require(__DIR__ . DIRECTORY_SEPARATOR . 'base' . DIRECTORY_SEPARATOR . 'index.php');

$js = <<<JS
function readMultiple(elm){
    var element = $(elm),
        keys=$("#" + gridViewKey + "-grid").yiiGridView('getSelectedRows'),
        url = element.attr('data-url');
    $.post(url,{pk : keys},
        function (){
            if (useModal != true) {
                $.pjax.reload({container: "#" + gridViewKey + "-pjax-container"});
            }
        }
    );
}
JS;

$this->registerJs($js,\yii\web\View::POS_HEAD);
