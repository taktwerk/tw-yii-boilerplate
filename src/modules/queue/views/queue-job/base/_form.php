<?php
//Generation Date: 11-Sep-2020 02:55:47pm
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\widget\form\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\components\ClassDispenser;
/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\queue\models\QueueJob $model
 * @var taktwerk\yiiboilerplate\widget\form\ActiveForm $form
 * @var boolean $useModal
 * @var boolean $multiple
 * @var array $pk
 * @var array $show
 * @var string $action
 * @var string $owner
 * @var array $languageCodes
 * @var boolean $relatedForm to know if this view is called from ajax related-form action. Since this is passing from
 * select2 (owner) it is always inherit from main form
 * @var string $relatedType type of related form, like above always passing from main form
 * @var string $owner string that representing id of select2 from which related-form action been called. Since we in
 * each new form appending "_related" for next opened form, it is always unique. In main form it is always ID without
 * anything appended
 */

$owner = $relatedForm ? '_' . $owner . '_related' : '';
$relatedTypeForm = Yii::$app->request->get('relatedType')?:$relatedTypeForm;

if (!isset($multiple)) {
$multiple = false;
}

if (!isset($relatedForm)) {
$relatedForm = false;
}

$tableHint = (!empty(taktwerk\yiiboilerplate\modules\queue\models\QueueJob::tableHint()) ? '<div class="table-hint">' . taktwerk\yiiboilerplate\modules\queue\models\QueueJob::tableHint() . '</div><hr />' : '<br />');

?>
<div class="queue-job-form">
        <?php  $formId = 'QueueJob' . ($ajax || $useModal ? '_ajax_' . $owner : ''); ?>    
    <?php $form = ActiveForm::begin([
        'id' => $formId,
        'layout' => 'default',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'action' => $useModal ? $action : '',
        'options' => [
        'name' => 'QueueJob',
            ],
    ]); ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>
        <?php echo $tableHint; ?>

        <?php if ($multiple) : ?>
            <?=Html::hiddenInput('update-multiple', true)?>
            <?php foreach ($pk as $id) :?>
                <?=Html::hiddenInput('pk[]', $id)?>
            <?php endforeach;?>
        <?php endif;?>

        <?php $fieldColumns = [
                
            'title' => 
            $form->field(
                $model,
                'title',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'title') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('title'),
                            'id' => Html::getInputId($model, 'title') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('title')),
            'command' => 
            $form->field(
                $model,
                'command',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'command') . $owner
                    ]
                ]
            )
                    ->textarea(
                        [
                            'rows' => 6,
                            'placeholder' => $model->getAttributePlaceholder('command'),
                            'id' => Html::getInputId($model, 'command') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('command')),
            'status' => 
            $form->field(
                $model,
                'status',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'status') . $owner
                    ]
                ]
            )
                    ->widget(
                        Select2::class,
                        [
                            'options' => [
                                'id' => Html::getInputId($model, 'status') . $owner,
                                'placeholder' => !$model->isAttributeRequired('status') ? Yii::t('twbp', 'Select a value...') : null,
                            ],
                            'data' => [
                                'queued' => Yii::t('twbp', 'Queued'),
                                'running' => Yii::t('twbp', 'Running'),
                                'finished' => Yii::t('twbp', 'Finished'),
                                'failed' => Yii::t('twbp', 'Failed'),
                            ],
                            'hideSearch' => 'true',
                            'pluginOptions' => [
                                'allowClear' => false
                            ],
                        ]
                    )
                    ->hint($model->getAttributeHint('status')),
            'parameters' => 
            $form->field(
                $model,
                'parameters',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'parameters') . $owner
                    ]
                ]
            )
                    ->textarea(
                        [
                            'rows' => 6,
                            'placeholder' => $model->getAttributePlaceholder('parameters'),
                            'id' => Html::getInputId($model, 'parameters') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('parameters')),
            'progress' => 
            $form->field(
                $model,
                'progress',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'progress') . $owner
                    ]
                ]
            )
                     ->checkbox(
                         [
                             'id' => Html::getInputId($model, 'progress') . $owner
                         ]
                     )
                    ->hint($model->getAttributeHint('progress')),
            'error' => 
            $form->field(
                $model,
                'error',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'error') . $owner
                    ]
                ]
            )
                    ->textarea(
                        [
                            'rows' => 6,
                            'placeholder' => $model->getAttributePlaceholder('error'),
                            'id' => Html::getInputId($model, 'error') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('error')),
            'is_reexecuted_on_error' => 
            $form->field($model, 'is_reexecuted_on_error')->checkbox(),
            'fail_minute_delay' => 
            $form->field(
                $model,
                'fail_minute_delay',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'fail_minute_delay') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'type' => 'number',
                            'step' => '1',
                            'placeholder' => $model->getAttributePlaceholder('fail_minute_delay'),
                            
                            'id' => Html::getInputId($model, 'fail_minute_delay') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('fail_minute_delay')),
            'execute_at' => 
            $form->field($model, 'execute_at')->widget(\kartik\datecontrol\DateControl::classname(), [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'ajaxConversion' => true,
                'options' => [
                    'type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_APPEND,
                    'pickerButton' => ['icon' => 'time'],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true,
                        'class' => 'form-control'
                    ]
                ],
            ]),
            'log' => 
            $form->field(
                $model,
                'log',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'log') . $owner
                    ]
                ]
            )
                    ->textarea(
                        [
                            'rows' => 6,
                            'placeholder' => $model->getAttributePlaceholder('log'),
                            'id' => Html::getInputId($model, 'log') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('log')),
            'email_address' => 
            $form->field(
                $model,
                'email_address',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'email_address') . $owner
                    ]
                ]
            )
                    ->textarea(
                        [
                            'rows' => 6,
                            'placeholder' => $model->getAttributePlaceholder('email_address'),
                            'id' => Html::getInputId($model, 'email_address') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('email_address')),
            'max_executions' => 
            $form->field(
                $model,
                'max_executions',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'max_executions') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'type' => 'number',
                            'step' => '1',
                            'placeholder' => $model->getAttributePlaceholder('max_executions'),
                            
                            'id' => Html::getInputId($model, 'max_executions') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('max_executions')),
            'execution_count' => 
            $form->field(
                $model,
                'execution_count',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'execution_count') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'type' => 'number',
                            'step' => '1',
                            'placeholder' => $model->getAttributePlaceholder('execution_count'),
                            
                            'id' => Html::getInputId($model, 'execution_count') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('execution_count')),]; ?>        <div class='clearfix'></div>
<?php $twoColumnsForm = true; ?>
<?php 
// form fields overwrite
$fieldColumns = Yii::$app->controller->crudColumnsOverwrite($fieldColumns, 'form', $model, $form, $owner);

foreach($fieldColumns as $attribute => $fieldColumn) {
            if (count($model->primaryKey())>1 && in_array($attribute, $model->primaryKey()) && $model->checkIfHidden($attribute)) {
            echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
            echo $fieldColumn;
            echo $twoColumnsForm ? '</div>' : '';
        } elseif ($model->checkIfHidden($attribute)) {
            echo $fieldColumn;
        } elseif (!$multiple || ($multiple && isset($show[$attribute]))) {
            if ((isset($show[$attribute]) && $show[$attribute]) || !isset($show[$attribute])) {
                echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
                echo $fieldColumn;
                echo $twoColumnsForm ? '</div>' : '';
            } else {
                echo $fieldColumn;
            }
        }
                
} ?><div class='clearfix'></div>

                <?php $this->endBlock(); ?>
        
        <?= ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_TAB) ?
            Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                    'label' => Yii::t('twbp', 'Queue Job'),
                    'content' => $this->blocks['main'],
                    'active' => true,
                ],
                ]
            ])
            : $this->blocks['main']
        ?>        
        <div class="col-md-12">
        <hr/>
        
        </div>
        
        <div class="clearfix"></div>
        <?php echo $form->errorSummary($model); ?>
        <?php echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::formButtons($model, $useModal, $relatedForm, $multiple, "twbp", ['id' => $model->id]);?>
        <?php ClassDispenser::getMappedClass(ActiveForm::class)::end(); ?>
        <?= ($relatedForm && $relatedType == ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL) ?
        '<div class="clearfix"></div>' :
        ''
        ?>
        <div class="clearfix"></div>
    </div>
</div>

<?php
if ($useModal) {
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::class)::register($this);
}
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
if(!isset($useDependentFieldsJs) || $useDependentFieldsJs==true){
$this->render('@taktwerk-views/_dependent-fields', ['model' => $model,'formId'=>$formId]);
}
?>