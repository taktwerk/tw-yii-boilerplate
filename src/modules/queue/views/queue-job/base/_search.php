<?php
//Generation Date: 11-Sep-2020 02:55:47pm
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\queue\models\search\QueueJob $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="queue-job-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'title') ?>

        <?= $form->field($model, 'command') ?>

        <?= $form->field($model, 'status') ?>

        <?= $form->field($model, 'parameters') ?>

        <?php // echo $form->field($model, 'progress') ?>

        <?php // echo $form->field($model, 'error') ?>

        <?php // echo $form->field($model, 'is_reexecuted_on_error') ?>

        <?php // echo $form->field($model, 'fail_minute_delay') ?>

        <?php // echo $form->field($model, 'execute_at') ?>

        <?php // echo $form->field($model, 'log') ?>

        <?php // echo $form->field($model, 'email_address') ?>

        <?php // echo $form->field($model, 'max_executions') ?>

        <?php // echo $form->field($model, 'execution_count') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
