<?php

namespace taktwerk\yiiboilerplate\modules\queue\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "QueueMessageController".
 */
class QueueMessageController extends \taktwerk\yiiboilerplate\modules\queue\controllers\base\QueueMessageController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\queue\models\QueueMessage';
    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\queue\models\search\QueueMessage';
    
//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
}
