<?php
//Generation Date: 11-Sep-2020 02:55:47pm
namespace taktwerk\yiiboilerplate\modules\queue\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * QueueJobController implements the CRUD actions for QueueJob model.
 */
class QueueJobController extends TwCrudController
{
}
