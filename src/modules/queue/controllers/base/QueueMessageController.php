<?php
//Generation Date: 11-Sep-2020 02:55:48pm
namespace taktwerk\yiiboilerplate\modules\queue\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * QueueMessageController implements the CRUD actions for QueueMessage model.
 */
class QueueMessageController extends TwCrudController
{
}
