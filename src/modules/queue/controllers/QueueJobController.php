<?php

namespace taktwerk\yiiboilerplate\modules\queue\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

/**
 * This is the class for controller "QueueJobController".
 */
class QueueJobController extends \taktwerk\yiiboilerplate\modules\queue\controllers\base\QueueJobController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\queue\models\QueueJob';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\queue\models\search\QueueJob';

    public function init()
    {
        $this->crudRelations = [
            'QueueMessage' => true
        ];

        $this->crudColumnsOverwrite = [
            'index' => [
                // index
                'parameters' => false,
                'progress' => false,
                'error' => false,
                'is_reexecuted_on_error' => false,
                'fail_minute_delay' => false,
                'execute_at' => false,
                'log' => false,
                'email_address' => false,
                'max_executions' => false,
                'execution_count' => false,
            ],
            'crud' => [ // index & view
                'after#status' => [
                    'attribute' => 'created_at',
                    'class' => '\taktwerk\yiiboilerplate\grid\DateTimeColumn',
                    'content' => function ($model) {
                        return \Yii::$app->formatter->asDatetime($model->created_at);
                    }

                ]
            ]
        ];
        
        return parent::init();
    }
    
//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
}