<?php

namespace taktwerk\yiiboilerplate\modules\queue\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\queue\models\base\QueueJob as BaseQueueJob;

/**
 * This is the model class for table "queue_job".
 */
class QueueJob extends BaseQueueJob
{
    protected $defaultOrder = ['created_at' => SORT_DESC];

    /**
     * Queue a job
     * @param string $title
     * @param string $command
     * @param array $parameters
     * @param boolean $reexecute
     * @param integer $delay
     * @return mixed
     */
    public function queue($title, $command, $parameters = array(), $reexecute = false, $delay = 15, $maxExecutions = null)
    {
        $this->title = $title;
        $this->command = $command;
        $this->parameters = json_encode($parameters);
        $this->status = QueueJob::STATUS_QUEUED;
        $this->is_reexecuted_on_error = $reexecute ? 1 : 0;
        $this->fail_minute_delay = $delay;
        $this->max_executions = $maxExecutions;
        $this->execution_count = 0;
        return $this->save();
    }

    /**
     * Duplicate a job that has failed to execute it again after a specified delay
     * @param QueueJob $original
     * @return mixed
     */
    public function duplicate(QueueJob $original)
    {
        // Don't re-execute too many times
        if (!empty($original->max_executions) && $original->execution_count >= $original->max_executions) {
            return false;
        }

        $this->title = $original->title;
        $this->command = $original->command;
        $this->parameters = $original->parameters;
        $this->status = QueueJob::STATUS_QUEUED;
        $this->created_by = $original->created_by;
        $this->fail_minute_delay = $original->fail_minute_delay;
        $this->email_address = $original->email_address;
        $this->max_executions = $original->max_executions;
        $this->execution_count = $original->execution_count + 1;

        $date = new \DateTime();
        $date->modify('+' . (!empty($original->fail_minute_delay) ? $original->fail_minute_delay : 15) . ' minutes');
        $this->execute_at = $date->format('Y-m-d H:i:s');

        return $this->save(false);
    }
}
