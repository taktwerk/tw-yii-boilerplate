<?php

namespace taktwerk\yiiboilerplate\modules\queue\models\search;

use taktwerk\yiiboilerplate\modules\queue\models\search\base\QueueMessage as QueueMessageSearchModel;

/**
* QueueMessage represents the model behind the search form about `taktwerk\yiiboilerplate\modules\queue\models\QueueMessage`.
*/
class QueueMessage extends QueueMessageSearchModel{

}