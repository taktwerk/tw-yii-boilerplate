<?php
//Generation Date: 25-Nov-2020 11:16:09am
namespace taktwerk\yiiboilerplate\modules\queue\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob as QueueJobModel;

/**
 * QueueJob represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\queue\models\QueueJob`.
 */
class QueueJob extends QueueJobModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'title',
                    'command',
                    'status',
                    'parameters',
                    'progress',
                    'error',
                    'is_reexecuted_on_error',
                    'fail_minute_delay',
                    'execute_at',
                    'log',
                    'email_address',
                    'max_executions',
                    'execution_count',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$showDeleted = (property_exists(\Yii::$app->controller, 'model')
            &&
            (is_subclass_of(QueueJobModel::class, \Yii::$app->controller->model) 
            	||
            	is_subclass_of(\Yii::$app->controller->model, QueueJobModel::class))
            &&
              (Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model)
                ||
                Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model)))
            );
        $query = QueueJobModel::find((Yii::$app->get('userUi')->get('show_deleted', QueueJobModel::class) === '1'|| $showDeleted)?false:true);

        $this->parseSearchParams(QueueJobModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(QueueJobModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(QueueJobModel::class),
                'params' => [
                    'page' => $this->parsePageParams(QueueJobModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('progress', $query);
        $this->applyHashOperator('is_reexecuted_on_error', $query);
        $this->applyHashOperator('fail_minute_delay', $query);
        $this->applyHashOperator('max_executions', $query);
        $this->applyHashOperator('execution_count', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('title', $query);
        $this->applyLikeOperator('command', $query);
        $this->applyLikeOperator('status', $query);
        $this->applyLikeOperator('parameters', $query);
        $this->applyLikeOperator('error', $query);
        $this->applyLikeOperator('log', $query);
        $this->applyLikeOperator('email_address', $query);
        $this->applyDateOperator('execute_at', $query, true);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(QueueJobModel::class);
    }
}
