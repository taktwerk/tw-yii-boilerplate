<?php

namespace taktwerk\yiiboilerplate\modules\queue\models\search;

use taktwerk\yiiboilerplate\modules\queue\models\search\base\QueueJob as QueueJobSearchModel;

/**
* QueueJob represents the model behind the search form about `taktwerk\yiiboilerplate\modules\queue\models\QueueJob`.
*/
class QueueJob extends QueueJobSearchModel{

}