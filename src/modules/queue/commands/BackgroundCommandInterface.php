<?php

namespace taktwerk\yiiboilerplate\modules\queue\commands;

use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;

interface BackgroundCommandInterface
{
    /**
     * Run the background command.
     * @param QueueJob $queue the object of the queue to track progress.
     * @return mixed
     */
    public function run(QueueJob $queue);
}
