<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170601_054405_add_queue_job_title extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%queue_job}}', 'title', Schema::TYPE_STRING);
    }

    public function down()
    {
        echo "m170601_054405_add_queue_job_title cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
