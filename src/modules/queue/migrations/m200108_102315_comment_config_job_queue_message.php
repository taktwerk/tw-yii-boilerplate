<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200108_102315_comment_config_job_queue_message extends TwMigration
{
    public function up()
    {
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\queue"}';
        $this->addCommentOnTable('{{%queue_job}}', $comment);
        $this->addCommentOnTable('{{%queue_message}}', $comment);
    }

    public function down()
    {
        echo "m200108_102315_comment_config_job_queue_message cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
