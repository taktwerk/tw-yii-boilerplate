<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200911_205200_add_to_table_queue_message_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_queue_message_deleted_at_job_id', '{{%queue_message}}', ['deleted_at','job_id']);
        $this->createIndex('idx_queue_message_deleted_at_user_id', '{{%queue_message}}', ['deleted_at','user_id']);
    }

    public function down()
    {
        echo "m200911_205200_add_to_table_queue_message_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
