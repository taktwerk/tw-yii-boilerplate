<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m180213_010000_queue_job_email extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%queue_job}}', 'email_address', $this->text()->null());
        $this->addColumn('{{%queue_job}}', 'max_executions', $this->smallInteger()->null());
        $this->addColumn('{{%queue_job}}', 'execution_count', $this->smallInteger()->null());
    }

    public function down()
    {
        $this->removeColumn('{{%queue_job}}', 'email_address');
        $this->removeColumn('{{%queue_job}}', 'max_executions');
        $this->removeColumn('{{%queue_job}}', 'execution_count');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
