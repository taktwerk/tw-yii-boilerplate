<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170531_120243_queue_job_progress extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%queue_job}}', 'progress', 'TINYINT(1)');
        $this->addColumn('{{%queue_job}}', 'error', Schema::TYPE_TEXT);
    }

    public function down()
    {
        echo "m170531_120243_queue_job_progress cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
