<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200321_070936_change_column_order extends TwMigration
{
    public function up()
    {
        $query = <<<EOF
        
            ALTER TABLE `queue_job`
                CHANGE COLUMN `title` `title` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `id`,
                CHANGE COLUMN `command` `command` TEXT(65535) NOT NULL COLLATE 'utf8_unicode_ci' AFTER `title`,
                CHANGE COLUMN `status` `status` ENUM('queued','running','finished','failed') NOT NULL DEFAULT 'queued' COLLATE 'utf8_unicode_ci' AFTER `command`,
                CHANGE COLUMN `execution_count` `execution_count` SMALLINT(6) NULL DEFAULT NULL AFTER `max_executions`,
                CHANGE COLUMN `created_by` `created_by` INT(11) NULL DEFAULT NULL AFTER `execution_count`,
                CHANGE COLUMN `created_at` `created_at` DATETIME NULL DEFAULT NULL AFTER `created_by`,
                CHANGE COLUMN `updated_by` `updated_by` INT(11) NULL DEFAULT NULL AFTER `created_at`,
                CHANGE COLUMN `updated_at` `updated_at` DATETIME NULL DEFAULT NULL AFTER `updated_by`,
                CHANGE COLUMN `deleted_by` `deleted_by` INT(11) NULL DEFAULT NULL AFTER `updated_at`,
                CHANGE COLUMN `deleted_at` `deleted_at` DATETIME NULL DEFAULT NULL AFTER `deleted_by`;
       
EOF;

        $this->execute($query);
    }

    public function down()
    {
        echo "m200321_070936_change_column_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
