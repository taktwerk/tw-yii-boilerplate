<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170524_113406_create_queue_job_table extends TwMigration
{
    public function up()
    {
        $this->createTable(
            '{{%queue_job}}',
            [
                'id' => Schema::TYPE_PK,
                'command' => Schema::TYPE_TEXT . ' NOT NULL',
                'parameters' => Schema::TYPE_TEXT,
                'status' => "enum('queued', 'running', 'finished', 'failed') NOT NULL DEFAULT 'queued'"
            ]
        );

        $this->createTable(
            '{{%queue_message}}',
            [
                'id' => Schema::TYPE_PK,
                'job_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'message' => Schema::TYPE_TEXT,
                'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            ]
        );

        $this->addForeignKey('queue_message_fk_queue_job_id', '{{%queue_message}}', 'job_id', '{{%queue_job}}', 'id');
        $this->addForeignKey('queue_message_fk_queue_user_id', '{{%queue_message}}', 'user_id', '{{%user}}', 'id');
    }

    public function down()
    {
       $this->dropTable('{{%queue_message}}');
       $this->dropTable('{{%queue_job}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
