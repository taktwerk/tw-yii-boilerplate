<?php

namespace taktwerk\yiiboilerplate\modules\translatemanager\models;

use Yii;
use yii\helpers\ArrayHelper;

class Language extends \taktwerk\yiiboilerplate\models\Language
{
    public static function getActiveLanguages()
    {
            return ArrayHelper::getColumn(self::find()
                ->where(['status' => static::STATUS_ACTIVE])
                ->all(),
                'language_id');
    }

    /**
     * @return array|false|string
     */
    public static function getClientLanguages(){
        return explode(",",getenv('APP_CLIENT_LANGUAGES'));
    }

    /**
     * @return array|false|string
     */
    public static function getClientLanguageCategories(){
        return explode(",",getenv('APP_CLIENT_LANGUAGES_CATEGORY'));
    }

}
