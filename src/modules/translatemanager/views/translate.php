<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;

/* @var $this \yii\web\View */
/* @var $language_id string */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel lajax\translatemanager\models\searches\LanguageSourceSearch */
/* @var $searchEmptyCommand string */

$this->title = Yii::t('language', 'Translation into {language_id}', ['language_id' => $language_id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('language', 'Languages'), 'url' => ['list']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss('.pagination {
	 margin: 0 0 5px 0; 
}');
?>
<div class="box box-default">
    <div class="giiant-crud box-body translate-index">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="glyphicon glyphicon-list"></i> <?=$this->title?>
                </h3>
            </div>
            <div class="table-responsive kv-grid-container">
                <?= Html::hiddenInput('language_id', $language_id, ['id' => 'language_id', 'data-url' => Yii::$app->urlManager->createUrl('/translatemanager/translator/save')]); ?>
                <div id="translates" class="<?= $language_id ?>">
                    <?php
                    Pjax::begin([
                        'id' => 'translates',
                    ]);
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'layout' => "<div class='col-md-12'>{items}</div><div class='col-md-9'>{pager}</div><div class='col-md-3 text-right'>{summary}</div>",
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'format' => 'raw',
                                'attribute' => 'message',
                                'filterInputOptions' => ['class' => 'form-control', 'id' => 'message'],
                                'label' => Yii::t('language', 'Source'),
                                'content' => function ($data) {
                                    if(Yii::$app->user->can('TranslatorAdmin')){
                                        return Html::textarea('LanguageSource[' . $data->id . ']', $data->source, ['class' => 'form-control source', 'readonly' => 'readonly']);
                                    }else{
                                        return $data->source;
                                    }
                                }
                            ],
                            [
                                'format' => 'raw',
                                'attribute' => 'translation',
                                'filterInputOptions' => [
                                    'class' => 'form-control',
                                    'id' => 'translation',
                                    'placeholder' => $searchEmptyCommand ? Yii::t('language', 'Enter "{command}" to search for empty translations.', ['command' => $searchEmptyCommand]) : '',
                                ],
                                'label' => Yii::t('language', 'Translation'),

                                'content' => function ($data) {
                                    if (Yii::$app->user->can('TranslatorAdmin')) {
                                        return Html::textarea('LanguageTranslate[' . $data->id . ']', $data->translation, ['class' => 'form-control translation', 'data-id' => $data->id, 'tabindex' => $data->id]);
                                    } else {
                                        return $data->translation;
                                    }
                                }
                            ],
                            [
                                'format' => 'raw',
                                'visible'=>Yii::$app->user->can('TranslatorAdmin'),
                                'label' => Yii::t('language', 'Action'),
                                'content' => function ($data) {
                                    return Html::button(Yii::t('language', 'Save'), ['type' => 'button', 'data-id' => $data->id, 'class' => 'btn btn-primary']);
                                },
                            ],
                        ],
                    ]);
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>