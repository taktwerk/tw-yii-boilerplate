<?php
/**
 * @author Lajos Molnár <lajax.m@gmail.com>
 *
 * @since 1.0
 */
use yii\grid\GridView;
use yii\helpers\Html;
use lajax\translatemanager\models\Language;
use yii\widgets\Pjax;

/* @var $this \yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel lajax\translatemanager\models\searches\LanguageSearch */

$this->title = Yii::t('language', 'List of languages');
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss('.pagination {
	padding-left: 15px;
	 margin: 0 0 10px 0; 
}');
?>
<div class="box box-default">
    <div class="giiant-crud box-body translate-index">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="glyphicon glyphicon-list"></i> <?=$this->title?>
                </h3>
            </div>
            <div class="table-responsive kv-grid-container">
                <div id="languages">

                    <?php
                    Pjax::begin([
                        'id' => 'languages',
                    ]);
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'layout' => "<div class='col-md-12'>{items}</div><div class='col-md-9'>{pager}</div><div class='col-md-3 text-right'>{summary}</div>",
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'language_id',
                            'name_ascii',
                            [
                                'format' => 'raw',
                                'attribute' => Yii::t('language', 'Statistic'),
                                'content' => function ($language) {
                                    return '<span class="statistic"><span style="width:' . $language->gridStatistic . '%"></span><i>' . $language->gridStatistic . '%</i></span>';
                                },
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'contentOptions' => ['style'=>'width:70px','class'=>'text-center'],
                                'template' => '{translate}',
                                'buttons' => [
                                    'translate' => function ($url, $model, $key) {
                                        return Html::a('<span class="glyphicon glyphicon-list-alt"></span>', ['translator/translate', 'language_id' => $model->language_id], [
                                            'title' => Yii::t('language', 'Translate'),
                                            'data-pjax' => '0',
                                        ]);
                                    },
                                ],
                            ],
                        ],
                    ]);
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
