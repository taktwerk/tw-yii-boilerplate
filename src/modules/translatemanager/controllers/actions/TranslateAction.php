<?php

namespace taktwerk\yiiboilerplate\modules\translatemanager\controllers\actions;

use taktwerk\yiiboilerplate\modules\translatemanager\models\Language;
use Yii;
use lajax\translatemanager\models\searches\LanguageSourceSearch;

class TranslateAction extends \lajax\translatemanager\controllers\actions\TranslateAction
{

    /**
     * List of language elements.
     *
     * @return string
     */
    public function run()
    {
        $searchModel = new LanguageSourceSearch([
            'searchEmptyCommand' => $this->controller->module->searchEmptyCommand,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $query = $dataProvider->query;
        $query->andWhere(['in','category',Language::getClientLanguageCategories()]);

        return $this->controller->render('@taktwerk-boilerplate/modules/translatemanager/views/translate.php', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'searchEmptyCommand' => $this->controller->module->searchEmptyCommand,
            'language_id' => Yii::$app->request->get('language_id', ''),
        ]);
    }
}
