<?php

namespace taktwerk\yiiboilerplate\modules\translatemanager\controllers\actions;

use taktwerk\yiiboilerplate\modules\translatemanager\models\Language;
use Yii;
use lajax\translatemanager\models\searches\LanguageSearch;
use lajax\translatemanager\bundles\LanguageAsset;
use lajax\translatemanager\bundles\LanguagePluginAsset;

class ListAction extends \lajax\translatemanager\controllers\actions\ListAction
{

    /**
     * List of languages
     *
     * @return string
     */
    public function run()
    {
        $searchModel = new LanguageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $dataProvider->query = $dataProvider->query->andWhere(['in','language_id' ,Language::getClientLanguages()]);
        $dataProvider->sort = ['defaultOrder' => ['status' => SORT_DESC]];

        return $this->controller->render('@taktwerk-boilerplate/modules/translatemanager/views/list.php', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
}
