<?php

namespace taktwerk\yiiboilerplate\modules\translatemanager\controllers;

use taktwerk\yiiboilerplate\modules\translatemanager\models\Language;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;

class TranslatorController extends \lajax\translatemanager\controllers\LanguageController
{
    public $layout = "@taktwerk-backend-views/layouts/main";

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'translate',
                            'list',
                            'save',
                            'system'
                        ],
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }
    public function beforeAction($action){
        $moduleActive = isset(\Yii::$app->params['modules_activation']['translatemanager'])
        && \Yii::$app->params['modules_activation']['translatemanager'];
        if($moduleActive==false){
            throw new \yii\web\NotFoundHttpException('Page not found');
        }
        return parent::beforeAction($action);
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions['scan'] = 'taktwerk\yiiboilerplate\actions\ScanAction';
        $actions['save'] =  'lajax\translatemanager\controllers\actions\SaveAction';
        $actions['translate'] = 'taktwerk\yiiboilerplate\modules\translatemanager\controllers\actions\TranslateAction';
        $actions['list'] = 'taktwerk\yiiboilerplate\modules\translatemanager\controllers\actions\ListAction';
        $actions['system'] = 'taktwerk\yiiboilerplate\modules\translatemanager\controllers\actions\SystemAction';
        return $actions;
    }

    /**
     * Returns an ArrayDataProvider consisting of language elements.
     * @param array $languageSources
     * @return ArrayDataProvider
     */
    public function createLanguageSourceDataProvider($languageSources)
    {
        $data = [];
        foreach ($languageSources as $category => $messages) {
            foreach ($messages as $message => $boolean) {
                if(in_array($category,[Language::getClientLanguageCategories()])){
                    $data[] = [
                        'category' => $category,
                        'message' => $message,
                    ];
                }
            }
        }

        return new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);
    }
}
