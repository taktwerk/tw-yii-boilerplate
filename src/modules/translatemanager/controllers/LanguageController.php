<?php

namespace taktwerk\yiiboilerplate\modules\translatemanager\controllers;

use taktwerk\yiiboilerplate\modules\customer\models\Client;
use taktwerk\yiiboilerplate\models\Language;

class LanguageController extends \lajax\translatemanager\controllers\LanguageController
{
    public function actions()
    {
        $actions = parent::actions();
        $actions['scan'] = 'taktwerk\yiiboilerplate\actions\ScanAction';
        $actions['index'] = [
            'class' => 'lajax\translatemanager\controllers\actions\ListAction',
        ];
        return $actions;
    }
    
    /**
     * Get details of one system entry
     * @param $id
     * @return false|string
     */
    public function actionSystemEntry($id)
    {
        $data['success'] = false;
        if ($id) {
            $model =  \Yii::createObject(Language::class);
            if (method_exists($model, 'findWithSystem')) {
                $pk = $model::primaryKey()[0];
                $model = $model::findWithSystem()->where([
                    $pk => $id
                ])->one();
                $field = strpos(get_class($model), "Client") === false ? "client_id" : "id";
                $client = Client::findWithSystem()->where([
                    'id' => $model->$field
                ])->one();
                if (!empty($client) && $client->is_system_entry) {
                    $data['success'] = true;
                }
            }
        }
        return json_encode($data);
    }
    /**
     * Get details of one entry
     * @param $id
     * @return false|string
     */
    public function actionEntryDetails($id)
    {
        $model = Language::findOne($id);
        if ($model) {
            $output = [
                'success' => true,
                'data' => $model->entryDetails
            ];
        } else {
            $output = [
                'success' => false,
                'message' => 'Model does not exist'
            ];
        }
        return json_encode($output);
    }
}
