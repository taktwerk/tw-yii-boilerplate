<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;
use taktwerk\yiiboilerplate\modules\translatemanager\models\Language;

class m200413_062406_add_active_languages extends TwMigration
{
    /**
     * @return bool|void
     */
    public function up()
    {

        if(!Language::find()->where(['language_id' => 'en'])->exists()){
            $query = <<<EOF
INSERT INTO `language` (`language_id`, `language`, `country`, `name`, `name_ascii`, `status`) VALUES ('en', 'en', 'en', 'English', 'English (en)', '1')
EOF;
            $this->execute($query);
        }
        if(!Language::find()->where(['language_id' => 'de'])->exists()){
            $query = <<<EOF
INSERT INTO `language` (`language_id`, `language`, `country`, `name`, `name_ascii`, `status`) VALUES ('de', 'de', 'de', 'Deutsch', 'German (de)', '1')
EOF;
            $this->execute($query);
        }
    }

    public function down()
    {
        echo "m200413_062406_add_active_languages cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
