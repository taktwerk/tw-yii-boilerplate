<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200419_190405_add_transaltemanager_roles extends TwMigration
{
    public function up()
    {
        // Create roles
        $this->createRole('TranslatorViewer');
        $this->createRole('TranslatorAdmin', 'TranslatorViewer');

        // Init the permission cascade for see
        $this->addSeeControllerPermission('translatemanager_translator', 'TranslatorViewer');
        // Administration permission assignment
        $this->addAdminControllerPermission('translatemanager_translator','TranslatorAdmin');
        
    }

    public function down()
    {
        echo "m200419_190405_add_transaltemanager_roles cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
