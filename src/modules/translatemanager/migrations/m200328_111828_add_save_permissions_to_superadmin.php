<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200328_111828_add_save_permissions_to_superadmin extends TwMigration
{
    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->createPermission('translatemanager_translator_save', 'Save translation', ['Superadmin']);
    }

    public function down()
    {
        echo "m200328_111828_add_save_permissions_to_superadmin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
