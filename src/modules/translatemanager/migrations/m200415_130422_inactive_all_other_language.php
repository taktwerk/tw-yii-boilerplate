<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;
use taktwerk\yiiboilerplate\modules\translatemanager\models\Language;

class m200415_130422_inactive_all_other_language extends TwMigration
{
    public function up()
    {
        Language::updateAll(['status'=>Language::STATUS_INACTIVE],['not in','language_id', ['de','en']]);
    }

    public function down()
    {
        echo "m200415_130422_inactive_all_other_language cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
