<?php

namespace taktwerk\yiiboilerplate\modules\setting;


use taktwerk\yiiboilerplate\components\MenuItemInterface;

/*
 * @link http://www.diemeisterei.de/
 *
 * @copyright Copyright (c) 2014 diemeisterei GmbH, Stuttgart
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Class Module.
 *
 * @author Tobias Munk <tobias@diemeisterei.de>
 */
class Module extends \taktwerk\yiiboilerplate\components\TwModule implements MenuItemInterface
{
    /**
     * @var integer the permission to be set for newly generated code files.
     * This value will be used by PHP chmod function.
     * Defaults to 0666, meaning the file is read-writable by all users.
     */
    public $newFileMode = 0666;
    /**
     * @var integer the permission to be set for newly generated directories.
     * This value will be used by PHP chmod function.
     * Defaults to 0777, meaning the directory can be read, written and executed by all users.
     */
    public $newDirMode = 0777;

    /**
     * Define the module's controllers for when the app extends it
     */
    public $controllerMap = [
        'general-setting' => 'taktwerk\yiiboilerplate\modules\setting\controllers\GeneralSettingController'
    ];
    public function getMenuItems()
    {
        // example for settings entry
        /* $items = [
            [
                'label' => \Yii::t('twbp', 'Backend'),
                'url' => 'javascript:',
                'icon' => 'fa fa-cog',
                'visible' => \Yii::$app->user->can('Authority')
            ],
        ];
        return $items; */
    }
}
