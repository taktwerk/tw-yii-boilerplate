<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200918_143712_add_column_attached_file_to_general_setting_table extends TwMigration
{
    public function up()
    {
        $comment = '{"allowedExtensions":["zip"]}';
        $this->addColumn('{{%general_setting}}','attached_file', $this->string(255)->null()->comment($comment));
    }

    public function down()
    {
        echo "m200918_143712_add_column_attached_file_to_general_setting_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
