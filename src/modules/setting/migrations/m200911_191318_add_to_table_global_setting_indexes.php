<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200911_191318_add_to_table_global_setting_indexes extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%global_setting}}', 'created_at', $this->dateTime()->defaultValue(null));
        $this->addColumn("{{%global_setting}}", "created_by", $this->integer()->defaultValue(null));
        $this->addColumn('{{%global_setting}}', 'updated_at', $this->dateTime()->defaultValue(null));
        $this->addColumn("{{%global_setting}}", "updated_by", $this->integer()->defaultValue(null));
        $this->addColumn('{{%global_setting}}', 'deleted_at', $this->dateTime()->defaultValue(null));
        $this->createIndex('idx_global_setting_deleted_at', '{{%global_setting}}', 'deleted_at');
        $this->addColumn("{{%global_setting}}", "deleted_by", $this->integer()->defaultValue(null));
    }

    public function down()
    {
        echo "m200911_191318_add_to_table_global_setting_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
