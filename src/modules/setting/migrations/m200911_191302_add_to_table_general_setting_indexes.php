<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200911_191302_add_to_table_general_setting_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_general_setting_deleted_at_client_id', '{{%general_setting}}', ['deleted_at','client_id']);
    }

    public function down()
    {
        echo "m200911_191302_add_to_table_general_setting_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
