<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200415_131031_add_namespace_setting extends TwMigration
{
    public function up()
    {
        $comment= '{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\setting"}';
        $this->addCommentOnTable('general_setting', $comment);
        $this->addCommentOnTable('global_setting', $comment);
    }

    public function down()
    {
        echo "m200415_131031_add_namespace_setting cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
