<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\widget\form\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;

use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\modules\setting\models\GeneralSetting;

/**
 * @var yii\web\View $this
 * @var app\models\GeneralSetting $model
 * @var string $relatedTypeForm
 */

$this->title = Yii::t('twbp', 'Global Setting');
    $this->params['breadcrumbs'][] =  Yii::t('twbp', 'Global Settings');
    //$this->params['breadcrumbs'][] = Yii::t('twbp', 'Create');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body global-setting-create">

        <div class="clearfix crud-navigation">
            <div class="pull-left">
                <?= Html::a(
                    Yii::t('twbp', 'Cancel'),
                    \yii\helpers\Url::previous(),
                    ['class' => 'btn btn-default']
                ) ?>
            </div>
            <div class="pull-right">
                <?php                 \yii\bootstrap\Modal::begin([
                    'header' => '<h2>' . Yii::t('twbp', 'Information') . '</h2>',
                    'toggleButton' => [
                        'tag' => 'btn',
                        'label' => '?',
                        'class' => 'btn btn-default',
                        'title' => Yii::t('twbp', 'Information about possible search operators')
                    ],
                ]);?>

                <?= $this->render('@taktwerk-boilerplate/views/_shortcut_info_modal') ?>
                <?php  \yii\bootstrap\Modal::end();
                ?>
            </div>
        </div>

        <div class="global-setting-form">
        <?php $form = ActiveForm::begin([
        'id' => 'GlobalSetting' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
        'layout' => 'default',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'action' => $useModal ? $action : '',
        'options' => [
        'name' => 'GeneralSetting'
            ]
        ]);
        ?>

			<div class="">
        <?php $this->beginBlock('main'); ?>
        <?php echo $tableHint; ?>

        <?php if ($multiple) : ?>
            <?=Html::hiddenInput('update-multiple', true)?>
            <?php foreach ($pk as $id) :?>
                <?=Html::hiddenInput('pk[]', $id)?>
            <?php endforeach;?>
        <?php endif;?>

<?php
$fieldColumns = [];
foreach($model->attributes as $k=>$a){
    
    $fieldColumns[$k] =  $form->field($model, $k)->hint($model->getAttributeHint($k));
}

?>
        <?php /* $fieldColumns = [
                
            'is_debug' => 
            $form->field($model, 'is_debug')->dropDownList([0=>\Yii::t('twbp', 'No'),1=>\Yii::t('twbp', 'Yes')]),
            'environment' => 
            $form->field(
                $model,
                'environment',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'environment') . $owner
                    ]
                ]
            )
            ->dropDownList(['prod'=>\Yii::t('twbp', 'Production'),'dev'=>\Yii::t('twbp', 'Development'),'test'=>\Yii::t('twbp', 'Testing')])
                    ->hint($model->getAttributeHint('environment')),];  */?>   
                         <div
					class='clearfix'></div>
				<br>
<?php $twoColumnsForm = true; ?><?php 
// form fields overwrite
//$fieldColumns = Yii::$app->controller->crudColumnsOverwrite($fieldColumns, 'form', $model, $form);

foreach($fieldColumns as $attribute => $fieldColumn) {
    if (!$multiple || ($multiple && isset($show[$attribute]))) {
        if ((isset($show[$attribute]) && $show[$attribute]) || !isset($show[$attribute])) {
            echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
            echo $fieldColumn;
            echo $twoColumnsForm ? '</div>' : '';
        }else{
            echo $fieldColumn;
        }
    }
}
?>
<div class='clearfix'></div>

                <?php $this->endBlock(); ?>
        
        <?= ($relatedType != RelatedForms::TYPE_TAB) ?
            Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                    'label' => Yii::t('twbp', 'Global Setting'),
                    'content' => $this->blocks['main'],
                    'active' => true,
                ],
                ]
            ])
            : $this->blocks['main']
        ?>        
        <div class="col-md-12">
					<hr />

				</div>

				<div class="clearfix"></div>
        <?php echo $form->errorSummary($model); ?>
        <div class="col-md-12"
					<?=!$relatedForm ? ' id="main-submit-buttons"' : ''?>>
        <?php if(Yii::$app->controller->crudMainButtons['createsave']): ?>
            <?= Html::submitButton(
            '<span class="glyphicon glyphicon-check"></span> ' .
                 Yii::t('twbp', 'Save'),
            [
                'id' => 'save-' . $model->formName(),
                'class' => 'btn btn-success',
                'name' => 'submit-default'
            ]
            );
            ?>
        <?php endif; ?>

        <?php if ((!$relatedForm && !$useModal) && !$multiple) { ?>
            <?php if (Yii::$app->getUser()->can(Yii::$app->controller->module->id .
            '_' . Yii::$app->controller->id . '_create') ): ?>
                
            <?php endif; ?>
        
        <?php } elseif ($multiple) { ?>
        <?= Html::a(
        '<span class="glyphicon glyphicon-exit"></span> ' .
        Yii::t('twbp', 'Close'),
        $useModal ? false : [],
        [
            'id' => 'save-' . $model->formName(),
            'class' => 'btn btn-danger' . ($useModal ? ' closeMultiple' : ''),
            'name' => 'close'
        ]
        );
        ?>
        <?php } else { ?>
        <?= Html::a(
        '<span class="glyphicon glyphicon-exit"></span> ' .
        Yii::t('twbp', 'Close'),
        ['#'],
        [
            'class' => 'btn btn-danger',
            'data-dismiss' => 'modal',
            'name' => 'close'
        ]
        );
        ?>
        <?php } ?>
        </div>
        <?php ActiveForm::end(); ?>
        <?= ($relatedForm && $relatedType == RelatedForms::TYPE_MODAL) ?
        '<div class="clearfix"></div>' :
        ''
        ?>
        <div class="clearfix"></div>
			</div>
		</div>
    </div>
</div>
<?php
if ($useModal) {
\taktwerk\yiiboilerplate\modules\setting\assets\ModalFormAsset::register($this);
}
\taktwerk\yiiboilerplate\modules\setting\assets\ShortcutsAsset::register($this);
?>