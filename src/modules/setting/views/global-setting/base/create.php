<?php
//Generation Date: 11-Sep-2020 02:43:12pm
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\setting\models\GlobalSetting $model
 * @var string $relatedTypeForm
 */

$this->title = Yii::t('twbp', 'Global Setting') . ', ' . Yii::t('twbp', 'Create');
if(!$fromRelation) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('twbp', 'Global Settings'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = Yii::t('twbp', 'Create');
}
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body global-setting-create">

        <div class="clearfix crud-navigation">
            <div class="pull-left">
                <?= Html::a(
                    Yii::t('twbp', 'Cancel'),
                    $fromRelation ? urldecode($fromRelation) : \yii\helpers\Url::previous(),
                    ['class' => 'btn btn-default cancel-form-btn']
                ) ?>
            </div>
            <div class="pull-right">
                <?php                 \yii\bootstrap\Modal::begin([
                    'header' => '<h2>' . Yii::t('twbp', 'Information') . '</h2>',
                    'toggleButton' => [
                        'tag' => 'btn',
                        'label' => '?',
                        'class' => 'btn btn-default',
                        'title' => Yii::t('twbp', 'Information about possible search operators')
                    ],
                ]);?>

                <?= $this->render('@taktwerk-boilerplate/views/_shortcut_info_modal') ?>
                <?php  \yii\bootstrap\Modal::end();
                ?>
            </div>
        </div>

        <?= $this->render('_form', [
            'model' => $model,
            'inlineForm' => $inlineForm,
            'action' => $action,
            'relatedTypeForm' => $relatedTypeForm,
            'fromRelation' => $fromRelation,
                ]); ?>

    </div>
</div>
