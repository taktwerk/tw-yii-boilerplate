<?php
namespace taktwerk\yiiboilerplate\modules\setting\controllers;

use dmstr\helpers\Metadata;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\base\DynamicModel;
use yii\helpers\FileHelper;
use taktwerk\yiiboilerplate\models\GlobalSetting;
use taktwerk\yiiboilerplate\modules\setting\models\GeneralSetting;
use taktwerk\yiiboilerplate\controllers\TwCrudController;

/**
 * Default setting controller.
 *
 * Usually renders a customized dashboard for logged in users
 */
class DefaultController extends TwCrudController
{

    /**
     *
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'error'
                        ]
                    ],
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return \Yii::$app->user->can($this->module->id . '_' . $this->id . '_' . $action->id, [
                                'route' => true
                            ]);
                        }
                    ]
                ]
            ]
        ];
    }

    /**
     * Actions defined in classes, eg.
     * error page.
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ]
        ];
    }

    /**
     * Application dashboard.
     *
     * @return string
     */
    public function actionIndex()
    {
        $envList = $_ENV;
        ksort($envList);
        /*   if (! $this->layout)
         $this->layout = '@taktwerk-setting-views/layouts/box'; */
        if(array_key_exists('ORIGINAL_SERVER_KEYS', $_SERVER)){
            $serverKeys = explode(',',$_SERVER['ORIGINAL_SERVER_KEYS']);
            foreach($serverKeys as $sKey){
                if(array_key_exists($sKey,$envList)){
                    unset($envList[$sKey]);
                }
            }
            if(array_key_exists('ORIGINAL_SERVER_KEYS',$envList)){
                unset($envList['ORIGINAL_SERVER_KEYS']);
            }
        }

        $attributes = array_keys($envList);
        
        $model = new DynamicModel($attributes);
        
        $model->addRule($attributes, 'string')
        ->validate();
        $model->load($envList,'');
        
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $envFile = \Yii::getAlias('@root/.env-admin');
            $handle = fopen($envFile, "w") or die("Unable to open file!");
            $content = [];
            foreach ($envList as $k => $m) {
                if (! $model->hasAttribute($k)) {
                    continue;
                }
                if($model->$k==$envList[$k]){
                    continue;
                }
                $gSet = new GlobalSetting();
                $check = $gSet->find()
                ->where([
                    'key' => $k
                ])
                ->one();
                $gSet->key = $k;
                if ($check) {
                    $gSet = $check;
                }
                $gSet->value = $model->$k;
                $gSet->save();
                $content[] = "{$k} = \"" . addslashes($model->$k)."\"";
            }
            fwrite($handle, implode(PHP_EOL, $content));
            fclose($handle);
        }
        return $this->render('index', [
            'model' => $model
        ]);
    }

    /**
     * Application configuration.
     *
     * @return string
     */
    public function actionViewConfig()
    {
        $loadedModules = Metadata::getModules();
        $loadedModulesDataProvider = new ArrayDataProvider([
            'allModels' => $loadedModules
        ]);
        
        return $this->render('view-config', [
            'params' => Yii::$app->params,
            'components' => Yii::$app->getComponents(),
            'modules' => Yii::$app->getModules(),
            'loadedModulesDataProvider' => $loadedModulesDataProvider
        ]);
    }

}
