<?php
//Generation Date: 11-Sep-2020 02:43:12pm
namespace taktwerk\yiiboilerplate\modules\setting\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * GlobalSettingController implements the CRUD actions for GlobalSetting model.
 */
class GlobalSettingController extends TwCrudController
{
}
