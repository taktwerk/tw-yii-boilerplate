<?php

namespace taktwerk\yiiboilerplate\modules\setting\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\setting\models\base\GeneralSetting as BaseGeneralSetting;

/**
 * This is the model class for table "general_setting".
 */
class GeneralSetting extends BaseGeneralSetting
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

}
