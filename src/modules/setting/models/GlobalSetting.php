<?php

namespace taktwerk\yiiboilerplate\modules\setting\models;

use \taktwerk\yiiboilerplate\modules\setting\models\base\GlobalSetting as BaseGlobalSetting; 

/**
 * This is the model class for table "country".
 */
class GlobalSetting extends BaseGlobalSetting
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

}
