<?php
//Generation Date: 11-Sep-2020 02:43:12pm
namespace taktwerk\yiiboilerplate\modules\setting\models\search;

use taktwerk\yiiboilerplate\modules\setting\models\search\base\GlobalSetting as GlobalSettingSearchModel;

/**
* GlobalSetting represents the model behind the search form about `taktwerk\yiiboilerplate\modules\setting\models\GlobalSetting`.
*/
class GlobalSetting extends GlobalSettingSearchModel{

}