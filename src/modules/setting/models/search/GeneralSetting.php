<?php

namespace taktwerk\yiiboilerplate\modules\setting\models\search;

use taktwerk\yiiboilerplate\modules\setting\models\search\base\GeneralSetting as GeneralSettingSearchModel;

/**
* GeneralSetting represents the model behind the search form about `taktwerk\yiiboilerplate\modules\setting\models\GeneralSetting`.
*/
class GeneralSetting extends GeneralSettingSearchModel{

}