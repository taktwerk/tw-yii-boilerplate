<?php
//Generation Date: 18-Sep-2020 02:49:51pm
namespace taktwerk\yiiboilerplate\modules\setting\models\base;

use Yii;
use \taktwerk\yiiboilerplate\models\TwActiveRecord;
use yii\db\Query;
use taktwerk\yiiboilerplate\traits\UploadTrait;

/**
 * This is the base-model class for table "general_setting".
 * - - - - - - - - -
 * Generated by the modified Giiant CRUD Generator by taktwerk.com
 *
 * @property integer $id
 * @property integer $client_id
 * @property string $sla_plan
 * @property integer $deleted_by
 * @property string $deleted_at
 * @property string $attached_file
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property string $toString
 * @property string $entryDetails
 *
 * @property \taktwerk\yiiboilerplate\modules\customer\models\Client $client
 */
class GeneralSetting extends TwActiveRecord implements \taktwerk\yiiboilerplate\modules\backend\models\RelationModelInterface{

    /**
     * We can upload images and files to this model, so we need our helper trait.
     */
    use UploadTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'general_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [
                ['client_id', 'sla_plan'],
                'required'
            ],
            [
                ['client_id', 'deleted_by'],
                'integer'
            ],
            [
                ['deleted_at'],
                'safe'
            ],
            [
                ['sla_plan', 'attached_file'],
                'string',
                'max' => 255
            ],
            [
                ['client_id'],
                'unique'
            ],
            [
                ['client_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => \taktwerk\yiiboilerplate\modules\customer\models\Client::class,
                'targetAttribute' => ['client_id' => 'id']
            ]
        ];
        
        return  array_merge($rules, $this->buildRequiredRulesFromFormDependentShowFields());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('twbp', 'ID'),
            'client_id' => \Yii::t('twbp', 'Client'),
            'sla_plan' => Yii::t('twbp', 'Sla Plan'),
            'created_by' => \Yii::t('twbp', 'Created By'),
            'created_at' => \Yii::t('twbp', 'Created At'),
            'updated_by' => \Yii::t('twbp', 'Updated By'),
            'updated_at' => \Yii::t('twbp', 'Updated At'),
            'deleted_by' => \Yii::t('twbp', 'Deleted By'),
            'deleted_at' => \Yii::t('twbp', 'Deleted At'),
            'attached_file' => Yii::t('twbp', 'Attached File'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributePlaceholders()
    {
        return [
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
        ];
    }

    /**
     * Auto generated method, that returns a human-readable name as string
     * for this model. This string can be called in foreign dropdown-fields or
     * foreign index-views as a representative value for the current instance.
     *
     * @author: taktwerk
     * This method is auto generated with a modified Model-Generator by taktwerk.com
     * @return String
     */
    public function toString()
    {
        return $this->sla_plan; // this attribute can be modified
    }


    /**
     * Getter for toString() function
     * @return String
     */
    public function getToString()
    {
        return $this->toString();
    }

    /**
     * Description for the table/model to show on grid an form view
     * @return String
     */
    public static function tableHint()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return [
            'client',
        ];
    }

  
     /**
     * Get all entries related to client with system client
     * @param bool $removedDeleted
     * @return \taktwerk\yiiboilerplate\models\TwActiveQuery|\yii\db\ActiveQuery
     */
    public static function find($removedDeleted = true){

        $model = parent::find($removedDeleted);

        $model->findWithSystemEntry(self::tableName(),$removedDeleted);

        return $model;
    }
                /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(
            \taktwerk\yiiboilerplate\modules\customer\models\Client::class,
            ['id' => 'client_id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function clientList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\customer\models\Client::filter($q);
    }
    

    /**
     * Return details for dropdown field
     * @return string
     */
    public function getEntryDetails()
    {
        return $this->toString;
    }

    /**
     * @param $string
     */
    public static function fetchCustomImportLookup($string)
    {
            return self::find()->andWhere([self::tableName() . '.sla_plan' => $string]);
    }

    /**
     * Load data into object
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;

        // Don't update these fields as they are handled separately.
        unset($data[$scope]['attached_file']);

        return parent::load($data,$formName);
    }

    /**
     * User for filtering results for Select2 element
     * @param null $q
     * @return array
     */
    public static function filter($q = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, sla_plan AS text')
            ->from(self::tableName())
            ->andWhere([self::tableName() . '.deleted_at' => null])
            ->andWhere(['like', 'sla_plan', $q])
            ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }

        /**
     * Get related CRUD controller's class
     * @return \taktwerk\yiiboilerplate\modules\setting\controllers\GeneralSettingController|\yii\web\Controller
     */
    public function getControllerInstance()
    {
        if(class_exists('\taktwerk\yiiboilerplate\modules\setting\controllers\GeneralSettingController')){
            return new \taktwerk\yiiboilerplate\modules\setting\controllers\GeneralSettingController("general-setting", \taktwerk\yiiboilerplate\modules\setting\Module::getInstance());
        }
    }
}