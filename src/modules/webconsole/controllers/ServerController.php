<?php

namespace taktwerk\yiiboilerplate\modules\webconsole\controllers;

use app\models\User;
use JsonRpc2\Controller;
use Exception;
use Yii;
use yii\helpers\Json;

class ServerController extends Controller
{
    /**
     * @param $message
     * @return array
     */
    public function actionUpdate($message)
    {
        return ["message" => "hello $message"];
    }

    /**
     * @param string $id
     * @return \yii\base\Action
     * @throws \JsonRpc2\Exception
     */
    public function createAction($id)
    {
        // If it's unknown,
        try {
            $action = parent::createAction($id);
        } catch(\Exception $e) {
            $id = 'execute';
            $action = parent::createAction($id);
        }
        return $action;
    }

    /**
     * @param $user
     * @param $passwd
     * @return string
     * @throws Exception
     */
    public function actionLogin($user, $passwd) {
        if (strcmp($user, 'taktwerk') == 0 &&
            strcmp($passwd, 'passepartout.0') == 0) {
            // If you need to handle more than one user you can
            // create new token and save it in database
            return md5($user . ":" . $passwd);
        } else {
            throw new Exception("Wrong Password");
        }
    }

    public function actionExecute()
    {
        $command = $this->requestObject->method;

        // Add params


        $output = ($command && !strlen($command) <= 0) ? $this->executeCommand($command) : '';
        if ($output && substr($output, -1) == "\n") {
            $output = substr($output, 0, -1);
        }

        return $output;
    }

    /**
     * @param $command
     * @return bool|string
     */
    protected function executeCommand($command)
    {
        $descriptors = array(
            0 => array('pipe', 'r'), // STDIN
            1 => array('pipe', 'w'), // STDOUT
            2 => array('pipe', 'w')  // STDERR
        );

        // Build params
        $requestObject = Json::decode(file_get_contents('php://input'), false);
        $params = $requestObject->params;
        unset($params[0]);
        foreach ($params as $param) {
            $command .= ' ' . $param;
        }

        $process = proc_open($command . ' 2>&1', $descriptors, $pipes);
        if (!is_resource($process)) die("Can't execute command.");

        // Nothing to push to STDIN
        fclose($pipes[0]);

        $output = stream_get_contents($pipes[1]);
        fclose($pipes[1]);

        $error = stream_get_contents($pipes[2]);
        fclose($pipes[2]);

        // All pipes must be closed before "proc_close"
        $code = proc_close($process);

        return $output;
    }
}
