<?php

namespace taktwerk\yiiboilerplate\modules\webconsole\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Site controller.
 */
class DefaultController extends Controller
{
    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->layout = '@taktwerk/yiiboilerplate/modules/webconsole/views/layout.php';
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
