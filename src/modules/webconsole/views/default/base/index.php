<?php
/* @var $this yii\web\View */
$this->title .= 'Webconsole';

$url = \yii\helpers\Url::to(['/webconsole/server']);
$terminal = <<<JS
    jQuery(function($) {
        $('#terminal').terminal("$url", {
            login: false,
            greetings: "You are authenticated"});
    });
JS;

$this->registerJs($terminal);
?>

<div class="site-index">

    <div id="terminal">

    </div>

</div>
