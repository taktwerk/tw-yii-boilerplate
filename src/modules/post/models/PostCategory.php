<?php

namespace taktwerk\yiiboilerplate\modules\post\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\post\models\base\PostCategory as BasePostCategory;

/**
 * This is the model class for table "post_category".
 */
class PostCategory extends BasePostCategory
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

    /**
     * @return String
     */
    public function toString()
    {
        return Yii::t('twbp', $this->name);
    }
}
