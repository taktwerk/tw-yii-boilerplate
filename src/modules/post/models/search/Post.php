<?php

namespace taktwerk\yiiboilerplate\modules\post\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\post\models\Post as PostModel;

/**
 * Post represents the model behind the search form about `taktwerk\yiiboilerplate\modules\post\models\Post`.
 */
class Post extends PostModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'post_category_id',
                    'author_id',
                    'language_id',
                    'title',
                    'body_html',
                    'excerpt',
                    'is_featured',
                    'slug',
                    'seo_title',
                    'meta_description',
                    'meta_keywords',
                    'status',
                    'published_at',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PostModel::find();

        $this->parseSearchParams(PostModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => $this->parseSortParams(PostModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(PostModel::class),
                'params' => [
                    'page' => $this->parsePageParams(PostModel::class),
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('post_category_id', $query);
        $this->applyHashOperator('author_id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('language_id', $query);
        $this->applyLikeOperator('title', $query);
        $this->applyLikeOperator('body_html', $query);
        $this->applyLikeOperator('excerpt', $query);
        $this->applyLikeOperator('is_featured', $query);
        $this->applyLikeOperator('slug', $query);
        $this->applyLikeOperator('seo_title', $query);
        $this->applyLikeOperator('meta_description', $query);
        $this->applyLikeOperator('meta_keywords', $query);
        $this->applyLikeOperator('status', $query);
        $this->applyDateOperator('published_at', $query, true);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(PostModel::class);
    }
}
