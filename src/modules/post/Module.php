<?php


namespace taktwerk\yiiboilerplate\modules\post;

class Module extends \taktwerk\yiiboilerplate\components\TwModule
{
    /**
     * @var string
     */
    public $defaultRoute = 'post';

    /**
     * Init module
     */
    public function init()
    {
        parent::init();

        // When in console mode, switch controller namespace to commands
        if (\Yii::$app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'taktwerk\yiiboilerplate\modules\post\commands';
        }
    }
}
