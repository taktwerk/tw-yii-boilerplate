<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200911_204000_add_to_table_post_category_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_post_category_deleted_at_parent_id', '{{%post_category}}', ['deleted_at','parent_id']);
    }

    public function down()
    {
        echo "m200911_204000_add_to_table_post_category_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
