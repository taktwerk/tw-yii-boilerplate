<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200911_203944_add_to_table_post_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_post_deleted_at_author_id', '{{%post}}', ['deleted_at','author_id']);
        $this->createIndex('idx_post_deleted_at_language_id', '{{%post}}', ['deleted_at','language_id']);
        $this->createIndex('idx_post_deleted_at_post_category_id', '{{%post}}', ['deleted_at','post_category_id']);
        $this->alterColumn('{{%post}}','is_featured', $this->tinyInteger(1)->defaultValue(null));
    }

    public function down()
    {
        echo "m200911_203944_add_to_table_post_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
