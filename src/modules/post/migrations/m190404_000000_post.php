<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m190404_000000_post extends TwMigration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable(
            '{{%post_category}}',
            [
                'id' => $this->primaryKey(),
                'parent_id' => $this->integer()->null(),
                'order' => $this->integer()->unsigned()->notNull(),
                'name' => $this->string(255)->notNull(),
                'slug' => $this->string(255)->notNull()
            ],
            $tableOptions
        );

        $this->createIndex('post_category_order', '{{%post_category}}', ['order', 'name']);
        $this->addForeignKey('post_category_fk_parent_id', '{{%post_category}}', 'parent_id', '{{%post_category}}', 'id');

        $this->createTable(
            '{{%post}}',
            [
                'id' => $this->primaryKey(),
                'post_category_id' => $this->integer()->notNull(),
                'author_id' => $this->integer()->null(),
                'language_id' => $this->string(5)->notNull(),
                'title' => $this->string(255)->notNull(),
                'body_html' => $this->text(),
                'excerpt' => $this->text(),
                'is_featured' => $this->tinyInteger()->defaultValue(0),
                'slug' => $this->string(255)->null(),
                'seo_title' => $this->string(255)->null(),
                'meta_description' => $this->text(),
                'meta_keywords' => $this->text(),
                'status' => "ENUM('published', 'draft', 'pending') NOT NULL DEFAULT 'draft'",
                'published_at' => $this->dateTime(),
            ],
            $tableOptions
        );
        $this->addForeignKey('post_fk_post_category_id', '{{%post}}', 'post_category_id', '{{%post_category}}', 'id');
        $this->addForeignKey('post_fk_language_id', '{{%post}}', 'language_id', '{{%language}}', 'language_id');
        $this->addForeignKey('post_fk_author_id', '{{%post}}', 'author_id', '{{%user}}', 'id');
        $this->createIndex('post_publication', '{{%post}}', ['status', 'published_at', 'language_id']);
    }

    public function safeDown()
    {
        $this->dropTable('{{%post_category}}');
        $this->dropTable('{{%post}}');
    }
}
