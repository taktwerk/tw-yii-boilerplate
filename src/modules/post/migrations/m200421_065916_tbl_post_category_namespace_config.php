<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200421_065916_tbl_post_category_namespace_config extends TwMigration
{
    public function up()
    {
        $tbl = "{{%post_category}}";
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\post"}';
        $this->addCommentOnTable($tbl, $comment);
    }

    public function down()
    {
        echo "m200421_065916_tbl_post_category_namespace_config cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
