<?php
//Generation Date: 11-Sep-2020 02:55:47pm
namespace taktwerk\yiiboilerplate\modules\post\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * PostCategoryController implements the CRUD actions for PostCategory model.
 */
class PostCategoryController extends TwCrudController
{
}
