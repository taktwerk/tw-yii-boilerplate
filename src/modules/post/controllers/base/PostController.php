<?php
//Generation Date: 11-Sep-2020 02:55:46pm
namespace taktwerk\yiiboilerplate\modules\post\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends TwCrudController
{
}
