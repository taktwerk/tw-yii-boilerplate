<?php
//Generation Date: 03-Nov-2020 10:29:12am
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\post\models\PostCategory $model
 * @var boolean $useModal
 */

$this->title = Yii::t('twbp', 'Post Category') . ' #' . (is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey) . ', ' . Yii::t('twbp', 'View') . ' - ' . $model->toString();
if(!$fromRelation) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('twbp', 'Post Categories'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => (string)is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('twbp', 'View');
}
$basePermission = Yii::$app->controller->module->id . '_' . Yii::$app->controller->id;

Yii::$app->controller->renderCustomBlocks(Yii::$app->controller::POS_MAIN);

?>
<div class="box box-default">
	<div class="giiant-crud box-body"
		id="post-category-view">

		<!-- flash message -->
        <?php if (Yii::$app->session->getFlash('deleteError') !== null) : ?>
            <span class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= Yii::$app->session->getFlash('deleteError') ?>
            </span>
        <?php endif; ?>
        <?php if (!$useModal) : ?>
            <div class="clearfix crud-navigation">
			<!-- menu buttons -->
			<div class='pull-left'>
                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_update'
                    ) && $model->editable() ?
                        Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span> ' . Yii::t('twbp', 'Edit'),
                            [
                                'update',
                                'id' => $model->id,                                 'fromRelation' => $fromRelation,
                                'show_deleted'=>Yii::$app->request->get('show_deleted')
                            ],
                            ['class' => 'btn btn-info']
                        )
                    :
                        null
                    ?>
                     <?php if(method_exists($model,'getUploadFields')  && Yii::$app->getUser()->can(
                        $basePermission . '_recompress'
                    )){
                    	$fields = $model->getUploadFields();
                    	$isCompressible = false;
                    	$compressibleFileTypes = $model::$compressibleFileTypes;
                    	foreach($fields as $field){
                        	if(in_array($model->getFileType($field),$compressibleFileTypes)){
                            	$isCompressible = true;
                            	break;
                        	}
                    	}
                    if($isCompressible){	
                       echo Html::a(
                            '<span class="glyphicon glyphicon-compressed"></span> ' . Yii::t('twbp', 'Recompress'),
                            ['recompress', 'id' => $model->id, 'fromRelation' => $fromRelation],
                            ['class' => 'btn btn-warning','title'=>Yii::t('twbp', 'Adds a compression job for this item\'s files')]
                        );}
                   
                    }?>
                    
                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_create'
                    ) ?
                        Html::a(
                            '<span class="glyphicon glyphicon-copy"></span> ' . Yii::t('twbp', 'Copy'),
                            ['create', 'id' => $model->id, 'fromRelation' => $fromRelation],
                            ['class' => 'btn btn-success']
                        )
                    :
                        null
                    ?>
                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_create'
                    ) ?
                        Html::a(
                            '<span class="glyphicon glyphicon-plus"></span>
				' . Yii::t('twbp', 'New'), ['create', 'fromRelation' =>
				$fromRelation], ['class' => 'btn btn-success create-new'] ) : null
				?>
			</div>
			<div class="pull-right">
                    <?= \taktwerk\yiiboilerplate\widget\CustomActionDropdownWidget::widget(['model'=>$model]) ?>
                    <?php if(Yii::$app->controller->crudMainButtons['listall']): ?>
                    <?= Html::a(
                        '<span class="glyphicon glyphicon-list"></span> '. Yii::t('twbp', 'List {model}',
                            ['model' => \Yii::t('twbp','Post Categories')]                        ),
                        ['index'],
                        ['class' => 'btn btn-default']
                    ) ?>
                    <?php endif; ?>
                </div>
		</div>
        <?php endif; ?>
        <?php $this->beginBlock('taktwerk\yiiboilerplate\modules\post\models\PostCategory'); ?>

        <?php $viewColumns = [
                   [
            'attribute'=> 'id'
           ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::attributeFormat
                [
                    'format' => 'html',
                    'attribute' => 'parent_id',
                    'value' => function ($model) {
                        $foreign = $model->getParent()->one();
                        if ($foreign) {
                            if (Yii::$app->getUser()->can('app_post-category_view') && $foreign->readable()) {
                                return Html::a($foreign->toString, [
                                    'post-category/view',
                                    'id' => $model->getParent()->one()->id,
                                ], ['data-pjax' => 0]);
                            }
                            return $foreign->toString;
                        }
                        return '<span class="label label-warning">?</span>';
                    },
                ],
[
        'attribute' => 'order',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->order != strip_tags($model->order)){
                return \yii\helpers\StringHelper::truncate($model->order,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->order,500));
            }
        },
    ],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],
[
        'attribute' => 'slug',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->slug != strip_tags($model->slug)){
                return \yii\helpers\StringHelper::truncate($model->slug,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->slug,500));
            }
        },
    ],
        ];

        // view columns overwrite
        $viewColumns = Yii::$app->controller->crudColumnsOverwrite($viewColumns, 'view');

        echo DetailView::widget([
        'model' => $model,
        'attributes' => $viewColumns
        ]); ?>

        
        <hr />

        <?=  ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::deleteButton($model, "twbp", ['id' => $model->id], 'view')?>

        <?php $this->endBlock(); ?>


        
        <?php $this->beginBlock('Posts'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsPosts = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('post_post_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('post_post_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('post_post_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/post/post' . '/' . $action;
                        $params['Post'] = [
                            'post_category_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'post'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'author_id',
                    'value' => function ($model) {
                        if ($rel = $model->getAuthor()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'user/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'language_id',
                    'value' => function ($model) {
                        if ($rel = $model->getLanguage()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'language/view',
                                    'language_id' => $rel->language_id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'title',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->title != strip_tags($model->title)){
                return \yii\helpers\StringHelper::truncate($model->title,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->title,500));
            }
        },
    ],
[
        'attribute' => 'body_html',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->body_html != strip_tags($model->body_html)){
                return \yii\helpers\StringHelper::truncate($model->body_html,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->body_html,500));
            }
        },
    ],
[
        'attribute' => 'excerpt',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->excerpt != strip_tags($model->excerpt)){
                return \yii\helpers\StringHelper::truncate($model->excerpt,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->excerpt,500));
            }
        },
    ],
                'is_featured:boolean',
[
        'attribute' => 'slug',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->slug != strip_tags($model->slug)){
                return \yii\helpers\StringHelper::truncate($model->slug,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->slug,500));
            }
        },
    ],
[
        'attribute' => 'seo_title',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->seo_title != strip_tags($model->seo_title)){
                return \yii\helpers\StringHelper::truncate($model->seo_title,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->seo_title,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\post\models\Post::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsPosts = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsPosts, 'tab'):$columnsPosts;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('post_post_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Post'),
                [
                    '/post/post/create',
                    'Post' => [
                        'post_category_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getPosts(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-posts',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsPosts
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Post Categories'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsPostCategories = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('post_post-category_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('post_post-category_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('post_post-category_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/post/post-category' . '/' . $action;
                        $params['PostCategory'] = [
                            'parent_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'post-category'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'order',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->order != strip_tags($model->order)){
                return \yii\helpers\StringHelper::truncate($model->order,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->order,500));
            }
        },
    ],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],
[
        'attribute' => 'slug',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->slug != strip_tags($model->slug)){
                return \yii\helpers\StringHelper::truncate($model->slug,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->slug,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\post\models\PostCategory::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsPostCategories = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsPostCategories, 'tab'):$columnsPostCategories;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('post_post-category_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Post Category'),
                [
                    '/post/post-category/create',
                    'PostCategory' => [
                        'parent_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getPostCategories(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-postcategories',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsPostCategories
        ])?>
        </div>
                <?php $this->endBlock() ?>

<?php /* MAPPED MODELS RELATIONS - START*/?>
<?php 
$extraTabItems = [];
foreach(\Yii::$app->modelNewRelationMapper->newRelationsMapping as $modelClass=>$relations){
    if(is_a($model,$modelClass)){
        foreach($relations as $relName => $relation){
            $relationKey = array_key_first($relation[1]);
            $foreignKey= $relation[1][$relationKey];
            $relClass = $relation[0];
            $baseName = \yii\helpers\StringHelper::basename($relClass);
            $cntrler = Inflector::camel2id($baseName, '-', true);
            $pluralName= Inflector::camel2words(Inflector::pluralize($baseName));
            $singularName = Inflector::camel2words(Inflector::singularize($baseName));
            $basePerm =  \Yii::$app->controller->module->id.'_'.$cntrler;
            $this->beginBlock($pluralName);
?>
<?php 
$relatedModelObject = Yii::createObject($relClass::className());
$relationController = $relatedModelObject->getControllerInstance();
//TODO CrudHelper::getColumnFormat('\app\modules\guide\models\Guide', 'id');
?>
                    <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
       	<div class="clearfix"></div>
        <div>
        <?php $relMapCols = [[
                    'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can($basePerm.'_view') ? '{view} ' : '') . (Yii::$app->getUser()->can($basePerm.'_update') ? '{update} ' : '') . (Yii::$app->getUser()->can($basePerm.'_delete') ? '{delete} ' : ''),
            'urlCreator' => function ($action, $relation_model, $key, $index) use($model,$relationKey,$foreignKey,$baseName,$relatedBaseUrl) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = $relatedBaseUrl . $action;
                        
                        $params[$baseName] = [
                            $relationKey => $model->{$foreignKey}
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) { 
                             return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => $cntrler
                ],
                
        ];

        $relMapCols =array_merge($relMapCols,array_slice(array_keys($relatedModelObject->attributes),0,5));
        
        $relMapCols = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($relMapCols, 'tab'):$relMapCols;
        
        echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can($basePerm.'_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp',$singularName),
                [
                    '/'.$relationController->module->id.'/'.$cntrler.'/create',
                    $baseName => [
                        $relationKey => $model->{$foreignKey}
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[
                'class'=>'grid-outer-div'
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->{'get'.$relName}(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-'.$relName,
                    ]
                ]
                ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            'columns' =>$relMapCols
        ])?>
        </div>
                <?php $this->endBlock();
                $extraTabItems[] = [
                    'content' => $this->blocks[$pluralName],
                    'label' => '<small>' .
                    \Yii::t('twbp', $pluralName) .
                    '&nbsp;<span class="badge badge-default">' .
                    $model->{'get'.$relName}()->count() .
                    '</span></small>',
                    'active' => false,
                    'visible' =>
                    Yii::$app->user->can('x_'.$basePerm.'_see') && Yii::$app->controller->crudRelations(ucfirst($relName)),
                    ];
?>

<?php 
        }
    }
}
?>

                
<?php /* MAPPED MODELS RELATIONS - END*/ ?>
        <?= Tabs::widget(
            [
                'id' => 'relation-tabs',
                'encodeLabels' => false,
                'items' =>  array_filter(array_merge([
                    [
                        'label' => '<b class=""># ' . $model->id . '</b>',
                        'content' => $this->blocks['taktwerk\yiiboilerplate\modules\post\models\PostCategory'],
                        'active' => true,
                    ],
        (\Yii::$app->hasModule('post'))?[
                        'content' => $this->blocks['Posts'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Posts') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getPosts()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_post_post_see') && Yii::$app->controller->crudRelations('Posts'),
                    ]:null,
        (\Yii::$app->hasModule('post'))?[
                        'content' => $this->blocks['Post Categories'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Post Categories') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getPostCategories()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_post_post-category_see') && Yii::$app->controller->crudRelations('PostCategories'),
                    ]:null,
                    [
                        'content' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\HistoryTab::class)::widget(['model' => $model]),
                        'label' => '<small>' .
                            \Yii::t('twbp', 'History') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getHistory()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('Administrator'),
                    ],
                ]
                ,$extraTabItems))
            ]
        );
        ?>
        <?= ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\RecordHistory::class)::widget(['model' => $model]) ?>
    </div>
</div>

<?php ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
$js = <<<JS
$(document).ready(function() {
    var hash=document.location.hash;
	var prefix="tab_" ;
    if (hash) {
        $('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
    } 
    
    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash=e.target.hash.replace(	"#", "#" + prefix);
    });
});
JS;

$this->registerJs($js);

Yii::$app->controller->renderCustomBlocks(Yii::$app->controller::POS_END);
