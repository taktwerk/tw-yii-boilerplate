<?php
//Generation Date: 11-Sep-2020 02:55:46pm
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\post\models\search\Post $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="post-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'post_category_id') ?>

        <?= $form->field($model, 'author_id') ?>

        <?= $form->field($model, 'language_id') ?>

        <?= $form->field($model, 'title') ?>

        <?php // echo $form->field($model, 'body_html') ?>

        <?php // echo $form->field($model, 'excerpt') ?>

        <?php // echo $form->field($model, 'is_featured') ?>

        <?php // echo $form->field($model, 'slug') ?>

        <?php // echo $form->field($model, 'seo_title') ?>

        <?php // echo $form->field($model, 'meta_description') ?>

        <?php // echo $form->field($model, 'meta_keywords') ?>

        <?php // echo $form->field($model, 'status') ?>

        <?php // echo $form->field($model, 'published_at') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
