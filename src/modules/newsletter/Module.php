<?php

namespace taktwerk\yiiboilerplate\modules\newsletter;
use Yii;

class Module extends \yii\base\Module implements \yii\base\BootstrapInterface
{
    public $controllerNamespace = 'app\modules\newsletter\controllers';
    public $enableFlashMessages = true;
    public $fromEmail;
    public $bootstrap;
    public $userSettings = [
        'layout' => '@taktwerk-boilerplate/modules/user/views/layouts/main',
        'indexView' => 'view',
    ];
    public $sendPeriodInSeconds;
    public $verifyEmailText;
    /**
     * @desc Properies which should be hidden/disabled in the newsletter recipient list and newsletter settings
     */
    public $newsletterRecipientDisabledAttributes = [];
    public function init()
    {
        parent::init();
        Yii::configure($this, require(__DIR__ . '/config/common.php'));
        Yii::configure($this, require(__DIR__ . '/config/console.php'));
        Yii::configure($this, require(__DIR__ . '/config/newsletter.php'));
        $this->registerTranslations();
        Yii::$app->mailer->setViewPath('@app/modules/newsletter/mail');
    }

    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                '/newsletter/auth/verify/<token:[\w\-]+>' => 'newsletter/auth/verify/view',
                '/newsletter/user-settings/<token:[\w\-]+>' => 'newsletter/user-settings/index',
            ]
        );

        if ($app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'app\modules\newsletter\commands';
        }
    }

    public function registerTranslations()
    {
        \Yii::$app->i18n->translations['newsletter'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@app/modules/newsletter/messages',

            'fileMap' => [
                'modules/login/app' => 'app.php',
            ],
        ];
    }
}
