<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <?php $this->head() ?>
</head>
<body style="margin: 0;background-image: url(<?=$this->params['background']?>)">
<?php $this->beginBody() ?>
<table border="0" cellpadding="0" cellspacing="0" style="margin:0; padding:0; width: 100%; font-family: 'Open Sans', sans-serif; font-size: 16px; color: white !important;">
    <tbody>
    <tr>
        <td style="min-height: 250px;padding: 15px;margin-right: auto;margin-left: auto;padding-left: 15px;padding-right: 15px;font-size: 14px;">
            <div style="max-width: 600px;margin: auto;border-color: #ddd;margin-bottom: 20px;background-color: #fff;border: 1px solid transparent;border-radius: 4px;box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);">
                <div style="color: #333333;background-color: #f5f5f5;border-color: #ddd;padding: 12px 15px;border-bottom: 1px solid transparent;border-top-left-radius: 3px;border-top-right-radius: 3px;">
                    <h3 style="margin-top: 0;margin-bottom: 0;font-size: 16px;color: inherit;font-weight: 500;line-height: 1.1;">
                        <?= Html::encode($this->params['title']) ?>
                    </h3>
                </div>
                <div class="panel-body" style="padding: 15px;">
                    <div
                        style="background-image: url(<?=$this->params['logo']?>);background-size: contain;background-repeat: no-repeat;font-size: 0;background-color: #fff !important;background-position: center;width: 100%;height: 100px;"
                    ></div>
                    <div style="font-size: 24px; font-weight: 700; color: black;">
                        <div style="text-align: center;margin: 20px;">
                            <span>Hi
                                <?php
                                if (!empty($this->params['newsletterRecipient']->last_name) || !empty($this->params['newsletterRecipient']->first_name)) {
                                    echo ', ' . ($this->params['newsletterRecipient']->first_name ?: '') . ' ' . ($this->params['newsletterRecipient']->last_name ?: '');
                                }
                                ?>
                            </span>
                            <br>
                        </div>
                        <?= $content ?>
                        <?php if (!$this->params['isVerifyEmail'] && !$this->params['showMailRecipientLinksAtTheEnd']) { ?>
                            <?= $this->render('@taktwerk-boilerplate/modules/newsletter/mail/fragments/mailRecipientLinks') ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    </tbody>
</table>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
