<?php
use Yii;
use yii\web\View;
?>

<?php
Yii::$app->mailer->view->on(View::EVENT_END_BODY, function () {
    if ($this->params['showMailRecipientLinksAtTheEnd']) {
        $this->params['showMailRecipientLinksAtTheEnd'] = false;
        echo $this->render('@taktwerk-boilerplate/modules/newsletter/mail/fragments/mailRecipientLinks');
    }
});
?>

<?= $this->params['subLayout'] ?
    $this->render($this->params['subLayout'], ['content' => $content]) :
    $content
?>
