<?php

use yii\helpers\Url;

?>
<!--[if gte mso 9]><!-- -->
<br><br>
<![endif]-->
<table border="0" cellpadding="0" cellspacing="0" style="margin:0; padding:0; width: 100%; font-family: 'Open Sans', sans-serif; font-size: 16px; color: white !important;">
    <tbody>
    <tr>
        <td style="text-align: center;">
            <?php if($this->params['newsletterRecipient']->token) { ?>
                <a href="<?= Url::base(true) . '/newsletter/user-settings/' . $this->params['newsletterRecipient']->token ?>"
                   style="padding: 3px 6px;display: block;border: 1px solid #ccc;color: gray;text-decoration: none;"
                >
                    <?= Yii::t('twbp', 'Recipient settings'); ?>
                </a>
            <?php } ?>
        </td>
    </tr>
    </tbody>
</table>
