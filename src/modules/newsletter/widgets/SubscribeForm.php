<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\widgets;

use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient;
use yii\base\Widget;
use Yii;

class SubscribeForm extends  Widget
{
    /**
     * @desc Setting this true will display group list to user in the subscribe form
     * */
    public $showGroups = false;
    /**
     * @inheritdoc 
     */
    public function run()
    {
        if(Yii::$app->hasModule('newsletter')){
            return $this->render('subscribe-form', [
                'model' => new NewsletterRecipient(),
                'showGroups' => $this->showGroups
            ]);
        }elseif(getenv('YII_ENV')== getenv('YII_ENV_DEV')){
            echo 'Please enable newsletter module to use newsletter widget';
        }
    }
}
