<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\newsletter\assets\NewsletterAsset;
use yii\web\View;
/**
 * View file for NewsletterForm widget.
 *
 * @var \yii\web\View $this
 * @var \taktwerk\yiiboilerplate\newsletter\models\NewsletterRecipient $model
 *
 * @author Samir
 * @since 1.0
 */

taktwerk\yiiboilerplate\modules\newsletter\widgets\assets\SubscribeFormAsset::register($this);
$erMsg = \Yii::$app->session->getFlash('errorSubscribe');
?>

<div class="newsletter-subscribe-form-widget">
<p class="newsletter-info"><?= Yii::t('twbp', 'Register for our newsletter'); ?></p>
<?php if($erMsg){?>
<div class="alert alert-error">
	<p><?php echo $erMsg?></p>
</div>
<?php }?>
    <div class="row">
        <div class="col-12">
            <div id="success-message" class="alert-success alert fade in" style="display: none;">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div id="error-message" class="alert-danger alert fade in" style="display: none;">
            </div>
        </div>
    </div>
<?php
$form = ActiveForm::begin([
    'action' => [
        '/newsletter/auth/register'
    ],
    'options' => [
        'class' => (! $showGroups) ? 'form-inline' : 'horizontal-sub-form',
    ],
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ],
        'template' => '{label}{input}'
    ],
]);

echo '<div class="input-group mb-3 w-100">';

echo $form->field($model, 'email')
    ->textInput([
        'placeholder' => $model->getAttributeLabel('email'),
        //'style' => 'width: 100%;',
])
    ->label(false);

$allNewsletterTopics = \taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterTopic::find()->all();

if ($showGroups && $allNewsletterTopics) {
    echo $form->field($model, 'selectedNewsletterTopics')
        ->checkboxList(ArrayHelper::map($allNewsletterTopics, 'id', 'name'), ['class' => 'v-a-top'])
        ->label(false);
}

echo '<div class="input-group-append">';

echo !(isset($ajaxSubmit) && $ajaxSubmit)?Html::submitButton(Yii::t('twbp', 'Subscribe'), [
    'class' => 'btn btn-primary', 'id'=>'newsletter-ajax-btn'
]):Html::button(Yii::t('twbp', 'Subscribe'), [
    'class' => 'btn btn-primary', 'id'=>'newsletter-ajax-btn'
]);

echo '</div>';

echo '</div>';

ActiveForm::end();
?>
<?php //if(isset($ajaxSubmit) && $ajaxSubmit){
$formId= $form->getId();
$verifyEmailMessage = Yii::t('twbp', 'Verify email was sent');
$jsonString = 'JSON';

    $script = <<< JS
    
   $(document).ready(function () {
    $("#{$formId}").submit(function(e){e.preventDefault();});
    $('#newsletter-ajax-btn').click(function (event) {
            event.preventDefault();
            var el = $("#{$formId}");
               // el.yiiActiveForm('validateAttribute', 'newsletterreceiver-email');
               setTimeout(function(){
                   if($('div.field-newsletterreceiver-email.has-error').length==0){
                       $.ajax({
                         url: el.attr('action'),
                         data: el.serialize(),
                         method:'POST',
                         beforeSend:function(){
                                $('#newsletter-ajax-btn').prop('disabled',true);
                            },
                        error:function( xhr, err ) {
                             var response = xhr.responseJSON;
                             var errorMessage = 'An error occurred, please try again';
                             if (response.error) {
                                 var isJsonError = false;
                                 try {
                                     var parsedJson = $jsonString.parse(response.error);
                                     for (field in parsedJson) {
                                         errorMessage = parsedJson[field];
                                     }
                                } catch (error) {
                                     errorMessage = response.error;
                                }
                             }
                             console.log('ERRRRRRRRRR');
                             $('#success-message').hide();
                             $('#error-message').text(errorMessage);
                             $('#error-message').show();
                         },
                         complete:function(){
                                $('#newsletter-ajax-btn').prop('disabled',false);
                            },
                         success: function(data){
                                if(data.success){
                                    $('#error-message').hide();
                                    $('#success-message').text('$verifyEmailMessage');
                                    $('#success-message').show();
                                    return false;
                                } else {
                                    el[0].reset();
                                    $('#success-message').hide();
                                    $('#error-message').text('An error occurred, please try again');
                                    $('#error-message').show();
                                }
                           }
                      });
                 }
            },200);
            return false;
        });
    });
    
JS;
$this->registerJs($script);
//}?>

</div>
