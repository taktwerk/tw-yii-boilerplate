<div id="newletter-sec" style="display: none">
    <?= $this->render('subscribe-form', [
        'model' => new \taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient(),
        'showGroups' => $showGroups,
        'ajaxSubmit' => true
    ])?>
</div>
