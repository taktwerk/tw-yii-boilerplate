<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\widgets;

use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient;
use yii\base\Widget;
use Yii;
use yii\widgets\InputWidget;
use yii\base\Model;
use yii\base\InvalidParamException;
use yii\helpers\Html;

class SubscribeModelForm extends  Widget
{
    public $showGroups = true;
    /**
     * Renders the widget.
     */
    public function run()
    {
        if (Yii::$app->hasModule('newsletter')) {
            return $this->render('subscribe-model-form', [
                'model' => new NewsletterRecipient(),
                'showGroups'=>$this->showGroups
            ]);
        } elseif(getenv('YII_ENV')== getenv('YII_ENV_DEV')) {
            echo 'Please enable newsletter module to use newsletter widget';
        }
    }
}
