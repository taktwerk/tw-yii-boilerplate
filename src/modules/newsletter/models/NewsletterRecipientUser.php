<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\models;

use taktwerk\yiiboilerplate\modules\customer\models\Client;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use Yii;

use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient as BaseNewsletterRecipient;

class NewsletterRecipientUser extends BaseNewsletterRecipient
{
    public $is_send_verify_mail = false;
    public $user_id;

    static $additionalAttributes = ['selectedNewsletterTopics'];

    public static function primaryKey()
    {
        return ['email'];
    }

    public static function find($removedDeleted = true)
    {
        $modelLeft = (new \yii\db\Query())
            ->from('newsletter_recipient')
            ->select(
                [
                    'newsletter_recipient.id',
                    'newsletter_recipient.first_name',
                    'newsletter_recipient.last_name',
                    'newsletter_recipient.gender',
                    'newsletter_recipient.is_verified',
                    'newsletter_recipient.is_subscribed',
                    'newsletter_recipient.token',
                    'newsletter_recipient.created_by',
                    'newsletter_recipient.created_at',
                    'newsletter_recipient.updated_by',
                    'newsletter_recipient.updated_at',
                    'newsletter_recipient.deleted_by',
                    'newsletter_recipient.deleted_at',
                    'newsletter_recipient.email as email',
                    'newsletter_recipient.user_id',
                ]
            )
            ->leftJoin('user', 'newsletter_recipient.email = user.email');

        $modelRight = (new \yii\db\Query())
            ->select(
                [
                    'newsletter_recipient.id',
                    'newsletter_recipient.first_name',
                    'newsletter_recipient.last_name',
                    'newsletter_recipient.gender',
                    'newsletter_recipient.is_verified',
                    'newsletter_recipient.is_subscribed',
                    'newsletter_recipient.token',
                    'newsletter_recipient.created_by',
                    'newsletter_recipient.created_at',
                    'newsletter_recipient.updated_by',
                    'newsletter_recipient.updated_at',
                    'newsletter_recipient.deleted_by',
                    'newsletter_recipient.deleted_at',
                    'user.email as email',
                    'user.id as user_id',
                ]
            )
            ->from('newsletter_recipient')
            ->rightJoin('user', 'newsletter_recipient.email = user.email')
            ->andWhere([static::tableName() . '.deleted_at' => null]);

        $modelLeft->union($modelRight);

        $model = parent::find(true)
            ->select('*')->from(['newsletter_recipient' => $modelLeft]);

        return $model;
    }

    public function beforeValidate()
    {
        $isSuccessBeforeValidate = parent::beforeValidate();

        if ($isSuccessBeforeValidate) {
            if (!$this->id && !$this->token) {
                $this->token = $this->generateToken();
            }
        }

        return $isSuccessBeforeValidate;
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), self::$additionalAttributes);
    }

    public function beforeSave($insert)
    {
        unset(self::$additionalAttributes['user_id']);
        foreach (self::$additionalAttributes as $key => $additionalAttribute) {
            if ($additionalAttribute === 'user_id') {
                unset(self::$additionalAttributes[$key]);
                unset($this->user_id);
            }
        }


        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $attributeLabels = parent::attributeLabels();

        $attributeLabels['is_send_verify_mail'] = Yii::t('twbp', 'Send verify mail');

        return $attributeLabels;
    }
}
