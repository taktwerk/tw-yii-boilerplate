<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\models;

use taktwerk\yiiboilerplate\modules\newsletter\behaviors\SoftDelete;
use taktwerk\yiiboilerplate\modules\newsletter\dto\NewsletterRecipientRegistrationDto;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use Yii;

use taktwerk\yiiboilerplate\modules\newsletter\models\base\NewsletterRecipient as BaseNewsletterRecipient;

class NewsletterRecipient extends BaseNewsletterRecipient
{
    public $selectedNewsletterTopics = [];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['softdelete'] = SoftDelete::class;

        return $behaviors;
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                [
                    'selectedNewsletterTopics',
                    'language_id',
                ],
                'safe'
            ],
        ]);
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['GUEST_FORM'] = [
            'email','selectedNewsletterTopics'
        ];
        return $scenarios;
    }

    public function load($data, $formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;
        $formData = [];
        if ($scope === '' && !empty($data)) {
            $formData = $data;
            $this->setAttributes($data);
        } elseif (isset($data[$scope])) {
            $formData = $data[$scope];
        }
        if (!empty($formData)) {
            if (!$formData['id'] && !$formData['token']) {
                $formData['token'] = $this->generateToken();
            }
            $this->setAttributes($formData);

            return true;
        }

        return false;
    }

    public function getNewsletterTopics()
    {
        return $this->hasMany(NewsletterTopic::class, ['id' => 'topic_id'])
            ->viaTable('newsletter_recipient_topic', ['recipient_id' => 'id']);
    }

    public function addAllNewsletterTopics($topicIds = [], $exclusiveAdd = true)
    {
        if (!is_array($topicIds)) {
            $topicIds = [];
        }
        $existingTopics = ArrayHelper::map($this->newsletterTopics, 'id', 'id');
        if ($exclusiveAdd) {
            NewsletterRecipientTopic::deleteAll([
                'recipient_id' => $this->id
            ]);
        }
        $toBeAdded = array_diff($topicIds, $existingTopics);
        foreach ($toBeAdded as $topicId) {
            $isExistNewsletterRecipientTopic = (boolean) NewsletterRecipientTopic::find()
                ->where(['recipient_id' => $this->id, 'topic_id' => $topicId])
                ->count();
            if ($isExistNewsletterRecipientTopic) {
                continue;
            }
            $recipientTopic = new NewsletterRecipientTopic();
            $recipientTopic->recipient_id = $this->id;
            $recipientTopic->topic_id = $topicId;
            if (!$recipientTopic->save()) {
                return false;
            }
        }
        $toBeDeleted = array_diff($existingTopics, $topicIds);
        foreach ($toBeDeleted as $topicId) {
            NewsletterRecipientTopic::deleteAll(['topic_id' => $topicId, 'recipient_id' => $this->id]);
        }
        return true;
    }

    public function removeAllNewsletterTopics()
    {
        NewsletterRecipientTopic::deleteAll(['recipient_id' => $this->id]);
    }

    public function generateToken()
    {
        return Yii::$app->security->generateRandomString(20) . time();
    }

    public function register(NewsletterRecipientRegistrationDto $newsletterRecipientRegistrationDto)
    {
        $this->token = $this->generateToken();
        if ($newsletterRecipientRegistrationDto->isMakeVerified()) {
            $this->is_verified = 1;
        }
        if ($newsletterRecipientRegistrationDto->isMakeSubscribed()) {
            $this->is_subscribed = 1;
        }
        $isSaved = $this->save();

        if (!$isSaved) {
            return null;
        }

        return $this;
    }

    public function sendVerifyEmail()
    {
        Yii::$app->mailer->htmlLayout = '@taktwerk-boilerplate/modules/newsletter/mail/layouts/confirm-newsletter-layout';
        Yii::$app->mailer->view->params['newsletterRecipient'] = $this;
        Yii::$app->mailer->view->params['isVerifyEmail'] = true;
        Yii::$app->mailer
            ->compose(
                '@taktwerk-boilerplate/modules/newsletter/mail/confirm-newsletter-confirmation',
                [
                    'recipientToken' => $this->token,
                    'verificationText' => Yii::$app->getModule('newsletter')->verifyEmailText
                ]
            )
            ->setFrom(Yii::$app->getModule('newsletter')->fromEmail)
            ->setTo($this->email)
            ->setSubject(
                Yii::t('twbp', 'Confirm your newsletter subscription.')
            )
            ->send();
    }

    public function beforeSave($insert)
    {
        if (empty($this->language_id)) {
            $this->language_id = 'de';
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            if (!is_array($this->selectedNewsletterTopics) || !count($this->selectedNewsletterTopics)) {
                $this->selectedNewsletterTopics = ArrayHelper::map(
                    NewsletterTopic::find()->where(['is_default' => 1])->all(),
                    'id',
                    'id'
                );
            }
        }
        if ($this->selectedNewsletterTopics) {
            $this->addAllNewsletterTopics($this->selectedNewsletterTopics, false);
        }
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), ['selectedNewsletterTopics']);
    }

    public function sendNewsletter()
    {

    }
}
