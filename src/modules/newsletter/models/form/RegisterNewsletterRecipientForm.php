<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\models\form;

use taktwerk\yiiboilerplate\modules\newsletter\dto\NewsletterRecipientRegistrationDto;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient;
use yii\base\Model;
use Yii;

class RegisterNewsletterRecipientForm extends Model
{
    public $email;
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['email'], 'trim'],
            [['email'], 'required'],
            [['email'], 'email'],
            [['email'], 'string', 'max' => 255],
            [
                'email',
                'unique',
                'targetClass' => NewsletterRecipient::class,
                'targetAttribute' => ['email' => 'email'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Email',
        ];
    }

    /**
     * @param NewsletterRecipientRegistrationDto $newsletterRecipientRegistrationDto
     * @return NewsletterRecipient|null
     * @throws \yii\base\Exception
     */
    public function register(NewsletterRecipientRegistrationDto $newsletterRecipientRegistrationDto)
    {
        $newsletterRecipient = new NewsletterRecipient();
        $newsletterRecipient->email = $this->email;
        $newsletterRecipient->token = Yii::$app->security->generateRandomString(20) . time();
        $newsletterRecipient->language_id = 'de';
        if ($newsletterRecipientRegistrationDto->isMakeVerified()) {
            $newsletterRecipient->is_verified = 1;
        }
        if ($newsletterRecipientRegistrationDto->isMakeSubscribed()) {
            $newsletterRecipient->is_subscribed = 1;
        }
        $isSaved = $newsletterRecipient->save();

        if (!$isSaved) {
            return null;
        }

        return $newsletterRecipient;
    }
}
