<?php

use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterTopic;

class ChangeNewsletterRecipientTopicsForm extends \yii\db\ActiveRecord
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['topic_id', 'required'],
            ['topic_id', 'integer'],
            [
                'topic_id',
                'exist',
                'targetClass' => NewsletterTopic::class,
                'targetAttribute' => ['topic_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'last_name' => 'Last name',
            'first_name' => 'First name',
            'gender' => 'Gender',
        ];
    }
}
