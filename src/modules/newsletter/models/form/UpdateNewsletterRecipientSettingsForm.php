<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\models\form;

use taktwerk\yiiboilerplate\modules\newsletter\components\Hook;
use taktwerk\yiiboilerplate\modules\newsletter\enum\GenderTypeEnum;
use taktwerk\yiiboilerplate\modules\newsletter\exceptions\NewsletterEmailValidationErrorException;
use taktwerk\yiiboilerplate\modules\newsletter\exceptions\NotFoundNewsletterTopicException;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterTopic;
use taktwerk\yiiboilerplate\modules\newsletter\plugins\NewsletterTopicMultipleDropdownSave;
use yii\base\Event;
use yii\base\Model;
use Yii;

class UpdateNewsletterRecipientSettingsForm extends Model
{
    public $id;
    public $email;
    public $last_name;
    public $first_name;
    public $gender;
    public $is_subscribed;
    public $topic_ids;
    public $token;
    public $user_id;
    public $language_id;
    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['token', 'safe'],
            [['email', 'last_name', 'first_name'], 'trim'],
            [['email'], 'required'],
            [['email'], 'email'],
            [['email', 'last_name', 'first_name'], 'string', 'max' => 255],
            [['language_id'], 'string', 'max' => 5],
            [
                'email',
                'unique',
                'targetClass' => NewsletterRecipient::class,
                'when' => function ($model) {
                    return !NewsletterRecipient::find()
                        ->where(['id' => $model->id, 'email' => $model->email])
                        ->exists();
                },
                'message' => 'This email address has already been taken.'
            ],
            [['gender'], 'in', 'range' => [GenderTypeEnum::MALE_TYPE, GenderTypeEnum::FEMALE_TYPE]],
            [['is_subscribed'], 'boolean'],
            [
                ['topic_ids'],
                'each',
                'rule' => [
                    'exist',
                    'targetClass' => NewsletterTopic::class,
                    'targetAttribute' => ['topic_ids' => 'id'],
                ],
                'when' => function() {
                    return !empty($this->topic_ids);
                }
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => \taktwerk\yiiboilerplate\models\User::class,
                'targetAttribute' => ['user_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('twbp', 'Email'),
            'last_name' => Yii::t('twbp', 'Last Name'),
            'first_name' => Yii::t('twbp', 'First Name'),
            'gender' => Yii::t('twbp', 'Gender'),
            'topic_ids' => Yii::t('twbp', 'Newsletter Topics'),
            'is_subscribed' => Yii::t('twbp', 'Subscribe'),
            'language_id' => Yii::t('twbp', 'Language'),
        ];
    }

    public function changeUserSettings(NewsletterRecipient $newsletterRecipientModel)
    {
        $newsletterRecipientModel->email = $this->email;
        $newsletterRecipientModel->first_name = $this->first_name ?: null;
        $newsletterRecipientModel->last_name = $this->last_name ?: null;
        $newsletterRecipientModel->gender = $this->gender ?: null;
        $newsletterRecipientModel->is_subscribed = $this->is_subscribed ?: null;
        $newsletterRecipientModel->language_id = $this->language_id ?: null;
        $isSavedRecipient = $newsletterRecipientModel->save();
        if (!$isSavedRecipient) {
            return false;
        }
        try {
            Event::trigger(
                Hook::class,
                Hook::MULTIPLE_TOPIC_LIST_SAVE,
                new NewsletterTopicMultipleDropdownSave($this->email, $this->topic_ids)
            );
        } catch (NewsletterEmailValidationErrorException $exception) {
            $this->addError('email', $exception->getMessage());
        } catch (NotFoundNewsletterTopicException $exception) {
            $this->addError('topic_ids', $exception->getMessage());
        }

        return true;
    }
}
