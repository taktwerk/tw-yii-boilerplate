<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\models;

use taktwerk\yiiboilerplate\models\TwActiveQuery;
use Yii;
use \taktwerk\yiiboilerplate\modules\newsletter\models\base\Newsletter as BaseNewsletter;

/**
 * This is the model class for table "newsletter".
 */
class Newsletter extends BaseNewsletter
{
    public function rules()
    {
        $rules = parent::rules();

        $newRules = [
            [
                'template',
                function($attribute, $params, $validator) {
                    if (!$this->$attribute) {
                        return true;
                    }
                    try {
                        Yii::$app->mailer->render($this->$attribute, [], false);
                        return true;
                    } catch (ViewNotFoundException $e) {
                        $this->addError($attribute, Yii::t('twbp', 'Wrong view template path'));
                        return false;
                    } catch (yii\base\InvalidArgumentException $e) {
                        $this->addError($attribute, Yii::t('twbp', 'Wrong view template path'));
                        return false;
                    }
                }
            ],
        ];

        $rules = array_merge($rules, $newRules);

        return $rules;
    }



    public function getNewsletterTopic()
    {
        return $this->hasOne(NewsletterTopic::class, ['id' => 'topic_id']);
    }

    public function getCountOfRecipients()
    {
        $newsletterTopic = $this->getNewsletterTopic()->one();
        $countOfRecipients = 0;
        if (!$newsletterTopic) {
            return 0;
        }

        return $newsletterTopic->getSubscribedNewsletterRecipients()->count();
    }

    public function send()
    {

    }
}
