<?php
//Generation Date: 11-Sep-2020 02:54:33pm
namespace taktwerk\yiiboilerplate\modules\newsletter\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipientTopic as NewsletterRecipientTopicModel;

/**
 * NewsletterRecipientTopic represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipientTopic`.
 */
class NewsletterRecipientTopic extends NewsletterRecipientTopicModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'recipient_id',
                    'topic_id',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NewsletterRecipientTopicModel::find();

        $this->parseSearchParams(NewsletterRecipientTopicModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(NewsletterRecipientTopicModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(NewsletterRecipientTopicModel::class),
                'params' => [
                    'page' => $this->parsePageParams(NewsletterRecipientTopicModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('recipient_id', $query);
        $this->applyHashOperator('topic_id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(NewsletterRecipientTopicModel::class);
    }
}
