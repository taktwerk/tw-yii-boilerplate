<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient;

/**
 * NewsletterRecipientSearch represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient`.
 */
class NewsletterRecipientSearch extends NewsletterRecipient
{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'email',
                    'first_name',
                    'last_name',
                    'gender',
                    'is_verified',
                    'is_subscribed',
                    'token',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NewsletterRecipient::find();

        $this->parseSearchParams(NewsletterRecipient::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(NewsletterRecipient::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(NewsletterRecipient::class),
                'params' => [
                    'page' => $this->parsePageParams(NewsletterRecipient::class),
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('is_verified', $query);
        $this->applyHashOperator('is_subscribed', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('email', $query);
        $this->applyLikeOperator('first_name', $query);
        $this->applyLikeOperator('last_name', $query);
        $this->applyLikeOperator('gender', $query);
        $this->applyLikeOperator('token', $query);
        $this->applyLikeOperator('language_id', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(NewsletterRecipient::class);
    }
}
