<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\models\search;

use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient as NewsletterRecipientModel;
use taktwerk\yiiboilerplate\modules\newsletter\models\search\base\NewsletterRecipient as NewsletterRecipientSearchModel;
use yii\data\ActiveDataProvider;

/**
* NewsletterRecipient represents the model behind the search form about `taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient`.
*/
class NewsletterRecipient extends NewsletterRecipientSearchModel
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                ['selectedNewsletterTopics', 'newsletterTopics', 'language_id'],
                'safe'
            ],
        ]);
    }

    public function load($data, $formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;
        $formData = [];
        if ($scope === '' && !empty($data)) {
            $formData = $data;
        } elseif (isset($data[$scope])) {
            $formData = $data[$scope];
        }
        if ($formData) {
            if ($formData['user_id'] == '1') {
                $formData['user_id'] = '!=0';
            } elseif ($formData['user_id'] == '0') {
                $formData['user_id'] = '=0';
            }
            $this->setAttributes($formData);

            return true;
        }

        return false;
    }

    public function search($params)
    {
        $query = NewsletterRecipientModel::find();
        $query->joinWith(['newsletterTopics'])->distinct();

        $this->parseSearchParams(NewsletterRecipientModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(NewsletterRecipientModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(NewsletterRecipientModel::class),
                'params' => [
                    'page' => $this->parsePageParams(NewsletterRecipientModel::class),
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('is_verified', $query);
        $this->applyHashOperator('is_subscribed', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyHashOperator('user_id', $query);
        $this->applyLikeOperator('email', $query);
        $this->applyLikeOperator('language_id', $query);
        $this->applyLikeOperator('first_name', $query);
        $this->applyLikeOperator('last_name', $query);
        $this->applyLikeOperator('gender', $query);
        $this->applyLikeOperator('token', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);

        if ($this->selectedNewsletterTopics) {
            $query->andFilterWhere(['in', 'newsletter_topic.id', $this->selectedNewsletterTopics]);
        }
        

        if ($this->user_id == '!=0') {
            $this->user_id = 1;
        } elseif ($this->user_id == '=0') {
            $this->user_id = 0;
        }

        return $dataProvider;
    }

    public function applyHashOperator($attribute, &$query, $tableName = null)
    {
        $tableName = $tableName ? $tableName : $this::tableName();
        $operator = $this->getOperator($this->$attribute);
        if (!is_array($operator)) {
            $query->andFilterWhere(
                [
                    $tableName . '.' . $attribute => $this->$attribute
                ]
            );
        } elseif (($operator['operator'] == 'OR')) {
            $query->andFilterWhere(
                [
                    $tableName . '.' . $attribute => $operator['operand']
                ]
            );
        } elseif (($operator['operator'] == 'AndLike')) {
            $query->andFilterWhere(
                [
                    'AND',
                    [
                        'like',
                        $tableName . '.' . $attribute,
                        $operator['first']
                    ],
                    [
                        'like',
                        $tableName . '.' . $attribute,
                        $operator['second']
                    ]
                ]
            );
        } elseif (($operator['operator'] == 'OrLike')) {
            $query->andFilterWhere(
                [
                    'OR',
                    [
                        'like',
                        $tableName . '.' . $attribute,
                        $operator['first']
                    ],
                    [
                        'like',
                        $tableName . '.' . $attribute,
                        $operator['second']
                    ]
                ]
            );
        }  elseif (($operator['operator'] == 'Empty')) {
            $query->andFilterWhere([
                    'OR',
                    [
                        'is',
                        $tableName . '.' . $attribute,
                        new \yii\db\Expression('null')
                    ],
                    ['=', "LENGTH(".$tableName . '.' . $attribute.")", 0]
                ]
            );
        }  elseif (($operator['operator'] == 'NotEmpty')) {
            $attribute_with_table = $tableName . '.' . $attribute;
            $query->andWhere("$attribute_with_table IS NOT NULL AND TRIM($attribute_with_table) <> ''");
        } elseif (($operator['operator'] !== 'between')) {
            $query->andFilterWhere(
                [
                    $operator['operator'],
                    $tableName . '.' . $attribute,
                    $operator['operand']
                ]
            );
        } else {
            $query->andFilterWhere(
                [
                    $operator['operator'],
                    $tableName . '.' . $attribute,
                    $operator['start'],
                    $operator['end']
                ]
            );
        }
    }
}
