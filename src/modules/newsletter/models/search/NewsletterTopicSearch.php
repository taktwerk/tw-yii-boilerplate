<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\models\search;

use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterTopic;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;

class NewsletterTopicSearch extends Model
{
    public $name;

    public function rules()
    {
        return [
            ['name', 'trim'],
            ['name', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('twbp', 'Username'),
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function search($params)
    {
        $query = NewsletterTopic::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
