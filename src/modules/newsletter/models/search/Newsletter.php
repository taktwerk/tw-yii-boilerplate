<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\models\search;

use taktwerk\yiiboilerplate\modules\newsletter\models\search\base\Newsletter as NewsletterSearchModel;

/**
* Newsletter represents the model behind the search form about `taktwerk\yiiboilerplate\modules\newsletter\models\Newsletter`.
*/
class Newsletter extends NewsletterSearchModel{

}