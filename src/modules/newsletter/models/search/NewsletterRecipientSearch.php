<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\models\search;

use taktwerk\yiiboilerplate\modules\newsletter\models\search\base\NewsletterRecipientSearch as NewsletterRecipientSearchModel;
use yii\data\ActiveDataProvider;
use Yii;

class NewsletterRecipientSearch extends NewsletterRecipientSearchModel
{}
