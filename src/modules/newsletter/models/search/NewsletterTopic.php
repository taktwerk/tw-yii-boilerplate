<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\models\search;

use taktwerk\yiiboilerplate\modules\newsletter\models\search\base\NewsletterTopic as NewsletterTopicSearchModel;

/**
* NewsletterTopic represents the model behind the search form about `taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterTopic`.
*/
class NewsletterTopic extends NewsletterTopicSearchModel{

}