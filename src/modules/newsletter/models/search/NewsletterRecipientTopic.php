<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\models\search;

use taktwerk\yiiboilerplate\modules\newsletter\models\search\base\NewsletterRecipientTopic as NewsletterRecipientTopicSearchModel;

/**
* NewsletterRecipientTopic represents the model behind the search form about `taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipientTopic`.
*/
class NewsletterRecipientTopic extends NewsletterRecipientTopicSearchModel{

}