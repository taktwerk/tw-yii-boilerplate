<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\models\search;

use taktwerk\yiiboilerplate\modules\newsletter\models\Newsletter;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;

class NewsletterSearch extends Model
{
    public $name;
    public $template;
    public $topic_id;

    public function rules()
    {
        return [
            ['name', 'trim'],
            ['name', 'safe'],
            ['template', 'trim'],
            ['template', 'safe'],
            ['topic_id', 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('twbp', 'Name'),
            'topic_id' => Yii::t('twbp', 'Topic'),
            'template' => Yii::t('twbp', 'Template'),
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function search($params)
    {
        $query = Newsletter::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        if ($this->name) {
            $query->andFilterWhere(['like', 'name', $this->name]);
        }
        if ($this->template) {
            $query->andFilterWhere(['like', 'template', $this->template]);
        }
        if ($this->topic_id) {
            $query->andFilterWhere(['=', 'topic_id', $this->topic_id]);
        }

        return $dataProvider;
    }
}
