<?php


namespace taktwerk\yiiboilerplate\modules\newsletter\models;

use taktwerk\yiiboilerplate\modules\newsletter\models\base\NewsletterTopic as BaseNewsletterTopic;
use yii\helpers\ArrayHelper;

class NewsletterTopic extends BaseNewsletterTopic
{
    public function getNewsletterRecipients()
    {
        return $this->hasMany(NewsletterRecipient::class, ['id' => 'recipient_id'])
            ->viaTable('newsletter_recipient_topic', ['topic_id' => 'id']);
    }

    public function getSubscribedNewsletterRecipients()
    {
        return $this->getNewsletterRecipients()->where(['is_subscribed' => 1])->where(['is_verified' => 1]);
    }

    public static function toIdName()
    {
        $allTopics = self::find()->select(['id', 'name'])->asArray()->all();

        return ArrayHelper::map($allTopics, 'id', 'name');
    }
}
