<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\components;

use taktwerk\yiiboilerplate\modules\newsletter\dto\NewsletterRecipientRegistrationDto;
use taktwerk\yiiboilerplate\modules\newsletter\exceptions\NewsletterRecipientNotRegisterException;
use taktwerk\yiiboilerplate\modules\newsletter\exceptions\NewsletterRecipientRegistrationValidationException;
use taktwerk\yiiboilerplate\modules\newsletter\models\form\RegisterNewsletterRecipientForm;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterTopic;
use yii\base\BaseObject;
use Yii;
use yii\helpers\ArrayHelper;

class NewsletterRecipientRegistrationComponent extends BaseObject
{
    public $newsletterRecipientRegistrationDto;

    protected $registerNewsletterRecipientFormModel;

    public function __construct(
        NewsletterRecipientRegistrationDto $newsletterRecipientRegistrationDto,
        $model = null,
        $config = []
    ) {
        $this->newsletterRecipientRegistrationDto = $newsletterRecipientRegistrationDto;
        $this->registerNewsletterRecipientFormModel = $model ? : new NewsletterRecipient(['scenario'=>'GUEST_FORM']);

        parent::__construct($config);
    }

    /**
     * @throws NewsletterRecipientNotRegisterException
     * @throws NewsletterRecipientRegistrationValidationException
     */
    public function register()
    {
        $registrationData = $this->newsletterRecipientRegistrationDto->getRegistrationData();
        if (empty($registrationData['NewsletterRecipient']) ||
            empty($registrationData['NewsletterRecipient']['email'])
        ) {
            throw(
                new NewsletterRecipientRegistrationValidationException(
                    'Email is required'
                )
            );
        }
        $email = $registrationData['NewsletterRecipient']['email'];
        $existNewsletterRecipient = NewsletterRecipient::find(true)
            ->where(['email' => $email])
            ->one();
        if ($existNewsletterRecipient /* && $existNewsletterRecipient->user_id */) {
            if ($existNewsletterRecipient->is_subscribed) {
                throw(new NewsletterRecipientRegistrationValidationException('User is already subscribed'));
            }
            $existNewsletterRecipient->is_subscribed = 1;
            $existNewsletterRecipient->is_verified = 0;
            $existNewsletterRecipient->save();
        } else {
            if (
                !$this->registerNewsletterRecipientFormModel->load(
                    $this->newsletterRecipientRegistrationDto->getRegistrationData()
                )
            ) {
                $this->registerNewsletterRecipientFormModel->attributes = $this->newsletterRecipientRegistrationDto
                    ->getRegistrationData();
                if (empty($this->registerNewsletterRecipientFormModel->attributes)) {
                    throw(
                        new NewsletterRecipientRegistrationValidationException(
                            'Something went wrong with loading data'
                        )
                    );
                }
            }
            if (!$this->registerNewsletterRecipientFormModel->validate()) {
                throw(
                    new NewsletterRecipientRegistrationValidationException(
                        json_encode($this->registerNewsletterRecipientFormModel->getFirstErrors())
                    )
                );
            }

            if ($existNewsletterRecipient && $this->newsletterRecipientRegistrationDto->isCheckEmailUnique()) {
                throw(
                    new NewsletterRecipientRegistrationValidationException('Recipient with this email is exist')
                );
            }
        }

        $newsletterRecipient = $existNewsletterRecipient ?
            $existNewsletterRecipient :
            $this->registerNewsletterRecipientFormModel->register($this->newsletterRecipientRegistrationDto);

        if (!$newsletterRecipient) {
            throw(
                new NewsletterRecipientNotRegisterException('The user has not been registered. Something went wrong.')
            );
        }

        if (empty($existNewsletterRecipient)) {
            if (!is_array($this->registerNewsletterRecipientFormModel->selectedNewsletterTopics) ||
                !count($this->registerNewsletterRecipientFormModel->selectedNewsletterTopics)) {
                $this->registerNewsletterRecipientFormModel->selectedNewsletterTopics = ArrayHelper::map(
                    NewsletterTopic::find()->where(['is_default' => 1])->all(), 'id', 'id'
                );
            }
        }
        $newsletterRecipient->addAllNewsletterTopics(
            $this->registerNewsletterRecipientFormModel->selectedNewsletterTopics,
            false
        );

        if ($this->newsletterRecipientRegistrationDto->isSendVerifyEmail()) {
            $this->sendVerifyEmail($newsletterRecipient);
        }

        return $newsletterRecipient;
    }

    /**
     * @return object
     */
    public function getRegisterNewsletterRecipientFormModel()
    {
        return $this->registerNewsletterRecipientFormModel;
    }

    /**
     * @param NewsletterRecipient $newsletterRecipient
     */
    public function sendVerifyEmail(NewsletterRecipient $newsletterRecipient)
    {
        $newsletterRecipient->sendVerifyEmail();
    }
}
