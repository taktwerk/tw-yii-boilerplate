<?php


namespace taktwerk\yiiboilerplate\modules\newsletter\components;

use yii\base\Component;

class Hook extends Component
{
    const MULTIPLE_TOPIC_LIST_SELECT = 'MULTIPLE_TOPIC_LIST_SELECT';
    const MULTIPLE_TOPIC_LIST_SAVE = 'MULTIPLE_TOPIC_LIST_SAVE';

    public static function show($event)
    {
        $event->show($event);
    }

    public static function save($event)
    {
        $event->save($event);
    }
}
