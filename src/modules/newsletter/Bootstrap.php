<?php

namespace taktwerk\yiiboilerplate\modules\newsletter;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                '/newsletter/auth/verify/<token:[\w\-]+>' => 'newsletter/auth/verify/view',
                '/newsletter/user-settings/<token:[\w\-]+>' => 'newsletter/user-settings/index',
            ]
        );

        if ($app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'taktwerk\yiiboilerplate\modules\newsletter\commands';
        }
    }
}
