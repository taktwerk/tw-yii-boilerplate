<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\commands;

use taktwerk\yiiboilerplate\modules\newsletter\exceptions\NotFoundNewsletterException;
use taktwerk\yiiboilerplate\modules\usermanager\models\User;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient;
use yii\console\Controller;
use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class RecipientController extends Controller
{
    /**
     * @param $newsletterId
     * @throws NotFoundNewsletterException
     */
    public function actionCreateFromUsers()
    {
        $users = User::find()->all();
        foreach ($users as $user) {
            $newsletterRecipientByEmail = NewsletterRecipient::find()
                ->where(['email' => $user->email])
                ->one();

            $newsletterRecipientClass = 'taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient';
            $isNewNewsletterRecipient = !$newsletterRecipientByEmail || !$newsletterRecipientByEmail->id;
            if ($isNewNewsletterRecipient) {
                $newsletterRecipientByEmail = new $newsletterRecipientClass();
                $newsletterRecipientByEmail->is_verified = 0;
                $newsletterRecipientByEmail->is_subscribed = 0;
                $newsletterRecipientByEmail->token = $newsletterRecipientByEmail->generateToken();
            }
            $newsletterRecipientByEmail->email = $user->email;
            $newsletterRecipientByEmail->user_id = $user->id;
            $newsletterRecipientByEmail->save();
            
            $newsletterTopicClass = 'taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterTopic';
            if (!class_exists($newsletterTopicClass)) {
                continue;
            }
            $allNewsletterTopics = ArrayHelper::map($newsletterTopicClass::find()->all(), 'id', 'id');
            $newsletterRecipientByEmail->addAllNewsletterTopics($allNewsletterTopics, false);
        }
    }
}
