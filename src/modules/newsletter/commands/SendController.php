<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\commands;

use taktwerk\yiiboilerplate\modules\newsletter\exceptions\NotFoundNewsletterException;
use taktwerk\yiiboilerplate\modules\newsletter\helpers\NewsletterSender;
use taktwerk\yiiboilerplate\modules\newsletter\models\Newsletter;
use yii\console\Controller;
use Yii;
use yii\db\Expression;

class SendController extends Controller
{
    /**
     * @param $newsletterId
     * @throws NotFoundNewsletterException
     */
    public function actionOne($newsletterId)
    {
        $newsletterId = (int) $newsletterId;
        $newsletter = Newsletter::find()
            ->where(['id' => $newsletterId])
            ->one();

        if (!$newsletter) {
            throw(
                new NotFoundNewsletterException('There is no such newsletter')
            );
        }

        $newsletterSender = new NewsletterSender($newsletter);
        $newsletterSender->send();
    }

    /**
     *
     */
    public function actionAll()
    {
        $newsletters = Newsletter::find()->all();

        foreach ($newsletters as $newsletter) {
            $newsletterSender = new NewsletterSender($newsletter);
            $newsletterSender->send();
        }
    }
}
