<?php


namespace taktwerk\yiiboilerplate\modules\newsletter\commands;


use taktwerk\yiiboilerplate\modules\newsletter\helpers\NewsletterSender;
use taktwerk\yiiboilerplate\modules\queue\commands\BackgroundCommandInterface;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use taktwerk\yiiboilerplate\modules\newsletter\models\Newsletter;

class SendNewsletter implements BackgroundCommandInterface
{
    public function run(QueueJob $queue)
    {
        $params = json_decode($queue->parameters);
        $model = Newsletter::find()->where(['id' => $params->id])->one();
        $newsletterSender = new NewsletterSender($model);

        if (!$newsletterSender) {
            $queue->status = QueueJob::STATUS_FAILED;
            return false;
        }
        try {
            $newsletterSender->send();
        } catch (Exception $e) {
            $queue->status = QueueJob::STATUS_FAILED;
            $queue->error = $e->getMessage();
            throw $e;
        }
    }
}
