<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace taktwerk\yiiboilerplate\modules\newsletter\grid;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use taktwerk\yiiboilerplate\grid\ActionColumn as AC;
use yii\base\BaseObject;

/**
 * ActionColumn is a column for the [[GridView]] widget that displays buttons for viewing and manipulating the items.
 *
 * To add an ActionColumn to the gridview, add it to the [[GridView::columns|columns]] configuration as follows:
 *
 * ```php
 * 'columns' => [
 * // ...
 * [
 * 'class' => ActionColumn::className(),
 * // you may configure additional properties here
 * ],
 * ]
 * ```
 *
 * For more details and usage information on ActionColumn, see the [guide article on data widgets](guide:output-data-widgets).
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ActionColumn extends AC
{

    protected function initDefaultButton($name, $iconName, $additionalOptions = [])
    {
        $this->urlCreator = function ($action, $model, $key, $index) {
            /**
             *
             * @var \yii\db\ActiveRecord $model
             */
            // using the column name as key, not mapping to 'id' like the standard generator
            $params = is_array($key) ? $key : [
                'email' => $model->email
            ];
            $params[0] = Yii::$app->controller->id ? Yii::$app->controller->id . '/' . $action : $action;
            return Url::toRoute($params);
        };
        $this->visibleButtons['delete'] = function ($model, $key, $index) {
                return ($model->hasMethod('deletable')) ?
                    $model->deletable() && !empty($model->id) && empty($model->deleted_at) :
                    false;
            };
        if (! isset($this->buttons[$name]) && strpos($this->template, '{' . $name . '}') !== false) {
            $this->buttons[$name] = function ($url, $model, $key) use ($name, $iconName, $additionalOptions) {
                switch ($name) {
                    case 'view':
                        $title = Yii::t('yii', 'View');
                        if ($this->useModal) {
                            $this->buttonOptions = array_merge([
                                'data-toggle' => 'modal',
                                'data-url' => $url,
                                'data-pjax' => 1
                            ], $additionalOptions, $this->buttonOptions);
                            $url = '#' . $this->modalId;
                        }
                        break;
                    case 'update':
                        $title = (!empty($model->id) && empty($model->deleted_at)) ?
                            Yii::t('yii', 'Change subscribe') :
                            Yii::t('yii', 'Subscribe');
                        if ($this->useModal) {
                            $this->buttonOptions = array_merge([
                                'data-toggle' => 'modal',
                                'data-url' => $url,
                                'data-pjax' => 1
                            ], $additionalOptions, $this->buttonOptions);
                            $url = '#' . $this->modalId;
                        }
                        break;
                    case 'delete':
                        $title = Yii::t('yii', 'Unsubscribe');
                        if ($this->useModal) {
                            $this->buttonOptions = array_merge([
                                'data-url' => $url,
                                'data-pjax' => 1,
                                'class' => 'ajaxDelete'
                            ], $additionalOptions, $this->buttonOptions);
                        }
                        break;
                    default:
                        $title = ucfirst($name);
                }
                $options = array_merge([
                    'title' => $title,
                    'aria-label' => $title,
                    'data-pjax' => '0'
                ], $additionalOptions, $this->buttonOptions);
                $icon = Html::tag('span', '', [
                        'class' => "glyphicon glyphicon-$iconName"
                    ]) . ' ' . $title;
                return Html::a($icon, $url, $options);
            };
        }
    }
}
