<?php

return [
    'bootstrap' => [],
    'controllerMap' => [
        'send' => [
            'class' => 'taktwerk\yiiboilerplate\modules\newsletter\commands\SendController',
        ],
        'recipient' => [
            'class' => 'taktwerk\yiiboilerplate\modules\newsletter\commands\RecipientController',
        ],
    ],
    'components' => [
        'urlManager' => [
            'class' => 'taktwerk\yiiboilerplate\UrlManager',
            'baseUrl' => getenv('APP_BASE_URL'),
        ],
    ],
];
