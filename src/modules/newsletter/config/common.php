<?php

return [
    'fromEmail' => getenv('NEWSLETTER_FROM_EMAIL', 'hello@email.com'),
    'bootstrap' => [
        'queue',
    ],
];
