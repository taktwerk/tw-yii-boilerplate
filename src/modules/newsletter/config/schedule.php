<?php
/**
 * https://github.com/omnilight/yii2-scheduling
 *
 * Setup:
 * In crontab file put this:
 *   * * * * * php /path/to/yii yii schedule/run --scheduleFile=@taktwerk/yiiboilerplate/modules/newsletter/config/schedule.php 1>> /dev/null 2>&1
 *
 * Example of usage are bellow
 */

/**
 * @var \omnilight\scheduling\Schedule $schedule
 */

use taktwerk\yiiboilerplate\modules\newsletter\models\Newsletter;

Yii::info('Running scheduled nedwsletter jobs', 'taktwerk\debug');

$newsletters = Newsletter::find()
            ->where(['disabled' => 0])
            ->andWhere(['not', ['cron_value' => null]])
            ->all();

foreach ($newsletters as $newsletter) {
	if ($newsletter->cron_value) {
		$command = "newsletter/send/one {$newsletter->id}";
		$cronValue = (string) $newsletter->cron_value;
		try {
			$schedule
				->command($command)
				->cron($cronValue)
				->withoutOverlapping();
		} catch (\Exception $e) {
			continue;
		}
	}
}