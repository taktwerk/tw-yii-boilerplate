<?php

return [
  'sendPeriodInSeconds' => getenv('NEWSLETTER_SEND_PERIOD_TIME_IN_SECONDS', 0),
];
