<?php

return [
    'Newsletters' => 'Newsletters (de)',
    'Newsletter settings' => 'Newsletter settings (de)',
    'Newsletter recipients' => 'Newsletter recipients (de)',
    'Newsletter topics' => 'Newsletter topics (de)',
    'Topics' => 'Topics (de)',
    'Recipients' => 'Recipients (de)',
    'Name' => 'Name (de)',
    'Create' => 'Create (de)',
    'Create topic' => 'Create topic (de)',
    'Save' => 'Save (de)',
    'The requested page does not exist.' => 'The requested page does not exist. (de)',
    'Unable to create new newsletter topic.' => 'Unable to create new newsletter topic. (de)',
    'Newsletter topic has been removed.' => 'Newsletter topic has been removed. (de)',
    'Update newsletter topic' => 'Update newsletter topic (de)',
    'Subscribe to newsletters' => 'Subscribe to newsletters (de)',
    'Confirm your newsletter subscription.' => 'Confirm your newsletter subscription (de).',
    'There is no such newsletter topic' => 'There is no such newsletter topic (de)',
    'Email is a required field' => 'Email is a required field (de)'
];
