<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\dto;

class NewsletterRecipientRegistrationDto
{
    protected $isMakeVerified;
    protected $isMakeSubscribed;
    protected $sendVerifyEmail;
    protected $registrationData;
    protected $checkEmailUnique;

    public function __construct()
    {
        $this->isMakeVerified = false;
        $this->isMakeSubscribed = false;
        $this->sendVerifyEmail = false;
        $this->checkEmailUnique = false;
        $this->registrationData = [];
    }

    /**
     * @return bool
     */
    public function isMakeVerified()
    {
        return $this->isMakeVerified;
    }

    /**
     * @param bool $isMakeVerified
     */
    public function setIsMakeVerified($isMakeVerified)
    {
        $this->isMakeVerified = $isMakeVerified;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSendVerifyEmail()
    {
        return $this->sendVerifyEmail;
    }

    /**
     * @param bool $sendVerifyEmail
     */
    public function setSendVerifyEmail($sendVerifyEmail)
    {
        $this->sendVerifyEmail = $sendVerifyEmail;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMakeSubscribed()
    {
        return $this->isMakeSubscribed;
    }

    /**
     * @param bool $isMakeSubscribed
     */
    public function setIsMakeSubscribed($isMakeSubscribed)
    {
        $this->isMakeSubscribed = $isMakeSubscribed;

        return $this;
    }

    /**
     * @return array
     */
    public function getRegistrationData()
    {
        return $this->registrationData;
    }

    /**
     * @param array $registrationData
     */
    public function setRegistrationData($registrationData)
    {
        $this->registrationData = $registrationData;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCheckEmailUnique(): bool
    {
        return $this->checkEmailUnique;
    }

    /**
     * @param bool $checkEmailUnique
     */
    public function setCheckEmailUnique(bool $checkEmailUnique)
    {
        $this->checkEmailUnique = $checkEmailUnique;

        return $this;
    }
}
