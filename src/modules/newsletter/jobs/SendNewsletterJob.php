<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\jobs;

use yii\base\BaseObject;

class SendNewsletterJob extends BaseObject implements \yii\queue\JobInterface
{
    public $newsletterSender;

    public function execute($queue)
    {
        $this->newsletterSender->send();
    }
}
