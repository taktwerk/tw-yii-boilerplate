<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\jobs;

use taktwerk\yiiboilerplate\modules\newsletter\helpers\NewsletterSender;
use taktwerk\yiiboilerplate\modules\newsletter\models\Newsletter;
use yii\base\BaseObject;
use Yii;
use yii\db\Expression;

class SendNewslettersJob extends BaseObject implements \yii\queue\JobInterface
{
    public function execute($queue)
    {
        $interval = Yii::$app->getModule('newsletter')->sendPeriodInSeconds;

        $newsletters = Newsletter::find()
            ->all();

        foreach ($newsletters as $newsletter) {
            $newsletterSender = new NewsletterSender($newsletter);
            Yii::$app->getModule('newsletter')
                ->queue
                ->push(
                    new SendNewsletterJob(
                        ['newsletterSender' => $newsletterSender]
                    )
                );
        }
    }
}
