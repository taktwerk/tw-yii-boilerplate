<?php
/**
 * Created by PhpStorm.
 * User: ben-g
 * Date: 18.01.2016
 * Time: 14:42
 */

namespace taktwerk\yiiboilerplate\modules\newsletter\behaviors;


use yii\behaviors\TimestampBehavior;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\ActiveQuery;
use taktwerk\yiiboilerplate\behaviors\SoftDelete as BaseSoftDelete;

class SoftDelete extends BaseSoftDelete
{
    /**
     * Remove (aka soft-delete) record
     */
    public function remove()
    {
        parent::remove();

        $this->owner->is_subscribed = false;
        $this->owner->is_verified = false;

        $this->owner->save(false, ['is_subscribed', 'is_verified']);
    }
}
