<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\enum;

class GenderTypeEnum
{
    const MALE_TYPE = 'MALE';
    const FEMALE_TYPE = 'FEMALE';

    public function toArray()
    {
        return [
            self::MALE_TYPE,
            self::FEMALE_TYPE,
        ];
    }

    public function toString()
    {
        return implode(',', $this->toArray());
    }
}
