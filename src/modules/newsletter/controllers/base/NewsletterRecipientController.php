<?php
//Generation Date: 11-Sep-2020 02:54:32pm
namespace taktwerk\yiiboilerplate\modules\newsletter\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * NewsletterRecipientController implements the CRUD actions for NewsletterRecipient model.
 */
class NewsletterRecipientController extends TwCrudController
{
}
