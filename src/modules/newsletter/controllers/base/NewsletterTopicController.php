<?php
//Generation Date: 11-Sep-2020 02:54:34pm
namespace taktwerk\yiiboilerplate\modules\newsletter\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * NewsletterTopicController implements the CRUD actions for NewsletterTopic model.
 */
class NewsletterTopicController extends TwCrudController
{
}
