<?php
//Generation Date: 11-Sep-2020 02:54:33pm
namespace taktwerk\yiiboilerplate\modules\newsletter\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * NewsletterRecipientTopicController implements the CRUD actions for NewsletterRecipientTopic model.
 */
class NewsletterRecipientTopicController extends TwCrudController
{
}
