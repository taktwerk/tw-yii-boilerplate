<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\controllers;

use taktwerk\yiiboilerplate\modules\newsletter\components\Hook;
use taktwerk\yiiboilerplate\modules\newsletter\models\form\UpdateNewsletterRecipientSettingsForm;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterTopic;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;

class UserSettingsController extends Controller
{
    public $layout = 'user-settings';
    public $indexView = 'view';

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        $newsletterModule = Yii::$app->getModule('newsletter');
        $userSettings = $newsletterModule->userSettings;
        if ($userSettings) {
            $this->layout = !empty($userSettings['layout']) ?
                $userSettings['layout'] :
                'user-settings';
            $this->indexView = !empty($userSettings['indexView']) ?
                $userSettings['indexView'] :
                'view';
        }

        Event::on(
            Hook::class,
            Hook::MULTIPLE_TOPIC_LIST_SELECT,
            [
                Hook::class,
                'show',
            ]
        );
        Event::on(
            Hook::class,
            Hook::MULTIPLE_TOPIC_LIST_SAVE,
            [
                Hook::class,
                'save',
            ]
        );
    }

    public function actionIndex($token)
    {
        $formModel = new UpdateNewsletterRecipientSettingsForm();

        $newsletterRecipientModel = NewsletterRecipient::find()
            ->with('newsletterTopics')
            ->where(['token' => $token, 'is_verified' => 1])
            ->one();

        if (!$newsletterRecipientModel) {
            throw new NotFoundHttpException(
                Yii::t('twbp', 'The requested page does not exist.')
            );
        }

        $newsletterRecipientTopicsArray = ArrayHelper::toArray($newsletterRecipientModel->newsletterTopics);

        $newsletterTopicsList = ArrayHelper::map(
            NewsletterTopic::find()->select(['id', 'name'])->asArray()->all(),
            'id',
            'name'
        );

        $formModel->attributes = $newsletterRecipientModel->getAttributes();
        $formModel->topic_ids = ArrayHelper::getColumn($newsletterRecipientTopicsArray, 'id');
        if (!Yii::$app->request->post()) {
            return $this->render(
                $this->indexView,
                ['formModel' => $formModel, 'newsletterTopicsList' => $newsletterTopicsList]
            );
        }

        $postData = Yii::$app->request->post();
        $formModel->load($postData);
        if (!$formModel->validate()) {
            return $this->render(
                $this->indexView,
                ['formModel' => $formModel, 'newsletterTopicsList' => $newsletterTopicsList]
            );
        }

        $isChangedUserSettings = $formModel->changeUserSettings($newsletterRecipientModel);
        if (!$isChangedUserSettings) {
            Yii::$app->getSession()->setFlash(
                'danger',
                Yii::t('twbp', 'Something went wrong during editing your settings.')
            );
        } else {
            Yii::$app->getSession()->setFlash(
                'success',
                Yii::t('twbp', 'Your settings was changed.')
            );
        }

        return $this->render(
            $this->indexView,
            ['formModel' => $formModel, 'newsletterTopicsList' => $newsletterTopicsList]
        );
    }

    public function actionUnsubscribe($token)
    {
        $formModel = new UpdateNewsletterRecipientSettingsForm();

        $newsletterRecipientModel = NewsletterRecipient::find()
            ->with('newsletterTopics')
            ->where(['token' => $token, 'is_verified' => 1])
            ->one();

        if (!$newsletterRecipientModel) {
            throw new NotFoundHttpException(
                Yii::t('twbp', 'The requested page does not exist.')
            );
        }

        $newsletterRecipientModel->is_subscribed = 0;
        $newsletterRecipientModel->save();
        $newsletterRecipientModel->removeAllNewsletterTopics();

        Yii::$app->getSession()->setFlash('success', Yii::t('twbp', 'You have unsubscribed.'));

        $formModel->attributes = $newsletterRecipientModel->getAttributes();

        return $this->redirect(['index', 'token' => $formModel->token]);
    }

    public function actionSubscribe($token)
    {
        $formModel = new UpdateNewsletterRecipientSettingsForm();

        $newsletterRecipient = NewsletterRecipient::find()
            ->where(['token' => $token, 'is_verified' => 1])
            ->one();

        if (!$newsletterRecipient) {
            return $this->redirect(['user-settings/' . $token]);
        }

        $newsletterRecipient->is_subscribed = 1;
        $newsletterRecipient->save();
        $allNewsletterTopics = ArrayHelper::map(NewsletterTopic::find()->where(['is_default' => 1])->all(), 'id', 'id');
        $newsletterRecipient->addAllNewsletterTopics($allNewsletterTopics, false);
        $newsletterRecipientTopicsArray = ArrayHelper::toArray($newsletterRecipient->newsletterTopics);
        $formModel->attributes = $newsletterRecipient->getAttributes();
        $formModel->topic_ids = ArrayHelper::getColumn($newsletterRecipientTopicsArray, 'id');

        Yii::$app->getSession()->setFlash(
            'success',
            Yii::t('twbp', 'You have subscribed.')
        );

        return $this->redirect(['index', 'token' => $formModel->token]);
    }
}
