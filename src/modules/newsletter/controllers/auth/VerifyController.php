<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\controllers\auth;

use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterTopic;
use yii\web\Controller;
use Yii;

class VerifyController extends Controller
{
    public function actionIndex($token)
    {
        $newsletterRecipient = NewsletterRecipient::find()
            ->where(['token' => $token, 'is_verified' => 0])
            ->one();
        if (!$newsletterRecipient) {
            return $this->redirect(['user-settings/' . $token]);
        }
        if($newsletterRecipient){
            if(\Yii::$app->request->isPost && $newsletterRecipient->is_verified==0){
                $newsletterRecipient->is_verified = 1;
                $newsletterRecipient->is_subscribed = 1;
                $isVerified = $newsletterRecipient->save();
                if ($isVerified) {
                    Yii::$app->getSession()->setFlash(
                        'success',
                        Yii::t('twbp', 'Your account is verified.')
                    );
                }
            }else if($newsletterRecipient->is_verified==0){
               return $this->render('index');
            }
        }
        return $this->redirect(['user-settings/' . $token]);
    }
}
