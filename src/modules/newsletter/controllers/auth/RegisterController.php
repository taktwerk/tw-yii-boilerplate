<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\controllers\auth;

use taktwerk\yiiboilerplate\modules\newsletter\components\NewsletterRecipientRegistrationComponent;
use taktwerk\yiiboilerplate\modules\newsletter\dto\NewsletterRecipientRegistrationDto;
use taktwerk\yiiboilerplate\modules\newsletter\exceptions\NewsletterRecipientNotRegisterException;
use taktwerk\yiiboilerplate\modules\newsletter\exceptions\NewsletterRecipientRegistrationValidationException;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient;
use taktwerk\yiiboilerplate\modules\newsletter\models\form\RegisterNewsletterRecipientForm;
use Da\User\Validator\AjaxRequestModelValidator;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterTopic;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use Yii;
use yii\helpers\Url;
use \yii\web\Response;

class RegisterController extends Controller
{
    public function actionIndex()
    {
        $registerData = Yii::$app->request->post();

        if (!Yii::$app->request->isAjax || !$registerData) {
            return $this->render(
                'index',
                ['model' => Yii::$container->get(RegisterNewsletterRecipientForm::class)]
            );
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $newsletterRecipientRegistrationDto = Yii::$container->get(
            NewsletterRecipientRegistrationDto::class
        );
        $registerData['language_id'] = 'de';
        $newsletterRecipientRegistrationDto->setIsMakeSubscribed(false)
            ->setIsMakeVerified(false)
            ->setSendVerifyEmail(true)
            ->setCheckEmailUnique(true)
            ->setRegistrationData($registerData);
        $newsletterRecipientRegistrationComponent = new NewsletterRecipientRegistrationComponent(
            $newsletterRecipientRegistrationDto
        );
//        Yii::$container->get(
//            AjaxRequestModelValidator::class,
//            [$newsletterRecipientRegistrationComponent->getRegisterNewsletterRecipientFormModel()]
//        )
//            ->validate();
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $newsletterRecipientRegistrationComponent->register();

            $transaction->commit();

            if (!$newsletterRecipientRegistrationDto->isSendVerifyEmail()) {
                Yii::$app->getResponse()->setStatusCode(400);
                return ['error' => 'Email was not sent'];
            }
        } catch (NewsletterRecipientRegistrationValidationException $exception) {
            $transaction->rollBack();
            Yii::$app->getResponse()->setStatusCode(400);
            return ['error' => $exception->getMessage()];
            ///
        } catch (NewsletterRecipientNotRegisterException $exception) {
            $transaction->rollBack();
            Yii::$app->getResponse()->setStatusCode(400);
            return ['error' => $exception->getMessage()];
        }

        Yii::$app->getResponse()->setStatusCode(200);

        return ['success' => true];
    }
}
