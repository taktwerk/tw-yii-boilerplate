<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\controllers;

use taktwerk\yiiboilerplate\grid\GridView;
use taktwerk\yiiboilerplate\modules\newsletter\components\NewsletterRecipientRegistrationComponent;
use taktwerk\yiiboilerplate\modules\newsletter\dto\NewsletterRecipientRegistrationDto;
use taktwerk\yiiboilerplate\modules\newsletter\exceptions\NewsletterRecipientNotRegisterException;
use taktwerk\yiiboilerplate\modules\newsletter\exceptions\NewsletterRecipientRegistrationValidationException;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterTopic;
use taktwerk\yiiboilerplate\widget\Select2;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;

/**
 * This is the class for controller "NewsletterRecipientController".
 */
class NewsletterRecipientController extends \taktwerk\yiiboilerplate\modules\newsletter\controllers\base\NewsletterRecipientController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\newsletter\models\search\NewsletterRecipient';

    public $languageModel = '\app\models\Language';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access']['rules'][0]['actions'][] = 'send-verify-email';
        $behaviors['access']['rules'][0]['actions'][] = 'subscribe';
        $behaviors['access']['rules'][0]['actions'][] = 'unsubscribe';
        $behaviors['access']['rules'][0]['actions'][] = 'subscribe-multiple';
        $behaviors['access']['rules'][0]['actions'][] = 'unsubscribe-multiple';

        return $behaviors;
    }

    public function init()
    {
        $this->gridDropdownActions = [
            [
                'url' => [
                    false
                ],
                'options' => [
                    'onclick' => 'deleteMultiple(this);',
                    'data-url' => Url::toRoute('/newsletter/newsletter-recipient/subscribe-multiple'),
                ],
                'label' => '<i class="fa fa-envelope" aria-hidden="true"></i>' . Yii::t('twbp', 'Subscribe'),
            ],
            [
                'url' => [
                    false
                ],
                'options' => [
                    'onclick' => 'deleteMultiple(this);',
                    'data-url' => Url::toRoute('/newsletter/newsletter-recipient/unsubscribe-multiple'),
                ],
                'label' => '<i class="fa fa-envelope" style="color:red;" aria-hidden="true"></i>' . Yii::t('twbp', 'Unsubscribe'),
            ],
        ];
        $this->customActions = [
            'subscribe' => function ($url, $model, $key) {
                if ($model->is_subscribed) {
                    return false;
                }

                return Html::a('<span class="glyphicon glyphicon-envelope"></span> ' . Yii::t('twbp', 'Subscribe'),
                    $url,
                    [
                        'title' => Yii::t('twbp', 'Subscribe'),
                        'url' => $url,
                        'data' => [
                            'pjax' => 0,
                            'method' => 'post',
                        ]
                    ]);
            },
            'unsubscribe' => function ($url, $model, $key) {
                if (!$model->is_subscribed) {
                    return false;
                }

                return Html::a('<span class="glyphicon glyphicon-envelope" style="color: red;"></span> ' . Yii::t('twbp', 'Unsubscribe'),
                    $url,
                    [
                        'title' => Yii::t('twbp', 'Unsubscribe'),
                        'url' => $url,
                        'data' => [
                            'pjax' => 0,
                            'method' => 'post',
                        ]
                    ]);
            },
        ];
        $this->crudColumnsOverwrite = [
            'view' => [
                'after#is_subscribed' => [
                    'attribute' => 'user_id',
                    'label' => \Yii::t('twbp', 'Is User'),
                    'value' => function ($model) {
                        return $model->getAttribute('user_id') ? Yii::t('twbp', 'Yes') : null;
                    },
                ]
            ],
            'index' => [
                'user_id' => [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'user_id',
                    'format' => 'html',
                    'content' => function ($model) {
                        if ($model->user) {
                            if (Yii::$app->getUser()->can('app_user_view') && $model->user->readable()) {
                                return Html::a(
                                    $model->user->toString,
                                    ['/usermanager/user/view', 'id' => $model->user_id],
                                    ['data-pjax' => 0]
                                );
                            } else {
                                return $model->user->toString;
                            }
                        }
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'data' => \taktwerk\yiiboilerplate\models\User::find()->count() > 50 ?
                            null :
                            \yii\helpers\ArrayHelper::map(
                                \taktwerk\yiiboilerplate\models\User::find()->all(),
                                'id',
                                'toString'
                        ),
                        'initValueText' => \taktwerk\yiiboilerplate\models\User::find()->count() > 50 ?
                            \yii\helpers\ArrayHelper::map(
                                \taktwerk\yiiboilerplate\models\User::find()
                                    ->andWhere(['id' => $searchModel->user_id])
                                    ->all(),
                                    'id',
                                    'toString'
                                ) :
                            '',
                        'options' => [
                            'placeholder' => '',
                            'multiple' => true,
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            (\taktwerk\yiiboilerplate\models\User::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                            (\taktwerk\yiiboilerplate\models\User::find()->count() > 50 ? 'ajax' : '') => [
                                'url' => \yii\helpers\Url::to(['/newsletter/newsletter-recipient/list']),
                                'dataType' => 'json',
                                'data' => new \yii\web\JsExpression('function(params) {
                                    return {
                                        q:params.term, m: \'User\'
                                    };
                                }')
                            ],
                        ]
                    ],
                ],
                'after#is_subscribed' => [
                    'attribute' => 'user_id',
                    'label' => \Yii::t('twbp', 'Is User'),
                    'class' => '\kartik\grid\BooleanColumn',
                    'trueLabel' => \Yii::t('twbp', 'Yes'),
                    'falseLabel' => \Yii::t('twbp', 'No'),
                    'content' => function ($model) {
                        return $model->getAttribute('user_id') ? Yii::t('twbp', 'Yes') : null;
                    },
                ],
                'after#gender' => [
                    'attribute' => 'selectedNewsletterTopics',
                    'value' => function ($model) {
                        return $model->id;
                    },
                    'content' => function ($model) {
                        $topics = [];
                        foreach ($model->newsletterTopics as $newsletterTopic) {
                            $topics[] = $newsletterTopic->name;
                        }

                        return implode(', ', $topics);
                    },
                    'label' => \Yii::t('twbp', 'Topics'),
                    'filter' => ArrayHelper::map(NewsletterTopic::find()->asArray()->all(), 'id', 'name'),
                    'filterType' => GridView::FILTER_SELECT2,
                    'class' => '\kartik\grid\DataColumn',
                    'filterWidgetOptions' => [
                        'options' => [
                            'placeholder' => '',
                            'multiple' => true,
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ],
                ]
            ],
            'update' => [

            ],
        ];
        foreach( $this->module->newsletterRecipientDisabledAttributes as $atr){
            $this->crudColumnsOverwrite['crud'][$atr] = false;
        }
    }

    public function formFieldsOverwrite($model, $form)
    {
        $newsletterRecipientTopics = ArrayHelper::getColumn(ArrayHelper::toArray($model->newsletterTopics), 'id');
        if (empty($newsletterRecipientTopics)) {
            $newsletterRecipientTopics = NewsletterTopic::find()->where(['is_default' => 1])->all();
        }
        $model->selectedNewsletterTopics = $newsletterRecipientTopics;
        $owner = $relatedForm ? '_' . $owner . '_related' : '';
        $this->formFieldsOverwrite['email'] = $form->field(
            $model,
            'email',
            [
                'selectors' => [
                    'input' => '#' .
                        Html::getInputId($model, 'email') . $owner
                ]
            ]
        )
            ->textInput(
                [
                    'type' => 'email',
                    'id' => Html::getInputId($model, 'email') . $owner,
                    'disabled' => !empty($model->user_id),
                ]
            )
            ->hint($model->getAttributeHint('email'));
            $newsletterTopicsIdsNames = NewsletterTopic::toIdName();
        if(count($newsletterTopicsIdsNames)>1){
            $this->formFieldsOverwrite['after#is_subscribed'] = $form->field($model, 'selectedNewsletterTopics')
                ->widget(
                    Select2::class, [
                        'value' => $newsletterRecipientTopics,
                        'data' => $newsletterTopicsIdsNames,
                        'options' => ['multiple' => true, 'placeholder' => \Yii::t('twbp','Select topics...')]
                    ]
                );
        }else{
            $model->selectedNewsletterTopics = [array_key_first($newsletterTopicsIdsNames)];
        }
        $this->formFieldsOverwrite['user_id'] = $form->field($model, 'user_id')
            ->hiddenInput()
            ->label(false);
        $languageModelClass = $this->languageModel;
        if (class_exists($languageModelClass)) {
            $this->formFieldsOverwrite['language_id'] = $form->field($model, 'language_id')
                ->widget(
                    Select2::class,
                    [
                        'data' => $languageModelClass::find()->count() > 50 ? null : ArrayHelper::map($languageModelClass::find()->all(), 'language_id', 'toString'),
                        'initValueText' => $languageModelClass::find()->count() > 50 ? \yii\helpers\ArrayHelper::map($languageModelClass::find()->andWhere(['language_id' => $model->language_id])->all(), 'language_id', 'toString') : '',
                        'options' => [
                            'placeholder' => Yii::t('twbp', 'Select a value...'),
                            'id' => 'language_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                        ],
                        'pluginOptions' => [
                            'allowClear' => false,
                            ($languageModelClass::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                            ($languageModelClass::find()->count() > 50 ? 'ajax' : '') => [
                                'url' => \yii\helpers\Url::to(['list']),
                                'dataType' => 'json',
                                'data' => new \yii\web\JsExpression('function(params) {
                                    return {
                                        q:params.term, m: \'Language\'
                                    };
                                }')
                            ],
                        ],
                        'pluginEvents' => [
                            "change" => "function() {
                                if (($(this).val() != null) && ($(this).val() != '')) {
                                    // Enable edit icon
                                    $($(this).next().next().children('button')[0]).prop('disabled', false);
                                    $.post('" .
                                    Url::toRoute('/translatemanager/language/entry-details?id=', true) .
                                    "' + $(this).val(), {
                                        dataType: 'json'
                                    })
                                    .done(function(json) {
                                        var json = $.parseJSON(json);
                                        if (json.data != '') {
                                            $('#language_id_well').html(json.data);
                                            //$('#language_id_well').show();
                                        }
                                    })
                                } else {
                                    // Disable edit icon and remove sub-form
                                    $($(this).next().next().children('button')[0]).prop('disabled', true);
                                }
                            }",
                        ],
                        'addon' => (Yii::$app->getUser()->can('app_language_create') && (!$relatedForm || ($relatedType != RelatedForms::TYPE_MODAL && !$useModal))) ? [
                            'append' => [
                                'content' => [
                                    RelatedForms::widget([
                                        'relatedController' => '/translatemanager/language',
                                        'type' => $relatedTypeForm,
                                        'selector' => 'language_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                        'primaryKey' => 'language_id',
                                    ]),
                                ],
                                'asButton' => true
                            ],
                        ] : []
                    ]
                );
        }
        foreach( $this->module->newsletterRecipientDisabledAttributes as $atr){
            $this->formFieldsOverwrite[$atr] = false;
        }
        return $this->formFieldsOverwrite;
    }

    public function actionSubscribe($id)
    {
        $model = $this->findModel($id);
        if (!$model->is_subscribed) {
            $this->subscribe($model);
        }

        if (Yii::$app->request->isAjax) {
            return $this->actionIndex();
        }

        // TODO: improve detection
        $isPivot = strstr('$email', ',');
        if (isset(Yii::$app->session['__crudReturnUrl']) && Yii::$app->session['__crudReturnUrl'] != '/') {
            Url::remember(null);
            $url = Yii::$app->session['__crudReturnUrl'];
            Yii::$app->session['__crudReturnUrl'] = null;

            // Don't redirect to the view of the model that was just deleted
            if (strpos($url, '/backend') !== false || strpos($url, 'view/?email=' . $email) !== false) {
                return $this->redirect([
                    'index'
                ]);
            }

            return $this->redirect($url);
        } else {
            return $this->redirect([
                'index'
            ]);
        }
    }

    public function subscribe($model)
    {
        try {
            if (empty($model->id)) {
                $model->setIsNewRecord(true);
            } else {
                $model->deleted_at = null;
                $model->deleted_by = null;
            }
            if ($model->id) {
                $model->is_subscribed = 1;
                $model->is_verified = 1;
                $model->selectedNewsletterTopics = ArrayHelper::map(NewsletterTopic::find()
                    ->where(['is_default' => 1])
                    ->all(), 'id', 'id');
                $model->save();
            } else {
                $newsletterRecipientRegistrationDto = Yii::$container->get(
                    NewsletterRecipientRegistrationDto::class
                );
                $isSendVerifyEmail = false;
                $newsletterRecipientRegistrationDto->setIsMakeSubscribed(true)
                    ->setIsMakeVerified(true)
                    ->setSendVerifyEmail($isSendVerifyEmail)
                    ->setCheckEmailUnique(true)
                    ->setRegistrationData(['email' => $model->email]);
                $newsletterRecipientRegistrationComponent = new NewsletterRecipientRegistrationComponent(
                    $newsletterRecipientRegistrationDto
                );

                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $newsletterRecipientRegistrationComponent->register();
                    $transaction->commit();
                } catch (NewsletterRecipientRegistrationValidationException $e) {
                    $transaction->rollBack();
                    $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
                    Yii::$app->getSession()->setFlash('error', $msg);
                    return $this->redirect(Url::previous());
                } catch (NewsletterRecipientNotRegisterException $e) {
                    $transaction->rollBack();
                    $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
                    Yii::$app->getSession()->setFlash('error', $msg);
                    return $this->redirect(Url::previous());
                }
            }
        } catch (Exception $e) {
            $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
            Yii::$app->getSession()->setFlash('error', $msg);
            return $this->redirect(Url::previous());
        }
    }

    public function actionUnsubscribe($id)
    {
        try {
            $model = $this->findModel($id);
            if ($model) {
                $model->is_subscribed = 0;
                $model->is_verified = 0;
                $model->save();
                $model->removeAllNewsletterTopics();
            } else {
                throw new HttpException(404, Yii::t('twbp', 'The requested page does not exist.'));
            }
        } catch (Exception $e) {
            $msg = (isset($e->errorInfo[2])) ? $e->errorInfo[2] : $e->getMessage();
            Yii::$app->getSession()->setFlash('error', $msg);
            return $this->redirect(Url::previous());
        }

        if (Yii::$app->request->isAjax) {
            return $this->actionIndex();
        }

        // TODO: improve detection
        $isPivot = strstr('$email', ',');
        if (isset(Yii::$app->session['__crudReturnUrl']) && Yii::$app->session['__crudReturnUrl'] != '/') {
            Url::remember(null);
            $url = Yii::$app->session['__crudReturnUrl'];
            Yii::$app->session['__crudReturnUrl'] = null;

            // Don't redirect to the view of the model that was just deleted
            if (strpos($url, '/backend') !== false || strpos($url, 'view/?email=' . $email) !== false) {
                return $this->redirect([
                    'index'
                ]);
            }

            return $this->redirect($url);
        } else {
            return $this->redirect([
                'index'
            ]);
        }
    }

    /**
     *
     * @return mixed
     * @throws HttpException
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function actionSubscribeMultiple()
    {
        $pk = Yii::$app->request->post('pk'); // Array or selected records primary keys
        // Preventing extra unnecessary query
        if (!empty($pk)) {
            foreach ($pk as $id) {
                $model = $this->findModel($id);
                if (!$model->is_subscribed) {
                    $this->subscribe($model);
                }
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->actionIndex();
        }
    }

    public function actionUnsubscribeMultiple()
    {
        $pk = Yii::$app->request->post('pk'); // Array or selected records primary keys
        // Preventing extra unnecessary query
        if (!empty($pk)) {
            foreach ($pk as $id) {
                $model = $this->findModel($id);
                if ($model->is_subscribed) {
                    $model->is_subscribed = 0;
                    $model->is_verified = 0;
                    $model->save();
                    $model->removeAllNewsletterTopics();
                }
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->actionIndex();
        }
    }
}
