<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\controllers;

use taktwerk\yiiboilerplate\commands\VideoCompressor;
use taktwerk\yiiboilerplate\modules\newsletter\commands\SendNewsletter;
use taktwerk\yiiboilerplate\modules\newsletter\helpers\NewsletterSender;
use taktwerk\yiiboilerplate\modules\newsletter\jobs\SendNewsletterJob;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\filters\AccessControl;

/**
 * This is the class for controller "NewsletterController".
 */
class NewsletterController extends \taktwerk\yiiboilerplate\modules\newsletter\controllers\base\NewsletterController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\newsletter\models\Newsletter';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\newsletter\models\search\Newsletter';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'send'
                        ],
                        'roles' => ['@']
                    ]
                ]
            ]
        ]);
    }

    public function init()
    {
        $this->customActions = [
            'send' => function ($url, $model, $key) {
            $sendPerm = Yii::$app->getUser()->can(Yii::$app->controller->module->id .'_' . Yii::$app->controller->id . '_send');
            if($sendPerm){
                    $countOfRecipients = $model->getCountOfRecipients();
    
                    $confirmPopupElement = "<div>Subscribed users count: {$countOfRecipients}</div>";
                    $confirmPopupElement .= '<div>Are you sure you want to send this newsletter?</div>';
    
                    return Html::a('<span class="glyphicon glyphicon-send"></span> ' . Yii::t('twbp', 'Send'),
                        $url,
                        [
                            'title' => Yii::t('twbp', 'Send'),
                            'url' => $url,
                            'data' => [
                                'pjax' => 0,
                                'confirm' => $confirmPopupElement,
                                'method' => 'post',
                            ]
                        ]);
                }
            },
        ];
    }

    public function actionSend($id)
    {
        $model = $this->findModel($id);

        $job = new QueueJob();
        $job->queue(
            'Send newsletter ' . $model->id,
            SendNewsletter::class,
            ['id' => $model->id],
            true
        );

        Yii::$app->getSession()->setFlash(
            'success',
            Yii::t('twbp', 'The newsletter has been sent.')
        );

        return $this->redirect(['index']);
    }

    public function redirect($url, $statusCode = 302)
    {
        return Yii::$app->getResponse()->redirect(Url::to($url), $statusCode);
    }
}
