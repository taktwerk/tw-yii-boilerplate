`Module newsletter:`
- /src/modules/newsletter/

`Commands:`
- newsletter/send/all
- newsletter/send/one
- newsletter/recipient/create-from-users (Command for generating newsletter recipient from user information)

`Command to listen queue:`
- php yii queue/listen

Register newsletter recipient link:
{URI}/newsletter/auth/register

`Add this variables to .env file:`
- NEWSLETTER_SEND_PERIOD_TIME_IN_SECONDS={YOUR PERIOD IN SECONDS}
- DOMAIN_NAME={YOUR URL}
- NEWSLETTER_FROM_EMAIL={YOUR FROM URL FOR NEWSLETTER}
