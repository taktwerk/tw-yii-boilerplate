<?php

use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="table-responsive">
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{pager}",
            'columns' => [
                [
                    'attribute' => 'name',
                    'header' => Yii::t('twbp', 'Name'),
                    'options' => [
                        'style' => 'width: 20%',
                    ],
                ],
                [
                    'attribute' => 'topic_id',
                    'header' => Yii::t('twbp', 'Topic'),
                    'filter' => Html::activeDropDownList(
                            $searchModel,
                            'topic_id',
                            ArrayHelper::map(
                                    \taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterTopic::find()
                                        ->asArray()
                                        ->all(),
                                    'id',
                                    'name'
                            ),
                            ['class'=>'form-control','prompt' => 'Select Topic']
                    ),
                    'value' => function ($model) {
                        return $model->newsletterTopic ? $model->newsletterTopic->name : null;
                    },
                    'options' => [
                        'style' => 'width: 20%',
                    ],
                ],
                [
                    'attribute' => 'template',
                    'header' => Yii::t('twbp', 'Template'),
                    'options' => [
                        'style' => 'width: 20%',
                    ],
                ],
                [
                    'class' => ActionColumn::class,
                    'template' => '{update} {delete} {send}',
                    'urlCreator' => function ($action, $model) {
                        return Url::to(['/newsletter/main/' . $action, 'id' => $model['id']]);
                    },
                    'options' => [
                        'style' => 'width: 5%',
                    ],
                    'buttons' => [
                        'send' => function ($url) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-send"></span>',
                                $url,
                                [
                                    'title' => 'Send',
                                    'data-pjax' => '0',
                                ]
                            );
                        }
                    ],
                ],
            ],
        ]
    ) ?>
</div>
