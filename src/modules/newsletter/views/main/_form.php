<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<?php $form = ActiveForm::begin(
    [
        'enableClientValidation' => false,
        'enableAjaxValidation' => false,
    ]
) ?>

<?= $form->field($model, 'name') ?>

<?= $form->field($model, 'template') ?>

<?= $form->field($model, 'topic_id')->dropDownList($newsletterTopicsList) ?>

<?= Html::submitButton(Yii::t('twbp', 'Save'), ['class' => 'btn btn-success btn-block']) ?>

<?php ActiveForm::end() ?>
