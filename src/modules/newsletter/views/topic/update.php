<?php

$this->title = Yii::t('twbp', 'Update newsletter topic');
$this->params['breadcrumbs'][] = $this->title;

$this->beginContent('@taktwerk-boilerplate/modules/newsletter/views/_container.php');
?>

<?= $this->render('/_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>

<?php $this->endContent(); ?>
