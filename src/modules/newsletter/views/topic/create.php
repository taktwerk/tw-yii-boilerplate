<?php

$this->title = Yii::t('twbp', 'Create newsletter topic');
$this->params['breadcrumbs'][] = $this->title;

$this->beginContent('@taktwerk/yiiboilerplate/modules/newsletter/views/_container.php');
?>

<?= $this->render('/_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>

<?php $this->endContent(); ?>
