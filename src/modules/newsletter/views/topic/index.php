<?php
use yii\helpers\Html;

$this->title = Yii::t('twbp', 'Newsletter Topics');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('@Da/User/resources/views/shared/_alert', ['module' => Yii::$app->getModule('newsletter')]) ?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->render('/_menu') ?>
                <?= $this->render('table', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]) ?>
            </div>
        </div>
    </div>
</div>
