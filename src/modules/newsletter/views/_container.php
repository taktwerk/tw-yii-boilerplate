<?php $this->beginContent('@taktwerk-boilerplate/modules/newsletter/views/_row.php'); ?>

<div class="container-layout">
    <div class="container">
        <?=
        \yii\widgets\Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        ) ?>
    </div>

    <div class="container">
        <?=$content?>
    </div>
</div>

<?php $this->endContent(); ?>
