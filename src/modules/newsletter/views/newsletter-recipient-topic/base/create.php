<?php
//Generation Date: 11-Sep-2020 02:54:33pm
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipientTopic $model
 * @var string $relatedTypeForm
 */

$this->title = Yii::t('twbp', 'Newsletter Recipient Topic') . ', ' . Yii::t('twbp', 'Create');
if(!$fromRelation) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('twbp', 'Newsletter Recipient Topics'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = Yii::t('twbp', 'Create');
}
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body newsletter-recipient-topic-create">

        <div class="clearfix crud-navigation">
            <div class="pull-left">
                <?= Html::a(
                    Yii::t('twbp', 'Cancel'),
                    $fromRelation ? urldecode($fromRelation) : \yii\helpers\Url::previous(),
                    ['class' => 'btn btn-default cancel-form-btn']
                ) ?>
            </div>
            <div class="pull-right">
                <?php                 \yii\bootstrap\Modal::begin([
                    'header' => '<h2>' . Yii::t('twbp', 'Information') . '</h2>',
                    'toggleButton' => [
                        'tag' => 'btn',
                        'label' => '?',
                        'class' => 'btn btn-default',
                        'title' => Yii::t('twbp', 'Information about possible search operators')
                    ],
                ]);?>

                <?= $this->render('@taktwerk-boilerplate/views/_shortcut_info_modal') ?>
                <?php  \yii\bootstrap\Modal::end();
                ?>
            </div>
        </div>

        <?= $this->render('_form', [
            'model' => $model,
            'inlineForm' => $inlineForm,
            'action' => $action,
            'relatedTypeForm' => $relatedTypeForm,
            'fromRelation' => $fromRelation,
                ]); ?>

    </div>
</div>
