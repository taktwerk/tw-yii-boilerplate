<?php
//Generation Date: 11-Sep-2020 02:54:33pm
use yii\helpers\Html;
use Yii;
/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipientTopic $model
 * @var array $pk
 * @var array $show
 */

$this->title = 'Newsletter Recipient Topic ' . $model->id . ', ' . Yii::t('twbp', 'Edit Multiple');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('twbp','Newsletter Recipient Topics'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('twbp', 'Edit Multiple');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body newsletter-recipient-topic-update">

        <h1>
            <?= \Yii::t('twbp','Newsletter Recipient Topic'); ?>
        </h1>

        <div class="crud-navigation">
        </div>

        <?php echo $this->render('_form', [
            'model' => $model,
            'pk' => $pk,
            'show' => $show,
            'multiple' => true,
            'useModal' => $useModal,
            'action' => $action,
        ]); ?>

    </div>
</div>