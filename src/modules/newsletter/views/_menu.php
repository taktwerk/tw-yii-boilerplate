<?php

use yii\bootstrap\Nav;

?>

<?= Nav::widget(
    [
        'options' => [
            'class' => 'nav-tabs',
            'style' => 'margin-bottom: 15px',
        ],
        'items' => [
            [
                'label' => Yii::t('twbp', 'Newsletters'),
                'url' => ['/newsletter/main'],
                'active' => $this->context->route == 'newsletter/main/index',
            ],
            [
                'label' => Yii::t('twbp', 'Topics'),
                'url' => ['/newsletter/topic'],
                'active' => $this->context->route == 'newsletter/topic/index',
            ],
            [
                'label' => Yii::t('twbp', 'Create'),
                'items' => [
                    [
                        'label' => Yii::t('twbp', 'Create newsletter'),
                        'url' => ['/newsletter/main/create'],
                    ],
                    [
                        'label' => Yii::t('twbp', 'Create topic'),
                        'url' => ['/newsletter/topic/create'],
                    ],
                ],
            ],
        ],
    ]
);
?>
