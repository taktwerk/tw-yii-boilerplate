<?php

use taktwerk\yiiboilerplate\modules\newsletter\components\Hook;
use taktwerk\yiiboilerplate\modules\newsletter\plugins\NewsletterTopicMultipleDropdownShow;
use yii\base\Event;
use yii\widgets\ActiveForm;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\modules\newsletter\enum\GenderTypeEnum;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use taktwerk\yiiboilerplate\widget\Select2;

$languageModelClass = '\app\models\Language';
$disabledAttributes  = $this->context->module->newsletterRecipientDisabledAttributes
?>

<style>
    .margin-auto {
        margin: auto;
    }
    .float-none {
        float: none;
    }
</style>

<div class="form_bg">
    <div class="form_bg_inner">
        <?php $form = ActiveForm::begin(
                ['fieldConfig' => false, 'enableAjaxValidation' => false, 'enableClientValidation' => false]
        ); ?>
        <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
        <div  class="col-xs-12">
        <?php if(!in_array('email',$disabledAttributes)){?>
            <div class="card-body pb-0 pt-0">
                <div class="margin-auto float-none">
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'disabled' => !empty($model->user_id)]) ?>
                </div>
            </div>
            <?php }?>
            <?php if(!in_array('first_name',$disabledAttributes)){?>
            <div class="card-body pb-0 pt-0">
                <div class="margin-auto float-none">
                    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <?php }?>
            <?php if(!in_array('last_name',$disabledAttributes)){?>
            <div class="card-body pb-0 pt-0">
                <div class="margin-auto float-none">
                    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <?php }?>
            <?php if(!in_array('gender',$disabledAttributes)){?>
            <div class="card-body pb-0 pt-0">
                <div class="margin-auto float-none">
                    <?= $form->field($model, 'gender')->radioList( [GenderTypeEnum::FEMALE_TYPE => GenderTypeEnum::FEMALE_TYPE, GenderTypeEnum::MALE_TYPE => GenderTypeEnum::MALE_TYPE] ) ?>
                </div>
            </div>
            <?php }?>
            <div class="card-body pb-0 pt-0">
                <div class="margin-auto float-none">
                    <?php
                        Event::trigger(
                                Hook::class,
                                Hook::MULTIPLE_TOPIC_LIST_SELECT,
                                new NewsletterTopicMultipleDropdownShow($model->email, $model, $form)
                        );
                    ?>
                </div>
            </div>
            <?php if(!in_array('language_id',$disabledAttributes)){?>
            <?php if (class_exists($languageModelClass)) : ?>
                <div class="card-body pb-0 pt-0">
                    <div class="margin-auto float-none">
                        <?=$form->field($model, 'language_id')
                                ->widget(
                                    Select2::class,
                                    [
                                        'data' => $languageModelClass::find()->count() > 50 ? null : ArrayHelper::map($languageModelClass::find()->all(), 'language_id', 'toString'),
                                        'initValueText' => $languageModelClass::find()->count() > 50 ? \yii\helpers\ArrayHelper::map($languageModelClass::find()->andWhere(['language_id' => $model->language_id])->all(), 'language_id', 'toString') : '',
                                        'options' => [
                                            'placeholder' => Yii::t('twbp', 'Select a value...'),
                                            'id' => 'language_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => false,
                                            ($languageModelClass::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                                            ($languageModelClass::find()->count() > 50 ? 'ajax' : '') => [
                                                'url' => \yii\helpers\Url::to(['list']),
                                                'dataType' => 'json',
                                                'data' => new \yii\web\JsExpression('function(params) {
                                                    return {
                                                        q:params.term, m: \'Language\'
                                                    };
                                                }')
                                            ],
                                        ],
                                        'pluginEvents' => [
                                            "change" => "function() {
                                                if (($(this).val() != null) && ($(this).val() != '')) {
                                                    // Enable edit icon
                                                    $($(this).next().next().children('button')[0]).prop('disabled', false);
                                                    $.post('" .
                                                    Url::toRoute('/translatemanager/language/entry-details?id=', true) .
                                                    "' + $(this).val(), {
                                                        dataType: 'json'
                                                    })
                                                    .done(function(json) {
                                                        var json = $.parseJSON(json);
                                                        if (json.data != '') {
                                                            $('#language_id_well').html(json.data);
                                                            //$('#language_id_well').show();
                                                        }
                                                    })
                                                } else {
                                                    // Disable edit icon and remove sub-form
                                                    $($(this).next().next().children('button')[0]).prop('disabled', true);
                                                }
                                            }",
                                        ],
                                        'addon' => (Yii::$app->getUser()->can('app_language_create') && (!$relatedForm || ($relatedType != RelatedForms::TYPE_MODAL && !$useModal))) ? [
                                            'append' => [
                                                'content' => [
                                                    RelatedForms::widget([
                                                        'relatedController' => '/translatemanager/language',
                                                        'type' => $relatedTypeForm,
                                                        'selector' => 'language_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                                        'primaryKey' => 'language_id',
                                                    ]),
                                                ],
                                                'asButton' => true
                                            ],
                                        ] : []
                                    ]
                                )

                        ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php }?>
            <div class="margin-auto float-none modal-footer">
                <?php if ($model->is_subscribed) { ?>
                    <?= Html::a(
                        '<span class="glyphicon glyphicon-envelope"></span> ' .
                        Yii::t('twbp', 'Unsubscribe'),
                        ['unsubscribe', 'token' => $model->token],
                        [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t(
                                    'twbp',
                                    'Are you sure you want to unsubscribe from the newsletter?'
                                ),
                                'method' => 'post',
                            ],
                        ]
                    );
                    ?>
                <?php } else { ?>
                    <?= Html::a(
                        '<span class="glyphicon glyphicon-envelope"></span> ' .
                        Yii::t('twbp', 'Subscribe'),
                        ['subscribe', 'token' => $model->token],
                        [
                            'class' => 'btn btn-success',
                        ]
                    );
                    ?>
                <?php } ?>
                <button class="btn btn-primary" type="submit">Save</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
