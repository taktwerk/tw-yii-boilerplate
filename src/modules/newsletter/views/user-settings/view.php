<?php
$this->title = Yii::t('twbp', 'Newsletter settings');

use yii\helpers\Html;
?>

<?= $this->render('@Da/User/resources/views/shared/_alert', ['module' => Yii::$app->getModule('newsletter')]) ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="panel-body">
                <div class="login-logo logo-image"></div>
                <?= $this->render(
                        '_form',
                        [
                            'model' => $formModel,
                            'newsletterTopicsList' => $newsletterTopicsList,
                        ]
                ) ?>
            </div>
        </div>
    </div>
</div>
