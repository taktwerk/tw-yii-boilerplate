<?php use yii\helpers\Html;

$this->context->layout = '@taktwerk-boilerplate/modules/user/views/layouts/main';
$this->title = Yii::t('twbp', 'Verify email');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="panel-body">
        <div class="login-logo logo-image"></div>
        <?php echo Html::beginForm()?>
        <div class="text-center">
        <p><?php echo $this->context->module->verifyEmailText?></p>
        	<?php echo Html::submitButton(Yii::t('twbp', 'Verify email'),['class'=>"btn btn-primary"])?>
        </div>
        <?php echo Html::endForm()?>
    </div>
</div>