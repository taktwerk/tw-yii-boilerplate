<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View                      $this
 * @var yii\widgets\ActiveForm            $form
 * @var Da\User\Model\RecoveryForm $model
 */
$this->context->layout = '@taktwerk-boilerplate/modules/user/views/layouts/main';
$this->title = Yii::t('twbp', 'Subscribe to newsletters');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="panel-body">
        <div class="login-logo logo-image"></div>
        <?= \taktwerk\yiiboilerplate\modules\newsletter\widgets\SubscribeForm::widget() ?>
    </div>
</div>
