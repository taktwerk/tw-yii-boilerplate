<?php

use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Url;

?>
<div class="table-responsive">
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{pager}",
            'columns' => [
                [
                    'attribute' => 'name',
                    'header' => Yii::t('twbp', 'Name'),
                    'options' => [
                        'style' => 'width: 20%',
                    ],
                ],
                [
                    'class' => ActionColumn::class,
                    'template' => '{update} {delete}',
                    'urlCreator' => function ($action, $model) {
                        return Url::to(['/newsletter/topic/' . $action, 'id' => $model['id']]);
                    },
                    'options' => [
                        'style' => 'width: 5%',
                    ],
                ],
            ],
        ]
    ) ?>
</div>
