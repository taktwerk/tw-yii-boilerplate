<?php
use dmstr\widgets\Alert;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
$this->title = $this->title;
\taktwerk\yiiboilerplate\assets\TwAsset::register($this);
if (class_exists('\app\modules\backend\assets\AdminAsset')) {
    \app\modules\backend\assets\AdminAsset::register($this);
}

$this->render('@taktwerk-boilerplate/views/blocks/raven');

// Get the user roles
$roles = [];
foreach (Yii::$app->authManager->getRolesByUser(Yii::$app->getUser()->id) as $role) {
    $roles[] = $role->name;
}
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <?= Html::csrfMetaTags() ?>
        <title><?= Yii::t('app', getenv('APP_TITLE')) . ' - ' . Html::encode($this->title) ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Theme style -->
        <?php $this->head() ?>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="/js/html5shiv.js"></script>
        <script src="/js/respond.min.js"></script>
        <![endif]-->
        <style>
            .content-wrapper, .main-footer {
                margin-left: 0px;
            }
        </style>
    </head>

    <body
        class="hold-transition <?= isset(Yii::$app->params['adminSkin']) ? Yii::$app->params['adminSkin'] : 'skin-black' ?> sidebar-mini">
    <?php $this->beginBody() ?>

    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->

            <a href="<?= Yii::$app->homeUrl ?>" class="logo logo-image">
        <span class="logo-lg">
&nbsp;
        </span>
                <span class="logo-mini">
        </span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <span class="navbar-text hidden-xs"><?= Yii::t('app', getenv('APP_TITLE')) ?></span>
            </nav>
        </header>

        <!-- Right side column. Contains the navbar and content of the page -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <small><?= $this->title ?></small>
                </h1>
                <?=
                \yii\widgets\Breadcrumbs::widget([
                    'homeLink' => ['label' => Yii::t('twbp', 'Dashboard'), 'url' => ['/backend']],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </section>

            <!-- Main content -->

            <section class="content">
                <?php if (Yii::$app->settings->get('planned', 'maintenance', false, 'boolean')): ?>
                    <div class="callout callout-warning">
                        <h4><?=Yii::t('twbp', 'Important Information')?></h4>
                        <p><?=Yii::t('app', 'The application will be updated today at HH:II and will not be available for XX minutes.')?></p>
                    </div>
                <?php endif; ?>

                <?= $content ?>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <strong>
                <?= \taktwerk\yiiboilerplate\components\Helper::version() ?></strong>
            &copy; <strong><a href="http://taktwerk.ch">taktwerk.ch</a></strong> | 2012-<?= date("Y", time()) ?>
            <?php
            if(YII_ENV_DEV) {
                $basePermission = Yii::$app->controller->module->id . '_' . Yii::$app->controller->id;
                echo '<i class="text-muted">ACL: ' . $basePermission . '</i>';
            }
            ?>
        </footer>
    </div>
    <!-- ./wrapper -->

    <?php $this->endBody() ?>
    <?php
    \yii\bootstrap\Modal::begin([
        'toggleButton' => false,
        'header' => Yii::t('twbp', 'Notification'),
        'size' => \yii\bootstrap\Modal::SIZE_SMALL,
        'id' => 'notificationModal',
    ]);
    \yii\bootstrap\Modal::end();
    ?>
    <?php
    \yii\bootstrap\Modal::begin([
        'toggleButton' => false,
        'size' => \yii\bootstrap\Modal::SIZE_LARGE,
        'id' => 'loadingModal',
        'closeButton' => false,
        //keeps from closing modal with esc key or by clicking out of the modal.
        // user must click cancel or X to close
        'clientOptions' => [
            'backdrop' => 'static',
            'keyboard' => false,
        ]
    ]);
    echo "<div id='modalContent'>
<i class='fa fa-circle-o-notch fa-spin fa-5x fa-fw'></i>
</div>";
    \yii\bootstrap\Modal::end();
    ?>
    </body>
    </html>
<?php
$this->endPage();
