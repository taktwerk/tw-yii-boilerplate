<?php

$baseUrl = Yii::$app->getModule('newsletter')->urlManager->getBaseUrl();

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <!--<link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900" rel="stylesheet">-->
    <?php $this->head() ?>
</head>
<body style="margin: 0">
<table border="0" cellpadding="0" cellspacing="0" style="margin:0; padding:0; width: 100%; font-family: 'Open Sans', sans-serif; font-size: 16px; color: white !important;">
    <tbody>
    <tr>
        <td>
            <div style="font-size: 24px; font-weight: 700; color: black;">
                <div style="text-align: center;margin: 20px;">
                    <span>Hi
                        <?php
                        if (!empty($newsletterRecipient->last_name) || !empty($newsletterRecipient->first_name)) {
                            $firstName = $newsletterRecipient->first_name ?: '';
                            $lastName = $newsletterRecipient->last_name ?: '';
                            echo ', ' . $firstName . ' ' . $lastName;
                        }
                        ?>
                    </span>
                    <br>
                </div>
                <div style="color: red; text-align: center;">
                    Test template 2345
                </div>
                <table border="0" cellpadding="0" cellspacing="0" style="margin:0; padding:0; width: 100%; font-family: 'Open Sans', sans-serif; font-size: 16px; color: white !important;">
                    <tbody>
                    <tr>
                        <td>
                            <?php if($newsletterRecipient->token) { ?>
                                <div style="text-align: center;margin: 40px;">
                                    <a href="<?= $baseUrl . '/newsletter/user-settings/' . $newsletterRecipient->token ?>"
                                       style="padding: 20px;background-color: #3681fc;color: white;"
                                    >
                                        Recipient Settings
                                    </a>
                                </div>
                            <?php } ?>
                            <div style="margin:0; padding:0; height: 56px; background: #3681fc; width: 100%;">
                                <div style="padding:0; width: 610px; height: 56px; display: block; margin: 0 auto;">
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>

