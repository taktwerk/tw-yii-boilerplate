<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\helpers;

use taktwerk\yiiboilerplate\modules\newsletter\exceptions\NotFoundNewsletterTopicException;
use taktwerk\yiiboilerplate\modules\newsletter\exceptions\NotFoundNewsletterTopicTemplateException;
use taktwerk\yiiboilerplate\modules\newsletter\models\Newsletter;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient;
use yii\db\Expression;
use Yii;
use yii\web\View;

class NewsletterSender
{
    public $newsletter;

    public function __construct(Newsletter $newsletter)
    {
        $this->newsletter = $newsletter;
        Yii::$app->mailer->view->params['logo'] = getenv('LETTER_LOGO') ? :
            getenv('APP_BASE_URL').'/images/logo.svg';
        Yii::$app->mailer->view->params['background'] = getenv('LETTER_BACKGROUND') ? :
            getenv('APP_BASE_URL').'/images/background.svg';
    }

    public function send()
    {
        $newsletterTopic = $this->newsletter->newsletterTopic;

        if (!$newsletterTopic) {
            throw(
                new NotFoundNewsletterTopicException('There is no such newsletter topic')
            );
        }

        $newsletterRecipients = $newsletterTopic->subscribedNewsletterRecipients;

        foreach ($newsletterRecipients as $newsletterRecipient) {
            $this->sendNewsletterEmailToRecipient($newsletterRecipient);
        }
    }

    public function sendNewsletterEmailToRecipient(NewsletterRecipient $newsletterRecipient)
    {
        $newsletterModuleMailDirectory = '@taktwerk-boilerplate/modules/newsletter/mail/';
        Yii::$app->mailer->htmlLayout = $newsletterModuleMailDirectory . 'layouts/html';
        Yii::$app->mailer->view->params['subLayout'] = $newsletterModuleMailDirectory . 'layouts/sub-layouts/html';
        Yii::$app->mailer->view->params['newsletterRecipient'] = $newsletterRecipient;
        Yii::$app->mailer->view->params['title'] = $this->newsletter->name;
        Yii::$app->mailer->view->params['showMailRecipientLinksAtTheEnd'] = false;
        Yii::$app->mailer->view->params['attachments'] = [];
        Yii::$app->mailer->view->params['embededImages'] = [];
        Yii::$app->mailer->view->params['language_id'] = $newsletterRecipient->language_id;
        Yii::$app->language = $newsletterRecipient->language_id;

        $template = $this->newsletter->template ? : $newsletterModuleMailDirectory . 'default';
        $emailContent = Yii::$app->mailer
            ->render(
                $template,
                [
                    'newsletterSettingsLink' => Yii::$app->getModule('newsletter')->urlManager->baseUrl,
                    'newsletterRecipient' => $newsletterRecipient
                ],
                Yii::$app->mailer->htmlLayout
            );

        $sendMailer = Yii::$app->mailer->compose();

        if (Yii::$app->mailer->view->params['attachments']) {
            foreach (Yii::$app->mailer->view->params['attachments'] as $attachment) {
                $sendMailer->attach($attachment);
            }
        }

        if (Yii::$app->mailer->view->params['embededImages']) {
            foreach (Yii::$app->mailer->view->params['embededImages'] as $embededImage) {
                if (empty($embededImage['tag']) || empty($embededImage['url'])) {
                    continue;
                }

                $newEmbededImage = str_replace(
                    [
                        "src=\"{$embededImage['url']}\"",
                        "src='{$embededImage['url']}'",
                    ],
                    'src="' . $sendMailer->embed($embededImage['url']) . '"',
                    $embededImage['tag'],
                );
                $emailContent = str_replace(
                    $embededImage['tag'],
                    $newEmbededImage,
                    $emailContent,
                );
            }
        }

        $sendMailer = $sendMailer->setFrom(Yii::$app->getModule('newsletter')->fromEmail)
            ->setTo($newsletterRecipient->email)
            ->setSubject($this->newsletter->name)
            ->setHtmlBody($emailContent)
            ->send();
    }
}


