<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200524_192008_add_new_column_user_id_to_newsletter_recipient extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%newsletter_recipient}}', 'user_id', $this->integer()->null());

        $this->addForeignKey(
            "fk_newsletter_recipient_user_id",
            "{{%newsletter_recipient}}",
            "user_id",
            "{{%user}}",
            "id"
        );

        $this->createIndex(
            'newsletter-recipient-user_id-unique',
            '{{%newsletter_recipient}}',
            'user_id',
            1
        );
    }

    public function down()
    {
        $this->dropIndex('newsletter-recipient-user_id-unique', '{{%newsletter_recipient}}');
        $this->dropForeignKey('fk_newsletter_recipient_user_id', '{{%newsletter_recipient}}');
        $this->dropColumn('{{%newsletter_recipient}}', 'user_id');
    }
}
