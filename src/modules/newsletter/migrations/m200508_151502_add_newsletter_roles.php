<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200508_151502_add_newsletter_roles extends TwMigration
{
    public function up()
    {
        $auth = Yii::$app->authManager;

        if ($auth instanceof \yii\rbac\DbManager) {
            // Add the Authority role if it doesn't exist
            $role = $auth->getRole('NewsletterAdmin');
            if ($role === null) {
                $newsletterAdmin = $auth->createRole('NewsletterAdmin');
                $newsletterAdmin->description = 'Newsletter Admin';
                $auth->add($newsletterAdmin);
                $auth->assign($newsletterAdmin, 1);

                $newsletterViewer = $auth->createRole('NewsletterViewer');
                $newsletterViewer->description = 'Newsletter Viewer';
                $auth->add($newsletterViewer);
                $auth->assign($newsletterViewer, 1);
            }
        } else {
            throw new \yii\base\Exception(
                'Application authManager must be an instance of \yii\rbac\DbManager'
            );
        }
    }

    public function down()
    {
        $auth = Yii::$app->authManager;

        if ($auth instanceof \yii\rbac\DbManager) {
            $auth->remove($auth->getRole('NewsletterAdmin'));
            $auth->remove($auth->getRole('NewsletterViewer'));
        } else {
            throw new \yii\base\Exception(
                'Application authManager must be an instance of \yii\rbac\DbManager'
            );
        }
    }
}
