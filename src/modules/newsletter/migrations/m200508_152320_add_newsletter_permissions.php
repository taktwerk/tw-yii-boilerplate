<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200508_152320_add_newsletter_permissions extends TwMigration
{
    public function up()
    {
        $this->createCrudControllerPermissions('newsletter');
        $this->addSeeControllerPermission(
            'newsletter',
            ['Administrator', 'NewsletterAdmin', 'NewsletterViewer']
        );
    }

    public function down()
    {
        echo "m200508_152320_add_newsletter_permissions cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
