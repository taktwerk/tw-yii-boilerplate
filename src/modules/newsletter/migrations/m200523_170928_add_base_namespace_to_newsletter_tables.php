<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200523_170928_add_base_namespace_to_newsletter_tables extends TwMigration
{
    public function up()
    {
        $query = <<<EOF
        
            ALTER TABLE `newsletter`
                COMMENT='';
                
            ALTER TABLE `newsletter`
                COMMENT='{"base_namespace":"taktwerk\\\\\\\\yiiboilerplate\\\\\\\\modules\\\\\\\\newsletter"}';
                
            ALTER TABLE `newsletter_recipient`
                COMMENT='';
                
            ALTER TABLE `newsletter_recipient`
                COMMENT='{"base_namespace":"taktwerk\\\\\\\\yiiboilerplate\\\\\\\\modules\\\\\\\\newsletter"}';
                
            ALTER TABLE `newsletter_recipient_topic`
                COMMENT='';
                
            ALTER TABLE `newsletter_recipient_topic`
                COMMENT='{"base_namespace":"taktwerk\\\\\\\\yiiboilerplate\\\\\\\\modules\\\\\\\\newsletter"}';
                
            ALTER TABLE `newsletter_topic`
                COMMENT='';
                
            ALTER TABLE `newsletter_topic`
                COMMENT='{"base_namespace":"taktwerk\\\\\\\\yiiboilerplate\\\\\\\\modules\\\\\\\\newsletter"}';            
       
EOF;

        $this->execute($query);
    }

    public function down()
    {
        echo "m200523_170928_add_base_namespace_to_newsletter_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
