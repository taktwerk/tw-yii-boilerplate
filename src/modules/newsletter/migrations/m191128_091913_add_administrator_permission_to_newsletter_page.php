<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191128_091913_add_administrator_permission_to_newsletter_page extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $permission = $auth->createPermission('newsletter_admin');
        $permission->description = 'Admin newsletter permission';
        $auth->add($permission);

        $role = $auth->getRole('Administrator');
        $auth->addChild($role, $permission);

    }

    public function down()
    {
        echo "m191128_091913_add_administrator_permission_to_newsletter_page cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
