<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191122_204017_add_newsletter_permissions extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $permission = $auth->createPermission('newsletter_user-settings');
        $permission->description = 'Newsletter settings controller';
        $auth->add($permission);

        $role = $auth->getRole('Public');
        $auth->addChild($role, $permission);
    }

    public function down()
    {
        echo "m191122_204017_add_newsletter_permissions cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
