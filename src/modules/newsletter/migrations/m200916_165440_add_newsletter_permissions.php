<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200916_165440_add_newsletter_permissions extends TwMigration
{
    public function up()
    {
        /*add newsletter module permissions*/
        $this->createCrudControllerPermissions('newsletter_newsletter');
        $this->addAdminControllerPermission('newsletter_newsletter','NewsletterAdmin');
        $this->addSeeControllerPermission('newsletter_newsletter','NewsletterViewer');

        $this->createCrudControllerPermissions('newsletter_newsletter-topic');
        $this->addAdminControllerPermission('newsletter_newsletter-topic','NewsletterAdmin');
        $this->addSeeControllerPermission('newsletter_newsletter-topic','NewsletterViewer');

        $this->createCrudControllerPermissions('newsletter_newsletter-recipient');
        $this->addAdminControllerPermission('newsletter_newsletter-recipient','NewsletterAdmin');
        $this->addSeeControllerPermission('newsletter_newsletter-recipient','NewsletterViewer');

        /* remove wrong permissions */
        $auth = $this->getAuth();
        $news = $auth->getRole('NewsletterViewer');
        $backend = $auth->getRole('Backend');
        if($auth->hasChild($backend, $news)){
            $auth->removeChild($backend, $news);
        }
    }

    public function down()
    {
        echo "m200916_165440_add_newsletter_permissions cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
