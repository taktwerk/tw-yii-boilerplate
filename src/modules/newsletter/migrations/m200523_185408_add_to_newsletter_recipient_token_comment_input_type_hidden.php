<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200523_185408_add_to_newsletter_recipient_token_comment_input_type_hidden extends TwMigration
{
    public function up()
    {
        $comment = <<<EOT
{"inputtype":"hidden"}
EOT;
        $this->addCommentOnColumn('{{%newsletter_recipient}}', 'token', $comment);
    }

    public function down()
    {
        echo "m200523_185408_add_to_newsletter_recipient_token_comment_input_type_hidden cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
