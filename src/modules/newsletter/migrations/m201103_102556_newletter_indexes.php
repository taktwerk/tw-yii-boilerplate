<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m201103_102556_newletter_indexes extends TwMigration
{
    public function up()
    {
       $this->createIndex('idx_newsletter_deleted_at_topic_id', '{{%newsletter}}', ['deleted_at','topic_id']);
       $this->createIndex('idx_newsletter_recipient_deleted_at_user_id', '{{%newsletter_recipient}}', ['deleted_at','user_id']);
    }

    public function down()
    {
        echo "m201103_102556_newletter_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
