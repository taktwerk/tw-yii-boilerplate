<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191128_092619_delete_global_public_permission_to_newsletter_page extends TwMigration
{
    public function up()
    {
        $this->removePermission('newsletter');
    }

    public function down()
    {
        echo "m191128_092619_delete_global_public_permission_to_newsletter_page cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
