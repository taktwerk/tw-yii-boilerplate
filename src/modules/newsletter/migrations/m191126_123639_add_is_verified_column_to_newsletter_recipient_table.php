<?php

use taktwerk\yiiboilerplate\TwMigration;
use yii\db\Migration;

/**
 * Handles adding columns to table `{{%newsletter_recipient}}`.
 */
class m191126_123639_add_is_verified_column_to_newsletter_recipient_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            '{{%newsletter_recipient}}',
            'is_verified',
            $this->boolean()->defaultValue(false)->after('gender')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%newsletter_recipient}}', 'is_verified');
    }
}
