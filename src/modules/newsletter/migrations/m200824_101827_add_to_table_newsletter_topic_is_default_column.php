<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200824_101827_add_to_table_newsletter_topic_is_default_column extends TwMigration
{
    public function up()
    {
        $this->addColumn(
            '{{%newsletter_topic}}',
            'is_default',
            $this->boolean()->defaultValue(false)->after('name')
        );
    }

    public function down()
    {
        echo "m200824_101827_add_to_table_newsletter_topic_is_default_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
