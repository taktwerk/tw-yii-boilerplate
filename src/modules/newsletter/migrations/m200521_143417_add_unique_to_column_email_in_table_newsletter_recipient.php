<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200521_143417_add_unique_to_column_email_in_table_newsletter_recipient extends TwMigration
{
    public function up()
    {
        $this->createIndex(
            'newsletter-recipient-email-unique',
            '{{%newsletter_recipient}}',
            'email',
            1
        );
    }

    public function down()
    {
        $this->dropIndex('newsletter-recipient-email-unique', '{{%newsletter_recipient}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
