<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191127_185104_create_table_newsletter extends TwMigration
{
    protected $tableName =  'newsletters';
    protected $topicTableName = 'newsletter_topic';
    protected $topicIdColumn = 'topic_id';

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable('{{%' . $this->tableName . '}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'topic_id' => $this->integer()->notNull(),
            'template' => $this->string(255)->null(),
        ], $tableOptions);

        $this->createIndex(
            "idx-{$this->tableName}-{$this->topicIdColumn}",
            '{{%' . $this->tableName . '}}',
            $this->topicIdColumn
        );

        $this->addForeignKey(
            "fk-{$this->tableName}-{$this->topicIdColumn}",
            '{{%' . $this->tableName . '}}',
            $this->topicIdColumn,
            '{{%' . $this->topicTableName . '}}',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            "fk-{$this->tableName}-{$this->topicIdColumn}",
            '{{%' . $this->tableName . '}}'
        );
        $this->dropIndex(
            "idx-{$this->tableName}-{$this->topicIdColumn}",
            '{{%' . $this->tableName . '}}'
        );

        $this->dropTable('{{%' . $this->tableName . '}}');
    }
}
