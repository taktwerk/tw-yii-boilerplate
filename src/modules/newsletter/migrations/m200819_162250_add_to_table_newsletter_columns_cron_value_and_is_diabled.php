<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200819_162250_add_to_table_newsletter_columns_cron_value_and_is_diabled extends TwMigration
{
    public function up()
    {
        $this->addColumn(
            '{{%newsletter}}',
            'disabled',
            $this->boolean()->defaultValue(true)->after('template')
        );

        $this->addColumn(
            '{{%newsletter}}',
            'cron_value',
            $this->string(255)->after('disabled')
        );

        $this->dropColumn(
            '{{%newsletter}}',
            'last_periodic_dispatch_at'
        );

        $this->dropColumn(
            '{{%newsletter}}',
            'last_dispatch_at'
        );
    }

    public function down()
    {
        echo "m200819_162250_add_to_table_newsletter_columns_cron_value_and_is_diabled cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
