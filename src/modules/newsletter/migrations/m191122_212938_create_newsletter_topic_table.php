<?php

use taktwerk\yiiboilerplate\TwMigration;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%newsletter_topic}}`.
 */
class m191122_212938_create_newsletter_topic_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%newsletter_topic}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%newsletter_topic}}');
    }
}
