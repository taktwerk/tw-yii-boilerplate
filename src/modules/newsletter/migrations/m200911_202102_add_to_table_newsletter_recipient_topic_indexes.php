<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200911_202102_add_to_table_newsletter_recipient_topic_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_newsletter_recipient_topic_deleted_at_recipient_id', '{{%newsletter_recipient_topic}}', ['deleted_at','recipient_id']);
        $this->createIndex('idx_newsletter_recipient_topic_deleted_at_topic_id', '{{%newsletter_recipient_topic}}', ['deleted_at','topic_id']);
    }

    public function down()
    {
        echo "m200911_202102_add_to_table_newsletter_recipient_topic_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
