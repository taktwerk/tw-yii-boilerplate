<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m201123_125609_newsletter_send_perm extends TwMigration
{
    public function safeUp()
    {
        $this->createPermission('newsletter_newsletter_send','To send newsletter');
        $auth = $this->getAuth();
        $newsletterAdmin = $auth->getRole('NewsletterAdmin');
        $perm = $this->getPermission('newsletter_newsletter_send');
        $auth->addChild($newsletterAdmin, $perm);
    }

    public function safeDown()
    {
        echo "m201123_125609_newsletter_send_perm cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
