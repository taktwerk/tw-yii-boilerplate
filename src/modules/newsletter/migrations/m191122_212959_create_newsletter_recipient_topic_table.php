<?php

use taktwerk\yiiboilerplate\TwMigration;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%newsletter_recipient_topic}}`.
 */
class m191122_212959_create_newsletter_recipient_topic_table extends TwMigration
{
    protected $tableName =  'newsletter_recipient_topic';
    protected $recipientTableName = 'newsletter_recipient';
    protected $topicTableName = 'newsletter_topic';
    protected $recipientIdColumn = 'recipient_id';
    protected $topicIdColumn = 'topic_id';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%' . $this->tableName . '}}', [
            'id' => $this->primaryKey(),
            'recipient_id' => $this->integer()->notNull(),
            'topic_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            "idx-{$this->tableName}-{$this->recipientIdColumn}",
            '{{%' . $this->tableName . '}}',
            $this->recipientIdColumn
        );

        $this->addForeignKey(
            "fk-{$this->tableName}-{$this->recipientIdColumn}",
            '{{%' . $this->tableName . '}}',
            $this->recipientIdColumn,
            '{{%' . $this->recipientTableName . '}}',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            "idx-{$this->tableName}-{$this->topicIdColumn}",
            '{{%' . $this->tableName . '}}',
            $this->topicIdColumn
        );

        $this->addForeignKey(
            "fk-{$this->tableName}-{$this->topicIdColumn}",
            '{{%' . $this->tableName . '}}',
            $this->topicIdColumn,
            '{{%' . $this->topicTableName . '}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            "fk-{$this->tableName}-{$this->recipientIdColumn}",
            '{{%' . $this->tableName . '}}'
        );
        $this->dropIndex(
            "idx-{$this->tableName}-{$this->recipientIdColumn}",
            '{{%' . $this->tableName . '}}'
        );

        $this->dropForeignKey(
            "fk-{$this->tableName}-{$this->topicIdColumn}",
            '{{%' . $this->tableName . '}}'
        );
        $this->dropIndex(
            "idx-{$this->tableName}-{$this->topicIdColumn}",
            '{{%' . $this->tableName . '}}'
        );

        $this->dropTable('{{%' . $this->tableName . '}}');
    }
}
