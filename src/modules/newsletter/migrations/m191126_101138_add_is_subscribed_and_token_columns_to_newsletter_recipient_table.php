<?php

use taktwerk\yiiboilerplate\TwMigration;
use yii\db\Migration;

/**
 * Handles adding columns to table `{{%newsletter_recipient}}`.
 */
class m191126_101138_add_is_subscribed_and_token_columns_to_newsletter_recipient_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            '{{%newsletter_recipient}}',
            'is_subscribed',
            $this->boolean()->defaultValue(true)->after('gender')
        );
        $this->addColumn(
            '{{%newsletter_recipient}}',
            'token',
            $this->string(255)->notNull()->after('is_subscribed')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%newsletter_recipient}}', 'is_subscribed');
        $this->dropColumn('{{%newsletter_recipient}}', 'token');
    }
}
