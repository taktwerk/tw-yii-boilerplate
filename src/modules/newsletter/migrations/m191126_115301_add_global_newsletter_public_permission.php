<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191126_115301_add_global_newsletter_public_permission extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $permission = $auth->createPermission('newsletter');
        $permission->description = 'Newsletter permission';
        $auth->add($permission);

        $role = $auth->getRole('Public');
        $auth->addChild($role, $permission);
    }

    public function down()
    {
        echo "m191126_115301_add_global_newsletter_public_permission cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
