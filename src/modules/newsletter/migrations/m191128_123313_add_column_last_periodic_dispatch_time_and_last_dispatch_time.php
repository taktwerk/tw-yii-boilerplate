<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191128_123313_add_column_last_periodic_dispatch_time_and_last_dispatch_time extends TwMigration
{
    public function up()
    {
        $this->addColumn(
            '{{%newsletters}}',
            'last_periodic_dispatch_at',
            Schema::TYPE_DATETIME
        );
        $this->addColumn(
            '{{%newsletters}}',
            'last_dispatch_at',
            Schema::TYPE_DATETIME
        );
    }

    public function down()
    {
        $this->dropColumn('{{%newsletters}}', 'last_periodic_dispatch_at');
        $this->dropColumn('{{%newsletters}}', 'last_dispatch_at');
    }
}
