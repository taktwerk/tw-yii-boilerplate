<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200515_142841_rename_newsletters_table_to_newsletter extends TwMigration
{
    public function safeUp()
    {
        $this->renameTable('{{%newsletters}}', '{{%newsletter}}');
    }

    public function safeDown()
    {
        $this->renameTable('{{%newsletter}}', '{{%newsletters}}');
    }
}
