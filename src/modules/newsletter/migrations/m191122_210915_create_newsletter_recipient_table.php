<?php

use taktwerk\yiiboilerplate\modules\newsletter\enum\GenderTypeEnum;
use taktwerk\yiiboilerplate\TwMigration;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%newsletter_recipient}}`.
 */
class m191122_210915_create_newsletter_recipient_table extends TwMigration
{
    protected $genderTypeEnumString = '';

    public function __construct($config = [])
    {
        parent::__construct($config);

        $this->genderTypeEnumString = "'" . GenderTypeEnum::FEMALE_TYPE . "','" . GenderTypeEnum::MALE_TYPE . "'";
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%newsletter_recipient}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string(255)->notNull(),
            'first_name' => $this->string(255)->null(),
            'last_name' => $this->string(255)->null(),
            'gender' => "ENUM({$this->genderTypeEnumString})",
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%newsletter_recipient}}');
    }
}
