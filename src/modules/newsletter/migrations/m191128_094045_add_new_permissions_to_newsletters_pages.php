<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191128_094045_add_new_permissions_to_newsletters_pages extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $permission = $auth->createPermission('newsletter_auth_register');
        $permission->description = 'Newsletter register controller';
        $auth->add($permission);

        $role = $auth->getRole('Public');
        $auth->addChild($role, $permission);

        $permission = $auth->createPermission('newsletter_auth_verify');
        $permission->description = 'Newsletter verify controller';
        $auth->add($permission);

        $role = $auth->getRole('Public');
        $auth->addChild($role, $permission);
    }

    public function down()
    {
        echo "m191128_094045_add_new_permissions_to_newsletters_pages cannot be reverted.\n";

        return false;
    }
}
