<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient;

class m200804_163606_add_indexes_to_language_id_to_newsletter_recipient_table extends TwMigration
{
    public function up()
    {
        $newsletterRecipients = NewsletterRecipient::find()->all();
        foreach ($newsletterRecipients as $newsletterRecipient) {
            $newsletterRecipient->language_id = 'de';
            $newsletterRecipient->save();
        }
        // $this->createIndex('idx_newsletter_recipient_deleted_at_user_id', '{{%newsletter_recipient}}', ['deleted_at','user_id']);
        $query = <<<EOF
        
            ALTER TABLE `newsletter_recipient`
CHANGE `language_id` `language_id` varchar(5) COLLATE 'utf8_unicode_ci' NOT NULL AFTER `user_id`;
ALTER TABLE `newsletter_recipient` COLLATE 'utf8_unicode_ci';
ALTER TABLE `newsletter_recipient` ADD INDEX `language_id` (`language_id`);
ALTER TABLE `newsletter_recipient`
CHANGE `language_id` `language_id` varchar(5) COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT 'de' AFTER `user_id`;
ALTER TABLE `newsletter_recipient`
CHANGE `language_id` `language_id` varchar(5) COLLATE 'utf8_unicode_ci' NULL AFTER `user_id`;
ALTER TABLE `newsletter_recipient`
CHANGE `language_id` `language_id` varchar(5) COLLATE 'utf8_unicode_ci' NULL DEFAULT 'de' AFTER `user_id`;
ALTER TABLE `newsletter_recipient`
ADD FOREIGN KEY (`language_id`) REFERENCES `language` (`language_id`)
       
EOF;

        $this->execute($query);
        $this->createIndex('idx_newsletter_recipient_deleted_at_language_id', '{{%newsletter_recipient}}', ['deleted_at','language_id']);
    }

    public function down()
    {
        echo "m200804_163606_add_indexes_to_language_id_to_newsletter_recipient_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
