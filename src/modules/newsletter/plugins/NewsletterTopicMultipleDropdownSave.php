<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\plugins;

use taktwerk\yiiboilerplate\modules\newsletter\components\NewsletterRecipientRegistrationComponent;
use taktwerk\yiiboilerplate\modules\newsletter\dto\NewsletterRecipientRegistrationDto;
use taktwerk\yiiboilerplate\modules\newsletter\exceptions\NewsletterEmailValidationErrorException;
use taktwerk\yiiboilerplate\modules\newsletter\exceptions\NotFoundNewsletterRecipientException;
use taktwerk\yiiboilerplate\modules\newsletter\exceptions\NotFoundNewsletterTopicException;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipientTopic;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterTopic;
use yii\base\Event;
use Yii;

class NewsletterTopicMultipleDropdownSave extends Event
{
    public $email;
    public $topicIds;
    public $doNeedCreateNewRecipient;

    public function __construct($email, $topicIds = [], $doNeedCreateNewRecipient = false, $config = [])
    {
        parent::__construct($config);
        $this->email = $email;
        $this->topicIds = $topicIds;
        $this->doNeedCreateNewRecipient = $doNeedCreateNewRecipient;
    }

    public function save(Event $event)
    {
        if (!$this->email) {
            throw(
                new NewsletterEmailValidationErrorException(
                    Yii::t('twbp', 'Email is a required field')
                )
            );
        }
        $newsletterRecipientModel = NewsletterRecipient::find()
            ->with('newsletterTopics')
            ->where(['email' => $this->email, 'is_verified' => 1])
            ->one();
        if (!$newsletterRecipientModel->id) {
            if (!$this->doNeedCreateNewRecipient) {
                throw(
                    new NotFoundNewsletterRecipientException(
                        'Newsletter recipient not found'
                    )
                );
            }
            $newsletterRecipientRegistrationDto = Yii::$container->get(
                NewsletterRecipientRegistrationDto::class
            );
            $newsletterRecipientRegistrationDto->setIsMakeSubscribed(true)
                ->setIsMakeVerified(true)
                ->setSendVerifyEmail(false)
                ->setRegistrationData(
                    ['email' => $this->email]
                );
            $newsletterRecipientRegistrationComponent = new NewsletterRecipientRegistrationComponent(
                $newsletterRecipientRegistrationDto
            );
            $newsletterRecipientModel = $newsletterRecipientRegistrationComponent->register();
        }

        $newsletterRecipientModel->addAllNewsletterTopics($this->topicIds, false);
    }
}
