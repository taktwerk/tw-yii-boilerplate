<?php

namespace taktwerk\yiiboilerplate\modules\newsletter\plugins;

use taktwerk\yiiboilerplate\modules\newsletter\exceptions\NewsletterActiveFormErrorException;
use taktwerk\yiiboilerplate\modules\newsletter\exceptions\NewsletterModelValidationErrorException;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterRecipient;
use taktwerk\yiiboilerplate\modules\newsletter\models\NewsletterTopic;
use taktwerk\yiiboilerplate\widget\Select2;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use Yii;

class NewsletterTopicMultipleDropdownShow extends Event
{
    public $email;
    public $model;
    public $form;

    public function __construct($email, $model, ActiveForm $form, $config = [])
    {
        parent::__construct($config);
        $this->email = $email;
        $this->model = $model;
        $this->form = $form;
    }

    public function show(Event $event)
    {
        if (empty($this->model)) {
            throw(
                new NewsletterModelValidationErrorException(
                    Yii::t('twbp', 'Please define a model for rendering the select.')
                )
            );
        }
        if (empty($this->form)) {
            throw(
                new NewsletterActiveFormErrorException(
                    Yii::t('twbp', 'Please define ActiveForm form for rendering the select.')
                )
            );
        }

        $newsletterTopicsList = ArrayHelper::map(
            NewsletterTopic::find()->select(['id', 'name'])->asArray()->all(),
            'id',
            'name'
        );
        $newsletterRecipientTopics = [];
        if ($this->email) {
            $newsletterRecipientModel = NewsletterRecipient::find()
                ->with('newsletterTopics')
                ->where(['email' => $this->email, 'is_verified' => 1])
                ->one();

            $newsletterRecipientTopics = ArrayHelper::toArray($newsletterRecipientModel->newsletterTopics);
        }

        $this->model->topic_ids = ArrayHelper::getColumn($newsletterRecipientTopics, 'id');
        if(count($newsletterTopicsList)>1){
            $newsletterTopicMultipleDropdown = $this->form->field($this->model, 'topic_ids')
                ->widget(Select2::class, [
                        'value' => $newsletterRecipientTopics,
                        'data' => $newsletterTopicsList,
                        'options' => ['multiple' => true, 'placeholder' => \Yii::t('twbp','Select topics...')]
                    ]);
            echo $newsletterTopicMultipleDropdown;
        }
    }
}
