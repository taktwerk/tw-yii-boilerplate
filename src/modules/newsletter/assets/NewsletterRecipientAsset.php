<?php
/**
 * @link http://www.yiiframework.com/
 *
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace taktwerk\yiiboilerplate\modules\newsletter\assets;

use dmstr\web\AdminLteAsset;
use taktwerk\yiiboilerplate\assets\TwAsset;
use yii\helpers\FileHelper;
use yii\web\AssetBundle;
use Yii;

/**
 * Configuration for `backend` client script files.
 *
 * @since 4.0
 */
class NewsletterRecipientAsset extends AssetBundle
{
    public $sourcePath = '@vendor/taktwerk/yii-boilerplate/src/modules/newsletter/assets/web';

    public $js = ['js/delete-multiple-newsletters.js'];

    public function init()
    {
        parent::init();

        // /!\ CSS/LESS development only setting /!\
        // Touch the asset folder with the highest mtime of all contained files
        // This will create a new folder in web/assets for every change and request
        // made to the app assets.
        if (getenv('APP_ASSET_FORCE_PUBLISH')) {
            $path = \Yii::getAlias($this->sourcePath);
            $files = FileHelper::findFiles($path);
            $mtimes = [];
            foreach ($files as $file) {
                $mtimes[] = filemtime($file);
            }
            touch($path, max($mtimes));
        }
    }
}
