/*
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by Nikola on 2/28/2017.
 */
$('#modalForm').on('shown.bs.modal', function(e) {
    var modal = $(this),
        url = $(e.relatedTarget).data('url');
    //if (url) {
        $('body').addClass('kv-grid-loading');
        $.ajax({
            cache: false,
            type: 'GET',
            url: url,
            success: function (data) {
                modal.find('.modal-body').html(data);
                $('body').removeClass('kv-grid-loading');
                formSubmitEvent();
                // For view modal form attach event to delete button
                deleteButtonEvent();
            }
        });
    //}
});
$('#modalForm').on('hidden.bs.modal', function(event) {
    var modal = $(this);
    modal.find('.modal-body').html('');
});
$(document).on('ready pjax:success', function () {
    deleteButtonEvent();
});
$('#edit-multiple').on('hidden.bs.modal', function() {
    // Reset form with fields to be multiple updated
    var form = $(this).closest('form');
    $(this).find('form')[0].reset();
    form.find('input[name="id[]"]').remove();
});
$('#edit-multiple').on('shown.bs.modal', function() {
    // Disable button if no entries are selected
    var keys=$("#" + gridViewKey + "-grid").yiiGridView('getSelectedRows');
    if (keys.length == 0) {
        $('#submit-multiple').prop('disabled', true);
    } else {
        $('#submit-multiple').prop('disabled', false);
    }
});
deleteButtonEvent = function() {
    $('.ajaxDelete').on('click', function (e) {
        e.preventDefault();
        var deleteUrl = $(this).attr('data-url'),
            pjaxContainer = gridViewKey + '-pjax-container',
            dialog = confirm('Are you sure you want to delete this item?'),
            pjaxGrid = gridViewKey + '-grid-container';
        if (dialog == true) {
            if ($('#modalForm').is(':visible')) {
                $('body').addClass('kv-grid-loading');
            } else {
                // Add loading class, it will be removed on end of pjax
                $('#' + pjaxGrid).addClass('kv-grid-loading');
            }
            $.ajax({
                url: deleteUrl,
                type: 'post',
            }).done(function (data) {
                if ($('#modalForm').is(':visible')) {
                    $('#modalForm').modal('hide');
                    $('body').removeClass('kv-grid-loading');
                }
                $.pjax.reload({container: '#' + $.trim(pjaxContainer)});
            });
        };
    });
}
formSubmitEvent = function() {
    $('#' + gridViewKey.charAt(0).toUpperCase() + gridViewKey.slice(1)).on('beforeSubmit',function(e){
        var form = $(this);
        $('body').addClass('kv-grid-loading');
        $.post(
            form.attr("action"),
            form.serialize()
            )
        .done(function (data) {
            $('#edit-multiple').modal('hide');
            $('#modalFormMultiple').modal('hide');
            $('#modalForm').modal('hide');
            $('body').removeClass('kv-grid-loading');
            $.pjax.reload({container:"#" + gridViewKey + "-pjax-container"});
        })
        return false;
    });
    $('#' + gridViewKey.charAt(0).toUpperCase() + gridViewKey.slice(1)).on('submit',function(e){
        e.preventDefault();
    });
};
