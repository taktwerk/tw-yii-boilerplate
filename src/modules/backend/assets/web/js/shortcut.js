$(document).ready(function (event) {
    var formSubmitViaShortcut = function(event, submit_type){
        event.preventDefault();
        var active_li = $('ul.nav li.active');
        if(active_li.prev().length){
            var active_tab_id = active_li.find('a').attr('href');
            $('button[name="submit-default"]').attr('name', submit_type);
            $(active_tab_id).find('form').submit();
        }else{
            var active_tab_id = active_li.find('a').attr('href');
            $('button[name="submit-default"]').attr('name', submit_type);
            $(active_tab_id).closest("form").submit();
        }
    };

    $(document).on('keydown', function(event){
        if((event.ctrlKey || event.metaKey) && event.shiftKey &&  event.code == 'KeyS'){
            formSubmitViaShortcut(event, 'submit-close');
        }

        if(event.key=='Escape' || event.keyCode == 27){
            event.preventDefault();
            var cancel_url = $('.cancel-form-btn').attr("href");
            if(cancel_url){
                setTimeout(function(){document.location.href = cancel_url},100);
            }
        }

        if((event.ctrlKey || event.metaKey) && !event.shiftKey && event.code == 'KeyS') {
            window[self.selector + 'changed'] = false;
            formSubmitViaShortcut(event, 'submit-default');
        }

        if((event.ctrlKey || event.metaKey) && !event.shiftKey && event.key == 'y') {
            event.preventDefault();
            var active_li = $('ul.nav li.active');
            if(active_li.prev().length){
                var active_tab_id = active_li.find('a').attr('href');
                window.location.href = $(active_tab_id).find('a').attr('href');
            }else{
                var create_link = $('a.create-new')[0];
                if(create_link){
                    create_link.click();
                }else{
                    var create_url = window.location.pathname;
                    create_url = create_url.replace('update','create');
                    window.location.href = create_url;
                }
            }
        }

        if((event.ctrlKey || event.metaKey) && event.shiftKey && event.key == 'Y') {
            formSubmitViaShortcut(event, 'submit-new');
        }
    });
});