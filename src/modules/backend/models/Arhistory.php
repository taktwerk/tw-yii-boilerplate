<?php

namespace taktwerk\yiiboilerplate\modules\backend\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\backend\models\base\Arhistory as BaseArhistory;

/**
 * This is the model class for table "arhistory".
 */
class Arhistory extends BaseArhistory
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

    public static function hideTables(){
        return [
            'user_ui_data'
        ];
    }

}
