<?php

namespace taktwerk\yiiboilerplate\modules\backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\backend\models\Arhistory as ArhistoryModel;

/**
 * Arhistory represents the model behind the search form about `taktwerk\yiiboilerplate\modules\backend\models\Arhistory`.
 */
class Arhistory extends ArhistoryModel{

    use SearchTrait;
    /**
     * @inheritdoc
     */
    protected $defaultOrder = ['created_at' => SORT_DESC];
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'table_name',
                    'row_id',
                    'event',
                    'created_at',
                    'created_by',
                    'field_name',
                    'old_value',
                    'new_value',
                ],
                'safe'
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ArhistoryModel::find();
        $this->parseSearchParams(ArhistoryModel::class, $params);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(ArhistoryModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(ArhistoryModel::class),
                'params' => [
                    'page' => $this->parsePageParams(ArhistoryModel::class),
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('row_id', $query);
        $this->applyHashOperator('event', $query);
        $this->applyDateOperator('created_at', $query, true,true);
        $this->applyHashOperator('created_by', $query);
        $this->applyLikeOperator('table_name', $query);
        $this->applyLikeOperator('field_name', $query);
        $this->applyLikeOperator('old_value', $query);
        $this->applyLikeOperator('new_value', $query);
        //echo $query->createCommand()->rawSql;exit;
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(ArhistoryModel::class);
    }
}
