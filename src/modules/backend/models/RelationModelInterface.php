<?php


namespace taktwerk\yiiboilerplate\modules\backend\models;


use yii\web\Controller;

interface RelationModelInterface
{
    /**
     * Get related CRUD controller's class
     * @return Controller
     */
    public function getControllerInstance();
}