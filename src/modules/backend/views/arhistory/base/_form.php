<?php
//Generation Date: 14-Sep-2020 12:43:18pm
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\widget\form\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\components\ClassDispenser;
/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\backend\models\Arhistory $model
 * @var taktwerk\yiiboilerplate\widget\form\ActiveForm $form
 * @var boolean $useModal
 * @var boolean $multiple
 * @var array $pk
 * @var array $show
 * @var string $action
 * @var string $owner
 * @var array $languageCodes
 * @var boolean $relatedForm to know if this view is called from ajax related-form action. Since this is passing from
 * select2 (owner) it is always inherit from main form
 * @var string $relatedType type of related form, like above always passing from main form
 * @var string $owner string that representing id of select2 from which related-form action been called. Since we in
 * each new form appending "_related" for next opened form, it is always unique. In main form it is always ID without
 * anything appended
 */

$owner = $relatedForm ? '_' . $owner . '_related' : '';
$relatedTypeForm = Yii::$app->request->get('relatedType')?:$relatedTypeForm;

if (!isset($multiple)) {
$multiple = false;
}

if (!isset($relatedForm)) {
$relatedForm = false;
}

$tableHint = (!empty(taktwerk\yiiboilerplate\modules\backend\models\Arhistory::tableHint()) ? '<div class="table-hint">' . taktwerk\yiiboilerplate\modules\backend\models\Arhistory::tableHint() . '</div><hr />' : '<br />');

?>
<div class="arhistory-form">
        <?php  $formId = 'Arhistory' . ($ajax || $useModal ? '_ajax_' . $owner : ''); ?>    
    <?php $form = ActiveForm::begin([
        'id' => $formId,
        'layout' => 'default',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'action' => $useModal ? $action : '',
        'options' => [
        'name' => 'Arhistory',
            ],
    ]); ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>
        <?php echo $tableHint; ?>

        <?php if ($multiple) : ?>
            <?=Html::hiddenInput('update-multiple', true)?>
            <?php foreach ($pk as $id) :?>
                <?=Html::hiddenInput('pk[]', $id)?>
            <?php endforeach;?>
        <?php endif;?>

        <?php $fieldColumns = [
                
            'table_name' => 
            $form->field(
                $model,
                'table_name',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'table_name') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('table_name'),
                            'id' => Html::getInputId($model, 'table_name') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('table_name')),
            'row_id' => 
            $form->field($model, 'row_id'),
            'event' => 
            $form->field(
                $model,
                'event',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'event') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'type' => 'number',
                            'step' => '1',
                            'placeholder' => $model->getAttributePlaceholder('event'),
                            
                            'id' => Html::getInputId($model, 'event') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('event')),
            'field_name' => 
            $form->field(
                $model,
                'field_name',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'field_name') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('field_name'),
                            'id' => Html::getInputId($model, 'field_name') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('field_name')),
            'old_value' => 
            $form->field(
                $model,
                'old_value',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'old_value') . $owner
                    ]
                ]
            )
                    ->textarea(
                        [
                            'rows' => 6,
                            'placeholder' => $model->getAttributePlaceholder('old_value'),
                            'id' => Html::getInputId($model, 'old_value') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('old_value')),
            'new_value' => 
            $form->field(
                $model,
                'new_value',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'new_value') . $owner
                    ]
                ]
            )
                    ->textarea(
                        [
                            'rows' => 6,
                            'placeholder' => $model->getAttributePlaceholder('new_value'),
                            'id' => Html::getInputId($model, 'new_value') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('new_value')),]; ?>        <div class='clearfix'></div>
<?php $twoColumnsForm = true; ?>
<?php 
// form fields overwrite
$fieldColumns = Yii::$app->controller->crudColumnsOverwrite($fieldColumns, 'form', $model, $form, $owner);

foreach($fieldColumns as $attribute => $fieldColumn) {
            if (count($model->primaryKey())>1 && in_array($attribute, $model->primaryKey()) && $model->checkIfHidden($attribute)) {
            echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
            echo $fieldColumn;
            echo $twoColumnsForm ? '</div>' : '';
        } elseif ($model->checkIfHidden($attribute)) {
            echo $fieldColumn;
        } elseif (!$multiple || ($multiple && isset($show[$attribute]))) {
            if ((isset($show[$attribute]) && $show[$attribute]) || !isset($show[$attribute])) {
                echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
                echo $fieldColumn;
                echo $twoColumnsForm ? '</div>' : '';
            } else {
                echo $fieldColumn;
            }
        }
                
} ?><div class='clearfix'></div>

                <?php $this->endBlock(); ?>
        
        <?= ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_TAB) ?
            Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                    'label' => Yii::t('twbp', 'Arhistory'),
                    'content' => $this->blocks['main'],
                    'active' => true,
                ],
                ]
            ])
            : $this->blocks['main']
        ?>        
        <div class="col-md-12">
        <hr/>
        
        </div>
        
        <div class="clearfix"></div>
        <?php echo $form->errorSummary($model); ?>
        <?php echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::formButtons($model, $useModal, $relatedForm, $multiple, "twbp", ['id' => $model->id]);?>
        <?php ClassDispenser::getMappedClass(ActiveForm::class)::end(); ?>
        <?= ($relatedForm && $relatedType == ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL) ?
        '<div class="clearfix"></div>' :
        ''
        ?>
        <div class="clearfix"></div>
    </div>
</div>

<?php
if ($useModal) {
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::class)::register($this);
}
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
if(!isset($useDependentFieldsJs) || $useDependentFieldsJs==true){
$this->render('@taktwerk-views/_dependent-fields', ['model' => $model,'formId'=>$formId]);
}
?>