<?php

use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\widget\form\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\modules\backend\models\GeneralSetting;

/**
 * @var yii\web\View $this
 * @var app\models\GeneralSetting $model
 * @var taktwerk\yiiboilerplate\widget\form\ActiveForm $form
 * @var boolean $useModal
 * @var boolean $multiple
 * @var array $pk
 * @var array $show
 * @var string $action
 * @var string $owner
 * @var array $languageCodes
 * @var boolean $relatedForm to know if this view is called from ajax related-form action. Since this is passing from
 * select2 (owner) it is always inherit from main form
 * @var string $relatedType type of related form, like above always passing from main form
 * @var string $owner string that representing id of select2 from which related-form action been called. Since we in
 * each new form appending "_related" for next opened form, it is always unique. In main form it is always ID without
 * anything appended
 */

$owner = $relatedForm ? '_' . $owner . '_related' : '';
$relatedTypeForm = Yii::$app->request->get('relatedType')?:$relatedTypeForm;

if (!isset($multiple)) {
$multiple = false;
}

if (!isset($relatedForm)) {
$relatedForm = false;
}

$tableHint = (!empty(GeneralSetting::tableHint()) ? '<div class="table-hint">' . GeneralSetting::tableHint() . '</div><hr />' : '<br />');

?>
<div class="general-setting-form">
        <?php $form = ActiveForm::begin([
        'id' => 'GeneralSetting' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
        'layout' => 'default',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'action' => $useModal ? $action : '',
        'options' => [
        'name' => 'GeneralSetting',
            ],
    ]); ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>
        <?php echo $tableHint; ?>

        <?php if ($multiple) : ?>
            <?=Html::hiddenInput('update-multiple', true)?>
            <?php foreach ($pk as $id) :?>
                <?=Html::hiddenInput('pk[]', $id)?>
            <?php endforeach;?>
        <?php endif;?>

        <?php $fieldColumns = [
                
            'client_id' => 
            $form->field($model, 'client_id'),
            'sla_plan' => 
            $form->field(
                $model,
                'sla_plan',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'sla_plan') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('sla_plan'),
                            'id' => Html::getInputId($model, 'sla_plan') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('sla_plan')),]; ?>        <div class='clearfix'></div>
<?php $twoColumnsForm = true; ?><?php 
// form fields overwrite
//$fieldColumns = Yii::$app->controller->crudColumnsOverwrite($fieldColumns, 'form', $model, $form);

foreach($fieldColumns as $attribute => $fieldColumn) {
    if (!$multiple || ($multiple && isset($show[$attribute]))) {
        if ((isset($show[$attribute]) && $show[$attribute]) || !isset($show[$attribute])) {
            echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
            echo $fieldColumn;
            echo $twoColumnsForm ? '</div>' : '';
        }else{
            echo $fieldColumn;
        }
    }
}
?>
<div class='clearfix'></div>

                <?php $this->endBlock(); ?>
        
        <?= ($relatedType != RelatedForms::TYPE_TAB) ?
            Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                    'label' => Yii::t('twbp', 'General Setting'),
                    'content' => $this->blocks['main'],
                    'active' => true,
                ],
                ]
            ])
            : $this->blocks['main']
        ?>        
        <div class="col-md-12">
        <hr/>
        
        </div>
        
        <div class="clearfix"></div>
        <?php echo $form->errorSummary($model); ?>
        <div class="col-md-12"<?=!$relatedForm ? ' id="main-submit-buttons"' : ''?>>
        <?php if(Yii::$app->controller->crudMainButtons['createsave']): ?>
            <?= Html::submitButton(
            '<span class="glyphicon glyphicon-check"></span> ' .
            ($model->isNewRecord && !$multiple ?
                Yii::t('twbp', 'Create') :
                Yii::t('twbp', 'Save')),
            [
                'id' => 'save-' . $model->formName(),
                'class' => 'btn btn-success',
                'name' => 'submit-default'
            ]
            );
            ?>
        <?php endif; ?>

        <?php if ((!$relatedForm && !$useModal) && !$multiple) { ?>
            <?php if (Yii::$app->getUser()->can(Yii::$app->controller->module->id .
            '_' . Yii::$app->controller->id . '_create') ): ?>
                
            <?php endif; ?>
        
        <?php } elseif ($multiple) { ?>
        <?= Html::a(
        '<span class="glyphicon glyphicon-exit"></span> ' .
        Yii::t('twbp', 'Close'),
        $useModal ? false : [],
        [
            'id' => 'save-' . $model->formName(),
            'class' => 'btn btn-danger' . ($useModal ? ' closeMultiple' : ''),
            'name' => 'close'
        ]
        );
        ?>
        <?php } else { ?>
        <?= Html::a(
        '<span class="glyphicon glyphicon-exit"></span> ' .
        Yii::t('twbp', 'Close'),
        ['#'],
        [
            'class' => 'btn btn-danger',
            'data-dismiss' => 'modal',
            'name' => 'close'
        ]
        );
        ?>
        <?php } ?>
        </div>
        <?php ActiveForm::end(); ?>
        <?= ($relatedForm && $relatedType == \taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms::TYPE_MODAL) ?
        '<div class="clearfix"></div>' :
        ''
        ?>
        <div class="clearfix"></div>
    </div>
</div>
<?php
if ($useModal) {
\taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::register($this);
}
\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::register($this);
?>