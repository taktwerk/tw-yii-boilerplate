<?php
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use yii\web\View;
$this->title = Yii::t('twbp', 'Schema Generator');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box"><div class="box-body">
<?php if (isset($errors)): ?>
    <?php if (!empty($errors)): ?>
        <div class="error-summary">
            <?php foreach ($errors as $table => $tableErrors): ?>
                <p><strong><?= $table ?> :</strong><br/>
                    <?= implode('<br>', $tableErrors) ?><br/>
                </p>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
<?php endif; ?>
<p><?= Yii::t('twbp', 'Paste your MySQL create schema statement to check it against the taktwerk conventions.'); ?></p>
<p><?= Yii::t('twbp', 'Be careful to start your create statements with'); ?><code>CREATE TABLE IF NOT EXISTS `name`
        (</code>.</p>
<p><?= Yii::t('twbp', 'To generate models, CRUD for a <b>module</b> make sure you add comment to the table in json with property <b>base_namespace</b> having value as the base namespace of the module e.g'); ?>
<pre style="color: #c7254e;">CREATE TABLE IF NOT EXISTS `register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='{"base_namespace":"app\\\\modules\\\\register"}';
</pre></p>


</div></div><div class="box"><div class="box-body">
<div class="branch-label-form">
<?php if(count(\Yii::$app->request->post())>0){?>
<?= Html::a('<span class="glyphicon glyphicon-arrow-left"></span> Go back',['/backend/schema-checker']);
?><br><br><?php }?>
    <?php $form = ActiveForm::begin([
        'fieldClass' => '\taktwerk\yiiboilerplate\widget\ActiveField',
        'id' => 'SchemaChecker',
        'layout' => 'horizontal',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'options' => [
            'enctype' => 'multipart/form-data'
        ],
    ]);
    ?><?php
    $isValidate = ($tables == null || \Yii::$app->request->post('regenerate'));
    
                $isRegenerate = \Yii::$app->request->post('regenerate');
                $isFileSubmit = \Yii::$app->request->post('submit_file');
                $isSysRegenerate = \Yii::$app->request->post('regenerate_sys');
                $isGenerated =  (key_exists('submit-model-crud', \Yii::$app->request->post()) || key_exists('submit-model', \Yii::$app->request->post()) || key_exists('submit-crud', \Yii::$app->request->post()));
               
                
   ?>

<?php echo Html::hiddenInput('system_tables',$isSysRegenerate);?>
<?php if($isRegenerate){?>
<?php echo Html::hiddenInput('regenerate',1);?>
<?php }?>
<?php if($isSysRegenerate){?>
<?php echo Html::hiddenInput('regenerate_sys',1);?>
<?php }?>
<?php if((!key_exists('schema',$_POST) && $isGenerated==false && $isSysRegenerate==false && $isRegenerate==false && $isFileSubmit==false) || !empty($errors)){?>
    <?= $form->field($model, 'sql_file')->widget(\kartik\file\FileInput::class, [
        'options'=>[
            'id'=>'dynamic-sql_file', 
        ],
        'pluginOptions' => [
            'msgPlaceholder'=>'Select the sql File',
            'showUpload' => false,
            'showRemove' => false,
            'showPreview' => false,
            'allowedFileExtensions' => ['sql']]])
        ->label('SQL File', ['class' => 'col-sm-12 text-left'])?>
	
	<?= Html::submitButton(
                '<span class="glyphicon glyphicon-check"></span> ' . Yii::t('twbp', 'Validate SQL'),
                [
                    'id' => 'save-schema-checker-sql',
                    'title'=>'Validate SQL File',
                    'class' => 'btn btn-success',
                    'name' =>'submit_file',
                    'value'=>1
                ]
            );
            ?>
<?php }?>
<?php if (empty($errors) && ! empty($tablesNamespace)) { ?>
<button type="button" class="btn btn-link" data-toggle="collapse" data-target="#demo-non-gen" title="Click to view tables which can't be generated from Schema Generator">View non-generatable CRUD tables</button>
<div id="demo-non-gen" class="collapse">
<div class="col-sm-12">
        <div class="check_ns_group">
        <label class="align-label">Non Generatable tables</label>
        	<div class=" column-3">      
        	<?php foreach($nonGeneratableTables as $tbl){?>   
            <div class="checkbox">
				<label class="control-label"> <?php echo $tbl?> </label>
			</div>
				<?php }?>
			</div>
		</div>
</div>

</div><?php }?>
    <?php if (empty($errors) && ! empty($tablesNamespace)) { ?>
    
			<div class="form-group">
				<label class="control-label col-sm-12 text-left"><?= Yii::t('twbp', 'Select tables that you want to generate models/CRUD for'); ?></label>
				<div class="col-sm-12">
            <?= Html::hiddenInput('schema', $schema) ?>
            <?php if(($isGenerated==true || $isSysRegenerate==true ||$isRegenerate==true || $isFileSubmit==true) && empty($errors)){?>
            <div class="">
            <?php echo Html::Checkbox(
                'selectall',
                false,
                [
                    'label' => Yii::t('twbp', 'Select all'),
                    'labelOptions'=>[
                        'class'=>'align-label'
                    ],
                    'id' => 'selectall-tbl-check',
                ]
            );
            ?>
            </div>
            <?php }?>
            <div class="checkbox-container">
            <div class="row">
        <?php foreach($tablesNamespace as $grp=>$tbls){?>
        
        <div class="col-sm-12">
        <div class="check_ns_group">
        <label class="align-label"><?php 
        echo Html::checkbox('',false,['class'=>'tbl-grp-parent-check']) ?>
        <?php
        if($grp){
            echo Yii::t('twbp',$grp);
        }
        else{
            echo 'Select this group ';
        } ?></label>
        <?php 
        if($grp==''){
        echo '<br><small style="display:block"><b>Models Path:</b> '.\Yii::$app->controllerMap['batch']['modelNamespace'].', ';
        echo '<b>Controller Path:</b> '.\Yii::$app->controllerMap['batch']['crudControllerNamespace'].', ';
        echo '<b>View Path:</b> '.\Yii::$app->controllerMap['batch']['crudViewPath'].'</small>';
        }
        ?> 
        <div class=" <?php if(count($tbls)>2){?>column-3<?php }?>">
        <?php foreach($tbls as $tbl){?>

            <div class="checkbox">
							<label class="control-label" for="tables[<?= $tbl ?>]">
                            <?= \yii\helpers\Html::checkbox('tables[' . $tbl . ']',
                                
                                @\Yii::$app->request->post('tables')[$tbl],
                                
                                ['id' => 'tables[' . $tbl . ']',
                                'class'=>'tbl-check']) ?>
                            <?= $tbl ?>
                        </label>
			</div><?php }?></div></div>
			</div>
			
<?php  }?>
</div>
 </div>
				</div>
			</div>
			
<?php } else if (empty($errors) && !empty($tables)) : ?>
        <div class="form-group">
            <label
                class="control-label col-sm-12 text-left"><?= Yii::t('twbp', 'Select tables that you want to generate'); ?></label>
            <div class="col-sm-12">
            <?= Html::hiddenInput('schema', $schema) ?>
            <div class="checkbox-container <?php if(count($tables)>2){?>column-3<?php }?>">
                <?php foreach ($tables as $table): ?>
                    <div class="checkbox">
                        <label class="control-label" for="tables[<?= $table ?>]">
                            <?= \yii\helpers\Html::checkbox(
                                'tables[' . $table . ']', @\Yii::$app->request->post('tables')[$table],
                                ['id' => 'tables[' . $table . ']','class'=>'tbl-check']) ?>
                            <?= $table ?>
                        </label>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php elseif (!empty($errors)) : ?>
        <div style="margin-top:15px">
            <div class="error-summary">
                <?php foreach ($errors as $table => $tableErrors): ?>
                    <p><strong><?= $table ?> :</strong><br/>
                        <?= implode('<br>', $tableErrors) ?><br/>
                    </p>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if($schema==false && $isGenerated==false && ($isSysRegenerate==false && $isRegenerate==false  && $isFileSubmit==false  || !empty($errors))){?>
	<div class="or-sep">or</div>
	<?php }?>
    <div class="form-group field-defaulttext-text required">
    
        <div class="col-sm-12">
        <div class="schema-generate-btn-sec">
            <?php
                $isValidate = ($tables == null || $_POST['regenerate']);
             ?>
            
            <?php if(($isGenerated || $isSysRegenerate || $isRegenerate || $isFileSubmit) && empty($errors)){?>
            <div class="">
            <?= Html::Checkbox(
                'regenerate_existing',
                \Yii::$app->request->post('regenerate_existing'),
                [
                    'label' => Yii::t('twbp', 'Regenerate existing non-base Model-CRUDs as well'),
                    'name' => 'regenerate_existing',
                    'title'=>Yii::t('twbp', 'Check this box to Regenerate existing non-base Model-CRUDs'),
                    'id'=> 'rengerate-exist-check',
                    'labelOptions'=>[
                        'class'=>'text-danger'
                    ]
                ]
            );
            ?><br>
            <?= Html::Checkbox(
                'just_show_command',
                \Yii::$app->request->post('just_show_command'),
                [
                    'label' => Yii::t('twbp', 'Only show the commands'),
                    'name' => 'just_show_command',
                    'title'=>Yii::t('twbp', 'Check this box to view the generation command(s)'),
                    'id'=> 'just-show-cmd-check',
                    'labelOptions'=>[
                        'class'=>'text-success'
                    ]
                ]
            );
            ?><br>
            <?= Html::Checkbox(
                'run_in_bg',
                \Yii::$app->request->post('run_in_bg'),
                [
                    'label' => Yii::t('twbp', 'Run in background'),
                    'name' => 'run_in_bg',
                    'title'=>Yii::t('twbp', 'Check this box to view the generation command(s)'),
                    'id'=> 'run-cmd-bg-check',
                    'labelOptions'=>[
                        'class'=>'text-info'
                    ]
                ]
            );
            ?>
            </div>
            
            <?php }?>
            <div class="">
            <?php  Html::Checkbox(
                'regenerate',
                \Yii::$app->request->post('regenerate'),
                [
                    'label' => Yii::t('twbp', 'Regenerate Model/CRUD for existing tables in database'),
                    'name' => 'regenerate',
                    'title'=>Yii::t('twbp', 'Check this box to regenerate Model/CRUD for existing tables in database'),
                    'id'=> 'rengerate-check'
                ]);
            ?>
            </div>
           <?php if(($isGenerated ==false && $isSysRegenerate==false && $isRegenerate==false) || $isFileSubmit==true  || !empty($errors)){ ?>
            <?php echo Html::label(Yii::t('twbp', 'Regenerate Model/CRUD for existing tables in database'))?>
            <br>
           <?= Html::submitButton(
                '<span class="glyphicon glyphicon-check"></span> ' . ($isValidate ? Yii::t('twbp', 'Show project tables') : Yii::t('twbp', 'Full Generate')),
                [
                    'id' => 'save-schema-checker',
                    'title'=>'Migrations + Models + CRUDs',
                    'class' => 'btn btn-info',
                    'name' => $isValidate ? 'regenerate' : 'submit-generate',
                    'value'=>'1',
                    'data-confirm' => $isValidate ? false : 'This will remove all migrations from migration folder. Are you sure you want to continue?',
                ]
            );
            ?>
            <?php if($isFileSubmit==false && !key_exists('schema',$_POST)){?>
            <?= Html::submitButton(
                '<span class="glyphicon glyphicon-check"></span> ' . ($isValidate ? Yii::t('twbp', 'Show system tables') : Yii::t('twbp', 'Full Generate System')),
                [
                    'id' => 'save-schema-checker-sys',
                    'title'=>'Migrations + Models + CRUDs',
                    'class' => 'btn btn-warning',
                    'name' => $isValidate ? 'regenerate_sys' : 'submit-generate-sys',
                    'value'=>'1',
                    'data-confirm' => $isValidate ? false : 'This will remove all migrations from migration folder. Are you sure you want to continue?',
                ]
            );
            ?>
            <?php }?>
            <?php }?>
            <?php if ($tables != null) : ?>
            
                <?= Html::submitButton(
                    '<span class="glyphicon glyphicon-check"></span> Generate Models + CRUDs',
                    [
                        'id' => 'save-schema-checker1',
                        'class' => 'btn btn-warning',
                        'name' => 'submit-model-crud'
                    ]
                );
                ?>
                <?= Html::submitButton(
                    '<span class="glyphicon glyphicon-check"></span> Generate Models',
                    [
                        'id' => 'save-schema-checker2',
                        'class' => 'btn btn-info',
                        'name' => 'submit-model'
                    ]
                );
                ?>
                <?= Html::submitButton(
                    '<span class="glyphicon glyphicon-check"></span> Generate CRUDs',
                    [
                        'id' => 'save-schema-checker3',
                        'class' => 'btn btn-primary',
                        'name' => 'submit-crud'
                    ]
                );
                ?>
                
            <?php endif; ?> </div>
        </div>
        <div class="col-sm-12" id="output-sect" <?php if (!$output) : ?>style="display:none" <?php endif;?>>
        <button class="btn btn-danger btn-output" type="button">Show/Hide Output Result</button>
            <div class="info-box sec-schema-output">
                <div class="info-box-content">
                    <p><?= Yii::t('twbp', 'Output result'); ?></p>
                    <code><?= nl2br($output);?></code>
                </div>
            </div>
        </div>
        <?php if ($tables != null) : ?>
            <br><br><br>
            <div class="col-sm-12">
                <div class="info-box">
                    <p>
                    <ul>
                        <li><strong>Full generate</strong> will generate migrations for selected tables, models and
                            CRUDs,
                            but also will
                            completely remove all files in migration folder, so intend of use this is only in beginning
                            of
                            project
                        </li>
                        <li><strong>Generate Models + CRUDs</strong> will generate models and CRUDs for selected tables,
                            but
                            only if there are
                            belonging tables in database created
                        </li>
                        <li><strong>Generate Models</strong> will generate models for selected tables, but only if there
                            are
                            belonging tables in
                            database created
                        </li>
                        <li><strong>Generate CRUDs</strong> will generate CRUDs for selected tables, plus missing
                            models,
                            but only if there are
                            belonging tables in database created
                        </li>
                    </ul>
                    </p>
                    <p>
                    <ul>
                        <li>Never add or change anything in base model file, unless you adding new property, but if we
                            follow this rules it could be done with generator. If need something to be changed do in
                            extended model. In template is added code for merging new rules with base model rules, just
                            uncomment to use it
                        </li>
                        <li>Never add or change anything in base controller file, always change or add actions in
                            extended
                            controller file. In template added code for merging access controls for additional actions.
                            Uncomment to use it
                        </li>
                        <li>Never change manually any view in base folder. If you want to customize it, copy it to
                            folder
                            above with same name, and that view will be used instead of base view. Base views are only
                            meant
                            to be generated by CRUD generator
                        </li>
                    </ul>
                    </p>
                </div>
            </div>
        <?php endif; ?>
    </div>
   
</div>
<?php ActiveForm::end(); ?>
</div>
</div></div>
<?php
$js = <<<JS
$('button.btn-output').click(function(){
    $('.sec-schema-output').slideToggle(400);
});
function setSqlfileInput(){
    var el = $('input#rengerate-check');    
    try{
    if(el.is(':checked'))
    {
        $('#dynamic-sql_file').fileinput('disable'); 
    }else{
        $('#dynamic-sql_file').fileinput('enable');
    }}catch(e){}
}
  $('input#rengerate-check').change(function(){
        setSqlfileInput();
   });setSqlfileInput();
$('input.tbl-grp-parent-check[type=checkbox]').change(function(){
    var el = $(this);
    if(el.is(":checked")){
        el.parents('.check_ns_group').find('input.tbl-check').prop('checked', true);
    }else{
        el.parents('.check_ns_group').find('input.tbl-check').prop('checked', false);
    }
    
});
$('#selectall-tbl-check').change(function(){
    var el = $(this);
    if(el.is(":checked")){
        $('#SchemaChecker .tbl-check, .tbl-grp-parent-check').prop('checked', true);
    }else{
        $('#SchemaChecker .tbl-check, .tbl-grp-parent-check').prop('checked', false);
    }
});
$('#SchemaChecker').submit(function(e){
    if($('#run-cmd-bg-check').prop('checked')){
        e.preventDefault();e.stopImmediatePropagation();
        var form  = $(this);
        var url = form.attr('action');
        var activeSubmitBtnName = $(this).find('button[type="submit"]:focus').prop('name');
        var data = form.serializeArray();
        var subBtnNames = $(this).find('button[type="submit"]').map(function(){return this.name;}).get();
        var formData = [];
        for(i=0;i<=data.length;i++){
            var item = data[i];
            if(item){
                if(subBtnNames.includes(item.name) && item.name!=activeSubmitBtnName){
                }else{
                    formData.push(item);
                }
           }
        }
        $('#output-sect').find('code').empty();
        $.ajax({
           type: "POST",
           url: url,
           data: formData,
           error:function(){
            alert('An error occurred while performing the operation');
           },
           success: function(data){
                var el = $('#output-sect');
                el.find('code').empty();
                if(data.output){
                    el.find('code').append(data.output.replace(/(\\r\\n|\\n\\r|\\r|\\n)/g, "<br>"));
                }
                setTimeout(function(){ $('#output-sect').slideDown(400); },500);
           }
         });
    }
});
JS;
$this->registerJs($js,View::POS_READY);
?>