<?php
$this->title = "Maintenance";
$tw_assets = \taktwerk\yiiboilerplate\assets\TwAsset::register($this);
\taktwerk\yiiboilerplate\components\Helper::registerWhiteLabelAssets();
Yii::$app->controller->layout = "@taktwerk-boilerplate/modules/user/views/layouts/main.php";
$css = <<<CSS
.skin-black .wrapper, .skin-black .main-sidebar, .skin-black .left-side {
	 background-color: unset; 
}
.message-box h1
{
    color: #252932;
    font-size: 25px;
    font-weight: 400;
    text-align: center;
    line-height: 40px;
}
.login-logo {
	height: 153px;
}
.center {
  padding: 15vh 0;
}
.bg-white{
background: #fff;
}
CSS;

$this->registerCss($css);
?>

<?= $this->render('@taktwerk/yiiboilerplate/modules/user/views/security/login', ['section' => '/maintenance/content']) ?>
