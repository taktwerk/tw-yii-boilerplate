<div class="error-template message-box">
    <h1><?=Yii::t('twbp','Temporarily down for maintenance');?></h1>
</div>
<div class="error-actions text-center">
    <a href="<?=Yii::$app->homeUrl;?>" class="btn btn-primary btn-lg">
        <span class="glyphicon glyphicon-home"></span> <?=Yii::t('twbp','Home')?>
    </a>
    <a href="<?= \yii\helpers\Url::to(['/user/security/logout']) ?>"
       class="btn btn-default btn-lg" data-method="post"><?='<span class="glyphicon glyphicon-log-out"></span> '.Yii::t('twbp', 'Logout')?>
    </a>
</div>