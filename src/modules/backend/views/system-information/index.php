<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Author: Eugine Terentev <eugine@terentev.net>
 * @var $this \yii\web\View
 * @var $provider \Probe\provider\ProviderInterface
 * @var $file_system_data array
 */
//use common\models\FileStorageItem;

$this->title = Yii::t('twbp', 'System Information');
$css = <<<CSS
.dl-horizontal dd {
	word-break: break-word;
}
.box td {
border: 1px solid #f4f4f4;
}
table {
    width: 730px;
    border: 1px solid #ccc;
    background: #fff;
    padding: 1px;
}

td, th {
    border: 1px solid #FFF;
    font-family: Verdana, sans-serif;
    font-size: 12px;
    padding:4px 8px;
}

h1 {
    font-family: "Trebuchet MS", Helvetica, sans-serif;
    font-size: 24px;
    margin: 10px;
}

h2 {
    font-family: "Trebuchet MS", Helvetica, sans-serif;
    font-size: 22px;
    color: #0B5FB4;
    text-align: left;
    margin: 25px auto 5px auto;
    width: 724px;
}

hr {
    background-color: #A9A9A9;
    color: #A9A9A9;
}

.e, .v, .vr {
    color: #333;
    font-family: Verdana, Helvetica, sans-serif;
    font-size: 11px;
}
.e {
    background-color: #eee;
}
.h {
    background-color: #0B5FB4;
    color: #fff;
}
.v {
    background-color: #F1F1F1;
    -ms-word-break: break-all;
    word-break: break-all;
    word-break: break-word;
    -webkit-hyphens: auto;
    -moz-hyphens: auto;
    hyphens: auto;
}
img {
    display:none;
}
CSS;

$this->registerCss($css);
$this->registerJs("window.paceOptions = { ajax: false }", \yii\web\View::POS_HEAD);
\yii\web\JqueryAsset::register($this);
\taktwerk\yiiboilerplate\modules\backend\assets\SystemInformationAsset::register($this);
?>
<div id="system-information-index">
    <div class="row">
        <div class="col-md-5">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-cog fa-spin"></i> <?php echo Yii::t('twbp', 'Processor') ?></h3>
                        </div>
                        <div class="box-body no-padding">
                            <table class="table table-hover">
                                <tbody>
                                <tr>
                                    <td><?php echo Yii::t('twbp', 'Processor') ?></td>
                                    <td><?php echo $provider->getCpuModel() ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo Yii::t('twbp', 'Processor Architecture') ?></td>
                                    <td><?php echo $provider->getArchitecture() ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo Yii::t('twbp', 'Number of cores') ?></td>
                                    <td><?php echo $provider->getCpuCores() ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-inbox"></i> <?php echo Yii::t('twbp', 'Operating System') ?></h3>
                        </div>
                        <div class="box-body no-padding">
                            <table class="table table-hover">
                                <tbody>
                                <tr>
                                    <td><?php echo Yii::t('twbp', 'OS') ?></td>
                                    <td><?php echo $provider->getOsType() ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo Yii::t('twbp', 'OS Release') ?></td>
                                    <td><?php echo $provider->getOsRelease() ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo Yii::t('twbp', 'Kernel version') ?></td>
                                    <td><?php echo $provider->getOsKernelVersion() ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-clock-o"></i> <?php echo Yii::t('twbp', 'Time') ?></h3>
                        </div>
                        <div class="box-body no-padding">
                            <table class="table table-hover">
                                <tbody>
                                <tr>
                                    <td><?php echo Yii::t('twbp', 'System Date') ?></td>
                                    <td><?php echo Yii::$app->formatter->asDate(time()) ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo Yii::t('twbp', 'System Time') ?></td>
                                    <td><?php echo Yii::$app->formatter->asTime(time()) ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo Yii::t('twbp', 'Timezone') ?></td>
                                    <td><?php echo date_default_timezone_get() ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-inbox"></i> <?php echo Yii::t('twbp', 'Software') ?></h3>
                        </div>
                        <div class="box-body no-padding table-responsive">
                            <table class="table table-hover">
                                <tbody>
                                <tr>
                                    <td><?php echo Yii::t('twbp', 'Web Server') ?></td>
                                    <td><?php echo $provider->getServerSoftware() ?></td>

                                </tr>
                                <tr>
                                    <td><?php echo Yii::t('twbp', 'PHP Version') ?></td>
                                    <td><?php echo $provider->getPhpVersion() ?></td>

                                </tr>
                                <tr>
                                    <td><?php echo Yii::t('twbp', 'DB Type') ?></td>
                                    <td><?php echo $provider->getDbType(Yii::$app->db->pdo) ?></td>

                                </tr>
                                <tr>
                                    <td><?php echo Yii::t('twbp', 'DB Version') ?></td>
                                    <td><?php echo $provider->getDbVersion(Yii::$app->db->pdo) ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-feed"></i> <?php echo Yii::t('twbp', 'Network') ?></h3>
                        </div>
                        <div class="box-body no-padding">
                            <table class="table table-hover">
                                <tbody>
                                <tr>
                                    <td><?php echo Yii::t('twbp', 'Hostname') ?></td>
                                    <td><?php echo $provider->getHostname() ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo Yii::t('twbp', 'Internal IP') ?></td>
                                    <td><?php echo $provider->getServerIP() ?></td>
                                </tr>
                                <tr>

                                    <td><?php echo Yii::t('twbp', 'External IP') ?></td>
                                    <td><?php echo $provider->getExternalIP() ?></td>

                                </tr>
                                <tr>
                                    <td><?php echo Yii::t('twbp', 'Port') ?></td>
                                    <td><?php echo $provider->getServerVariable('SERVER_PORT') ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-hdd-o"></i> <?php echo Yii::t('twbp', 'Memory') ?></h3>
                        </div>
                        <div class="box-body no-padding">
                            <table class="table table-hover">
                                <tbody>
                                <tr>
                                    <td><?php echo Yii::t('twbp', 'Total memory') ?></td>
                                    <td><?php echo Yii::$app->formatter->asShortSize($provider->getTotalMem()) ?></td>
                                </tr>
                                <tr>

                                    <td><?php echo Yii::t('twbp', 'Free memory') ?></td>
                                    <td><?php echo Yii::$app->formatter->asShortSize($provider->getFreeMem()) ?></td>
                                </tr>
                                <tr>

                                    <td><?php echo Yii::t('twbp', 'Total Swap') ?></td>
                                    <td><?php echo Yii::$app->formatter->asShortSize($provider->getTotalSwap()) ?></td>

                                </tr>
                                <tr>
                                    <td><?php echo Yii::t('twbp', 'Free Swap') ?></td>
                                    <td><?php echo Yii::$app->formatter->asShortSize($provider->getFreeSwap()) ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>
                                <?php echo $uptime ?>
                            </h3>
                            <p>
                                <?php echo Yii::t('twbp', 'Uptime') ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        <div class="small-box-footer">
                            &nbsp;
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3 id="load-average-html">
                                <?php echo $provider->getLoadAverage() ?>
                            </h3>
                            <p>
                                <?php echo Yii::t('twbp', 'Load Average') ?>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <div class="small-box-footer">
                            &nbsp;
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <?php if(isset($all_disks) && count($all_disks)>=1):?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">
                                    <i class="fa fa-bars"></i> <?php echo Yii::t('twbp', 'All Disks') ?></h3>
                            </div>
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tbody>
                                    <tr class="text-bold">
                                        <td>FileSystem</td>
                                        <td>Size</td>
                                        <td>Used</td>
                                        <td>Free</td>
                                        <td>Percent</td>
                                        <td>Mounted on</td>
                                        <td>Label</td>
                                    </tr>
                                    <?php foreach ($all_disks as $disk){?>
                                        <tr>
                                            <td><?php echo $disk['file'] ?></td>
                                            <td><?php echo $disk['size'] ?></td>
                                            <td><?php echo $disk['used'] ?></td>
                                            <td><?php echo $disk['free'] ?></td>
                                            <td>
                                                <div class="progress progress-xs progress-striped active">
                                                    <div class="progress-bar progress-bar-<?=$disk['bar']?>" style="width:<?php echo $disk['percent'] ?>">

                                                    </div>
                                                </div>
                                            </td>
                                            <td><?php echo $disk['mounted_on'] ?></td>
                                            <td>
                                            <span class="badge <?=$disk['class']?>">
                                                <?php echo $disk['percent'] ?>
                                            </span>
                                            </td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif;?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-folder-open-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><?=Yii::t("twbp",$file_system_data['fs']['path']." Usage")?></span>
                    <p><b><?=$file_system_data['fs']['used_by_path']?></b></p>
                    <div class="progress">
                        <div class="progress-bar" style="width: 100%"></div>
                    </div>
                    <span class="progress-description">
                            <?=Yii::t("twbp","Used by {storage}",[
                                'storage'=>$file_system_data['fs']['bucket']
                            ])?>
                        </span>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-folder-open-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><?=Yii::t("twbp",$file_system_data['fs_assetsprod']['path']." Usage")?></span>
                    <p><b><?=$file_system_data['fs_assetsprod']['used_by_path']?></b></p>
                    <div class="progress">
                        <div class="progress-bar" style="width: 100%"></div>
                    </div>
                    <span class="progress-description">
                            <?=Yii::t("twbp","Used by {storage}",[
                                'storage'=>$file_system_data['fs_assetsprod']['bucket']
                            ])?>
                        </span>
                </div>
            </div>
        </div>
        <?php if(\taktwerk\yiiboilerplate\components\Helper::isLocalFileSystem()){
            $file_system_data_fs = $file_system_data;
            ?>
            <div class="col-md-4">
                <div class="info-box bg-orange">
                    <span class="info-box-icon"><i class="fa fa-hdd-o"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text"><?=Yii::t("twbp","File system Usage")?> <div class="pull-right"><b><?=Yii::t("twbp","{percent}% Used",['percent'=>$file_system_data_fs['used_percentage']])?></b></div></span>
                        <?=Yii::t("twbp","Free").": ",$file_system_data_fs['free']?>
                        <p><?=Yii::t("twbp","Used").": ",$file_system_data_fs['used']?></p>
                        <div class="progress">
                            <div class="progress-bar" style="width: <?=$file_system_data_fs['used_percentage']?>%"></div>
                        </div>
                    </div>
                </div>
            </div>
        <?php }?>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div id="cpu-usage" class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">
                        <?php echo Yii::t('twbp', 'CPU Usage') ?>
                    </h3>
                    <div class="box-tools pull-right">
                        <?php echo Yii::t('twbp', 'Real time') ?>
                        <div class="realtime btn-group" data-toggle="btn-toggle">
                            <button type="button" class="btn btn-default btn-xs active" data-toggle="on">
                                <?php echo Yii::t('twbp', 'On') ?>
                            </button>
                            <button type="button" class="btn btn-default btn-xs" data-toggle="off">
                                <?php echo Yii::t('twbp', 'Off') ?>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart" style="height: 320px;">
                    </div>
                </div><!-- /.box-body-->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div id="memory-usage" class="box box-warning">
                <div class="box-header">
                    <h3 class="box-title">
                        <?php echo Yii::t('twbp', 'Memory Usage') ?>
                    </h3>
                    <div class="box-tools pull-right">
                        <?php echo Yii::t('twbp', 'Real time') ?>
                        <div class="btn-group realtime" data-toggle="btn-toggle">
                            <button type="button" class="btn btn-default btn-xs active" data-toggle="on">
                                <?php echo Yii::t('twbp', 'On') ?>
                            </button>
                            <button type="button" class="btn btn-default btn-xs" data-toggle="off">
                                <?php echo Yii::t('twbp', 'Off') ?>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart" style="height: 300px;">
                    </div>
                </div><!-- /.box-body-->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div id="load-average" class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">
                        <?php echo Yii::t('twbp', 'Load Average') ?>
                    </h3>
                    <div class="box-tools pull-right">
                        <?php echo Yii::t('twbp', 'Real time') ?>
                        <div class="btn-group realtime" data-toggle="btn-toggle">
                            <button type="button" class="btn btn-default btn-xs active" data-toggle="on">
                                <?php echo Yii::t('twbp', 'On') ?>
                            </button>
                            <button type="button" class="btn btn-default btn-xs" data-toggle="off">
                                <?php echo Yii::t('twbp', 'Off') ?>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart" style="height: 300px;">
                    </div>
                </div><!-- /.box-body-->
            </div>
        </div>
    </div>
    <div id="php-info" class="box box-primary collapsed-box">
        <div class="box-header">
            <h3 class="box-title">
                <?php echo Yii::t('twbp', 'PHP Info') ?>
            </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body" id="phpinfo">
            <?php
            ob_start();
            phpinfo();
            $phpInfo = ob_get_contents();
            ob_end_clean();
            echo preg_replace('%^.*<body>(.*)</body>.*$%ms', '$1', $phpInfo);
            ?>
        </div>
    </div>
</div>
