<?php
namespace _;

use taktwerk\yiiboilerplate\modules\backend\widgets\Menu;
use Yii;
use taktwerk\yiiboilerplate\components\Helper;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\components\ClassDispenser;

$helperClass = ClassDispenser::getMappedClass(Helper::class);

$translatorActive = (!Yii::$app->params['modules_activation'] || (Yii::$app->params['modules_activation'] &&
    (!key_exists('translatemanager',\Yii::$app->params['modules_activation'])
        || \Yii::$app->params['modules_activation']['translatemanager']==true)))
        && Yii::$app->hasModule('translatemanager') && (Yii::$app->user->can('TranslatorViewer') || Yii::$app->user->can('Authority'));

$adminMenuItems[] = [
    'label' => Yii::t('twbp', 'FAQ / Help'),
    'url' => [
        '/backend/faq'
    ],
    'icon' => 'fa fa-question',
    'visible' => Yii::$app->hasModule('faq') && (Yii::$app->user->can('FaqViewer') || Yii::$app->user->can('Authority')),
];

$general_modules = [
    [
        'label' => Yii::t('twbp', 'Feedback'),
        'icon' => 'fa fa-star',
        'url' => [
            '/feedback/feedback'
        ],
        'visible' => Yii::$app->hasModule('feedback') && (Yii::$app->user->can('FeedbackViewer') || Yii::$app->user->can('Authority')),
    ],[
        'label' => Yii::t('twbp', 'User Manager'),
        'icon' => 'fa fa-users',
        'url' => ['/usermanager/user'],
        'visible' => Yii::$app->hasModule('usermanager') && (Yii::$app->user->can('usermanager_user_index') || Yii::$app->user->can('UsermanagerViewer') || Yii::$app->user->can('Authority')),
    ],
    [
        'label' => \Yii::t('twbp', 'Translation Manager'),
        'url' => [
            '/translatemanager/translator'
        ],
        'icon' => 'fa fa-language',
        'visible' => $translatorActive,
    ],
    [
        'label' => Yii::t('twbp', 'FAQ Admin'),
        'url' => [
            '/faq'
        ],
        'icon' => 'fa fa-question',
        'visible' => Yii::$app->hasModule('faq') && (Yii::$app->user->can('FaqAdmin') || Yii::$app->user->can('Authority')),
    ],
    [
        'label' => Yii::t('twbp', 'Pages'),
        'icon' => 'fa fa-tree',
        'url' => [
            '/page/page'
        ],
        'visible' => Yii::$app->hasModule('page') && (Yii::$app->user->can('x_page_page_see') || Yii::$app->user->can('Authority')),
    ],
    [
        'label' => Yii::t('twbp', 'Guider'),
        'icon' => 'fa fa-arrows',
        'visible' => Yii::$app->hasModule('guide')
            && (Yii::$app->user->can('GuideViewer') || Yii::$app->user->can('Authority')),
        'items'=>[
            [
                'label' => Yii::t('twbp', 'Explore '),
                'url' => ['/guide/explore'],
                'template'=> '<a href="{url}" target="_blank">{icon}{label}</a>',
                'icon' => 'fa fa-bars',
            ],
            [
                'label' => Yii::t('twbp', 'Guides'),
                'url' => ['/guide/guide'],
                'icon' => 'fa fa-bars',
            ],
            [
                'label' => Yii::t('twbp', 'Guide Steps'),
                'url' => ['/guide/guide-step'],
                'icon' => 'fa fa-bars',
                'visible' => Yii::$app->user->can('Authority')
            ],
            [
                'label' => Yii::t('twbp', 'Guide Categories'),
                'url' => ['/guide/guide-category'],
                'icon' => 'fa fa-bars',
            ],
            [
                'label' => Yii::t('twbp', 'Guide Assets'),
                'url' => ['/guide/guide-asset'],
                'icon' => 'fa fa-bars',
            ],
            [
                'label' => Yii::t('twbp', 'Guide Templates'),
                'url' => ['/guide/guide-template'],
                'icon' => 'fa fa-bars',
                'visible' => Yii::$app->user->can('Authority')
            ],
            [
                'label' => Yii::t('twbp', 'Customer Devices'),
                'url' => ['/customer/client-device'],
                'icon' => 'fa fa-bars',
                'visible' => Yii::$app->user->can('customer_client-device_index')
            ],
            [
                'label' => Yii::t('twbp', 'Production Line'),
                'url' => ['/guide/production-line'],
                'icon' => 'fa fa-bars',
                'visible' => Yii::$app->user->can('Authority')
            ],[
                'label' => Yii::t('twbp', 'Production Line History'),
                'url' => ['/guide/production-line-history'],
                'icon' => 'fa fa-bars',
                'visible' => Yii::$app->user->can('Authority')
            ],
            [
                'label' => Yii::t('twbp', 'Guide View History'),
                'url' => ['/guide/guide-view-history'],
                'icon' => 'fa fa-bars',
                'visible' => Yii::$app->user->can('GuiderAdmin')
            ],
        ],
    ],
    [
        'label' => Yii::t('app', 'Protocol'),
        'icon' => 'fa fa-list',
        'visible' => Yii::$app->hasModule('protocol')
            && (Yii::$app->user->can('ProtocolViewer') || Yii::$app->user->can('Authority')),
        'items' => [
            [
                'label' => Yii::t('app', 'Protocols'),
                'url' => ['/protocol/protocol'],
                'icon' => 'fa fa-bars',
                'visible' => Yii::$app->user->can('ProtocolViewer')
            ],
            [
                'label' => Yii::t('app', 'Protocol Templates'),
                'url' => ['/protocol/protocol-template'],
                'icon' => 'fa fa-bars',
                'visible' => Yii::$app->user->can('ProtocolAdmin')
            ],
            [
                'label' => Yii::t('app', 'Workflows'),
                'url' => ['/workflow/workflow'],
                'icon' => 'fa fa-bars',
                'visible' => Yii::$app->user->can('ProtocolAdmin')
            ],
            [
                'label' => Yii::t('app', 'Workflow Steps'),
                'url' => ['/workflow/workflow-step'],
                'icon' => 'fa fa-bars',
                'visible' => Yii::$app->user->can('ProtocolAdmin')
            ],
            [
                'label' => Yii::t('app', 'Workflow Steps Status'),
                'url' => ['/workflow/workflow-step-status'],
                'icon' => 'fa fa-bars',
                'visible' => Yii::$app->user->can('ProtocolAdmin')
            ],
            [
                'label' => Yii::t('app', 'Workflow Transitions'),
                'url' => ['/workflow/workflow-transition'],
                'icon' => 'fa fa-bars',
                'visible' => Yii::$app->user->can('ProtocolAdmin')
            ],
        ],
    ],
    [
        'label' => Yii::t('twbp', 'Payment'),
        'url' => [
            '/#'
        ],
        'visible' => Yii::$app->hasModule('payment') && (Yii::$app->user->can('PaymentViewer') || Yii::$app->user->can('Authority')),
        'icon' => 'fa fa-money',
        'items' => [
            [
                'label' => Yii::t('twbp', 'Payment Log'),
                'url' => [
                    '/payment/payment-log'
                ],
                'visible'=>Yii::$app->user->can('payment_payment-log_index'),
                'icon' => 'fa fa-money'
            ],
            [
                'label' => Yii::t('twbp', 'Payment File'),
                'url' => [
                    '/payment/payment-file'
                ],
                'visible'=>Yii::$app->user->can('payment_payment-file_index'),
                'icon' => 'fa fa-money'
            ]
        ]
    ],
    [
        'label' => Yii::t('twbp', 'Newsletter'),
        'visible' => Yii::$app->hasModule('newsletter') && (Yii::$app->user->can('NewsletterViewer') || Yii::$app->user->can('Authority')),
        'icon' => 'fa fa-newspaper-o',
        'items' => [
            [
                'label' => Yii::t('twbp', 'Newsletters'),
                'url' => ['/newsletter/newsletter'],
                'icon' => 'fa fa-caret-right',
            ],
            [
                'label' => Yii::t('twbp', 'Newsletter Topics'),
                'url' => ['/newsletter/newsletter-topic'],
                'icon' => 'fa fa-caret-right',
            ],
            [
                'label' => Yii::t('twbp', 'Newsletter Recipients'),
                'url' => ['/newsletter/newsletter-recipient'],
                'icon' => 'fa fa-caret-right',
            ],
        ]
    ],
    [
        'icon' => 'fa fa-list',
        'label' => Yii::t('twbp', 'Dynamic Form'),
        'visible' => Yii::$app->hasModule('dynamicForm') && (Yii::$app->user->can('DynamicFormsViewer') || Yii::$app->user->can('Authority')),
        'items' => [
            [
                'label' => \Yii::t('twbp', 'Forms'),
                'url' => [
                    '/dynamicForm/dynamic-form'
                ],
                'icon' => 'fa fa-caret-right',
            ],
            [
                'label' => \Yii::t('twbp', 'Form Fields'),
                'url' => [
                    '/dynamicForm/dynamic-form-field'
                ],
                'icon' => 'fa fa-caret-right',
            ]
        ]
    ],
    [
        'label' => Yii::t('twbp', 'Reports'),
        'url' => [
            '/report/report'
        ],
        'visible' => Yii::$app->hasModule('report') && Yii::$app->user->can('ReportViewer'),
        'icon' => 'fa fa-files-o',
    ],
];

$adminMenuItems[] = [
    'label'=>Yii::t('twbp','General Modules'),
    'icon'=>'fa fa-cube',
    'items'=>$general_modules
];

$settings =  [
    'icon' => 'fa fa-gear',
    'label' => Yii::t('twbp', 'Settings'),
    'items' => array_merge([
        [
            'label' => \Yii::t('twbp', 'Global'),
            'url' => [
                '/setting'
            ],
            'icon' => 'fa fa-globe',
            'visible' => Yii::$app->user->can('backend_default_global-setting')
        ],
        [
            'label' => \Yii::t('twbp', 'General'),
            'url' => [
                '/setting/general-setting'
            ],
            'icon' => 'fa fa-user',
            'visible' => Yii::$app->user->can('x_setting_general-setting_see')
        ],
        [
            'label' => \Yii::t('twbp', 'Payment'),
            'url' => [
                '/payment/payment-setting'
            ],
            'icon' => 'fa fa-money',
            'visible' => Yii::$app->user->can('payment_payment-setting_index')
        ]
    ], $helperClass::getModulesMenuItems()),
    'options' => [
        'class' => 'treeview'
    ]
];

$adminMenuItems[] = $settings;

if(Yii::$app->user->can('Superadmin')){
    $superadmin_modules = [
        [
            'label' => Yii::t('twbp', 'System Information'),
            'url' => [
                '/backend/system-information'
            ],
            'icon' => 'fa fa-inbox'
        ],
        [
            'label' => Yii::t('twbp', 'Log Viewer'),
            'url' => [
                '/log-reader'
            ],
            'icon' => 'fa fa-warning',
            'visible' => Yii::$app->user->can('log-reader_default_index')
        ],
        [
            'label' => Yii::t('twbp', 'Queued Jobs'),
            'url' => [
                '/queue/queue-job'
            ],
            'icon' => 'fa fa-tasks',
            'visible' => Yii::$app->user->can('queue_queue-job_list')
        ],
        [
            'label' => Yii::t('twbp', 'History'),
            'url' => [
                '/backend/history'
            ],
            'icon' => 'fa fa-history',
            'visible' => Yii::$app->user->can('backend_history_index')
        ],
        [
            'label' => Yii::t('twbp', 'Filemanager'),
            'url' => [
                '/elfinder/manager'
            ],
            'icon' => 'fa fa-file'
        ],
        [
            'label' => Yii::t('twbp', 'Database'),
            'icon' => 'fa fa-database',
            'items'=>[
                [
                    'label' => Yii::t('twbp', 'Database Administration'),
                    'url' => [
                        '/adminer'
                    ],
                    'icon' => 'fa fa-caret-right',
                    'template' => '<a href="{url}" target="_blank">{icon}{label}</a>'
                ],
                [
                    'label' => Yii::t('twbp', 'DB Backup Manager'),
                    'url' => [
                        '/backup-manager'
                    ],
                    'icon' => 'fa fa-caret-right'
                ],
            ]
        ],
        [
            'label' => Yii::t('twbp', 'Terminal'),
            'icon' => 'fa fa-code',
            'items' => [
                [
                    'label' => Yii::t('twbp', 'Shell In A Box'),
                    'url' => [
                        '/webshell/index/shell-box'
                    ],
                    'icon' => 'fa fa-caret-right',
                    'visible' => Yii::$app->user->can('webshell_index_index')
                ],
                [
                    'label' => Yii::t('twbp', 'Webshell'),
                    'url' => [
                        '/webshell/index/index'
                    ],
                    'icon' => 'fa fa-caret-right',
                    'visible' => Yii::$app->user->can('webshell_index_index')
                ],
            ],
        ],
    ];
    $adminMenuItems[] = [
        'label'=>Yii::t('twbp','Superadmin Modules'),
        'icon'=>'fa fa-reorder',
        'items'=>$superadmin_modules
    ];
}

$developerMenuItems = [];
$modulesMenuItems = [];

// create developer menu, when user is admin
if (Yii::$app->user->identity && Yii::$app->user->can('Authority')) {

    $developerMenuItems = [
        [
            'label' => Yii::t('twbp', 'Users'),
            'url' => [
                '/user/admin'
            ],
            'icon' => 'fa fa-cube'
        ],
        [
            'label' => Yii::t('twbp', 'User Auth Logs'),
            'url' => [
                '/user/user-auth-log'
            ],
            'icon' => 'fa fa-lock'
        ],
        [
            'label' => Yii::t('twbp', 'Groups'),
            'url' => [
                '/#'
            ],
            'icon' => 'fa fa-users',
            'items' => [
                [
                    'label' => Yii::t('twbp', 'Groups List'),
                    'icon' => 'fa fa-group',
                    'url' => [
                        '/user/group'
                    ]
                ],
                [
                    'label' => Yii::t('twbp', 'User Groups'),
                    'icon' => 'fa fa-user',
                    'url' => [
                        '/user/user-group'
                    ]
                ],
                [
                    'label' => Yii::t('twbp', 'Group Emails'),
                    'icon' => 'fa fa-envelope',
                    'url' => [
                        '/user/group-email'
                    ]
                ],

            ]
        ],
        [
            'label' => Yii::t('twbp', 'Roles & Access'),
            'url' => [
                '/user/role'
            ],
            'icon' => 'fa fa-cube'
        ],
        [
            'label' => Yii::t('twbp', 'Settings'),
            'url' => [
                '/settings'
            ],
            'icon' => 'fa fa-cube'
        ],
        [
            'label' => Yii::t('twbp', 'Config Viewer'),
            'url' => [
                '/env'
            ],
            'icon' => 'fa fa-cube'
        ],
        [
            'label' => Yii::t('twbp', 'DB Backup Manager'),
            'url' => [
                '/backup-manager'
            ],
            'icon' => 'fa fa-cube'
        ],
        [
            'label' => Yii::t('twbp', 'Filemanager'),
            'url' => [
                '/elfinder/manager'
            ],
            'icon' => 'fa fa-cube'
        ],
        [
            'label' => Yii::t('twbp', 'Media'),
            'url' => [
                '/media/media-file'
            ],
            'icon' => 'fa fa-cube'
        ],
        [
            'label' => Yii::t('twbp', 'User Parameters'),
            'url' => [
                '/user-parameter/user-parameter'
            ],
            'icon' => 'fa fa-cube',
            'visible' => Yii::$app->hasModule('user-parameter')
        ],
        [
            'label' => Yii::t('twbp', 'Boilerplate Models'),
            'icon' => 'fa fa-cube',
            'items' => [
                [
                    'label' => Yii::t('twbp', 'Countries'),
                    'url' => [
                        '/country'
                    ],
                    'icon' => 'fa fa-cube'
                ]

            ]
        ],
        [
            'label' => Yii::t('twbp', 'Boilerplate Modules'),
            'url' => [
                '/#'
            ],
            'icon' => 'fa fa-cube',
            'items' => [
                [
                    'label' => Yii::t('twbp', 'Faq'),
                    'url' => [
                        '/#'
                    ],
                    'icon' => 'fa fa-cube',
                    'items' => [
                        [
                            'label' => Yii::t('twbp', 'FAQ Admin'),
                            'url' => [
                                '/faq'
                            ],
                            'icon' => 'fa fa-cube'
                        ],
                        [
                            'label' => Yii::t('twbp', 'FAQ Reader'),
                            'url' => [
                                '/backend/faq'
                            ],
                            'icon' => 'fa fa-cube'
                        ]
                    ]
                ],
                [
                    'label' => Yii::t('twbp', 'Feedback'),
                    'icon' => 'fa fa-star',
                    'url' => [
                        '/feedback/feedback'
                    ],
                    'visible' => Yii::$app->hasModule('feedback') && (Yii::$app->user->can('FeedbackViewer') || Yii::$app->user->can('Authority')),
                ],
                [
                    'label' => Yii::t('twbp', 'Sync Processes'),
                    'icon' => 'fa fa-hourglass-start',
                    'url' => [
                        '/sync/backend-sync'
                    ],
                    'visible' => Yii::$app->hasModule('sync')
                ],
                [
                    'label' => Yii::t('twbp', 'Clients'),
                    'url' => ['/customer/client'],
                    'icon' => 'fa fa-users',
                    'visible' => Yii::$app->hasModule('customer')
                ],
                [
                    'label' => Yii::t('twbp', 'User Manager'),
                    'icon' => 'fa fa-user',
                    'url' => [
                        '/usermanager/user'
                    ],
                    'visible' => Yii::$app->hasModule('usermanager')
                ],
                [
                    'label' => Yii::t('twbp', 'Pages'),
                    'icon' => 'fa fa-tree',
                    'url' => [
                        '/page/page'
                    ],
                    'visible' => Yii::$app->hasModule('page')
                ],
                [
                    'label' => Yii::t('twbp', 'Shares'),
                    'url' => [
                        '/#'
                    ],
                    'visible' => Yii::$app->hasModule('share'),
                    'icon' => 'fa fa-cube',
                    'items' => [
                        [
                            'label' => Yii::t('twbp', 'Shares'),
                            'url' => [
                                '/share/flysystem'
                            ],
                            'icon' => 'fa fa-cube'
                        ],
                        [
                            'label' => Yii::t('twbp', 'Shares to Roles'),
                            'url' => [
                                '/share/flysystem-role'
                            ],
                            'icon' => 'fa fa-cube'
                        ],
                        [
                            'label' => Yii::t('twbp', 'Shares to Users'),
                            'url' => [
                                '/share/flysystem-user'
                            ],
                            'icon' => 'fa fa-cube'
                        ],
                        [
                            'label' => Yii::t('twbp', 'Share Attributes'),
                            'url' => [
                                '/share/flysystem-attribute'
                            ],
                            'icon' => 'fa fa-cube'
                        ],
                        [
                            'label' => Yii::t('twbp', 'Fs Components'),
                            'url' => [
                                '/share/fs-component'
                            ],
                            'icon' => 'fa fa-cube'
                        ]
                    ]
                ],
                [
                    'label' => Yii::t('twbp', 'Importer'),
                    'url' => [
                        '/#'
                    ],
                    'visible' => Yii::$app->hasModule('import'),
                    'icon' => 'fa fa-cube',
                    
                    'items' => [
                        [
                            'label' => Yii::t('twbp', 'Import Processes'),
                            'url' => [
                                '/import/import-progress'
                            ],
                            'icon' => 'fa fa-cube'
                        ],
                        [
                            'label' => Yii::t('twbp', 'Import Logs'),
                            'url' => [
                                '/import'
                            ],
                            'icon' => 'fa fa-cube'
                        ]
                    ]
                ],
            ]
        ],
        [
            'label' => Yii::t('twbp', 'Generation'),
            'url' => [
                '/#'
            ],
            'icon' => 'fa fa-cube',
            'items' => [
                [
                    'label' => Yii::t('twbp', 'Translator'),
                    'url' => [
                        '/translatemanager'
                    ],
                    'visible' => Yii::$app->hasModule('translatemanager')
                    && (Yii::$app->user->can('TranslatorViewer') || Yii::$app->user->can('Authority')),
                    'icon' => 'fa fa-arrow-right',
                    'template' => '<a href="{url}" target="_blank">{icon}{label}</a>'
                ],
                [
                    'label' => Yii::t('twbp', 'Schema Builder'),
                    'url' => [
                        '/builder'
                    ],
                    'icon' => 'fa fa-cube',
                    'visible' => Yii::$app->hasModule('builder') && Yii::$app->getModule('builder')->checkAccess()
                ],
                [
                    'label' => Yii::t('twbp', 'Schema Generator'),
                    'url' => [
                        '/backend/schema-checker'
                    ],
                    'icon' => 'fa fa-cube',
                    'visible' => Yii::$app->hasModule('builder') && Yii::$app->getModule('builder')->checkAccess()
                ],
                [
                    'label' => Yii::t('twbp', 'Gii Generator'),
                    'url' => [
                        '/gii'
                    ],
                    'icon' => 'fa fa-arrow-right',
                    'template' => '<a href="{url}" target="_blank">{icon}{label}</a>',
                    'visible' => YII_ENV == 'dev' && Yii::$app->hasModule('gii') && Yii::$app->getModule('gii')->checkAccess()
                ]
            ]
        ],
        [
            'label' => Yii::t('twbp', 'Database Administration'),
            'url' => [
                '/adminer'
            ],
            'icon' => 'fa fa-arrow-right',
            'template' => '<a href="{url}" target="_blank">{icon}{label}</a>'
        ],
        [
            'label' => \Yii::t('twbp', 'Server Check'),
            'url' => ['/servercheck'],
            'icon' => 'fa fa-tasks',
            'template' => '<a href="{url}" target="_blank">{icon}{label}</a>'
        ],
        [
            'label' => Yii::t('twbp', 'Debug'),
            'url' => [
                '/debug'
            ],
            'icon' => 'fa fa-arrow-right',
            'visible' => getenv('YII_DEBUG'),
            'template' => '<a href="{url}" target="_blank">{icon}{label}</a>'
        ]
    ];
}
$extraMenu = [];
if (Yii::$app->user->identity && (Yii::$app->user->can('Authority'))) {
    $extraMenu = [

        [
            'label' => Yii::t('twbp', 'Translator'),
            'icon' => 'fa fa-cube',
            'items' => [
                [
                    'label' => \Yii::t('twbp', 'Translation Manager'),
                    'url' => [
                        '/translatemanager/translator'
                    ],
                    'icon' => 'fa fa-language',
                    'visible' =>$translatorActive
                ],
                [
                    'label' => \Yii::t('twbp', 'System Translator'),
                    'url' => [
                        '/translatemanager/translator/system?language_id=de'
                    ],
                    'icon' => 'fa fa-language',
                    'visible' =>$translatorActive
                ],
            ],
        ],
        [
            'label' => Yii::t('twbp', 'System Information'),
            'url' => [
                '/backend/system-information'
            ],
            'icon' => 'fa fa-cube'
        ],
        [
            'label' => Yii::t('twbp', 'History'),
            'url' => [
                '/backend/history'
            ],
            'icon' => 'fa fa-cube',
            'visible' => Yii::$app->user->can('backend_history_index')
        ],
        [
            'label' => Yii::t('twbp', 'Terminal'),
            'icon' => 'fa fa-cube',
            'items' => [
                [
                    'label' => Yii::t('twbp', 'Shell In A Box'),
                    'url' => [
                        '/webshell/index/shell-box'
                    ],
                    'icon' => 'fa fa-cube',
                    'visible' => Yii::$app->user->can('webshell_index_index')
                ],
                [
                    'label' => Yii::t('twbp', 'Webshell'),
                    'url' => [
                        '/webshell/index/index'
                    ],
                    'icon' => 'fa fa-cube',
                    'visible' => Yii::$app->user->can('webshell_index_index')
                ],
                [
                    'label' => Yii::t('twbp', 'Webconsole'),
                    'url' => [
                        '/webconsole'
                    ],
                    'icon' => 'fa fa-arrow-right',
                    'template' => '<a href="{url}" target="_blank">{icon}{label}</a>',
                    'visible' => Yii::$app->user->can('webconsole_default_index')
                ],

            ],
            'visible' => Yii::$app->user->can('Superadmin')
        ],
        [
            'label' => Yii::t('twbp', 'Log Viewer'),
            'url' => [
                '/log-reader'
            ],
            'icon' => 'fa fa-cube',
            'visible' => Yii::$app->user->can('log-reader_default_index')
        ],
        [
            'label' => Yii::t('twbp', 'Queued Jobs'),
            'url' => [
                '/queue/queue-job'
            ],
            'icon' => 'fa fa-cube',
            'visible' => Yii::$app->user->can('queue_queue-job_list')
        ]

    ];
}
$developerMenuItems = array_merge($developerMenuItems, $extraMenu);
if (count($developerMenuItems) >= 1) {
    $adminMenuItems[] = [
        'url' => '/#',
        'icon' => 'fa fa-cogs',
        'label' => Yii::t('twbp', 'Developer'),
        'items' => $developerMenuItems,
        'options' => [
            'class' => 'treeview'
        ]
        // 'visible' => Yii::$app->user->can('Authority'),
    ];
    // Helper::getModulesMenuItems();
}
echo Menu::widget([
    'options' => [
        'class' => 'sidebar-menu tree',
        'data-widget' => 'tree'
    ],
    'items' => ArrayHelper::merge(\dmstr\modules\pages\models\Tree::getMenuItems('backend', true), $adminMenuItems)
]);
