<?php
use dmstr\widgets\Alert;
use yii\helpers\Html;
use taktwerk\yiiboilerplate\components\Helper;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use taktwerk\yiiboilerplate\modules\user\models\UserGroup;
use taktwerk\yiiboilerplate\modules\user\models\Group;

/* @var $this \yii\web\View */
/* @var $content string */

$tw_assets = \taktwerk\yiiboilerplate\assets\TwAsset::register($this);
$helperClass = ClassDispenser::getMappedClass(Helper::class);

$helperClass::registerWhiteLabelAssets();

if (class_exists('\app\modules\backend\assets\AdminAsset')) {
    \app\modules\backend\assets\AdminAsset::register($this);
}

$this->render('@taktwerk-boilerplate/views/blocks/raven');
$user = \Yii::$app->user;
$userIdentity = $user->identity;
// Get the user roles
$roles = [];
foreach (Yii::$app->authManager->getRolesByUser($user->id) as $role) {
    $roles[] = $role->name;
}

$userGroups = (method_exists($userIdentity, 'getGroups')?$userIdentity->getGroups():
Group::find()->joinWith('userGroups')->andWhere(['user_group.user_id'=>$user->id]))->select('group.name')
->column();
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <?= Html::csrfMetaTags() ?>
        <title><?= Yii::t('app', getenv('APP_TITLE')) . ' - ' . Html::encode($this->title) ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Theme style -->
        <?php $this->head() ?>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="/js/html5shiv.js"></script>
        <script src="/js/respond.min.js"></script>
        <![endif]-->
        <style>
            .detail-view {
                table-layout: fixed;
            }
            .detail-view th {
                width: 20%;
            }
        </style>
    </head>

    <body
            class="hold-transition <?= $helperClass::getSkin(); ?> sidebar-mini">
    <?php $this->beginBody() ?>

    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->

            <a href="<?= Yii::$app->homeUrl ?>" class="logo logo-image">
        <span class="logo-lg">
&nbsp;
        </span>
                <span class="logo-mini">
        </span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only"><?=Yii::t('twbp', 'Toggle navigation')?></span>
                </a>
                <span class="navbar-text hidden-xs"><?= Yii::t('app', getenv('APP_TITLE')) ?></span>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <?php if (Yii::$app->params['hasFrontend']): ?>
                            <li><a href="/"><i
                                            class="fa fa-long-arrow-left"></i> <?= Yii::t('twbp', 'Back to frontend') ?>
                                </a></li>
                        <?php endif; ?>
                        <?php if (!$user->isGuest): ?>
                            <?php
                            if(\Yii::$app->hasModule('notification')){
                                echo \taktwerk\yiiboilerplate\modules\notification\widgets\NotificationWidget::widget();
                            }
                            ?>
                            <li class="dropdown tasks-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <?= strtoupper(Yii::$app->language) ?>
                                </a>
                                <ul class="dropdown-menu" style="width: 89px;min-width: 89px;">
                                    <li class="header"><?=Yii::t('twbp', 'Languages')?></li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <?php foreach (Yii::$app->urlManager->getLanguagesForDropdown() as $language): ?>
                                                <li>
                                                    <?= Html::a(
                                                        strtoupper($language['label']),
                                                        $language['url']
                                                    ) ?>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-user"></i>
                                    <span><?= $userIdentity->toString ?>
                                        <i class="caret"></i></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header bg-light-blue" style="height: unset;">
                                        <?php echo \taktwerk\yiiboilerplate\widget\Gravatar::widget(
                                            [
                                                'email' => !isset($userIdentity->profile->gravatar_email) || $userIdentity->profile->gravatar_email === null
                                                ? $userIdentity->email
                                                : $userIdentity->profile->gravatar_email,
                                                'options' => [
                                                    'alt' => $userIdentity->toString,
                                                    'style' => 'margin-right:10px;'
                                                ],
                                                'size' => 128,
                                                'width' => 55,
                                                'height' => 55,
                                                'image' => $userIdentity->profilePicture(['width' => 90, 'height' => 90]),
                                            ]
                                        ); ?>
                                        <p>
                                            <?= $userIdentity->toString ?>
                                            <small><?= $userIdentity->email ?></small>
                                        </p>
                                    </li>
                                    <li class="roles-header user-header bg-light-blue" style="height: unset;">
                                        <small  style="word-wrap: break-word;"><?=Yii::t('twbp', 'Role(s)') . ': ' . implode(', ', $roles)?></small>
                                    </li>
                                    <?php if(!empty($userGroups)){?>
                                    <li class="roles-header user-header bg-light-blue" style="height: unset;">
                                        <small  style="word-wrap: break-word;"><?=Yii::t('twbp', 'Group(s)') . ': ' . implode(', ', $userGroups)?></small>
                                    </li>
                                    <?php }?>
                                    <!-- Menu Footer-->
                                    <?php if ($userIdentity->isAdmin){ ?>
                                        <li class="user-body">
                                            <div class="row">
                                                <?php if ($user->can('user_settings_profile') && $userIdentity->profileUrl()): ?>

                                                    <div class="col-xs-6 text-center">
                                                        <a href="<?= $userIdentity->profileUrl() ?>"
                                                        ><?=Yii::t('twbp', 'Profile')?></a>

                                                    </div>
                                                <?php endif; ?>
                                                <?php if ($userIdentity->isAdmin){ ?>
                                                    <div class="col-xs-6 text-center">
                                                        <a href="#" data-toggle="modal"
                                                           data-target="#modalApiKey"><?=Yii::t('twbp', 'API Key')?></a>
                                                    </div>
                                                <?php }?>
                                            </div>
                                        </li>
                                    <?php }?>
                                    <!-- /.row -->

                                    <li class="user-footer">
                                        <div class="btn-group pull-left">

                                            <?php if(Yii::$app->hasModule('feedback') && $user->can('feedback_feedback_new-feedback')){ ?>
                                                <?php try {
                                                    echo \taktwerk\yiiboilerplate\modules\feedback\widgets\FeedbackButton::widget([
                                                        'btn_class'=>'btn btn-default btn-flat',
                                                        'icon'=>'<i class="glyphicon glyphicon-star-empty"></i>'
                                                    ]);
                                                } catch (Exception $e) {
                                                } ?>
                                            <?php }?>
                                        </div>

                                        <div class="btn-group pull-right">
                                            <?php
                                            $module = Yii::$app->getModule('user');
                                            if(Yii::$app->session->has($module->switchIdentitySessionKey)): ?>
                                                <?=Html::a(
                                                    '<span class="glyphicon glyphicon-chevron-left"></span> ' . Yii::t('twbp', 'Main User'),
                                                    ['/user/admin/switch-identity'], ['class' => 'btn btn-default btn-flat mb-2', 'data-method' => 'POST']
                                                ); ?>
                                            <?php else: ?>
                                                <a href="<?= \yii\helpers\Url::to(['/user/security/logout']) ?>"
                                                   class="btn btn-default btn-flat" data-method="post"><?='<span class="glyphicon glyphicon-log-out"></span> '.Yii::t('twbp', 'Logout')?></a>
                                            <?php endif; ?>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <?= $this->render('_sidebar') ?>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Right side column. Contains the navbar and content of the page -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <?php
                if(method_exists(Yii::$app->controller, 'renderCustomBlocks')){
                    Yii::$app->controller->renderCustomBlocks(Yii::$app->controller::POS_START);
                }
                ?>
                <h1>
                    <small><?= $this->title ?></small>
                </h1>
                <?=
                \yii\widgets\Breadcrumbs::widget([
                    'homeLink' => ['label' => Yii::t('twbp', 'Dashboard'), 'url' => ['/backend']],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </section>

            <!-- Main content -->

            <section class="content">
                <?= $helperClass::getMaintenanceContent()?>

                <div class="alert-wrapper"><?= Alert::widget() ?></div>
                <?= $content ?>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <strong>
                <?= $helperClass::version() ?></strong>
            &copy; <strong><a href="http://taktwerk.ch">taktwerk.ch</a></strong> | 2012-<?= date("Y", time()) ?>
            <?php
            if($userIdentity->isAdmin && YII_ENV_DEV) {
                $basePermission = Yii::$app->controller->module->id . '_' . Yii::$app->controller->id;
                echo '<i class="text-muted">ACL: ' . $basePermission . '</i>';
            }
            ?>
        </footer>
    </div>
    <!-- ./wrapper -->

    <?php $this->endBody() ?>
    <?php
    if ($userIdentity->isAdmin) {
        \yii\bootstrap\Modal::begin([
            'header' => '<h3>' . Yii::t('twbp', 'API Key') . '</h3>',
            'toggleButton' => false,
            'size' => \yii\bootstrap\Modal::SIZE_SMALL,
            'id' => 'modalApiKey',
        ]);
        echo $userIdentity->getAuthKey();
        \yii\bootstrap\Modal::end();
    }
    ?>
    <?php
    \yii\bootstrap\Modal::begin([
        'toggleButton' => false,
        'header' => Yii::t('twbp', 'Notification'),
        'size' => \yii\bootstrap\Modal::SIZE_DEFAULT,
        'id' => 'notificationModal',
    ]);
    \yii\bootstrap\Modal::end();
    ?>
    <?php
    \yii\bootstrap\Modal::begin([
        'toggleButton' => false,
        'size' => \yii\bootstrap\Modal::SIZE_LARGE,
        'id' => 'loadingModal',
        'closeButton' => false,
        //keeps from closing modal with esc key or by clicking out of the modal.
        // user must click cancel or X to close
        'clientOptions' => [
            'backdrop' => 'static',
            'keyboard' => false,
        ]
    ]);
    echo "<div id='modalContent'>
<i class='fa fa-circle-o-notch fa-spin fa-5x fa-fw'></i>
</div>";
    \yii\bootstrap\Modal::end();
    ?>
    </body>
    </html>
<?php
$headJsSideBar = <<<JS
    (function () {
        if (Boolean(sessionStorage.getItem('sidebar-toggle-collapsed'))) {
            var body = document.getElementsByTagName('body')[0];
            body.className = body.className + ' sidebar-collapse';
        }
    })();
JS;
$this->registerJs($headJsSideBar, \yii\web\View::POS_BEGIN);
$sideBarClickEvent = <<<JS
    // Click handler can be added latter, after jQuery is loaded...
    $('.sidebar-toggle').click(function(event) {
        event.preventDefault();
        if (Boolean(sessionStorage.getItem('sidebar-toggle-collapsed'))) {
            sessionStorage.setItem('sidebar-toggle-collapsed', '');
        } else {
            sessionStorage.setItem('sidebar-toggle-collapsed', '1');
        }
    });
JS;
if (Yii::$app->hasModule('feedback')) {
    echo Yii::$app->hasModule('feedback') ? taktwerk\yiiboilerplate\modules\feedback\widgets\FeedbackFormAssets::widget() : "";
}
$this->registerJs($sideBarClickEvent, \yii\web\View::POS_END);
$this->endPage();