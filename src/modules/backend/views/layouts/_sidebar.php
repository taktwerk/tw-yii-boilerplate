<?php

namespace _;

use Yii;

// This is the dashboard sidebar
$adminMenuItems = [];

$adminMenuItems[] = [
    'label' => Yii::t('twbp', 'Dashboard'),
    'url' => ['/backend'],
    'visible' => !\Yii::$app->user->isGuest,
    'icon' => 'fa fa-dashboard',
];

echo $this->render('_sidebar_admin', ['adminMenuItems' => $adminMenuItems]);
