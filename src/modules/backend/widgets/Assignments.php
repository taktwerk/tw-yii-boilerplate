<?php
namespace taktwerk\yiiboilerplate\modules\backend\widgets;

use Yii;
use Da\User\Widget\AssignmentsWidget as BaseAssignments;
use taktwerk\yiiboilerplate\modules\backend\models\Assignment;

class Assignments extends BaseAssignments
{
    /** @var integer ID of the user to whom auth items will be assigned. */
    public $userId;

    /** @inheritdoc */
    public function run()
    {
        $model = Yii::createObject([
            'class'   => Assignment::class,
            'user_id' => $this->userId,
        ]);

        if ($model->load(\Yii::$app->request->post())) {
            $model->updateAssignments();
        }

        return $this->render('@dektrium/rbac/widgets/views/form', [
            'model' => $model,
        ]);
    }
}