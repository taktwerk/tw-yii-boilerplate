<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Author: Eugine Terentev <eugine@terentev.net>
 */

namespace taktwerk\yiiboilerplate\modules\backend\controllers;

use Probe\ProviderFactory;
use taktwerk\yiiboilerplate\components\Helper;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Response;
use Yii;
use taktwerk\yiiboilerplate\components\ClassDispenser;

class SystemInformationController extends Controller
{
    /**
     * @return array|float|int|string
     */
    public function actionIndex()
    {

        Yii::$app->formatter->sizeFormatBase = 1000;
        $this->layout = "@taktwerk-backend-views/layouts/main";
        $provider = ProviderFactory::create();
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($key = Yii::$app->request->get('data')) {
                switch ($key) {
                    case 'cpu_usage':
                        return $this->getCpuUsage();
                        break;
                    case 'load_avg':
                        return $provider->getLoadAverage();
                        break;
                    case 'memory_usage':
                        return ($provider->getTotalMem() - $provider->getFreeMem()) / $provider->getTotalMem();
                        break;
                }
            }
        } else {

            $file_system_data = [];
            $all_disks = [];
            $used_array = [];
            $free_array = [];

            foreach (ClassDispenser::getMappedClass(Helper::class)::getDisks() as $key=>$disc){
                if($key>=1){
                    $disks = [];
                    $parts = explode("|",$disc);
                    if(!in_array($parts[0], $used_array) && !in_array($parts[1], $free_array)){
                        $file_system_data['used'] += $parts[0];
                        $file_system_data['free'] += $parts[1];
                        $disks['used'] = Yii::$app->formatter->asShortSize(($parts[0])*1024);
                        $disks['free'] = Yii::$app->formatter->asShortSize(($parts[1])*1024);
                        $disks['percent'] = $parts[2];
                        $disks['class'] = $disks['percent']>=90?"bg-red":"bg-green";
                        $disks['bar'] = $disks['percent']>=90?"danger":"success";
                        $disks['file'] = $parts[3];
                        $disks['mounted_on'] = $parts[4];
                        $disks['size'] =  Yii::$app->formatter->asShortSize(($parts[5])*1024);
                        $all_disks[] = $disks;
                        $used_array[]=$parts[0];
                        $free_array[]=$parts[1];
                    }
                }
            }

            $seconds = $provider->getUptime();
            $dt1 = new \DateTime("@0");
            $dt2 = new \DateTime("@$seconds");
            if ($seconds>=86400){
                $uptime = $dt1->diff($dt2)->format('%a days, %H:%I:%S');
            }else{
                $uptime = $dt1->diff($dt2)->format('%H:%I:%S');
            }

            $file_system_data['fs_assetsprod'] = $this->getBucketInfo('fs_assetsprod',$file_system_data['used'], $file_system_data['free']);
            $file_system_data['fs'] = $this->getBucketInfo('fs',$file_system_data['used'], $file_system_data['free']);

            $file_system_data['used_percentage'] = round(($file_system_data['used']*100)/($file_system_data['free']+$file_system_data['used']),2);

            $file_system_data['total'] = Yii::$app->formatter->asShortSize(($file_system_data['used']+$file_system_data['free'])*1024);
            $file_system_data['free'] = Yii::$app->formatter->asShortSize(($file_system_data['free'])*1024);
            $file_system_data['used'] = Yii::$app->formatter->asShortSize($file_system_data['used']*1024);

            return $this->render('index', [
                'provider' => $provider,
                'all_disks' => $all_disks,
                'file_system_data' => $file_system_data,
                'uptime' => $uptime
            ]);
        }
    }


    public function getCpuUsage($interval = 1)
    {
        $provider = ProviderFactory::create();
        if(empty($provider->getCpuCores()) && empty($provider->getCpuUsage()) ){
            $stat = function () {
                $stat = file_get_contents('/proc/stat');
                $stat = explode("\n", $stat);
                $result = [];
                foreach ($stat as $v) {
                    $v = explode(' ', $v);
                    if (isset($v[0])
                        && strpos(strtolower($v[0]), 'cpu') === 0
                        && preg_match('/cpu[\d]/sim', $v[0])
                    ) {
                        $result[] = array_slice($v, 1, 4);
                    }

                }
                return $result;
            };
            $stat1 = $stat();
            usleep($interval * 1000000);
            $stat2 = $stat();
            $usage = [];
            $cores = shell_exec('cat /proc/cpuinfo | grep processor | wc -l');
            for ($i = 0; $i < $cores; $i++) {
                if (isset($stat1[$i]) && isset($stat2[$i])) {
                    $total = array_sum($stat2[$i]) - array_sum($stat1[$i]);
                    $idle = $stat2[$i][3] - $stat1[$i][3];
                    $usage[$i] = $total !== 0 ? ($total - $idle) / $total : 0;
                }
            }
            return $usage;
        }else{
            return $provider->getCpuUsage();
        }
    }

    /**
     * @param string $bucket
     * @param $used
     * @param $free
     * @return array
     */
    public function getBucketInfo($bucket='fs', $used, $free){

        $file_system_data = [];

        if(ClassDispenser::getMappedClass(Helper::class)::isLocalFileSystem($bucket)){

            //get file system information for non-external file system
            $dir = realpath(Yii::$app->$bucket->path);
            $file_system_data['bucket'] = str_replace(Yii::getAlias('@root'),'@root',Yii::$app->$bucket->path);
            $file_system_data['bucket'] = str_replace(Yii::getAlias('@webroot'),'@webroot',$file_system_data['bucket']);
            $bytes_total = 0;
            if($dir!==false && $dir!='' && file_exists($dir)){
                foreach(new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir, \FilesystemIterator::SKIP_DOTS)) as $object){
                    $bytes_total += $object->getSize();
                }
            }
            $file_system_data['used_by_path'] = Yii::$app->formatter->asShortSize($bytes_total);

        }else{
            //get file system information for external file system
            $contents = Yii::$app->$bucket->listContents();
            $file_system_data['bucket'] = isset(Yii::$app->$bucket->bucket)?Yii::$app->$bucket->bucket:"";
            $file_system_data['used_by_path'] = 0;
            foreach ($contents as $object) {
                if ($object['type']=="file"){
                    $file_system_data['used_by_path'] = $object['size'];
                }
            }
            $file_system_data['used_by_path'] = Yii::$app->formatter->asShortSize($file_system_data['used_by_path']);
        }
        $file_system_data['path'] = $bucket;

        return $file_system_data;
    }

}
