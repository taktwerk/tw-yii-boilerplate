<?php

namespace taktwerk\yiiboilerplate\modules\backend\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;

/**
 * ArhistoryController implements the CRUD actions for Arhistory model.
 */
class HistoryController extends TwCrudController
{
}
