<?php

namespace taktwerk\yiiboilerplate\modules\backend\controllers;

use Yii;
use yii\bootstrap\BootstrapAsset;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Site controller.
 */
class MaintenanceController extends Controller
{    /**
 *
 * {@inheritdoc}
 */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index'
                        ]
                    ],
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        return $this->render('@taktwerk-boilerplate/modules/backend/views/maintenance/index');
    }

}
