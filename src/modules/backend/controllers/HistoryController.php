<?php

namespace taktwerk\yiiboilerplate\modules\backend\controllers;

use taktwerk\yiiboilerplate\helpers\CrudHelper;
use taktwerk\yiiboilerplate\modules\backend\models\Arhistory;
use taktwerk\yiiboilerplate\modules\user\models\User;
use taktwerk\yiiboilerplate\templates\migration\TwGenerator;
use yii\base\Module;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * This is the class for controller "ArhistoryController".
 */
class HistoryController extends \taktwerk\yiiboilerplate\modules\backend\controllers\base\HistoryController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\backend\models\Arhistory';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\backend\models\search\Arhistory';

    /**
     * Additional actions for controllers, uncomment to use them
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'summary',
                        ],
                        'roles' => ['@']
                    ]
                ]
            ]
        ]);
    }

    public function init()
    {
        $this->crudColumnsOverwrite = [
            'summary'=>[
                'id' => false,
                'after#old_value' => [
                    'label'=>'User',
                    'attribute'=>'created_by',
                    'value' => function($model){
                        $user = User::findOne($model->created_by);
                        if($user){
                            return $user->username;
                        }
                        return $model->created_by;
                    }
                ],
                'event' => [
                    'attribute'=>'event',
                    'value' => function($model){
                        $model_arhistory = \taktwerk\yiiboilerplate\models\ArHistory::findOne($model->id);
                        return $model_arhistory->getEventName();
                    }
                ],
                'row_id' => [
                    'attribute'=>'row_id',
                    'format'=>'raw',
                    'value' => function($model){
                        $generator = new TwGenerator();
                        $comment = $generator->getTableComment($model->table_name);
                        $comment = json_decode($comment, true);
                        if ($comment && is_array($comment)) {
                            if (isset($comment['base_namespace'])) {
                                $baseNamespace = '\\\\'.rtrim($comment['base_namespace'], '\\');
                                $modelNamespace = $baseNamespace. '\\\\models\\\\';
                                $modelNamespace = stripslashes($modelNamespace);
                                $modelName .= Inflector::camelize($model->table_name);
                                $controller .= Inflector::camel2id($model->table_name);
                                $modelNamespace .= $modelName;

                                $model_arhistory = $modelNamespace::findOne($model->row_id);
                                if($model_arhistory){

                                    if(stripos($baseNamespace, 'taktwerk')===false){
                                        $moduleDir = FileHelper::normalizePath(\Yii::getAlias('@' . str_replace('\\', '/', ltrim($baseNamespace, '\\'))));

                                        if (is_dir($moduleDir)) {
                                            $files = glob($moduleDir . '/*.{php}', GLOB_BRACE);
                                            foreach ($files as $file) {
                                                $mCName = ClassDispenser::getMappedClass(CrudHelper::class)::getClassFullNameFromFile($file);
                                                if ($mCName != null && is_subclass_of($mCName, Module::class)) {
                                                    $moduleClass = $mCName::getInstance();
                                                    $moduleId = $moduleClass->id;
                                                    if($moduleId){
                                                        $url = Url::toRoute(['/'.$moduleId.'/'.$controller.'/index?'.$modelName."[".$modelNamespace::primaryKey()[0].']='.$model->row_id]);
                                                        return Html::a($model_arhistory->toString(), $url,['target'=>'_blank', 'data-pjax'=>0]);
                                                    }
                                                }
                                            }
                                        }

                                    }

                                    return $model_arhistory->toString();
                                }
                            }
                        }
                        return $model->row_id;
                    }
                ],
                'field_name' => [
                    'attribute'=>'field_name',
                    'value' => function($model){
                        $model_arhistory = ArrayHelper::getColumn(Arhistory::find()
                            ->where([
                                'created_by'=>$model->created_by,
                                'created_at'=>$model->created_at,
                                'row_id'=>$model->row_id,
                                'table_name'=>$model->table_name,
                                'event'=>$model->event
                            ])
                            ->all(),'field_name');
                        return implode(', ', $model_arhistory);
                    }
                ],
                'old_value' => false,
                'new_value' => false
            ]
        ];
        return parent::init(); // TODO: Change the autogenerated stub
    }

    public function actionSummary()
    {
        $searchModel = $this->getSearchModel();
        /**@var $dataProvider ActiveDataProvider*/
        $dataProvider = $searchModel->search($_GET);

        /**@var $query ActiveQuery*/
        $query = $dataProvider->query;
        $query->select(['id','created_by','created_at','row_id','table_name','event']);
        $query->groupBy(['created_by','created_at','row_id','table_name','event']);
        $query->andWhere(['not in','table_name', Arhistory::hideTables()]);
        $query->orderBy(['id'=>SORT_DESC]);

        $dataProvider->query = $query;


        return $this->render('summary', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'useModal' => $this->useModal,
            'importer' => $this->importer
        ]);
    }
}
