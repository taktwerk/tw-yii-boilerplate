<?php

namespace taktwerk\yiiboilerplate\modules\backend\controllers;

use taktwerk\yiiboilerplate\helpers\DebugHelper;
use taktwerk\yiiboilerplate\models\ArHistory;
use taktwerk\yiiboilerplate\templates\menu\Generator;
use taktwerk\yiiboilerplate\templates\migration\TwGenerator;
use taktwerk\yiiboilerplate\TwMigration;
use Yii;
use yii\base\DynamicModel;
use yii\db\Query;
use yii\filters\AccessControl;
use Exception;
use yii\gii\CodeFile;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii\web\UploadedFile;
use taktwerk\yiiboilerplate\helpers\TableHelper;
use yii\db\ActiveRecord;
use taktwerk\yiiboilerplate\helpers\CrudHelper;
use iamcal\SQLParser;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use yii\helpers\StringHelper;
use yii\helpers\Json;

/**
 * Default backend controller.
 *
 * Usually renders a customized dashboard for logged in users
 */
class SchemaCheckerController extends DefaultController
{
    
    
    /**
     * @var array of tables needed for the migration
     */
    protected $tablesMigration = [];
    /**
     * @var array of tables namespaces
     */
    protected $tablesNamespace = [];
    /**
     * @var array of tables needed for the Crud
     */
    protected $tablesCrud = [];
    
    /**
     * @var array of errors
     */
    protected $errors = [];
    /**
     * @var array The list of common columns in every BP table
     */
    protected $commonColumns=['created_at','created_by','updated_at','updated_by','deleted_at','deleted_by'];
   /**
    * @var array The list of tables which doesn't follow BP conventions e.g not having a created_by column 
    */
    protected $errorTables =[];
    /**
     * @var array Default tables of Yii/boilerplate that should be ignored.
     */
    protected $defaultTables = [
       
    ];
    protected $showCommand = false;
    protected $nonGeneratableTables = [
        'audit_data',
        'audit_entry',
        'audit_error',
        'audit_javascript',
        'audit_mail',
        'audit_trail',
        'auth_assignment',
        'auth_item',
        'auth_item_child',
        'auth_rule',
        'cache',
        'dmstr_page',
        'dmstr_page_translation',
        'dmstr_page_translation_meta',
        'html',
        'language_source',
        'language_translate',
        'less',
        'migration',
        'settings',
        'twig',
        'social_account',
        'token'
    ];
    protected $tablesSuffixUnsensual = [
        'arhistory',
        'language',
        'profile',
        'research_profile',
        'user',
        'media_file',
        'protocol',
        'sync_index'
    ];
    protected $importedTables = [];
    public $regenerateNonBaseFiles = false;
    private function getTablesByModelDirPath($dirPath){
        $di = new \RecursiveDirectoryIterator($dirPath, \FilesystemIterator::KEY_AS_PATHNAME | \FilesystemIterator::CURRENT_AS_SELF);
        $tbls = [];
        foreach (new \RecursiveIteratorIterator($di) as $filename => $file) {
            if($file->isDot()){
                continue;
            }
            $path = $file->getPath();
            if(strpos($path, 'models') !== false &&
                strpos($path, 'models/base') === false &&
                  strpos($path, 'models/search') === false &&
                $file->getExtension()=='php')
            {
                try{
                    $modelName = ClassDispenser::getMappedClass(CrudHelper::class)::getClassFullNameFromFile($filename);
                    if ($modelName == null || ! is_subclass_of($modelName, ActiveRecord::class)) {
                        continue;
                    }
                    try {
                        $schema = $modelName::getTableSchema();
                    } catch (\yii\base\InvalidConfigException $e) {
                        continue;
                    }
                    if ($schema) {
                        $tblName = $schema->fullName;
                        $tbls[]=$tblName;
                    }
                }catch(\Exception $e){
                    continue;
                }
            }
        }
        return array_unique($tbls);
    }
    public function init(){
        parent::init();
        /***DEFAULT TABLES - START ***/
        $bpTables = $this->getTablesByModelDirPath(\Yii::getAlias('@taktwerk-boilerplate'));

        $tables = array_diff(Yii::$app->db->schema->tableNames,$bpTables);
           /*New tables which have base_namespace of BP but their model is not generated yet in the BP - START*/
        foreach($tables as $tbl){
            if(in_array($tbl,$bpTables)){
                continue;
            }
            $cmnt = TableHelper::getTableComment($tbl);
            if($cmnt && isset($cmnt['base_namespace'])){
                if(StringHelper::startsWith(ltrim($cmnt['base_namespace'],'\\'),'taktwerk\\yiiboilerplate\\')){
                    $bpTables[] = $tbl;
                }
            }
        }
            /*New tables which have base_namespace of BP but their model is not generated yet in the BP - END*/
        foreach($bpTables as $tblName)
        {
            if ($tblName && ! in_array($tblName, $this->defaultTables)) {
                if(!in_array($tblName,$this->nonGeneratableTables))
                {
                    $this->defaultTables[] = $tblName;
                }
            }
        }
        /***DEFAULT TABLES - END ***/
    }
    
    /**
     * Actions defined in classes, eg. error page.
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    /**
     * Application dashboard.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new DynamicModel([
            'sql_file'
        ]);
        $model->addRule(['sql_file'], 'required', ['on' => 'validate']);
        
        $postParams = \Yii::$app->request->post();
        if (Yii::$app->request->isPost) {
            $this->showCommand = (bool) @$postParams['just_show_command'];
            if(\Yii::$app->request->post('regenerate_existing')){
                $this->regenerateNonBaseFiles = true;
            }
           
            if (isset($postParams['regenerate']) || isset($postParams['regenerate_sys'])) {
                //prd(array_values(array_intersect($tables, $this->defaultTables)));
                $tables = Yii::$app->db->schema->tableNames;
                if(isset($postParams['regenerate']))
                {
                    $this->tablesMigration = array_diff($tables, $this->defaultTables);
                    $this->tablesMigration = array_values(array_diff($this->tablesMigration,$this->nonGeneratableTables));
                }elseif(isset($postParams['regenerate_sys'])){
                    $this->tablesMigration = array_values(array_intersect($tables, $this->defaultTables));
                }
                foreach($this->tablesMigration as $tbl){
                    $cmnt = TableHelper::getTableComment($tbl);
                    $ns = '';
                    if($cmnt && isset($cmnt['base_namespace'])){
                        $ns = $cmnt['base_namespace'];
                    }
                    if($postParams['regenerate_sys']){
                    /* Show tables having BP namespace */
                    if(StringHelper::startsWith(ltrim($ns,'\\'),'taktwerk\\yiiboilerplate\\')){
                        $this->tablesNamespace[$ns][] = $tbl;
                    }
                    }elseif(isset($postParams['regenerate'])){
                        /* Show tables having app or no namespace */
                       if(StringHelper::startsWith(ltrim($ns,'\\'),'app\\') || $ns==''){
                        $this->tablesNamespace[$ns][] = $tbl;
                       }
                    }
                }
                
            }
            else if (isset($postParams['submit_file'])) {
                $model->sql_file = UploadedFile::getInstance($model, 'sql_file');
                if($model->sql_file){
                    $model->sql_file->saveAs(Yii::getAlias('@runtime/temp.sql'), true);
                    $schema = file_get_contents(Yii::getAlias('@runtime/temp.sql'));
                }
                else {
                    $this->logError('Schema', 'Upload failed');
                }
            } else {
                $schema = Yii::$app->request->post('schema');
            }
            if($schema) {
                $this->validateSchema($schema);
            }
            $errors = $this->errors;
            
            if (empty($errors)) {
                if (isset($postParams['tables'])) {
                    foreach (Yii::$app->request->post('tables') as $table => $value) {
                        $this->importedTables[] = $table;
                    }
                    if(\Yii::$app->request->post('system_tables',false)==false){
                        $this->importedTables = array_diff($this->importedTables, $this->defaultTables);
                    }
                }
                set_time_limit(0);
                if (isset($postParams['submit-generate'])) {
                    // First we import whole schema into database
                    
                    $this->import(Yii::$app->request->post('schema'));
                    // Then we generate migrations
                    $this->generateMigrations();
                    // After we droping those tables we imported, so migrations will generate them
                    $this->drop();
                    // Finally we applying migrations and generating models and cruds files
                    $output = $this->output();
                    // Save output in logs
                    Yii::debug($output);
                } elseif (isset($postParams['submit-crud'])) {
                    // Generate only CRUDs for selected tables
                    $output = $this->generateCrud(true);
                } elseif (isset($postParams['submit-model'])) {
                    // Generate only models for selected tables
                    $output = $this->generateModel(true);
                } elseif (isset($postParams['submit-model-crud'])) {
                    // Generate only models and CRUDs for selected tables
                   $output = $this->generateModel(true);
                   $output .= $this->generateCrud();
                }
                $tables = $this->tablesMigration;
                $tableMigrationList = implode(',', $this->tablesMigration);
                $tableCrudList = implode(',', $this->tablesCrud);
                $tablesNamespace = $this->tablesNamespace;
            }
            $nonGeneratableTables = $this->nonGeneratableTables;
            if(\Yii::$app->request->post('run_in_bg')){
                \Yii::$app->response->format = 'json';
                return ['output'=>$output];
            }
            return $this->render(
                'index',
                compact('model','nonGeneratableTables', 'errors','tablesNamespace', 'tableMigrationList', 'tableCrudList', 'schema', 'tables', 'output')
                );
        }
        return $this->render('index', compact('model'));
    }
    
    /**
     * Drop selected tables
     * @throws \yii\db\Exception
     */
    private function drop()
    {
        $db = Yii::$app->db;
        $db->createCommand()->setSql('SET FOREIGN_KEY_CHECKS = 0')->execute();
        foreach ($this->importedTables as $table) {
            $db->createCommand('DROP TABLE IF EXISTS `' . $table . '`')->execute();
            // Delete history
            $histories = ArHistory::find()->andWhere(['table_name' => $table])->all();
            foreach ($histories as $history) {
                $history->delete();
            }
        }
        $db->createCommand()->setSql('SET FOREIGN_KEY_CHECKS = 1')->execute();
    }
    
    /**
     * Run applying migrations, model and cruds files generations
     * @return string
     */
    private function output()
    {
        $output = '';
        $migrate = 'php ../yii migrate --interactive=0';
        $output .= shell_exec($migrate);
        $output .= $this->generateModel();
        $output .= $this->generateCrud();
        $output .= $this->generateMenu();
        return $output;
    }
    
    private function generateModel($tableValidate = false)
    {
        $output = '';
        // Check first if have all tables in database and all necessary models
        $db = Yii::$app->db;
        $this->errorTables = [];
        $count = 1;

        foreach ($this->importedTables as $key => $table) {
            
            if ($db->getTableSchema($table) == null) {
                
                // There is no table in database, unset it from CRUD generation
                unset($this->importedTables[$key]);
            }
            else 
            {
                $validate = $this->validateTable($table);
                if($tableValidate && !empty($validate))
                {
                    $output .= "<b>$count. Table name</b> '$table':<br/>";
                    $output .= $this->tableErrorOutput($validate[$table],$table);
                    
                    $this->errorTables[] = $table;
                }
            }
            $count++;
        }
        
        $tables = array_diff($this->importedTables, $this->errorTables);
        
        if(count($tables)>0){
            $models = 'php ../yii batch/models --regenerateNonBaseFiles='.$this->regenerateNonBaseFiles.' --tables=' . implode(',', $tables) . ' --interactive=0 2>&1';
            if($this->showCommand){
                $output .= str_replace("php ../yii", "php yii", $models).'<br>';
            }else{
                $output .= "Running php ../yii batch/models --regenerateNonBaseFiles=".$this->regenerateNonBaseFiles." --tables=" . implode(',', $tables) . " --interactive=0 2>&1\n";
                $output .= shell_exec($models);
            }
        }
        return $output;
        
    }
    
    private function generateCrud()
    {
        $output = '';
        // Check first if have all tables in database and all necessary models
        $db = Yii::$app->db;
        foreach ($this->importedTables as $key => $table) {
            if ($db->getTableSchema($table) == null) {
                // There is no table in database, unset it from CRUD generation
                unset($this->importedTables[$key]);
            } else {
                // If table exist, make sure that we have model created
                $modelName = Inflector::camelize($table);
                $cmnt = TableHelper::getTableComment($table);
                $mClass = "\\app\\models\\$modelName";
                if ($cmnt && is_array($cmnt)) {
                    if (isset($cmnt['base_namespace'])) {
                        $mClass = rtrim($cmnt['base_namespace'], '\\') . '\models\\'.$modelName;
                    }
                }
                if (!class_exists($mClass)) {
                    // If model not exist, create it first before create CRUD for it
                    $output .= 'No model exist for table ' . $table . ". Creating...\n";
                    $modelCmd = 'php ../yii batch/models --regenerateNonBaseFiles='.$this->regenerateNonBaseFiles.' --tables=' . $table . ' --interactive=0 2>&1';
                    if($this->showCommand){
                        $output .= str_replace("php ../yii", "php yii", $modelCmd).'<br>';
                    }else{
                        shell_exec($modelCmd);
                    }
                }
            }
        }
        $tables = array_diff($this->importedTables, $this->errorTables);
        if(count($tables)>0){
            $cruds = 'php ../yii batch/cruds --regenerateNonBaseFiles='.$this->regenerateNonBaseFiles.' --tables=' . implode(',', $this->importedTables) . ' --interactive=0 2>&1';// --crud-view-path="@app/modules/backend/views"';
            if($this->showCommand){
                $output .= str_replace("php ../yii", "php yii", $cruds) .'<br>';
            }else{
                $output .= "Running " . $cruds . "\n";
                $output .= shell_exec($cruds);
            }
        }
        return $output;
    }
    private function tableErrorOutput($tableErrorList)
    {
        $output = '';
        $errors = str_repeat('&nbsp', 4) . "<b> Error: </b><br/>";
        $solution = str_repeat('&nbsp', 5) . "<b>Solution: </b><br/>" . str_repeat('&nbsp', 8)." Please create new migration having below query/queries: <br/> " ;
        
        foreach ($tableErrorList as $key => $error) {
            $errors.= str_repeat('&nbsp', 9) . $error['error'] . "<br>";
            $solution .= str_repeat('&nbsp', 10) .$error['solution'] . ";<br>";
        }
        return $errors.$solution;
    }
    
    /**
     * Generate migration files
     */
    private function generateMigrations()
    {
        $this->removeMigrationFiles();
        $migrationTables = implode(', ', $this->importedTables);
        $migration = new TwGenerator();
        $migration->tableName = $migrationTables;
        $migration->templates = [
            'taktwerk' => Yii::getAlias('@taktwerk-boilerplate/templates/migration/default')
        ];
        $migration->template = 'taktwerk';
        $migration->validateTableName();
        $files = $migration->generate();
        foreach ($files as $file) {
            /**
             * @var $file CodeFile
             */
            $file->save();
        }
    }
    
    private function removeMigrationFiles()
    {
        $path = Yii::getAlias('@app/migrations');
        $files = FileHelper::findFiles($path, ['with' => '*.php', 'recursive' => false]);
        foreach ($files as $file) {
            @unlink($file);
        }
    }
    
    /**
     * @return string
     * Generate menu endpoints for generated controllers
     */
    private function generateMenu()
    {
        $controllers = [];
        foreach ($this->importedTables as $importedTable) {
            $controllers[] = Generator::table2Controller($importedTable);
        }
        $menuGenerator = new Generator();
        $menuGenerator->controllers = $controllers;
        $menuGenerator->templates = [
            'taktwerk' => Yii::getAlias('@taktwerk-boilerplate/templates/menu/default')
        ];
        $menuGenerator->template = 'taktwerk';
        $files = $menuGenerator->generate();
        foreach ($files as $file) {
            /**
             * @var $file CodeFile
             */
            $file->save();
        }
        return 'Menu endpoints generated for controllers: ' . implode(', ', $controllers) . "\n";
    }
    
    /**
     * Import schema into database
     * @param $schema
     * @throws \yii\db\Exception
     */
    private function import($schema)
    {
        $this->drop();
        $db = Yii::$app->db;
        $db->createCommand()->setSql('SET FOREIGN_KEY_CHECKS = 0')->execute();
        $db->createCommand($schema)->execute();
        $db->createCommand()->setSql('SET FOREIGN_KEY_CHECKS = 1')->execute();
    }
    
    /**
     * Analyse and validate a mysql schema.
     * @param $schema
     */
    private function validateSchema($schema)
    {
        $tables = $this->parseTables($schema);
        $parser = new SQLParser();
        $parser->parse($schema);
        $tables = $parser->tables;
        // Loop all tables and validate them.
        foreach ($tables as $name => $table) {
            // Ignore default Yii2/boilerplate tables
            $this->tablesCrud[] = $name;
            if (!in_array($name, $this->defaultTables)) {
                $this->tablesMigration[] = $name;
                $this->validateTableSchema($name, $table);
            }
        }
        
        // No tables? That's fishy
        if (empty($this->tablesMigration)) {
            if (empty($schema)) {
                $this->logError("Schema", "The schema was empty. Try submitting only parts of it instead.");
            } else {
                $this->logError("Schema", "Couldn't find any tables in the schema.");
            }
        }
    }
    
    /**
     * @param $schema
     * @return array
     */
    private function parseTables($schema)
    {
        $tables = [];
        $lines = preg_split('/$\R?^/m', $schema);
        $table = null;
        $tableName = null;
        $lineCount = 0;
        
        foreach ($lines as $line) {
            $trimmed = trim($line);
            
            // Look for a CREATE beginning
            if (substr($line, 0, 6) == 'CREATE') {
                $tableName = str_replace('CREATE TABLE IF NOT EXISTS `', '', str_replace('` (', '', $trimmed));
                
                $table = [
                    'column' => [],
                    'primary' => [],
                    'unique' => [],
                    'index' => [],
                    'foreign' => [],
                ];
            } // We are in a table
            elseif (!empty($tableName)) {
                
                // Finished parsing a table
                if (substr($trimmed, -1) == ';') {
                    $tables[$tableName] = $table;
                    $tableName = $table = null;
                } else {          
                    // Start with some tabs? Might be rows
                    if (substr($line, 0, 2) == '  ') {
                        // Field
                        if (substr($trimmed, 0, 1) == '`') {
                            $col = $this->parseColumn($line);
                            $table['column'][$col['name']] = $col['data'];
                        } elseif (substr($trimmed, 0, 7) == 'PRIMARY') {
                            $table['primary'][] = $this->parsePrimary($trimmed);
                        } elseif (substr($trimmed, 0, 6) == 'UNIQUE') {
                            $table['unique'][] = $trimmed;
                        } elseif (substr($trimmed, 0, 5) == 'INDEX') {
                            $index = $this->parseIndex($trimmed);
                            $table['index'][$index['name']] = $index['fields'];
                        } elseif (substr($trimmed, 0, 10) == 'CONSTRAINT') {
                            $foreign = $this->parseForeign($trimmed . trim($lines[$lineCount + 1]) . trim($lines[$lineCount + 2]));
                            $table['foreign'][$foreign['key']] = $foreign['foreign'];
                        }
                    }
                }
            }
            // Ignore the rest!
            $lineCount++;
        }
        
        return $tables;
    }
    
    /**
     * @param $line
     * @return array
     */
    private function parseColumn($line)
    {
        $line = trim($line);
        if (substr($line, -1) != ',') {
            throw new Exception('Column declaration not ending with \',\'. Do you have multiline comments? ' . $line);
        }
        preg_match("/`(?<name>.*?)`(?<data>.*?),/", $line, $data);
        return ['name' => trim($data['name']), 'data' => trim($data['data'])];
    }
    
    /**
     * @param $line
     * @return array
     */
    private function parsePrimary($line)
    {
        $line = trim($line);
        preg_match("/PRIMARY KEY \(`(?<key>.*?)`\)/", $line, $data);
        return trim($data['key']);
    }
    
    /**
     * @param $line
     * @return array
     */
    private function parseForeign($line)
    {
        $line = trim($line);
        preg_match("/CONSTRAINT `(?<key>.*?)`\s*FOREIGN KEY \(`(?<name>.*?)`\)\s*REFERENCES `(?<foreign_table>.*?)` \(`(?<foreign_key>.*?)`\)/", $line, $data);
        return ['key' => trim($data['name']), 'foreign' => ['table' => trim($data['foreign_table']), 'key' => trim($data['foreign_key'])]];
    }
    
    /**
     * @param $line
     * @return array
     */
    private function parseIndex($line)
    {
        $line = trim($line);
        preg_match("/\s*INDEX `(?<name>.*?)`\s* \((?<fields>.*?)\)/", $line, $data);
        $sqlFields = explode(',', $data['fields']);
        $fields = [];
        foreach ($sqlFields as $field) {
            preg_match("/`(?<key>.*?) (?<order>.*)/", $field, $fieldData);
            $fields[$fieldData['key']] = trim($fieldData['order']);
        }
        return ['name' => trim($data['name']), 'fields' => $fields];
    }
    
    /**
     * Add errors
     * @param $name
     * @param $message
     */
    private function logError($name, $message)
    {
        $this->errors[$name][] = $message;
    }
    
    /**
     * @param $tableName
     * @param $tableData
     */
    
    private function validateTableSchema($name, $data)
    {
        $solutions = "<b>Use query like: </b> <br/>CREATE TABLE IF NOT EXISTS ";
        $fieldNames = array_column($data['fields'],'name');
        $missingCommonColumns = array_diff($this->commonColumns,$fieldNames);
        if (preg_match('/[a-z_]*/', $name) != 1) {
            $this->logError($name, "The table '$name' in your Schema doesn't follow the Tw MySQL conventions of only lowercase letters a-z or '_'.");
            $solutions .= "`".strtolower(preg_replace("/[^a-zA-Z_]/", "", $name))."` (";
        }
        else
        {
            $solutions .= "`$name` (";
        }
        $solutions .= "<br/>";
        $columnSolution = [];
        $columnForeignKey = [];
        
        $isIncorrect  = false;
        
        $foreignKeys = [];
        $indexKeys = [];
        $uniqKeys = [];
        $otherConstraints=[];
        $primaryColumnName = '';
       foreach ($data['indexes'] as $item) {
            if ($item['type'] == 'FOREIGN') {
                $i = $item['cols'][0]['name'];
                $foreignKeys[$i] = [
                    'key' => '',
                    'ref_table' => $item['ref_table'],
                    'col' => $i,
                    'ref_col' => $item['ref_cols'][0]['name']
                ];
            } else if ($item['type'] == 'INDEX') {
                $indexKeys[] = [
                    'col' => array_column($item['cols'], 'name'),
                    'name' => @$item['name']
                ];
            } else if ($item['type'] == 'UNIQUE') {
                $uniqKeys[] = [
                    'col' => array_column($item['cols'], 'name'),
                    'name' => @$item['name']
                ];
            }else if ($item['type'] == 'PRIMARY') {
                $primaryColumnName = @$item['cols'][0]['name'];
            }
        }
        $columnNames = [];
        foreach($data['fields'] as $k=>$val){
            $columnNames[] = $val['name'];
        }
        
        // Check each column.
        foreach ($data['fields'] as $k => $val) {
            $key = $val['name'];
            $value = $val['type'];
            if(array_key_exists('length', $val)){
                $value .= '('.$val['length'].')';
            }
            if(array_key_exists('null', $val)){
                if($val['null'])
                {
                    $value .= ' NULL';
                }
                else{
                    $value .= ' NOT NULL';
                }
            }
            if(array_key_exists('default', $val)){
                
                if($val['default']==='')
                {
                    $value .= ' DEFAULT \'\'';
                }else if($val['default']=='NULL'){
                    $value .= ' DEFAULT NULL';
                }else if(is_string($val['default'])){
                    $value .= ' DEFAULT \''.addslashes($val['default'])."'";
                }
                else{
                    $value .= ' DEFAULT '.$val['default'];
                }
            }
            if(array_key_exists('auto_increment', $val)){
                if($val['auto_increment']){
                 $value .= ' AUTO_INCREMENT';   
                }
            }
            if(array_key_exists('more', $val)){
                foreach ($val['more'] as $m){
                    $value.= ' '.$m;
                }
            }
            if(preg_match('/^[a-z_]*$/', $key)!=1)
            {
                //prd($key);
            }
            /* $value =  */
            $lowerValue = strtolower($value);
            
            if (preg_match('/^[a-z_]*$/', $key) != 1) {
                $isIncorrect = true;
                $this->logError($name, "The column name '$key' in your Schema does not follow the tw mysql conventions. Please make sure to create columns with only lowercase letters a-z or '_'.");
                $newKey = strtolower(preg_replace('/[^a-zA-Z_]/i', '', $key));
            }
            else
            {
                $newKey = $key;
            }
            
            // Now the fun part, look at the names.
            if (substr($key, -3) == '_id') {
                // Check that it is an integer and has a foreign key
                /*uncomment to enable _id integer,foreign key validation, index validations
                 if (!in_array($key, ['language_id', 'country_id']) && strpos($lowerValue, 'int') === false) {
                     
                    $this->logError($name, "The column name '$key' isn't an <strong>Integer</strong> (<i>$value</i>).");
                    $isIncorrect = true;
                    $columnSolution[]= "'$key' int(11) NOT NULL";
                    
                }else{*/
                    $columnSolution[]= "`$key`" . " " . $value;
                //}
                if (!isset($foreignKeys[$key]) &&
                    !in_array($name, $this->tablesSuffixUnsensual)
                ) {
                    $isIncorrect = true;
                    $this->logError($name, "The column name '$key' is an '_id' but doesn't have a foreign key defined.");
                    $foreignKeyTable = str_replace("_id", "", $key);
                    $foreignKeyName = "fk_".$name."_".$key;
                    $columnForeignKey[] = "CONSTRAINT `$foreignKeyName` FOREIGN KEY (`$key`) REFERENCES `$foreignKeyTable` (`id`)";
                    $columnForeignKey[] = "INDEX `".$this->sanitizeIndexName("idx_{$name}_deleted_at_{$key}")."`(`deleted_at`,`{$key}`)";
                } 
            } // Booleans
            elseif (substr($key, 0, 3) == 'is_' || substr($key, 0, 4) == 'has_') {
                if (strpos($lowerValue, 'boolean') === false && strpos($lowerValue, 'tinyint(') === false) {
                    $isIncorrect = true;
                    $this->logError($name, "The column name '$key' isn't a <strong>Boolean</strong> or a <strong>TinyInt(1)</strong> (<i>$value</i>).");
                    $columnSolution[]= "'$key' tinyint(1) DEFAULT NULL";
                }else{
                    $columnSolution[]=  "`$key`" . " " . $value;
                }
            } // Dates
            elseif (substr($key, -3) == '_at') {
                
                if (strpos($lowerValue, 'datetime') === false && !in_array($name, $this->tablesSuffixUnsensual)) {
                    
                    $isIncorrect = true;
                    $this->logError($name, "The column name '$key' isn't a <strong>DateTime</strong> (<i>$value</i>)).");
                    $columnSolution[]= "'$key' datetime DEFAULT NULL";
                }else{
                    $columnSolution[]=  "`$key`" . " " . $value;
                }
            } elseif (substr($key, -5) == '_date') {
                if (strpos($lowerValue, 'date') === false &&
                    !in_array($name, $this->tablesSuffixUnsensual)
                ) {
                    $isIncorrect = true;
                    $this->logError($name, "The column name '$key'  isn't a <strong>Date</strong> (<i>$value</i>)).");
                    $columnSolution[]= "'$key' date DEFAULT NULL";
                }else{
                    $columnSolution[]=  "`$key`" . " " . $value;
                }
            }elseif(substr($key, -5) == '_file'){
                
                if(!in_array($key.'_filemeta', $columnNames)){
                    $isIncorrect = true;
                    $this->logError($name, "The column `$key`  requires another column named '{$key}_filemeta' to store meta information of the file.");
                    $columnSolution[]= "`{$key}_filemeta` text";
                }
                
            }else{
                if(!in_array($key, $this->commonColumns))
                {
                    $columnSolution[]=  "`$key`" . " " . $value;
                }
            }
        }
        foreach($missingCommonColumns as $c){
            if(substr($c, -3) == '_at'){
                $isIncorrect = true;
                $this->logError($name, "The column '{$c}' isn't present in the table.");
                $columnSolution[]= "`$c` datetime DEFAULT NULL";
            }
            if(substr($c, -3) == '_by'){
                $isIncorrect = true;
                $this->logError($name, "The column '{$c}' isn't present in the table.");
                $columnSolution[]= "`$c` int(11) DEFAULT NULL";
            }
        }
        if(!empty($columnSolution))
        {
            $solutions .= implode(", <br/>", $columnSolution);
            $solutions .= ",<br/>";
        }
        // Check there is a primary key.
        if ($primaryColumnName) {
             $solutions .= "PRIMARY KEY (`$primaryColumnName`)";
        } else {
            $isIncorrect = true;
            $this->logError($name, "The table '$name' doesn't seem to have a primary key.");
            $solutions .= "PRIMARY KEY (`id`)";
        }
        
        if(!empty($foreignKeys))
        {
            foreach($foreignKeys as $l=>$z)
            {
                $foreignKeyName = "fk_".$name."_".$l;
                $columnForeignKey[] = "CONSTRAINT `{$foreignKeyName}` FOREIGN KEY (`$l`) REFERENCES `{$z['ref_table']}`(`{$z['ref_col']}`)";
                $isIndexedWithDeletedAt = false;
                foreach ($indexKeys as $i){
                    if($i['col'][0]=='deleted_at'){
                        if(isset($i['col'][1]) && $i['col'][1]==$l){
                            $isIndexedWithDeletedAt = true;
                        }
                    }
                }
                if($isIndexedWithDeletedAt==false){
                    $columnForeignKey[] = "INDEX `".$this->sanitizeIndexName("idx_{$name}_deleted_at_{$l}")."`(`deleted_at`,`{$l}`)";
                }
            }
        }
       
        if(!empty($columnForeignKey))
        {
            $solutions .= ", <br/>". implode(", <br>", $columnForeignKey);
        }
        if(!empty($uniqKeys))
        {
            foreach($uniqKeys as $i){
                $indexString = implode(",", array_map(function($string) {
                    return '`' . $string . '`';
                }, $i['col']));
                $solutions .= ", <br/>CONSTRAINT ".(($i['name'])?'`'.$i['name'].'`':''). "UNIQUE"."(". $indexString.")";
            }
        }
        $hasDeletedAtIndex = false;
        if(!empty($indexKeys))
        {
            foreach($indexKeys as $i){
                if(isset($i['col'][0]) && $i['col'][0]=='deleted_at'){
                    if(count($i['col'])<=1){
                        $hasDeletedAtIndex = true;
                    }
                }
                $indexString = implode(",", array_map(function($string) {
                    return '`' . $string . '`';
                }, $i['col']));
                $solutions .= ", <br/>INDEX ".(($i['name'])?'`'.$i['name'].'`':'') ."(". $indexString.")";
            }
            
            
        }
        if($hasDeletedAtIndex==false){
            if(array_search('deleted_at',$missingCommonColumns)===false)
            {//To SHOW this error message when deleted_at column is present in the sql
                $isIncorrect = true;
                $this->logError($name, "The column 'deleted_at' does not have an index.");
            }
            $solutions .= ", <br/>INDEX `".$this->sanitizeIndexName("idx_{$name}_deleted_at")."` (`deleted_at`)";
        }
        
        $solutions .= "<br/> )";
        if(isset($data['props'])){
            foreach($data['props'] as $j=>$i){
                $solutions .=' '.$j.'='.$i;
            }
        }
        $solutions .= ";";
        
        if($isIncorrect)
        {
            $this->logError($name, $solutions);
        }
    }
    private function sanitizeIndexName($indexName){
        $indexName = str_replace(['{{%','}}'],['',''] , $indexName);
        if(strlen($indexName)>63){
            //limiting the max-length of the index name to around 60-64 characters
            $indexName = substr($indexName, 0, 55).'_'.substr(str_shuffle(str_repeat($x='abcdefghijklmnopqrstuvwxyz', ceil(5/strlen($x)) )),1,5);
        }
        return $indexName;
    }
    /**
     * @param $tableName
     * @param $tableData
     */
    private function validateTable($name)
    {
        $data = \Yii::$app->db->getTableSchema($name);
        $foreignKeyColumns = [];
        foreach($data->foreignKeys as $k=>$fKey){
            $fkCol = array_keys($fKey)[1];
            $foreignKeyColumns[$fkCol]=['key'=>$k,'ref_table'=>$fKey[0], 'col'=>$fkCol,'ref_col'=>$fKey[$fkCol]];
        }
        $tablePrefix = \Yii::$app->db->tablePrefix;
        $nameWithoutPrefix = ($tablePrefix!='')?preg_replace("/^{$tablePrefix}/", '', $name):$name;
        $nameWithoutPrefix = "{{%".$nameWithoutPrefix."}}";
        $indexList=[];
        try{
        $indexList = \Yii::$app->db->createCommand("SHOW INDEXES FROM `$name`")->queryAll();
        }catch(\Exception $e){}
        $i = 0;
        if (preg_match('/[a-z_]*/', $name) != 1) {
            $i++;
            $logError[$name][$i]['error'] = "The table '$name' in your Schema doesn't follow the Tw MySQL conventions of only lowercase letters a-z or '_'.";
            $newName = strtolower(preg_replace("/[^a-zA-Z_]/", "", $name));
            $logError[$name][$i]['solution'] = "\$this->renameTable('$name', '$newName')";   
        }
        $columnSolution = [];
        $columnForeignKey = [];
        $logError = [];
        // Check each column. 
        
        //Checking common columns existence in the table
        foreach($this->commonColumns as $colName){
            if(!isset($data->columns[$colName])){
                $i++;
                $logError[$name][$i]['error'] = 'The column \''.$colName.'\' is missing from the table';
                if(substr($colName, -3) == '_by')
                {
                    $logError[$name][$i]['solution'] = '$this->addColumn("'.$nameWithoutPrefix.'", "'.$colName.'", $this->integer()->defaultValue(null))';
                }else
                if(substr($colName, -3) == '_at' && !in_array($name, $this->tablesSuffixUnsensual)){
                    $logError[$name][$i]['solution'] = '$this->addColumn(\''.$nameWithoutPrefix.'\', \''.$colName.'\', $this->dateTime()->defaultValue(null))';
                    if($colName=='deleted_at'){
                        $indexName = $this->sanitizeIndexName("idx_".$name."_deleted_at");
                        $logError[$name][$i]['solution'] .= ";<br>".str_repeat('&nbsp', 10)." \$this->createIndex('$indexName', '$nameWithoutPrefix', 'deleted_at')";
                    }
                }
            }
        }
        //Checking deleted_at index existence
        if ( isset($data->columns['deleted_at']) && array_search('deleted_at', array_column($indexList, 'Column_name')) === FALSE) {
            $i ++;
            $logError[$name][$i]['error'] = "The column name 'deleted_at' doesn't have any <strong>Index</strong>.";
            $indexName =  $this->sanitizeIndexName('idx_'.$name . "_deleted_at");
            $logError[$name][$i]['solution'] = "\$this->createIndex('$indexName', '$nameWithoutPrefix', 'deleted_at')";
        }
        // DELETED_AT & FOREIGN MULTI-COLUMN INDEX - START
        foreach ($foreignKeyColumns as $k=>$fKey) {
            $fkCol = $fKey['col'];
            $seqIndex = false;
            foreach ($indexList as $index) {
                if ($index['Column_name'] == $fkCol) {
                    if ($index['Seq_in_index'] == 2)
                    {
                      $deleteIndexExist = \Yii::$app->db->createCommand("SHOW INDEXES FROM `$name` WHERE `Key_name`='{$index['Key_name']}' AND `Column_name`='deleted_at'")->queryAll();
                      if(count($deleteIndexExist)>0){
                          if($deleteIndexExist[0]['Seq_in_index']==1)
                          {
                              $seqIndex = true;
                              break;
                          }
                      }
                    }
                }
            }
            if($seqIndex==false){
                $i++;
                $logError[$name][$i]['error'] = "The column name '".$fkCol."' doesn't have any <strong>Index</strong> with 'deleted_at' column.";
                $indexName =  $this->sanitizeIndexName("idx_".$name."_deleted_at_".$fkCol);
                $logError[$name][$i]['solution'] = "\$this->createIndex('$indexName', '$nameWithoutPrefix', ['deleted_at','{$fkCol}'])";
            }
        }
        // DELETED_AT & FOREIGN MULTI-COLUMN INDEX - END
        foreach ($data->columns as $key => $value) {
            $value = $value->dbType;
            $lowerValue = strtolower($value);
            if (preg_match('/^[a-z_]*$/', $key) != 1) {
                $i++;
                $logError[$name][$i]['error'] = "The column name '$key' in your Schema does not follow the tw mysql conventions. Please make sure to create columns with only lowercase letters a-z or '_'.";
                $newKey = strtolower(preg_replace('/[^a-zA-Z_]/i', '', $key));
                $logError[$name][$i]['solution'] = "\$this->renameColumn('$nameWithoutPrefix','$key', '$newKey')";
            }
            
            // Now the fun part, look at the names.
            if (substr($key, -3) == '_id' &&
                !in_array($name, $this->tablesSuffixUnsensual)
            ) {/* --uncomment to enable _id integer--
                // Check that it is an integer and has a foreign key
                if (!in_array($key, ['language_id', 'country_id']) && strpos($lowerValue, 'int') === false) {
                    $i++;
                    $logError[$name][$i]['error'] = "The column name '$key' isn't an <strong>Integer</strong> (<i>$value</i>).";
                    $logError[$name][$i]['solution']= "\$this->alterColumn('$nameWithoutPrefix','$key', \$this->integer(11)->notNull())";
                }*/
                if(!isset($foreignKeyColumns[$key])){
                    $i++;
                    $logError[$name][$i]['error'] = "The column name '$key' is an '_id' but doesn't have a foreign key defined.";
                    $foreignKeyTable = str_replace("_id", "", $key);
                    $foreignKeyTable = "{{%".$foreignKeyTable."}}";
                    $foreignKeyName = "fk_".$name."_".$key;
                    $logError[$name][$i]['solution'] = "\$this->addForeignKey('$foreignKeyName', '$nameWithoutPrefix', '$key', '$foreignKeyTable', 'id');<br>";
                    //MULTI-COLUMN INDEX - START
                    $indexName = "idx_".$name."_deleted_at_".$key;
                    $logError[$name][$i]['solution'] .= str_repeat('&nbsp', 10)."\$this->createIndex('$indexName', '$nameWithoutPrefix', ['deleted_at','{$key}'])";
                    //MULTI-COLUMN INDEX - END
                } 
            } // Booleans
            elseif (substr($key, 0, 3) == 'is_' || substr($key, 0, 4) == 'has_') {
                if (strpos($lowerValue, 'boolean') === false && strpos($lowerValue, 'tinyint(') === false) {
                    $i++;
                    $logError[$name][$i]['error'] = "The column name '$key' isn't a <strong>Boolean</strong> or a <strong>TinyInt(1)</strong> (<i>$value</i>).";
                    $logError[$name][$i]['solution']= "\$this->alterColumn('$nameWithoutPrefix','$key', \$this->tinyInteger(1)->defaultValue(null))";
                }
            }
            elseif (substr($key, -3) == '_at') {
                if (strpos($lowerValue, 'datetime') === false && !in_array($name, $this->tablesSuffixUnsensual)) {
                    $i++;
                    $logError[$name][$i]['error'] = "The column name '$key' isn't a <strong>DateTime</strong> (<i>$value</i>)).";
                    $logError[$name][$i]['solution']= "\$this->alterColumn('$nameWithoutPrefix','$key', \$this->dateTime()->defaultValue(null))";
                }
            } elseif (substr($key, -5) == '_date') {
                if (strpos($lowerValue, 'date') === false &&
                    !in_array($name, $this->tablesSuffixUnsensual)
                ) {
                    $i++;
                    $logError[$name][$i]['error'] = "The column name '$key'  isn't a <strong>Date</strong> (<i>$value</i>)).";
                    $logError[$name][$i]['solution']= "\$this->alterColumn('$nameWithoutPrefix','$key', \$this->date()->defaultValue(null))";
                }
            }elseif(substr($key, -5) == '_file'){
                if(!isset($data->columns[$key.'_filemeta'])){
                    $hasMetaAtr = false;
                    foreach($data->columns as $j=>$c){
                        if($c->comment){
                            try{
                                $comment = Json::decode($c->comment);
                                if($comment 
                                    && isset($comment['inputtype']) 
                                    && $comment['inputtype']== 'filemeta' 
                                    && isset($comment['related_attribute']) 
                                    && $comment['related_attribute'] == $key)
                                {
                                    
                                    $hasMetaAtr = true;
                                    break;
                                }
                            }catch(\Exception $e){
                                
                            }
                        }
                    }
                    if($hasMetaAtr==false){
                        $i++;
                        $logError[$name][$i]['error'] = "The column '$key'  requires another column named '{$key}_filemeta' to store meta information of the file.";
                        $logError[$name][$i]['solution']= "\$this->addColumn('$nameWithoutPrefix', '{$key}_filemeta', \$this->text())";
                    }
                }
            }
        }
        $solutions .= implode(", <br/>", $columnSolution);
        
        // Check there is a primary key.
        if (empty($data->primaryKey[0])) {
            $i++;
            $logError[$name][$i]['error'] = "The table '$name' doesn't seem to have a primary key.";
            $pkName = $name. "_pk";
            $logError[$name][$i]['solution'] = "\$this->addPrimaryKey('$pkName', '$nameWithoutPrefix', ['id']);";
        }
       
        return $logError;
    }
}