<?php

namespace taktwerk\yiiboilerplate\modules\payment;

/**
 * payment module definition class
 */
class Module extends \taktwerk\yiiboilerplate\components\TwModule
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\payment\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // When in console mode, switch controller namespace to commands
        if (\Yii::$app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'taktwerk\yiiboilerplate\modules\payment\commands';
        }
    }
}
