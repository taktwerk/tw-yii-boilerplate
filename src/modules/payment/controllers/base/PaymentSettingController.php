<?php
//Generation Date: 07-Jan-2021 11:46:22am
namespace taktwerk\yiiboilerplate\modules\payment\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * PaymentSettingController implements the CRUD actions for PaymentSetting model.
 */
class PaymentSettingController extends TwCrudController
{
}
