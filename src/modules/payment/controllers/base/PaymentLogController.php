<?php
//Generation Date: 05-Jan-2021 08:41:29am
namespace taktwerk\yiiboilerplate\modules\payment\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * PaymentLogController implements the CRUD actions for PaymentLog model.
 */
class PaymentLogController extends TwCrudController
{
}
