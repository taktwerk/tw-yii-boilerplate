<?php
//Generation Date: 05-Jan-2021 08:41:29am
namespace taktwerk\yiiboilerplate\modules\payment\controllers;

use Yii;
use taktwerk\yiiboilerplate\modules\payment\components\TransferFileFacadeFactory;
use taktwerk\yiiboilerplate\modules\payment\components\PaymentInterface;
use taktwerk\yiiboilerplate\modules\payment\models\PaymentFile;
use taktwerk\yiiboilerplate\modules\payment\models\PaymentLog;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii2tech\spreadsheet\Spreadsheet;

use Z38\SwissPayment\BIC;
use Z38\SwissPayment\IBAN;
use Z38\SwissPayment\Message\CustomerCreditTransfer;
use Z38\SwissPayment\Money;
use Z38\SwissPayment\PaymentInformation\PaymentInformation;
use Z38\SwissPayment\PostalAccount;
use Z38\SwissPayment\StructuredPostalAddress;
use Z38\SwissPayment\TransactionInformation\BankCreditTransfer;
use Z38\SwissPayment\TransactionInformation\IS1CreditTransfer;
use Z38\SwissPayment\UnstructuredPostalAddress;
use Mpdf\Mpdf;
use taktwerk\yiiboilerplate\modules\customer\models\Client;

/**
 * This is the class for controller "PaymentLogController".
 */
class PaymentLogController extends \taktwerk\yiiboilerplate\modules\payment\controllers\base\PaymentLogController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\payment\models\PaymentLog';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\payment\models\search\PaymentLog';

    /**
     * Additional actions for controllers, uncomment to use them
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'export',
                        ],
                        'roles' => ['@']
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'export'  => ['POST'],
                ],
            ]
        ]);
    }

    public function actionExport()
    {
        if(\Yii::$app->request->isAjax){
            if(\Yii::$app->request->get('exportcheck')){
                \Yii::$app->response->format = 'json';
                if(\Yii::$app->session->getFlash('paymentExported')==='yes')
                {
                   return true;
                }
                return false;
            }
        }
        try {
            set_time_limit(0);
            $ids = \Yii::$app->request->post('ids');
            $modelClass = \Yii::$app->request->post('model_class');
            $modelClass = str_replace("\\\\", "\\", $modelClass);
            $modelName = substr($modelClass, strrpos($modelClass, '\\') + 1);

            $timeId = date("ymd-His", time());
            $filename = $timeId . '_payment_' . getenv('APP_NAME') . '_' . $modelName;
            // max 35 chars
            $messageId = substr(str_replace(['-', '_', '.', ' '], ['', '', '', ''], $filename), 0, 35);

            $allIds = explode(',', $ids);
            $processedIds = [];
            $processedModels = [];
            foreach ($allIds as $key => $id) {
                $model = $modelClass::findOne($id);
                if ($model && $model instanceof PaymentInterface) {
                    $amount = $model->getPaymentAmount();
                    if ($amount) {
                        $processedIds[] = $id;
                        $processedModels[] = $model;
                        /**@var $model PaymentInterface */
                        $paymentLog = PaymentLog::find()
                        ->andWhere(['reference' => $id,
                            'reference_class' => $modelClass])->one();
                        if (!$paymentLog) {
                            $paymentLog = new PaymentLog();
                        }
                        $paymentLog->reference = $id;
                        $paymentLog->reference_class = $modelClass;
                        $paymentLog->amount = $amount;
                        $paymentLog->iban = $model->getPaymentIBAN();
                        $paymentLog->payment_date = $model->getPaymentDate();
                        $paymentLog->description = $model->getPaymentDescription();
                        $paymentLog->identification = $model->getPaymentIdentification();
                        $paymentLog->save();
                    }
                } else {
                    throw new \Exception('Model is not an instance of payment interface');
                }
            }
            $paymentLogs = PaymentLog::find()
            ->andWhere(['reference' => $processedIds])
                ->andWhere(['reference_class' => $modelClass])
                ->asArray()->all();
                if (count($paymentLogs) < 1) {
                \Yii::$app->session->setFlash('danger', \Yii::t('twbp', 'Export not possible, because no valid payment entries selected'));
                return $this->redirect(\Yii::$app->request->referrer);
            }

            $paymentSetting = new \taktwerk\yiiboilerplate\modules\payment\components\Setting();
            $xmlFileName = $filename . ".xml";
            $type = 'sepa';
            
            if($type == 'swiss') {
                $sepaPayment = new PaymentInformation(
                    $timeId,
                    $paymentSetting->get('sender_name', PaymentLog::SENDER_NAME),
                    new BIC($paymentSetting->get('sender_bic', PaymentLog::SENDER_BIC)),
                    new IBAN($paymentSetting->get('sender_iban', PaymentLog::SENDER_IBAN))
                );

                // swiss
                foreach ($paymentLogs as $key => $payment) {
                    if ($payment['amount']) {
                        $transaction = new BankCreditTransfer(
                            $timeId . '-' . $payment['id'],
                            $payment['description'],
                            new Money\CHF($payment['amount'] * 100), // CHF 1300.00
                            $payment['identification'],
                            null, //new StructuredPostalAddress('Wiesenweg', '14b', '8058', 'Zürich-Flughafen'),
                            new IBAN($payment['iban']),
                            new BIC('CRESCHZZ80A')//new BIC($payment['bic'])
                        );
                        $sepaPayment->addTransaction($transaction);

                        /*$transaction2 = new IS1CreditTransfer(
                            'instr-002',
                            'e2e-002',
                            new Money\CHF(30000), // CHF 300.00
                            'Finanzverwaltung Stadt Musterhausen',
                            UnstructuredPostalAddress::sanitize('Altstadt 1a', '4998 Musterhausen'),
                            new PostalAccount('80-151-4')
                        );*/
                    }
                }

                $message = new CustomerCreditTransfer($messageId, $paymentSetting->get('sender_name', PaymentLog::SENDER_NAME));
                $message->addPayment($sepaPayment);

                $xmlContent = $message->asXml();
            }
            else {

                $customerCredit = TransferFileFacadeFactory::createCustomerCredit($messageId, $paymentSetting->get('sender_name', PaymentLog::SENDER_NAME), 'pain.001.001.03');
                $customerCredit->addPaymentInfo('Payment', array(
                    'id' => $timeId,
                    'debtorName' => $paymentSetting->get('sender_name', PaymentLog::SENDER_NAME),
                    'debtorAccountIBAN' => $paymentSetting->get('sender_iban', PaymentLog::SENDER_IBAN),
                    'debtorAgentBIC' => $paymentSetting->get('sender_bic', PaymentLog::SENDER_BIC),
                    'originAccountCurrency' => $paymentSetting->get('currency', PaymentLog::CURRENCY)
                ));
                foreach ($paymentLogs as $key => $payment) {
                    if ($payment['amount']) {
                        $customerCredit->addTransfer('Payment', array(
                            'instructionId' => $payment['id'],
                            'amount' => $payment['amount'] * 100,
                            'creditorIban' => $payment['iban'],
                            'creditorBic' => $payment['bic'],
                            'creditorName' => $payment['identification'],
                            'remittanceInformation' => $payment['description'],
                            'currency' => $paymentSetting->get('currency', PaymentLog::CURRENCY)
                        ));
                    }
                }
                $xmlContent = $customerCredit->asXML();
                // hack
                $xmlContent = str_replace('<Document xmlns="urn:iso:std:iso:20022:tech:xsd:pain.001.001.03" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:iso:std:iso:20022:tech:xsd:pain.001.001.03 pain.001.001.03.xsd">', '<Document xmlns="http://www.six-interbank-clearing.com/de/pain.001.001.03.ch.02.xsd">', $xmlContent);
                
            }

            $xmlFilePath = \Yii::getAlias('@runtime/upload/' . $xmlFileName);
            file_put_contents($xmlFilePath, $xmlContent);

            $xlFileName = $filename . ".xls";
            $exporter = new Spreadsheet([
                'dataProvider' => new ArrayDataProvider([
                    'allModels' => $paymentLogs
                ]),
                'columns' => [
                    [
                        'attribute' => 'identification',
                        'label' => Yii::t('twbp', 'Payment Identification')
                    ],
                    [
                        'attribute' => 'iban',
                        'label' => Yii::t('twbp', 'Iban')
                    ],
                    [
                        'attribute' => 'description',
                        'label' => Yii::t('twbp', 'Payment Description')
                    ],
                    [
                        'attribute' => 'amount',
                        'label' => Yii::t('twbp', 'Payment Amount')
                    ],
                    // needs setting to add or remove
                    /*
                     * [
                     * 'attribute' => 'payment_date',
                     * ],
                     */
                ]
            ]);
            $xlFilePath = \Yii::getAlias('@runtime/upload/' . $xlFileName);
            $exporter->save($xlFilePath);
            $pdfFileName = $filename.'.pdf';
            $paymentFile = new PaymentFile();
            $paymentFile->xml_file = $xmlFileName;
            $paymentFile->pdf_file = $pdfFileName;
            $paymentFile->excel_file = $xlFileName;
            $paymentFile->reference_class = $modelClass;
            $paymentFile->save();
            /*PDF - GENERATION START*/
            $pdfFilePath = \Yii::getAlias('@runtime/upload/' . $pdfFileName);
            
            $mpdf = new Mpdf([
                'tempDir'=>\Yii::getAlias('@runtime/tmp')
            ]);

            /* echo $this->renderPartial('pdf',
                [
                    'client'=>Client::findOne(1),// $paymentSetting->get('client'),
                    'paymentLogs'=>$paymentLogs,
                    'paymentFile'=>$paymentFile
                ]
                );exit;
              */
            $mpdf->setFooter('{PAGENO}');
            $mpdf->WriteHTML($this->renderPartial('pdf',
                [
                    'client'=>$paymentSetting->get('client'),
                    'paymentLogs'=>$paymentLogs,
                    'paymentFile'=>$paymentFile
                ]));
            $mpdf->Output($pdfFilePath,'F');
            /*PDF - GENERATION END*/

            $stream = fopen($xmlFilePath, 'r+');
            \Yii::$app->fs->putStream($paymentFile->getUploadPath() . $xmlFileName, $stream);
            fclose($stream);
            $stream = fopen($xlFilePath, 'r+');
            \Yii::$app->fs->putStream($paymentFile->getUploadPath() . $xlFileName, $stream);
            fclose($stream);
            $stream = fopen($pdfFilePath, 'r+');
            \Yii::$app->fs->putStream($paymentFile->getUploadPath() . $pdfFileName, $stream);
            fclose($stream);
            
            @unlink($xmlFilePath);
            @unlink($xlFilePath);
            @unlink($pdfFilePath);

            // trigger models aterPaid method
            foreach($processedModels as $m) {
                if($m->hasMethod('afterPaid')) {
                    $m->afterPaid();
                }
            }

            \Yii::$app->session->setFlash('paymentExported', 'yes');
            return $this->redirect(\Yii::$app->request->referrer);

            /* $fileToDownload = (\Yii::$app->request->post('download_file_type') == 'xml') ? $xmlFileName : $xlFileName;
            $fileToDownloadName = (\Yii::$app->request->post('download_file_type') == 'xml') ? $xmlFileName : $xlFileName;
            $filePath = \Yii::$app->fs->read($paymentFile->getUploadPath() . $fileToDownload);
            return \Yii::$app->response->sendContentAsFile($filePath, $fileToDownloadName); */
        } catch (\Exception $exception) {
            if(YII_ENV_DEV) {
                throw new \yii\base\Exception($exception->getMessage());
            }
            \Yii::$app->session->setFlash('danger', $exception->getMessage());
            return $this->redirect(\Yii::$app->request->referrer);
        }
    }
}
