<?php
//Generation Date: 05-Jan-2021 08:41:29am
namespace taktwerk\yiiboilerplate\modules\payment\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use Yii;
use taktwerk\yiiboilerplate\components\ClassDispenser;
/**
 * This is the class for controller "PaymentFileController".
 */
class PaymentFileController extends \taktwerk\yiiboilerplate\modules\payment\controllers\base\PaymentFileController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\payment\models\PaymentFile';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\payment\models\search\PaymentFile';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
    /**
     * Init controller
     * @throws \Exception
     */
    public function init()
    {
        if(php_sapi_name() != "cli") {
            $this->crudRelations = [
                'GuideSteps' => true,
                'GuideAssetPivots' => true,
                'GuideChildren0' => true
            ];
            $this->crudColumnsOverwrite = [
                'crud' => [
                    'reference_class' => [
                        'attribute' => 'reference_class',
                        'visible' => \Yii::$app->user->can('Authority')
                    ],
                    'after#reference_class' =>
                        [
                            'attribute' => 'created_at',
                            'content' => function ($model) {
                                return \Yii::$app->formatter->asDatetime($model->created_at);
                            },
                            'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\DateTimeColumn::class),
                            'format' => 'datetime'
                        ],

                ]
            ];
        }
        return parent::init();
    }
}
