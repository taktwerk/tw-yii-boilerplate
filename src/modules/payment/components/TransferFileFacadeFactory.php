<?php
namespace taktwerk\yiiboilerplate\modules\payment\components;

use Digitick\Sepa\DomBuilder\CustomerDirectDebitTransferDomBuilder;
use Digitick\Sepa\GroupHeader;
use Digitick\Sepa\TransferFile\CustomerCreditTransferFile;
use Digitick\Sepa\TransferFile\CustomerDirectDebitTransferFile;
use Digitick\Sepa\TransferFile\Facade\CustomerDirectDebitFacade;
use Digitick\Sepa\TransferFile\Factory\TransferFileFacadeFactory as BaseTransferFileFacadeFactory;
class TransferFileFacadeFactory extends BaseTransferFileFacadeFactory
{
    /**
     * @param string $uniqueMessageIdentification Maximum length: 35. Reference Number of the bulk.
     *                                            Part of the duplication check (unique daily reference).
     *                                            The first 8 or 11 characters of <Msgld> must match the BIC of the
     *                                            Instructing Agent. The rest of the field can be freely defined.
     */
    public static function createDirectDebit(string $uniqueMessageIdentification, string $initiatingPartyName, string $painFormat = 'pain.008.002.02'): CustomerDirectDebitFacade
    {
        $groupHeader = new GroupHeader($uniqueMessageIdentification, $initiatingPartyName);

        return self::createDirectDebitWithGroupHeader($groupHeader, $painFormat);
    }

    public static function createDirectDebitWithGroupHeader(GroupHeader $groupHeader, string $painFormat = 'pain.008.002.02'): CustomerDirectDebitFacade
    {
        return new CustomerDirectDebitFacade(new CustomerDirectDebitTransferFile($groupHeader), new CustomerDirectDebitTransferDomBuilder($painFormat));
    }

    public static function createCustomerCredit(string $uniqueMessageIdentification,string $initiatingPartyName,
        string $painFormat = 'pain.001.002.03'): CustomerCreditFacade
    {
        $groupHeader = new GroupHeader($uniqueMessageIdentification, $initiatingPartyName);

        return self::createCustomerCreditWithGroupHeader($groupHeader, $painFormat);
    }

    public static function createCustomerCreditWithGroupHeader(GroupHeader $groupHeader, string $painFormat = 'pain.001.002.03'): CustomerCreditFacade
    {
        return new CustomerCreditFacade(new CustomerCreditTransferFile($groupHeader), new CustomerCreditTransferDomBuilder($painFormat));
    }
}
