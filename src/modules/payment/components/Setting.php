<?php

namespace taktwerk\yiiboilerplate\modules\payment\components;

use taktwerk\yiiboilerplate\modules\payment\models\PaymentSetting;
use Yii;

class Setting extends \yii\base\Component {

    /**
     * Get setting value
     * @param string $code
     * @param string $default
     * @return string | null
     * @throws \Throwable
     */
    public function get($field = null, $default = null)
    {
        if (!$field)
            return $default;

        $setting = PaymentSetting::getDb()->cache(function ($db) {
            return PaymentSetting::find()
                ->andWhere(['client_id' => Yii::$app->user->identity->client_id])
                ->one();
        });

        if ($setting && $setting->$field)
            return $setting->$field;
        else
            return $default;
    }

}
