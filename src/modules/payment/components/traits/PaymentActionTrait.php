<?php

namespace taktwerk\yiiboilerplate\modules\payment\components\traits;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

trait PaymentActionTrait
{
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        if(php_sapi_name() != "cli") {
            $customActions = [
                'export-payment' => function ($url, $modal, $key) {
                    if (!Yii::$app->getUser()->can('payment_payment-log_export')) {
                        return false;
                    }
                    return \yii\helpers\Html::a('<span class="glyphicon glyphicon-export"></span>' . Yii::t('twbp', 'Generate payment files'), 'javascript:;', [
                        'title' => Yii::t('twbp', 'Generate payment files'),
                        'onclick' => 'exportPayments(this);',
                        'data-id' => $modal->id,
                        'data-url' => \yii\helpers\Url::to(['/payment/payment-log/export', 'id' => $modal->id])
                    ]);
                },
            ];
            $gridDropdownActions = [
                'export-payment-multiple' => [
                    'label' => '<span class="glyphicon glyphicon-export"></span> ' . Yii::t('twbp', 'Generate payment files'),
                    'url' => 'javascript:',
                    'visible' => Yii::$app->getUser()->can('payment_payment-log_export'),
                    'options' => [
                        'onclick' => 'exportMultiple(this);',
                        'data-url' => \yii\helpers\Url::to(['/payment/payment-log/export']),
                        'data-pjax' => false
                    ],
                ],
            ];
            $this->customActions = ArrayHelper::merge($this->customActions, $customActions);
            $this->gridDropdownActions = ArrayHelper::merge($this->gridDropdownActions, $gridDropdownActions);
        }
    }
}
