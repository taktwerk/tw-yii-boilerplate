<?php
namespace taktwerk\yiiboilerplate\modules\payment\components;

use Digitick\Sepa\PaymentInformation;
use Digitick\Sepa\TransferFile\TransferFileInterface;
use Digitick\Sepa\GroupHeader;
use Digitick\Sepa\TransferInformation\CustomerCreditTransferInformation;
use Digitick\Sepa\TransferInformation\TransferInformationInterface;
use Digitick\Sepa\DomBuilder\CustomerCreditTransferDomBuilder as CustomerCreditTransferDomBuilderBase;

/**
 * Class CustomerCreditTransferDomBuilder tw
 */
class CustomerCreditTransferDomBuilder extends CustomerCreditTransferDomBuilderBase
{

    function __construct(string $painFormat = 'pain.001.002.03', $withSchemaLocation = true)
    {
        parent::__construct($painFormat, $withSchemaLocation);
    }

    /**
     * Build the root of the document
     */
    public function visitTransferFile(TransferFileInterface $transferFile): void
    {
        $this->currentTransfer = $this->doc->createElement('CstmrCdtTrfInitn');
        $this->root->appendChild($this->currentTransfer);
    }

    /**
     * Crawl PaymentInformation containing the Transactions
     */
    public function visitPaymentInformation(PaymentInformation $paymentInformation): void
    {
        $this->currentPayment = $this->createElement('PmtInf');
        $this->currentPayment->appendChild($this->createElement('PmtInfId', $paymentInformation->getId()));
        $this->currentPayment->appendChild($this->createElement('PmtMtd', $paymentInformation->getPaymentMethod()));

        if ($paymentInformation->getBatchBooking() !== null) {
            $this->currentPayment->appendChild($this->createElement('BtchBookg', $paymentInformation->getBatchBooking() ? 'true' : 'false'));
        }

        $this->currentPayment->appendChild(
            $this->createElement('NbOfTxs', $paymentInformation->getNumberOfTransactions())
        );

        $this->currentPayment->appendChild(
            $this->createElement('CtrlSum', $this->intToCurrency($paymentInformation->getControlSumCents()))
        );

        $paymentTypeInformation = $this->createElement('PmtTpInf');
        if ($paymentInformation->getInstructionPriority()) {
            $instructionPriority = $this->createElement('InstrPrty', $paymentInformation->getInstructionPriority());
            $paymentTypeInformation->appendChild($instructionPriority);
        }
        if ($this->painFormat !== 'pain.001.001.03') { // 001.001.03.ch.02, hack tw
            $serviceLevel = $this->createElement('SvcLvl');
            $serviceLevel->appendChild($this->createElement('Cd', 'SEPA'));
            $paymentTypeInformation->appendChild($serviceLevel);
            if ($paymentInformation->getCategoryPurposeCode()) {
                $categoryPurpose = $this->createElement('CtgyPurp');
                $categoryPurpose->appendChild($this->createElement('Cd', $paymentInformation->getCategoryPurposeCode()));
                $paymentTypeInformation->appendChild($categoryPurpose);
            }
            $this->currentPayment->appendChild($paymentTypeInformation);
        }

        if ($paymentInformation->getLocalInstrumentCode()) {
            $localInstrument = $this->createElement('LclInstr');
            $localInstrument->appendChild($this->createElement('Cd', $paymentInformation->getLocalInstrumentCode()));
            $this->currentPayment->appendChild($localInstrument);
        }

        $this->currentPayment->appendChild($this->createElement('ReqdExctnDt', $paymentInformation->getDueDate()));
        $debtor = $this->createElement('Dbtr');
        $debtor->appendChild($this->createElement('Nm', $paymentInformation->getOriginName()));
        $this->currentPayment->appendChild($debtor);

        if ($paymentInformation->getOriginBankPartyIdentification() !== null && $this->painFormat === 'pain.001.001.03') {
            $organizationId = $this->getOrganizationIdentificationElement(
                $paymentInformation->getOriginBankPartyIdentification(),
                $paymentInformation->getOriginBankPartyIdentificationScheme());

            $debtor->appendChild($organizationId);
        }

        $debtorAccount = $this->createElement('DbtrAcct');
        $id = $this->createElement('Id');
        $id->appendChild($this->createElement('IBAN', $paymentInformation->getOriginAccountIBAN()));
        $debtorAccount->appendChild($id);
        if ($paymentInformation->getOriginAccountCurrency()) {
            $debtorAccount->appendChild($this->createElement('Ccy', $paymentInformation->getOriginAccountCurrency()));
        }
        $this->currentPayment->appendChild($debtorAccount);

        $debtorAgent = $this->createElement('DbtrAgt');
        $financialInstitutionId = $this->getFinancialInstitutionElement($paymentInformation->getOriginAgentBIC());
        $debtorAgent->appendChild($financialInstitutionId);
        $this->currentPayment->appendChild($debtorAgent);

        $this->currentPayment->appendChild($this->createElement('ChrgBr', 'SLEV'));
        $this->currentTransfer->appendChild($this->currentPayment);
    }
}