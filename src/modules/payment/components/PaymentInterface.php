<?php

namespace taktwerk\yiiboilerplate\modules\payment\components;

/**
 * PaymentModelInterface is the interface that should be implemented by a class providing payment information.
 *
 * This interface can typically be implemented by any model class. For example, the following
 * code shows how to implement this interface in HfrCase class:
 *
 * ```php
 * class HfrCase extends ActiveRecord implements PaymentModelInterface
 * {
 *
 *     public function getPaymentAmount()
 *     {
 *         return $this->amount; // dummy return = 1000
 *     }
 *
 *     public function getPaymentIBAN()
 *     {
 *         return $this->iban; // dummy return = ‘Test IBAN’
 *     }
 *
 *     public function getPaymentDate()
 *     {
 *         return $this->payment_date; // dummy return = ‘2021-01-30’
 *     }
 *
 *     public function getPaymentDescription()
 *     {
 *         return $this->iban; // dummy return = ‘Test Description’
 *     }
 *
 *     public function getPaymentIdentification()
 *     {
 *         return $this->iban; // dummy return = ‘Test Identification’
 *     }
 *
 * }
 * ```
 */
interface PaymentInterface
{

    /**
     * Returns payment amount
     * @return float|int
     */
    public function getPaymentAmount();

    /**
     * Returns payment iban.
     * @return string
     */
    public function getPaymentIBAN();

    /**
     * Returns payment date.
     * @return string
     */
    public function getPaymentDate();

    /**
     * Returns payment description.
     * @return string
     */
    public function getPaymentDescription();

    /**
     * Returns an payment ID that can uniquely identify a payment identification.
     * @return string
     */
    public function getPaymentIdentification();

}
