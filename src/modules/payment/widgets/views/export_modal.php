<?php
/**@var $model \taktwerk\yiiboilerplate\modules\payment\models\Log */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use Yii;

Modal::begin([
    'size' => Modal::SIZE_DEFAULT,
    'id' => 'exportPayment',
    'header' => Yii::t('twbp', 'Payment Files'),
    'clientOptions' => [
        'backdrop' => 'static',
    ],
]);
$model_class = str_replace("\\","\\\\",Yii::$app->controller->model);
?>
    <div class="payment-export-form">
        <div class="row">
            <?= Html::beginForm(['/payment/payment-log/export'], 'POST',['id'=>'payment_export_form']); ?>
            <div class="col-md-12">
                <?=Yii::t('twbp',"Generate payment files");?>.
            </div>

            <?= Html::hiddenInput('ids', '',['id'=>'payment_ids']); ?>
            <?= Html::hiddenInput('download_file_type', '',['id'=>'download_file_type']); ?>
            <?= Html::hiddenInput('model_class',$model_class); ?>

            <div class="col-md-12" style="margin-top: 10px">
                <?= Html::button('<i class="glyphicon glyphicon-export" aria-hidden="true"></i> '.Yii::t('twbp', 'Generate'), ['class' => 'btn btn-success','id'=>'submit_export_button','onclick'=>'downloadExcelFile(this);']); ?>

                <?php /* if(\Yii::$app->user->can('payment_payment-log_export')){?>
                    <?= Html::button('<i class="glyphicon glyphicon-export" aria-hidden="true"></i> '.Yii::t('twbp', 'Export Excel File'), ['class' => 'btn btn-success','id'=>'submit_export_button','onclick'=>'downloadExcelFile(this);']); ?>
                <?php }?>
                <?php if(\Yii::$app->user->can('payment_payment-log_export')){?>
                    <?= Html::button('<i class="glyphicon glyphicon-export" aria-hidden="true"></i> '.Yii::t('twbp', 'Export XML File'), ['class' => 'btn btn-primary','id'=>'xml_export_button','onclick'=>'downloadXMLFile(this);']); ?>
                <?php } */ ?>
            </div>
            <?= Html::endForm(); ?>
        </div>
    </div>
    <?php Modal::end();?>
<?php
$checkUrl = Url::toRoute(['/payment/payment-log/export','exportcheck'=>'yes']);
$js = <<<JS
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
function reloadIfExported(){
    $.ajax({
        url:"{$checkUrl}",
        method:'POST',
        error:function(){
            setTimeout(function(){
                reloadIfExported();},500);
        },
        success:function(data){
            if(data==true){
                setCookie('show_payment_modal','yes',1);
                location.reload();
            }else{
               setTimeout(function(){
                    reloadIfExported();
                },300);
            }
}
});
}
function afterPayExportSubmit(){
    $('#submit_export_button').prop('disabled',true);
    $('#xml_export_button').prop('disabled',true);
    setTimeout(function(){
            reloadIfExported();
        },300);
    setTimeout(hideModal,160);
}
if(getCookie('show_payment_modal')==='yes'){
    setCookie('show_payment_modal','no',1);
    setTimeout(function(){
       if($('#payment-file-modal').length){
        $('#payment-file-modal').modal('show');
       }
    },350);
}
function hideModal(){
     $('#loadingModal').modal('hide');
}
function downloadExcelFile(e){
    $('#download_file_type').val('excel');
    $('#payment_export_form').submit();
    afterPayExportSubmit();
}
function downloadXMLFile(e){
    $('#download_file_type').val('xml');
    $('#payment_export_form').submit();
    afterPayExportSubmit();
}
function exportPayments(elm){
    let id = $(elm).attr('data-id');
    $('#payment_ids').val(id);
    $('#exportPayment').modal('show');
}
function exportMultiple(elm){
    var element = $(elm),
        keys=$("#" + gridViewKey + "-grid").yiiGridView('getSelectedRows'),
        url = element.attr('data-url');
    if(keys.length>=1){
        $('#payment_ids').val(keys);
        $('#exportPayment').modal('show');
    }
}
JS;

$this->registerJs($js, \yii\web\View::POS_HEAD,'bp-payment-export-modal-js');