<?php

namespace taktwerk\yiiboilerplate\modules\payment\widgets;

use taktwerk\yiiboilerplate\modules\payment\models\Log;
use yii\base\Widget;

class ExportButtonPopup extends Widget
{
    /**
     * @return string|void
     */
    public function run()
    {
        return $this->render('@taktwerk-boilerplate/modules/payment/widgets/views/export_modal');
    }
}
