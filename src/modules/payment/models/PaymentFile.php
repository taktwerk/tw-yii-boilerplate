<?php
//Generation Date: 05-Jan-2021 08:41:22am
namespace taktwerk\yiiboilerplate\modules\payment\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\payment\models\base\PaymentFile as BasePaymentFile;

/**
 * This is the model class for table "payment_file".
 */
class PaymentFile extends BasePaymentFile
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

}
