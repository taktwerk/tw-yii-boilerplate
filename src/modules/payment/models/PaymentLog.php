<?php
//Generation Date: 05-Jan-2021 08:41:22am
namespace taktwerk\yiiboilerplate\modules\payment\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\payment\models\base\PaymentLog as BasePaymentLog;

/**
 * This is the model class for table "payment_log".
 */
class PaymentLog extends BasePaymentLog
{
    const SENDER_NAME = 'Test Setting Company';

    const SENDER_IBAN = 'Test Setting IBAN';

    const SENDER_BIC = 'Test Setting BIC';

    const CURRENCY = 'CHF';

    const REAL_TIME = true;

    const MESSAGE_ID = 'Test Setting ID';

//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

}
