<?php
//Generation Date: 07-Jan-2021 11:46:21am
namespace taktwerk\yiiboilerplate\modules\payment\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\payment\models\base\PaymentSetting as BasePaymentSetting;

/**
 * This is the model class for table "payment_setting".
 */
class PaymentSetting extends BasePaymentSetting
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

}
