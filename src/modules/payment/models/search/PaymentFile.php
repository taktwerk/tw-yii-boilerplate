<?php
//Generation Date: 05-Jan-2021 08:41:29am
namespace taktwerk\yiiboilerplate\modules\payment\models\search;

use taktwerk\yiiboilerplate\modules\payment\models\search\base\PaymentFile as PaymentFileSearchModel;

/**
* PaymentFile represents the model behind the search form about `taktwerk\yiiboilerplate\modules\payment\models\PaymentFile`.
*/
class PaymentFile extends PaymentFileSearchModel{

}