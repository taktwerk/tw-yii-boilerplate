<?php
//Generation Date: 05-Jan-2021 08:41:29am
namespace taktwerk\yiiboilerplate\modules\payment\models\search;

use taktwerk\yiiboilerplate\modules\payment\models\search\base\PaymentLog as PaymentLogSearchModel;

/**
* PaymentLog represents the model behind the search form about `taktwerk\yiiboilerplate\modules\payment\models\PaymentLog`.
*/
class PaymentLog extends PaymentLogSearchModel{

}