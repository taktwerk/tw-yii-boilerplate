<?php
//Generation Date: 05-Jan-2021 08:56:59am
namespace taktwerk\yiiboilerplate\modules\payment\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\payment\models\PaymentLog as PaymentLogModel;

/**
 * PaymentLog represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\payment\models\PaymentLog`.
 */
class PaymentLog extends PaymentLogModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'reference',
                    'amount',
                    'iban',
                    'payment_date',
                    'description',
                    'identification',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'reference_class',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$showDeleted = (property_exists(\Yii::$app->controller, 'model')
            &&
            (is_subclass_of(PaymentLogModel::class, \Yii::$app->controller->model) 
            	||
            	is_subclass_of(\Yii::$app->controller->model, PaymentLogModel::class))
            &&
              (Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model)
                ||
                Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model)))
            );
        $query = PaymentLogModel::find((Yii::$app->get('userUi')->get('show_deleted', PaymentLogModel::class) === '1'|| $showDeleted)?false:true);

        $this->parseSearchParams(PaymentLogModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(PaymentLogModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(PaymentLogModel::class),
                'params' => [
                    'page' => $this->parsePageParams(PaymentLogModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('reference', $query);
        $this->applyHashOperator('amount', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('iban', $query);
        $this->applyLikeOperator('description', $query);
        $this->applyLikeOperator('identification', $query);
        $this->applyLikeOperator('reference_class', $query);
        $this->applyDateOperator('payment_date', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(PaymentLogModel::class);
    }
}
