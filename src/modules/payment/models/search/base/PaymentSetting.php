<?php
//Generation Date: 15-Jan-2021 11:13:11am
namespace taktwerk\yiiboilerplate\modules\payment\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\payment\models\PaymentSetting as PaymentSettingModel;

/**
 * PaymentSetting represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\payment\models\PaymentSetting`.
 */
class PaymentSetting extends PaymentSettingModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'sender_name',
                    'sender_iban',
                    'sender_bic',
                    'currency',
                    'client_id',
                    'is_real_time',
                    'is_active',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'pay_day',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$showDeleted = (property_exists(\Yii::$app->controller, 'model')
            &&
            (is_subclass_of(PaymentSettingModel::class, \Yii::$app->controller->model) 
            	||
            	is_subclass_of(\Yii::$app->controller->model, PaymentSettingModel::class))
            &&
              (Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model)
                ||
                Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model)))
            );
        $query = PaymentSettingModel::findWithOutSystemEntry((Yii::$app->get('userUi')->get('show_deleted', PaymentSettingModel::class) === '1'|| $showDeleted)?false:true);

        $this->parseSearchParams(PaymentSettingModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(PaymentSettingModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(PaymentSettingModel::class),
                'params' => [
                    'page' => $this->parsePageParams(PaymentSettingModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('client_id', $query);
        $this->applyHashOperator('is_real_time', $query);
        $this->applyHashOperator('is_active', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyHashOperator('pay_day', $query);
        $this->applyLikeOperator('sender_name', $query);
        $this->applyLikeOperator('sender_iban', $query);
        $this->applyLikeOperator('sender_bic', $query);
        $this->applyLikeOperator('currency', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(PaymentSettingModel::class);
    }
}
