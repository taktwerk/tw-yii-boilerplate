<?php
//Generation Date: 08-Feb-2021 05:46:46am
namespace taktwerk\yiiboilerplate\modules\payment\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\payment\models\PaymentFile as PaymentFileModel;

/**
 * PaymentFile represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\payment\models\PaymentFile`.
 */
class PaymentFile extends PaymentFileModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'excel_file',
                    'xml_file',
                    'pdf_file',
                    'pdf_file_filemeta',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'reference_class',
                    'excel_file_filemeta',
                    'xml_file_filemeta',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$showDeleted = (property_exists(\Yii::$app->controller, 'model')
            &&
            (is_subclass_of(PaymentFileModel::class, \Yii::$app->controller->model) 
            	||
            	is_subclass_of(\Yii::$app->controller->model, PaymentFileModel::class))
            &&
              (Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model)
                ||
                Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model)))
            );
        $query = PaymentFileModel::find((Yii::$app->get('userUi')->get('show_deleted', PaymentFileModel::class) === '1'|| $showDeleted)?false:true);

        $this->parseSearchParams(PaymentFileModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(PaymentFileModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(PaymentFileModel::class),
                'params' => [
                    'page' => $this->parsePageParams(PaymentFileModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('excel_file', $query);
        $this->applyLikeOperator('xml_file', $query);
        $this->applyLikeOperator('pdf_file', $query);
        $this->applyLikeOperator('pdf_file_filemeta', $query);
        $this->applyLikeOperator('reference_class', $query);
        $this->applyLikeOperator('excel_file_filemeta', $query);
        $this->applyLikeOperator('xml_file_filemeta', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(PaymentFileModel::class);
    }
}
