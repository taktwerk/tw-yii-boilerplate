<?php
//Generation Date: 07-Jan-2021 11:46:22am
namespace taktwerk\yiiboilerplate\modules\payment\models\search;

use taktwerk\yiiboilerplate\modules\payment\models\search\base\PaymentSetting as PaymentSettingSearchModel;

/**
* PaymentSetting represents the model behind the search form about `taktwerk\yiiboilerplate\modules\payment\models\PaymentSetting`.
*/
class PaymentSetting extends PaymentSettingSearchModel{

}