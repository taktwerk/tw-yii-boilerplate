<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%payment_setting}}`.
 */
class m210108_064711_create_payment_setting_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%payment_setting}}', [
            'id' => $this->primaryKey(),
            'sender_name' => $this->string(255)->notNull(),
            'sender_iban' => $this->string(255)->notNull(),
            'sender_bic' => $this->string(255)->notNull(),
            'currency' => $this->string(3)->notNull(),
            'message_identification' => $this->string(255)->notNull(),
            'client_id' => $this->integer()->null(),
            'is_real_time' => $this->tinyInteger(1)->defaultValue(0),
            'is_active' => $this->tinyInteger(1)->defaultValue(0),
        ],$tableOptions);
        $this->addCommentOnTable('{{%payment_setting}}','{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\payment"}');
        $this->createIndex('idx_payment_setting_deleted_at_client_id', '{{%payment_setting}}', ['deleted_at','client_id']);
        $this->addForeignKey('fk_payment_setting_client_id', '{{%payment_setting}}', 'client_id', '{{%client}}', 'id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            $this->dropTable('{{%payment_setting}}');
    }
}
