<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%payment_file}}`.
 */
class m210210_043510_alter_amount_to_decimal extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%payment_log}}', 'amount', $this->decimal(14,2));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
