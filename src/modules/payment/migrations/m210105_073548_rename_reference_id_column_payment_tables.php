<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m210105_073548_rename_reference_id_column_payment_tables extends TwMigration
{
    public function up()
    {
        $this->renameColumn('{{%payment_log}}','reference_id','reference');
        $this->renameColumn('{{%payment_file}}','reference_id','reference');
        $this->addColumn('{{%payment_file}}', 'excel_file_filemeta', $this->text());
        $this->addColumn('{{%payment_file}}', 'xml_file_filemeta', $this->text());
    }

    public function down()
    {
        echo "m210105_073548_rename_reference_id_column_payment_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
