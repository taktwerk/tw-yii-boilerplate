<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%payment_setting}}`.
 */
class m210115_091026_payment_setting_table_update extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%payment_setting}}','pay_day', $this->integer(11)->null());
        $this->dropColumn('{{%payment_setting}}','message_identification');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            return false;
    }
}
