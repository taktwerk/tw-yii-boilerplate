<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m210115_094509_add_paymentAdmin_paymentViewer_role extends TwMigration
{
    public function safeUp()
    {
        $this->createCrudControllerPermissions('payment_payment-file');
        $this->createCrudControllerPermissions('payment_payment-setting');
        $this->createCrudControllerPermissions('payment_payment-log');
        $this->createRole('PaymentViewer');
        $this->createPermission('payment_payment-log_export','Payment Export',['PaymentViewer']);
        $this->addSeeControllerPermission('payment_payment-file', ['PaymentViewer']);
        $this->createRole('PaymentAdmin','PaymentViewer');
        $this->addAdminControllerPermission('payment_payment-setting', 'PaymentAdmin');
    }

    public function safeDown()
    {
        echo "m210115_094509_add_paymentAdmin_paymentViewer_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
