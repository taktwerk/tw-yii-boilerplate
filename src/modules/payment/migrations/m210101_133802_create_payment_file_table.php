<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%payment_file}}`.
 */
class m210101_133802_create_payment_file_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%payment_file}}', [
            'id' => $this->primaryKey(),
            'reference_id' => $this->integer()->notNull(),
            'excel_file' => $this->string(255)->null(),
            'xml_file' => $this->string(255)->null(),
        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            $this->dropTable('{{%payment_file}}');
    }
}
