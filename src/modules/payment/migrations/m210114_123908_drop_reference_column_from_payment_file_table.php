<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles dropping columns from table `{{%payment_file}}`.
 */
class m210114_123908_drop_reference_column_from_payment_file_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%payment_file}}', 'reference');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%payment_file}}', 'reference', $this->integer(11));
    }
}
