<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the dropping of table `{{%payment_setting}}`.
 */
class m210108_064553_drop_payment_setting_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%payment_setting}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210108_064553_drop_payment_setting_table cannot be reverted.\n";

        return false;
    }
}
