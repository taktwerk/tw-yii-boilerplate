<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%payment_log}}`.
 */
class m210101_124439_create_payment_log_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%payment_log}}', [
            'id' => $this->primaryKey(),
            'reference_id' => $this->integer()->null(),
            'amount' => $this->float()->null(),
            'iban' => $this->string(255)->null(),
            'date' => $this->date()->null(),
            'description' => $this->text()->null(),
            'identification' => $this->string(255)->null(),
        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            $this->dropTable('{{%payment_log}}');
    }
}
