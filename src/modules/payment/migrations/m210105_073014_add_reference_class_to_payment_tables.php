<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m210105_073014_add_reference_class_to_payment_tables extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%payment_log}}','reference_class',$this::string(255));
        $this->addColumn('{{%payment_file}}','reference_class',$this::string(255));
    }

    public function down()
    {
        echo "m210105_073014_add_reference_class_to_payment_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
