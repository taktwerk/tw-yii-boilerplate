<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m210105_075555_rename_date_column_payment_tables extends TwMigration
{
    public function up()
    {

        $this->renameColumn('{{%payment_log}}','date','payment_date');
    }

    public function down()
    {
        echo "m210105_075555_rename_date_column_payment_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
