<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m210105_072957_add_namespace_comment_to_payment_tables extends TwMigration
{
    public function up()
    {

        $this->addCommentOnTable('payment_log','{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\payment"}');
        $this->addCommentOnTable('payment_file','{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\payment"}');
    }

    public function down()
    {
        echo "m210105_072957_add_namespace_comment_to_payment_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
