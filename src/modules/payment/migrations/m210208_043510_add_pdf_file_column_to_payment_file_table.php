<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%payment_file}}`.
 */
class m210208_043510_add_pdf_file_column_to_payment_file_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%payment_file}}', 'pdf_file', $this->string(255)
            ->comment('{"fileSize":"","allowedExtensions":["pdf"],"inputtype":"file"}')->after('xml_file')->null());
        $this->addColumn('{{%payment_file}}', 'pdf_file_filemeta', $this->text()->after('pdf_file'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            $this->dropColumn('{{%payment_file}}', 'pdf_file');
    }
}
