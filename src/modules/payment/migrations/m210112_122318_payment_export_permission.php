<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m210112_122318_payment_export_permission extends TwMigration
{
    public function up()
    {
        $this->createPermission('payment_payment-log_export','Payment Export',['Payment']);
    }

    public function down()
    {
        echo "m210112_122318_payment_export_permission cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
