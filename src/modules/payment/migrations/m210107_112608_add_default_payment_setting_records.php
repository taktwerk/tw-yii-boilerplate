<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;
use yii\db\Expression;
class m210107_112608_add_default_payment_setting_records extends TwMigration
{
    public function up()
    {
        $this->insert('{{%payment_setting}}', [
            'key' => 'SENDER_NAME',
            'value' => 'Test Setting Company',
            'is_active' => 1,
            'created_at' => new Expression("NOW()"),
        ]);
        $this->insert('{{%payment_setting}}', [
            'key' => 'SENDER_IBAN',
            'value' => 'Test Setting IBAN',
            'is_active' => 1,
            'created_at' => new Expression("NOW()"),
        ]);
        $this->insert('{{%payment_setting}}', [
            'key' => 'SENDER_BIC',
            'value' => 'Test Setting BIC',
            'is_active' => 1,
            'created_at' => new Expression("NOW()"),
        ]);
        $this->insert('{{%payment_setting}}', [
            'key' => 'CURRENCY',
            'value' => 'CHF',
            'is_active' => 1,
            'created_at' => new Expression("NOW()"),
        ]);
        $this->insert('{{%payment_setting}}', [
            'key' => 'REAL_TIME',
            'value' => true,
            'is_active' => 1,
            'created_at' => new Expression("NOW()"),
        ]);
        $this->insert('{{%payment_setting}}', [
            'key' => 'MESSAGE_ID',
            'value' => 'Test Setting ID',
            'is_active' => 1,
            'created_at' => new Expression("NOW()"),
        ]);
    }

    public function down()
    {
        echo "m210107_112608_add_default_payment_setting_records cannot be reverted.\n";

        return false;
    }
}
