<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%payment_setting}}`.
 */
class m210107_101949_create_payment_setting_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('{{%payment_setting}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(255)->notNull(),
            'value' => $this->string(255)->notNull(),
            'is_active' => $this->tinyInteger(1)->defaultValue(0),
        ],$tableOptions);
        $this->addCommentOnTable('{{%payment_setting}}','{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\payment"}');
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            $this->dropTable('{{%payment_setting}}');
    }
}
