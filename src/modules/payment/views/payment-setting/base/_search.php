<?php
//Generation Date: 15-Jan-2021 11:13:11am
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\payment\models\search\PaymentSetting $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="payment-setting-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'sender_name') ?>

        <?= $form->field($model, 'sender_iban') ?>

        <?= $form->field($model, 'sender_bic') ?>

        <?= $form->field($model, 'currency') ?>

        <?php // echo $form->field($model, 'client_id') ?>

        <?php // echo $form->field($model, 'is_real_time') ?>

        <?php // echo $form->field($model, 'is_active') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

        <?php // echo $form->field($model, 'pay_day') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
