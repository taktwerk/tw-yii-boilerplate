<?php
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\modules\payment\models\PaymentFile;
use yii\grid\GridView;
use yii\helpers\Html;
use taktwerk\yiiboilerplate\models\User;

if(Yii::$app->getUser()->can('payment_payment-log_export')){
Modal::begin([
    'id'=>'payment-file-modal',
    'header' => '<h2>'.\Yii::t('twbp', 'Payment Files').'</h2>',
    'toggleButton' => false,
]);

echo '<p>' . \Yii::t('twbp','SEPA XML file (pain Format) to directly import payments to your E-Banking, PDF to control payments visually and Excel for manual processing') . '</p>';

if(!isset($query) && isset($referenceClass)){
    $query = PaymentFile::find()->andWhere(['reference_class'=>$referenceClass])
    ->orderBy(['created_at' => SORT_DESC])
    ->limit(5);
}
if(!isset($query)){
    $query = PaymentFile::find()->orderBy(['created_at' => SORT_DESC])->limit(5);
}
$provider = new ActiveDataProvider([
    'query' => $query,
    'sort' => false,
    'pagination'=>false
]);
echo GridView::widget([
    'dataProvider' => $provider,
    'summary'=>false,
    'columns'=>[
        [
            'label' => \Yii::t('twbp', 'Name'),
            'format' => 'raw',
            'value' => function ($m) {
                $path_parts = pathinfo($m->excel_file);
                return $path_parts['filename'];
            }
        ],
        [
            'label' => 'XML',
            'format' => 'raw',
            'value' => function ($m) {
                return Html::a('<i class="fa fa-download"></i>', $m->getFileUrl('xml_file'), [
                    'target' => '_blank',
                    'download'=>true,
                    'class'=>'btn btn-primary',
                    'title'=>\Yii::t('twbp','Download'),
                    'data' => [
                        'pjax' => 0
                    ]
                ]);
            }
        ],
        [
            'label' => 'PDF',
            'format' => 'raw',
            'value' => function ($m) {
                if($m->pdf_file){
                    return Html::a('<i class="fa fa-download"></i>', $m->getFileUrl('pdf_file'), [
                        'target' => '_blank',
                        'download'=>true,
                        'class'=>'btn btn-danger',
                        'title'=>\Yii::t('twbp','Download'),
                        'data' => [
                            'pjax' => 0
                        ]
                    ]);
                }
            }
        ],
        [
            'label' => 'Excel',
            'format' => 'raw',
            'value' => function ($m) {
                return Html::a('<i class="fa fa-download"></i>', $m->getFileUrl('excel_file'), [
                    'target' => '_blank',
                    'class'=>'btn btn-success',
                    'download'=>true,
                    'title'=>\Yii::t('twbp','Download'),
                    'data' => [
                        'pjax' => 0
                    ]
                ]);
            }
        ],
        'created_at:datetime',
        'created_by' => [
            'label' => \Yii::t('twbp', 'Created By'),
            'value' => function ($m) {
                $user = User::findOne($m->created_by);
                return $user->toString();
            }
        ]
    ]
]);

echo Html::a(\Yii::t('twbp', 'List all entries'),['/payment/payment-file'],['class'=>'btn btn-success']);
Modal::end();
}
