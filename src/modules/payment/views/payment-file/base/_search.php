<?php
//Generation Date: 08-Feb-2021 05:46:46am
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\payment\models\search\PaymentFile $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="payment-file-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'excel_file') ?>

        <?= $form->field($model, 'xml_file') ?>

        <?= $form->field($model, 'pdf_file') ?>

        <?= $form->field($model, 'pdf_file_filemeta') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

        <?php // echo $form->field($model, 'reference_class') ?>

        <?php // echo $form->field($model, 'excel_file_filemeta') ?>

        <?php // echo $form->field($model, 'xml_file_filemeta') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
