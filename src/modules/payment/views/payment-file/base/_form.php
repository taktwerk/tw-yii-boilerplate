<?php
//Generation Date: 08-Feb-2021 05:46:46am
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\widget\form\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\components\ClassDispenser;
/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\payment\models\PaymentFile $model
 * @var taktwerk\yiiboilerplate\widget\form\ActiveForm $form
 * @var boolean $useModal
 * @var boolean $multiple
 * @var array $pk
 * @var array $show
 * @var string $action
 * @var string $owner
 * @var array $languageCodes
 * @var boolean $relatedForm to know if this view is called from ajax related-form action. Since this is passing from
 * select2 (owner) it is always inherit from main form
 * @var string $relatedType type of related form, like above always passing from main form
 * @var string $owner string that representing id of select2 from which related-form action been called. Since we in
 * each new form appending "_related" for next opened form, it is always unique. In main form it is always ID without
 * anything appended
 */

$owner = $relatedForm ? '_' . $owner . '_related' : '';
$relatedTypeForm = Yii::$app->request->get('relatedType')?:$relatedTypeForm;

if (!isset($multiple)) {
$multiple = false;
}

if (!isset($relatedForm)) {
$relatedForm = false;
}

$tableHint = (!empty(taktwerk\yiiboilerplate\modules\payment\models\PaymentFile::tableHint()) ? '<div class="table-hint">' . taktwerk\yiiboilerplate\modules\payment\models\PaymentFile::tableHint() . '</div><hr />' : '<br />');

?>
<div class="payment-file-form">
        <?php  $formId = 'PaymentFile' . ($ajax || $useModal ? '_ajax_' . $owner : ''); ?>    
    <?php $form = ActiveForm::begin([
        'id' => $formId,
        'layout' => 'default',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'action' => $useModal ? $action : '',
        'options' => [
        'name' => 'PaymentFile',
    'enctype' => 'multipart/form-data'
        
        ],
    ]); ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>
        <?php echo $tableHint; ?>

        <?php if ($multiple) : ?>
            <?=Html::hiddenInput('update-multiple', true)?>
            <?php foreach ($pk as $id) :?>
                <?=Html::hiddenInput('pk[]', $id)?>
            <?php endforeach;?>
        <?php endif;?>

        <?php $fieldColumns = [
                
            'excel_file' => 
            $form->field(
                $model,
                'excel_file',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'excel_file') . $owner
                    ]
                ]
            )->widget(
                ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\FileInput::class),
                [
                    'pluginOptions' => [
                        'showUpload' => false,
                        'showRemove' => false,
                        
                        
                                   'initialPreview' => (!empty($model->excel_file) ? [
                            !empty($model->getMinFilePath('excel_file')  && $model->getFileType('excel_file') == 'video') ? $model->getMinFilePath('excel_file') : $model->getFileUrl('excel_file',[],true)
                        ] : ''),
                        'initialCaption' => $model->excel_file,
                        'initialPreviewAsData' => true,
                        'initialPreviewFileType' => $model->getFileType('excel_file'),
                        'fileActionSettings' => [
                            'indicatorNew' => $model->excel_file === null ? '' : '<a href=" ' . (!empty($model->getMinFilePath('excel_file') && $model->getFileType('excel_file') == 'video') ? $model->getMinFilePath('excel_file') : $model->getFileUrl('excel_file')) . '" target="_blank"><i class="glyphicon glyphicon-hand-down text-warning"></i></a>',
                            'indicatorNewTitle' => \Yii::t('twbp','Download'),
                            'showDrag'=>false,
                        ],
                        'overwriteInitial' => true,
                         'initialPreviewConfig'=> [
                                                     [
                                                         'url' => Url::toRoute(['delete-file','id'=>$model->getPrimaryKey(), 'attribute' => 'excel_file']),
                                                         'downloadUrl'=> !empty($model->getMinFilePath('excel_file')  && $model->getFileType('excel_file') == 'video') ? $model->getMinFilePath('excel_file') : $model->getFileUrl('excel_file'),
                                                         'filetype' => !empty($model->getMinFilePath('excel_file') && $model->getFileType('excel_file') == 'video') ? 'video/mp4':'',
                                                     ],
                                                 ],
                    ],
                     'pluginEvents' => [
                         'fileclear' => 'function() { var prev = $("input[name=\'" + $(this).attr("name") + "\']")[0]; $(prev).val(-1); }',
                        
                        'filepredelete'=>'function(xhr){ 
                               var abort = true; 
                               if (confirm("Are you sure you want to remove this file? This action will delete the file even without pressing the save button")) { 
                                   abort = false; 
                               } 
                               return abort; 
                        }', 
                     ],
                    'options' => [
                        'id' => Html::getInputId($model, 'excel_file') . $owner
                    ]
                ]
            )
            ->hint($model->getAttributeHint('excel_file')),
            'xml_file' => 
            $form->field(
                $model,
                'xml_file',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'xml_file') . $owner
                    ]
                ]
            )->widget(
                ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\FileInput::class),
                [
                    'pluginOptions' => [
                        'showUpload' => false,
                        'showRemove' => false,
                        
                        
                                   'initialPreview' => (!empty($model->xml_file) ? [
                            !empty($model->getMinFilePath('xml_file')  && $model->getFileType('xml_file') == 'video') ? $model->getMinFilePath('xml_file') : $model->getFileUrl('xml_file',[],true)
                        ] : ''),
                        'initialCaption' => $model->xml_file,
                        'initialPreviewAsData' => true,
                        'initialPreviewFileType' => $model->getFileType('xml_file'),
                        'fileActionSettings' => [
                            'indicatorNew' => $model->xml_file === null ? '' : '<a href=" ' . (!empty($model->getMinFilePath('xml_file') && $model->getFileType('xml_file') == 'video') ? $model->getMinFilePath('xml_file') : $model->getFileUrl('xml_file')) . '" target="_blank"><i class="glyphicon glyphicon-hand-down text-warning"></i></a>',
                            'indicatorNewTitle' => \Yii::t('twbp','Download'),
                            'showDrag'=>false,
                        ],
                        'overwriteInitial' => true,
                         'initialPreviewConfig'=> [
                                                     [
                                                         'url' => Url::toRoute(['delete-file','id'=>$model->getPrimaryKey(), 'attribute' => 'xml_file']),
                                                         'downloadUrl'=> !empty($model->getMinFilePath('xml_file')  && $model->getFileType('xml_file') == 'video') ? $model->getMinFilePath('xml_file') : $model->getFileUrl('xml_file'),
                                                         'filetype' => !empty($model->getMinFilePath('xml_file') && $model->getFileType('xml_file') == 'video') ? 'video/mp4':'',
                                                     ],
                                                 ],
                    ],
                     'pluginEvents' => [
                         'fileclear' => 'function() { var prev = $("input[name=\'" + $(this).attr("name") + "\']")[0]; $(prev).val(-1); }',
                        
                        'filepredelete'=>'function(xhr){ 
                               var abort = true; 
                               if (confirm("Are you sure you want to remove this file? This action will delete the file even without pressing the save button")) { 
                                   abort = false; 
                               } 
                               return abort; 
                        }', 
                     ],
                    'options' => [
                        'id' => Html::getInputId($model, 'xml_file') . $owner
                    ]
                ]
            )
            ->hint($model->getAttributeHint('xml_file')),
            'pdf_file' => 
            $form->field(
                $model,
                'pdf_file',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'pdf_file') . $owner
                    ]
                ]
            )->widget(
                ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\FileInput::class),
                [
                    'pluginOptions' => [
                        'showUpload' => false,
                        'showRemove' => false,
                        
                        
                        'allowedFileExtensions' => ["pdf"],
                                   'initialPreview' => (!empty($model->pdf_file) ? [
                            !empty($model->getMinFilePath('pdf_file')  && $model->getFileType('pdf_file') == 'video') ? $model->getMinFilePath('pdf_file') : $model->getFileUrl('pdf_file',[],true)
                        ] : ''),
                        'initialCaption' => $model->pdf_file,
                        'initialPreviewAsData' => true,
                        'initialPreviewFileType' => $model->getFileType('pdf_file'),
                        'fileActionSettings' => [
                            'indicatorNew' => $model->pdf_file === null ? '' : '<a href=" ' . (!empty($model->getMinFilePath('pdf_file') && $model->getFileType('pdf_file') == 'video') ? $model->getMinFilePath('pdf_file') : $model->getFileUrl('pdf_file')) . '" target="_blank"><i class="glyphicon glyphicon-hand-down text-warning"></i></a>',
                            'indicatorNewTitle' => \Yii::t('twbp','Download'),
                            'showDrag'=>false,
                        ],
                        'overwriteInitial' => true,
                         'initialPreviewConfig'=> [
                                                     [
                                                         'url' => Url::toRoute(['delete-file','id'=>$model->getPrimaryKey(), 'attribute' => 'pdf_file']),
                                                         'downloadUrl'=> !empty($model->getMinFilePath('pdf_file')  && $model->getFileType('pdf_file') == 'video') ? $model->getMinFilePath('pdf_file') : $model->getFileUrl('pdf_file'),
                                                         'filetype' => !empty($model->getMinFilePath('pdf_file') && $model->getFileType('pdf_file') == 'video') ? 'video/mp4':'',
                                                     ],
                                                 ],
                    ],
                     'pluginEvents' => [
                         'fileclear' => 'function() { var prev = $("input[name=\'" + $(this).attr("name") + "\']")[0]; $(prev).val(-1); }',
                        
                        'filepredelete'=>'function(xhr){ 
                               var abort = true; 
                               if (confirm("Are you sure you want to remove this file? This action will delete the file even without pressing the save button")) { 
                                   abort = false; 
                               } 
                               return abort; 
                        }', 
                     ],
                    'options' => [
                        'id' => Html::getInputId($model, 'pdf_file') . $owner
                    ]
                ]
            )
            ->hint($model->getAttributeHint('pdf_file')),
            'reference_class' => 
            $form->field(
                $model,
                'reference_class',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'reference_class') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('reference_class'),
                            'id' => Html::getInputId($model, 'reference_class') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('reference_class')),]; ?>        <div class='clearfix'></div>
<?php $twoColumnsForm = true; ?>
<?php 
// form fields overwrite
$fieldColumns = Yii::$app->controller->crudColumnsOverwrite($fieldColumns, 'form', $model, $form, $owner);

foreach($fieldColumns as $attribute => $fieldColumn) {
             if($model->checkIfHidden($attribute))
            {
                echo $fieldColumn;
            }
            else if(!$model->isNewRecord || !$model->checkIfCanvas($attribute))
            {
                if (!$multiple || ($multiple && isset($show[$attribute]))) {
                    if ((isset($show[$attribute]) && $show[$attribute]) || !isset($show[$attribute])) {
                        echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
                        echo $fieldColumn;
                        echo $twoColumnsForm ? '</div>' : '';
                    }else{
                        echo $fieldColumn;
                    }
                }
            }
                
} ?><div class='clearfix'></div>

                <?php $this->endBlock(); ?>
        
        <?= ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_TAB) ?
            Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                    'label' => Yii::t('twbp', 'Payment File'),
                    'content' => $this->blocks['main'],
                    'active' => true,
                ],
                ]
            ])
            : $this->blocks['main']
        ?>        
        <div class="col-md-12">
        <hr/>
        
        </div>
        
        <div class="clearfix"></div>
        <?php echo $form->errorSummary($model); ?>
        <?php echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::formButtons($model, $useModal, $relatedForm, $multiple, "twbp", ['id' => $model->id]);?>
        <?php ClassDispenser::getMappedClass(ActiveForm::class)::end(); ?>
        <?= ($relatedForm && $relatedType == ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL) ?
        '<div class="clearfix"></div>' :
        ''
        ?>
        <div class="clearfix"></div>
    </div>
</div>

<?php
if ($useModal) {
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::class)::register($this);
}
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
if(!isset($useDependentFieldsJs) || $useDependentFieldsJs==true){
$this->render('@taktwerk-views/_dependent-fields', ['model' => $model,'formId'=>$formId]);
}
?>