<?php
use Yii;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use taktwerk\yiiboilerplate\models\User;
use taktwerk\yiiboilerplate\components\ImageHelper;
/**
 *
 * @var $client taktwerk\yiiboilerplate\modules\customer\models\Client;
 * @var $paymentLogs taktwerk\yiiboilerplate\modules\payment\models\PaymentLog[];
 */
?>
<?php
if ($client) {
    ?>
<div style="text-align: right;">
    	<?php
    if ($client->logo_file && Yii::$app->fs->has($client->getUploadPath() . '/' . $client->logo_file)) {
        $cachePath = ImageHelper::processImage($client->getUploadPath() . '/' . $client->logo_file, [
            'width' => '350'
        ]);
        $logoContent = Yii::$app->fs_assetsprod->read($cachePath);
        $logoExt = pathinfo($client->logo_file, PATHINFO_EXTENSION);
        $logo = 'data:image/' . $logoExt . ';base64,' . base64_encode($logoContent);
        echo Html::img($logo, [
            'style' => [
                'height' => '50px'
            ]
        ]);
    }
    ?>
</div>
<div style="font-size: 9pt">
	<div style="margin-bottom: 10px">
		<h2><?=Yii::t('twbp', 'Payment Overview');?></h2>
	</div>
<?php }?>

<?php $paymentFile->refresh();?>
<div style="margin-bottom: 10px">
		<table
			style="list-style: none; padding: 0; margin: 0; display: table; font-size: 9pt">
        <?php
        $user = User::findOne($paymentFile->created_by);
        $meta = [
            [
                \Yii::t('twbp', 'Created At'),
                \Yii::$app->formatter->asDate($paymentFile->created_at)
            ],
            [
                \Yii::t('twbp', 'Created By'),
                ($user) ? $user->toString() : ''
            ],
            [
                \Yii::t('twbp', 'Related Payment File'),
                $paymentFile->xml_file
            ]
        ]?>
        <?php foreach($meta as $data){?>
            <tr
				style="box-sizing: border-box; border-collapse: collapse;">
				<td style="padding-right: 20px;"><b><?php echo $data[0]?></b></td>
				<td><?php echo $data[1]?></td>
			</tr>
        <?php }?>
    </table>
	</div>

<?php
$colHeaderOp = [
    'style' => 'background: #e6e4e4;padding: 5px 10px; font-size: 9pt;'
];
$colOp = [
    'style' => 'padding: 5px 10px;border-bottom: 1px solid #e6e4e4; font-size: 9pt;'
];
$total = 0;

// for debugging several pages
/* $paymentLogs = array_merge($paymentLogs, $paymentLogs);
$paymentLogs = array_merge($paymentLogs, $paymentLogs);
$paymentLogs = array_merge($paymentLogs, $paymentLogs);
$paymentLogs = array_merge($paymentLogs, $paymentLogs);
$paymentLogs = array_merge($paymentLogs, $paymentLogs);
$paymentLogs = array_merge($paymentLogs, $paymentLogs); */

foreach ($paymentLogs as $i => $log) {
    $total = $total + $log['amount'];
    $paymentLogs[$i]['position'] = $i + 1;
}
?>

	<div id="pay-log-section" class="grid-view">
		<table
			style="width: 100%; text-align: left; box-sizing: border-box; border-collapse: collapse; border-left: 1px solid #e6e4e4; border-right: 1px solid #e6e4e4;">
			<?php
$style1 = "padding: 5px 10px; border-bottom: 1px solid #e6e4e4; font-size: 9pt;";
$style1Left = $style1 . "text-align: left;";
$style1Right = $style1 . "text-align: right;";
$style2 = "padding: 5px 10px; border-bottom: 1px solid #e6e4e4; font-size: 9pt; font-weight: bold; background-color: #e6e4e4;";
?>
			<thead>
				<?php $ths = [];
				$ths[] = Html::tag('th',Yii::t('twbp', 'Payment Identification'),['style'=>'background: #e6e4e4;'.$style1Left]);
				$ths[] = Html::tag('th',Yii::t('twbp', 'Iban'),['style'=>'background: #e6e4e4;'.$style1Left]);
				$ths[] = Html::tag('th',Yii::t('twbp', 'Payment Description'),['style'=>'background: #e6e4e4;'.$style1Left]);
				$ths[] = Html::tag('th',Yii::t('twbp', 'Payment Amount'),['style'=>'background: #e6e4e4;'.$style1Right]);
				$ths[] = Html::tag('th',Yii::t('twbp', 'Payment Position'),['style'=>'background: #e6e4e4;'.$style1Right]);
				echo Html::tag('tr',implode('', $ths));
				?>
			</thead>
			<tbody>
			<?php foreach ($paymentLogs as $k => $log) { ?>
				<?php
				$tds = [];
				$tds[] = Html::tag('td',$log['identification'],['style'=>$style1Left]);
				$tds[] = Html::tag('td',$log['iban'],['style'=>$style1Left]);
				$tds[] = Html::tag('td',$log['description'],['style'=>$style1]);
				$tds[] = Html::tag('td',\Yii::$app->formatter->asCurrency($log['amount']),['style'=>$style1Right]);
				$tds[] = Html::tag('td',$log['position'],['style'=>$style1Right]);
				echo Html::tag('tr',implode('', $tds),['data'=>['key'=>$k]]);?>
			<?php }?>
				<?php
				$tds = [];
				$tds[] = Html::tag('td',\Yii::t('twbp','Total').':',['colspan'=>3, 'style'=>$style2]);
				$tds[] = Html::tag('td',\Yii::$app->formatter->asCurrency($total),['style'=>$style2.'text-align: right;']);
				$tds[] = Html::tag('td',count($paymentLogs),['style'=>$style2.'text-align: right;']);
				echo Html::tag('tr',implode('', $tds));
				?>
			</tbody>
		</table>
	</div>
</div>