<?php
namespace taktwerk\yiiboilerplate\modules\servercheck;

use taktwerk\yiiboilerplate\components\TwModule;
use taktwerk\yiiboilerplate\components\MenuItemInterface;
use yii\base\InvalidConfigException;
use taktwerk\yiiboilerplate\modules\servercheck\checkers\CheckerInterface;

/**
 * servercheck module definition class
 */
class Module extends TwModule
{

    /*
     * BOOTSTRAP.php CODE
     * $this->registerModule('servercheck',[
     * 'class' => 'taktwerk\yiiboilerplate\modules\servercheck\Module',
     * 'checkerList'=>[
     * 'database'=>[
     * 'class'=>'\taktwerk\yiiboilerplate\modules\servercheck\checkers\DatabaseChecker',
     * 'dbInstanceIds'=>['db']
     * ]
     * ]
     * ]);
     */
    /**
     *
     * {@inheritdoc}
     */
    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\servercheck\controllers';

    public $checkerList = [];

    private $defaultCheckerList = [
        'database' => '\taktwerk\yiiboilerplate\modules\servercheck\checkers\DatabaseChecker',
        'file_compressor' => '\taktwerk\yiiboilerplate\modules\servercheck\checkers\FileCompressorChecker',
        'misc' => '\taktwerk\yiiboilerplate\modules\servercheck\checkers\MiscChecker'
    ];

    /**
     *
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
    }

    public function getCheckersList()
    {
        $list = array_merge($this->defaultCheckerList, $this->checkerList);
        if (! isset($list['ldap']) && getenv('LDAP_ENABLED')) {
            $list['ldap'] = '\taktwerk\yiiboilerplate\modules\servercheck\checkers\LdapChecker';
        }

        $checkers = [];
        foreach ($list as $k => $item) {
            $ob = null;
            if (is_string($item)) {
                $ob = new $item();
            } elseif (is_array($item)) {
                if (! isset($item['class'])) {
                    throw new InvalidConfigException('Missing "class" config in the checker ' . $k);
                }
                $option = $item;
                unset($option['class']);
                $ob = new $item['class']($option);
            }
            if (! $ob instanceof CheckerInterface) {
                throw new InvalidConfigException('The checker ' . get_class($ob) . ' must implement the \taktwerk\yiiboilerplate\modules\servercheck\checkers\CheckerInterface');
            }
            $checkers[$k] = $ob;
        }
        return $checkers;
    }
}
