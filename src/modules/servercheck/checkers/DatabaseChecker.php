<?php
namespace taktwerk\yiiboilerplate\modules\servercheck\checkers;

use yii\base\BaseObject;
use yii\helpers\Html;
use yii\db\Connection;

/**
 *
 * @property \yii\db\Connection[] $dbInstanceIds
 */
class DatabaseChecker extends BaseObject implements CheckerInterface
{

    public $fixes = [];

    /**
     *
     * @var \yii\db\Connection[] $dbInstanceIds
     */
    public $dbInstanceIds = [
        'db',
    ];
    protected $supportedDrivers=[
        'mysql'
    ];
    public function init(){
        parent::init();
        foreach($this->dbInstanceIds as $dbId){
            $db = \Yii::$app->get($dbId);
            if(!$db instanceof Connection){
                throw new \Exception("The database id \"$dbId\" is not valid");
            }
            if(!in_array($db->driverName,$this->supportedDrivers)){
                throw new \Exception("This checker doesn't have any checks for \"".$db->driverName . "\" database");
            }
        }
        
    }
    public function getTitle(): string
    {
        return \Yii::t('twbp', 'Database Checker');
    }

    public function getDescription(): string
    {
        return \Yii::t('twbp', 'Database Optimization checker, checks if Query Caching enabled or not and few more things.');
    }

    public function checksPerformed(): array
    {
        $list = [
            \Yii::t('twbp', 'Query Cache enabled'),
            \Yii::t('twbp', 'Schema Cache enabled')
        ];
        return $list;
    }

    public function getFixesList(): array
    {
        return $this->fixes;
    }

    public function check()
    {
        foreach ($this->dbInstanceIds as $dbId) {
            $this->checkDB($dbId);
        }
    }

    private function checkDB($dbInstanceId)
    {
        $query = "show variables like 'query_cache_size';";

        /**
         *
         * @var $db \yii\db\Connection
         */
        $db = \Yii::$app->get($dbInstanceId);
        
        $res = $db->createCommand($query)->queryOne();
        
        if ($res['Value'] < 1) {
            $this->fixes[] = 'Query Cache is not enabled for db instance: <b>' . $dbInstanceId . '</b>. Add something like below to your <i>/etc/mysql/my.cnf</i> to enable query cache 
<pre>
query_cache_type=1
query_cache_size=80M
query_cache_limit=2M
</pre>
and then restart the MySQL service.';
        }elseif($res['Value'] < 80000000){
            $this->fixes[] =  'It\'s recommended to set the MySql Query Cache size to a minimum of 80MB. Add something like below to your <i>/etc/mysql/my.cnf</i> 
<pre>
query_cache_type=1
query_cache_size=80M
query_cache_limit=2M
</pre>';
        }
        if ($db->enableSchemaCache == false) {
            $this->fixes[] = 'Schema Cache is not enabled for db instance <b>' . $dbInstanceId . '</b>: <a target="_blank" href="https://www.yiiframework.com/doc/guide/2.0/en/tutorial-performance-tuning#enable-schema-caching">Here is how to enable it</a>.';
        }
    }
}