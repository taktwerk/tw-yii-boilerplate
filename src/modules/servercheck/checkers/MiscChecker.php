<?php
namespace taktwerk\yiiboilerplate\modules\servercheck\checkers;

use yii\base\BaseObject;


/**
 *
 * @property \yii\db\Connection[] $dbInstanceIds
 */
class MiscChecker extends BaseObject implements CheckerInterface
{

    public $fixes = [];

    public function getTitle(): string
    {
        return \Yii::t('twbp', 'Miscellaneous Checker');
    }

    public function getDescription(): string
    {
        return \Yii::t('twbp', 'Contains miscellaneous checks for a Yii2 app');
    }

    public function checksPerformed(): array
    {
        $list = [
            \Yii::t('twbp', 'Cookie validation enabled and secret key min length'),
            \Yii::t('twbp', 'CSRF validation enabled')
            
        ];
        return $list;
    }

    public function getFixesList(): array
    {
        return $this->fixes;
    }

    public function check()
    {
        $charLen = 8;
        if(\Yii::$app->request->enableCookieValidation==false){
            $this->fixes[] = 'It\'s recommended to set <code>enableCookieValidation</code> to <code>true</code>  and set <code>cookieValidationKey</code> to a minimum of  characters in the <b>request</b> component to ensure cookies are not tampered thus making the app more secure.';
        }
        if(strlen(\Yii::$app->request->cookieValidationKey)<$charLen){
            $this->fixes[] = 'It\'s recommended to set <code>cookieValidationKey</code> to a minimum of ' . $charLen . ' characters in the <b>request</b> component to make your web app more secure.';
        }
        if(\Yii::$app->request->enableCsrfValidation==false){
            $this->fixes[] = 'It\'s recommended to set <code>enableCsrfValidation</code> to <code>true</code>   in the <b>request</b> component to avoid CSRF thus making the app more secure.';
        }
    }
}