<?php
namespace taktwerk\yiiboilerplate\modules\servercheck\checkers;

use yii\base\BaseObject;


/**
 *
 * @property \yii\db\Connection[] $dbInstanceIds
 */
class LdapChecker extends BaseObject implements CheckerInterface
{

    public $fixes = [];

    public function getTitle(): string
    {
        return \Yii::t('twbp', 'Ldap Checker');
    }

    public function getDescription(): string
    {
        return \Yii::t('twbp', 'Checks if the programs required to run ldap are installed or not.');
    }

    public function checksPerformed(): array
    {
        $list = [
            \Yii::t('twbp', 'Extension Ldap installed'),
            \Yii::t('twbp', 'Extension Json installed')
        ];
        return $list;
    }

    public function getFixesList(): array
    {
        return $this->fixes;
    }

    public function check()
    {
        if(!extension_loaded('ldap')){
            $this->fixes[] = \Yii::t('twbp','ldap extension is not installed/enabled. To install use command like <br> {0}',"<pre>
sudo apt-get install php7.3-ldap</pre>
or
<pre>
sudo yum install rh-php73-php-ldap
</pre>");
        }
        if(!extension_loaded('json')){
            $this->fixes[] =  \Yii::t('twbp','json extension is not installed/enabled. To install use command like<br> {0}',"<pre>
sudo apt-get install php-json </pre>
or
<pre>
yum install php-pear php-devel gcc make
pecl install json
</pre>");
       }
    }
}