<?php

namespace taktwerk\yiiboilerplate\modules\servercheck\checkers;

interface CheckerInterface{
    
    /**
     * @desc Title of the Checker
     */
    public function getTitle():string;
    /**
     * @desc Description of the Checker
     */
    public function getDescription():string;
    /**
     * @desc runs the sub-checks
     */
    public function check();
    /**
     * @desc List of sub-checks performed
     */
    public function checksPerformed():array;
    
    /**
     * @desc List of Fixes still pending
     */
    public function getFixesList():array;
}