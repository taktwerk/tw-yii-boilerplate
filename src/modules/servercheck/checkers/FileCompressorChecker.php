<?php
namespace taktwerk\yiiboilerplate\modules\servercheck\checkers;

use yii\base\BaseObject;
use yii\helpers\Html;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use taktwerk\yiiboilerplate\components\Helper;
use taktwerk\yiiboilerplate\helpers\FileHelper;


/**
 *
 * @property \yii\db\Connection[] $dbInstanceIds
 */
class FileCompressorChecker extends BaseObject implements CheckerInterface
{

    public $fixes = [];

    public function getTitle(): string
    {
        return \Yii::t('twbp', 'File Compressor/Converter Checker');
    }

    public function getDescription(): string
    {
        return \Yii::t('twbp', 'Checks if the application required to compress file types like audio, video etc are available on server or not.');
    }

    public function checksPerformed(): array
    {
        $list = [
            \Yii::t('twbp', 'FFmpeg installed'),
            \Yii::t('twbp', 'Imagick installed'),
            \Yii::t('twbp', 'glTF Pipeline installed')
            
        ];
        return $list;
    }

    public function getFixesList(): array
    {
        return $this->fixes;
    }

    public function check()
    {
        $program = 'ffmpeg';
        $helperClass = ClassDispenser::getMappedClass(Helper::class);
        if (!$helperClass::shellProgramExists($program)) {
            $this->fixes[] = 'ffmpeg program is not installed. Please install it: <a target="_blank" href="https://www.ffmpeg.org/">https://www.ffmpeg.org/</a>';
        }
        if (!extension_loaded('imagick')) {
            $this->fixes[] = 'imagick is not installed or enabled. Please install it: <a target="_blank" href="https://www.php.net/manual/en/imagick.setup.php">https://www.php.net/manual/en/imagick.setup.php</a>';
        }else{
            $pdfContent = $this->getPdfContent();
            $tmp_folder = \Yii::getAlias('@runtime/tmp/');
            \yii\helpers\FileHelper::createDirectory($tmp_folder);
            $path = 'servercheckertest';
            $tmp_file = $tmp_folder . md5($path).'.pdf';
            if (file_put_contents($tmp_file, $pdfContent)) {
                chmod($tmp_file,0777);
                $convertResponse = ClassDispenser::getMappedClass(FileHelper::class)::makePdfToImage($tmp_file);
                if($convertResponse['success']==false){
                   $this->fixes[] = 'The imagick program is not able to convert pdf to image.<br>
The exact error was: <pre>' . $convertResponse['error'] . '</pre><br>
Make sure the imagick allows <b>pdf</b> to image conversion. To allow that open <i>/etc/ImageMagick-6/policy.xml</i> (or <i>/etc/ImageMagick/policy.xml</i>)<br>
change line
<pre>' . Html::encode('<policy domain="coder" rights="none" pattern="PDF" />') . '</pre>
to 
<pre>' . Html::encode('<policy domain="coder" rights="read|write" pattern="PDF" />') . '</pre>
<br>And then restart the server';
                }
                //WORKING HERE
            }
            //file_put_contents(\Yii::$app->, $data)
        }
        $program = 'npm';
        if (!$helperClass::shellProgramExists($program)) {
            $this->fixes[] = 'npm program is not installed. Please install it using this commands:<br/>' .
            '`apt-get install nodejs`<br/>'.
            '`apt-get install npm`<br/>'.
            '`npm install npm@latest -g`<br/>';
        }

        $program = 'gltf-pipeline';
        if (!$helperClass::shellProgramExists($program)) {
            $this->fixes[] = 'gltf-pipeline program is not installed. Please install it using `npm install -g gltf-pipeline` command';
        }
    }
    
    public function getPdfContent(){
       $content = <<<EOT
%PDF-1.3
%âãÏÓ

1 0 obj
<<
/Type /Catalog
/Outlines 2 0 R
/Pages 3 0 R
>>
endobj

2 0 obj
<<
/Type /Outlines
/Count 0
>>
endobj

3 0 obj
<<
/Type /Pages
/Count 2
/Kids [ 4 0 R 6 0 R ] 
>>
endobj

4 0 obj
<<
/Type /Page
/Parent 3 0 R
/Resources <<
/Font <<
/F1 9 0 R 
>>
/ProcSet 8 0 R
>>
/MediaBox [0 0 612.0000 792.0000]
/Contents 5 0 R
>>
endobj

5 0 obj
<< /Length 1074 >>
stream
2 J
BT
0 0 0 rg
/F1 0027 Tf
57.3750 722.2800 Td
( A S ) Tj
ET
BT
/F1 0010 Tf
69.2500 664.7040 Td
( just for ) Tj
ET
BT
/F1 0010 Tf
69.2500 569.0880 Td
( And ) Tj
ET
BT
/F1 0010 Tf
69.2500 557.1360 Td
( text) Tj
ET
endstream
endobj

6 0 obj
<<
/Type /Page
/Parent 3 0 R
/Resources <<
/Font <<
/F1 9 0 R 
>>
/ProcSet 8 0 R
>>
/MediaBox [0 0 612.0000 792.0000]
/Contents 7 0 R
>>
endobj

7 0 obj
<< /Length 676 >>
stream
2 J
BT
0 0 0 rg
/F1 0027 Tf
57.3750 722.2800 Td
( Simple ) Tj
ET
BT
/F1 0010 Tf
69.2500 640.8000 Td
( Bor. ) Tj
ET
endstream
endobj

8 0 obj
[/PDF /Text]
endobj

9 0 obj
<<
/Type /Font
/Subtype /Type1
/Name /F1
/BaseFont /Helvetica
/Encoding /WinAnsiEncoding
>>
endobj

10 0 obj
<<
/Creator (Taktwerk \(http://www.taktwerk.com/\))
/Producer (Taktwerk)
/CreationDate (D:20060301072826)
>>
endobj

xref
0 11
0000000000 65535 f
0000000019 00000 n
0000000093 00000 n
0000000147 00000 n
0000000222 00000 n
0000000390 00000 n
0000001522 00000 n
0000001690 00000 n
0000002423 00000 n
0000002456 00000 n
0000002574 00000 n

trailer
<<
/Size 11
/Root 1 0 R
/Info 10 0 R
>>

startxref
2714
%%EOF

EOT;
       return $content;
    }
}