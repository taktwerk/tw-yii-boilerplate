<?php
namespace taktwerk\yiiboilerplate\modules\servercheck\controllers;

use yii\web\Controller;
use yii\web\Response;
use yii\helpers\Json;

/**
 * Default controller for the `servercheck` module
 */
class DefaultController extends Controller
{

    /**
     * Renders the index view for the module
     *
     * @return string
     */
    public function actionIndex()
    {
        $list = $this->module->getCheckersList();
        return $this->render('index', [
            'checkers' => $list
        ]);
    }

    public function actionPerformCheck($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $list = $this->module->getCheckersList();
        if (! isset($list[$id])) {
            throw new \yii\web\NotFoundHttpException('Checker not found');
        }
        /**
         *
         * @var $checker \taktwerk\yiiboilerplate\modules\servercheck\checkers\CheckerInterface
         */
        $checker = $list[$id];

        $checker->check();
        $fixList = $checker->getFixesList();
        
        return [
            'fixes' => $fixList
        ];
    }
}
