<?php
use yii\helpers\Url;
use yii\helpers\Json;
/**
 *
 * @var $checkers \taktwerk\yiiboilerplate\modules\servercheck\checkers\CheckerInterface
 */
$this->title = Yii::t('twbp', 'Server Check');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-md-12"><button class="pull-right btn btn-primary" id="btn-all-checks"><?php echo \Yii::t('twbp','Run All Checks')?></button></div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row space-16">&nbsp;</div>
		<div class="row">
		<?php $i = 1;?>
		<?php foreach($checkers as $key=>$checker){?>
			<div class="col-sm-6 check-parent">
				<div class="thumbnail">
					<div class="caption text-left">
						<h4 id="thumbnail-label" class="text-primary">
							<?php echo $checker->getTitle()?>
						</h4>
						<div class="thumbnail-description smaller"><?php echo $checker->getDescription()?></div>
					</div>
					<div class=""><?php $acId = 'accordion'.$i; $colapseId = 'collapse'.$i;?>
						<div class="panel-group" id="<?php echo $acId;?>">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse"
											data-parent="#<?php echo $acId?>"
											href="#<?php echo $colapseId?>"><?php echo \Yii::t('twbp','Checks Performed')?></a>
									</h4>
								</div>
								<div id="<?php echo $colapseId?>"
									class="panel-collapse collapse">
									<div class="panel-body">
									<?php foreach($checker->checksPerformed() as $item){?><li><?php echo $item?></li><?php }?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php $facId = 'failaccordion'.$i; $fcolapseId = 'failcollapse'.$i;?>
						<div class="panel-group" id="<?php echo $facId;?>"
						style="display: none;">
						<div class="panel panel-default">
							<div class="panel-heading bg-red">
								<h4 class="panel-title text-danger">
									<a class="accordion-toggle text-danger" data-toggle="collapse"
										data-parent="#<?php echo $facId?>"
										href="#<?php echo $fcolapseId?>"><?php echo \Yii::t('twbp', 'Pending Fixes')?> </a>
								</h4>
							</div>
							<div id="<?php echo $fcolapseId?>"
								class="panel-collapse collapse in checker-fixes"
								data-name="<?php echo $key?>">
								<div class="panel-body"></div>
							</div>
						</div>
					</div>
					<div class="caption card-footer text-left">
<button data-name="<?php echo $key?>"
									class="btn btn-warning check-start-btn" title="Start the check"> <?php echo \Yii::t('twbp','Start Check' ) ?> <i class="fa fa-cog fa-spin" style="display: none;"></i>
								</button>
							<i title="Check Passed" class="fa fa-2x fa-check-circle text-success check-passed v-middle" style="display: none;"></i>
							<i title="Check failed" class="fa fa-2x fa-times-circle text-danger check-failed v-middle"style="display: none;"></i>
					</div>
				</div>
			</div>
			<?php $i++;}?>
		</div>
	</div>
</div>

<?php
$checkUrl = Url::toRoute([
    '/servercheck/default/perform-check'
]);
$checkNames = Json::encode(array_keys($checkers));

$js = <<<EOT
function performCheck(checkName){
    //var delay = 1500;
    var btn = $('button.check-start-btn[data-name="'+checkName+'"]');
    var loader = btn.find('i');
    
    loader.show();
    btn.addClass('disabled');
    var el = $('.checker-fixes[data-name="'+checkName+'"]');
    var parentGroup = el.parents('.panel-group:first');
    var checkParent = el.parents('.check-parent:first');
    var failedCheck  = checkParent.find('.check-failed');
    var passedCheck  = checkParent.find('.check-passed')
    
    parentGroup.slideUp(350);
    passedCheck.slideUp(200);
    failedCheck.slideUp(200);

    setTimeout(function(){
        $.ajax({
            url:'{$checkUrl}?id='+checkName,
            type:'get',
            complete:function(){
                loader.hide();
                btn.removeClass('disabled');
                checksDone[checkName] = true;
            },
            success:function(data){
                     if(typeof data=='object'){ 
                         if(data.fixes.length>0){
                          var sub_el = el.find('.panel-body');
                            if(sub_el.length){
                                sub_el.empty();
                                html = '';
                                data.fixes.forEach(function(val){
                                    html += '<li>'+val+'</li>';
                                });
                                sub_el.append(html);
                            }
                            parentGroup.slideDown(500);
                            checkParent.find('.check-failed').slideDown(300);
                            checkParent.find('.check-passed').slideUp(300);
                         }else{
                            parentGroup.slideUp(500);
                            checkParent.find('.check-passed').slideDown(300);
                            checkParent.find('.check-failed').slideUp(300);
                        }
                }
           }
    });
    },1200);
    
}
$('.check-start-btn').click(function(e){
    var checkName = $(this).data('name');
    if(checkName){
        performCheck(checkName);
    }
});
var checksDone = {};
$('#btn-all-checks').click(function(){
       var checkList = $checkNames;
       checkList.forEach(function(checkName){
            checksDone = {...checksDone, [checkName]:false};
            performCheck(checkName);
       });
       $('#btn-all-checks').addClass('disabled');
});

$(document).ajaxComplete(function( event, xhr, settings ) {
    var checkList = $checkNames;
    var allDone = true;
    var i;
    for (i = 0; i < checkList.length; i++) {
        if(checksDone[checkList[i]]==false){
               allDone = false;break;
        }
    }
    if(allDone==true){
        $('#btn-all-checks').removeClass('disabled')
    }
    
});

EOT;
$this->registerJs($js);