<?php

namespace taktwerk\yiiboilerplate\modules\workflow\models\mobile;

use taktwerk\yiiboilerplate\modules\workflow\models\WorkflowTransition as BaseWorkflowTransition;
use taktwerk\yiiboilerplate\traits\MobileDateTrait;

/**
 * This is the model class for table "workflow_step".
 */
class WorkflowTransition extends BaseWorkflowTransition
{
    use MobileDateTrait;

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $arrayData = parent::toArray($fields, $expand, $recursive);
        $additionalData = [
            'deleted_at' => $this->getIntDeletedAt(),
            'updated_at' => $this->getIntUpdatedAt(),
            'created_at' => $this->getIntCreatedAt(),
        ];

        return array_merge($arrayData, $additionalData);
    }
}
