<?php

namespace taktwerk\yiiboilerplate\modules\workflow\models\mobile;

use taktwerk\yiiboilerplate\modules\workflow\models\Workflow as BaseWorkflow;
use taktwerk\yiiboilerplate\traits\MobileDateTrait;

/**
 * This is the model class for table "workflow".
 */
class Workflow extends BaseWorkflow
{
    use MobileDateTrait;

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $arrayData = parent::toArray($fields, $expand, $recursive);
        $additionalData = [
            'deleted_at' => $this->getIntDeletedAt(),
            'updated_at' => $this->getIntUpdatedAt(),
            'created_at' => $this->getIntCreatedAt(),
        ];

        return array_merge($arrayData, $additionalData);
    }
}
