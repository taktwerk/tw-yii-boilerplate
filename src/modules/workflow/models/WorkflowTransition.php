<?php

namespace taktwerk\yiiboilerplate\modules\workflow\models;

use taktwerk\yiiboilerplate\modules\workflow\components\TranslationComponent;
use taktwerk\yiiboilerplate\modules\sync\traits\UserAppDataVersionTrait;
use Yii;
use \taktwerk\yiiboilerplate\modules\workflow\models\base\WorkflowTransition as BaseWorkflowTransition;

/**
 * This is the model class for table "workflow_transition".
 */
class WorkflowTransition extends BaseWorkflowTransition
{
    use UserAppDataVersionTrait;

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'default_order' => Yii::t('twbp', 'If value is 0, button is only shown for Authority role. Can be used for transition programmatically.'),
        ];
    }

    /**
     * Submit Action name for forms
     * @return string
     */
    public function submitActionName()
    {
        return 'transition-' . $this->id;
    }

    public function favicon()
    {
        if (empty($this->icon)) {
            return null;
        }

        return '<i class="fa fa-' . ltrim($this->icon, 'fa-') . '"></i> ';
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        // Re-generate command
        $translator = new TranslationComponent();
        $translator->generate();
    }

}
