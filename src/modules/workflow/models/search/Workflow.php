<?php

namespace taktwerk\yiiboilerplate\modules\workflow\models\search;

use taktwerk\yiiboilerplate\modules\workflow\models\search\base\Workflow as WorkflowSearchModel;

/**
* Workflow represents the model behind the search form about `\taktwerk\yiiboilerplate\modules\workflow\models\Workflow`.
*/
class Workflow extends WorkflowSearchModel{

}