<?php

namespace taktwerk\yiiboilerplate\modules\workflow\models\search;

use taktwerk\yiiboilerplate\modules\workflow\models\search\base\WorkflowTransition as WorkflowTransitionSearchModel;

/**
* WorkflowTransition represents the model behind the search form about `\taktwerk\yiiboilerplate\modules\workflow\models\WorkflowTransition`.
*/
class WorkflowTransition extends WorkflowTransitionSearchModel{

}