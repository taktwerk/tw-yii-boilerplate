<?php
//Generation Date: 29-Dec-2020 05:31:33am
namespace taktwerk\yiiboilerplate\modules\workflow\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep as WorkflowStepModel;

/**
 * WorkflowStep represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep`.
 */
class WorkflowStep extends WorkflowStepModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'workflow_id',
                    'name',
                    'type',
                    'role',
                    'user_id',
                    'group_id',
                    'is_first',
                    'order_number',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$showDeleted = (property_exists(\Yii::$app->controller, 'model')
            &&
            (is_subclass_of(WorkflowStepModel::class, \Yii::$app->controller->model) 
            	||
            	is_subclass_of(\Yii::$app->controller->model, WorkflowStepModel::class))
            &&
              (Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model)
                ||
                Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model)))
            );
        $query = WorkflowStepModel::find((Yii::$app->get('userUi')->get('show_deleted', WorkflowStepModel::class) === '1'|| $showDeleted)?false:true);

        $this->parseSearchParams(WorkflowStepModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>['order_number'=>SORT_ASC],
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(WorkflowStepModel::class),
                'params' => [
                    'page' => $this->parsePageParams(WorkflowStepModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('workflow_id', $query);
        $this->applyHashOperator('type', $query);
        $this->applyHashOperator('user_id', $query);
        $this->applyHashOperator('group_id', $query);
        $this->applyHashOperator('is_first', $query);
        $this->applyHashOperator('order_number', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('name', $query);
        $this->applyLikeOperator('role', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(WorkflowStepModel::class);
    }
}
