<?php

namespace taktwerk\yiiboilerplate\modules\workflow\models\search;

use taktwerk\yiiboilerplate\modules\workflow\models\search\base\WorkflowStep as WorkflowStepSearchModel;

/**
* WorkflowStep represents the model behind the search form about `\taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep`.
*/
class WorkflowStep extends WorkflowStepSearchModel{

}