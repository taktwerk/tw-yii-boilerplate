<?php
//Generation Date: 24-Dec-2020 10:15:45am
namespace taktwerk\yiiboilerplate\modules\workflow\models\search;

use taktwerk\yiiboilerplate\modules\workflow\models\search\base\WorkflowStepStatus as WorkflowStepStatusSearchModel;

/**
* WorkflowStepStatus represents the model behind the search form about `taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStepStatus`.
*/
class WorkflowStepStatus extends WorkflowStepStatusSearchModel{

}