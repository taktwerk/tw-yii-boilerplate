<?php

namespace taktwerk\yiiboilerplate\modules\workflow\models;

use taktwerk\yiiboilerplate\modules\customer\models\ClientUser;
use taktwerk\yiiboilerplate\modules\workflow\components\TranslationComponent;
use Yii;
use \taktwerk\yiiboilerplate\modules\workflow\models\base\WorkflowStep as BaseWorkflowStep;
use taktwerk\yiiboilerplate\modules\user\models\UserGroup;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "workflow_step".
 */
class WorkflowStep extends BaseWorkflowStep
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }
    public function getActiveWorkflowStepStatus(){
        $statuses = $this->workflowStepStatuses;
        $user = \Yii::$app->user->identity;
        $groupIds = ArrayHelper::map(UserGroup::find()->select('group_id')
            ->where(['user_id' => $user->id])
            ->asArray()->all(), 'group_id', 'group_id');
        $roles = Yii::$app->getUser()->getRoles(true);
        foreach($statuses as $status){
            if($status->user_id==\Yii::$app->user->identity->id){
                return $status;
            }
            if(in_array($status->group_id,$groupIds)){
                return $status;
            }
            if(in_array($status->role,$roles)){
                return $status;
            }
        }
        return $this;
    }
    public function getName(){
        $status = $this->getActiveWorkflowStepStatus();
        return $status->name;
    }
    public function getType(){
        if((php_sapi_name() == "cli")) {
            return 'input';
        }
        $status = $this->getActiveWorkflowStepStatus();
        return $status->type;
    }
    public function getGroupId(){
        $status = $this->getActiveWorkflowStepStatus();
        return $status->type;
    }
    public function getUserId(){
        $status = $this->getActiveWorkflowStepStatus();
        return $status->user_id;
    }
    public function getRole(){
        $status = $this->getActiveWorkflowStepStatus();
        return $status->role;
    }
    public function assignee()
    {
        if ($this->getUserId()) {
            $user = User::findOne($this->getUserId());
            if ($user) {
                return $user->toString();
            }
        } elseif ($this->getRole()) {
            return $this->getRole();
        }
        
        return null;
    }
    
    /**
     * SoftDeleteBehavior to find only not deleted entries
     * @param $removedDeleted bool To remove soft-deleted entries from results, false to include them in re
     * @inheritdoc
     */
    public static function find($removedDeleted = true)
    {
        $model = parent::find($removedDeleted);

        // If not authority, only show tickets of our customers
        if (php_sapi_name() != "cli" && !Yii::$app->getUser()->canManageClients() && !Yii::$app->user->isGuest) {
            $clients = [];
            $guideIds = [];
            $clientUsers = ClientUser::findAll(['user_id' => Yii::$app->getUser()->id]);
            $systemUsers = \taktwerk\yiiboilerplate\modules\customer\models\Client::findWithSystem()->where(["is_system_entry" => 1])->all();

            foreach ($systemUsers as $system_client) {
                /**@var $system_client \taktwerk\yiiboilerplate\modules\customer\models\Client*/
                $clients[] = $system_client->id;

                foreach (Workflow::find()->where(['client_id' => $system_client->id])->all() as $guide) {
                    $guideIds[] = $guide->id;
                }
            }

            foreach ($clientUsers as $client) {
                $clients[] = $client->client_id;

                foreach (Workflow::find()->where(['client_id' => $client->client_id])->all() as $guide) {
                    $guideIds[] = $guide->id;
                }
            }

            $model->andWhere(['in', 'workflow_id', $guideIds]);
        }
        return $model;
    }

    /**
     * @return bool
     */
    public function isFormEnabled()
    {
        return $this->getType() == self::TYPE_INPUT;
    }

    /**
     * @return bool
     */
    public function isFinal()
    {
        return $this->getType() == self::TYPE_FINAL;
    }

    /**
     * The name of the Workflow Step can be translated.
     * @return String
     */
    public function toString()
    {
        return Yii::t('twbp', $this->getName());
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        // Re-generate command
        $translator = new TranslationComponent();
        $translator->generate();
    }

}
