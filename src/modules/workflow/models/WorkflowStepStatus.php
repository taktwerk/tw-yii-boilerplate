<?php
//Generation Date: 24-Dec-2020 10:15:37am
namespace taktwerk\yiiboilerplate\modules\workflow\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\workflow\models\base\WorkflowStepStatus as BaseWorkflowStepStatus;

/**
 * This is the model class for table "workflow_step_status".
 */
class WorkflowStepStatus extends BaseWorkflowStepStatus
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

}
