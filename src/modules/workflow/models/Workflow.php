<?php

namespace taktwerk\yiiboilerplate\modules\workflow\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\workflow\models\base\Workflow as BaseWorkflow;

/**
 * This is the model class for table "workflow".
 */
class Workflow extends BaseWorkflow
{
    public function toString()
    {
        return $this->name; // this attribute can be modified
    }
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
    public function rules()
    {
        $rules = parent::rules();
        foreach($rules as $k => $r){
            if($r[1]=='unique'){
                if($r[0][0]=='code' && $r[0][1]=='client_id'){
                    $rules[$k][0] = ['code'];
                    $rules[$k]['message'] = \Yii::t('twbp','This code already exists');
                }
            }
        }
        return $rules;
    }

    /**
     * Get the starting step.
     * @return mixed
     */
    public function startingStep()
    {
        $starting = $this->getWorkflowSteps()->where(['is_first' => true])->one();
        if (empty($starting)) {
            $starting = $this->getWorkflowSteps()->one();
        }
        return $starting;
    }

}
