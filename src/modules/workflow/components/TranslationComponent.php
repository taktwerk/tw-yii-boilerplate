<?php

namespace taktwerk\yiiboilerplate\modules\workflow\components;

use taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep;
use taktwerk\yiiboilerplate\modules\workflow\models\WorkflowTransition;
use Yii;

/**
 * Class TranslationComponent
 * @package \taktwerk\yiiboilerplate\modules\workflow\components
 */
class TranslationComponent
{
    /**
     * @return int
     */
    public function generate()
    {
        $translations = [];

        foreach (WorkflowStep::find()->all() as $workflow) {
            $translations[] = $workflow->name;
        }

        foreach (WorkflowTransition::find()->all() as $transition) {
            $translations[] = $transition->action_key;
        }

        if (!empty($translations)) {
            $contents = "<?php\nYii::t('twbp', '" . implode("');\nYii::t('twbp', '", $translations) . "');\n";
            $filename = Yii::getAlias('@taktwerk-boilerplate/modules/protocol/translations') . '/workflow-translations.php';
            file_put_contents($filename, $contents);
        }

        return count($translations);
    }
}
