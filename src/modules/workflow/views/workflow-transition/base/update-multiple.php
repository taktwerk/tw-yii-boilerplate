<?php
//Generation Date: 11-Sep-2020 02:58:36pm
use yii\helpers\Html;
use Yii;
/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\workflow\models\WorkflowTransition $model
 * @var array $pk
 * @var array $show
 */

$this->title = 'Workflow Transition ' . $model->id . ', ' . Yii::t('twbp', 'Edit Multiple');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('twbp','Workflow Transitions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('twbp', 'Edit Multiple');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body workflow-transition-update">

        <h1>
            <?= \Yii::t('twbp','Workflow Transition'); ?>
        </h1>

        <div class="crud-navigation">
        </div>

        <?php echo $this->render('_form', [
            'model' => $model,
            'pk' => $pk,
            'show' => $show,
            'multiple' => true,
            'useModal' => $useModal,
            'action' => $action,
        ]); ?>

    </div>
</div>