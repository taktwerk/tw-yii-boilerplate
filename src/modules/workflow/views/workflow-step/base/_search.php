<?php
//Generation Date: 29-Dec-2020 05:31:34am
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\workflow\models\search\WorkflowStep $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="workflow-step-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'workflow_id') ?>

        <?= $form->field($model, 'name') ?>

        <?= $form->field($model, 'type') ?>

        <?= $form->field($model, 'role') ?>

        <?php // echo $form->field($model, 'user_id') ?>

        <?php // echo $form->field($model, 'group_id') ?>

        <?php // echo $form->field($model, 'is_first') ?>

        <?php // echo $form->field($model, 'order_number') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
