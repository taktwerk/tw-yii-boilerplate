<?php
//Generation Date: 24-Feb-2021 11:54:11am
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep $model
 * @var boolean $useModal
 */

$this->title = Yii::t('twbp', 'Workflow Step') . ' #' . (is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey) . ', ' . Yii::t('twbp', 'View') . ' - ' . $model->toString();
if(!$fromRelation) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('twbp', 'Workflow Steps'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => (string)is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('twbp', 'View');
}
$basePermission = Yii::$app->controller->module->id . '_' . Yii::$app->controller->id;

Yii::$app->controller->renderCustomBlocks(Yii::$app->controller::POS_MAIN);

?>
<div class="box box-default">
	<div class="giiant-crud box-body"
		id="workflow-step-view">

		<!-- flash message -->
        <?php if (Yii::$app->session->getFlash('deleteError') !== null) : ?>
            <span class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= Yii::$app->session->getFlash('deleteError') ?>
            </span>
        <?php endif; ?>
        <?php if (!$useModal) : ?>
            <div class="clearfix crud-navigation">
			<!-- menu buttons -->
			<div class='pull-left'>
                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_update'
                    ) && $model->editable() ?
                        Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span> ' . Yii::t('twbp', 'Edit'),
                            [
                                'update',
                                'id' => $model->id,                                 'fromRelation' => $fromRelation,
                                'show_deleted'=>Yii::$app->request->get('show_deleted')
                            ],
                            ['class' => 'btn btn-info']
                        )
                    :
                        null
                    ?>
                     <?php if(method_exists($model,'getUploadFields')  && Yii::$app->getUser()->can(
                        $basePermission . '_recompress'
                    )){
                    	$fields = $model->getUploadFields();
                    	$isCompressible = false;
                    	$compressibleFileTypes = $model::$compressibleFileTypes;
                    	foreach($fields as $field){
                        	if(in_array($model->getFileType($field),$compressibleFileTypes)){
                            	$isCompressible = true;
                            	break;
                        	}
                    	}
                    if($isCompressible){	
                       echo Html::a(
                            '<span class="glyphicon glyphicon-compressed"></span> ' . Yii::t('twbp', 'Recompress'),
                            ['recompress', 'id' => $model->id, 'fromRelation' => $fromRelation],
                            ['class' => 'btn btn-warning','title'=>Yii::t('twbp', 'Adds a compression job for this item\'s files')]
                        );}
                   
                    }?>
                    
                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_create'
                    ) ?
                        Html::a(
                            '<span class="glyphicon glyphicon-copy"></span> ' . Yii::t('twbp', 'Copy'),
                            ['create', 'id' => $model->id, 'fromRelation' => $fromRelation],
                            ['class' => 'btn btn-success']
                        )
                    :
                        null
                    ?>
                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_create'
                    ) ?
                        Html::a(
                            '<span class="glyphicon glyphicon-plus"></span>
				' . Yii::t('twbp', 'New'), ['create', 'fromRelation' =>
				$fromRelation], ['class' => 'btn btn-success create-new'] ) : null
				?>
			</div>
			<div class="pull-right">
                    <?= \taktwerk\yiiboilerplate\widget\CustomActionDropdownWidget::widget(['model'=>$model]) ?>
                    <?php if(Yii::$app->controller->crudMainButtons['listall']): ?>
                    <?= Html::a(
                        '<span class="glyphicon glyphicon-list"></span> '. Yii::t('twbp', 'List {model}',
                            ['model' => \Yii::t('twbp','Workflow Steps')]                        ),
                        ['index'],
                        ['class' => 'btn btn-default']
                    ) ?>
                    <?php endif; ?>
                </div>
		</div>
        <?php endif; ?>
        <?php $this->beginBlock('taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep'); ?>

        <?php $viewColumns = [
                   [
            'attribute'=> 'id'
           ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::attributeFormat
                [
                    'format' => 'html',
                    'attribute' => 'workflow_id',
                    'value' => function ($model) {
                        $foreign = $model->getWorkflow()->one();
                        if ($foreign) {
                            if (Yii::$app->getUser()->can('app_workflow_view') && $foreign->readable()) {
                                return Html::a($foreign->toString, [
                                    'workflow/view',
                                    'id' => $model->getWorkflow()->one()->id,
                                ], ['data-pjax' => 0]);
                            }
                            return $foreign->toString;
                        }
                        return '<span class="label label-warning">?</span>';
                    },
                ],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],

                [
                    'attribute' => 'type',
                    'value' => function ($model) {
                        return \Yii::t('twbp', $model->type);
                    },
                ],
[
        'attribute' => 'role',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->role != strip_tags($model->role)){
                return \yii\helpers\StringHelper::truncate($model->role,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->role,500));
            }
        },
    ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::attributeFormat
                [
                    'format' => 'html',
                    'attribute' => 'user_id',
                    'value' => function ($model) {
                        $foreign = $model->getUser()->one();
                        if ($foreign) {
                            if (Yii::$app->getUser()->can('app_user_view') && $foreign->readable()) {
                                return Html::a($foreign->toString, [
                                    'user/view',
                                    'id' => $model->getUser()->one()->id,
                                ], ['data-pjax' => 0]);
                            }
                            return $foreign->toString;
                        }
                        return '<span class="label label-warning">?</span>';
                    },
                ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::attributeFormat
                [
                    'format' => 'html',
                    'attribute' => 'group_id',
                    'value' => function ($model) {
                        $foreign = $model->getGroup()->one();
                        if ($foreign) {
                            if (Yii::$app->getUser()->can('app_group_view') && $foreign->readable()) {
                                return Html::a($foreign->toString, [
                                    'group/view',
                                    'id' => $model->getGroup()->one()->id,
                                ], ['data-pjax' => 0]);
                            }
                            return $foreign->toString;
                        }
                        return '<span class="label label-warning">?</span>';
                    },
                ],
                'is_first:boolean',
[
        'attribute' => 'order_number',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->order_number != strip_tags($model->order_number)){
                return \yii\helpers\StringHelper::truncate($model->order_number,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->order_number,500));
            }
        },
    ],
        ];

        // view columns overwrite
        $viewColumns = Yii::$app->controller->crudColumnsOverwrite($viewColumns, 'view');

        echo DetailView::widget([
        'model' => $model,
        'attributes' => $viewColumns
        ]); ?>

        
        <hr />

        <?=  ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::deleteButton($model, "twbp", ['id' => $model->id], 'view')?>

        <?php $this->endBlock(); ?>


        
        <?php $this->beginBlock('Protocols'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsProtocols = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('workflow_protocol_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('workflow_protocol_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('workflow_protocol_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/protocol/protocol' . '/' . $action;
                        $params['Protocol'] = [
                            'workflow_step_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'protocol'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'client_id',
                    'value' => function ($model) {
                        if ($rel = $model->getClient()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'client/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'protocol_template_id',
                    'value' => function ($model) {
                        if ($rel = $model->getProtocolTemplate()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'protocol-template/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],
[
        'attribute' => 'protocol_form_table',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->protocol_form_table != strip_tags($model->protocol_form_table)){
                return \yii\helpers\StringHelper::truncate($model->protocol_form_table,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->protocol_form_table,500));
            }
        },
    ],
[
        'attribute' => 'protocol_form_number',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->protocol_form_number != strip_tags($model->protocol_form_number)){
                return \yii\helpers\StringHelper::truncate($model->protocol_form_number,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->protocol_form_number,500));
            }
        },
    ],
[
        'attribute' => 'reference_model',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->reference_model != strip_tags($model->reference_model)){
                return \yii\helpers\StringHelper::truncate($model->reference_model,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->reference_model,500));
            }
        },
    ],
[
        'attribute' => 'reference_id',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->reference_id != strip_tags($model->reference_id)){
                return \yii\helpers\StringHelper::truncate($model->reference_id,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->reference_id,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\protocol\models\Protocol::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsProtocols = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsProtocols, 'tab'):$columnsProtocols;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('workflow_protocol_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Protocol'),
                [
                    '/protocol/protocol/create',
                    'Protocol' => [
                        'workflow_step_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getProtocols(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-protocols',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsProtocols
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Protocol Comments'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsProtocolComments = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('workflow_protocol-comment_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('workflow_protocol-comment_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('workflow_protocol-comment_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/protocol/protocol-comment' . '/' . $action;
                        $params['ProtocolComment'] = [
                            'new_workflow_step_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'protocol-comment'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'protocol_id',
                    'value' => function ($model) {
                        if ($rel = $model->getProtocol()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'protocol/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'comment',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->comment != strip_tags($model->comment)){
                return \yii\helpers\StringHelper::truncate($model->comment,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->comment,500));
            }
        },
    ],
[
        'attribute' => 'event',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->event != strip_tags($model->event)){
                return \yii\helpers\StringHelper::truncate($model->event,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->event,500));
            }
        },
    ],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'old_workflow_step_id',
                    'value' => function ($model) {
                        if ($rel = $model->getOldWorkflowStep()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'workflow-step/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\protocol\models\ProtocolComment::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsProtocolComments = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsProtocolComments, 'tab'):$columnsProtocolComments;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('workflow_protocol-comment_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Protocol Comment'),
                [
                    '/protocol/protocol-comment/create',
                    'ProtocolComment' => [
                        'new_workflow_step_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getProtocolComments(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-protocolcomments',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsProtocolComments
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Protocol Comment Old Workflow Steps'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsProtocolComments0 = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('workflow_protocol-comment_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('workflow_protocol-comment_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('workflow_protocol-comment_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/protocol/protocol-comment' . '/' . $action;
                        $params['ProtocolComment'] = [
                            'old_workflow_step_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'protocol-comment'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'protocol_id',
                    'value' => function ($model) {
                        if ($rel = $model->getProtocol()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'protocol/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'comment',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->comment != strip_tags($model->comment)){
                return \yii\helpers\StringHelper::truncate($model->comment,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->comment,500));
            }
        },
    ],
[
        'attribute' => 'event',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->event != strip_tags($model->event)){
                return \yii\helpers\StringHelper::truncate($model->event,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->event,500));
            }
        },
    ],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'new_workflow_step_id',
                    'value' => function ($model) {
                        if ($rel = $model->getNewWorkflowStep()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'workflow-step/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\protocol\models\ProtocolComment::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsProtocolComments0 = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsProtocolComments0, 'tab'):$columnsProtocolComments0;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('workflow_protocol-comment_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Protocol Comments0'),
                [
                    '/protocol/protocol-comment/create',
                    'ProtocolComment' => [
                        'old_workflow_step_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getProtocolComments0(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-protocolcomments0',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsProtocolComments0
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Protocol Field Comments'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsProtocolFieldComments = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('workflow_protocol-field-comment_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('workflow_protocol-field-comment_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('workflow_protocol-field-comment_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/protocol/protocol-field-comment' . '/' . $action;
                        $params['ProtocolFieldComment'] = [
                            'workflow_step_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'protocol-field-comment'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],
[
        'attribute' => 'comment',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->comment != strip_tags($model->comment)){
                return \yii\helpers\StringHelper::truncate($model->comment,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->comment,500));
            }
        },
    ],
[
        'attribute' => 'validation',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->validation != strip_tags($model->validation)){
                return \yii\helpers\StringHelper::truncate($model->validation,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->validation,500));
            }
        },
    ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'protocol_id',
                    'value' => function ($model) {
                        if ($rel = $model->getProtocol()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'protocol/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\protocol\models\ProtocolFieldComment::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsProtocolFieldComments = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsProtocolFieldComments, 'tab'):$columnsProtocolFieldComments;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('workflow_protocol-field-comment_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Protocol Field Comment'),
                [
                    '/protocol/protocol-field-comment/create',
                    'ProtocolFieldComment' => [
                        'workflow_step_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getProtocolFieldComments(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-protocolfieldcomments',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsProtocolFieldComments
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Workflow Step Statuses'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsWorkflowStepStatuses = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('workflow_workflow-step-status_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('workflow_workflow-step-status_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('workflow_workflow-step-status_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/workflow/workflow-step-status' . '/' . $action;
                        $params['WorkflowStepStatus'] = [
                            'workflow_step_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'workflow-step-status'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],

                [
                    'attribute' => 'type',
                    'value' => function ($model) {
                        return \Yii::t('twbp', $model->type);
                    },
                ],
[
        'attribute' => 'role',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->role != strip_tags($model->role)){
                return \yii\helpers\StringHelper::truncate($model->role,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->role,500));
            }
        },
    ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'user_id',
                    'value' => function ($model) {
                        if ($rel = $model->getUser()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'user/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'group_id',
                    'value' => function ($model) {
                        if ($rel = $model->getGroup()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'group/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStepStatus::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsWorkflowStepStatuses = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsWorkflowStepStatuses, 'tab'):$columnsWorkflowStepStatuses;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('workflow_workflow-step-status_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Workflow Step Status'),
                [
                    '/workflow/workflow-step-status/create',
                    'WorkflowStepStatus' => [
                        'workflow_step_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getWorkflowStepStatuses(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-workflowstepstatuses',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsWorkflowStepStatuses
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Workflow Transitions'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsWorkflowTransitions = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('workflow_workflow-transition_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('workflow_workflow-transition_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('workflow_workflow-transition_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/workflow/workflow-transition' . '/' . $action;
                        $params['WorkflowTransition'] = [
                            'next_workflow_step_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'workflow-transition'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'workflow_step_id',
                    'value' => function ($model) {
                        if ($rel = $model->getWorkflowStep()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'workflow-step/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'action_key',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->action_key != strip_tags($model->action_key)){
                return \yii\helpers\StringHelper::truncate($model->action_key,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->action_key,500));
            }
        },
    ],
[
        'attribute' => 'skip_step_validation',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->skip_step_validation != strip_tags($model->skip_step_validation)){
                return \yii\helpers\StringHelper::truncate($model->skip_step_validation,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->skip_step_validation,500));
            }
        },
    ],
[
        'attribute' => 'icon',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->icon != strip_tags($model->icon)){
                return \yii\helpers\StringHelper::truncate($model->icon,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->icon,500));
            }
        },
    ],
[
        'attribute' => 'default_order',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->default_order != strip_tags($model->default_order)){
                return \yii\helpers\StringHelper::truncate($model->default_order,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->default_order,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\workflow\models\WorkflowTransition::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsWorkflowTransitions = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsWorkflowTransitions, 'tab'):$columnsWorkflowTransitions;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('workflow_workflow-transition_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Workflow Transition'),
                [
                    '/workflow/workflow-transition/create',
                    'WorkflowTransition' => [
                        'next_workflow_step_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getWorkflowTransitions(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-workflowtransitions',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsWorkflowTransitions
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Workflow Transition Workflow Steps'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsWorkflowTransitions0 = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('workflow_workflow-transition_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('workflow_workflow-transition_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('workflow_workflow-transition_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/workflow/workflow-transition' . '/' . $action;
                        $params['WorkflowTransition'] = [
                            'workflow_step_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'workflow-transition'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'next_workflow_step_id',
                    'value' => function ($model) {
                        if ($rel = $model->getNextWorkflowStep()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'workflow-step/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'action_key',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->action_key != strip_tags($model->action_key)){
                return \yii\helpers\StringHelper::truncate($model->action_key,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->action_key,500));
            }
        },
    ],
[
        'attribute' => 'skip_step_validation',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->skip_step_validation != strip_tags($model->skip_step_validation)){
                return \yii\helpers\StringHelper::truncate($model->skip_step_validation,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->skip_step_validation,500));
            }
        },
    ],
[
        'attribute' => 'icon',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->icon != strip_tags($model->icon)){
                return \yii\helpers\StringHelper::truncate($model->icon,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->icon,500));
            }
        },
    ],
[
        'attribute' => 'default_order',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->default_order != strip_tags($model->default_order)){
                return \yii\helpers\StringHelper::truncate($model->default_order,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->default_order,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\workflow\models\WorkflowTransition::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsWorkflowTransitions0 = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsWorkflowTransitions0, 'tab'):$columnsWorkflowTransitions0;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('workflow_workflow-transition_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Workflow Transitions0'),
                [
                    '/workflow/workflow-transition/create',
                    'WorkflowTransition' => [
                        'workflow_step_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getWorkflowTransitions0(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-workflowtransitions0',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsWorkflowTransitions0
        ])?>
        </div>
                <?php $this->endBlock() ?>

<?php /* MAPPED MODELS RELATIONS - START*/?>
<?php 
$extraTabItems = [];
foreach(\Yii::$app->modelNewRelationMapper->newRelationsMapping as $modelClass=>$relations){
    if(is_a($model,$modelClass)){
        foreach($relations as $relName => $relation){
            $relationKey = array_key_first($relation[1]);
            $foreignKey= $relation[1][$relationKey];
            $relClass = $relation[0];
            $baseName = \yii\helpers\StringHelper::basename($relClass);
            $cntrler = Inflector::camel2id($baseName, '-', true);
            $pluralName= Inflector::camel2words(Inflector::pluralize($baseName));
            $singularName = Inflector::camel2words(Inflector::singularize($baseName));
            $basePerm =  \Yii::$app->controller->module->id.'_'.$cntrler;
            $this->beginBlock($pluralName);
?>
<?php 
$relatedModelObject = Yii::createObject($relClass::className());
$relationController = $relatedModelObject->getControllerInstance();
//TODO CrudHelper::getColumnFormat('\app\modules\guide\models\Guide', 'id');
?>
                    <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
       	<div class="clearfix"></div>
        <div>
        <?php $relMapCols = [[
                    'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can($basePerm.'_view') ? '{view} ' : '') . (Yii::$app->getUser()->can($basePerm.'_update') ? '{update} ' : '') . (Yii::$app->getUser()->can($basePerm.'_delete') ? '{delete} ' : ''),
            'urlCreator' => function ($action, $relation_model, $key, $index) use($model,$relationKey,$foreignKey,$baseName,$relatedBaseUrl) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = $relatedBaseUrl . $action;
                        
                        $params[$baseName] = [
                            $relationKey => $model->{$foreignKey}
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) { 
                             return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => $cntrler
                ],
                
        ];

        $relMapCols =array_merge($relMapCols,array_slice(array_keys($relatedModelObject->attributes),0,5));
        
        $relMapCols = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($relMapCols, 'tab'):$relMapCols;
        
        echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can($basePerm.'_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp',$singularName),
                [
                    '/'.$relationController->module->id.'/'.$cntrler.'/create',
                    $baseName => [
                        $relationKey => $model->{$foreignKey}
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[
                'class'=>'grid-outer-div'
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->{'get'.$relName}(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-'.$relName,
                    ]
                ]
                ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            'columns' =>$relMapCols
        ])?>
        </div>
                <?php $this->endBlock();
                $extraTabItems[] = [
                    'content' => $this->blocks[$pluralName],
                    'label' => '<small>' .
                    \Yii::t('twbp', $pluralName) .
                    '&nbsp;<span class="badge badge-default">' .
                    $model->{'get'.$relName}()->count() .
                    '</span></small>',
                    'active' => false,
                    'visible' =>
                    Yii::$app->user->can('x_'.$basePerm.'_see') && Yii::$app->controller->crudRelations(ucfirst($relName)),
                    ];
?>

<?php 
        }
    }
}
?>

                
<?php /* MAPPED MODELS RELATIONS - END*/ ?>
        <?= Tabs::widget(
            [
                'id' => 'relation-tabs',
                'encodeLabels' => false,
                'items' =>  array_filter(array_merge([
                    [
                        'label' => '<b class=""># ' . $model->id . '</b>',
                        'content' => $this->blocks['taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep'],
                        'active' => true,
                    ],
        (\Yii::$app->hasModule('protocol'))?[
                        'content' => $this->blocks['Protocols'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Protocols') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getProtocols()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_workflow_protocol_see') && Yii::$app->controller->crudRelations('Protocols'),
                    ]:null,
        (\Yii::$app->hasModule('protocol'))?[
                        'content' => $this->blocks['Protocol Comments'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Protocol Comments') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getProtocolComments()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_workflow_protocol-comment_see') && Yii::$app->controller->crudRelations('ProtocolComments'),
                    ]:null,
        (\Yii::$app->hasModule('protocol'))?[
                        'content' => $this->blocks['Protocol Comment Old Workflow Steps'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Protocol Comment Old Workflow Steps') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getProtocolComments0()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_workflow_protocol-comment_see') && Yii::$app->controller->crudRelations('ProtocolComments0'),
                    ]:null,
        (\Yii::$app->hasModule('protocol'))?[
                        'content' => $this->blocks['Protocol Field Comments'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Protocol Field Comments') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getProtocolFieldComments()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_workflow_protocol-field-comment_see') && Yii::$app->controller->crudRelations('ProtocolFieldComments'),
                    ]:null,
        (\Yii::$app->hasModule('workflow'))?[
                        'content' => $this->blocks['Workflow Step Statuses'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Workflow Step Statuses') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getWorkflowStepStatuses()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_workflow_workflow-step-status_see') && Yii::$app->controller->crudRelations('WorkflowStepStatuses'),
                    ]:null,
        (\Yii::$app->hasModule('workflow'))?[
                        'content' => $this->blocks['Workflow Transitions'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Workflow Transitions') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getWorkflowTransitions()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_workflow_workflow-transition_see') && Yii::$app->controller->crudRelations('WorkflowTransitions'),
                    ]:null,
        (\Yii::$app->hasModule('workflow'))?[
                        'content' => $this->blocks['Workflow Transition Workflow Steps'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Workflow Transition Workflow Steps') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getWorkflowTransitions0()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_workflow_workflow-transition_see') && Yii::$app->controller->crudRelations('WorkflowTransitions0'),
                    ]:null,
                    [
                        'content' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\HistoryTab::class)::widget(['model' => $model]),
                        'label' => '<small>' .
                            \Yii::t('twbp', 'History') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getHistory()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('Administrator'),
                    ],
                ]
                ,$extraTabItems))
            ]
        );
        ?>
        <?= ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\RecordHistory::class)::widget(['model' => $model]) ?>
    </div>
</div>

<?php ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
$js = <<<JS
$(document).ready(function() {
    var hash=document.location.hash;
	var prefix="tab_" ;
    if (hash) {
        $('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
    } 
    
    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash=e.target.hash.replace(	"#", "#" + prefix);
    });
});
JS;

$this->registerJs($js);

Yii::$app->controller->renderCustomBlocks(Yii::$app->controller::POS_END);
