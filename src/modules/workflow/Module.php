<?php

namespace taktwerk\yiiboilerplate\modules\workflow;

class Module extends \taktwerk\yiiboilerplate\components\TwModule
{
    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\workflow\controllers';

    public function init()
    { 
        parent::init();

        // custom initialization code goes here
    }
}
