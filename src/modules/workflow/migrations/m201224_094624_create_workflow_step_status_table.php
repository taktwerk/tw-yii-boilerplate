<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%workflow_step_status}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%workflow_step}}`
 */
class m201224_094624_create_workflow_step_status_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%workflow_step_status}}', [
            'id' => $this->primaryKey(),
            'workflow_step_id' => $this->integer(11)->notNull(),
            'name' => $this->string(45),
            'type' => "ENUM('input', 'validation', 'final') DEFAULT 'input'",
            'role' => $this->string(45)->null(),
            'user_id' => $this->integer(11)->null(),
            'group_id'=> $this->integer(11)->null(),
        ]);

        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\workflow"}';
        $this->addCommentOnTable('{{%workflow_step_status}}', $comment);
        // creates index for column `workflow_step_id`
        $this->createIndex(
            'idx-workflow_step_status-workflow_step_id',
            '{{%workflow_step_status}}',
            'workflow_step_id'
        );
        $this->createIndex(
            'idx-workflow_step_status-user_id',
            '{{%workflow_step_status}}',
            'user_id'
            );
        $this->createIndex(
            'idx-workflow_step_status-group_id',
            '{{%workflow_step_status}}',
            'group_id'
            );
        // add foreign key for table `{{%workflow_step}}`
        $this->addForeignKey('fk-workflow_step_status-workflow_step_id','{{%workflow_step_status}}','workflow_step_id','{{%workflow_step}}','id','CASCADE');
        $this->addForeignKey('fk_workflow_step_status_user_id', '{{%workflow_step_status}}', 'user_id', '{{%user}}', 'id','CASCADE');
        $this->addForeignKey('fk_workflow_step_status_group_id', '{{%workflow_step_status}}', 'group_id', '{{%group}}', 'id','CASCADE');
        
        $this->createIndex('idx_workflow_step_status_deleted_at_workflow_step_id', '{{%workflow_step_status}}', ['deleted_at','workflow_step_id']);
        $this->createIndex('idx_workflow_step_status_deleted_at_user_id', '{{%workflow_step_status}}', ['deleted_at','user_id']);
        $this->createIndex('idx_workflow_step_status_deleted_at_group_id', '{{%workflow_step_status}}', ['deleted_at','group_id']);
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx_workflow_step_status_deleted_at_workflow_step_id', '{{%workflow_step_status}}');
        $this->dropIndex('idx_workflow_step_status_deleted_at_user_id', '{{%workflow_step_status}}');
        $this->dropIndex('idx_workflow_step_status_deleted_at_group_id', '{{%workflow_step_status}}');
                // drops foreign key for table `{{%workflow_step}}`
        $this->dropForeignKey('fk-workflow_step_status-workflow_step_id','{{%workflow_step_status}}');
        $this->dropForeignKey('fk_workflow_step_status_user_id','{{%workflow_step_status}}');
        $this->dropForeignKey('fk_workflow_step_status_group_id','{{%workflow_step_status}}');
            // drops index for column `workflow_step_id`
        $this->dropIndex('idx-workflow_step_status-workflow_step_id','{{%workflow_step_status}}');
        $this->dropIndex('idx-workflow_step_status-group_id','{{%workflow_step_status}}');
        $this->dropIndex('idx-workflow_step_status-user_id','{{%workflow_step_status}}');
        $this->dropTable('{{%workflow_step_status}}');
    }
}
