<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%workflow_transition}}`.
 */
class m210224_101239_add_skip_step_validation_column_to_workflow_transition_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%workflow_transition}}', 'skip_step_validation', $this->boolean()
            ->null()->defaultValue(0)
            ->after('action_key')
            ->comment('{"inputtype":"checkbox","options":{"1":"On","0":"Off"}}'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            $this->dropColumn('{{%workflow_transition}}', 'skip_step_validation');
    }
}
