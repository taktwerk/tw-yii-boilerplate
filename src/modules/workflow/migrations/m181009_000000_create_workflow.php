<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m181009_000000_create_workflow extends TwMigration
{
    public function up()
    {
        $this->createTable('{{%workflow}}', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer()->notNull(),
            'name' => $this->string(45)->notNull()
        ]);
        $this->addForeignKey('workflow_fk_client_id', '{{%workflow}}', 'client_id', '{{%client}}', 'id');

        $this->createTable('{{%workflow_step}}', [
            'id' => $this->primaryKey(),
            'workflow_id' => $this->integer()->notNull(),
            'name' => $this->string(45)->notNull(),
            'type' => "ENUM('input', 'validation', 'final') DEFAULT 'input'",
            'role' => $this->string(45)->null(),
            'user_id' => $this->integer()->null(),
            'is_first' => $this->boolean()->defaultValue(false),
        ]);
        $this->addForeignKey('workflow_step_fk_workflow_id', '{{%workflow_step}}', 'workflow_id', '{{%workflow}}', 'id');


        $this->createTable('{{%workflow_transition}}', [
            'id' => $this->primaryKey(),
            'workflow_step_id' => $this->integer()->notNull(),
            'next_workflow_step_id' => $this->integer()->notNull(),
            'action_key' => $this->string(45)->notNull()
        ]);
        $this->addForeignKey('workflow_transition_fk_workflow_step_id', '{{%workflow_transition}}', 'workflow_step_id', '{{%workflow_step}}', 'id');
        $this->addForeignKey('workflow_transition_fk_next_workflow_step_id', '{{%workflow_transition}}', 'next_workflow_step_id', '{{%workflow_step}}', 'id');
    }

    public function down()
    {
        $this->dropTable('{{%workflow_transition}}');
        $this->dropTable('{{%workflow_step}}');
        $this->dropTable('{{%workflow}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
