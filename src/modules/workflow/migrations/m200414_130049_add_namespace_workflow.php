<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200414_130049_add_namespace_workflow extends TwMigration
{
    public function up()
    {
        $comment= '{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\workflow"}';
        $this->addCommentOnTable('workflow', $comment);
        $this->addCommentOnTable('workflow_step', $comment);
        $this->addCommentOnTable('workflow_transition', $comment);
    }

    public function down()
    {
        echo "m200414_130049_add_namespace_workflow cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
