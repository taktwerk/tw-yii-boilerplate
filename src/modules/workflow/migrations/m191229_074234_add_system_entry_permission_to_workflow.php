<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191229_074234_add_system_entry_permission_to_workflow extends TwMigration
{
    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->createPermission('workflow_workflow_system-entry', 'Check Workflow System Entry', ['ProtocolAdmin']);
        $this->createPermission('workflow_workflow-step_system-entry', 'Check Workflow Step System Entry', ['ProtocolAdmin']);
        $this->createPermission('workflow_workflow-transition_system-entry', 'Check Workflow Transition System Entry', ['ProtocolAdmin']);
    }

    public function down()
    {
        $this->removePermission('workflow_workflow_system-entry', ['ProtocolAdmin']);
        $this->removePermission('workflow_workflow-step_system-entry', ['ProtocolAdmin']);
        $this->removePermission('workflow_workflow-transition_system-entry', ['ProtocolAdmin']);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
