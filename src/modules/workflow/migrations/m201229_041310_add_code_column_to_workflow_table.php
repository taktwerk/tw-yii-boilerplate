<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%workflow}}`.
 */
class m201229_041310_add_code_column_to_workflow_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%workflow}}', 'code', $this->string(255)->after('name'));
        $this->createIndex('uni-workflow-client-code', '{{%workflow}}', ['code','client_id'],1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            $this->dropColumn('{{%workflow}}', 'code');
    }
}
