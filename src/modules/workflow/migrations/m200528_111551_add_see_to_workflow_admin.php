<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200528_111551_add_see_to_workflow_admin extends TwMigration
{
    public function up()
    {
        $this->createCrudControllerPermissions('workflow_workflow');
        $this->addAdminControllerPermission('workflow_workflow','ProtocolAdmin');
        $this->addSeeControllerPermission('workflow_workflow','ProtocolAdmin');
        $this->createCrudControllerPermissions('workflow_workflow-step');
        $this->addAdminControllerPermission('workflow_workflow-step','ProtocolAdmin');
        $this->addSeeControllerPermission('workflow_workflow-step','ProtocolAdmin');
        $this->createCrudControllerPermissions('workflow_workflow-transition');
        $this->addAdminControllerPermission('workflow_workflow-transition','ProtocolAdmin');
        $this->addSeeControllerPermission('workflow_workflow-transition','ProtocolAdmin');

    }

    public function down()
    {
        echo "m200528_111551_add_see_to_workflow_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
