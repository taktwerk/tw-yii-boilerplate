<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%workflow_step}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%group}}`
 */
class m201224_093841_add_group_id_column_to_workflow_step_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%workflow_step}}', 'group_id', $this->integer(11)->after('user_id')->null());

        // creates index for column `group_id`
        $this->createIndex(
            'idx-workflow_step-group_id',
            '{{%workflow_step}}',
            'group_id'
        );

        // add foreign key for table `{{%group}}`
        $this->addForeignKey(
            'fk-workflow_step-group_id',
            '{{%workflow_step}}',
            'group_id',
            '{{%group}}',
            'id',
            'CASCADE'
        );
        $this->createIndex('idx_workflow_step_deleted_at_group_id', '{{%workflow_step}}', ['deleted_at','group_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            // drops foreign key for table `{{%group}}`
        $this->dropForeignKey(
            'fk-workflow_step-group_id',
            '{{%workflow_step}}'
        );

        // drops index for column `group_id`
        $this->dropIndex(
            'idx-workflow_step-group_id',
            '{{%workflow_step}}'
        );

        $this->dropColumn('{{%workflow_step}}', 'group_id');
    }
}
