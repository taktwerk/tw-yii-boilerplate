<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191224_133002_add_comment_to_workflow_tables extends TwMigration
{

    public function up()
    {
        $query = <<<EOF
            
            ALTER TABLE `workflow`
                COMMENT='{"base_namespace":"app\\\\modules\\\\workflow"}';
            
            ALTER TABLE `workflow_step`
                COMMENT='{"base_namespace":"app\\\\modules\\\\workflow"}';
            
            ALTER TABLE `workflow_transition`
                COMMENT='{"base_namespace":"app\\\\modules\\\\workflow"}';
           
EOF;
        $this->execute($query);
    }

    public function down()
    {
        echo "m191224_133002_add_comment_to_workflow_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
