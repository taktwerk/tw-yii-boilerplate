<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m181012_000000_add_workflow_transition_icon extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%workflow_transition}}', 'icon', $this->string(20)->null());
        $this->addColumn('{{%workflow_transition}}', 'default_order', $this->smallInteger()->notNull()->defaultValue(1));
    }

    public function down()
    {
        $this->dropColumn('{{%workflow_transition}}', 'icon');
        $this->dropColumn('{{%workflow_transition}}', 'default_order');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
