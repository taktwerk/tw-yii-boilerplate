<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200903_034936_workflow_step_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_workflow_step_deleted_at_user_id', '{{%workflow_step}}', ['deleted_at','user_id']);
        $this->createIndex('idx_workflow_step_deleted_at_workflow_id', '{{%workflow_step}}', ['deleted_at','workflow_id']);
    }

    public function down()
    {
        echo "m200903_034936_workflow_step_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
