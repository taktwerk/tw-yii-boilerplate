<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200903_035011_workflow_transition_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_workflow_transition_deleted_at_next_workflow_step_id', '{{%workflow_transition}}', ['deleted_at','next_workflow_step_id']);
        $this->createIndex('idx_workflow_transition_deleted_at_workflow_step_id', '{{%workflow_transition}}', ['deleted_at','workflow_step_id']);
    }

    public function down()
    {
        echo "m200903_035011_workflow_transition_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
