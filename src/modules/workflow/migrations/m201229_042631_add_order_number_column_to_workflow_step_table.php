<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%workflow_step}}`.
 */
class m201229_042631_add_order_number_column_to_workflow_step_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%workflow_step}}', 'order_number', $this
            ->integer(11)
            ->after('is_first')
            ->defaultValue(1)
            ->comment('{"sortRestrictAttributes":["workflow_id"]}'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            $this->dropColumn('{{%workflow_step}}', 'order_number');
    }
}
