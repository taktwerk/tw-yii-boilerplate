<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191224_132043_create_user_foreign_key_workflow_step extends TwMigration
{
    public function up()
    {
        $this->createIndex('user_idx', '{{%workflow_step}}', 'user_id', 0);
        $this->addForeignKey('workflow_step_fk_user_id', '{{%workflow_step}}', ['user_id'], '{{%user}}', 'id');
    }

    public function down()
    {
        $this->dropIndex('user_id_idx', '{{%user}}');
        $this->dropForeignKey('workflow_step_fk_user_id', '{{%workflow_step}}');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
