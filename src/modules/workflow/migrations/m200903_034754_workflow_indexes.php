<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200903_034754_workflow_indexes extends TwMigration
{
    public function up()
    {
      $this->createIndex('idx_workflow_deleted_at_client_id', '{{%workflow}}', ['deleted_at','client_id']);
    }

    public function down()
    {
        echo "m200903_034754_workflow_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
