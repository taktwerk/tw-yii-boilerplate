<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m181012_000002_workflow_acl extends TwMigration
{
    public function up()
    {
        $this->createCrudPermissions('workflow_workflow', 'Workflow', ['GuiderAdmin']);
        $this->createCrudPermissions('workflow_workflow-step', 'Workflow Step', ['GuiderAdmin']);
        $this->createCrudPermissions('workflow_workflow-transition', 'Workflow Transition', ['GuiderAdmin']);
    }

    public function down()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
