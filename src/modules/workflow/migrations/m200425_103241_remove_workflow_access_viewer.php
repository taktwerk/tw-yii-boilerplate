<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200425_103241_remove_workflow_access_viewer extends TwMigration
{
    public function up()
    {
        $this->removePermission('x_workflow_workflow_see','ProtocolViewer');
        $this->removePermission('x_workflow_workflow-step_see','ProtocolViewer');
        $this->removePermission('x_workflow_workflow-transition_see','ProtocolViewer');
    }

    public function down()
    {
        echo "m200425_103241_remove_workflow_access_viewer cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
