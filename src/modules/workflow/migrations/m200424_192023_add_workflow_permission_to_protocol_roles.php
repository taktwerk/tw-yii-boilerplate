<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200424_192023_add_workflow_permission_to_protocol_roles extends TwMigration
{
    public function up()
    {
        $this->createCrudControllerPermissions('workflow_workflow');
        $this->addAdminControllerPermission('workflow_workflow','ProtocolAdmin');
        $this->addSeeControllerPermission('workflow_workflow','ProtocolViewer');
        $this->createCrudControllerPermissions('workflow_workflow-step');
        $this->addAdminControllerPermission('workflow_workflow-step','ProtocolAdmin');
        $this->addSeeControllerPermission('workflow_workflow-step','ProtocolViewer');
        $this->createCrudControllerPermissions('workflow_workflow-transition');
        $this->addAdminControllerPermission('workflow_workflow-transition','ProtocolAdmin');
        $this->addSeeControllerPermission('workflow_workflow-transition','ProtocolViewer');
    }

    public function down()
    {
        echo "m200424_192023_add_workflow_permission_to_protocol_roles cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
