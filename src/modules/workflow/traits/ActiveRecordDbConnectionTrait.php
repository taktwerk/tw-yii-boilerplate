<?php

namespace taktwerk\yiiboilerplate\modules\workflow\traits;

trait ActiveRecordDbConnectionTrait
{
    public static function getDb()
    {
        return \Yii::$app->db;
    }
}
