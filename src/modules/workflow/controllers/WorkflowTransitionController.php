<?php

namespace taktwerk\yiiboilerplate\modules\workflow\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the class for controller "WorkflowTransitionController".
 */
class WorkflowTransitionController extends \taktwerk\yiiboilerplate\modules\workflow\controllers\base\WorkflowTransitionController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\workflow\models\WorkflowTransition';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\workflow\models\search\WorkflowTransition';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }

    /**
     * Init override
     */
    public function init()
    {
        /* $this->crudColumnsOverwrite = [
            'index'=>[
            'after#default_order'=>'icon',
            ]
        ];*/
    }
}
