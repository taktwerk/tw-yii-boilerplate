<?php

namespace taktwerk\yiiboilerplate\modules\workflow\controllers;

use taktwerk\yiiboilerplate\grid\GridView;
use Yii;
use yii\helpers\Html;
use taktwerk\yiiboilerplate\models\User;

/**
 * This is the class for controller "WorkflowStepController".
 */
class WorkflowStepController extends \taktwerk\yiiboilerplate\modules\workflow\controllers\base\WorkflowStepController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\workflow\models\search\WorkflowStep';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }

    /**
     * Init override
     */
    public function init()
    {
        $this->crudColumnsOverwrite = [
            'index'=>[
                'user_id'=>false,
                'is_first'=>false,
                'before#type'=>[
                    'attribute' => 'is_first',
                    'class' => '\kartik\grid\BooleanColumn',
                    'trueLabel' => \Yii::t('twbp','Yes'),
                    'falseLabel' => \Yii::t('twbp','No'),
                    'content' => function ($model) {
                        return $model->is_first == 1 ? Yii::t('twbp', 'Yes') : Yii::t('twbp', 'No');
                    },
                ],
                'after#name'=>
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'attribute' => 'user_id',
                        'format' => 'html',
                        'content' => function ($model) {
                            if ($model->user) {
                                if (\Yii::$app->getUser()->can('app_user_view') && $model->user->readable()) {
                                    return Html::a($model->user->toString, ['user/view', 'id' => $model->user_id], ['data-pjax' => 0]);
                                } else {
                                    return $model->user->toString;
                                }
                            }
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'data' => User::find()->count() > 50 ? null : \yii\helpers\ArrayHelper::map(User::find()->all(), 'id', 'toString'),
                            'initValueText' => User::find()->count() > 50 ? \yii\helpers\ArrayHelper::map(User::find()->andWhere(['id' => $searchModel->user_id])->all(), 'id', 'toString') : '',
                            'options' => [
                                'placeholder' => '',
                                'multiple' => true,
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                (User::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                                (User::find()->count() > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['/user/list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                        return {
                            q:params.term, m: \'User\'
                        };
                    }')
                                ],
                            ]
                        ],
                    ],
            ]
        ];
    }
}
