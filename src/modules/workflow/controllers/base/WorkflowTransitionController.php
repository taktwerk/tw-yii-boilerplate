<?php
//Generation Date: 11-Sep-2020 02:58:36pm
namespace taktwerk\yiiboilerplate\modules\workflow\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * WorkflowTransitionController implements the CRUD actions for WorkflowTransition model.
 */
class WorkflowTransitionController extends TwCrudController
{
}
