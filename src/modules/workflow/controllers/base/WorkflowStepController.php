<?php
//Generation Date: 29-Dec-2020 05:31:33am
namespace taktwerk\yiiboilerplate\modules\workflow\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * WorkflowStepController implements the CRUD actions for WorkflowStep model.
 */
class WorkflowStepController extends TwCrudController
{
	public function actions(){
        return array_merge(parent::actions(), [
            'sort-item' => [
                'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\actions\SortableAction::className()),
                'activeRecordClassName' => \taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStep::className(),
                'orderColumn' => 'order_number',
            ],
            // your other actions
        ]);
    }
    /**
    * Additional actions for controllers, uncomment to use them
    * @inheritdoc
    */
   public function behaviors()
   {
       return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
           'access' => [
               'class' => \yii\filters\AccessControl::class,
               'rules' => [
                   [
                       'allow' => true,
                       'actions' => [
                           'sort-item'
                       ],
                       'roles' => ['@']
                   ]
               ]
           ]
       ]);
   }
}
