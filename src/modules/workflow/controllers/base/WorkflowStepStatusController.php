<?php
//Generation Date: 24-Dec-2020 10:15:45am
namespace taktwerk\yiiboilerplate\modules\workflow\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * WorkflowStepStatusController implements the CRUD actions for WorkflowStepStatus model.
 */
class WorkflowStepStatusController extends TwCrudController
{
}
