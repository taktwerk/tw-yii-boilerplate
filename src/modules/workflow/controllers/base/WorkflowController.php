<?php
//Generation Date: 11-Sep-2020 02:57:49pm
namespace taktwerk\yiiboilerplate\modules\workflow\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * WorkflowController implements the CRUD actions for Workflow model.
 */
class WorkflowController extends TwCrudController
{
}
