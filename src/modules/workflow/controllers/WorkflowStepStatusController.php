<?php
//Generation Date: 24-Dec-2020 10:15:45am
namespace taktwerk\yiiboilerplate\modules\workflow\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "WorkflowStepStatusController".
 */
class WorkflowStepStatusController extends \taktwerk\yiiboilerplate\modules\workflow\controllers\base\WorkflowStepStatusController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\workflow\models\WorkflowStepStatus';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\workflow\models\search\WorkflowStepStatus';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
}
