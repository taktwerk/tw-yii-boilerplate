<?php

namespace taktwerk\yiiboilerplate\modules\workflow\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\modules\workflow\controllers\base\WorkflowController as BaseWorkflowController;

/**
 * This is the class for controller "WorkflowController".
 */
class WorkflowController extends BaseWorkflowController
{
    /**
     * Model class with namespace
     */
    public $model = '\taktwerk\yiiboilerplate\modules\workflow\models\Workflow';

    /**
     * Search Model class
     */
    public $searchModel = '\taktwerk\yiiboilerplate\modules\workflow\models\search\Workflow';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
}
