<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200918_042131_misc_model_namespace_update extends TwMigration
{
    public function up()
    {
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\system"}';
        $this->addCommentOnTable('{{%country}}', $comment);
        $this->addCommentOnTable('{{%google_spreadsheet}}', $comment);
    }

    public function down()
    {
        echo "m200918_042131_misc_model_namespace_update cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
