<?php
//Generation Date: 18-Sep-2020 01:01:44pm
namespace taktwerk\yiiboilerplate\modules\system\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "UserUiDataController".
 */
class UserUiDataController extends \taktwerk\yiiboilerplate\modules\system\controllers\base\UserUiDataController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\system\models\UserUiData';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\system\models\search\UserUiData';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
}
