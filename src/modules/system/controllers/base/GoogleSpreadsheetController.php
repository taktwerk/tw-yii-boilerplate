<?php
//Generation Date: 18-Sep-2020 07:36:46am
namespace taktwerk\yiiboilerplate\modules\system\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * GoogleSpreadsheetController implements the CRUD actions for GoogleSpreadsheet model.
 */
class GoogleSpreadsheetController extends TwCrudController
{
}
