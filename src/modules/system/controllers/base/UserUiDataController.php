<?php
//Generation Date: 18-Sep-2020 01:01:44pm
namespace taktwerk\yiiboilerplate\modules\system\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * UserUiDataController implements the CRUD actions for UserUiData model.
 */
class UserUiDataController extends TwCrudController
{
}
