<?php
//Generation Date: 18-Sep-2020 01:01:44pm
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\system\models\search\UserUiData $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="user-ui-data-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'user_id') ?>

        <?= $form->field($model, 'key') ?>

        <?= $form->field($model, 'param') ?>

        <?= $form->field($model, 'value') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
