<?php
//Generation Date: 18-Sep-2020 07:36:46am
use yii\helpers\Html;
use Yii;
/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\system\models\GoogleSpreadsheet $model
 * @var array $pk
 * @var array $show
 */

$this->title = 'Google Spreadsheet ' . $model->title . ', ' . Yii::t('twbp', 'Edit Multiple');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('twbp','Google Spreadsheets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('twbp', 'Edit Multiple');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body google-spreadsheet-update">

        <h1>
            <?= \Yii::t('twbp','Google Spreadsheet'); ?>
        </h1>

        <div class="crud-navigation">
        </div>

        <?php echo $this->render('_form', [
            'model' => $model,
            'pk' => $pk,
            'show' => $show,
            'multiple' => true,
            'useModal' => $useModal,
            'action' => $action,
        ]); ?>

    </div>
</div>