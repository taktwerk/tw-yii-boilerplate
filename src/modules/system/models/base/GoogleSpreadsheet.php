<?php
//Generation Date: 18-Sep-2020 06:58:03am
namespace taktwerk\yiiboilerplate\modules\system\models\base;

use Yii;
use \taktwerk\yiiboilerplate\models\TwActiveRecord;
use yii\db\Query;


/**
 * This is the base-model class for table "google_spreadsheet".
 * - - - - - - - - -
 * Generated by the modified Giiant CRUD Generator by taktwerk.com
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $url
 * @property integer $deleted_by
 * @property string $deleted_at
 * @property string $type
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property string $toString
 * @property string $entryDetails
 *
 * @property \taktwerk\yiiboilerplate\models\User $user
 */
class GoogleSpreadsheet extends TwActiveRecord implements \taktwerk\yiiboilerplate\modules\backend\models\RelationModelInterface{
    /**
     * ENUM field values
     */
    const TYPE_EXPORT = 'export';
    const TYPE_IMPORT = 'import';
    protected $enum_labels = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'google_spreadsheet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [
                ['user_id', 'deleted_by'],
                'integer'
            ],
            [
                ['title', 'url'],
                'required'
            ],
            [
                ['deleted_at'],
                'safe'
            ],
            [
                ['type'],
                'string'
            ],
            [
                ['title', 'url'],
                'string',
                'max' => 255
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => \taktwerk\yiiboilerplate\models\User::class,
                'targetAttribute' => ['user_id' => 'id']
            ],
            ['type', 'in', 'range' => [
                    self::TYPE_EXPORT,
                    self::TYPE_IMPORT,
                ]
            ],
            ['type', 'default', 'value' => null]
        ];
        
        return  array_merge($rules, $this->buildRequiredRulesFromFormDependentShowFields());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('twbp', 'ID'),
            'user_id' => Yii::t('twbp', 'User'),
            'title' => Yii::t('twbp', 'Title'),
            'url' => Yii::t('twbp', 'Url'),
            'created_by' => \Yii::t('twbp', 'Created By'),
            'created_at' => \Yii::t('twbp', 'Created At'),
            'updated_by' => \Yii::t('twbp', 'Updated By'),
            'updated_at' => \Yii::t('twbp', 'Updated At'),
            'deleted_by' => \Yii::t('twbp', 'Deleted By'),
            'deleted_at' => \Yii::t('twbp', 'Deleted At'),
            'type' => Yii::t('twbp', 'Type'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributePlaceholders()
    {
        return [
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
        ];
    }

    /**
     * Auto generated method, that returns a human-readable name as string
     * for this model. This string can be called in foreign dropdown-fields or
     * foreign index-views as a representative value for the current instance.
     *
     * @author: taktwerk
     * This method is auto generated with a modified Model-Generator by taktwerk.com
     * @return String
     */
    public function toString()
    {
        return $this->title; // this attribute can be modified
    }


    /**
     * Getter for toString() function
     * @return String
     */
    public function getToString()
    {
        return $this->toString();
    }

    /**
     * Description for the table/model to show on grid an form view
     * @return String
     */
    public static function tableHint()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return [
            'user',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(
            \taktwerk\yiiboilerplate\models\User::class,
            ['id' => 'user_id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function userList($q = null)
    {
        return \taktwerk\yiiboilerplate\models\User::filter($q);
    }
    

    /**
     * Return details for dropdown field
     * @return string
     */
    public function getEntryDetails()
    {
        return $this->toString;
    }

    /**
     * @param $string
     */
    public static function fetchCustomImportLookup($string)
    {
            return self::find()->andWhere([self::tableName() . '.title' => $string]);
    }

    /**
     * User for filtering results for Select2 element
     * @param null $q
     * @return array
     */
    public static function filter($q = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, title AS text')
            ->from(self::tableName())
            ->andWhere([self::tableName() . '.deleted_at' => null])
            ->andWhere(['like', 'title', $q])
            ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }

        /**
     * Get related CRUD controller's class
     * @return \taktwerk\yiiboilerplate\modules\system\controllers\GoogleSpreadsheetController|\yii\web\Controller
     */
    public function getControllerInstance()
    {
        if(class_exists('\taktwerk\yiiboilerplate\modules\system\controllers\GoogleSpreadsheetController')){
            return new \taktwerk\yiiboilerplate\modules\system\controllers\GoogleSpreadsheetController("google-spreadsheet", \taktwerk\yiiboilerplate\modules\system\Module::getInstance());
        }
    }
}