<?php
//Generation Date: 18-Sep-2020 07:36:46am
namespace taktwerk\yiiboilerplate\modules\system\models\search;

use taktwerk\yiiboilerplate\modules\system\models\search\base\GoogleSpreadsheet as GoogleSpreadsheetSearchModel;

/**
* GoogleSpreadsheet represents the model behind the search form about `taktwerk\yiiboilerplate\modules\system\models\GoogleSpreadsheet`.
*/
class GoogleSpreadsheet extends GoogleSpreadsheetSearchModel{

}