<?php
//Generation Date: 18-Sep-2020 01:01:44pm
namespace taktwerk\yiiboilerplate\modules\system\models\search;

use taktwerk\yiiboilerplate\modules\system\models\search\base\UserUiData as UserUiDataSearchModel;

/**
* UserUiData represents the model behind the search form about `taktwerk\yiiboilerplate\modules\system\models\UserUiData`.
*/
class UserUiData extends UserUiDataSearchModel{

}