<?php
//Generation Date: 18-Sep-2020 07:36:46am
namespace taktwerk\yiiboilerplate\modules\system\models\search;

use taktwerk\yiiboilerplate\modules\system\models\search\base\Country as CountrySearchModel;

/**
* Country represents the model behind the search form about `taktwerk\yiiboilerplate\modules\system\models\Country`.
*/
class Country extends CountrySearchModel{

}