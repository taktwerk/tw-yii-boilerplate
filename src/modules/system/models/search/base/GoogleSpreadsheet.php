<?php
//Generation Date: 18-Sep-2020 07:36:46am
namespace taktwerk\yiiboilerplate\modules\system\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\system\models\GoogleSpreadsheet as GoogleSpreadsheetModel;

/**
 * GoogleSpreadsheet represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\system\models\GoogleSpreadsheet`.
 */
class GoogleSpreadsheet extends GoogleSpreadsheetModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'user_id',
                    'title',
                    'url',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'type',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GoogleSpreadsheetModel::find();

        $this->parseSearchParams(GoogleSpreadsheetModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(GoogleSpreadsheetModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(GoogleSpreadsheetModel::class),
                'params' => [
                    'page' => $this->parsePageParams(GoogleSpreadsheetModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('user_id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('title', $query);
        $this->applyLikeOperator('url', $query);
        $this->applyLikeOperator('type', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(GoogleSpreadsheetModel::class);
    }
}
