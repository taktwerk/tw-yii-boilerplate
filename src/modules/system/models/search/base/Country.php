<?php
//Generation Date: 18-Sep-2020 07:36:46am
namespace taktwerk\yiiboilerplate\modules\system\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\system\models\Country as CountryModel;

/**
 * Country represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\system\models\Country`.
 */
class Country extends CountryModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'iso',
                    'name',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CountryModel::find();

        $this->parseSearchParams(CountryModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(CountryModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(CountryModel::class),
                'params' => [
                    'page' => $this->parsePageParams(CountryModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('iso', $query);
        $this->applyLikeOperator('name', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(CountryModel::class);
    }
}
