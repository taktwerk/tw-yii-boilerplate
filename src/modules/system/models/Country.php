<?php
//Generation Date: 18-Sep-2020 06:35:03am
namespace taktwerk\yiiboilerplate\modules\system\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\system\models\base\Country as BaseCountry;

/**
 * This is the model class for table "country".
 */
class Country extends BaseCountry
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

}
