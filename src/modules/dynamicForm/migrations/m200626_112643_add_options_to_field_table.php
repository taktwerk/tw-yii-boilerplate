<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200626_112643_add_options_to_field_table extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%dynamic_form_field}}', 'options', $this->text()->null());
    }

    public function down()
    {
        echo "m200626_112643_add_options_to_field_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
