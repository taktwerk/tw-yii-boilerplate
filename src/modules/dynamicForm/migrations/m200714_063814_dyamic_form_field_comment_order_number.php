<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200714_063814_dyamic_form_field_comment_order_number extends TwMigration
{
    public function up()
    {

        $comment = <<<EOT
{ "sortRestrictAttributes":["form_id"]}
EOT;
        $this->addCommentOnColumn('{{%dynamic_form_field}}', 'order_number', $comment);
        

    }

    public function down()
    {
        echo "m200714_063814_dyamic_form_field_comment_order_number cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
