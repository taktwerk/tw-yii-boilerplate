<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200624_052834_create_table_dynamic_form_tables extends TwMigration
{
    public function up()
    {
        $tableOptions = 'ENGINE=InnoDB';
        $connection=Yii::$app->db;
        $transaction=$connection->beginTransaction();
        try{
            $this->createTable('{{%dynamic_form}}',
                [
                    'id'=> Schema::TYPE_PK."",
                    'form_name'=> Schema::TYPE_STRING."(255) NOT NULL",
                    'is_active'=> Schema::TYPE_TINYINT."(4) NOT NULL DEFAULT '1'",
                    'created_at'=> Schema::TYPE_DATETIME."",
                    'created_by'=> Schema::TYPE_INTEGER."(11)",
                    'deleted_by'=> Schema::TYPE_INTEGER."(11)",
                    'deleted_at'=> Schema::TYPE_DATETIME."",
                    'updated_by'=> Schema::TYPE_INTEGER."(11)",
                    'updated_at'=> Schema::TYPE_DATETIME."",
                ], $tableOptions);
            $this->addCommentOnTable('{{%dynamic_form}}', '');
            $this->createIndex('deleted_at', '{{%dynamic_form}}','deleted_at',0);

            $this->createTable('{{%dynamic_form_field}}',
                [
                    'id'=> Schema::TYPE_PK."",
                    'type'=> Schema::TYPE_STRING."(255) NOT NULL",
                    'name'=> Schema::TYPE_STRING."(255) NOT NULL",
                    'text'=> Schema::TYPE_TEXT."",
                    'is_required'=> Schema::TYPE_TINYINT."(4) NOT NULL DEFAULT '0'",
                    'order_number'=> Schema::TYPE_INTEGER."(11) NOT NULL DEFAULT '0'",
                    'created_at'=> Schema::TYPE_DATETIME."",
                    'created_by'=> Schema::TYPE_INTEGER."(11)",
                    'deleted_by'=> Schema::TYPE_INTEGER."(11)",
                    'deleted_at'=> Schema::TYPE_DATETIME."",
                    'updated_by'=> Schema::TYPE_INTEGER."(11)",
                    'updated_at'=> Schema::TYPE_DATETIME."",
                ], $tableOptions);
            $this->createIndex('deleted_at', '{{%dynamic_form_field}}','deleted_at',0);

            $this->createTable('{{%dynamic_form_field_mapping}}',
                [
                    'id'=> Schema::TYPE_PK."",
                    'form_id'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                    'field_id'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                    'created_at'=> Schema::TYPE_DATETIME."",
                    'created_by'=> Schema::TYPE_INTEGER."(11)",
                    'deleted_by'=> Schema::TYPE_INTEGER."(11)",
                    'deleted_at'=> Schema::TYPE_DATETIME."",
                    'updated_by'=> Schema::TYPE_INTEGER."(11)",
                    'updated_at'=> Schema::TYPE_DATETIME."",
                ], $tableOptions);
            $this->addCommentOnTable('{{%dynamic_form_field_mapping}}', '');
            $this->createIndex('deleted_at', '{{%dynamic_form_field_mapping}}','deleted_at',0);
            $this->createIndex('fk_dynamic_form', '{{%dynamic_form_field_mapping}}','form_id',0);
            $this->createIndex('fk_dynamic_form_field', '{{%dynamic_form_field_mapping}}','field_id',0);

            $this->addForeignKey('fk_dynamic_form_field_mapping_form_id', '{{%dynamic_form_field_mapping}}', 'form_id', '{{%dynamic_form}}', 'id');
            $this->addForeignKey('fk_dynamic_form_field_mapping_field_id', '{{%dynamic_form_field_mapping}}', 'field_id', '{{%dynamic_form_field}}', 'id');
            
            $this->addCommentOnTable('{{%dynamic_form_field}}', '{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\dynamicForm"}');
            $this->addCommentOnTable('{{%dynamic_form}}', '{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\dynamicForm"}');
            $this->addCommentOnTable('{{%dynamic_form_field_mapping}}', '{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\dynamicForm"}');

            $transaction->commit();
        } catch (Exception $e) {
            echo 'Catch Exception '.$e->getMessage().' and rollBack this';
            $transaction->rollBack();
        }

    }

    public function down()
    {
        echo "m200624_052834_create_table_dynamic_form_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
