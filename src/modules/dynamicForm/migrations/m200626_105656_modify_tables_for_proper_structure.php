<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200626_105656_modify_tables_for_proper_structure extends TwMigration
{
    public function up()
    {

        $this->alterColumn('{{%dynamic_form}}','is_active', $this->tinyInteger(1)->defaultValue(null));


        $this->createIndex('idx_dynamic_form_field_deleted_at_form_id', '{{%dynamic_form_field}}', ['deleted_at','form_id']);
        $this->alterColumn('{{%dynamic_form_field}}','is_required', $this->tinyInteger(1)->defaultValue(null));


        $this->createIndex('idx_dynamic_form_field_mapping_deleted_at_form_id', '{{%dynamic_form_field_mapping}}', ['deleted_at','form_id']);
        $this->createIndex('idx_dynamic_form_field_mapping_deleted_at_field_id', '{{%dynamic_form_field_mapping}}', ['deleted_at','field_id']);

    }

    public function down()
    {
        echo "m200626_105656_modify_tables_for_proper_structure cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
