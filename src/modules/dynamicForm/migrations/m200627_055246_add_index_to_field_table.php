<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200627_055246_add_index_to_field_table extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_dynamic_form_field_deleted_at_field_id', '{{%dynamic_form_field}}', ['deleted_at','field_id']);

    }

    public function down()
    {
        echo "m200627_055246_add_index_to_field_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
