<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200626_090526_add_form_id_to_field extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%dynamic_form_field}}', 'form_id', $this->integer()->null());
        $this->addForeignKey('fk_dynamic_form_dynamic_form_field', '{{%dynamic_form_field}}', 'form_id', '{{%dynamic_form}}', 'id');
    }

    public function down()
    {
        echo "m200626_090526_add_form_id_to_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
