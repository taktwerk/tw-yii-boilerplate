<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200626_111828_add_field_id_to_field extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%dynamic_form_field}}', 'field_id', $this->integer()->null());
        $this->addForeignKey('fk_dynamic_form_field_dynamic_form_field', '{{%dynamic_form_field}}', 'field_id', '{{%dynamic_form_field}}', 'id');

    }

    public function down()
    {
        echo "m200626_111828_add_field_id_to_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
