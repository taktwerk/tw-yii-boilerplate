<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200707_081907_add_permissions extends TwMigration
{
    /**
     * DynamicFormsAdmin and DynamicFormsViewer roles and permissions
     * @return bool|void|null
     */
    public function up()
    {
        // Create roles
        $this->createRole('DynamicFormsViewer', 'Backend');
        $this->createRole('DynamicFormsAdmin');
        $this->createRole('DynamicFormsAdmin', 'DynamicFormsViewer');

        // Init the permission cascade for see
        $this->addSeeControllerPermission('dynamicForm_dynamic-form', 'DynamicFormsViewer');
        $this->addSeeControllerPermission('dynamicForm_dynamic-form-field', 'DynamicFormsViewer');
        // Administration permission assignment
        $this->addAdminControllerPermission('dynamicForm_dynamic-form','DynamicFormsAdmin');
        $this->addAdminControllerPermission('dynamicForm_dynamic-form-field','DynamicFormsAdmin');

    }

    public function down()
    {
        echo "m200707_081907_add_permissions cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
