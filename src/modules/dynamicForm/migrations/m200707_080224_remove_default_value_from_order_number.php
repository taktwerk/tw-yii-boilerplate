<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200707_080224_remove_default_value_from_order_number extends TwMigration
{
    public function up()
    {
        $this->alterColumn('{{%dynamic_form_field}}', 'order_number', $this->integer(11)->null());

    }

    public function down()
    {
        echo "m200707_080224_remove_default_value_from_order_number cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
