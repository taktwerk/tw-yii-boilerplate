<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200707_062825_change_form_id_order extends TwMigration
{
    public function up()
    {
        $query = <<<EOF
ALTER TABLE `dynamic_form_field` CHANGE `form_id` `form_id` INT(11) NULL DEFAULT NULL AFTER `id`;
ALTER TABLE `dynamic_form_field` CHANGE `is_required` `is_required` TINYINT(1) NULL DEFAULT 0 AFTER `options`;
ALTER TABLE `dynamic_form_field` CHANGE `text` `text` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL AFTER `order_number`;
EOF;
        $this->execute($query);

    }

    public function down()
    {
        echo "m200707_062825_change_form_id_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
