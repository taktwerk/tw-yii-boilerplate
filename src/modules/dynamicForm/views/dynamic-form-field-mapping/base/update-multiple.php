<?php
//Generation Date: 11-Sep-2020 02:35:55pm
use yii\helpers\Html;
use Yii;
/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\dynamicForm\models\DynamicFormFieldMapping $model
 * @var array $pk
 * @var array $show
 */

$this->title = 'Dynamic Form Field Mapping ' . $model->id . ', ' . Yii::t('twbp', 'Edit Multiple');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('twbp','Dynamic Form Field Mappings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('twbp', 'Edit Multiple');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body dynamic-form-field-mapping-update">

        <h1>
            <?= \Yii::t('twbp','Dynamic Form Field Mapping'); ?>
        </h1>

        <div class="crud-navigation">
        </div>

        <?php echo $this->render('_form', [
            'model' => $model,
            'pk' => $pk,
            'show' => $show,
            'multiple' => true,
            'useModal' => $useModal,
            'action' => $action,
        ]); ?>

    </div>
</div>