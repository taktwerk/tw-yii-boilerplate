<?php
use taktwerk\yiiboilerplate\modules\dynamicForm\models\DynamicFormField;

echo $this->render('base/_form', [
    'model' => $model,
    'relatedForm' => $relatedForm,
    'owner' => $owner,
    'relatedType' => $relatedType,
    'ajax' => $ajax,
    'tab_id' => $tab_id
]);
$template = DynamicFormField::TYPE_TEMPLATE;
$js = <<<EOT
$('#DynamicFormField_ajax__field_id_related #dynamicformfield-type').removeAttr('style');
$('#DynamicFormField_ajax__field_id_related select[name="DynamicFormField[type]"] option[value="{$template}"]').remove();
$('.field-form_id_ajax__field_id_related').parents('.form-group-block').hide();
EOT;

$this->registerJs($js);