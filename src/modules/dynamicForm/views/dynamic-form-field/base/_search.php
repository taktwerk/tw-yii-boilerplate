<?php
//Generation Date: 11-Sep-2020 02:35:54pm
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\dynamicForm\models\search\DynamicFormField $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="dynamic-form-field-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'form_id') ?>

        <?= $form->field($model, 'type') ?>

        <?= $form->field($model, 'name') ?>

        <?= $form->field($model, 'order_number') ?>

        <?php // echo $form->field($model, 'text') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'field_id') ?>

        <?php // echo $form->field($model, 'options') ?>

        <?php // echo $form->field($model, 'is_required') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
