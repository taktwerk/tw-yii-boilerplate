<?php

namespace taktwerk\yiiboilerplate\modules\dynamicForm;
use yii\base\InvalidParamException;
use yii\web\NotFoundHttpException;

/**
 * Class Module.
 */
class Module extends \taktwerk\yiiboilerplate\components\TwModule
{
    /**
     * @var string The controller namespace to use
     */
    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\dynamicForm\controllers';

    /**
     * @var string source language for translation
     */
    public $sourceLanguage = 'en-US';

    /**
     * @var null|array The roles which have access to module controllers, eg. ['admin']. If set to `null`, there is no accessFilter applied
     */
    public $accessRoles = null;

    /**
     * @var string Directory URL address, where images files are stored. 'http://my_site_name/upload/dynamicForm/'
     */
    public $imagesUrl = '';

    /**
     * @var string Alias or absolute path to directory where images files are stored. '@frontend/web/upload/dynamicForm/'
     */
    public $imagesPath =  '';

    /**
     * @var string Language selector for ImperaviWidget
     */
    public $imperaviLanguage = 'en';


    /**
     * Init module
     */
    public function init()
    {
        parent::init();
    }
}
