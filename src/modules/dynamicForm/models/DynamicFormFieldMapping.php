<?php

namespace taktwerk\yiiboilerplate\modules\dynamicForm\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\dynamicForm\models\base\DynamicFormFieldMapping as BaseDynamicFormFieldMapping;

/**
 * This is the model class for table "dynamic_form_field_mapping".
 */
class DynamicFormFieldMapping extends BaseDynamicFormFieldMapping
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

}
