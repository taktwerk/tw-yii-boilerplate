<?php

namespace taktwerk\yiiboilerplate\modules\dynamicForm\models\search;

use taktwerk\yiiboilerplate\modules\dynamicForm\models\search\base\DynamicForm as DynamicFormSearchModel;

/**
* DynamicForm represents the model behind the search form about `taktwerk\yiiboilerplate\modules\dynamicForm\models\DynamicForm`.
*/
class DynamicForm extends DynamicFormSearchModel{

}