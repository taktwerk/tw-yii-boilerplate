<?php
//Generation Date: 11-Sep-2020 02:35:54pm
namespace taktwerk\yiiboilerplate\modules\dynamicForm\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\dynamicForm\models\DynamicFormField as DynamicFormFieldModel;

/**
 * DynamicFormField represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\dynamicForm\models\DynamicFormField`.
 */
class DynamicFormField extends DynamicFormFieldModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'form_id',
                    'type',
                    'name',
                    'order_number',
                    'text',
                    'created_at',
                    'created_by',
                    'deleted_by',
                    'deleted_at',
                    'updated_by',
                    'updated_at',
                    'field_id',
                    'options',
                    'is_required',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DynamicFormFieldModel::find();

        $this->parseSearchParams(DynamicFormFieldModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>['order_number'=>SORT_ASC],
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(DynamicFormFieldModel::class),
                'params' => [
                    'page' => $this->parsePageParams(DynamicFormFieldModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('form_id', $query);
        $this->applyHashOperator('order_number', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('field_id', $query);
        $this->applyHashOperator('is_required', $query);
        $this->applyLikeOperator('type', $query);
        $this->applyLikeOperator('name', $query);
        $this->applyLikeOperator('text', $query);
        $this->applyLikeOperator('options', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(DynamicFormFieldModel::class);
    }
}
