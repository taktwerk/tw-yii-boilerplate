<?php
//Generation Date: 11-Sep-2020 02:35:53pm
namespace taktwerk\yiiboilerplate\modules\dynamicForm\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\dynamicForm\models\DynamicForm as DynamicFormModel;

/**
 * DynamicForm represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\dynamicForm\models\DynamicForm`.
 */
class DynamicForm extends DynamicFormModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'form_name',
                    'is_active',
                    'created_at',
                    'created_by',
                    'deleted_by',
                    'deleted_at',
                    'updated_by',
                    'updated_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DynamicFormModel::find();

        $this->parseSearchParams(DynamicFormModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(DynamicFormModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(DynamicFormModel::class),
                'params' => [
                    'page' => $this->parsePageParams(DynamicFormModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('is_active', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyLikeOperator('form_name', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(DynamicFormModel::class);
    }
}
