<?php

namespace taktwerk\yiiboilerplate\modules\dynamicForm\models\search;

use taktwerk\yiiboilerplate\modules\dynamicForm\models\search\base\DynamicFormFieldMapping as DynamicFormFieldMappingSearchModel;

/**
* DynamicFormFieldMapping represents the model behind the search form about `taktwerk\yiiboilerplate\modules\dynamicForm\models\DynamicFormFieldMapping`.
*/
class DynamicFormFieldMapping extends DynamicFormFieldMappingSearchModel{

}