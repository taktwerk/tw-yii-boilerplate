<?php

namespace taktwerk\yiiboilerplate\modules\dynamicForm\controllers;

use taktwerk\yiiboilerplate\modules\dynamicForm\models\DynamicForm;
use taktwerk\yiiboilerplate\modules\dynamicForm\models\DynamicFormField;
use taktwerk\yiiboilerplate\widget\Select2;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the class for controller "DynamicFormController".
 */
class DynamicFormController extends \taktwerk\yiiboilerplate\modules\dynamicForm\controllers\base\DynamicFormController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\dynamicForm\models\DynamicForm';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\dynamicForm\models\search\DynamicForm';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }

    /**
     * @param $model DynamicForm
     * @param $form
     * @return array
     */
    public function formFieldsOverwrite($model, $form)
    {
        /**@var $model DynamicForm*/

        $this->formFieldsOverwrite = [
            'before#is_active' =>
                $form->field(
                    $model,
                    'form_fields',
                    [
                        'selectors' => [
                            'input' => '#' .
                                Html::getInputId($model, 'form_fields') . $owner
                        ]
                    ]
                )
                    ->widget(
                        Select2::class,
                        [
                            'options' => [
                                'id' => Html::getInputId($model, 'form_fields') . $owner,
                                'placeholder' => !$model->isAttributeRequired('form_fields') ? Yii::t('twbp', 'Select a value...') : null,
                            ],
                            'data' => ArrayHelper::map(DynamicFormField::find()->all(), 'id', 'toString'),
                            'hideSearch' => 'true',
                            'pluginOptions' => [
                                'allowClear' => false,
                                'multiple' => true,
                            ],
                        ]
                    )
                    ->hint($model->getAttributeHint('form_fields'))
        ];

        return parent::formFieldsOverwrite($model, $form);
    }
}
