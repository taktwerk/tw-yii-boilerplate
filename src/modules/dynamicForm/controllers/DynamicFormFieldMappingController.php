<?php

namespace taktwerk\yiiboilerplate\modules\dynamicForm\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "DynamicFormFieldMappingController".
 */
class DynamicFormFieldMappingController extends \taktwerk\yiiboilerplate\modules\dynamicForm\controllers\base\DynamicFormFieldMappingController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\dynamicForm\models\DynamicFormFieldMapping';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\dynamicForm\models\search\DynamicFormFieldMapping';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
}
