<?php
//Generation Date: 11-Sep-2020 02:35:53pm
namespace taktwerk\yiiboilerplate\modules\dynamicForm\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * DynamicFormController implements the CRUD actions for DynamicForm model.
 */
class DynamicFormController extends TwCrudController
{
}
