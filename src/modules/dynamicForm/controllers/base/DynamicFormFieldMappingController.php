<?php
//Generation Date: 11-Sep-2020 02:35:55pm
namespace taktwerk\yiiboilerplate\modules\dynamicForm\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * DynamicFormFieldMappingController implements the CRUD actions for DynamicFormFieldMapping model.
 */
class DynamicFormFieldMappingController extends TwCrudController
{
}
