<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200911_210211_add_to_table_user_parameter_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_user_parameter_deleted_at_user_id', '{{%user_parameter}}', ['deleted_at','user_id']);
    }

    public function down()
    {
        echo "m200911_210211_add_to_table_user_parameter_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
