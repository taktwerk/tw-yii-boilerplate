<?php

namespace taktwerk\yiiboilerplate\modules\userParameter\models;

use taktwerk\yiiboilerplate\modules\userParameter\Module;
use Yii;
use \taktwerk\yiiboilerplate\modules\userParameter\models\base\UserParameter as BaseUserParameter;

/**
 * This is the model class for table "user_param".
 */
class UserParameter extends BaseUserParameter
{

//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

    /**
     * @param $key
     * @return bool|mixed
     */
    public static function get($key, $userId = null)
    {
        if ($userParam = self::find()->andWhere([
            'user_id' => !is_null($userId) ? $userId : Yii::$app->user->id,
            'key' => $key])->one()
        ) {
            return Yii::$app->getModule('user-parameter')->params[$key]['type'] == Module::DROPDOWN ?
                unserialize($userParam->value) : $userParam->value;
        }
        return false;
    }
}
