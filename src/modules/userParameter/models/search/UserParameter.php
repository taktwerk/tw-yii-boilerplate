<?php
//Generation Date: 11-Sep-2020 02:57:24pm
namespace taktwerk\yiiboilerplate\modules\userParameter\models\search;

use taktwerk\yiiboilerplate\modules\userParameter\models\search\base\UserParameter as UserParameterSearchModel;

/**
* UserParameter represents the model behind the search form about `taktwerk\yiiboilerplate\modules\userParameter\models\UserParameter`.
*/
class UserParameter extends UserParameterSearchModel{

}