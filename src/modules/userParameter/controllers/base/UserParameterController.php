<?php
//Generation Date: 11-Sep-2020 02:57:24pm
namespace taktwerk\yiiboilerplate\modules\userParameter\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * UserParameterController implements the CRUD actions for UserParameter model.
 */
class UserParameterController extends TwCrudController
{
}
