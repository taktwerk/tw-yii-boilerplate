<?php
//Generation Date: 11-Sep-2020 02:57:24pm
use yii\helpers\Html;
use Yii;
/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\userParameter\models\UserParameter $model
 * @var array $pk
 * @var array $show
 */

$this->title = 'User Parameter ' . $model->id . ', ' . Yii::t('twbp', 'Edit Multiple');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('twbp','User Parameters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('twbp', 'Edit Multiple');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body user-parameter-update">

        <h1>
            <?= \Yii::t('twbp','User Parameter'); ?>
        </h1>

        <div class="crud-navigation">
        </div>

        <?php echo $this->render('_form', [
            'model' => $model,
            'pk' => $pk,
            'show' => $show,
            'multiple' => true,
            'useModal' => $useModal,
            'action' => $action,
        ]); ?>

    </div>
</div>