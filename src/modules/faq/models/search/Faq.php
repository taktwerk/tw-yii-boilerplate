<?php

namespace taktwerk\yiiboilerplate\modules\faq\models\search;

use taktwerk\yiiboilerplate\modules\faq\models\search\base\Faq as FaqSearchModel;

/**
* Faq represents the model behind the search form about `taktwerk\yiiboilerplate\modules\faq\models\Faq`.
*/
class Faq extends FaqSearchModel{

}