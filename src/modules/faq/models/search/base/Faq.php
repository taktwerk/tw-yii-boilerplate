<?php
//Generation Date: 11-Sep-2020 02:35:55pm
namespace taktwerk\yiiboilerplate\modules\faq\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\faq\models\Faq as FaqModel;

/**
 * Faq represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\faq\models\Faq`.
 */
class Faq extends FaqModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'title',
                    'content',
                    'language_id',
                    'place',
                    'level',
                    'order',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FaqModel::find();

        $this->parseSearchParams(FaqModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(FaqModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(FaqModel::class),
                'params' => [
                    'page' => $this->parsePageParams(FaqModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('level', $query);
        $this->applyHashOperator('order', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('title', $query);
        $this->applyLikeOperator('content', $query);
        $this->applyLikeOperator('language_id', $query);
        $this->applyLikeOperator('place', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(FaqModel::class);
    }
}
