<?php

namespace taktwerk\yiiboilerplate\modules\faq\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\faq\models\base\Faq as BaseFaq;
use yii\db\Query;
use taktwerk\yiiboilerplate\models\User;

/**
 * This is the model class for table "faq".
 */
class Faq extends BaseFaq
{
    const ROOT_LEVEL = 0;
    //    /**
    //     * List of additional rules to be applied to model, uncomment to use them
    //     * @return array
    //     */
    //    public function rules()
    //    {
    //        return array_merge(parent::rules(), [
    //            [['something'], 'safe'],
    //        ]);
    //    }
    public static function LanguageList($q = null, $id = null)
    {
        return \taktwerk\yiiboilerplate\models\Language::filter($q);
    }
    public function getLevelName()
    {
        $level = self::find()->andWhere(['id' => $this->level])->one();
        if ($level)
            return $level->toString;
        return Yii::t('twbp', 'Root');
    }
    public static function FaqLevelList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, title AS text')
                ->from(self::tableName())
                ->andWhere(['level' => self::ROOT_LEVEL])
                ->andWhere(['like', 'title', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => User::find($id)->name];
        }
        return $out;
    }

}
