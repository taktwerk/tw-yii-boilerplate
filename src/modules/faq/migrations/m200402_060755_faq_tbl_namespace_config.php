<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200402_060755_faq_tbl_namespace_config extends TwMigration
{
    public function up()
    {
        $tbl = "{{%faq}}";
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\faq"}';
        $this->addCommentOnTable($tbl, $comment);
    }

    public function down()
    {
        echo "m200402_060755_faq_tbl_namespace_config cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
