<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200911_185727_add_to_table_faq_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_faq_deleted_at_language_id', '{{%faq}}', ['deleted_at','language_id']);
    }

    public function down()
    {
        echo "m200911_185727_add_to_table_faq_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
