<?php
/**
 * Copyright (c) 2016.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m180509_000001_create_faq_acl extends TwMigration
{
    public function up()
    {
        $this->createPermission('backend_faq', 'Backend FAQ (for Viewer)', ['Viewer']);
        $this->createPermission('faq', 'FAQ (for Admin)', ['Editor']);
        $this->createCrudPermissions('faq_default', 'FAQ', ['Editor']);
    }

    public function down()
    {
        echo "m160119_083725_faq_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
