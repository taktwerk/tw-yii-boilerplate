<?php
//Generation Date: 11-Sep-2020 02:35:55pm
namespace taktwerk\yiiboilerplate\modules\faq\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * FaqController implements the CRUD actions for Faq model.
 */
class FaqController extends TwCrudController
{
}
