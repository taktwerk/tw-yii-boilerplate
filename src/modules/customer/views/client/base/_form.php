<?php
//Generation Date: 28-Sep-2020 08:46:05am
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\widget\form\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\components\ClassDispenser;
/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\customer\models\Client $model
 * @var taktwerk\yiiboilerplate\widget\form\ActiveForm $form
 * @var boolean $useModal
 * @var boolean $multiple
 * @var array $pk
 * @var array $show
 * @var string $action
 * @var string $owner
 * @var array $languageCodes
 * @var boolean $relatedForm to know if this view is called from ajax related-form action. Since this is passing from
 * select2 (owner) it is always inherit from main form
 * @var string $relatedType type of related form, like above always passing from main form
 * @var string $owner string that representing id of select2 from which related-form action been called. Since we in
 * each new form appending "_related" for next opened form, it is always unique. In main form it is always ID without
 * anything appended
 */

$owner = $relatedForm ? '_' . $owner . '_related' : '';
$relatedTypeForm = Yii::$app->request->get('relatedType')?:$relatedTypeForm;

if (!isset($multiple)) {
$multiple = false;
}

if (!isset($relatedForm)) {
$relatedForm = false;
}

$tableHint = (!empty(taktwerk\yiiboilerplate\modules\customer\models\Client::tableHint()) ? '<div class="table-hint">' . taktwerk\yiiboilerplate\modules\customer\models\Client::tableHint() . '</div><hr />' : '<br />');

?>
<div class="client-form">
        <?php  $formId = 'Client' . ($ajax || $useModal ? '_ajax_' . $owner : ''); ?>    
    <?php $form = ActiveForm::begin([
        'id' => $formId,
        'layout' => 'default',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'action' => $useModal ? $action : '',
        'options' => [
        'name' => 'Client',
    'enctype' => 'multipart/form-data'
        
        ],
    ]); ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>
        <?php echo $tableHint; ?>

        <?php if ($multiple) : ?>
            <?=Html::hiddenInput('update-multiple', true)?>
            <?php foreach ($pk as $id) :?>
                <?=Html::hiddenInput('pk[]', $id)?>
            <?php endforeach;?>
        <?php endif;?>

        <?php $fieldColumns = [
                
            'name' => 
            $form->field(
                $model,
                'name',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'name') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('name'),
                            'id' => Html::getInputId($model, 'name') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('name')),
            'identifier' => 
            $form->field(
                $model,
                'identifier',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'identifier') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('identifier'),
                            'id' => Html::getInputId($model, 'identifier') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('identifier')),
            'website' => 
            $form->field(
                $model,
                'website',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'website') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('website'),
                            'id' => Html::getInputId($model, 'website') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('website')),
            'default_autologin_id' => 
            $form->field(
                $model,
                'default_autologin_id'
            )
                    ->widget(
                        Select2::class,
                        [
                            'data' => taktwerk\yiiboilerplate\modules\customer\models\ClientUser::find()->count() > 50 ? null : ArrayHelper::map(taktwerk\yiiboilerplate\modules\customer\models\ClientUser::find()->all(), 'id', 'toString'),
                            'initValueText' => taktwerk\yiiboilerplate\modules\customer\models\ClientUser::find()->count() > 50 ? \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\modules\customer\models\ClientUser::find()->andWhere(['id' => $model->default_autologin_id])->all(), 'id', 'toString') : '',
                            'options' => [
                                'placeholder' => Yii::t('twbp', 'Select a value...'),
                                'id' => 'default_autologin_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                (taktwerk\yiiboilerplate\modules\customer\models\ClientUser::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                                (taktwerk\yiiboilerplate\modules\customer\models\ClientUser::find()->count() > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'ClientUser\'
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                                    var data_id = $(this);
                                $.post('" .
            Url::toRoute('/customer/client-user/system-entry?id=', true) .
            "' + data_id.val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if(!json.success){
                                                if ((data_id.val() != null) && (data_id.val() != '')) {
                                        // Enable edit icon
                                        $(data_id.next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
            Url::toRoute('/customer/client-user/entry-details?id=', true) .
            "' + data_id.val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#default_autologin_id_well').html(json.data);
                                                //$('#default_autologin_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $(data_id.next().next().children('button')[0]).prop('disabled', true);
                                    }
                                            }else{
                                        // Disable edit icon and remove sub-form
                                        $(data_id.next().next().children('button')[0]).prop('disabled', true);
                                            }
                                        });
                                }",
                            ],
                            'addon' => (Yii::$app->getUser()->can('modules_client-user_create') && (!$relatedForm || ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL && !$useModal))) ? [
                                'append' => [
                                    'content' => [
                                        ClassDispenser::getMappedClass(RelatedForms::class)::widget([
                                            'relatedController' => '/customer/client-user',
                                            'type' => $relatedTypeForm,
                                            'selector' => 'default_autologin_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'primaryKey' => 'id',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    ) . '
                    
                
                <div id="default_autologin_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' . 
                    (taktwerk\yiiboilerplate\modules\customer\models\ClientUser::findOne(['id' => $model->default_autologin_id])
                        ->entryDetails == '' ?
                    ' display:none;' :
                    '') . '
                    ">' . 
                    (taktwerk\yiiboilerplate\modules\customer\models\ClientUser::findOne(['id' => $model->default_autologin_id])->entryDetails != '' ?
                    taktwerk\yiiboilerplate\modules\customer\models\ClientUser::findOne(['id' => $model->default_autologin_id])->entryDetails :
                    '') . ' 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>',
            'info' => 
            $form->field(
                $model,
                'info',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'info') . $owner
                    ]
                ]
            )
                    ->textarea(
                        [
                            'rows' => 6,
                            'placeholder' => $model->getAttributePlaceholder('info'),
                            'id' => Html::getInputId($model, 'info') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('info')),
            'logo_file' => 
            $form->field(
                $model,
                'logo_file',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'logo_file') . $owner
                    ]
                ]
            )->widget(
                ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\FileInput::class),
                [
                    'pluginOptions' => [
                        'showUpload' => false,
                        'showRemove' => false,
                        
                        
                                   'initialPreview' => (!empty($model->logo_file) ? [
                            !empty($model->getMinFilePath('logo_file')  && $model->getFileType('logo_file') == 'video') ? $model->getMinFilePath('logo_file') : $model->getFileUrl('logo_file',[],true)
                        ] : ''),
                        'initialCaption' => $model->logo_file,
                        'initialPreviewAsData' => true,
                        'initialPreviewFileType' => $model->getFileType('logo_file'),
                        'fileActionSettings' => [
                            'indicatorNew' => $model->logo_file === null ? '' : '<a href=" ' . (!empty($model->getMinFilePath('logo_file') && $model->getFileType('logo_file') == 'video') ? $model->getMinFilePath('logo_file') : $model->getFileUrl('logo_file')) . '" target="_blank"><i class="glyphicon glyphicon-hand-down text-warning"></i></a>',
                            'indicatorNewTitle' => \Yii::t('twbp','Download'),
                            'showDrag'=>false,
                        ],
                        'overwriteInitial' => true,
                         'initialPreviewConfig'=> [
                                                     [
                                                         'url' => Url::toRoute(['delete-file','id'=>$model->getPrimaryKey(), 'attribute' => 'logo_file']),
                                                         'downloadUrl'=> !empty($model->getMinFilePath('logo_file')  && $model->getFileType('logo_file') == 'video') ? $model->getMinFilePath('logo_file') : $model->getFileUrl('logo_file'),
                                                         'filetype' => !empty($model->getMinFilePath('logo_file') && $model->getFileType('logo_file') == 'video') ? 'video/mp4':'',
                                                     ],
                                                 ],
                    ],
                     'pluginEvents' => [
                         'fileclear' => 'function() { var prev = $("input[name=\'" + $(this).attr("name") + "\']")[0]; $(prev).val(-1); }',
                        
                        'filepredelete'=>'function(xhr){ 
                               var abort = true; 
                               if (confirm("Are you sure you want to remove this file? This action will delete the file even without pressing the save button")) { 
                                   abort = false; 
                               } 
                               return abort; 
                        }', 
                     ],
                    'options' => [
                        'id' => Html::getInputId($model, 'logo_file') . $owner
                    ]
                ]
            )
            ->hint($model->getAttributeHint('logo_file')),
            'is_system_entry' => 
            $form->field($model, 'is_system_entry')->checkbox(),
            'icon_file' => 
            $form->field(
                $model,
                'icon_file',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'icon_file') . $owner
                    ]
                ]
            )->widget(
                ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\FileInput::class),
                [
                    'pluginOptions' => [
                        'showUpload' => false,
                        'showRemove' => false,
                        
                        
                                   'initialPreview' => (!empty($model->icon_file) ? [
                            !empty($model->getMinFilePath('icon_file')  && $model->getFileType('icon_file') == 'video') ? $model->getMinFilePath('icon_file') : $model->getFileUrl('icon_file',[],true)
                        ] : ''),
                        'initialCaption' => $model->icon_file,
                        'initialPreviewAsData' => true,
                        'initialPreviewFileType' => $model->getFileType('icon_file'),
                        'fileActionSettings' => [
                            'indicatorNew' => $model->icon_file === null ? '' : '<a href=" ' . (!empty($model->getMinFilePath('icon_file') && $model->getFileType('icon_file') == 'video') ? $model->getMinFilePath('icon_file') : $model->getFileUrl('icon_file')) . '" target="_blank"><i class="glyphicon glyphicon-hand-down text-warning"></i></a>',
                            'indicatorNewTitle' => \Yii::t('twbp','Download'),
                            'showDrag'=>false,
                        ],
                        'overwriteInitial' => true,
                         'initialPreviewConfig'=> [
                                                     [
                                                         'url' => Url::toRoute(['delete-file','id'=>$model->getPrimaryKey(), 'attribute' => 'icon_file']),
                                                         'downloadUrl'=> !empty($model->getMinFilePath('icon_file')  && $model->getFileType('icon_file') == 'video') ? $model->getMinFilePath('icon_file') : $model->getFileUrl('icon_file'),
                                                         'filetype' => !empty($model->getMinFilePath('icon_file') && $model->getFileType('icon_file') == 'video') ? 'video/mp4':'',
                                                     ],
                                                 ],
                    ],
                     'pluginEvents' => [
                         'fileclear' => 'function() { var prev = $("input[name=\'" + $(this).attr("name") + "\']")[0]; $(prev).val(-1); }',
                        
                        'filepredelete'=>'function(xhr){ 
                               var abort = true; 
                               if (confirm("Are you sure you want to remove this file? This action will delete the file even without pressing the save button")) { 
                                   abort = false; 
                               } 
                               return abort; 
                        }', 
                     ],
                    'options' => [
                        'id' => Html::getInputId($model, 'icon_file') . $owner
                    ]
                ]
            )
            ->hint($model->getAttributeHint('icon_file')),
            'skin' => 
            $form->field(
                $model,
                'skin',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'skin') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('skin'),
                            'id' => Html::getInputId($model, 'skin') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('skin')),]; ?>        <div class='clearfix'></div>
<?php $twoColumnsForm = true; ?>
<?php 
// form fields overwrite
$fieldColumns = Yii::$app->controller->crudColumnsOverwrite($fieldColumns, 'form', $model, $form, $owner);

foreach($fieldColumns as $attribute => $fieldColumn) {
             if($model->checkIfHidden($attribute))
            {
                echo $fieldColumn;
            }
            else if(!$model->isNewRecord || !$model->checkIfCanvas($attribute))
            {
                if (!$multiple || ($multiple && isset($show[$attribute]))) {
                    if ((isset($show[$attribute]) && $show[$attribute]) || !isset($show[$attribute])) {
                        echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
                        echo $fieldColumn;
                        echo $twoColumnsForm ? '</div>' : '';
                    }else{
                        echo $fieldColumn;
                    }
                }
            }
                
} ?><div class='clearfix'></div>

                <?php $this->endBlock(); ?>
        
        <?= ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_TAB) ?
            Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                    'label' => Yii::t('twbp', 'Client'),
                    'content' => $this->blocks['main'],
                    'active' => true,
                ],
                ]
            ])
            : $this->blocks['main']
        ?>        
        <div class="col-md-12">
        <hr/>
        
        </div>
        
        <div class="clearfix"></div>
        <?php echo $form->errorSummary($model); ?>
        <?php echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::formButtons($model, $useModal, $relatedForm, $multiple, "twbp", ['id' => $model->id]);?>
        <?php ClassDispenser::getMappedClass(ActiveForm::class)::end(); ?>
        <?= ($relatedForm && $relatedType == ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL) ?
        '<div class="clearfix"></div>' :
        ''
        ?>
        <div class="clearfix"></div>
    </div>
</div>

<?php
if ($useModal) {
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::class)::register($this);
}
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
if(!isset($useDependentFieldsJs) || $useDependentFieldsJs==true){
$this->render('@taktwerk-views/_dependent-fields', ['model' => $model,'formId'=>$formId]);
}
?>