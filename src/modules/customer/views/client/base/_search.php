<?php
//Generation Date: 28-Sep-2020 08:46:06am
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\customer\models\search\Client $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="client-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'name') ?>

        <?= $form->field($model, 'identifier') ?>

        <?= $form->field($model, 'website') ?>

        <?= $form->field($model, 'default_autologin_id') ?>

        <?php // echo $form->field($model, 'info') ?>

        <?php // echo $form->field($model, 'logo_file') ?>

        <?php // echo $form->field($model, 'logo_file_filemeta') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

        <?php // echo $form->field($model, 'is_system_entry') ?>

        <?php // echo $form->field($model, 'icon_file') ?>

        <?php // echo $form->field($model, 'icon_file_filemeta') ?>

        <?php // echo $form->field($model, 'skin') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
