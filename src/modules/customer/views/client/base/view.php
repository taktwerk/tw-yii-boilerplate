<?php
//Generation Date: 09-Oct-2020 08:02:02am
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\customer\models\Client $model
 * @var boolean $useModal
 */

$this->title = Yii::t('twbp', 'Client') . ' #' . (is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey) . ', ' . Yii::t('twbp', 'View') . ' - ' . $model->toString();
if(!$fromRelation) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('twbp', 'Clients'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => (string)is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('twbp', 'View');
}
$basePermission = Yii::$app->controller->module->id . '_' . Yii::$app->controller->id;

Yii::$app->controller->renderCustomBlocks(Yii::$app->controller::POS_MAIN);

?>
<div class="box box-default">
	<div class="giiant-crud box-body"
		id="client-view">

		<!-- flash message -->
        <?php if (Yii::$app->session->getFlash('deleteError') !== null) : ?>
            <span class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= Yii::$app->session->getFlash('deleteError') ?>
            </span>
        <?php endif; ?>
        <?php if (!$useModal) : ?>
            <div class="clearfix crud-navigation">
			<!-- menu buttons -->
			<div class='pull-left'>
                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_update'
                    ) && $model->editable() ?
                        Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span> ' . Yii::t('twbp', 'Edit'),
                            [
                                'update',
                                'id' => $model->id,                                 'fromRelation' => $fromRelation,
                                'show_deleted'=>Yii::$app->request->get('show_deleted')
                            ],
                            ['class' => 'btn btn-info']
                        )
                    :
                        null
                    ?>
                     <?php if(method_exists($model,'getUploadFields')  && Yii::$app->getUser()->can(
                        $basePermission . '_recompress'
                    )){
                    	$fields = $model->getUploadFields();
                    	$isCompressible = false;
                    	$compressibleFileTypes = $model::$compressibleFileTypes;
                    	foreach($fields as $field){
                        	if(in_array($model->getFileType($field),$compressibleFileTypes)){
                            	$isCompressible = true;
                            	break;
                        	}
                    	}
                    if($isCompressible){	
                       echo Html::a(
                            '<span class="glyphicon glyphicon-compressed"></span> ' . Yii::t('twbp', 'Recompress'),
                            ['recompress', 'id' => $model->id, 'fromRelation' => $fromRelation],
                            ['class' => 'btn btn-warning','title'=>Yii::t('twbp', 'Adds a compression job for this item\'s files')]
                        );}
                   
                    }?>
                    
                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_create'
                    ) ?
                        Html::a(
                            '<span class="glyphicon glyphicon-copy"></span> ' . Yii::t('twbp', 'Copy'),
                            ['create', 'id' => $model->id, 'fromRelation' => $fromRelation],
                            ['class' => 'btn btn-success']
                        )
                    :
                        null
                    ?>
                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_create'
                    ) ?
                        Html::a(
                            '<span class="glyphicon glyphicon-plus"></span>
				' . Yii::t('twbp', 'New'), ['create', 'fromRelation' =>
				$fromRelation], ['class' => 'btn btn-success create-new'] ) : null
				?>
			</div>
			<div class="pull-right">
                    <?= \taktwerk\yiiboilerplate\widget\CustomActionDropdownWidget::widget(['model'=>$model]) ?>
                    <?php if(Yii::$app->controller->crudMainButtons['listall']): ?>
                    <?= Html::a(
                        '<span class="glyphicon glyphicon-list"></span> '. Yii::t('twbp', 'List {model}',
                            ['model' => \Yii::t('twbp','Clients')]                        ),
                        ['index'],
                        ['class' => 'btn btn-default']
                    ) ?>
                    <?php endif; ?>
                </div>
		</div>
        <?php endif; ?>
        <?php $this->beginBlock('taktwerk\yiiboilerplate\modules\customer\models\Client'); ?>

        <?php $viewColumns = [
                   [
            'attribute'=> 'id'
           ],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],
[
        'attribute' => 'identifier',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->identifier != strip_tags($model->identifier)){
                return \yii\helpers\StringHelper::truncate($model->identifier,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->identifier,500));
            }
        },
    ],
                'website:url',
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::attributeFormat
                [
                    'format' => 'html',
                    'attribute' => 'default_autologin_id',
                    'value' => function ($model) {
                        $foreign = $model->getDefaultAutologin()->one();
                        if ($foreign) {
                            if (Yii::$app->getUser()->can('app_client-user_view') && $foreign->readable()) {
                                return Html::a($foreign->toString, [
                                    'client-user/view',
                                    'id' => $model->getDefaultAutologin()->one()->id,
                                ], ['data-pjax' => 0]);
                            }
                            return $foreign->toString;
                        }
                        return '<span class="label label-warning">?</span>';
                    },
                ],
[
        'attribute' => 'info',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->info != strip_tags($model->info)){
                return \yii\helpers\StringHelper::truncate($model->info,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->info,500));
            }
        },
    ],
[
        'attribute' => 'logo_file',
        'format' => 'html',
        'value' => function ($model) {
                return $model->showFilePreviewWithLink('logo_file');
                },
    ],
                'is_system_entry:boolean',
[
        'attribute' => 'icon_file',
        'format' => 'html',
        'value' => function ($model) {
                return $model->showFilePreviewWithLink('icon_file');
                },
    ],
[
        'attribute' => 'skin',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->skin != strip_tags($model->skin)){
                return \yii\helpers\StringHelper::truncate($model->skin,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->skin,500));
            }
        },
    ],
        ];

        // view columns overwrite
        $viewColumns = Yii::$app->controller->crudColumnsOverwrite($viewColumns, 'view');

        echo DetailView::widget([
        'model' => $model,
        'attributes' => $viewColumns
        ]); ?>

        
        <hr />

        <?=  ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::deleteButton($model, "twbp", ['id' => $model->id], 'view')?>

        <?php $this->endBlock(); ?>


        
        <?php $this->beginBlock('Synced Devices'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsSyncedDevices = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('customer_user-device_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_user-device_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_user-device_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/user/user-device' . '/' . $action;
                        $params['UserDevice'] = [
                            'user_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'user-device'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'uuid',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->uuid != strip_tags($model->uuid)){
                return \yii\helpers\StringHelper::truncate($model->uuid,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->uuid,500));
            }
        },
    ],
[
        'attribute' => 'device_information',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->device_information != strip_tags($model->device_information)){
                return \yii\helpers\StringHelper::truncate($model->device_information,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->device_information,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\user\models\UserDevice::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsSyncedDevices = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsSyncedDevices, 'tab'):$columnsSyncedDevices;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('customer_user-device_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Synced Device'),
                [
                    '/user/user-device/create',
                    'UserDevice' => [
                        'user_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getSyncedDevices(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-synceddevices',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsSyncedDevices
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Last Synced Devices'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsLastSyncedDevices = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('customer_user-device_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_user-device_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_user-device_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/user/user-device' . '/' . $action;
                        $params['UserDevice'] = [
                            'user_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'user-device'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'uuid',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->uuid != strip_tags($model->uuid)){
                return \yii\helpers\StringHelper::truncate($model->uuid,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->uuid,500));
            }
        },
    ],
[
        'attribute' => 'device_information',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->device_information != strip_tags($model->device_information)){
                return \yii\helpers\StringHelper::truncate($model->device_information,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->device_information,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\user\models\UserDevice::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsLastSyncedDevices = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsLastSyncedDevices, 'tab'):$columnsLastSyncedDevices;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('customer_user-device_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Last Synced Device'),
                [
                    '/user/user-device/create',
                    'UserDevice' => [
                        'user_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getLastSyncedDevices(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-lastsynceddevices',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsLastSyncedDevices
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Sync Processes'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsSyncProcesses = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('customer_sync-process_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_sync-process_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_sync-process_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/sync/sync-process' . '/' . $action;
                        $params['SyncProcess'] = [
                            'device_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'sync-process'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'progress',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->progress != strip_tags($model->progress)){
                return \yii\helpers\StringHelper::truncate($model->progress,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->progress,500));
            }
        },
    ],
[
        'attribute' => 'all_items_count',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->all_items_count != strip_tags($model->all_items_count)){
                return \yii\helpers\StringHelper::truncate($model->all_items_count,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->all_items_count,500));
            }
        },
    ],
[
        'attribute' => 'synced_items_count',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->synced_items_count != strip_tags($model->synced_items_count)){
                return \yii\helpers\StringHelper::truncate($model->synced_items_count,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->synced_items_count,500));
            }
        },
    ],

                [
                    'attribute' => 'status',
                    'value' => function ($model) {
                        return \Yii::t('twbp', $model->status);
                    },
                ],
[
        'attribute' => 'description',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->description != strip_tags($model->description)){
                return \yii\helpers\StringHelper::truncate($model->description,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->description,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\sync\models\SyncProcess::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsSyncProcesses = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsSyncProcesses, 'tab'):$columnsSyncProcesses;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('customer_sync-process_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Sync Process'),
                [
                    '/sync/sync-process/create',
                    'SyncProcess' => [
                        'device_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getSyncProcesses(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-syncprocesses',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsSyncProcesses
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Users'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsUsers = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('customer_user_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_user_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_user_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/usermanager/user' . '/' . $action;
                        $params['User'] = [
                            'id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'user'
                ],
[
        'attribute' => 'identifier',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->identifier != strip_tags($model->identifier)){
                return \yii\helpers\StringHelper::truncate($model->identifier,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->identifier,500));
            }
        },
    ],
[
        'attribute' => 'username',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->username != strip_tags($model->username)){
                return \yii\helpers\StringHelper::truncate($model->username,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->username,500));
            }
        },
    ],
                'email:email',
[
        'attribute' => 'password_hash',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->password_hash != strip_tags($model->password_hash)){
                return \yii\helpers\StringHelper::truncate($model->password_hash,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->password_hash,500));
            }
        },
    ],
[
        'attribute' => 'auth_key',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->auth_key != strip_tags($model->auth_key)){
                return \yii\helpers\StringHelper::truncate($model->auth_key,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->auth_key,500));
            }
        },
    ],
                'unconfirmed_email:email',
[
        'attribute' => 'registration_ip',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->registration_ip != strip_tags($model->registration_ip)){
                return \yii\helpers\StringHelper::truncate($model->registration_ip,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->registration_ip,500));
            }
        },
    ],
[
        'attribute' => 'flags',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->flags != strip_tags($model->flags)){
                return \yii\helpers\StringHelper::truncate($model->flags,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->flags,500));
            }
        },
    ],
[
        'attribute' => 'confirmed_at',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->confirmed_at != strip_tags($model->confirmed_at)){
                return \yii\helpers\StringHelper::truncate($model->confirmed_at,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->confirmed_at,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\user\models\User::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsUsers = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsUsers, 'tab'):$columnsUsers;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('customer_user_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','User'),
                [
                    '/usermanager/user/create',
                    'User' => [
                        'id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getUsers(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-users',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsUsers
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Client Devices'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsClientDevices = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('customer_client-device_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_client-device_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_client-device_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/customer/client-device' . '/' . $action;
                        $params['ClientDevice'] = [
                            'client_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'client-device'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'client_user_id',
                    'value' => function ($model) {
                        if ($rel = $model->getClientUser()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'client-user/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],
[
        'attribute' => 'number',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->number != strip_tags($model->number)){
                return \yii\helpers\StringHelper::truncate($model->number,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->number,500));
            }
        },
    ],
                'last_used_at:datetime',
                'is_blocked:boolean',
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\customer\models\ClientDevice::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsClientDevices = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsClientDevices, 'tab'):$columnsClientDevices;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('customer_client-device_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Client Device'),
                [
                    '/customer/client-device/create',
                    'ClientDevice' => [
                        'client_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getClientDevices(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-clientdevices',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsClientDevices
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Client Users'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsClientUsers = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('customer_client-user_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_client-user_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_client-user_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/customer/client-user' . '/' . $action;
                        $params['ClientUser'] = [
                            'client_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'client-user'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'user_id',
                    'value' => function ($model) {
                        if ($rel = $model->getUser()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'user/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                'is_default_autologin:boolean',
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\customer\models\ClientUser::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsClientUsers = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsClientUsers, 'tab'):$columnsClientUsers;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('customer_client-user_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Client User'),
                [
                    '/customer/client-user/create',
                    'ClientUser' => [
                        'client_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getClientUsers(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-clientusers',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsClientUsers
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Protocols'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsProtocols = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('customer_protocol_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_protocol_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_protocol_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/protocol/protocol' . '/' . $action;
                        $params['Protocol'] = [
                            'client_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'protocol'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'protocol_template_id',
                    'value' => function ($model) {
                        if ($rel = $model->getProtocolTemplate()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'protocol-template/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'workflow_step_id',
                    'value' => function ($model) {
                        if ($rel = $model->getWorkflowStep()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'workflow-step/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],
[
        'attribute' => 'protocol_form_table',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->protocol_form_table != strip_tags($model->protocol_form_table)){
                return \yii\helpers\StringHelper::truncate($model->protocol_form_table,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->protocol_form_table,500));
            }
        },
    ],
[
        'attribute' => 'protocol_form_number',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->protocol_form_number != strip_tags($model->protocol_form_number)){
                return \yii\helpers\StringHelper::truncate($model->protocol_form_number,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->protocol_form_number,500));
            }
        },
    ],
[
        'attribute' => 'reference_model',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->reference_model != strip_tags($model->reference_model)){
                return \yii\helpers\StringHelper::truncate($model->reference_model,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->reference_model,500));
            }
        },
    ],
[
        'attribute' => 'reference_id',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->reference_id != strip_tags($model->reference_id)){
                return \yii\helpers\StringHelper::truncate($model->reference_id,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->reference_id,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\protocol\models\Protocol::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsProtocols = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsProtocols, 'tab'):$columnsProtocols;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('customer_protocol_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Protocol'),
                [
                    '/protocol/protocol/create',
                    'Protocol' => [
                        'client_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getProtocols(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-protocols',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsProtocols
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Protocol Templates'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsProtocolTemplates = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('customer_protocol-template_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_protocol-template_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_protocol-template_recompress') ? '{recompress} ' : '') . (Yii::$app->getUser()->can('customer_protocol-template_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/protocol/protocol-template' . '/' . $action;
                        $params['ProtocolTemplate'] = [
                            'client_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'protocol-template'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'workflow_id',
                    'value' => function ($model) {
                        if ($rel = $model->getWorkflow()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'workflow/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'protocol_form_table',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->protocol_form_table != strip_tags($model->protocol_form_table)){
                return \yii\helpers\StringHelper::truncate($model->protocol_form_table,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->protocol_form_table,500));
            }
        },
    ],
[
        'attribute' => 'protocol_file',
        'format' => 'html',
        'value' => function ($model) {
                return $model->showFilePreviewWithLink('protocol_file');
                },
    ],
[
        'attribute' => 'thumbnail',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->thumbnail != strip_tags($model->thumbnail)){
                return \yii\helpers\StringHelper::truncate($model->thumbnail,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->thumbnail,500));
            }
        },
    ],
[
        'attribute' => 'pdf_image',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->pdf_image != strip_tags($model->pdf_image)){
                return \yii\helpers\StringHelper::truncate($model->pdf_image,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->pdf_image,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsProtocolTemplates = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsProtocolTemplates, 'tab'):$columnsProtocolTemplates;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('customer_protocol-template_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Protocol Template'),
                [
                    '/protocol/protocol-template/create',
                    'ProtocolTemplate' => [
                        'client_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getProtocolTemplates(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-protocoltemplates',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsProtocolTemplates
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Workflows'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsWorkflows = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('customer_workflow_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_workflow_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_workflow_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/workflow/workflow' . '/' . $action;
                        $params['Workflow'] = [
                            'client_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'workflow'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\workflow\models\Workflow::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsWorkflows = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsWorkflows, 'tab'):$columnsWorkflows;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('customer_workflow_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Workflow'),
                [
                    '/workflow/workflow/create',
                    'Workflow' => [
                        'client_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getWorkflows(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-workflows',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsWorkflows
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Client Model Types'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsClientModelTypes = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('customer_client-model-type_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_client-model-type_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_client-model-type_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/customer/client-model-type' . '/' . $action;
                        $params['ClientModelType'] = [
                            'client_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'client-model-type'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'type',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->type != strip_tags($model->type)){
                return \yii\helpers\StringHelper::truncate($model->type,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->type,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\customer\models\ClientModelType::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsClientModelTypes = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsClientModelTypes, 'tab'):$columnsClientModelTypes;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('customer_client-model-type_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Client Model Type'),
                [
                    '/customer/client-model-type/create',
                    'ClientModelType' => [
                        'client_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getClientModelTypes(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-clientmodeltypes',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsClientModelTypes
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Feedbacks'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsFeedbacks = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('customer_feedback_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_feedback_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_feedback_recompress') ? '{recompress} ' : '') . (Yii::$app->getUser()->can('customer_feedback_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/feedback/feedback' . '/' . $action;
                        $params['Feedback'] = [
                            'client_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'feedback'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'attached_file',
        'format' => 'html',
        'value' => function ($model) {
                return $model->showFilePreviewWithLink('attached_file');
                },
    ],
[
        'attribute' => 'title',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->title != strip_tags($model->title)){
                return \yii\helpers\StringHelper::truncate($model->title,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->title,500));
            }
        },
    ],
[
        'attribute' => 'description',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->description != strip_tags($model->description)){
                return \yii\helpers\StringHelper::truncate($model->description,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->description,500));
            }
        },
    ],
                'feedback_url:url',
[
        'attribute' => 'reference_model',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->reference_model != strip_tags($model->reference_model)){
                return \yii\helpers\StringHelper::truncate($model->reference_model,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->reference_model,500));
            }
        },
    ],
[
        'attribute' => 'reference',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->reference != strip_tags($model->reference)){
                return \yii\helpers\StringHelper::truncate($model->reference,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->reference,500));
            }
        },
    ],

                [
                    'attribute' => 'status',
                    'value' => function ($model) {
                        return \Yii::t('twbp', $model->status);
                    },
                ],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\feedback\models\Feedback::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsFeedbacks = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsFeedbacks, 'tab'):$columnsFeedbacks;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('customer_feedback_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Feedback'),
                [
                    '/feedback/feedback/create',
                    'Feedback' => [
                        'client_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getFeedbacks(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-feedbacks',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsFeedbacks
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Groups'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsGroups = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('customer_group_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_group_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_group_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/user/group' . '/' . $action;
                        $params['Group'] = [
                            'client_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'group'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\user\models\Group::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsGroups = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGroups, 'tab'):$columnsGroups;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('customer_group_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Group'),
                [
                    '/user/group/create',
                    'Group' => [
                        'client_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getGroups(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-groups',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsGroups
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Guides'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsGuides = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('customer_guide_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_guide_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_guide_recompress') ? '{recompress} ' : '') . (Yii::$app->getUser()->can('customer_guide_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/guide/guide' . '/' . $action;
                        $params['Guide'] = [
                            'client_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'guide'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'short_name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->short_name != strip_tags($model->short_name)){
                return \yii\helpers\StringHelper::truncate($model->short_name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->short_name,500));
            }
        },
    ],
[
        'attribute' => 'title',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->title != strip_tags($model->title)){
                return \yii\helpers\StringHelper::truncate($model->title,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->title,500));
            }
        },
    ],
[
        'attribute' => 'description',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->description != strip_tags($model->description)){
                return \yii\helpers\StringHelper::truncate($model->description,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->description,500));
            }
        },
    ],
[
        'attribute' => 'preview_file',
        'format' => 'html',
        'value' => function ($model) {
                return $model->showFilePreviewWithLink('preview_file');
                },
    ],
                'is_collection:boolean',
[
        'attribute' => 'revision_term',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->revision_term != strip_tags($model->revision_term)){
                return \yii\helpers\StringHelper::truncate($model->revision_term,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->revision_term,500));
            }
        },
    ],
[
        'attribute' => 'revision_counter',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->revision_counter != strip_tags($model->revision_counter)){
                return \yii\helpers\StringHelper::truncate($model->revision_counter,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->revision_counter,500));
            }
        },
    ],
[
        'attribute' => 'duration',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->duration != strip_tags($model->duration)){
                return \yii\helpers\StringHelper::truncate($model->duration,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->duration,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\guide\models\Guide::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsGuides = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGuides, 'tab'):$columnsGuides;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('customer_guide_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Guide'),
                [
                    '/guide/guide/create',
                    'Guide' => [
                        'client_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getGuides(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-guides',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsGuides
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Guide Assets'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsGuideAssets = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('customer_guide-asset_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_guide-asset_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_guide-asset_recompress') ? '{recompress} ' : '') . (Yii::$app->getUser()->can('customer_guide-asset_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/guide/guide-asset' . '/' . $action;
                        $params['GuideAsset'] = [
                            'client_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'guide-asset'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],
[
        'attribute' => 'asset_file',
        'format' => 'html',
        'value' => function ($model) {
                return $model->showFilePreviewWithLink('asset_file');
                },
    ],
[
        'attribute' => 'asset_html',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->asset_html != strip_tags($model->asset_html)){
                return \yii\helpers\StringHelper::truncate($model->asset_html,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->asset_html,500));
            }
        },
    ],
[
        'attribute' => 'pdf_image',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->pdf_image != strip_tags($model->pdf_image)){
                return \yii\helpers\StringHelper::truncate($model->pdf_image,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->pdf_image,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\guide\models\GuideAsset::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsGuideAssets = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGuideAssets, 'tab'):$columnsGuideAssets;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('customer_guide-asset_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Guide Asset'),
                [
                    '/guide/guide-asset/create',
                    'GuideAsset' => [
                        'client_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getGuideAssets(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-guideassets',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsGuideAssets
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Guide Categories'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsGuideCategories = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('customer_guide-category_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_guide-category_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_guide-category_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/guide/guide-category' . '/' . $action;
                        $params['GuideCategory'] = [
                            'client_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'guide-category'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\guide\models\GuideCategory::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsGuideCategories = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGuideCategories, 'tab'):$columnsGuideCategories;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('customer_guide-category_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Guide Category'),
                [
                    '/guide/guide-category/create',
                    'GuideCategory' => [
                        'client_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getGuideCategories(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-guidecategories',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsGuideCategories
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Guide Templates'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsGuideTemplates = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('customer_guide-template_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_guide-template_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_guide-template_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/guide/guide-template' . '/' . $action;
                        $params['GuideTemplate'] = [
                            'client_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'guide-template'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'name',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
            }
        },
    ],
[
        'attribute' => 'script',
        'format' => 'html',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->script != strip_tags($model->script)){
                return \yii\helpers\StringHelper::truncate($model->script,500,'...',null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->script,500));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\guide\models\GuideTemplate::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsGuideTemplates = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGuideTemplates, 'tab'):$columnsGuideTemplates;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('customer_guide-template_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Guide Template'),
                [
                    '/guide/guide-template/create',
                    'GuideTemplate' => [
                        'client_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getGuideTemplates(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-guidetemplates',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsGuideTemplates
        ])?>
        </div>
                <?php $this->endBlock() ?>

<?php /* MAPPED MODELS RELATIONS - START*/?>
<?php 
$extraTabItems = [];
foreach(\Yii::$app->modelNewRelationMapper->newRelationsMapping as $modelClass=>$relations){
    if(is_a($model,$modelClass)){
        foreach($relations as $relName => $relation){
            $relationKey = array_key_first($relation[1]);
            $foreignKey= $relation[1][$relationKey];
            $relClass = $relation[0];
            $baseName = \yii\helpers\StringHelper::basename($relClass);
            $cntrler = Inflector::camel2id($baseName, '-', true);
            $pluralName= Inflector::camel2words(Inflector::pluralize($baseName));
            $singularName = Inflector::camel2words(Inflector::singularize($baseName));
            $basePerm =  \Yii::$app->controller->module->id.'_'.$cntrler;
            $this->beginBlock($pluralName);
?>
<?php 
$relatedModelObject = Yii::createObject($relClass::className());
$relationController = $relatedModelObject->getControllerInstance();
//TODO CrudHelper::getColumnFormat('\app\modules\guide\models\Guide', 'id');
?>
                    <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
       	<div class="clearfix"></div>
        <div>
        <?php $relMapCols = [[
                    'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can($basePerm.'_view') ? '{view} ' : '') . (Yii::$app->getUser()->can($basePerm.'_update') ? '{update} ' : '') . (Yii::$app->getUser()->can($basePerm.'_delete') ? '{delete} ' : ''),
            'urlCreator' => function ($action, $relation_model, $key, $index) use($model,$relationKey,$foreignKey,$baseName,$relatedBaseUrl) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = $relatedBaseUrl . $action;
                        
                        $params[$baseName] = [
                            $relationKey => $model->{$foreignKey}
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) { 
                             return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => $cntrler
                ],
                
        ];

        $relMapCols =array_merge($relMapCols,array_slice(array_keys($relatedModelObject->attributes),0,5));
        
        $relMapCols = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($relMapCols, 'tab'):$relMapCols;
        
        echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can($basePerm.'_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp',$singularName),
                [
                    '/'.$relationController->module->id.'/'.$cntrler.'/create',
                    $baseName => [
                        $relationKey => $model->{$foreignKey}
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[
                'class'=>'grid-outer-div'
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->{'get'.$relName}(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-'.$relName,
                    ]
                ]
                ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            'columns' =>$relMapCols
        ])?>
        </div>
                <?php $this->endBlock();
                $extraTabItems[] = [
                    'content' => $this->blocks[$pluralName],
                    'label' => '<small>' .
                    \Yii::t('twbp', $pluralName) .
                    '&nbsp;<span class="badge badge-default">' .
                    $model->{'get'.$relName}()->count() .
                    '</span></small>',
                    'active' => false,
                    'visible' =>
                    Yii::$app->user->can('x_'.$basePerm.'_see') && Yii::$app->controller->crudRelations(ucfirst($relName)),
                    ];
?>

<?php 
        }
    }
}
?>

                
<?php /* MAPPED MODELS RELATIONS - END*/ ?>
        <?= Tabs::widget(
            [
                'id' => 'relation-tabs',
                'encodeLabels' => false,
                'items' =>  array_filter(array_merge([
                    [
                        'label' => '<b class=""># ' . $model->id . '</b>',
                        'content' => $this->blocks['taktwerk\yiiboilerplate\modules\customer\models\Client'],
                        'active' => true,
                    ],
        (\Yii::$app->hasModule('user'))?[
                        'content' => $this->blocks['Synced Devices'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Synced Devices') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getSyncedDevices()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_customer_user-device_see') && Yii::$app->controller->crudRelations('SyncedDevices'),
                    ]:null,
        (\Yii::$app->hasModule('user'))?[
                        'content' => $this->blocks['Last Synced Devices'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Last Synced Devices') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getLastSyncedDevices()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_customer_user-device_see') && Yii::$app->controller->crudRelations('LastSyncedDevices'),
                    ]:null,
        (\Yii::$app->hasModule('sync'))?[
                        'content' => $this->blocks['Sync Processes'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Sync Processes') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getSyncProcesses()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_customer_sync-process_see') && Yii::$app->controller->crudRelations('SyncProcesses'),
                    ]:null,
        (\Yii::$app->hasModule('usermanager'))?[
                        'content' => $this->blocks['Users'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Users') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getUsers()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_customer_user_see') && Yii::$app->controller->crudRelations('Users'),
                    ]:null,
        (\Yii::$app->hasModule('customer'))?[
                        'content' => $this->blocks['Client Devices'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Client Devices') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getClientDevices()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_customer_client-device_see') && Yii::$app->controller->crudRelations('ClientDevices'),
                    ]:null,
        (\Yii::$app->hasModule('customer'))?[
                        'content' => $this->blocks['Client Users'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Client Users') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getClientUsers()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_customer_client-user_see') && Yii::$app->controller->crudRelations('ClientUsers'),
                    ]:null,
        (\Yii::$app->hasModule('protocol'))?[
                        'content' => $this->blocks['Protocols'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Protocols') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getProtocols()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_customer_protocol_see') && Yii::$app->controller->crudRelations('Protocols'),
                    ]:null,
        (\Yii::$app->hasModule('protocol'))?[
                        'content' => $this->blocks['Protocol Templates'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Protocol Templates') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getProtocolTemplates()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_customer_protocol-template_see') && Yii::$app->controller->crudRelations('ProtocolTemplates'),
                    ]:null,
        (\Yii::$app->hasModule('workflow'))?[
                        'content' => $this->blocks['Workflows'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Workflows') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getWorkflows()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_customer_workflow_see') && Yii::$app->controller->crudRelations('Workflows'),
                    ]:null,
        (\Yii::$app->hasModule('customer'))?[
                        'content' => $this->blocks['Client Model Types'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Client Model Types') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getClientModelTypes()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_customer_client-model-type_see') && Yii::$app->controller->crudRelations('ClientModelTypes'),
                    ]:null,
        (\Yii::$app->hasModule('feedback'))?[
                        'content' => $this->blocks['Feedbacks'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Feedbacks') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getFeedbacks()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_customer_feedback_see') && Yii::$app->controller->crudRelations('Feedbacks'),
                    ]:null,
        (\Yii::$app->hasModule('user'))?[
                        'content' => $this->blocks['Groups'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Groups') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getGroups()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_customer_group_see') && Yii::$app->controller->crudRelations('Groups'),
                    ]:null,
        (\Yii::$app->hasModule('guide'))?[
                        'content' => $this->blocks['Guides'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Guides') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getGuides()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_customer_guide_see') && Yii::$app->controller->crudRelations('Guides'),
                    ]:null,
        (\Yii::$app->hasModule('guide'))?[
                        'content' => $this->blocks['Guide Assets'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Guide Assets') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getGuideAssets()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_customer_guide-asset_see') && Yii::$app->controller->crudRelations('GuideAssets'),
                    ]:null,
        (\Yii::$app->hasModule('guide'))?[
                        'content' => $this->blocks['Guide Categories'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Guide Categories') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getGuideCategories()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_customer_guide-category_see') && Yii::$app->controller->crudRelations('GuideCategories'),
                    ]:null,
        (\Yii::$app->hasModule('guide'))?[
                        'content' => $this->blocks['Guide Templates'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Guide Templates') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getGuideTemplates()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_customer_guide-template_see') && Yii::$app->controller->crudRelations('GuideTemplates'),
                    ]:null,
                    [
                        'content' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\HistoryTab::class)::widget(['model' => $model]),
                        'label' => '<small>' .
                            \Yii::t('twbp', 'History') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getHistory()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('Administrator'),
                    ],
                ]
                ,$extraTabItems))
            ]
        );
        ?>
        <?= ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\RecordHistory::class)::widget(['model' => $model]) ?>
    </div>
</div>

<?php ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
$js = <<<JS
$(document).ready(function() {
    var hash=document.location.hash;
	var prefix="tab_" ;
    if (hash) {
        $('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
    } 
    
    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash=e.target.hash.replace(	"#", "#" + prefix);
    });
});
JS;

$this->registerJs($js);

Yii::$app->controller->renderCustomBlocks(Yii::$app->controller::POS_END);
