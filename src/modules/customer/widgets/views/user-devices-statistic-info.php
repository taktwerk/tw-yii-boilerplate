    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?=$devicesCount?:0;?></h3>
                <p><?=Yii::t('twbp', 'Devices');?></p>
            </div>
            <div class="icon">
                <i class="fa fa-list"></i>
            </div>
            <a href="<?=\yii\helpers\Url::toRoute("/customer/client-device")?>" class="small-box-footer">
                <?=Yii::t('twbp', 'More Info');?> <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
