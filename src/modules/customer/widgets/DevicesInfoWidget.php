<?php

namespace taktwerk\yiiboilerplate\modules\customer\widgets;

use taktwerk\yiiboilerplate\modules\customer\models\ClientDevice;
use yii\base\Widget;

class DevicesInfoWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $devicesCount = ClientDevice::find()
            ->findWithSystemEntry(ClientDevice::tableName(), true)
            ->count();

        return $this->render('user-devices-statistic-info', ['devicesCount' => $devicesCount]);
    }
}
