<?php

namespace taktwerk\yiiboilerplate\modules\customer;

/**
 * customer module definition class
 */
class Module extends \taktwerk\yiiboilerplate\components\TwModule
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\customer\controllers';
    
    /**
     * List of tables allowed in ClientModelType
     * @var array
     */
    public $clientModelTypes=[];
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->setViewPath(\Yii::getAlias("@taktwerk-boilerplate/modules/customer/views"));
        parent::init();

        // custom initialization code goes here
    }
}
