<?php

namespace taktwerk\yiiboilerplate\modules\customer\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\customer\models\base\ClientDevice as BaseClientDevice;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "client_device".
 */
class ClientDevice extends BaseClientDevice
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return ArrayHelper::merge(self::extraFields(),[
            'client',
            'clientUser',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(
            Client::class,
            ['id' => 'client_id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function clientList($q = null)
    {
        return Client::filter($q);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientUser()
    {
        return $this->hasOne(
            ClientUser::class,
            ['id' => 'client_user_id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function clientUserList($q = null)
    {
        return ClientUser::filter($q);
    }


}
