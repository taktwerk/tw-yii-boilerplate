<?php

namespace taktwerk\yiiboilerplate\modules\customer\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\customer\models\base\ClientModelType as BaseClientModelType;
use yii\helpers\Html;

/**
 * This is the model class for table "client_model_type".
 */
class ClientModelType extends BaseClientModelType
{
    public function toString()
    {
        if($this->icon_file)
            return Html::img($this->getFileUrl('icon_file',['height'=>30]),['height'=>30]).' '. \Yii::t('app',$this->type); // this attribute can be modified
            return \Yii::t('app',$this->type);
    }
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['type', 'in','range'=>\Yii::$app->getModule('customer')->clientModelTypes],
        ]);
    }
    
    /**
     * Don't allow clients to see our other clients.
     *
     * @param $removedDeleted bool To remove soft-deleted entries from results, false to include them in re
     * @inheritdoc
     */
    public static function find($removedDeleted = true)
    {
        if(is_array($removedDeleted)){
            $findParams = $removedDeleted;
            $defaultFindParams = [
                'filterClient'=>true,
                'removeDeleted'=>true
            ];
            $findParams = array_merge($defaultFindParams,$findParams);
        }else{
            $findParams = [
                'filterClient'=>true,
                'removeDeleted'=>$removedDeleted
            ];
        }
        $model = parent::find($findParams);
        // If not authority, only show tickets of our customers
        if (php_sapi_name() != "cli" && !Yii::$app->getUser()->can('Authority') && $findParams['filterClient']==true) {
            $clients = [];
            $clientUsers = ClientUser::findAll(['user_id' => Yii::$app->getUser()->id]);
            foreach ($clientUsers as $client) {
                $clients[] = $client->client_id;
            }
            
            if (empty($clients)) {
                //$model->andWhere([self::tableName().'.client_id' => 1]);
                $model->andWhere([self::tableName().'.id' => null]);
            } else {
                $model->andWhere([
                    self::tableName().'.client_id' => $clients
                ]);
            }
        }
        
        return $model;
    }

}
