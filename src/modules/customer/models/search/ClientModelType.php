<?php

namespace taktwerk\yiiboilerplate\modules\customer\models\search;

use taktwerk\yiiboilerplate\modules\customer\models\search\base\ClientModelType as ClientModelTypeSearchModel;

/**
* ClientModelType represents the model behind the search form about `taktwerk\yiiboilerplate\modules\customer\models\ClientModelType`.
*/
class ClientModelType extends ClientModelTypeSearchModel{

}