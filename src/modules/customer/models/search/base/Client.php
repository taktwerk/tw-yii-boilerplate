<?php
//Generation Date: 28-Sep-2020 08:46:05am
namespace taktwerk\yiiboilerplate\modules\customer\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\customer\models\Client as ClientModel;

/**
 * Client represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\customer\models\Client`.
 */
class Client extends ClientModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'name',
                    'identifier',
                    'website',
                    'default_autologin_id',
                    'info',
                    'logo_file',
                    'logo_file_filemeta',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_system_entry',
                    'icon_file',
                    'icon_file_filemeta',
                    'skin',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientModel::find();

        $this->parseSearchParams(ClientModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(ClientModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(ClientModel::class),
                'params' => [
                    'page' => $this->parsePageParams(ClientModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('default_autologin_id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyHashOperator('is_system_entry', $query);
        $this->applyLikeOperator('name', $query);
        $this->applyLikeOperator('identifier', $query);
        $this->applyLikeOperator('website', $query);
        $this->applyLikeOperator('info', $query);
        $this->applyLikeOperator('logo_file', $query);
        $this->applyLikeOperator('logo_file_filemeta', $query);
        $this->applyLikeOperator('icon_file', $query);
        $this->applyLikeOperator('icon_file_filemeta', $query);
        $this->applyLikeOperator('skin', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(ClientModel::class);
    }
}
