<?php
//Generation Date: 11-Sep-2020 02:34:26pm
namespace taktwerk\yiiboilerplate\modules\customer\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\customer\models\ClientDevice as ClientDeviceModel;

/**
 * ClientDevice represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\customer\models\ClientDevice`.
 */
class ClientDevice extends ClientDeviceModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'client_id',
                    'client_user_id',
                    'name',
                    'number',
                    'last_used_at',
                    'is_blocked',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientDeviceModel::findWithOutSystemEntry();

        $this->parseSearchParams(ClientDeviceModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(ClientDeviceModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(ClientDeviceModel::class),
                'params' => [
                    'page' => $this->parsePageParams(ClientDeviceModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('client_id', $query);
        $this->applyHashOperator('client_user_id', $query);
        $this->applyHashOperator('is_blocked', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('name', $query);
        $this->applyLikeOperator('number', $query);
        $this->applyDateOperator('last_used_at', $query, true);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(ClientDeviceModel::class);
    }
}
