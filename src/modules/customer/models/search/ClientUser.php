<?php

namespace taktwerk\yiiboilerplate\modules\customer\models\search;

use taktwerk\yiiboilerplate\modules\customer\models\search\base\ClientUser as ClientUserSearchModel;

/**
* ClientUser represents the model behind the search form about `taktwerk\yiiboilerplate\modules\customer\models\ClientUser`.
*/
class ClientUser extends ClientUserSearchModel{

}