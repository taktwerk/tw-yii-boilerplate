<?php

namespace taktwerk\yiiboilerplate\modules\customer\models\search;

use taktwerk\yiiboilerplate\modules\customer\models\search\base\Client as ClientSearchModel;

/**
* Client represents the model behind the search form about `taktwerk\yiiboilerplate\modules\customer\models\Client`.
*/
class Client extends ClientSearchModel{

}