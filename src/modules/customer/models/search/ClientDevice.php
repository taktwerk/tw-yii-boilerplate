<?php

namespace taktwerk\yiiboilerplate\modules\customer\models\search;

use taktwerk\yiiboilerplate\modules\customer\models\search\base\ClientDevice as ClientDeviceSearchModel;

/**
* ClientDevice represents the model behind the search form about `taktwerk\yiiboilerplate\modules\customer\models\ClientDevice`.
*/
class ClientDevice extends ClientDeviceSearchModel{

}