<?php
//Generation Date: 06-Nov-2020 06:35:31am
namespace taktwerk\yiiboilerplate\modules\customer\models\base;

use Yii;
use \taktwerk\yiiboilerplate\models\TwActiveRecord;
use yii\db\Query;
use taktwerk\yiiboilerplate\traits\UploadTrait;

/**
 * This is the base-model class for table "client".
 * - - - - - - - - -
 * Generated by the modified Giiant CRUD Generator by taktwerk.com
 *
 * @property integer $id
 * @property string $name
 * @property string $identifier
 * @property string $website
 * @property integer $default_autologin_id
 * @property string $info
 * @property string $logo_file
 * @property string $logo_file_filemeta
 * @property integer $deleted_by
 * @property string $deleted_at
 * @property integer $is_system_entry
 * @property string $icon_file
 * @property string $icon_file_filemeta
 * @property string $skin
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property string $toString
 * @property string $entryDetails
 *
 * @property \taktwerk\yiiboilerplate\modules\customer\models\ClientUser $defaultAutologin
 * @property \taktwerk\yiiboilerplate\modules\customer\models\ClientDevice[] $clientDevices
 * @property \taktwerk\yiiboilerplate\modules\customer\models\ClientModelType[] $clientModelTypes
 * @property \taktwerk\yiiboilerplate\modules\customer\models\ClientUser[] $clientUsers
 * @property \taktwerk\yiiboilerplate\modules\feedback\models\Feedback[] $feedbacks
 * @property \taktwerk\yiiboilerplate\modules\setting\models\GeneralSetting $generalSetting
 * @property \taktwerk\yiiboilerplate\modules\user\models\Group[] $groups
 * @property \taktwerk\yiiboilerplate\modules\guide\models\Guide[] $guides
 * @property \taktwerk\yiiboilerplate\modules\guide\models\GuideAsset[] $guideAssets
 * @property \taktwerk\yiiboilerplate\modules\guide\models\GuideCategory[] $guideCategories
 * @property \taktwerk\yiiboilerplate\modules\guide\models\GuideSetting $guideSetting
 * @property \taktwerk\yiiboilerplate\modules\guide\models\GuideTemplate[] $guideTemplates
 * @property \taktwerk\yiiboilerplate\modules\guide\models\GuideViewHistory[] $guideViewHistories
 * @property \taktwerk\yiiboilerplate\modules\protocol\models\Protocol[] $protocols
 * @property \taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate[] $protocolTemplates
 * @property \taktwerk\yiiboilerplate\modules\workflow\models\Workflow[] $workflows
 */
class Client extends TwActiveRecord implements \taktwerk\yiiboilerplate\modules\backend\models\RelationModelInterface{

    /**
     * We can upload images and files to this model, so we need our helper trait.
     */
    use UploadTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [
                ['default_autologin_id', 'deleted_by', 'is_system_entry'],
                'integer'
            ],
            [
                ['info', 'logo_file_filemeta', 'icon_file_filemeta'],
                'string'
            ],
            [
                ['deleted_at'],
                'safe'
            ],
            [
                ['name', 'website', 'logo_file', 'icon_file'],
                'string',
                'max' => 255
            ],
            [
                ['identifier'],
                'string',
                'max' => 16
            ],
            [
                ['skin'],
                'string',
                'max' => 150
            ],
            [
                ['default_autologin_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => \taktwerk\yiiboilerplate\modules\customer\models\ClientUser::class,
                'targetAttribute' => ['default_autologin_id' => 'id']
            ]
        ];
        
        return  array_merge($rules, $this->buildRequiredRulesFromFormDependentShowFields());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('twbp', 'ID'),
            'name' => Yii::t('twbp', 'Name'),
            'identifier' => Yii::t('twbp', 'Identifier'),
            'website' => Yii::t('twbp', 'Website'),
            'default_autologin_id' => Yii::t('twbp', 'Default Autologin'),
            'info' => Yii::t('twbp', 'Info'),
            'logo_file' => Yii::t('twbp', 'Logo File'),
            'logo_file_filemeta' => Yii::t('twbp', 'Logo File Filemeta'),
            'created_by' => \Yii::t('twbp', 'Created By'),
            'created_at' => \Yii::t('twbp', 'Created At'),
            'updated_by' => \Yii::t('twbp', 'Updated By'),
            'updated_at' => \Yii::t('twbp', 'Updated At'),
            'deleted_by' => \Yii::t('twbp', 'Deleted By'),
            'deleted_at' => \Yii::t('twbp', 'Deleted At'),
            'is_system_entry' => Yii::t('twbp', 'Is System Entry'),
            'icon_file' => Yii::t('twbp', 'Icon File'),
            'icon_file_filemeta' => Yii::t('twbp', 'Icon File Filemeta'),
            'skin' => Yii::t('twbp', 'Skin'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributePlaceholders()
    {
        return [
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
        ];
    }

    /**
     * Auto generated method, that returns a human-readable name as string
     * for this model. This string can be called in foreign dropdown-fields or
     * foreign index-views as a representative value for the current instance.
     *
     * @author: taktwerk
     * This method is auto generated with a modified Model-Generator by taktwerk.com
     * @return String
     */
    public function toString()
    {
        return $this->name; // this attribute can be modified
    }


    /**
     * Getter for toString() function
     * @return String
     */
    public function getToString()
    {
        return $this->toString();
    }

    /**
     * Description for the table/model to show on grid an form view
     * @return String
     */
    public static function tableHint()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return [
            'defaultAutologin',
            'clientDevices',
            'clientModelTypes',
            'clientUsers',
            'feedbacks',
            'generalSetting',
            'groups',
            'guides',
            'guideAssets',
            'guideCategories',
            'guideSetting',
            'guideTemplates',
            'guideViewHistories',
            'protocols',
            'protocolTemplates',
            'workflows',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultAutologin()
    {
        return $this->hasOne(
            \taktwerk\yiiboilerplate\modules\customer\models\ClientUser::class,
            ['id' => 'default_autologin_id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function defaultAutologinList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\customer\models\ClientUser::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientDevices()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\customer\models\ClientDevice::class,
            ['client_id' => 'id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function clientDevicesList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\customer\models\ClientDevice::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientModelTypes()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\customer\models\ClientModelType::class,
            ['client_id' => 'id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function clientModelTypesList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\customer\models\ClientModelType::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientUsers()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\customer\models\ClientUser::class,
            ['client_id' => 'id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function clientUsersList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\customer\models\ClientUser::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbacks()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\feedback\models\Feedback::class,
            ['client_id' => 'id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function feedbacksList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\feedback\models\Feedback::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeneralSetting()
    {
        return $this->hasOne(
            \taktwerk\yiiboilerplate\modules\setting\models\GeneralSetting::class,
            ['client_id' => 'id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function generalSettingList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\setting\models\GeneralSetting::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\user\models\Group::class,
            ['client_id' => 'id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function groupsList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\user\models\Group::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuides()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\guide\models\Guide::class,
            ['client_id' => 'id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function guidesList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\guide\models\Guide::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuideAssets()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\guide\models\GuideAsset::class,
            ['client_id' => 'id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function guideAssetsList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\guide\models\GuideAsset::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuideCategories()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\guide\models\GuideCategory::class,
            ['client_id' => 'id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function guideCategoriesList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\guide\models\GuideCategory::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuideSetting()
    {
        return $this->hasOne(
            \taktwerk\yiiboilerplate\modules\guide\models\GuideSetting::class,
            ['client_id' => 'id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function guideSettingList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\guide\models\GuideSetting::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuideTemplates()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\guide\models\GuideTemplate::class,
            ['client_id' => 'id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function guideTemplatesList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\guide\models\GuideTemplate::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuideViewHistories()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\guide\models\GuideViewHistory::class,
            ['client_id' => 'id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function guideViewHistoriesList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\guide\models\GuideViewHistory::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getProtocols()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\protocol\models\Protocol::class,
            ['client_id' => 'id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function protocolsList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\protocol\models\Protocol::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getProtocolTemplates()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::class,
            ['client_id' => 'id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function protocolTemplatesList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflows()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\workflow\models\Workflow::class,
            ['client_id' => 'id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function workflowsList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\workflow\models\Workflow::filter($q);
    }
    

    /**
     * Return details for dropdown field
     * @return string
     */
    public function getEntryDetails()
    {
        return $this->toString;
    }

    /**
     * @param $string
     */
    public static function fetchCustomImportLookup($string)
    {
            return self::find()->andWhere([self::tableName() . '.name' => $string]);
    }

    /**
     * Load data into object
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;

        // Don't update these fields as they are handled separately.
        unset($data[$scope]['logo_file']);
        unset($data[$scope]['icon_file']);

        return parent::load($data,$formName);
    }

    /**
     * User for filtering results for Select2 element
     * @param null $q
     * @return array
     */
    public static function filter($q = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, name AS text')
            ->from(self::tableName())
            ->andWhere([self::tableName() . '.deleted_at' => null])
            ->andWhere(['like', 'name', $q])
            ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }

        /**
     * Get related CRUD controller's class
     * @return \taktwerk\yiiboilerplate\modules\customer\controllers\ClientController|\yii\web\Controller
     */
    public function getControllerInstance()
    {
        if(class_exists('\taktwerk\yiiboilerplate\modules\customer\controllers\ClientController')){
            return new \taktwerk\yiiboilerplate\modules\customer\controllers\ClientController("client", \taktwerk\yiiboilerplate\modules\customer\Module::getInstance());
        }
    }
}