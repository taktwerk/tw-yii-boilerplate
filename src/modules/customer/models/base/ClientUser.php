<?php
//Generation Date: 11-Sep-2020 02:34:25pm
namespace taktwerk\yiiboilerplate\modules\customer\models\base;

use Yii;
use \taktwerk\yiiboilerplate\models\TwActiveRecord;
use yii\db\Query;


/**
 * This is the base-model class for table "client_user".
 * - - - - - - - - -
 * Generated by the modified Giiant CRUD Generator by taktwerk.com
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $user_id
 * @property integer $is_default_autologin
 * @property integer $deleted_by
 * @property string $deleted_at
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property string $toString
 * @property string $entryDetails
 *
 * @property \taktwerk\yiiboilerplate\modules\customer\models\Client[] $clients
 * @property \taktwerk\yiiboilerplate\modules\customer\models\ClientDevice[] $clientDevices
 * @property \taktwerk\yiiboilerplate\modules\customer\models\Client $client
 * @property \taktwerk\yiiboilerplate\modules\customer\models\User $user
 */
class ClientUser extends TwActiveRecord implements \taktwerk\yiiboilerplate\modules\backend\models\RelationModelInterface{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [
                ['client_id', 'user_id', 'is_default_autologin', 'deleted_by'],
                'integer'
            ],
            [
                ['deleted_at'],
                'safe'
            ],
            [
                ['client_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => \taktwerk\yiiboilerplate\modules\customer\models\Client::class,
                'targetAttribute' => ['client_id' => 'id']
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => \taktwerk\yiiboilerplate\modules\customer\models\User::class,
                'targetAttribute' => ['user_id' => 'id']
            ]
        ];
        
        return  array_merge($rules, $this->buildRequiredRulesFromFormDependentShowFields());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('twbp', 'ID'),
            'client_id' => \Yii::t('twbp', 'Client'),
            'user_id' => Yii::t('twbp', 'User'),
            'is_default_autologin' => Yii::t('twbp', 'Is Default Autologin'),
            'created_by' => \Yii::t('twbp', 'Created By'),
            'created_at' => \Yii::t('twbp', 'Created At'),
            'updated_by' => \Yii::t('twbp', 'Updated By'),
            'updated_at' => \Yii::t('twbp', 'Updated At'),
            'deleted_by' => \Yii::t('twbp', 'Deleted By'),
            'deleted_at' => \Yii::t('twbp', 'Deleted At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributePlaceholders()
    {
        return [
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
        ];
    }

    /**
     * Auto generated method, that returns a human-readable name as string
     * for this model. This string can be called in foreign dropdown-fields or
     * foreign index-views as a representative value for the current instance.
     *
     * @author: taktwerk
     * This method is auto generated with a modified Model-Generator by taktwerk.com
     * @return String
     */
    public function toString()
    {
        return $this->is_default_autologin; // this attribute can be modified
    }


    /**
     * Getter for toString() function
     * @return String
     */
    public function getToString()
    {
        return $this->toString();
    }

    /**
     * Description for the table/model to show on grid an form view
     * @return String
     */
    public static function tableHint()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return [
            'clients',
            'clientDevices',
            'client',
            'user',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\customer\models\Client::class,
            ['default_autologin_id' => 'id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function clientsList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\customer\models\Client::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientDevices()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\customer\models\ClientDevice::class,
            ['client_user_id' => 'id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function clientDevicesList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\customer\models\ClientDevice::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(
            \taktwerk\yiiboilerplate\modules\customer\models\Client::class,
            ['id' => 'client_id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function clientList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\customer\models\Client::filter($q);
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(
            \taktwerk\yiiboilerplate\modules\customer\models\User::class,
            ['id' => 'user_id']
        );
    }

    /**
     * @param null $q
     * @return array
     */
    public static function userList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\customer\models\User::filter($q);
    }
    

    /**
     * Return details for dropdown field
     * @return string
     */
    public function getEntryDetails()
    {
        return $this->toString;
    }

    /**
     * @param $string
     */
    public static function fetchCustomImportLookup($string)
    {
            return self::find()->andWhere([self::tableName() . '.is_default_autologin' => $string]);
    }

    /**
     * User for filtering results for Select2 element
     * @param null $q
     * @return array
     */
    public static function filter($q = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, is_default_autologin AS text')
            ->from(self::tableName())
            ->andWhere([self::tableName() . '.deleted_at' => null])
            ->andWhere(['like', 'is_default_autologin', $q])
            ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }

        /**
     * Get related CRUD controller's class
     * @return \taktwerk\yiiboilerplate\modules\customer\controllers\ClientUserController|\yii\web\Controller
     */
    public function getControllerInstance()
    {
        if(class_exists('\taktwerk\yiiboilerplate\modules\customer\controllers\ClientUserController')){
            return new \taktwerk\yiiboilerplate\modules\customer\controllers\ClientUserController("client-user", \taktwerk\yiiboilerplate\modules\customer\Module::getInstance());
        }
    }
}