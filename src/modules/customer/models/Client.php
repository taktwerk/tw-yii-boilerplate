<?php

namespace taktwerk\yiiboilerplate\modules\customer\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\customer\models\base\Client as BaseClient;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\modules\user\models\User;
use taktwerk\yiiboilerplate\modules\sync\models\SyncProcess;
use taktwerk\yiiboilerplate\modules\sync\traits\SyncInfoTrait;
use taktwerk\yiiboilerplate\modules\user\models\UserDevice;
use taktwerk\yiiboilerplate\enums\AppConfigurationModeEnum;
use taktwerk\yiiboilerplate\modules\protocol\models\Protocol;
use taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate;
use taktwerk\yiiboilerplate\modules\workflow\models\Workflow;
use taktwerk\yiiboilerplate\modules\customer\models\ClientModelType;
use Endroid\QrCode\Builder\Builder;

/**
 * This is the model class for table "client".
 * @property \taktwerk\yiiboilerplate\modules\customer\models\ClientUser $defaultAutologin
 * @property \taktwerk\yiiboilerplate\modules\customer\models\ClientDevice[] $clientDevices
 * @property \taktwerk\yiiboilerplate\modules\customer\models\ClientUser[] $clientUsers
 */
class Client extends BaseClient
{
    use SyncInfoTrait;

    public function getSyncedDevices()
    {
        return $this->hasMany(UserDevice::class, ['user_id' => 'id'])
            ->via('users');
    }

    public function getLastSyncedDevices()
    {
            return $this->hasMany(UserDevice::class, ['user_id' => 'id'])
                    ->via('users');
    }

    public function getSyncProcesses()
    {
            return $this->hasMany(SyncProcess::class, ['device_id' => 'id'])
                    ->via('syncedDevices');
    }
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return ArrayHelper::merge(self::extraFields(),[
            'defaultAutologin',
            'clientDevices',
            'clientUsers',
            'guides',
            'guideAssets',
            'guideCategories',
            'guideTemplates',
            'protocols',
            'protocolTemplates',
            'workflows'
        ]);
    }


    public function getUsers()
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])
            ->via('clientUsers');
    }
/**
     * @return \yii\db\ActiveQuery
     */
    /* public function getGuides()
    {
        return $this->hasMany(
            \app\modules\guide\models\Guide::class,
            ['client_id' => 'id']
        );
    } */
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultAutologin()
    {
        return $this->hasOne(
            ClientUser::class,
            ['id' => 'default_autologin_id']
        );
    }
    
    /**
     * @param null $q
     * @return array
     */
    public static function defaultAutologinList($q = null)
    {
        return ClientUser::filter($q);
    }
 
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientDevices()
    {
        return $this->hasMany(
            ClientDevice::class,
            ['client_id' => 'id']
        );
    }
  
    /**
     * @param null $q
     * @return array
     */
    public static function clientDevicesList($q = null)
    {
        return ClientDevice::filter($q);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientUsers()
    {
        return $this->hasMany(
            ClientUser::class,
            ['client_id' => 'id']
        );
    }
 
    /**
     * @param null $q
     * @return array
     */
    public static function clientUsersList($q = null)
    {
        return ClientUser::filter($q);
    }
/**
     * @return \yii\db\ActiveQuery
     */
    public function getProtocols()
    {
        return $this->hasMany(
            Protocol::class,
            ['client_id' => 'id']
        );
    }
 /**
     * @param null $q
     * @return array
     */
    public static function protocolsList($q = null)
    {
        return Protocol::filter($q);
    }
    /**
     * Don't allow clients to see our other clients and allow to see system client.
     *
     * @param $removedDeleted bool To remove soft-deleted entries from results, false to include them in re
     * @inheritdoc
     */
    public static function findWithSystem($removedDeleted = true)
    {
        $model = parent::find($removedDeleted);

        // If not authority, only show tickets of our customers
        if (php_sapi_name() != "cli" && !Yii::$app->getUser()->can('Authority')) {
            $clients = [];
            $clientUsers = ClientUser::findAll(['user_id' => Yii::$app->getUser()->id]);
            $systemUsers = $model->where(["is_system_entry" => 1])->all();
            foreach ($clientUsers as $client) {
                $clients[] = $client->client_id;
            }
            foreach ($systemUsers as $client) {
                $clients[] = $client->id;
            }

            if (empty($clients)) {
                $model->andWhere([self::tableName().'.id' => null]);
            } else {
                $model->andWhere([
                    self::tableName().'.id' => $clients
                ]);
            }
        }

        return $model;
    }
 /**
     * @return \yii\db\ActiveQuery
     */
    public function getProtocolTemplates()
    {
        return $this->hasMany(
            ProtocolTemplate::class,
            ['client_id' => 'id']
        );
    }
    /**
     * Don't allow clients to see our other clients.
     *
     * @param $removedDeleted bool To remove soft-deleted entries from results, false to include them in re
     * @inheritdoc
     */
    public static function find($removedDeleted = true)
    {
        $model = parent::find($removedDeleted);

        // If not authority, only show tickets of our customers
        if (php_sapi_name() != "cli" && !Yii::$app->getUser()->can('Authority') && !Yii::$app->user->isGuest) {
            $clients = [];
            $clientUsers = ClientUser::findAll(['user_id' => Yii::$app->getUser()->id]);
            foreach ($clientUsers as $client) {
                $clients[] = $client->client_id;
            }

            if (empty($clients)) {
                $model->andWhere([self::tableName().'.id' => null]);
            } else {
                $model->andWhere([
                    self::tableName().'.id' => $clients
                ]);
            }
        }

        return $model;
    }

/**
     * @param null $q
     * @return array
     */
    public static function protocolTemplatesList($q = null)
    {
        return ProtocolTemplate::filter($q);
    }

    public static function findForLogin($removedDeleted = true)
    {
        return parent::find($removedDeleted);
    }

/**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflows()
    {
        return $this->hasMany(
            Workflow::class,
            ['client_id' => 'id']
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'default_autologin_id' => Yii::t('twbp', 'Default client user used for app login.'),
            'identifier' => Yii::t('twbp', 'App client identifier'),
        ];
    }


    /**
     * @return string
     */
    public function qrcode()
    {
        if (!Yii::$app->params['appSettings']) {
            return null;
        }
        // Create a basic QR code
        $config = Yii::$app->params['appSettings'];
        if (isset($config['mode']) && (int) $config['mode'] === AppConfigurationModeEnum::CONFIGURE_AND_DEVICE_LOGIN) {
            $config['clientIdentifier'] = $this->identifier;
        }
        $json = json_encode($config);
        return Builder::create()->data($json)->build()->getDataUri();
    }
 /**
     * @param null $q
     * @return array
     */
    public static function workflowsList($q = null)
    {
        return Workflow::filter($q);
    }

    /**
     * @param string $object_type
     * @param string $type
     * @return string|null |null
     */
    public function appTheme($object_type = "logo", $type = "main"){
        if($object_type=="logo"){
            if($type=="icon"){
                return $this->showFilePreview('icon_file',[],true);
            }else{
                return $this->showFilePreview('logo_file',[],true);
            }
        }else{
            return $this->skin;
        }
    }
    /**
     * Get the tags
     */
    public function getModelTypes()
    {
        $tags = [];
        foreach ($this->clientModelTypes as $tag) {
            $tags[] = $tag->type;
        }
        return $tags;
    }
    /**
     * After Save event
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveModelTypes();
    }
    /**
     * Manage category bindings after a guide has been saved
     */
    protected function saveModelTypes()
    {
        // Manage tags
        if (Yii::$app instanceof \yii\web\Application) {
            
            $request = Yii::$app->getRequest()->post('Client');
            
            if ($request) {
                $existingModelTypes = [];
                foreach ($this->clientModelTypes as $tag) {
                    $existingModelTypes[$tag->type] = $tag;
                }
                $submittedTags = ! empty($request['modelTypes']) ? $request['modelTypes'] : [];
                
                // Loop submitted tags to see if they are new or existing (and don't require deleting)
                foreach ($submittedTags as $tag) {
                    if (! empty($existingModelTypes[$tag])) {
                        unset($existingModelTypes[$tag]);
                    } else {
                        $modelType = ClientModelType::find()->where([
                            'type' => $tag
                        ])->one();
                        if (empty($modelType)) {
                            $modelType = new ClientModelType();
                            $modelType->client_id = $this->id;
                            $modelType->type = $tag;
                            $modelType->save();
                        }
                        
                        // Already have this binding?
                        $binding = ClientModelType::find()->where([
                            'client_id' => $this->id,
                            'type' => $tag
                        ])->one();
                        if (empty($binding)){
                            $tagModel = new ClientModelType();
                            $tagModel->client_id = $this->id;
                            $tagModel->type = $tag;
                            $tagModel->save();
                        }
                    }
                }
                
                // Remove old tags
                foreach ($existingModelTypes as $tagName => $tag) {
                    // We don't want to keep soft-deletes of tags so force delete.
                    $tag->forceDelete();
                }
            }
        }
    }

}
