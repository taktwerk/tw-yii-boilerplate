<?php

namespace taktwerk\yiiboilerplate\modules\customer\models;

use \app\models\User as BaseUser;

/**
 * This is the model class for table "user".
 */
class User extends BaseUser
{
}