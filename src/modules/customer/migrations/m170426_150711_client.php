<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170426_150711_client extends TwMigration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%client}}',
            [
                'id' => Schema::TYPE_PK . "",
                'name' => Schema::TYPE_STRING . "(255)",
                'identifier' => Schema::TYPE_STRING . "(16)",
                'website' => Schema::TYPE_STRING . "(255)",
                'default_autologin_id' => $this->integer()->null(),
                'info' => Schema::TYPE_TEXT . "",
                'logo_file' => Schema::TYPE_STRING . "(255)",
            ],
            $tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropIndex('deleted_at_idx', '{{%client}}');
        $this->dropIndex('toggl_client_id_idx', '{{%client}}');
        $this->dropTable('{{%client}}');
    }
}