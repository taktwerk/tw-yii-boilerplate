<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200928_063550_customer_file_meta_columns extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%client}}', 'logo_file_filemeta', $this->text()->after('logo_file'));
        $this->addColumn('{{%client}}', 'icon_file_filemeta', $this->text()->after('icon_file'));
    }

    public function down()
    {
        $this->dropColumn('{{%client}}', 'logo_file_filemeta');
        $this->dropColumn('{{%client}}', 'icon_file_filemeta');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
