<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200911_184636_add_to_table_client_device_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_client_device_deleted_at_client_id', '{{%client_device}}', ['deleted_at','client_id']);
        $this->createIndex('idx_client_device_deleted_at_client_user_id', '{{%client_device}}', ['deleted_at','client_user_id']);
    }

    public function down()
    {
        echo "m200911_184636_add_to_table_client_device_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
