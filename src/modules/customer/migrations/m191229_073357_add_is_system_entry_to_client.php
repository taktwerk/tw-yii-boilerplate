<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191229_073357_add_is_system_entry_to_client extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%client}}', 'is_system_entry', $this->tinyInteger(4)->defaultValue(0));
    }

    public function down()
    {
        echo "m191229_073357_add_is_system_entry_to_client cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
