<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200401_061620_customer_module_tbl_namespace extends TwMigration
{
    public function up()
    {
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\customer"}';
        $this->addCommentOnTable("{{%client}}", $comment);
        $this->addCommentOnTable("{{%client_device}}", $comment);
        $this->addCommentOnTable("{{%client_user}}", $comment);
        
    }

    public function down()
    {
        echo "m200401_061620_customer_module_tbl_namespace cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
