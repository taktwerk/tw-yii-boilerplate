<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m180426_150811_client_user extends TwMigration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%client_user}}',
            [
                'id' => Schema::TYPE_PK . "",
                'client_id' => Schema::TYPE_INTEGER . "(11)",
                'user_id' => Schema::TYPE_INTEGER . "(11)",
                'is_default_autologin' => $this->boolean()->defaultValue(false)
            ],
            $tableOptions
        );

        $this->createIndex('user_id_idx', '{{%client_user}}', 'user_id', 0);
        $this->createIndex('client_id_idx', '{{%client_user}}', 'client_id', 0);
    }

    public function safeDown()
    {
        $this->dropIndex('deleted_at_idx', '{{%client_user}}');
        $this->dropIndex('user_id_idx', '{{%client_user}}');
        $this->dropIndex('moco_customer_id_idx', '{{%client_user}}');
        $this->dropTable('{{%client_user}}');
    }
}