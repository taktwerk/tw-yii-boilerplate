<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%client_model_type}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%client}}`
 */
class m200821_133838_create_client_model_type_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%client_model_type}}', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer(11),
            'type' => $this->string(255)->notNull(),
        ]);
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\customer"}';
        
        $this->addCommentOnTable('{{%client_model_type}}', $comment);
        // creates index for column `client_id`
        $this->createIndex(
            '{{%idx-client_model_type-client_id}}',
            '{{%client_model_type}}',
            'client_id'
        );

        // add foreign key for table `{{%client}}`
        $this->addForeignKey(
            '{{%fk-client_model_type-client_id}}',
            '{{%client_model_type}}',
            'client_id',
            '{{%client}}',
            'id',
            'CASCADE'
        );
        $this->createIndex('idx_client_model_type_deleted_at_client_id', '{{%client_model_type}}', ['deleted_at','client_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    $this->dropIndex('idx_client_model_type_deleted_at_client_id', '{{%client_model_type}}');
            // drops foreign key for table `{{%client}}`
        $this->dropForeignKey(
            '{{%fk-client_model_type-client_id}}',
            '{{%client_model_type}}'
        );

        // drops index for column `client_id`
        $this->dropIndex(
            '{{%idx-client_model_type-client_id}}',
            '{{%client_model_type}}'
        );

        $this->dropTable('{{%client_model_type}}');
    }
}
