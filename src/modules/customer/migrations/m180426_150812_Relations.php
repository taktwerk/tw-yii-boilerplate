<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m180426_150812_Relations extends TwMigration
{
    public function safeUp()
    {
        $this->addForeignKey('fk_client_user_client_id', '{{%client_user}}', 'client_id', '{{%client}}', 'id');
        $this->addForeignKey('fk_client_user_user_id', '{{%client_user}}', 'user_id', '{{%user}}', 'id');

        $this->addForeignKey('client_fk_client_user_id', '{{%client}}', 'default_autologin_id', '{{%client_user}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_client_user_client_id', '{{%client_user}}');
        $this->dropForeignKey('fk_client_user_user_id', '{{%client_user}}');
        $this->dropForeignKey('client_fk_client_user_id', '{{%client}}');
    }
}
