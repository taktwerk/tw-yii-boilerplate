<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200824_055852_client_tbl_index extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_client_deleted_at_default_autologin_id', '{{%client}}', ['deleted_at','default_autologin_id']);
        $this->alterColumn('{{%client}}','is_system_entry', $this->tinyInteger(1)->defaultValue(null));
    }

    public function down()
    {
        echo "m200824_055852_client_tbl_index cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
