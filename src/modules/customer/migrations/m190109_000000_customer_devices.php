<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m190109_000000_customer_devices extends TwMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{%client_device}}',
            [
                'id' => $this->primaryKey(),
                'client_id' => $this->integer(),
                'client_user_id' => $this->integer()->null(),
                'name' => $this->string(45)->null(),
                'number' => $this->string(255)->null(),
                'last_used_at' => $this->dateTime(),
                'is_blocked' => $this->boolean(),
            ]
        );

        $this->addForeignKey('fk_client_device_client_id', '{{%client_device}}', 'client_id', '{{%client}}', 'id');
        $this->addForeignKey('fk_client_device_client_user_id', '{{%client_device}}', 'client_user_id', '{{%client_user}}', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('{{%client_device}}');
    }
}
