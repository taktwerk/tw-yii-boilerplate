<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%client_model_type}}`.
 */
class m201111_121846_add_icon_file_column_to_client_model_type_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $comment = '{"allowedExtensions":["image"]}';
        $this->addColumn('{{%client_model_type}}', 'icon_file', $this->string(255)->null()->after('type')->comment($comment));
        $this->addColumn('{{%client_model_type}}', 'icon_file_filemeta', $this->text()->after('icon_file'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%client_model_type}}', 'icon_file');
    }
}
