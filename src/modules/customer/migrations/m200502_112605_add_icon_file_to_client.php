<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200502_112605_add_icon_file_to_client extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%client}}', 'icon_file', $this->string(255)->defaultValue(null));
    }

    public function down()
    {
        echo "m200502_112605_add_icon_file_to_client cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
