<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m190110_000000_webview_login_acl extends TwMigration
{
    public function safeUp()
    {
        $this->createPermission('app_webview-login_index', 'Webview Login', ['Public']);
        $this->createPermission('app_webview-login_confirm', 'Webview Confirm', ['Public']);
    }

    public function safeDown()
    {
        $this->removePermission('app_webview-login_index', ['Public']);
        $this->removePermission('app_webview-login_confirm', ['Public']);
    }
}
