<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m201112_074718_client_model_type_icon_file_update extends TwMigration
{
    public function up()
    {

$comment = '{"allowedExtensions":["svg","image"]}';
$this->alterColumn('{{%client_model_type}}', 'icon_file', $this->string(255)->null()->after('type')->comment($comment));
    }

    public function down()
    {
        echo "m201112_074718_client_model_type_icon_file_update cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
