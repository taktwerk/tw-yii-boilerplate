<?php
//Generation Date: 28-Sep-2020 08:46:05am
namespace taktwerk\yiiboilerplate\modules\customer\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * ClientController implements the CRUD actions for Client model.
 */
class ClientController extends TwCrudController
{
}
