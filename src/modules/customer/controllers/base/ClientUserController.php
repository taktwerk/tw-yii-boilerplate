<?php
//Generation Date: 08-Oct-2020 08:07:11am
namespace taktwerk\yiiboilerplate\modules\customer\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * ClientUserController implements the CRUD actions for ClientUser model.
 */
class ClientUserController extends TwCrudController
{
}
