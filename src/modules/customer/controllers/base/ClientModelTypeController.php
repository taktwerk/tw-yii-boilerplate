<?php
//Generation Date: 03-Sep-2020 07:03:40am
namespace taktwerk\yiiboilerplate\modules\customer\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * ClientModelTypeController implements the CRUD actions for ClientModelType model.
 */
class ClientModelTypeController extends TwCrudController
{
}
