<?php

namespace taktwerk\yiiboilerplate\modules\customer\controllers;

use Endroid\QrCode\QrCode;
use taktwerk\yiiboilerplate\enums\AppConfigurationModeEnum;
use taktwerk\yiiboilerplate\widget\Select2;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use taktwerk\yiiboilerplate\modules\workflow\models\Workflow;
use taktwerk\yiiboilerplate\modules\protocol\models\Protocol;
use taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate;
use taktwerk\yiiboilerplate\modules\guide\models\Guide;
use taktwerk\yiiboilerplate\modules\guide\models\GuideAsset;
use taktwerk\yiiboilerplate\modules\guide\models\GuideCategory;
use taktwerk\yiiboilerplate\modules\guide\models\GuideTemplate;
use Endroid\QrCode\Writer\PngWriter;
use Endroid\QrCode\Builder\Builder;

/**
 * This is the class for controller "ClientController".
 */
class ClientController extends \taktwerk\yiiboilerplate\modules\customer\controllers\base\ClientController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\customer\models\Client';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\customer\models\search\Client';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }

    public function init()
    {
        $this->crudColumnsOverwrite = [
            'view' => [
                'after#skin' => [
                    'attribute' => 'qrcode',
                    'format'=>'raw',
                    'value' => function ($model) {
                        $content = '';
                        Yii::$app->params['appSettings'] = [
                            'hello'=>'ehjat'
                        ];
                        if (!Yii::$app->params['appSettings']) {
                            return null;
                        }
                        $appSettings = Yii::$app->params['appSettings'];
                        $config = [
                            'taktwerk' => isset($appSettings['taktwerk']) ?
                                $appSettings['taktwerk'] :
                                getenv('TAKTWERK_TYPE'),
                            'host' => isset($appSettings['host']) ?
                                $appSettings['host'] :
                                Url::base(true),
                            'client' => $model->identifier,
                            'mode' => AppConfigurationModeEnum::CONFIGURE_AND_LOGIN_BY_CLIENT,
                        ];
                        $json = json_encode($config);
                        $content .= Html::img(Builder::create()->data($json)->build()->getDataUri());

                        if (Yii::$app->getUser()->can('Authority')) {
                            unset($config['mode']);
                            $content .= '<div>' . json_encode($config) . '</div>';
                        }

                        return $content;
                    },
                ]
            ],
        ];

        return parent::init();
    }

    /**
     * @param $model
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function tabBlocks($model)
    {?>
        <?php \Yii::$app->view->beginBlock('Guides'); ?>
        <?php if ($useModal !== true) : ?>
    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
            <?php $columnsGuides = [[
                'class' => 'taktwerk\yiiboilerplate\grid\ActionColumn',
                'template' => (Yii::$app->getUser()->can('customer_guide_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_guide_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_guide_delete') ? '{delete} ' : ''),
                'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                    $params[0] = 'guide' . '/' . $action;
                    $params['Guide'] = [
                        'client_id' => $model->id
                    ];
                    $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                    return $params;
                },
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>'.Yii::t('twbp', 'Delete'),
                            $url,
                            [
                                'title' => Yii::t('twbp', 'Delete'),
                                'data-confirm' => Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                ],
                'controller' => 'guide'
            ],
                [
                    'attribute' => 'short_name',
                    'format' => 'html',

                    'value' => function ($model) {
                        if($model->short_name != strip_tags($model->short_name)){
                            return \yii\helpers\StringHelper::truncate($model->short_name,500,'...',null,true);
                        }else{
                            return nl2br(\yii\helpers\StringHelper::truncate($model->short_name,500));
                        }
                    },
                ],
                [
                    'attribute' => 'title',
                    'format' => 'html',

                    'value' => function ($model) {
                        if($model->title != strip_tags($model->title)){
                            return \yii\helpers\StringHelper::truncate($model->title,500,'...',null,true);
                        }else{
                            return nl2br(\yii\helpers\StringHelper::truncate($model->title,500));
                        }
                    },
                ],
                [
                    'attribute' => 'description',
                    'format' => 'html',

                    'value' => function ($model) {
                        if($model->description != strip_tags($model->description)){
                            return \yii\helpers\StringHelper::truncate($model->description,500,'...',null,true);
                        }else{
                            return nl2br(\yii\helpers\StringHelper::truncate($model->description,500));
                        }
                    },
                ],
                [
                    'attribute' => 'preview_file',
                    'format' => 'html',
                    'content' => function ($model) {
                        return Html::a($model->showFilePreview('preview_file'),['update', $model->primaryKey()[0] => $model->primaryKey]);
                    },
                ],
                [
                    'attribute' => 'revision_term',
                    'format' => 'html',

                    'value' => function ($model) {
                        if($model->revision_term != strip_tags($model->revision_term)){
                            return \yii\helpers\StringHelper::truncate($model->revision_term,500,'...',null,true);
                        }else{
                            return nl2br(\yii\helpers\StringHelper::truncate($model->revision_term,500));
                        }
                    },
                ],
                [
                    'attribute' => 'revision_counter',
                    'format' => 'html',

                    'value' => function ($model) {
                        if($model->revision_counter != strip_tags($model->revision_counter)){
                            return \yii\helpers\StringHelper::truncate($model->revision_counter,500,'...',null,true);
                        }else{
                            return nl2br(\yii\helpers\StringHelper::truncate($model->revision_counter,500));
                        }
                    },
                ],
                [
                    'attribute' => 'duration',
                    'format' => 'html',

                    'value' => function ($model) {
                        if($model->duration != strip_tags($model->duration)){
                            return \yii\helpers\StringHelper::truncate($model->duration,500,'...',null,true);
                        }else{
                            return nl2br(\yii\helpers\StringHelper::truncate($model->duration,500));
                        }
                    },
                ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => yii\grid\DataColumn::class,
                    'attribute' => 'template_id',
                    'value' => function ($model) {
                        if ($rel = $model->getTemplate()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'guide-template/view',
                                    'id' => $rel->id,
                                ],
                                [
                                    'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => yii\grid\DataColumn::class,
                    'attribute' => 'protocol_template_id',
                    'value' => function ($model) {
                        if ($rel = $model->getProtocolTemplate()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'protocol-template/view',
                                    'id' => $rel->id,
                                ],
                                [
                                    'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
            ];
            $relatedModelObject = Yii::createObject(Guide::class);
            $relationController = $relatedModelObject->getControllerInstance();
            $columnsGuides = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGuides, 'tab'):$columnsGuides;
            echo \taktwerk\yiiboilerplate\grid\GridView::widget([
                'layout' => '{summary}{pager}<br/>{items}{pager}',
                'options'=>[
                    'class'=>'grid-outer-div'
                ],
                'summary'=>'<div class="sowing-outer">
<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>'.
                    (($useModal !== true)?Html::a(
                        '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                        ' ' .
                        Yii::t('twbp', 'Guide'),
                        [
                            'guide/create',
                            'Guide' => [
                                'client_id' => $model->id
                            ],
                            'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                        ],
                        ['class' => 'btn btn-success']
                    ):'').'</div>',
                'dataProvider' => new \yii\data\ActiveDataProvider(
                    [
                        'query' => $model->getGuides(),
                        'pagination' => [
                            'pageSize' => 20,
                            'pageParam' => 'page-guides',
                        ]
                    ]
                ),
                'pager' => [
                    'class'          => yii\widgets\LinkPager::class,
                    'firstPageLabel' => \Yii::t('twbp', 'First'),
                    'lastPageLabel'  => \Yii::t('twbp', 'Last')
                ],
                'pjax' => true,
                'striped' => true,
                'hover' => true,

                'columns' => $columnsGuides
            ])?>
        </div>
        <?php Yii::$app->view->endBlock() ?>


        <?php \Yii::$app->view->beginBlock('Guide Assets'); ?>
        <?php if ($useModal !== true) : ?>
    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
            <?php $columnsGuideAssets = [[
                'class' => 'taktwerk\yiiboilerplate\grid\ActionColumn',
                'template' => (Yii::$app->getUser()->can('customer_guide-asset_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_guide-asset_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_guide-asset_delete') ? '{delete} ' : ''),
                'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                    $params[0] = 'guide-asset' . '/' . $action;
                    $params['GuideAsset'] = [
                        'client_id' => $model->id
                    ];
                    $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                    return $params;
                },
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>'.Yii::t('twbp', 'Delete'),
                            $url,
                            [
                                'title' => Yii::t('twbp', 'Delete'),
                                'data-confirm' => Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                ],
                'controller' => 'guide-asset'
            ],
                [
                    'attribute' => 'name',
                    'format' => 'html',

                    'value' => function ($model) {
                        if($model->name != strip_tags($model->name)){
                            return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
                        }else{
                            return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
                        }
                    },
                ],
                [
                    'attribute' => 'asset_file',
                    'format' => 'html',
                    'content' => function ($model) {
                        return Html::a($model->showFilePreview('asset_file'),['update', $model->primaryKey()[0] => $model->primaryKey]);
                    },
                ],
                [
                    'attribute' => 'asset_html',
                    'format' => 'html',

                    'value' => function ($model) {
                        if($model->asset_html != strip_tags($model->asset_html)){
                            return \yii\helpers\StringHelper::truncate($model->asset_html,500,'...',null,true);
                        }else{
                            return nl2br(\yii\helpers\StringHelper::truncate($model->asset_html,500));
                        }
                    },
                ],
//                [
//                    'attribute' => 'pdf_image',
//                    'format' => 'html',
//
//                    'value' => function ($model) {
//                        if($model->pdf_image != strip_tags($model->pdf_image)){
//                            return \yii\helpers\StringHelper::truncate($model->pdf_image,500,'...',null,true);
//                        }else{
//                            return nl2br(\yii\helpers\StringHelper::truncate($model->pdf_image,500));
//                        }
//                    },
//                ],
            ];
            $relatedModelObject = Yii::createObject(GuideAsset::class);
            $relationController = $relatedModelObject->getControllerInstance();
            $columnsGuideAssets = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGuideAssets, 'tab'):$columnsGuideAssets;
            echo \taktwerk\yiiboilerplate\grid\GridView::widget([
                'layout' => '{summary}{pager}<br/>{items}{pager}',
                'options'=>[
                    'class'=>'grid-outer-div'
                ],
                'summary'=>'<div class="sowing-outer">
<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>'.
                    (($useModal !== true)?Html::a(
                        '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                        ' ' .
                        Yii::t('twbp', 'Guide Asset'),
                        [
                            'guide-asset/create',
                            'GuideAsset' => [
                                'client_id' => $model->id
                            ],
                            'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                        ],
                        ['class' => 'btn btn-success']
                    ):'').'</div>',
                'dataProvider' => new \yii\data\ActiveDataProvider(
                    [
                        'query' => $model->getGuideAssets(),
                        'pagination' => [
                            'pageSize' => 20,
                            'pageParam' => 'page-guideassets',
                        ]
                    ]
                ),
                'pager' => [
                    'class'          => yii\widgets\LinkPager::class,
                    'firstPageLabel' => \Yii::t('twbp', 'First'),
                    'lastPageLabel'  => \Yii::t('twbp', 'Last')
                ],
                'pjax' => true,
                'striped' => true,
                'hover' => true,

                'columns' => $columnsGuideAssets
            ])?>
        </div>
        <?php \Yii::$app->view->endBlock() ?>


        <?php \Yii::$app->view->beginBlock('Guide Categories'); ?>
        <?php if ($useModal !== true) : ?>
    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
            <?php $columnsGuideCategories = [[
                'class' => 'taktwerk\yiiboilerplate\grid\ActionColumn',
                'template' => (Yii::$app->getUser()->can('customer_guide-category_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_guide-category_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_guide-category_delete') ? '{delete} ' : ''),
                'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                    $params[0] = 'guide-category' . '/' . $action;
                    $params['GuideCategory'] = [
                        'client_id' => $model->id
                    ];
                    $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                    return $params;
                },
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>'.Yii::t('twbp', 'Delete'),
                            $url,
                            [
                                'title' => Yii::t('twbp', 'Delete'),
                                'data-confirm' => Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                ],
                'controller' => 'guide-category'
            ],
                [
                    'attribute' => 'name',
                    'format' => 'html',

                    'value' => function ($model) {
                        if($model->name != strip_tags($model->name)){
                            return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
                        }else{
                            return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
                        }
                    },
                ],
            ];
            $relatedModelObject = Yii::createObject(GuideCategory::class);
            $relationController = $relatedModelObject->getControllerInstance();
            $columnsGuideCategories = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGuideCategories, 'tab'):$columnsGuideCategories;
            echo \taktwerk\yiiboilerplate\grid\GridView::widget([
                'layout' => '{summary}{pager}<br/>{items}{pager}',
                'options'=>[
                    'class'=>'grid-outer-div'
                ],
                'summary'=>'<div class="sowing-outer">
<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>'.
                    (($useModal !== true)?Html::a(
                        '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                        ' ' .
                        Yii::t('twbp', 'Guide Category'),
                        [
                            'guide-category/create',
                            'GuideCategory' => [
                                'client_id' => $model->id
                            ],
                            'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                        ],
                        ['class' => 'btn btn-success']
                    ):'').'</div>',
                'dataProvider' => new \yii\data\ActiveDataProvider(
                    [
                        'query' => $model->getGuideCategories(),
                        'pagination' => [
                            'pageSize' => 20,
                            'pageParam' => 'page-guidecategories',
                        ]
                    ]
                ),
                'pager' => [
                    'class'          => yii\widgets\LinkPager::class,
                    'firstPageLabel' => \Yii::t('twbp', 'First'),
                    'lastPageLabel'  => \Yii::t('twbp', 'Last')
                ],
                'pjax' => true,
                'striped' => true,
                'hover' => true,

                'columns' => $columnsGuideCategories
            ])?>
        </div>
        <?php \Yii::$app->view->endBlock() ?>


        <?php \Yii::$app->view->beginBlock('Guide Templates'); ?>
        <?php if ($useModal !== true) : ?>
    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
            <?php $columnsGuideTemplates = [[
                'class' => 'taktwerk\yiiboilerplate\grid\ActionColumn',
                'template' => (Yii::$app->getUser()->can('customer_guide-template_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_guide-template_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_guide-template_delete') ? '{delete} ' : ''),
                'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                    $params[0] = 'guide-template' . '/' . $action;
                    $params['GuideTemplate'] = [
                        'client_id' => $model->id
                    ];
                    $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                    return $params;
                },
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>'.Yii::t('twbp', 'Delete'),
                            $url,
                            [
                                'title' => Yii::t('twbp', 'Delete'),
                                'data-confirm' => Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                ],
                'controller' => 'guide-template'
            ],
                [
                    'attribute' => 'name',
                    'format' => 'html',

                    'value' => function ($model) {
                        if($model->name != strip_tags($model->name)){
                            return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
                        }else{
                            return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
                        }
                    },
                ],
                [
                    'attribute' => 'script',
                    'format' => 'html',

                    'value' => function ($model) {
                        if($model->script != strip_tags($model->script)){
                            return \yii\helpers\StringHelper::truncate($model->script,500,'...',null,true);
                        }else{
                            return nl2br(\yii\helpers\StringHelper::truncate($model->script,500));
                        }
                    },
                ],
            ];
            $relatedModelObject = Yii::createObject(GuideTemplate::class);
            $relationController = $relatedModelObject->getControllerInstance();
            $columnsGuideTemplates = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGuideTemplates, 'tab'):$columnsGuideTemplates;
            echo \taktwerk\yiiboilerplate\grid\GridView::widget([
                'layout' => '{summary}{pager}<br/>{items}{pager}',
                'options'=>[
                    'class'=>'grid-outer-div'
                ],
                'summary'=>'<div class="sowing-outer">
<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>'.
                    (($useModal !== true)?Html::a(
                        '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                        ' ' .
                        Yii::t('twbp', 'Guide Template'),
                        [
                            'guide-template/create',
                            'GuideTemplate' => [
                                'client_id' => $model->id
                            ],
                            'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                        ],
                        ['class' => 'btn btn-success']
                    ):'').'</div>',
                'dataProvider' => new \yii\data\ActiveDataProvider(
                    [
                        'query' => $model->getGuideTemplates(),
                        'pagination' => [
                            'pageSize' => 20,
                            'pageParam' => 'page-guidetemplates',
                        ]
                    ]
                ),
                'pager' => [
                    'class'          => yii\widgets\LinkPager::class,
                    'firstPageLabel' => \Yii::t('twbp', 'First'),
                    'lastPageLabel'  => \Yii::t('twbp', 'Last')
                ],
                'pjax' => true,
                'striped' => true,
                'hover' => true,

                'columns' => $columnsGuideTemplates
            ])?>
        </div>
        <?php \Yii::$app->view->endBlock() ?>


        <?php \Yii::$app->view->beginBlock('Protocols'); ?>
        <?php if ($useModal !== true) : ?>
    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
            <?php $columnsProtocols = [[
                'class' => 'taktwerk\yiiboilerplate\grid\ActionColumn',
                'template' => (Yii::$app->getUser()->can('customer_protocol_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_protocol_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_protocol_delete') ? '{delete} ' : ''),
                'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                    $params[0] = 'protocol' . '/' . $action;
                    $params['Protocol'] = [
                        'client_id' => $model->id
                    ];
                    $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                    return $params;
                },
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>'.Yii::t('twbp', 'Delete'),
                            $url,
                            [
                                'title' => Yii::t('twbp', 'Delete'),
                                'data-confirm' => Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                ],
                'controller' => 'protocol'
            ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => yii\grid\DataColumn::class,
                    'attribute' => 'protocol_template_id',
                    'value' => function ($model) {
                        if ($rel = $model->getProtocolTemplate()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'protocol-template/view',
                                    'id' => $rel->id,
                                ],
                                [
                                    'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => yii\grid\DataColumn::class,
                    'attribute' => 'workflow_step_id',
                    'value' => function ($model) {
                        if ($rel = $model->getWorkflowStep()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'workflow-step/view',
                                    'id' => $rel->id,
                                ],
                                [
                                    'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'name',
                    'format' => 'html',

                    'value' => function ($model) {
                        if($model->name != strip_tags($model->name)){
                            return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
                        }else{
                            return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
                        }
                    },
                ],
                [
                    'attribute' => 'protocol_form_table',
                    'format' => 'html',

                    'value' => function ($model) {
                        if($model->protocol_form_table != strip_tags($model->protocol_form_table)){
                            return \yii\helpers\StringHelper::truncate($model->protocol_form_table,500,'...',null,true);
                        }else{
                            return nl2br(\yii\helpers\StringHelper::truncate($model->protocol_form_table,500));
                        }
                    },
                ],
                [
                    'attribute' => 'protocol_form_number',
                    'format' => 'html',

                    'value' => function ($model) {
                        if($model->protocol_form_number != strip_tags($model->protocol_form_number)){
                            return \yii\helpers\StringHelper::truncate($model->protocol_form_number,500,'...',null,true);
                        }else{
                            return nl2br(\yii\helpers\StringHelper::truncate($model->protocol_form_number,500));
                        }
                    },
                ],
            ];
            $relatedModelObject = Yii::createObject(Protocol::class);
            $relationController = $relatedModelObject->getControllerInstance();
            $columnsProtocols = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsProtocols, 'tab'):$columnsProtocols;
            echo \taktwerk\yiiboilerplate\grid\GridView::widget([
                'layout' => '{summary}{pager}<br/>{items}{pager}',
                'options'=>[
                    'class'=>'grid-outer-div'
                ],
                'summary'=>'<div class="sowing-outer">
<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>'.
                    (($useModal !== true)?Html::a(
                        '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                        ' ' .
                        Yii::t('twbp', 'Protocol'),
                        [
                            'protocol/create',
                            'Protocol' => [
                                'client_id' => $model->id
                            ],
                            'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                        ],
                        ['class' => 'btn btn-success']
                    ):'').'</div>',
                'dataProvider' => new \yii\data\ActiveDataProvider(
                    [
                        'query' => $model->getProtocols(),
                        'pagination' => [
                            'pageSize' => 20,
                            'pageParam' => 'page-protocols',
                        ]
                    ]
                ),
                'pager' => [
                    'class'          => yii\widgets\LinkPager::class,
                    'firstPageLabel' => \Yii::t('twbp', 'First'),
                    'lastPageLabel'  => \Yii::t('twbp', 'Last')
                ],
                'pjax' => true,
                'striped' => true,
                'hover' => true,

                'columns' => $columnsProtocols
            ])?>
        </div>
        <?php \Yii::$app->view->endBlock() ?>


        <?php \Yii::$app->view->beginBlock('Protocol Templates'); ?>
        <?php if ($useModal !== true) : ?>
    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
            <?php $columnsProtocolTemplates = [[
                'class' => 'taktwerk\yiiboilerplate\grid\ActionColumn',
                'template' => (Yii::$app->getUser()->can('customer_protocol-template_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_protocol-template_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_protocol-template_delete') ? '{delete} ' : ''),
                'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                    $params[0] = 'protocol-template' . '/' . $action;
                    $params['ProtocolTemplate'] = [
                        'client_id' => $model->id
                    ];
                    $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                    return $params;
                },
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>'.Yii::t('twbp', 'Delete'),
                            $url,
                            [
                                'title' => Yii::t('twbp', 'Delete'),
                                'data-confirm' => Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                ],
                'controller' => 'protocol-template'
            ],
                [
                    'attribute' => 'name',
                    'format' => 'html',

                    'value' => function ($model) {
                        if($model->name != strip_tags($model->name)){
                            return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
                        }else{
                            return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
                        }
                    },
                ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => yii\grid\DataColumn::class,
                    'attribute' => 'workflow_id',
                    'value' => function ($model) {
                        if ($rel = $model->getWorkflow()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'workflow/view',
                                    'id' => $rel->id,
                                ],
                                [
                                    'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'protocol_form_table',
                    'format' => 'html',

                    'value' => function ($model) {
                        if($model->protocol_form_table != strip_tags($model->protocol_form_table)){
                            return \yii\helpers\StringHelper::truncate($model->protocol_form_table,500,'...',null,true);
                        }else{
                            return nl2br(\yii\helpers\StringHelper::truncate($model->protocol_form_table,500));
                        }
                    },
                ],
            ];
            $relatedModelObject = Yii::createObject(ProtocolTemplate::class);
            $relationController = $relatedModelObject->getControllerInstance();
            $columnsProtocolTemplates = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsProtocolTemplates, 'tab'):$columnsProtocolTemplates;
            echo \taktwerk\yiiboilerplate\grid\GridView::widget([
                'layout' => '{summary}{pager}<br/>{items}{pager}',
                'options'=>[
                    'class'=>'grid-outer-div'
                ],
                'summary'=>'<div class="sowing-outer">
<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>'.
                    (($useModal !== true)?Html::a(
                        '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                        ' ' .
                        Yii::t('twbp', 'Protocol Template'),
                        [
                            'protocol-template/create',
                            'ProtocolTemplate' => [
                                'client_id' => $model->id
                            ],
                            'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                        ],
                        ['class' => 'btn btn-success']
                    ):'').'</div>',
                'dataProvider' => new \yii\data\ActiveDataProvider(
                    [
                        'query' => $model->getProtocolTemplates(),
                        'pagination' => [
                            'pageSize' => 20,
                            'pageParam' => 'page-protocoltemplates',
                        ]
                    ]
                ),
                'pager' => [
                    'class'          => yii\widgets\LinkPager::class,
                    'firstPageLabel' => \Yii::t('twbp', 'First'),
                    'lastPageLabel'  => \Yii::t('twbp', 'Last')
                ],
                'pjax' => true,
                'striped' => true,
                'hover' => true,

                'columns' => $columnsProtocolTemplates
            ])?>
        </div>
        <?php \Yii::$app->view->endBlock() ?>


        <?php \Yii::$app->view->beginBlock('Workflows'); ?>
        <?php if ($useModal !== true) : ?>
    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
            <?php $columnsWorkflows = [[
                'class' => 'taktwerk\yiiboilerplate\grid\ActionColumn',
                'template' => (Yii::$app->getUser()->can('customer_workflow_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('customer_workflow_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('customer_workflow_delete') ? '{delete} ' : ''),
                'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                    $params[0] = 'workflow' . '/' . $action;
                    $params['Workflow'] = [
                        'client_id' => $model->id
                    ];
                    $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                    return $params;
                },
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>'.Yii::t('twbp', 'Delete'),
                            $url,
                            [
                                'title' => Yii::t('twbp', 'Delete'),
                                'data-confirm' => Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                ],
                'controller' => 'workflow'
            ],
                [
                    'attribute' => 'name',
                    'format' => 'html',

                    'value' => function ($model) {
                        if($model->name != strip_tags($model->name)){
                            return \yii\helpers\StringHelper::truncate($model->name,500,'...',null,true);
                        }else{
                            return nl2br(\yii\helpers\StringHelper::truncate($model->name,500));
                        }
                    },
                ],
            ];
            $relatedModelObject = Yii::createObject(Workflow::class);
            $relationController = $relatedModelObject->getControllerInstance();
            $columnsWorkflows = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsWorkflows, 'tab'):$columnsWorkflows;
            echo \taktwerk\yiiboilerplate\grid\GridView::widget([
                'layout' => '{summary}{pager}<br/>{items}{pager}',
                'options'=>[
                    'class'=>'grid-outer-div'
                ],
                'summary'=>'<div class="sowing-outer">
<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>'.
                    (($useModal !== true)?Html::a(
                        '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                        ' ' .
                        Yii::t('twbp', 'Workflow'),
                        [
                            'workflow/create',
                            'Workflow' => [
                                'client_id' => $model->id
                            ],
                            'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                        ],
                        ['class' => 'btn btn-success']
                    ):'').'</div>',
                'dataProvider' => new \yii\data\ActiveDataProvider(
                    [
                        'query' => $model->getWorkflows(),
                        'pagination' => [
                            'pageSize' => 20,
                            'pageParam' => 'page-workflows',
                        ]
                    ]
                ),
                'pager' => [
                    'class'          => yii\widgets\LinkPager::class,
                    'firstPageLabel' => \Yii::t('twbp', 'First'),
                    'lastPageLabel'  => \Yii::t('twbp', 'Last')
                ],
                'pjax' => true,
                'striped' => true,
                'hover' => true,

                'columns' => $columnsWorkflows
            ])?>
        </div>
        <?php \Yii::$app->view->endBlock() ?>
        <?php

        $this->tabBlocks = [

            [
                'content' => \Yii::$app->view->blocks['Guides'],
                'label' => '<small>' .
                    Yii::t('twbp', 'Guides') .
                    '&nbsp;<span class="badge badge-default">' .
                    $model->getGuides()->count() .
                    '</span></small>',
                'active' => false,
                'visible' => Yii::$app->user->can('x_customer_guide_see') && Yii::$app->controller->crudRelations('Guides'),
            ],
            [
                'content' => \Yii::$app->view->blocks['Guide Assets'],
                'label' => '<small>' .
                    Yii::t('twbp', 'Guide Assets') .
                    '&nbsp;<span class="badge badge-default">' .
                    $model->getGuideAssets()->count() .
                    '</span></small>',
                'active' => false,
                'visible' => Yii::$app->user->can('x_customer_guide-asset_see') && Yii::$app->controller->crudRelations('GuideAssets'),
            ],
            [
                'content' => \Yii::$app->view->blocks['Guide Categories'],
                'label' => '<small>' .
                    Yii::t('twbp', 'Guide Categories') .
                    '&nbsp;<span class="badge badge-default">' .
                    $model->getGuideCategories()->count() .
                    '</span></small>',
                'active' => false,
                'visible' => Yii::$app->user->can('x_customer_guide-category_see') && Yii::$app->controller->crudRelations('GuideCategories'),
            ],
            [
                'content' => \Yii::$app->view->blocks['Guide Templates'],
                'label' => '<small>' .
                    Yii::t('twbp', 'Guide Templates') .
                    '&nbsp;<span class="badge badge-default">' .
                    $model->getGuideTemplates()->count() .
                    '</span></small>',
                'active' => false,
                'visible' => Yii::$app->user->can('x_customer_guide-template_see') && Yii::$app->controller->crudRelations('GuideTemplates'),
            ],
            [
                'content' => \Yii::$app->view->blocks['Protocols'],
                'label' => '<small>' .
                    Yii::t('twbp', 'Protocols') .
                    '&nbsp;<span class="badge badge-default">' .
                    $model->getProtocols()->count() .
                    '</span></small>',
                'active' => false,
                'visible' => Yii::$app->user->can('x_customer_protocol_see') && Yii::$app->controller->crudRelations('Protocols'),
            ],
            [
                'content' => \Yii::$app->view->blocks['Protocol Templates'],
                'label' => '<small>' .
                    Yii::t('twbp', 'Protocol Templates') .
                    '&nbsp;<span class="badge badge-default">' .
                    $model->getProtocolTemplates()->count() .
                    '</span></small>',
                'active' => false,
                'visible' => Yii::$app->user->can('x_customer_protocol-template_see') && Yii::$app->controller->crudRelations('ProtocolTemplates'),
            ],
            [
                'content' => \Yii::$app->view->blocks['Workflows'],
                'label' => '<small>' .
                    Yii::t('twbp', 'Workflows') .
                    '&nbsp;<span class="badge badge-default">' .
                    $model->getWorkflows()->count() .
                    '</span></small>',
                'active' => false,
                'visible' => Yii::$app->user->can('x_customer_workflow_see') && Yii::$app->controller->crudRelations('Workflows'),
            ],
        ];
        return parent::tabBlocks($model); // TODO: Change the autogenerated stub
    }

    public function formFieldsOverwrite($model, $form)
    {
        $skin = [
            'skin-blue',
            'skin-blue-light',
            'skin-yellow',
            'skin-yellow-light',
            'skin-green',
            'skin-green-light',
            'skin-purple',
            'skin-purple-light',
            'skin-red',
            'skin-red-light',
            'skin-black',
            'skin-black-light',
        ];
        $skin = array_combine($skin, $skin);
        
        
        $this->formFieldsOverwrite = [
            'after#info' =>
            $form->field(
                $model,
                'modelTypes',
                )
            ->widget(
                Select2::class,
                [
                    'data' => array_combine($this->module->clientModelTypes,$this->module->clientModelTypes),
                    'options' => [
                        'placeholder' => Yii::t('twbp', 'Select a value...'),
                        'id' => 'tags' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                        'multiple' => true
                    ],
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'tags' => false,
                        'maximumInputLength' => 45
                    ]
                ]
                )
            ->hint($model->getAttributeHint('modelTypes')),
            'skin' =>
                $form->field(
                    $model,
                    'skin'
                )
                    ->widget(
                        Select2::class,
                        [
                            'data' => $skin,
                            'options' => [
                                'placeholder' => Yii::t('twbp', 'Select a value...'),
                                'id' => 'skin' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                'multiple' => false
                            ]
                        ]
                    )
                    ->hint($model->getAttributeHint('skin'))
        ];

        return parent::formFieldsOverwrite($model, $form); // TODO: Change the autogenerated stub
    }
}
