<?php


namespace taktwerk\yiiboilerplate\modules\user\controllers;

use Da\User\Event\ProfileEvent;
use Da\User\Event\UserEvent;
use Da\User\Validator\AjaxRequestModelValidator;
use taktwerk\yiiboilerplate\modules\user\models\Profile;
use taktwerk\yiiboilerplate\modules\userParameter\models\UserParameterForm;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * Class SettingsController
 * @package taktwerk\yiiboilerplate\modules\user\controllers
 */
class SettingsController extends \Da\User\Controller\SettingsController
{
    public $layout = "@taktwerk-backend-views/layouts/main";

    /**
     * Additional actions for controllers, uncomment to use them
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'parameters',
                        ],
                        'roles' => ['@']
                    ]
                ]
            ]
        ]);
    }

    /**
     * Shows profile settings form.
     *
     * @return string|\yii\web\Response
     */
    public function actionProfile()
    {
        /**
         * @var $model Profile
         */
        $model = Profile::find()->andWhere(['user_id' => \Yii::$app->user->identity->getId()])->one();
        if ($model == null) {
            $model = \Yii::createObject(Profile::class);
            $model->link('user', \Yii::$app->user->identity);
        }

        $event = $this->make(ProfileEvent::class, [$model]);

        $this->make(AjaxRequestModelValidator::class, [$model])->validate();

        $this->trigger(UserEvent::EVENT_BEFORE_PROFILE_UPDATE, $event);
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            $model->uploadFile(
                'picture',
                $_FILES['Profile']['tmp_name'],
                $_FILES['Profile']['name']
            );
            \Yii::$app->getSession()->setFlash('success', \Yii::t('twbp', 'Your profile has been updated'));
            $this->trigger(UserEvent::EVENT_AFTER_PROFILE_UPDATE, $event);
            return $this->refresh();
        }

        return $this->render('profile', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     */
    public function actionParameters()
    {
        $model = new UserParameterForm();
        if (\Yii::$app->request->isPost) {
            $model->load(\Yii::$app->request->post());
            $model->save();
        }
        return $this->render('paramaters', ['model' => $model]);
    }
}
