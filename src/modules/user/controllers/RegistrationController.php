<?php

namespace taktwerk\yiiboilerplate\modules\user\controllers;

/**
 * Class RegistrationController
 * @package taktwerk\yiiboilerplate\modules\user\controllers
 */
class RegistrationController extends \Da\User\Controller\RegistrationController
{
    public $layout = false;
}
