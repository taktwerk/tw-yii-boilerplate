<?php


namespace taktwerk\yiiboilerplate\modules\user\controllers;

use taktwerk\yiiboilerplate\controllers\TwCrudController;

class UserDeviceController extends TwCrudController
{
    public $model = 'taktwerk\yiiboilerplate\modules\user\models\UserDevice';
    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\user\models\search\UserDevice';
}
