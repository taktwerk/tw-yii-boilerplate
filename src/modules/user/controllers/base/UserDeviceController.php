<?php
//Generation Date: 11-Sep-2020 02:47:28pm
namespace taktwerk\yiiboilerplate\modules\user\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * UserDeviceController implements the CRUD actions for UserDevice model.
 */
class UserDeviceController extends TwCrudController
{
}
