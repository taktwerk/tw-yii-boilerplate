<?php
//Generation Date: 25-Jan-2021 10:26:46am
namespace taktwerk\yiiboilerplate\modules\user\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * GroupEmailController implements the CRUD actions for GroupEmail model.
 */
class GroupEmailController extends TwCrudController
{
}
