<?php
//Generation Date: 11-Sep-2020 02:47:27pm
namespace taktwerk\yiiboilerplate\modules\user\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * UserAuthCodeController implements the CRUD actions for UserAuthCode model.
 */
class UserAuthCodeController extends TwCrudController
{
}
