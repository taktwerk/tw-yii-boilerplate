<?php
//Generation Date: 11-Sep-2020 03:29:27pm
namespace taktwerk\yiiboilerplate\modules\user\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * UserTwDataController implements the CRUD actions for UserTwData model.
 */
class UserTwDataController extends TwCrudController
{
}
