<?php

namespace taktwerk\yiiboilerplate\modules\user\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "GroupController".
 */
class GroupController extends \taktwerk\yiiboilerplate\modules\user\controllers\base\GroupController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\user\models\Group';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\user\models\search\Group';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
}
