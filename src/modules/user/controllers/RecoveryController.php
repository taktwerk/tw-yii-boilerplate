<?php

namespace taktwerk\yiiboilerplate\modules\user\controllers;

use Da\User\Event\FormEvent;
use Da\User\Event\ResetPasswordEvent;

use Da\User\Form\RecoveryForm;
use Da\User\Service\PasswordRecoveryService;
use Da\User\Service\ResetPasswordService;
use Da\User\Validator\AjaxRequestModelValidator;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;

use yii\web\NotFoundHttpException;
use taktwerk\yiiboilerplate\modules\usermanager\models\Token;

/**
 * Class RecoveryController
 * @package taktwerk\yiiboilerplate\modules\user\controllers
 */
class RecoveryController extends \Da\User\Controller\RecoveryController
{

    /**
     * Displays / handles user password reset.
     *
     * @param $id
     * @param $code
     *
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     * @throws InvalidParamException
     * @return string
     *
     */
    public function actionReset($id, $code, $set=false)
    {
        if (!$this->module->allowPasswordRecovery && !$this->module->allowAdminPasswordRecovery) {
            throw new NotFoundHttpException();
        }
        /** @var Token $token */
        $token = $this->tokenQuery->whereUserId($id)->whereCode($code)
        ->whereIsTypes([Token::TYPE_RECOVERY,Token::TYPE_SET_PASSWORD])
        ->one();
        /** @var ResetPasswordEvent $event */
        $event = $this->make(ResetPasswordEvent::class, [$token]);

        $this->trigger(ResetPasswordEvent::EVENT_BEFORE_TOKEN_VALIDATE, $event);

        if ($token === null || $token->getIsExpired() || $token->user === null) {
            Yii::$app->session->setFlash(
                'danger',
                Yii::t('twbp', 'Recovery link is invalid or expired. Please try requesting a new one.')
            );

            return $this->render(
                'reset',
                [
                    'title' => Yii::t('twbp', 'Invalid or expired link'),
                    'module' => $this->module,
                    'message' => true
                ]
            );
        }

        /** @var \Da\User\Form\RecoveryForm $form */
        $form = $this->make(RecoveryForm::class, [], ['scenario' => RecoveryForm::SCENARIO_RESET]);
        $event = $event->updateForm($form);

        $this->make(AjaxRequestModelValidator::class, [$form])->validate();

        if ($form->load(Yii::$app->getRequest()->post())) {
            if ($this->make(ResetPasswordService::class, [$form->password, $token->user])->run()) {
                $this->trigger(ResetPasswordEvent::EVENT_AFTER_RESET, $event);
                if(Yii::$app->request->get('set')=='set'){
                    Yii::$app->session->setFlash('success', Yii::t('twbp', 'Password has been set'));   
                }else{
                    Yii::$app->session->setFlash('success', Yii::t('twbp', 'Password has been changed'));
                }
                Yii::$app->user->login($token->user);
                $token->delete();
                return $this->redirect(Yii::$app->getUser()->getReturnUrl());
                return $this->goHome();
            }
        }

        return $this->render('reset', ['model' => $form]);
    }


    /**
     * Displays / handles user password recovery request.
     *
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     * @throws InvalidParamException
     * @return string
     *
     */
    public function actionRequest()
    {
        if (!$this->module->allowPasswordRecovery) {
            throw new NotFoundHttpException();
        }

        /** @var RecoveryForm $form */
        $form = $this->make(RecoveryForm::class, [], ['scenario' => RecoveryForm::SCENARIO_REQUEST]);

        $event = $this->make(FormEvent::class, [$form]);

        $this->make(AjaxRequestModelValidator::class, [$form])->validate();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->trigger(FormEvent::EVENT_BEFORE_REQUEST, $event);

            $mailService = \taktwerk\yiiboilerplate\modules\user\factory\MailFactory::makeRecoveryMailerService($form->email);
            if ($this->make(PasswordRecoveryService::class, [$form->email, $mailService])->run()) {
                $this->trigger(FormEvent::EVENT_AFTER_REQUEST, $event);
            }

            $form->email = '';

            Yii::$app->session->setFlash('success',Yii::t('twbp','Recovery message sent'));
            return $this->render('request', ['model' => $form]);
        }

        return $this->render('request', ['model' => $form]);
    }
}
