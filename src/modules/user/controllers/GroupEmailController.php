<?php
//Generation Date: 25-Jan-2021 10:26:46am
namespace taktwerk\yiiboilerplate\modules\user\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\Response;

/**
 * This is the class for controller "GroupEmailController".
 */
class GroupEmailController extends \taktwerk\yiiboilerplate\modules\user\controllers\base\GroupEmailController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\user\models\GroupEmail';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\user\models\search\GroupEmail';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
    /* public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'list-of-additional-actions',
                        ],
                        'roles' => ['@']
                    ]
                ]
            ]
        ]);
    } */
    public function formFieldsOverwrite($model, $form)
    {
        $form->enableAjaxValidation = true;
        $form->enableClientValidation = false;
    }
    public function actionCreate(int $id = null, $fromRelation = null, $model_id = null){
        $model = $this->getModel();
        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        return parent::actionCreate($id,$fromRelation,$model_id);
    }
    public function actionUpdate($id, $fromRelation = null)
    {
        $model = $this->findModel($id);
        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        return parent::actionUpdate($id,$fromRelation);
    }
}
