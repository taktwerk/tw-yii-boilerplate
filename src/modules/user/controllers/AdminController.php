<?php

namespace taktwerk\yiiboilerplate\modules\user\controllers;

use Da\User\Controller\AdminController as BaseController;
use Da\User\Event\UserEvent;
use Da\User\Factory\MailFactory;
use Da\User\Filter\AccessRuleFilter;
use taktwerk\yiiboilerplate\modules\user\models\User;
use Da\User\Service\PasswordRecoveryService;
use Da\User\Validator\AjaxRequestModelValidator;
use taktwerk\yiiboilerplate\modules\user\models\Profile;
use taktwerk\yiiboilerplate\modules\user\models\UserTwData;
use taktwerk\yiiboilerplate\modules\user\service\UserCreateService;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\base\Exception;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use Yii;
use yii\helpers\VarDumper;

class AdminController extends BaseController
{
    /**
     * @var string
     */
    public $layout = "@taktwerk-backend-views/layouts/main";

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'ruleConfig' => [
                    'class' => AccessRuleFilter::class,
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['switch-identity','resend-recovery'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['admin', 'Administrator'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => YII_ENV == 'test' ? ['post', 'get'] : ['post'],
                ],
            ],
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id
     * @return \yii\web\Response
     * @throws \Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        return parent::actionDelete($id);
    }

    /**
     * Updates an existing profile.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionUpdateProfile($id)
    {
        Url::remember('', 'actions-redirect');
        $user = $this->userQuery->where(['id' => $id])->one();
        $profile = $user->profile;

        if ($profile == null) {
            $profile = Yii::createObject(Profile::class);
            $profile->link('user', $user);
        }
        /** @var UserEvent $event */
        $event = $this->make(UserEvent::class, [$user]);

        $this->make(AjaxRequestModelValidator::class, [$profile])->validate();

        if ($profile->load(Yii::$app->request->post())) {
            if ($profile->save()) {
                $profile->uploadFile(
                    'picture',
                    $_FILES['Profile']['tmp_name'],
                    $_FILES['Profile']['name']
                );

                $this->trigger(UserEvent::EVENT_BEFORE_PROFILE_UPDATE, $event);
                Yii::$app->getSession()->setFlash('success', Yii::t('twbp', 'Profile details have been updated'));
                $this->trigger(UserEvent::EVENT_AFTER_PROFILE_UPDATE, $event);

                return $this->refresh();
            }
        }

        if ($profile->load(Yii::$app->request->post()) && $profile->save()) {

            Yii::$app->getSession()->setFlash('success', Yii::t('twbp', 'Profile details have been updated'));
            $this->trigger(self::EVENT_AFTER_PROFILE_UPDATE, $event);
            return $this->refresh();
        }

        return $this->render('_profile', [
            'user' => $user,
            'profile' => $profile,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws InvalidConfigException
     */
    public function actionTwData($id)
    {
        Url::remember('', 'actions-redirect');
        $user = $this->userQuery->where(['id' => $id])->one();
        $userTwData = $user->userTwData;

        if ($userTwData == null) {
            $userTwData = Yii::createObject(UserTwData::class);
            $userTwData->link('user', $user);
        }
        if ($userTwData->load(Yii::$app->request->post()) && $userTwData->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('twbp', 'User Tw Data details have been updated'));
            return $this->refresh();
        }

        return $this->render('_tw_data', [
            'user' => $user,
            'model' => $userTwData,
        ]);
    }


    public function actionCreate()
    {
        /** @var User $user */
        $user = $this->make(User::class, [], ['scenario' => 'create']);

        /** @var UserEvent $event */
        $event = $this->make(UserEvent::class, [$user]);

        $this->make(AjaxRequestModelValidator::class, [$user])->validate();

        if ($user->load(Yii::$app->request->post()) && $user->validate()) {

            $this->trigger(UserEvent::EVENT_BEFORE_CREATE, $event);

            if($user->password_email){
                $user->confirmed_at = time();
                $user->password = !empty($user->password)
                ? $user->password
                    : Yii::$app->security->generatePasswordHash(time());
                $user->set_password = true;
                $user->save();
                $mailService = \taktwerk\yiiboilerplate\modules\user\factory\MailFactory::makeWelcomeWithPasswordMailerService($user, $user->set_password);
                $this->make(PasswordRecoveryService::class, [$user, $mailService])->run();
                Yii::$app->getSession()->setFlash('success', Yii::t('twbp', 'User has been created'));
                $this->trigger(UserEvent::EVENT_AFTER_CREATE, $event);
                return $this->redirect(['update', 'id' => $user->id]);
            }else{
                $mailService = MailFactory::makeWelcomeMailerService($user);
                if ($this->make(UserCreateService::class, [$user, $mailService])->run()) {
                    Yii::$app->getSession()->setFlash('success', Yii::t('twbp', 'User has been created'));
                    $this->trigger(UserEvent::EVENT_AFTER_CREATE, $event);
                    return $this->redirect(['update', 'id' => $user->id]);
                }
            }
            Yii::$app->session->setFlash('danger', Yii::t('twbp', 'User account could not be created.'));
        }

        return $this->render('create', ['user' => $user]);
    }

    /**
     * Permissions of a user
     *
     * @param int $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPermissions($id)
    {
        Url::remember('', 'actions-redirect');
        $user = $this->userQuery->where(['id' => $id])->one();

        return $this->render('_permissions', [
            'user' => $user,
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\base\InvalidConfigException
     */
    public function actionResendRecovery($id){
        $user = User::findOne($id);

        $mailService = \taktwerk\yiiboilerplate\modules\user\factory\MailFactory::makeWelcomeWithPasswordMailerService($user, true);
        $this->make(PasswordRecoveryService::class, [$user->email, $mailService])->run();
        Yii::$app->getSession()->setFlash('success', Yii::t('twbp', 'Registration email resent successfully'));
        return $this->redirect(Yii::$app->request->referrer?:['index']);
    }
}
