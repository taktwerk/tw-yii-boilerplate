<?php


namespace taktwerk\yiiboilerplate\modules\user\controllers;

use app\models\User;
use \Da\User\Controller\SecurityController as BaseSecurityController;
use Da\User\Event\FormEvent;
use Da\User\Event\UserEvent;
use taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory\implementations\LoginAction;
use taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory\implementations\LoginAdfsAction;
use taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory\implementations\LoginLdapAction;
use taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory\implementations\LoginSelectionAction;
use taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory\implementations\LoginUsernameAction;
use taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory\implementations\VerifyAction;
use taktwerk\yiiboilerplate\modules\user\forms\LoginForm;
use taktwerk\yiiboilerplate\modules\user\components\adfs\LoginAdfsForm;
use taktwerk\yiiboilerplate\modules\user\models\TwoWay;
use taktwerk\yiiboilerplate\modules\user\models\UserAuthCode;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\ForbiddenHttpException;
use Exception;
use yii\web\HttpException;
use yii\web\Response;
use Yii;
use yii\widgets\ActiveForm;
use taktwerk\yiiboilerplate\modules\user\plugins\LoginFlashMessage;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use yii\helpers\Url;

class SecurityController extends BaseSecurityController
{
    /**
     * @var bool disable the layout for the views
     */
    public $layout = false;

    /**
     * @var string login view
     */
    public $loginView = '@taktwerk-boilerplate/modules/user/views/security/login';

    protected $viewSection = 'login-regular';
    protected $scenario;
    protected $afterLoadModelAction;
    protected $pageResponseAction;
    protected $activeFormClass = ActiveForm::class;

    /** @inheritdoc */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'login-username',
                            'login-selection',
                            'verify',
                            'check-code',
                            'refresh-code',
                            'code',
                            'login-ldap',
                            'login-adfs',
                            'login-adfs-callback',
                        ],
                        'roles' => ['?', '@']
                    ]
                ]
            ]
        ]);
    }

    /**
     * Override the Actions
     */
    public function actions()
    {
        return [
            'login' => ['class' => ClassDispenser::getMappedClass(LoginAction::class)],
            'login-username' => ['class' => ClassDispenser::getMappedClass(LoginUsernameAction::class)],
            'login-selection' => ['class' => ClassDispenser::getMappedClass(LoginSelectionAction::class)],
            'login-ldap' => ['class' => ClassDispenser::getMappedClass(LoginLdapAction::class)],
            'login-adfs' => ['class' => ClassDispenser::getMappedClass(LoginAdfsAction::class)],
            'verify' => ['class' => ClassDispenser::getMappedClass(VerifyAction::class)],
        ];
    }

    /**
     * Callback after ADFS login request
     * @return Response
     */
    public function actionLoginAdfsCallback()
    {
        if (Yii::$app->request->get('state') && Yii::$app->request->get('code')) {
            try {
                $form = $this->make(LoginAdfsForm::class);

                $form->login();
                LoginFlashMessage::save(
                    'success',
                    Html::a(
                        Yii::t(
                            'twbp',
                            'Login successful. Welcome! Click here to go to your dashboard.'
                        ),
                        '/backend'
                    ));
            } catch (Exception $e) {
                LoginFlashMessage::save('danger', $e->getMessage());
            } catch (yii\base\Exception $e) {
                LoginFlashMessage::save('danger', $e->getMessage());
            } catch (\Auth0\SDK\Exception\CoreException $e) {
                LoginFlashMessage::save('danger', Yii::t('twbp', 'Incorrect ADFS callback'));
            }
        } else {
            LoginFlashMessage::save('danger', Yii::t('twbp', 'Incorrect ADFS callback'));
        }
        return $this->goBack();
    }

    /**
     * Reset the origin site
     * @return Response
     */
    public function actionResetOrigin()
    {
        $cookieName = Yii::$app->getModule('user')->originCookieName;
        Yii::$app->getResponse()->cookies->remove($cookieName);
        return $this->goHome();
    }

    /**
     * @return array|string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCheckCode()
    {
        $user_id = Yii::$app->request->post('TwoWay')['user_id'];
        $twoWay = new TwoWay($user_id);
        $twoWay->setScenario(TwoWay::SCENARIO_WITH_CODE);
        if ($twoWay->load(Yii::$app->request->post())) {
            $twoWay->checkCode();
            if ($twoWay->status === TwoWay::SUCCESS) {
                $model = \Yii::createObject(LoginForm::class);
                $user = User::findOne($user_id);
                $model->login = $user->username;
                $model->setScenario(LoginForm::TWO_WAY);
                if ($model->login()) {
                    $event = $this->getFormEvent($model);
                    $this->trigger(FormEvent::EVENT_AFTER_LOGIN, $event);
                    if (!Yii::$app->request->isAjax) {
                        return $this->redirect(Yii::$app->getUser()->getReturnUrl());
                    }
                }
            }
        }
        if (Yii::$app->request->isAjax) {
            return json_encode([
                'status' => $twoWay->status,
                'message' => $twoWay->message,
                'returnUrl' => Yii::$app->getUser()->getReturnUrl()
            ]);
        }

        return $this->render($this->loginView, [
            'section' => $twoWay->view,
            'model' => $twoWay,
            'module' => $this->module,
            'qr' => $twoWay->getCode(),
        ]);
    }

    /**
     * @return bool|string
     */
    public function actionRefreshCode()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $twoWay = new TwoWay(Yii::$app->request->post('user_id'));
        $twoWay->code = Yii::$app->request->post('code');

        $code = $twoWay->refreshCode();

        return json_encode([
            'code' => $code,
            'imageLink' => UserAuthCode::generateQr($code),
        ]);
    }

    public function actionLogout()
    {
        $event = $this->make(UserEvent::class, [Yii::$app->getUser()->getIdentity()]);

        $this->trigger(UserEvent::EVENT_BEFORE_LOGOUT, $event);

        if (Yii::$app->getUser()->logout()) {
            $this->trigger(UserEvent::EVENT_AFTER_LOGOUT, $event);

            if (isset(Yii::$app->components['adfs'])) {
                return $this->redirect(Yii::$app->adfs->getLogoutUrl()); // logout user on Adfs
            }
        }

        return $this->goHome();
    }
}
