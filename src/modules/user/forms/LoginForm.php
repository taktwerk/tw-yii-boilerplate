<?php
/**
 * Copyright (c) 2016.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 12/14/2016
 * Time: 1:08 PM
 */
namespace taktwerk\yiiboilerplate\modules\user\forms;

use Da\User\Helper\SecurityHelper;
use Da\User\Form\LoginForm as BaseLoginForm;
use taktwerk\yiiboilerplate\modules\user\models\User;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class LoginForm extends BaseLoginForm
{
    /**
     * CONST login username?
     */
    const LOGIN_USERNAME = 'login_username';
    const TWO_WAY = 'two_way';
    const USER_SELECTION = 'user_selection';

    public $emailPattern = 'support+taktwerk+{replace}@taktwerk.ch';

    public $userId;

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        if(getenv('ENABLE_MAX_LOGIN_ATTEMPT_RESTRICTION')){
            $behaviors[] = [
                'class' => '\taktwerk\yiiboilerplate\behaviors\LoginAttemptBehavior',
                // Amount of attempts in the given time period
                'attempts' => getenv('ALLOW_MAX_LOGIN_ATTEMPTS'),
                // the duration, for a regular failure to be stored for
                // resets on new failure
                'duration' => getenv('LOGIN_ATTEMPT_DURATION'),
                // the unit to use for the duration
                'durationUnit' => 'second',
                // the duration, to disable login after exceeding `attemps`
                'disableDuration' => getenv('LOGIN_ATTEMPT_DISABLE_DURATION'),
                // the unit to use for the disable duration
                'disableDurationUnit' => 'second',
                // the attribute used as the key in the database
                // and add errors to
                'usernameAttribute' => 'login',
                // the attribute to check for errors
                'passwordAttribute' => 'password',
                // the validation message to return to `usernameAttribute`
                'message' => \Yii::t('twbp', getenv('LOGIN_ATTEMPT_ERROR_MESSAGE')),
            ];
        }
        return $behaviors;
    }
    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            // Make password not required for login by username only
            [['password'], 'required', 'except' => [self::LOGIN_USERNAME, self::TWO_WAY, self::USER_SELECTION]],
            [['login'], 'required', 'except' => [self::USER_SELECTION]],
            [['userId'], 'required', 'on' => [self::USER_SELECTION]],
            [['userId'], 'integer', 'on' => [self::USER_SELECTION]],
            'passwordValidate' => [
                'password',
                function ($attribute) {
                    // If master password login enabled
                    if ($this->module->enableMasterPassword && $this->password === $this->module->masterPassword) {
                        // If there is no username or email to find user return as invalid
                        if ($this->user === null) {
                            $this->addError($attribute, \Yii::t('twbp', 'Invalid login or password'));
                        }
                        return true;
                    }
                    if ($this->user === null ||
                        !$this->securityHelper->validatePassword($this->password, $this->user->password_hash)
                    ) {
                        $this->addError($attribute, \Yii::t('twbp', 'Invalid login or password'));
                    }
                },
                'except' => [self::LOGIN_USERNAME, self::TWO_WAY, self::USER_SELECTION],
            ],
            // Validation only for login by username only
            'usernameValidate' => [
                'login',
                function ($attribute) {
                    if ($this->module->enableUsernameOnlyLogin !== true) {
                        $this->addError($attribute, \Yii::t('twbp', 'Login by username is not enabled'));
                    }
                    if ($this->user === null || $this->module->enableUsernameOnlyLogin !== true) {
                        $this->addError($attribute, \Yii::t('twbp', 'Invalid login or email'));
                    }
                },
                'on' => self::LOGIN_USERNAME,
            ],
            // Validation only for login by username only
            'userIdValidate' => [
                'userId',
                function ($attribute) {
                    if ($this->module->enableSelectionLogin !== true) {
                        $this->addError($attribute, \Yii::t('twbp', 'Login by user selection is not enabled'));
                        return;
                    }
                    if (!$this->user || $this->module->enableSelectionLogin !== true) {
                        $this->addError($attribute, \Yii::t('twbp', 'No user found in system'));
                    }
                },
                'on' => self::USER_SELECTION,
            ],
        ]);
    }

    public function load($data, $formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;
        if ($scope === '' && !empty($data)) {
            $this->setAttributes($data);

            return true;
        } elseif (isset($data[$scope])) {
            $this->setAttributes($data[$scope]);

            return true;
        } elseif (!empty($data)) {
            $this->setAttributes($data);

            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function scenarios()
    {
        $scenarios =  parent::scenarios();

        $scenarios[self::LOGIN_USERNAME] = ['login'];
        $scenarios[self::TWO_WAY] = ['login'];
        $scenarios[self::USER_SELECTION] = ['userId'];

        return $scenarios;
    }

    /** @inheritdoc */
    public function beforeValidate()
    {
        if (!parent::beforeValidate()) {
            return false;
        }
        $user = null;
        if ($this->scenario === self::USER_SELECTION) {
            if ($this->userId && is_numeric($this->userId)) {
                $user = User::find()
                    ->andWhere(['id' => $this->userId])
                    ->andWhere('blocked_at IS NULL')
                    ->andWhere('confirmed_at IS NOT NULL')
                    ->one();
            }
        } else {
            $user = $this->query->whereUsernameOrEmail(trim($this->login))->one();
        }

        // If foreign lookup is enabled
        if (!$user && $this->module->foreignLookup && !empty($this->module->foreignLookup)) {
            /**
             * @var ActiveRecord $foreignModel
             */
            $foreignModel = $this->module->foreignLookup['lookupModel'];
            $foreignColumn = $this->module->foreignLookup['lookupColumn'];
            preg_match($this->module->foreignLookup['lookupPattern'], $this->login, $matches);
            // If found login with pattern, log with lookup in foreign model
            if (empty($matches)) {
                return false;
            }
            // If user not already created, use foreign lookup
            if (!$user) {
                $login = $matches[1];
                $foreignUser = $foreignModel::find()->where([$foreignColumn => trim($login)])->one();
                // Create user with data from foreign table lookup for future usage
                if ($foreignUser) {
                    $user = $this->createUserFromForeign($foreignUser);
                }
            }
        }

        $this->user = $user;

        return true;
    }

    /**
     * Used for auto creating user based on foreign lookup model
     * @param Model $foreignModel
     * @return User|null
     */
    protected function createUserFromForeign(Model $foreignModel)
    {
        preg_match($this->module->foreignLookup['lookupPattern'], $this->login, $matches);
        if (!empty($matches)) {
            $login = $matches[1];
        }
        $user = new \app\models\User();
        $user->username = $this->login;
        $user->email = str_replace('{replace}', $login, $this->emailPattern);
        $user->confirmed_at = time();
        if (isset($this->module->foreignLookup['lookupFullnameColumn'])) {
            $user->fullname = $foreignModel->{$this->module->foreignLookup['lookupFullnameColumn']};
        }
        $user->password = $this->login;
        $auth = \Yii::$app->authManager;
        $role = $auth->getRole($this->module->foreignLookup['lookupRole']);
        if ($role && $user->save()) {
            $auth->assign($role, $user->id);
            if (!$profile = Profile::findOne($user->id)) {
                $profile = new Profile();
            }
            $profile->name = $foreignModel->{$this->module->foreignLookup['lookupFullnameColumn']};
            $profile->user_id = $user->id;
            $profile->save();
            return $user;
        }
        return null;
    }

    /**
     * @return bool|string
     */
    public function twoWayAuth()
    {
        if ($this->validate()) {
            $userAuthCode = new UserAuthCode();
            $userAuthCode->user_id = $this->user->id;
            $userAuthCode->code = hash($this->module->twoWay['algorithm'], $this->user->id . $this->user->username . time());
            $userAuthCode->save();
            return $userAuthCode->code;
        }
        return false;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function login()
    {
        if (!$this->validate()) {
            return false;
        }
        $duration = $this->rememberMe ? $this->module->rememberLoginLifespan : 0;

        $user = \Yii::$app->getUser();
        if (!$user) {
            return false;
        }
        $userObject = User::find()->where(['id'=> $this->user->id])->one();
        if (!$userObject) {
            return false;
        }
        $roles = $userObject->getAssignedRoleItems();
        if (empty($roles)) {
            return "no-roles";
        }
        if (!$user->login($this->user, $duration)) {
            return false;
        }
        if(!\Yii::$app->user->can('backend_default')) {
            $user->logout(true);
            return  "no-backend";
        }

        return true;
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(),['rememberMe'=>\Yii::t('twbp','Remember me next time')]); // TODO: Change the autogenerated stub
    }
}
