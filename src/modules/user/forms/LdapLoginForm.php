<?php

namespace taktwerk\yiiboilerplate\modules\user\forms;

use taktwerk\yiiboilerplate\modules\user\exceptions\LdapLoginException;
use app\models\User;
use Da\User\Helper\SecurityHelper;
use taktwerk\yiiboilerplate\helpers\DebugHelper;
use Yii;
use Adldap\Adldap;
use Adldap\Exceptions\AdldapException;
use yii\helpers\ArrayHelper;

/**
 * Class LdapLogin
 * @package app\components
 */
class LdapLoginForm
{
    /**
     * @var SecurityHelper
     */
    protected $securityHelper;

    /**
     * LdapLogin constructor.
     * @param SecurityHelper $securityHelper
     */
    public function __construct(SecurityHelper $securityHelper)
    {
        $this->securityHelper = $securityHelper;
    }

    /**
     * @param array $data
     */
    public function authenticate($username = '', $password = 'password')
    {
        /** @var \Adldap\AdldapInterface $ldap */
        $ldap = Yii::$app->ldap;

        // Authenticate user
        $auth = !empty(Yii::$app->params['ldap']['login']) ? Yii::$app->params['ldap']['login'] : 'cn';
        $dn = !empty(Yii::$app->params['ldap']['dn']) ? ',' . Yii::$app->params['ldap']['dn'] : '';

        // If we are login using something else than the cn, we need to get the cn first for the auth to work.
        if ($auth != 'cn') {
            $results = $ldap->search()->where($auth, '=', $username)->get();
            foreach ($results as $result) {
                $username = $result->getAttribute('cn')[0];
                continue;
            }
        }

        $authUsername = "cn=$username" . $dn;
        if ($ldap->auth()->attempt($authUsername, $password) === false) {
            throw new LdapLoginException(Yii::t('twbp', "Couldn't authenticate user {user}.", ['user' => $username]));
        }

        // Get the user info
        $results = $ldap->search()->where('cn', '=', $username)->get();
        foreach ($results as $result) {
            // Found the user, get his info
            $fullnameAttribute = Yii::$app->params['ldap']['fullname'];
            $fullname = $result->$fullnameAttribute;
            if (is_array($fullname)) {
                $fullname = $fullname[0];
            }
            $email = $result->getAttribute('mail')[0];
            $group = $this->getGroupName($result->groupmembership);

            // Log user in
            $this->logUser($email, $fullname, $password, $group);

            return true;
        }

        return false;
    }

    /**
     * Log in a user
     * @param $email
     * @param string $fullname
     * @param string $password
     * @return string|bool $group
     * @throws LdapLoginException
     */
    protected function logUser($email, $fullname = '', $password = '', $group = '')
    {
        // Check if the user exists
        $identity = User::findOne(['email' => $email]);

        if (empty($identity) && $group !== false) {
            // Set up the account
            $identity = $this->setupAccount($email, $fullname, $password, $group);
        }

        // Identity still null? Obviously we have an error and need to check what it is.
        if (empty($identity)) {
            throw new LdapLoginException(Yii::t('twbp', "Couldn't set up or connect account identity."));
        }

        // Log in the user
        Yii::$app->getUser()->login($identity);

        return true;
    }

    /**
     * Set up a new account
     *
     * @param $email
     * @param $fullname
     * @param string $password
     * @return mixed
     * @throws LdapLoginException
     */
    protected function setupAccount($email, $fullname, $password = '', $group = '')
    {
        // Account identity
        $username = str_replace(' ', '.', strtolower($fullname)); // Can't have spaces in usernames
        $password = $this->securityHelper->generatePassword(12); //!empty($password) ? Password::hash($password) : Password::generate(6);

        $user = Yii::createObject([
            'class'    => User::class,
            'scenario' => 'connect',
            'username' => $username,
            'email'    => $email,
            'fullname' => $fullname,
            'password_hash' => $password,
            'auth_key' => 'fake', // Is replaced in the beforeSave method.
        ]);

        if ($user->save(false)) {
            // LDAP users are by default Viewers.
            $role = Yii::$app->getAuthManager()->getRole($group);
            if ($role) {
                Yii::$app->getAuthManager()->assign($role, $user->id);
            }

            $user = User::findOne(['email' => $email]);
            $user->profile->name = $fullname;
            $user->profile->save();

            return $user;
        } else {
            throw new LdapLoginException(Yii::t('twbp', "Couldn't create an account for {email}.", ['email' => $email]));
        }
    }


    /**
     * Get the default group name
     * @param $groups array
     * @return string
     */
    protected function getGroupName($groups = [])
    {
        if (empty($groups)) {
            return Yii::$app->params['ldap']['defaultRole'];
        }

        $mappings = Yii::$app->params['ldap']['roleMapping'];
        foreach ($groups as $group) {
            $segments = explode(',', $group);
            $group = str_replace('cn=', '', $segments[0]);

            if (!empty($mappings[$group])) {
                return $mappings[$group];
            }
        }

        return Yii::$app->params['ldap']['defaultRole'];
    }
}
