<?php


namespace taktwerk\yiiboilerplate\modules\user\factory;

use \Da\User\Factory\TokenFactory as TF;
use taktwerk\yiiboilerplate\modules\usermanager\models\Token;
use yii\base\InvalidConfigException;

class TokenFactory extends TF
{
    /**
     * @param $userId
     *
     * @throws InvalidConfigException::
     * @return Token
     */
    public static function makeSetPasswordToken($userId)
    {
        $token = self::make($userId, Token::TYPE_SET_PASSWORD);
        $token->save(false);
        return $token;
    }
}