<?php


namespace taktwerk\yiiboilerplate\modules\user\factory;

use Da\User\Event\MailEvent;
use Da\User\Model\User;
use Da\User\Module;
use Da\User\Service\MailService;
use Yii;
use yii\base\InvalidConfigException;

class MailFactory extends \Da\User\Factory\MailFactory
{
    /**
     * @param User $user
     * @param bool $showPassword
     *
     * @throws InvalidConfigException
     * @return MailService
     */
    public static function makeWelcomeWithPasswordMailerService(User $user, $set_password=false)
    {
        /** @var Module $module */
        $module = Yii::$app->getModule('user');
        $to = $user->email;
        $from = $module->mailParams['fromEmail'];

        // hack
        $module->mailParams['welcomeMailSubject'] = Yii::t('twbp', 'Welcome to {0}', '"' . getenv('APP_TITLE') . '"');
        $subject = $module->mailParams['welcomeMailSubject'];
        $params = [
            'user' => $user,
            'token' => TokenFactory::makeSetPasswordToken($user->id),
            'module' => $module,
            'set_password' => $set_password
        ];
        $mail_service = static::makeMailerService('register', $from, $to, $subject, 'register', $params);
        return $mail_service;
    }

    /**
     * @param string                $type
     * @param array|\Closure|string $from
     * @param string                $to
     * @param string                $subject
     * @param string                $view
     * @param array                 $params
     *
     * @return \Da\User\Service\MailService|object
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public static function makeMailerService($type, $from, $to, $subject, $view, $params = [])
    {
        if ($from instanceof \Closure) {
            $from = $from($type);
        }
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return Yii::$container->get(
            \taktwerk\yiiboilerplate\modules\user\service\MailService::class,
            [$type, $from, $to, $subject, $view, $params, Yii::$app->mailer]
        );
    }
}
