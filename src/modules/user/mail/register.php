<?php

use yii\helpers\Html;

/**
 * @var \Da\User\Module      $module
 * @var \Da\User\Model\User  $user
 * @var \Da\User\Model\Token $token
 */
?>
    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
        <?= Yii::t('twbp', 'Hello') ?>,
    </p>

    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
        <?= Yii::t('twbp', 'Your account on {0} has been created', '"' . Yii::t('app', getenv('APP_TITLE')) . '"') ?>.
    </p>

<?php if ($token !== null): ?>
    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
        <?= Yii::t('twbp', 'In order to complete your registration and set password, please click the link below') ?>.
    </p>
    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
        <?php
        $url = $set_password?"/set":"";
        echo Html::a(Html::encode($token->url.$url), $token->url.$url); ?>
    </p>
    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
        <?= Yii::t('twbp', 'If you cannot click the link, please try pasting the text into your browser') ?>.
    </p>
    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    <?= Yii::t('twbp', 'The link is valid till') ?> <b><?= \Yii::$app->formatter->asDatetime(time()+ (($set_password)?$module->tokenSetPasswordLifespan:$module->tokenRecoveryLifespan));?></b></p>
    <p>&nbsp;</p>
    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
        <?= Yii::t('app', getenv('APP_MAILTEXT_REGISTER'))  ?>
    </p>
<?php endif ?>

<?php if(!$set_password):?>
    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
        <?= Yii::t('twbp', 'If you did not make this request you can ignore this email') ?>.
    </p>
<?php endif ?>