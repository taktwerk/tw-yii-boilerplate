<?php

/**
 * @var \Da\User\Model\User  $user
 * @var \Da\User\Model\Token $token
 * @var \taktwerk\yiiboilerplate\modules\user\Module $module
 */
$url = $set_password==true?"/set":"";
?>
<?= Yii::t('twbp', 'Hello') ?>,

<?= Yii::t('twbp', 'Your account on {0} has been created', '"' . Yii::t('app', getenv('APP_TITLE')) . '"') ?>.

<?php if ($token !== null): ?>
    <?= Yii::t('twbp', 'In order to complete your registration and set password, please click the link below') ?>.
    <?= $token->url.$url ?>
    <?= Yii::t('twbp', 'If you cannot click the link, please try pasting the text into your browser') ?>.

    <?= Yii::t('twbp', 'The link is valid till') ?> <?= \Yii::$app->formatter->asDatetime(time()+ (($set_password)?$module->tokenSetPasswordLifespan:$module->tokenRecoveryLifespan));?>

    <?= Yii::t('app', getenv('APP_MAILTEXT_REGISTER'))  ?>
<?php endif ?>

<?php if(!$set_password):?>
    <?= Yii::t('twbp', 'If you did not make this request you can ignore this email') ?>.
<?php endif ?>