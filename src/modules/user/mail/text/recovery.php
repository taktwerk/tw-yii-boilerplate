<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use yii\helpers\Html;

/**
 * @var \Da\User\Model\User  $user
 * @var \Da\User\Model\Token $token
 */
?>
    <?= Yii::t('twbp', 'Hello') ?>,
    <?= Yii::t(
        'twbp',
        'We have received a request to reset the password for your account on {0}',
        Yii::$app->name
    ) ?>.
    <?= Yii::t('twbp', 'Please click the link below to complete your password reset') ?>.


    <?= Html::a(Html::encode($token->url), $token->url); ?>


    <?= Yii::t('twbp', 'If you cannot click the link, please try pasting the text into your browser') ?>.

    <?= Yii::t('twbp', 'The link is valid till') ?> <?= \Yii::$app->formatter->asDatetime(time()+\Yii::$app->module->tokenRecoveryLifespan); ?>

    <?= Yii::t('twbp', 'If you did not make this request you can ignore this email') ?>.
