<?php

namespace taktwerk\yiiboilerplate\modules\user\widgets;

use Da\User\Model\Assignment;
use taktwerk\yiiboilerplate\models\User;
use Yii;
use Da\User\Widget\AssignmentsWidget as BaseAssignment;
use yii\helpers\ArrayHelper;

class AssignmentsWidget extends BaseAssignment
{

    /**
     * Returns all available auth items to be attached to user.
     * @return array
     */
    public function getAvailableItems()
    {
        $type = Yii::$app->user->can('Authority') ? null: 1;
        return ArrayHelper::map(
            $this->getAuthManager()->getItems($type),
            'name',
            function ($item) {
                return empty($item->description)
                    ? $item->name
                    : $item->name . ' (' . $item->description . ')';
            }
        );
    }

    public function run()
    {
        $model = $this->make(Assignment::class, [], ['user_id' => $this->userId]);
        $oldItems = $model->items;
        $vidgetRenderView = parent::run();
        $user = User::findIdentity($this->userId);
        if ($user) {
            $newItems = $this->params['Assignment']['items'];
            if (is_array($newItems)) {
                if (array_diff($oldItems, $newItems) ||
                    array_diff($newItems, $oldItems)
                ) {
                    $user->role_changed_at = time();
                    $user->save();
                }
            }
        }

        return $vidgetRenderView;
    }
}
