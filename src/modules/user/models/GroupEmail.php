<?php
//Generation Date: 25-Jan-2021 10:26:37am
namespace taktwerk\yiiboilerplate\modules\user\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\user\models\base\GroupEmail as BaseGroupEmail;
use taktwerk\yiiboilerplate\modules\user\models\base\Group;

/**
 * This is the model class for table "group_email".
 */
class GroupEmail extends BaseGroupEmail
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                'user_id',
                'validateUserId'
            ],
            [
                'email',
                'required',
                'when'=>function($m){
                   return $m->user_id==null;
                },
                'message'=>\Yii::t('twbp','Email is required if user not selected')
            ],
            [
                'group_id',
                'required'
            ]
        ]);
    }
    public function validateUserId($attribute, $params, $validator){
        $userGroup = UserGroup::findOne(['user_id'=>$this->$attribute,'group_id'=>$this->group_id]);
        if($userGroup==null){
            $this->addError($attribute, \Yii::t('twbp','This user does not belongs to the selected group'));
        }
        if($this->email!=null){
            $this->addError($attribute, \Yii::t('twbp','You can either set the email field or the user'));
        }
    }


    public static function mailingList($groupId) {
        $emails = [];
        $groupReceiversQ = GroupEmail::find()
        ->addSelect(['email','user_id'])
        ->where(['group_id' => $groupId]);
        foreach($groupReceiversQ->batch() as $groupReceivers){
            foreach($groupReceivers as $receiver) {
                if($receiver->email) {
                    $emails[] = $receiver->email;
                }
                else if($receiver->user && $receiver->user->email) {
                    $emails[] = $receiver->user->email;
                }
            }
        }
        return $emails;
    }
}
