<?php

namespace taktwerk\yiiboilerplate\modules\user\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\user\models\base\Group as BaseGroup;

/**
 * This is the model class for table "group".
 */
class Group extends BaseGroup
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }
    public static function find($removedDeleted = true)
    {
        $model = parent::find($removedDeleted);
        // If not authority, only show groups assigned to the current user
        if (php_sapi_name() != "cli" && ! Yii::$app->getUser()->can('Authority')) {
            $tbl = static::getTableSchema()->fullName;
            $groupIds = UserGroup::find()->select('group_id')
                ->where([
                'user_id' => Yii::$app->getUser()->id
            ])
                ->column();
            if (empty($groupIds)) {
                $model->andWhere([
                    $tbl . '.id' => null
                ]);
            } else {
                $model->andWhere([
                    $tbl . '.id' => $groupIds
                ]);
            }
        }
        return $model;
    }
    public function duplicatable(){
        return false;
    }
}
