<?php

namespace taktwerk\yiiboilerplate\modules\user\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\user\models\base\UserAuthLog as BaseUserAuthLog;

/**
 * This is the model class for table "user_auth_log".
 */
class UserAuthLog extends BaseUserAuthLog
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

}
