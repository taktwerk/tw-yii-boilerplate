<?php

namespace taktwerk\yiiboilerplate\modules\user\models;

use taktwerk\yiiboilerplate\models\TwActiveRecord;

/**
 * Class LoginAttempt
 *
 * @package taktwerk\yiiboilerplate\modules\user\models
 */
class LoginAttempt extends TwActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'login_attempt';
    }

    /**
     * @return array[]
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
        ];
    }
}
