<?php

/**
 * @property \taktwerk\yiiboilerplate\modules\share\models\FlysystemUser[] $flysystemUsers
 * @property \taktwerk\yiiboilerplate\modules\share\models\Flysystem[]     $flysystems
 * @property \taktwerk\yiiboilerplate\modules\user\models\UserTwData       $userTwData
 * @property \taktwerk\yiiboilerplate\modules\user\models\Profile          $profile
 * @property $password_email boolean
 */
namespace taktwerk\yiiboilerplate\modules\user\models;

use Da\User\Event\UserEvent;
use Da\User\Module;
use Da\User\Service\MailService;
use taktwerk\yiiboilerplate\enums\AppConfigurationModeEnum;
use taktwerk\yiiboilerplate\rest\Token;
use taktwerk\yiiboilerplate\traits\UserAfterSaveTrait;
use yii\base\InvalidConfigException;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\UnauthorizedHttpException;
use Yii;
use Da\User\Model\Assignment;
use taktwerk\yiiboilerplate\modules\user\models\UserTwData;
use taktwerk\yiiboilerplate\modules\backend\models\RelationModelInterface;
use yii2tech\authlog\AuthLogIdentityBehavior;
use kartik\password\StrengthValidator;
use Endroid\QrCode\Builder\Builder;
use DeviceDetector\Parser\Device\AbstractDeviceParser;
use DeviceDetector\DeviceDetector;
use yii\helpers\Json;
class User extends \Da\User\Model\User implements RelationModelInterface
{
    /**
     * @var bool
     */
    public $password_email = false;
    /**
     * @var bool
     */
    public $set_password = false;
    /**
     * @var string
     */
    public $password_pattern = 'normal';

    const IDENTIFIER_STRING_LENGTH = 16;

    use UserAfterSaveTrait;

    public function init(){
        $this->on(UserEvent::EVENT_AFTER_BLOCK, [$this, 'afterBlock']);
    }

    /**
     * todo: determine if this is the best way to handle this. It is similar in dektrium, which seems ridiculous.
     * @var UserTwData
     */
    private $_userTwData;

    /**
     * @return array
     */
    public function rules()
    {
        $rules = ['passwordEmail' => [['password_email','password_pattern','set_password'],'safe']];
        if(getenv('ENABLE_PASSWORD_PATTERN')){
            if(!empty(getenv('PASSWORD_PATTERN'))){
                $this->password_pattern = getenv('PASSWORD_PATTERN');
            }
            $rules[] = [['password'], StrengthValidator::className(), 'preset'=>$this->password_pattern];
        }
        return ArrayHelper::merge(parent::rules(),$rules);
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(),[
            'authLog' => [
                'class' => AuthLogIdentityBehavior::className(),
                'authLogRelation' => 'authLogs',
                'defaultAuthLogData' => function ($model) {
                    return [
                        'ip' => Yii::$app->request->getUserIP(),
                        'host' => @gethostbyaddr(Yii::$app->request->getUserIP()),
                        'url' => Yii::$app->request->getAbsoluteUrl(),
                        'user_agent' => Yii::$app->request->getUserAgent(),
                        'user_env' => $model->getUserDeviceEnv(),
                    ];
                },
            ],
        ]);
    }
    public function getUserDeviceEnv(){
        AbstractDeviceParser::setVersionTruncation(AbstractDeviceParser::VERSION_TRUNCATION_NONE);
        $dd = new DeviceDetector(\Yii::$app->request->getUserAgent());
        $dd->parse();
        $devData = [];
        if ($dd->isBot()) {
            // handle bots,spiders,crawlers,...
            $devData['botInfo'] = $dd->getBot();
        } else {
            $devData['clientInfo'] = $dd->getClient(); // holds information about browser, feed reader, media player, ...
            $devData['osInfo'] = $dd->getOs();
            $devData['device'] = $dd->getDeviceName();
            $devData['brand'] = $dd->getBrandName();
            $devData['model'] = $dd->getModel();
        }
        return Json::encode($devData);
    }
    public function getAuthLogs()
    {
        return $this->hasMany(UserAuthLog::className(), ['user_id' => 'id']);
    }
    /**
     * User's toString method. Can be overridden.
     * @return mixed
     */
    public function toString()
    {
        $name = $this->username;
        if(isset($this->profile) && !empty($this->profile->name)) {
            $name = $this->profile->name;
        }
        else if(isset($this->fullname) && !empty($this->fullname)) {
            $name = $this->fullname;
        }

        return $name;
    }

    /**
     * Getter for toString() function
     * @return String
     */
    public function getToString()
    {
        return $this->toString();
    }

    /**
     * @param mixed $token
     * @param null  $type
     * @return User
     * @throws UnauthorizedHttpException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        /** @var Token $t */
        $tokenModel = Token::find()->where(['code' => $token])->one();

        if ($tokenModel == null) {
            throw new UnauthorizedHttpException("not a valid access token");
        }
        if ($tokenModel->isExpired) {
            $tokenModel->delete();
            throw new UnauthorizedHttpException("access token is expired");
        }

        $tokenModel->created_at = time();
        $tokenModel->save(false, ['created_at']);

        /** @var User $u */
        $user = User::findOne($tokenModel->user_id);

        return $user;
    }

    /**
     * Check if user is admin
     * @return bool
     */
    public function getIsAdmin()
    {
        return $this->isReallyAdmin() || $this->checkAdminRoles() || $this->isRootUser();
    }


    /**
     * @return bool Whether the user is an admin or not.
     */
    private function isReallyAdmin()
    {
        return (Yii::$app->getAuthManager() && $this->module->administratorPermissionName ?
                Yii::$app->authManager->checkAccess($this->id, $this->module->administratorPermissionName) : false)
            || in_array($this->username, $this->module->administrators);
    }

    /**
     * Check if the user is a root user
     * @return bool
     */
    private function isRootUser()
    {
        return in_array($this->username, Yii::$app->user->rootUsers);
    }

    /**
     * Check if user has admin roles
     * @return bool
     */
    private function checkAdminRoles()
    {
        foreach (Yii::$app->authManager->getRolesByUser($this->id) as $role) {
            if (in_array($role->name, Yii::$app->user->rootRoles)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return details for dropdown field
     * @return string
     */
    public function getEntryDetails()
    {
        return '';
    }

    /**
     * @return array
     */
    public function extraFields()
    {
        return array_merge([
            'flysystemUsers',
            'flysystems',
            'userTwData',
        ], parent::extraFields());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlysystemUsers()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\share\models\FlysystemUser::class,
            ['user_id' => 'id']
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlysystems()
    {
        return $this->hasMany(
            \taktwerk\yiiboilerplate\modules\share\models\Flysystem::class,
            ['id' => 'user_id']
        )
            ->viaTable(\taktwerk\yiiboilerplate\modules\share\models\FlysystemUser::tableName(), ['fs_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTwData()
    {
        return $this->hasOne(UserTwData::class, ['user_id' => 'id']);
    }

    /**
     * @param null $options
     * @return mixed|null
     * @throws \Exception
     */
    public function profilePicture($options = null)
    {
        if ($this->profile == null) {
            return null;
        }
        if ($this->profile->picture == null) {
            return null;
        }
        return $this->profile->getFileUrl('picture', $options);
    }

    /**
     * Returns url to users own profile form
     * @return string
     */
    public function profileUrl()
    {
        return \yii\helpers\Url::to(['/user/settings/profile']);
    }

    /**
     * User for filtering results for Select2 element
     * @param null $q
     * @return array
     */
    public static function filter($q = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select(['id', 'text' => Profile::tableName() . '.name'])
                ->leftJoin(Profile::tableName(), Profile::tableName() . '.user_id = ' . User::tableName() . '.id')
                ->from(self::tableName())
                ->andWhere([\app\models\User::tableName() . '.blocked_at' => null])
                ->andWhere(['like', Profile::tableName() . '.name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentProfile()
    {
        return $this->getProfile();
    }

    /**
     * @param $insert
     * @param $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        // This will take care of the profile
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
        if (!$insert) {
            if (!empty($changedAttributes['password_changed_at']) ||
                !empty($changedAttributes['role_changed_at']) ||
                !empty($changedAttributes['blocked_at'])
            ) {
                Yii::$app->getUser()->logoutByUserId($this->id);
            }
        }
        // Make sure the schema exists first. Need to do this for projects that create users before the migration
        // creating the table exists.
        try {
            if ($this->userTwData == null) {
                $this->userTwData = Yii::createObject(UserTwData::class);
                $this->userTwData->link('user', $this);
            }
        } catch (\Exception $e) {
            // If something went wrong, and it's not because the table doesn't exist yet, we want to catch it.
            $tableSchema = Yii::$app->db->schema->getTableSchema('{{%user_tw_data}}');
            if (!$tableSchema === null) {
                throw $e;
            }
        }
        $this->newsletterUserAfterSave($insert, $changedAttributes);
    }

    /**
     * Todo: is this really how this should be done?
     * @param UserTwData $userTwData
     */
    public function setUserTwData(UserTwData $userTwData)
    {
        $this->userTwData = $userTwData;
    }

    /**
     * @return mixed
     */
    public function beforeDelete()
    {
        if ($this->userTwData) {
            $this->userTwData->forceDelete();
        }
        if ($this->profile) {
            $this->profile->delete();
        }
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->identifier = Yii::$app->security->generateRandomString(self::IDENTIFIER_STRING_LENGTH);
        }

        return parent::beforeSave($insert);
    }

    public function afterBlock()
    {
        Yii::$app->getUser()->logoutByUserId($this->id);
    }
    /**
     * @return bool whether is blocked or not
     */
    public function getIsBlocked()
    {
        return $this->blocked_at !== null;
    }
    public function qrcode()
    {
        if (!Yii::$app->params['appSettings']) {
            return null;
        }
        $config = Yii::$app->params['appSettings'];
        if (isset($config['mode']) && (int) $config['mode'] === AppConfigurationModeEnum::CONFIGURE_AND_USER_LOGIN) {
            $config['userIdentifier'] = $this->identifier;
        }
        $json = json_encode($config);
        echo $json;
        return Builder::create()->data($json)->build()->getDataUri();
    }

    /**
     * B(R)EAD - Determine if the model can be Read by the user.
     * To be overwritten in the model.
     * @return bool
     */
    public function readable()
    {
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        if (Yii::$app->hasModule('customer') && isset($this->client_id)) {
            return $this->hasOne(
                \taktwerk\yiiboilerplate\modules\customer\models\Client::class,
                ['id' => 'client_id']
            );
        }
        return null;
    }
    public function getUserGroups()
    {
        return $this->hasMany(UserGroup::class, ['user_id' => 'id']);
    }
    public function getGroups(){
        return $this->hasMany(Group::className(), ['id' => 'group_id'])
        ->via('userGroups');
    }
    public function getAssignedRoleItems()
    {
        return $this->id ? $this->make(Assignment::class, [], ['user_id' => $this->id])->items : "";
    }
    public function getControllerInstance()
    {
        if(class_exists('\taktwerk\yiiboilerplate\modules\usermanager\controllers\UserController')){

            return new \taktwerk\yiiboilerplate\modules\usermanager\controllers\UserController("user", \taktwerk\yiiboilerplate\modules\usermanager\Module::getInstance());
        }
    }

}
