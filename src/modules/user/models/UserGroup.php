<?php

namespace taktwerk\yiiboilerplate\modules\user\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\user\models\base\UserGroup as BaseUserGroup;

/**
 * This is the model class for table "user_group".
 */
class UserGroup extends BaseUserGroup
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['user_id', 'group_id'], 'unique', 'targetAttribute' => ['user_id', 'group_id'],
                'message'=>\Yii::t('twbp','This Group and User combination already exist')],
        ]);
    }
    public function duplicatable(){
        return false;
    }
}
