<?php


namespace taktwerk\yiiboilerplate\modules\user\models;

use yii\db\ActiveRecord;

class Session extends ActiveRecord
{
    public $id;
    public $user_id;
    public $expire;
    public $data;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'session';
    }
}
