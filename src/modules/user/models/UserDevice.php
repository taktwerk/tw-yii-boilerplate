<?php

namespace taktwerk\yiiboilerplate\modules\user\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\user\models\base\UserDevice as BaseUserDevice;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "feedback".
 */
class UserDevice extends BaseUserDevice
{

    public function getToString()
    {
        return $this->toString();
    }

    public function toString()
    {
        return $this->device_information;
    }

    public function getDeviceModel()
    {
        if (!$this->device_information) {
            return '';
        }
        $parsedDeviceInformation = json_decode($this->device_information);
        if (!$parsedDeviceInformation || !$parsedDeviceInformation->model) {
            return '';
        }

        return $parsedDeviceInformation->model;
    }

    public function getLastSyncProcess()
    {
        return $this->getSyncProcesses()->orderBy(['updated_at' => SORT_DESC])->one();
    }
}
