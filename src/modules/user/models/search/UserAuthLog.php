<?php

namespace taktwerk\yiiboilerplate\modules\user\models\search;

use taktwerk\yiiboilerplate\modules\user\models\search\base\UserAuthLog as UserAuthLogSearchModel;

/**
* UserAuthLog represents the model behind the search form about `taktwerk\yiiboilerplate\modules\user\models\UserAuthLog`.
*/
class UserAuthLog extends UserAuthLogSearchModel
{
	public $defaultOrder = ['date' => SORT_DESC];
}