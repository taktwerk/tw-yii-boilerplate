<?php
//Generation Date: 11-Sep-2020 02:47:27pm
namespace taktwerk\yiiboilerplate\modules\user\models\search;

use taktwerk\yiiboilerplate\modules\user\models\search\base\UserAuthCode as UserAuthCodeSearchModel;

/**
* UserAuthCode represents the model behind the search form about `taktwerk\yiiboilerplate\modules\user\models\UserAuthCode`.
*/
class UserAuthCode extends UserAuthCodeSearchModel{

}