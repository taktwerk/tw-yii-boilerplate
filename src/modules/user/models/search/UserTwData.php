<?php
//Generation Date: 11-Sep-2020 03:29:27pm
namespace taktwerk\yiiboilerplate\modules\user\models\search;

use taktwerk\yiiboilerplate\modules\user\models\search\base\UserTwData as UserTwDataSearchModel;

/**
* UserTwData represents the model behind the search form about `taktwerk\yiiboilerplate\modules\user\models\UserTwData`.
*/
class UserTwData extends UserTwDataSearchModel{

}