<?php
//Generation Date: 11-Sep-2020 02:47:28pm
namespace taktwerk\yiiboilerplate\modules\user\models\search;

use taktwerk\yiiboilerplate\modules\user\models\search\base\UserDevice as UserDeviceSearchModel;

/**
* UserDevice represents the model behind the search form about `taktwerk\yiiboilerplate\modules\user\models\UserDevice`.
*/
class UserDevice extends UserDeviceSearchModel{

}