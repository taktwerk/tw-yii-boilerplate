<?php
//Generation Date: 25-Jan-2021 10:26:46am
namespace taktwerk\yiiboilerplate\modules\user\models\search;

use taktwerk\yiiboilerplate\modules\user\models\search\base\GroupEmail as GroupEmailSearchModel;

/**
* GroupEmail represents the model behind the search form about `taktwerk\yiiboilerplate\modules\user\models\GroupEmail`.
*/
class GroupEmail extends GroupEmailSearchModel{

}