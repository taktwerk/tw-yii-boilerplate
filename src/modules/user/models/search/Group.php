<?php

namespace taktwerk\yiiboilerplate\modules\user\models\search;

use taktwerk\yiiboilerplate\modules\user\models\search\base\Group as GroupSearchModel;

/**
* Group represents the model behind the search form about `taktwerk\yiiboilerplate\modules\user\models\Group`.
*/
class Group extends GroupSearchModel{

}