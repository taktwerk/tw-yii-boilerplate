<?php

namespace taktwerk\yiiboilerplate\modules\user\models\search;

use taktwerk\yiiboilerplate\modules\user\models\search\base\UserGroup as UserGroupSearchModel;

/**
* UserGroup represents the model behind the search form about `taktwerk\yiiboilerplate\modules\user\models\UserGroup`.
*/
class UserGroup extends UserGroupSearchModel{

}