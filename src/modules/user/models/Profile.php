<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 5/10/2017
 * Time: 1:47 PM
 */
/**
 * @property string $picture
 */
namespace taktwerk\yiiboilerplate\modules\user\models;

use taktwerk\yiiboilerplate\modules\user\components\ProfileInterface;
use taktwerk\yiiboilerplate\traits\UploadTrait;

class Profile extends \Da\User\Model\Profile implements ProfileInterface
{

    /**
     * We can upload images and files to this model, so we need our helper trait.
     */
    use UploadTrait;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['picture'], 'string', 'max' => 255],
        ]);
    }

    /**
     * Load data into object
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;

        // Don't update these fields as they are handled separately.
        unset($data[$scope]['picture']);

        return parent::load($data,$formName);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(),[
            'picture' => \Yii::t('twbp', 'Picture')
        ]);
    }

    /**
     * Get unique upload path
     * @return string
     */
    protected function getUploadPath()
    {
        $reflect = new \ReflectionClass($this);
        return $this->uploadPath . $reflect->getShortName() . '/' . $this->user_id . '/';
    }

    public function setExternalProfile(array $attributes, $user = null, $attributesForSave = [])
    {
        if (empty($attributesForSave)) {
            return;
        }
        if ($user && $user->id) {
            $this->user_id = $user->id;
        }
        if (isset($attributes['name']) && in_array('name', $attributesForSave)) {
            $this->name = $attributes['name'];
        }
        if (isset($attributes['email']) && in_array('public_email', $attributesForSave)) {
            $this->public_email = $attributes['email'];
        }
    }

    public function uploadExternalFileToProfile(array $attributes, $user = null, $attributesForSave = [])
    {
        if (isset($attributes['picture']) && in_array('picture', $attributesForSave)) {
            $this->uploadByUrl('picture', $attributes['picture'], 'picture.png');
        }
    }



    //// TwActiveRecord methods
    //// TODO create trait for the some TWActiveRecord methods and use in TwActiveRecord and in Profile models
    /**
     * Extract comments from database
     *
     * @param
     *            $column
     * @return bool|mixed
     */
    public function extractComments($column)
    {
        $column = \Yii::$app->db->getTableSchema($this->tableName())->getColumn($column);
        $output = json_decode($column->comment);
        if (is_object($output)) {
            return $output;
        }
        return false;
    }

    public function checkIfCanvas($column)
    {
        $comment = $this->extractComments($column);
        if (preg_match('/(_canvas)$/i', $column) || ($comment && (@$comment->inputtype === 'canvas'))) {
            return true;
        }
        return false;
    }
}
