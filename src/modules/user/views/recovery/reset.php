<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View                      $this
 * @var yii\widgets\ActiveForm            $form
 * @var Da\User\Model\RecoveryForm $model
 */
$this->context->layout = '@taktwerk-boilerplate/modules/user/views/layouts/main';

if(Yii::$app->request->get('set')!='set'){
    $this->title = Yii::t('twbp', 'Reset your password');
}else{
    $this->title = Yii::t('twbp', 'Set your password');
}
$this->params['breadcrumbs'][] = $this->title;

taktwerk\yiiboilerplate\modules\user\assets\LoginAssets::register($this);
?>

<div class="login-wrapper">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Html::encode($this->title) ?></h3></div>
        <div class="panel-body">
            <div class="login-logo logo-image"></div>
            <?php
            if($message){
                echo \yii\bootstrap\Alert::widget([
                    'options' => ['class' => 'alert-success'],
                    'body' => $title,
                    'closeButton' => false,
                ]);
            }
            ?>
            <?php
            if(!$message){
                $form = ActiveForm::begin([
                    'id' => 'password-recovery-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                ]); ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= Html::submitButton(Yii::t('twbp', 'Finish'), ['class' => 'btn btn-success btn-block']) ?><br>

                <?php ActiveForm::end(); ?>
            <?php } ?>
        </div>
    </div>
</div>
