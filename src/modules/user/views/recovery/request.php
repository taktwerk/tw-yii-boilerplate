<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View                      $this
 * @var yii\widgets\ActiveForm            $form
 * @var Da\User\Model\RecoveryForm $model
 */
$this->context->layout = '@taktwerk-boilerplate/modules/user/views/layouts/main';
$this->title = Yii::t('twbp', 'Recover your password');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-wrapper">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="panel-body">
            <div class="login-logo logo-image"></div>
            <div class="row">
                <div class="col-xs-12">
                    <?php foreach (Yii::$app->session->getAllFlashes() as $type => $message) : ?>
                        <?php if (in_array($type, ['success', 'danger', 'warning', 'info'])) : ?>
                            <?= \yii\bootstrap\Alert::widget([
                                'options' => ['class' => 'alert-' . $type],
                                'body' => $message,
                                'closeButton' => false,
                            ]) ?>
                        <?php endif ?>
                    <?php endforeach ?>
                </div>
            </div>
            <?php $form = ActiveForm::begin([
                'id' => 'password-recovery-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
            ]); ?>

            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

            <?= Html::submitButton(Yii::t('twbp', 'Continue'), ['class' => 'btn btn-primary btn-block']) ?><br>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
