<?php

/**
 * Copyright (c) 2016.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var Da\User\Model\LoginForm $model
 * @var Da\User\Module $module
 */

$this->title = Yii::t('twbp', 'Sign in');
?>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'validateOnBlur' => false,
    'validateOnType' => false,
    'validateOnChange' => false,
]) ?>

<?= $form->field(
    $model,
    'login',
    [
        'inputOptions' => [
            'autofocus' => 'autofocus',
            'class' => 'form-control keyboard keyboard-numpad',
            'tabindex' => '1',
            'type' => 'tel',
            'id' => 'login-form-login'
        ],
    ]
) ?>

<?= $this->render('login-partials/number-pad') ?>

<?= Html::submitButton(
    Yii::t('twbp', 'Sign in'),
    ['class' => 'btn btn-primary btn-block', 'tabindex' => '3']
) ?>

<?php ActiveForm::end(); ?>
