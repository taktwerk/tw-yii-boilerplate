<style>
    #login-form-login {
        height: auto;
    }
    .number-button, button[type="submit"] {
        font-size: 2.5em;
    }
    .number-pad {
        width: 100%;
        display: inline-block;
        padding-right: 5px;
        max-height: calc(100% - 94px - 64px);
        min-height: 240px;
    }
    .number-button {
        display: inline-block;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        width: calc((100%/3) - 5px);
        height: calc((100% / 4) - 5px);
        margin-left: 5px;
        margin-bottom: 5px;
        float: right;
    }
</style>

<div class="number-pad">
    <button type="button" class="btn btn-default number-button" data-num="3"></button>
    <button type="button" class="btn btn-default number-button" data-num="2"></button>
    <button type="button" class="btn btn-default number-button" data-num="1"></button>
    <button type="button" class="btn btn-default number-button" data-num="6"></button>
    <button type="button" class="btn btn-default number-button" data-num="5"></button>
    <button type="button" class="btn btn-default number-button" data-num="4"></button>
    <button type="button" class="btn btn-default number-button" data-num="9"></button>
    <button type="button" class="btn btn-default number-button" data-num="8"></button>
    <button type="button" class="btn btn-default number-button" data-num="7"></button>
    <button type="button" class="btn btn-default number-button" data-num="DEL"></button>
    <button type="button" class="btn btn-default number-button" data-num="0"></button>
    <button type="button" class="btn btn-default number-button" data-num="."></button>
</div>
