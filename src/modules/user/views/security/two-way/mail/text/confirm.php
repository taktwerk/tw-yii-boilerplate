<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 7/26/2017
 * Time: 8:37 AM
 */

/**
 * @var Da\User\Model\User
 */
?>

<?= Yii::t('twbp', 'Dear {0}', $user) ?>, <br>
<?= Yii::t('twbp', 'Click on following link to complete login process') ?>: <br>
<?= \yii\helpers\Html::a(Yii::t('twbp', 'Click'), \taktwerk\yiiboilerplate\modules\user\models\UserAuthCode::getUrl($code)) ?><br><br>

<?= Yii::t('twbp', 'If you did not make this request you can ignore this email') ?>.
