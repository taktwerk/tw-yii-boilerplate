<?php

/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \taktwerk\yiiboilerplate\modules\user\models\UserAuthCode;

/**
 * @var yii\web\View                   $this
 * @var Da\User\Model\LoginForm $model
 * @var Da\User\Module           $module
 * @var string                         $qr
 */

$this->title = Yii::t('twbp', 'Sign in');


$user_id = $model->user->id;
$ga = new \taktwerk\yiiboilerplate\modules\user\models\twoway\lib\GoogleAuthenticator();
echo Html::img($ga->getQRCodeGoogleUrl($model->user->email, $qr, Yii::t('app', getenv('APP_TITLE'))), [
    'class' => 'qr-code-image',
    'style' => 'margin: auto;display: block;'
]);
?>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'action' => \yii\helpers\Url::toRoute('/user/security/check-code'),
    'enableAjaxValidation' => false,
]) ?>
<?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>
<?= $form->field(
    $model,
    'code',
    ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']]
) ?>

    <div class="alert alert-danger" style="display: none" id="info"></div>
<?= Html::submitButton(
    Yii::t('twbp', 'Sign in'),
    ['class' => 'btn btn-primary btn-block', 'tabindex' => '3']
) ?>

<?php
ActiveForm::end();
