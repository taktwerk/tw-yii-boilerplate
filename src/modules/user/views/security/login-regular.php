<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View                   $this
 * @var Da\User\Model\LoginForm $model
 * @var Da\User\Module           $module
 */

$this->title = Yii::t('twbp', 'Sign in');
?>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'enableAjaxValidation' => true,
    'validateOnBlur' => false,
    'validateOnType' => false,
    'validateOnChange' => false,
]) ?>

<?= $form->field(
    $model,
    'login',
    [
        'inputOptions' => [
            'autofocus' => 'autofocus',
            'class' => 'form-control',
            'tabindex' => '1'
        ],
    ]
) ?>

<?= $form
    ->field(
        $model,
        'password',
        ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']]
    )
    ->passwordInput()
    ->label(
        Yii::t('twbp', 'Password')
        . ($module->allowPasswordRecovery ?
            ' (' . Html::a(
                Yii::t('twbp', 'Forgot password?'),
                ['/user/recovery/request'],
                ['tabindex' => '5']
            )
            . ')' : '')
    ) ?>

<?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '4']) ?>

<?= Html::submitButton(
    Yii::t('twbp', 'Sign in'),
    ['class' => 'btn btn-primary btn-block', 'tabindex' => '3']
) ?>

<?php
ActiveForm::end();
