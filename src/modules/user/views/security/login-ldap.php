<?php

use dektrium\user\widgets\Connect;
use dektrium\user\models\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Alert;

/**
 * @var yii\web\View                   $this
 * @var Da\User\Model\LoginForm $model
 * @var Da\User\Module           $module
 */

$this->title = Yii::t('twbp', 'Sign in [LDAP]');
taktwerk\yiiboilerplate\assets\TwAsset::register($this);
taktwerk\yiiboilerplate\modules\user\assets\LoginAssets::register($this);


$this->context->layout = '@taktwerk-boilerplate/modules/user/views/layouts/main';

Yii::$app->getModule('user')->enableFlashMessages = true;
echo $this->render('/shared/_alert', ['module' => Yii::$app->getModule('user')]);
Yii::$app->getModule('user')->enableFlashMessages = false;
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="panel-body">
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
            'validateOnBlur' => false,
            'validateOnType' => false,
            'validateOnChange' => false,
        ]) ?>

<?= $form->field(
    $model,
    'login',
    ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']]
) ?>

<?= $form
    ->field(
        $model,
        'password',
        ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']]
    )
    ->passwordInput()
    ->label(
        Yii::t('twbp', 'Password')
    ) ?>

<?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '4']) ?>

<?= Html::submitButton(
    Yii::t('twbp', 'Sign in'),
    ['class' => 'btn btn-primary btn-block', 'tabindex' => '3']
) ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>
