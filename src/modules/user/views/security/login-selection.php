<?php

/**
 * Copyright (c) 2016.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 */

$this->title = Yii::t('twbp', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
\taktwerk\yiiboilerplate\modules\user\assets\LoginSelectionAssets::register($this);
?>

<div class="row">
    <div class="col-xs-12">
        <?= \yii\bootstrap\Alert::widget([
            'options' => [
                'class' => 'alert-dismissible alert-danger hidden'
            ],
            'body' => false,
            'id' => 'error-message',
        ]) ?>
    </div>
</div>

<style>
    .number-button, button[type="submit"] {
        font-size: 2.5em;
    }
    .number-pad {
        width: 100%;
        display: inline-block;
        padding-right: 5px;
    }
    .number-button {
        display: inline-block;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        width: 100%;
        margin-bottom: 5px;
        float: left;
        line-height: 100px;
        text-overflow: ellipsis;
        overflow: hidden;
    }
    .list-view {
        overflow-y: auto;
    }
</style>

<?= $dataProvider ? \yii\widgets\ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => "{items}",
    'itemView' => function ($model, $key, $index, $widget) {
        return Html::a(
            $model->username,
            '#',
            [
                'class' => 'btn btn-default number-button username-selection ml-0',
                'title' => $model->username,
                'data-id' => $key,
                'data-url' => (\yii\helpers\Url::current([], true)),
            ]
        );
    }
]) : ''; ?>
