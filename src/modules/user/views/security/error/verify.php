<?php
use dmstr\widgets\Alert;
use yii\helpers\Html;
use Da\User\Widget\ConnectWidget;
use Yii;

/* @var $this \yii\web\View */
/* @var $content string */
$this->title = 'Verification';
taktwerk\yiiboilerplate\assets\TwAsset::register($this);
taktwerk\yiiboilerplate\modules\user\assets\LoginAssets::register($this);


$this->context->layout = '@taktwerk-boilerplate/modules/user/views/layouts/main';
?>

<style>
    .panel-body {
        display: flex;
        flex-direction: column;
        height: calc(100% - 42px);
    }
    .login-form {
        overflow-y: auto;
        height: 100%;
        overflow-x: hidden;
        text-align: center;
        color: red;
    }
    .h-100 {
        height: 100%;
    }
    .login-wrapper {
        max-height: 100%;
        min-width: 310px;
        max-width: 600px;
        overflow-y: hidden;
        height: auto !important;
        flex-direction: row;
        display: flex;
    }
    .panel {
        width: 100%;
        margin-bottom: 0px;
    }
</style>

<div class="login-wrapper">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('app', getenv('APP_TITLE')); ?></h3>
        </div>
        <div class="panel-body">
            <div class="login-logo logo-image"></div>
            <div class="login-form">
            	<h3>
            		<?=$text?>
            	</h3>
            </div>
        </div>
    </div>
</div>

