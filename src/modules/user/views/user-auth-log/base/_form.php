<?php
//Generation Date: 30-Jun-2021 05:56:45am
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\widget\form\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\components\ClassDispenser;
/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\user\models\UserAuthLog $model
 * @var taktwerk\yiiboilerplate\widget\form\ActiveForm $form
 * @var boolean $useModal
 * @var boolean $multiple
 * @var array $pk
 * @var array $show
 * @var string $action
 * @var string $owner
 * @var array $languageCodes
 * @var boolean $relatedForm to know if this view is called from ajax related-form action. Since this is passing from
 * select2 (owner) it is always inherit from main form
 * @var string $relatedType type of related form, like above always passing from main form
 * @var string $owner string that representing id of select2 from which related-form action been called. Since we in
 * each new form appending "_related" for next opened form, it is always unique. In main form it is always ID without
 * anything appended
 */

$owner = $relatedForm ? '_' . $owner . '_related' : '';
$relatedTypeForm = Yii::$app->request->get('relatedType')?:$relatedTypeForm;

if (!isset($multiple)) {
$multiple = false;
}

if (!isset($relatedForm)) {
$relatedForm = false;
}

$tableHint = (!empty(taktwerk\yiiboilerplate\modules\user\models\UserAuthLog::tableHint()) ? '<div class="table-hint">' . taktwerk\yiiboilerplate\modules\user\models\UserAuthLog::tableHint() . '</div><hr />' : '<br />');

?>
<div class="user-auth-log-form">
        <?php  $formId = 'UserAuthLog' . ($ajax || $useModal ? '_ajax_' . $owner : ''); ?>    
    <?php $form = ActiveForm::begin([
        'id' => $formId,
        'layout' => 'default',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'action' => $useModal ? $action : '',
        'options' => [
        'name' => 'UserAuthLog',
            ],
    ]); ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>
        <?php echo $tableHint; ?>

        <?php if ($multiple) : ?>
            <?=Html::hiddenInput('update-multiple', true)?>
            <?php foreach ($pk as $id) :?>
                <?=Html::hiddenInput('pk[]', $id)?>
            <?php endforeach;?>
        <?php endif;?>

        <?php $fieldColumns = [
                
            'user_id' => 
            $form->field(
                $model,
                'user_id'
            )
                    ->widget(
                        Select2::class,
                        [
                            'data' => taktwerk\yiiboilerplate\modules\user\models\User::find()->count() > 50 ? null : ArrayHelper::map(taktwerk\yiiboilerplate\modules\user\models\User::find()->groupBy('{{%user}}.id')->all(), 'id', 'toString'),
                                    'initValueText' => taktwerk\yiiboilerplate\modules\user\models\User::find()->groupBy('{{%user}}.id')->count() > 50 ? \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\modules\user\models\User::find()->andWhere(['{{%user}}.id' => $model->user_id])->groupBy('{{%user}}.id')->all(), 'id', 'toString') : '',
                            'options' => [
                                'placeholder' => Yii::t('twbp', 'Select a value...'),
                                'id' => 'user_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                (taktwerk\yiiboilerplate\modules\user\models\User::find()->groupBy('{{%user}}.id')->count() > 50 ? 'minimumInputLength' : '') => 3,
                                (taktwerk\yiiboilerplate\modules\user\models\User::find()->groupBy('{{%user}}.id')->count() > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'User\', page:params.page || 1
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
                                        Url::toRoute('/usermanager/user/entry-details?id=', true) .
                                        "' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#user_id_well').html(json.data);
                                                //$('#user_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }",
                            ],
                            'addon' => (Yii::$app->getUser()->can('modules_user_create') && (!$relatedForm || ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL && !$useModal))) ? [
                                'append' => [
                                    'content' => [
                                        ClassDispenser::getMappedClass(RelatedForms::class)::widget([
                                            'relatedController' => '/usermanager/user',
                                            'type' => $relatedTypeForm,
                                            'selector' => 'user_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'primaryKey' => 'id',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    ) . '
                    
                
                <div id="user_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' . 
                    (taktwerk\yiiboilerplate\modules\user\models\User::find()->where(['{{%user}}.id' => $model->user_id])->one()
                        ->entryDetails == '' ?
                    ' display:none;' :
                    '') . '
                    ">' . 
                    (taktwerk\yiiboilerplate\modules\user\models\User::find()->where(['{{%user}}.id' => $model->user_id])->one()->entryDetails != '' ?
                    taktwerk\yiiboilerplate\modules\user\models\User::find()->where(['{{%user}}.id' => $model->user_id])->one()->entryDetails :
                    '') . ' 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>',
            'date' => 
            $form->field(
                $model,
                'date',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'date') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'type' => 'number',
                            'step' => '1',
                            'placeholder' => $model->getAttributePlaceholder('date'),
                            
                            'id' => Html::getInputId($model, 'date') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('date')),
            'cookie_based' => 
            $form->field(
                $model,
                'cookie_based',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'cookie_based') . $owner
                    ]
                ]
            )
                     ->checkbox(
                         [
                             'id' => Html::getInputId($model, 'cookie_based') . $owner
                         ]
                     )
                    ->hint($model->getAttributeHint('cookie_based')),
            'duration' => 
            $form->field(
                $model,
                'duration',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'duration') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'type' => 'number',
                            'step' => '1',
                            'placeholder' => $model->getAttributePlaceholder('duration'),
                            
                            'id' => Html::getInputId($model, 'duration') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('duration')),
            'error' => 
            $form->field(
                $model,
                'error',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'error') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('error'),
                            'id' => Html::getInputId($model, 'error') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('error')),
            'ip' => 
            $form->field(
                $model,
                'ip',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'ip') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('ip'),
                            'id' => Html::getInputId($model, 'ip') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('ip')),
            'host' => 
            $form->field(
                $model,
                'host',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'host') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('host'),
                            'id' => Html::getInputId($model, 'host') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('host')),
            'url' => 
            $form->field(
                $model,
                    'url',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'url') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'type' => 'url',
                            'placeholder' => $model->getAttributePlaceholder('url'),
                            'id' => Html::getInputId($model, 'url') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('url')),
            'user_agent' => 
            $form->field(
                $model,
                'user_agent',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'user_agent') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('user_agent'),
                            'id' => Html::getInputId($model, 'user_agent') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('user_agent')),
            'user_env' => 
            $form->field(
                $model,
                'user_env',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'user_env') . $owner
                    ]
                ]
            )
                    ->textarea(
                        [
                            'rows' => 6,
                            'placeholder' => $model->getAttributePlaceholder('user_env'),
                            'id' => Html::getInputId($model, 'user_env') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('user_env')),]; ?>        <div class='clearfix'></div>
<?php $twoColumnsForm = true; ?>
<?php 
// form fields overwrite
$fieldColumns = Yii::$app->controller->crudColumnsOverwrite($fieldColumns, 'form', $model, $form, $owner);

foreach($fieldColumns as $attribute => $fieldColumn) {
            if (count($model->primaryKey())>1 && in_array($attribute, $model->primaryKey()) && $model->checkIfHidden($attribute)) {
            echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
            echo $fieldColumn;
            echo $twoColumnsForm ? '</div>' : '';
        } elseif ($model->checkIfHidden($attribute) || (strpos($fieldColumn, 'input type="hidden"')!== false && strpos($fieldColumn, '<div') === false)) {
            echo $fieldColumn;
        } elseif (!$multiple || ($multiple && isset($show[$attribute]))) {
            if ((isset($show[$attribute]) && $show[$attribute]) || !isset($show[$attribute])) {
                echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
                echo $fieldColumn;
                echo $twoColumnsForm ? '</div>' : '';
            } else {
                echo $fieldColumn;
            }
        }
                
} ?><div class='clearfix'></div>

                <?php $this->endBlock(); ?>
        
        <?= ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_TAB) ?
            Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                    'label' => Yii::t('twbp', 'User Auth Log'),
                    'content' => $this->blocks['main'],
                    'active' => true,
                ],
                ]
            ])
            : $this->blocks['main']
        ?>        
        <div class="col-md-12">
        <hr/>
        
        </div>
        
        <div class="clearfix"></div>
        <?php echo $form->errorSummary($model); ?>
        <?php echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::formButtons($model, $useModal, $relatedForm, $multiple, "twbp", ['id' => $model->id]);?>
        <?php ClassDispenser::getMappedClass(ActiveForm::class)::end(); ?>
        <?= ($relatedForm && $relatedType == ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL) ?
        '<div class="clearfix"></div>' :
        ''
        ?>
        <div class="clearfix"></div>
    </div>
</div>

<?php
if(count($hiddenFieldsName)>0){
    $fName = $model->formName();
    $hideJs='';
    foreach($hiddenFieldsName as $name => $op){
        $pClass = $op['parent_class']?$op['parent_class']:'form-group-block';
        $hideJs .= <<<EOT
$('[name="{$fName}[{$name}]"]').parents('.{$pClass}:first').hide();

EOT;
    }
    if($hideJs){
        $this->registerJs($hideJs,\yii\web\View::POS_END);
    }
}
if ($useModal) {
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::class)::register($this);
}
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
if(!isset($useDependentFieldsJs) || $useDependentFieldsJs==true){
$this->render('@taktwerk-views/_dependent-fields', ['model' => $model,'formId'=>$formId]);
}
?>