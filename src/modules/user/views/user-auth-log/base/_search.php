<?php
//Generation Date: 30-Jun-2021 05:56:45am
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\user\models\search\UserAuthLog $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="user-auth-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'user_id') ?>

        <?= $form->field($model, 'date') ?>

        <?= $form->field($model, 'cookie_based') ?>

        <?= $form->field($model, 'duration') ?>

        <?php // echo $form->field($model, 'error') ?>

        <?php // echo $form->field($model, 'ip') ?>

        <?php // echo $form->field($model, 'host') ?>

        <?php // echo $form->field($model, 'url') ?>

        <?php // echo $form->field($model, 'user_agent') ?>

        <?php // echo $form->field($model, 'user_env') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
