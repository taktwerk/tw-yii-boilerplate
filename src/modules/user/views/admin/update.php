<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use Da\User\Model\User;
use yii\bootstrap\Nav;
use yii\web\View;
use yii\helpers\Html;

/**
 * @var View    $this
 * @var User    $user
 * @var string  $content
 */

$this->title = Yii::t('twbp', 'Update user account');
$this->params['breadcrumbs'][] = ['label' => Yii::t('twbp', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="clearfix"></div>
<?= $this->render(
    '/shared/_alert',
    [
        'module' => Yii::$app->getModule('user'),
    ]
) ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->render('/shared/_menu') ?>
                <div class="row">
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-body">

                                <?= Nav::widget([
                                    'options' => [
                                        'class' => 'nav-pills nav-stacked',
                                    ],
                                    'items' => [
                                        [
                                            'label' => Yii::t('twbp', 'Account details'),
                                            'url' => ['/user/admin/update', 'id' => $user->id]
                                        ],
                                        [
                                            'label' => Yii::t('twbp', 'Profile details'),
                                            'url' => ['/user/admin/update-profile', 'id' => $user->id],
                                            'visible' => Yii::$app->user->can('Authority')
                                        ],
                                        [
                                            'label' => Yii::t('twbp', 'Data'),
                                            'url' => ['/user/admin/tw-data', 'id' => $user->id],
                                            'visible' => Yii::$app->user->can('Authority')
                                        ],
                                        [
                                            'label' => Yii::t('twbp', 'Information'),
                                            'url' => ['/user/admin/info', 'id' => $user->id],
                                            'visible' => Yii::$app->user->can('Authority'),
                                        ],
                                        [
                                            'label' => Yii::t('twbp', 'Assignments'),
                                            'url' => ['/user/admin/assignments', 'id' => $user->id],
                                            'visible' => Yii::$app->user->can('Administrator')
                                        ],
                                        [
                                            'label' => Yii::t('twbp', 'Permissions'),
                                            'url' => ['/user/admin/permissions', 'id' => $user->id],
                                            'visible' => Yii::$app->user->can('Administrator')
                                        ],
                                        '<hr>',
                                        [
                                            'label' => Yii::t('twbp', 'Confirm'),
                                            'url'   => ['/user/admin/confirm', 'id' => $user->id],
                                            'visible' => !$user->isConfirmed && Yii::$app->user->can('Authority'),
                                            'linkOptions' => [
                                                'class' => 'text-success',
                                                'data-method' => 'post',
                                                'data-confirm' => Yii::t('twbp', 'Are you sure you want to confirm this user?'),
                                            ],
                                        ],
                                        [
                                            'label' => Yii::t('twbp', 'Block'),
                                            'url'   => ['/user/admin/block', 'id' => $user->id],
                                            'visible' => !$user->isBlocked && Yii::$app->user->can('Authority'),
                                            'linkOptions' => [
                                                'class' => 'text-danger',
                                                'data-method' => 'post',
                                                'data-confirm' => Yii::t('twbp', 'Are you sure you want to block this user?'),
                                            ],
                                        ],
                                        [
                                            'label' => Yii::t('twbp', 'Unblock'),
                                            'url'   => ['/user/admin/block', 'id' => $user->id],
                                            'visible' => $user->isBlocked && Yii::$app->user->can('Authority'),
                                            'linkOptions' => [
                                                'class' => 'text-success',
                                                'data-method' => 'post',
                                                'data-confirm' => Yii::t('twbp', 'Are you sure you want to unblock this user?'),
                                            ],
                                        ],
                                        [
                                            'label' => Yii::t('twbp', 'Delete'),
                                            'url'   => ['/user/admin/delete', 'id' => $user->id],
                                            'visible' => Yii::$app->user->can('Authority'),
                                            'linkOptions' => [
                                                'class' => 'text-danger',
                                                'data-method' => 'post',
                                                'data-confirm' => Yii::t('twbp', 'Are you sure you want to delete this user?'),
                                            ],
                                        ],
                                    ],
                                ]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <?= $content ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
