<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use Da\User\Model\UserSearch;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var UserSearch $searchModel
 */

$this->title = Yii::t('twbp', 'Manage users');
$this->params['breadcrumbs'][] = $this->title;
$this->context->layout = '@taktwerk-backend-views/layouts/box';

$module = Yii::$app->getModule('user');
?>

<?= $this->render('/shared/_alert', [
    'module' => Yii::$app->getModule('user'),
]) ?>

<?php $this->beginContent('@Da/User/resources/views/shared/admin_layout.php') ?>

<?php Pjax::begin() ?>

<?= GridView::widget([
    'dataProvider'  =>  $dataProvider,
    'filterModel'   =>  $searchModel,
    'layout'        =>  "{items}\n{pager}",
    'columns' => [
        [
            'attribute' => 'username',
            'value' => function($model) {
                return $model->toString();
            }
        ],
        [
            'attribute' => 'profile.picture',
            'format' => 'html',
            'value' => function ($model) {
                return !empty($model->profile->picture) ?
                    '<div class="datagrid-image">' . Html::img($model->profile->getFileUrl('picture')) . '</div>'
                    : '';
            }
        ],
        'email:email',
        [
            'header' => Yii::t('twbp', 'Roles'),
            'value' => function($model) {
                $roles = [];
                foreach (\Yii::$app->authManager->getRolesByUser($model->id) as $role) {
                    $roles[] = $role->name;
                }
                return implode(', ', $roles);
            }
        ],
        [
            'attribute' => 'created_at',
            'value' => function ($model) {
                if (extension_loaded('intl')) {
                    return Yii::t('twbp', '{0, date, MMMM dd, YYYY HH:mm}', [$model->created_at]);
                } else {
                    return date('Y-m-d G:i:s', $model->created_at);
                }
            },
        ],
        [
            'attribute' => 'last_login_at',
            'value' => function ($model) {
                if (!$model->last_login_at || $model->last_login_at == 0) {
                    return Yii::t('twbp', 'Never');
                } elseif (extension_loaded('intl')) {
                    return Yii::t('twbp', '{0, date, MMMM dd, YYYY HH:mm}', [$model->last_login_at]);
                } else {
                    return date('Y-m-d G:i:s', $model->last_login_at);
                }
            },
        ],
        [
            'header' => Yii::t('twbp', 'Confirmation'),
            'value' => function ($model) {
                if ($model->isConfirmed) {
                    return '<div class="text-center">
                                <span class="text-success">' . Yii::t('twbp', 'Confirmed') . '</span>
                            </div>';
                } else {
                    return Html::a(Yii::t('twbp', 'Confirm'), ['confirm', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-success btn-block',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('twbp', 'Are you sure you want to confirm this user?'),
                    ]);
                }
            },
            'format' => 'raw',
            'visible' => Yii::$app->getModule('user')->enableConfirmation,
        ],
        [
            'header' => Yii::t('twbp', 'Block status'),
            'value' => function ($model) {
                if ($model->isBlocked) {
                    return Html::a(Yii::t('twbp', 'Unblock'), ['block', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-success btn-block',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('twbp', 'Are you sure you want to unblock this user?'),
                    ]);
                } else {
                    return Html::a(Yii::t('twbp', 'Block'), ['block', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-danger btn-block',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('twbp', 'Are you sure you want to block this user?'),
                    ]);
                }
            },
            'format' => 'raw',
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete} {switch} {resend-recovery}',
            'buttons' => [
                'delete' => function ($url, $model, $key) {
                    if (YII_ENV == 'test' && !Yii::$app->user->can('Authority')) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('twbp', 'Delete'),
                            'aria-label' => Yii::t('twbp', 'Delete'),
                            'data-pjax' => '0',
                        ]);
                    }

                    return null;
                },
                'switch' => function ($url, $model) use ($module) {
                    if (Yii::$app->user->can('Authority') && $model->id != Yii::$app->user->id && $module->enableSwitchIdentities) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-user"></span>',
                            ['/user/admin/switch-identity', 'id' => $model->id],
                            [
                                'title' => Yii::t('twbp', 'Impersonate this user'),
                                'data-confirm' => Yii::t(
                                    'twbp',
                                    'Are you sure you want to switch to this user for the rest of this Session?'
                                ),
                                'data-method' => 'POST',
                            ]
                        );
                    }

                    return null;
                },
                'resend-recovery' => function ($url, $model) use ($module) {
                    if (Yii::$app->user->can('Authority')  && $model->id != Yii::$app->user->id) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-envelope"></span>',
                            ['/user/admin/resend-recovery', 'id' => $model->id],
                            [
                                'title' => Yii::t('twbp', 'Resend registration email'),
                                'data-confirm' => Yii::t(
                                    'twbp',
                                    'Are you sure you want to resend the email to this user for setting the password?'
                                ),
                                'data-method' => 'POST',
                            ]
                        );
                    }

                    return null;
                },
            ]
        ],
    ],
]); ?>

<?php Pjax::end() ?>

<?php $this->endContent() ?>
