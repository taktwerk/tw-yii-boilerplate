<?php

/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View                                            $this
 * @var \taktwerk\yiiboilerplate\modules\user\models\User       $user
 * @var \taktwerk\yiiboilerplate\modules\user\models\UserTwData $model
 */
$this->context->layout = '@taktwerk-backend-views/layouts/box';

$db = Yii::$app->authManager;
$perms = $db->getPermissions();
?>

<?php $this->beginContent('@taktwerk-boilerplate/modules/user/views/admin/update.php', ['user' => $user]) ?>

<table class="table">
    <thead>
    <tr>
        <th><?=Yii::t('twbp', 'Permission'); ?></th>
        <th><?=Yii::t('twbp', 'Description'); ?></th>
        <th><?=Yii::t('twbp', 'Access'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($perms as $perm): ?>
        <tr>
            <td><?=$perm->name?></td>
            <td><?=$perm->description?></td>
            <td><?=(Yii::$app->authManager->checkAccess($user->id, $perm->name) || $user->isAdmin ? '<i class="fa fa-check"></i>' : null)?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php $this->endContent() ?>
