<?php

/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use taktwerk\yiiboilerplate\modules\user\widgets\AssignmentsWidget;

/**
 * @var yii\web\View                $this
 * @var Da\User\Model\User   $user
 */
$this->context->layout = '@taktwerk-backend-views/layouts/box';
?>

<?php $this->beginContent('@taktwerk-boilerplate/modules/user/views/admin/update.php', ['user' => $user]) ?>

<?= yii\bootstrap\Alert::widget([
    'options' => [
        'class' => 'alert-info alert-dismissible',
    ],
    'body' => Yii::t('twbp', 'You can assign multiple roles or permissions to user by using the form below'),
]) ?>

<?= AssignmentsWidget::widget(['userId' => $user->id, 'params' => $params]) ?>

<?php $this->endContent() ?>
