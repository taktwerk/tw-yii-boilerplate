<?php

/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var yii\web\View
 * @var Da\User\Model\User
 */

$this->context->layout = '@taktwerk-backend-views/layouts/box';

use Da\User\Model\User;
use yii\web\View; ?>

<?php $this->beginContent('@taktwerk-boilerplate/modules/user/views/admin/update.php', ['user' => $user]) ?>

<table class="table">
    <tr>
        <td><strong><?= Yii::t('twbp', 'Registration time') ?>:</strong></td>
        <td><?= Yii::t('twbp', '{0, date, MMMM dd, YYYY HH:mm}', [$user->created_at]) ?></td>
    </tr>
    <?php if ($user->registration_ip !== null): ?>
        <tr>
            <td><strong><?= Yii::t('twbp', 'Registration Ip') ?>:</strong></td>
            <td><?= $user->registration_ip ?></td>
        </tr>
    <?php endif ?>
    <tr>
        <td><strong><?= Yii::t('twbp', 'Confirmation status') ?>:</strong></td>
        <?php if ($user->isConfirmed): ?>
            <td class="text-success"><?= Yii::t(
                    'usuario',
                    'Confirmed at {0, date, MMMM dd, YYYY HH:mm}',
                    [$user->confirmed_at]
                ) ?></td>
        <?php else: ?>
            <td class="text-danger"><?= Yii::t('twbp', 'Unconfirmed') ?></td>
        <?php endif ?>
    </tr>
    <tr>
        <td><strong><?= Yii::t('twbp', 'Last login') ?>:</strong></td>
        <td><?= Yii::t(
                'usuario',
                '{0, date, MMMM dd, YYYY HH:mm}',
                [$user->last_login_at]
            ) ?>
        </td>
    </tr>
    <tr>
        <td><strong><?= Yii::t('twbp', 'Block status') ?>:</strong></td>
        <?php if ($user->isBlocked): ?>
            <td class="text-danger"><?= Yii::t(
                    'usuario',
                    'Blocked at {0, date, MMMM dd, YYYY HH:mm}',
                    [$user->blocked_at]
                ) ?>
            </td>
        <?php else: ?>
            <td class="text-success"><?= Yii::t('twbp', 'Not blocked') ?></td>
        <?php endif ?>
    </tr>
</table>

<?php $this->endContent() ?>
