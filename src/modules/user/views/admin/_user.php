<?php

use yii\helpers\Html;

/**
 * @var yii\widgets\ActiveForm
 * @var \taktwerk\yiiboilerplate\modules\user\models\User $user
 */
?>

<?= $form->field($user, 'email')->textInput(['maxlength' => 255]) ?>
<?= $form->field($user, 'username')->textInput(['maxlength' => 255]) ?>
<?= $form->field($user, 'password')->passwordInput() ?>
<?= $user->isNewRecord?$form->field($user, 'password_email')->checkbox()->label(Yii::t('twbp','Send Email to user to set password')):'' ?>
<div style="text-align: end;">
    <?= Html::img($user->qrcode()) ?>
</div>

