<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use dmstr\widgets\Alert;
use yii\helpers\Html;
use taktwerk\yiiboilerplate\components\Helper;

/* @var $this \yii\web\View */
/* @var $content string */
$this->title = $this->title . ' [Login]';
$tw_asset = taktwerk\yiiboilerplate\assets\TwAsset::register($this);
Helper::registerWhiteLabelAssets();
taktwerk\yiiboilerplate\modules\user\assets\LoginAssets::register($this);
if (class_exists('\app\modules\backend\assets\AdminAsset')) {
    \app\modules\backend\assets\AdminAsset::register($this);
}


$this->render('@taktwerk-boilerplate/views/blocks/raven');?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="login">
<head>
    <meta charset="UTF-8">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Theme style -->
    <?php $this->head() ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition <?= Helper::getSkin(); ?> login background-image">
<?php $this->beginBody() ?>
<div class="wrapper login">
    <!-- Main content -->
    <section class="content login">
        <div class="col-xs-12 h-100">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 h-100">
                <?=\taktwerk\yiiboilerplate\components\Helper::getMaintenanceContent()?>
                <?= $content ?>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- ./wrapper -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
