<?php

/*
 * This file is part of the 2amigos/yii2-app project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use yii\bootstrap\Nav;

?>

<?= Nav::widget(
    [
        'options' => [
            'class' => 'nav-tabs',
            'style' => 'margin-bottom: 15px',
        ],
        'items' => [
            [
                'label' => Yii::t('twbp', 'Users'),
                'url' => ['/user/admin/index'],
            ],
            [
                'label' => Yii::t('twbp', 'Roles'),
                'url' => ['/user/role/index'],
            ],
            [
                'label' => Yii::t('twbp', 'Permissions'),
                'url' => ['/user/permission/index'],
            ],
            [
                'label' => Yii::t('twbp', 'Rules'),
                'url' => ['/user/rule/index'],
            ],
            [
                'label' => Yii::t('twbp', 'Create'),
                'items' => [
                    [
                        'label' => Yii::t('twbp', 'New user'),
                        'url' => ['/user/admin/create'],
                    ],
                    [
                        'label' => Yii::t('twbp', 'New role'),
                        'url' => ['/user/role/create'],
                    ],
                    [
                        'label' => Yii::t('twbp', 'New permission'),
                        'url' => ['/user/permission/create'],
                    ],
                    [
                        'label' => Yii::t('twbp', 'New rule'),
                        'url' => ['/user/rule/create'],
                    ],
                ],
            ],
        ],
    ]
) ?>
