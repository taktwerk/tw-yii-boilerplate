<?php

namespace taktwerk\yiiboilerplate\modules\user\plugins;

use Yii;

class LoginFlashMessage
{
    public static function getText()
    {
        if(\Yii::$app->session->hasFlash('loginSuccess')) {
            return '<div class="alert alert-success" role="alert">' .
                \Yii::$app->session->getFlash('loginSuccess') .
            '</div>';
        }
        if(\Yii::$app->session->hasFlash('loginError')) {
            return '<div class="alert alert-danger" role="alert">' .
                \Yii::$app->session->getFlash('loginError') .
            '</div>';
        }

        return '';
    }

    public static function show()
    {
        echo self::getText();
    }

    public static function save($type = 'success', $text)
    {
        $messageType = '';

        switch ($type) {
            case 'danger':
                $messageType = 'loginError';
                break;
            case 'success':
            default:
                $messageType = 'loginSuccess';
                break;
        }

        Yii::$app->session->setFlash($messageType, $text);
    }
}
