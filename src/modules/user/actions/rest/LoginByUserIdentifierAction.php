<?php

namespace taktwerk\yiiboilerplate\modules\user\actions\rest;

use taktwerk\yiiboilerplate\modules\user\models\User;
use taktwerk\yiiboilerplate\modules\user\helpers\ResponseHelper;
use yii\rest\Action;
use Yii;
use taktwerk\yiiboilerplate\rest\Token;
use yii\web\Response;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * Class LoginUsernameAction
 * @package taktwerk\yiiboilerplate\modules\user\actions\rest
 */
class LoginByUserIdentifierAction extends Action
{
    /**
     * Log in a user using just his username
     * @param $identifier
     * @return mixed
     */
    public function run()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $identifier = Yii::$app->request->get('user', null);
        $user = User::find()->andWhere(['identifier' => $identifier])
            ->andWhere('confirmed_at IS NOT NULL')
            ->one();

        // User was found, send back token
        if ($user) {
            if ($user->blocked_at) {
                Yii::$app->getResponse()->setStatusCode(400);
                return ['error' => 'User was blocked', 'blocked_user_id' => $user->id];
            }
            Yii::$app->getUser()->login($user);
            $allUserRoles = Yii::$app->getUser()->getAllRoles(true);
            if (!$allUserRoles || !in_array('Backend', $allUserRoles)) {
                Yii::$app->getResponse()->setStatusCode(400);
                return ['error' => 'The user has no rights to log in'];
            }
            /** @var Token $token */
            $token = Token::find()->where(['user_id' => $user->id])->one();

            // User has already a valid token
            if ($token === null || $token->isExpired) {
                $token =ClassDispenser::makeObject(Token::class);
                $token->user_id = $user->id;
                $token->type =  ClassDispenser::getMappedClass(Token::class)::TYPE_RECOVERY;
                if (!$token->save()) {
                    die("ERROR: " . print_r($token->getErrors(),true));
                }
            }

            Yii::$app->getResponse()->setStatusCode(200);
            $user->updateAttributes(['last_login_at' =>
                strtotime(\Yii::$app->formatter->asDatetime(time()))
            ]);
            return ClassDispenser::getMappedClass(ResponseHelper::class)::getSuccessLoginActionResponse($user, $token);
        }
        Yii::$app->getResponse()->setStatusCode(400);

        return ['error' => 'Could not authenticate user.'];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [];
    }
}
