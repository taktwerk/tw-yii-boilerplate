<?php


namespace taktwerk\yiiboilerplate\modules\user\actions\rest;

use taktwerk\yiiboilerplate\modules\customer\models\Client;
use taktwerk\yiiboilerplate\modules\customer\models\ClientDevice;
use taktwerk\yiiboilerplate\modules\user\helpers\ResponseHelper;
use yii\db\Expression;
use yii\rest\Action;
use taktwerk\yiiboilerplate\rest\Token;
use Yii;
use yii\web\Response;
use taktwerk\yiiboilerplate\components\ClassDispenser;

class LoginByClientIdentifierAction extends Action
{
    public function run()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $clientName = Yii::$app->request->get('client', null);
        $isLoginByDefaultUser = Yii::$app->request->get('is_login_by_default_user', null);
        $deviceKey = Yii::$app->request->get('device_key', null);
        $deviceName = Yii::$app->request->get('device_name', null);

        if (empty($clientName) or
            (empty($isLoginByDefaultUser) && empty($deviceKey))
        ) {
            return ['error' => Yii::t(
                'twbp',
                'Both the `client` and `device_key` parameters need to be set.'
            )];
        }

        $client = Client::findForLogin()->where(['identifier' => $clientName])->one();
        if (empty($client)) {
            return ['error' => Yii::t('twbp', 'Invalid Client Identifier')];
        }

        $clientUser = null;

        if (!$isLoginByDefaultUser) {
            $device = ClientDevice::findOne(['number' => $deviceKey, 'client_id' => $client->id]);
            if (empty($device)) {
                $device = new ClientDevice();
                $device->client_id = $client->id;
                $device->number = $deviceKey;
                $device->name = $deviceName;
            } elseif ($device->is_blocked) {
                return ['error' => Yii::t(
                    'twbp',
                    'Your device has been blocked from accessing this application.'
                )];
            }

            $device->last_used_at = new Expression('NOW()');
            // Skip validation because the client_id will never be found during the foreign
            // lookup, since it uses find() which will only look for clients that the user
            // has access to.
            $isSavedDeviceInfo = $device->save(false);
            if (!$isSavedDeviceInfo) {
                return ['error' => 'Could not authenticate user.'];
            }
            $clientUser = $device->clientUser;
        }
        // The device is attached to a user? Use that.
        // Get the client's default client user
        if (!$clientUser) {
            if (empty($client->defaultAutologin)) {
                return ['error' => 'Could not authenticate user.'];
            }
            // Just get the first client user
            $clientUser = $client->defaultAutologin;
        }

        $user = $clientUser->user;
        if (!$user) {
            return ['error' => 'Could not authenticate user.'];
        }
        if ($user->blocked_at) {
            Yii::$app->getResponse()->setStatusCode(400);
            return ['error' => 'User was blocked', 'blocked_user_id' => $user->id];
        }
        Yii::$app->getUser()->login($user);
        $allUserRoles = Yii::$app->getUser()->getAllRoles(true);
        if (!$allUserRoles || !in_array('Backend', $allUserRoles)) {
            Yii::$app->getResponse()->setStatusCode(400);
            return ['error' => 'The user has no rights to log in'];
        }
        $token = Token::find()->where(['user_id' => $user->id])->one();
        if ($token !== null && !$token->isExpired) {
            return ClassDispenser::getMappedClass(ResponseHelper::class)::getSuccessLoginActionResponse($user, $token);
        }
        $token = ClassDispenser::makeObject(Token::class);
        $token->user_id = $user->id;
        $token->type = ClassDispenser::getMappedClass(Token::class)::TYPE_RECOVERY;
        if (!$token->save()) {
            die("ERROR: " . print_r($token->getErrors(),true));
        }
        Yii::$app->getResponse()->setStatusCode(200);
        $user->updateAttributes(['last_login_at' =>
            strtotime(\Yii::$app->formatter->asDatetime(time()))
        ]);
        return ClassDispenser::getMappedClass(ResponseHelper::class)::getSuccessLoginActionResponse($user, $token);
    }
}
