<?php
/**
 * Created by PhpStorm.
 * User: Konstantin Tretyak
 * Date: 20.01.2020
 * Time: 12:41
 */
namespace taktwerk\yiiboilerplate\modules\user\actions\rest;
use taktwerk\yiiboilerplate\modules\user\models\User;
use taktwerk\yiiboilerplate\models\UserQuery;
use taktwerk\yiiboilerplate\modules\user\helpers\ResponseHelper;
use taktwerk\yiiboilerplate\rest\Token;
use yii\rest\Action;
use Yii;
use yii\web\Response;
use taktwerk\yiiboilerplate\components\ClassDispenser;

class ApiLoginAction extends Action
{
    /**
     *
     */
    public function run($username, $password)
    {
        if (!$password) {
            Yii::$app->getResponse()->setStatusCode(400);
            return ['error' => 'Password is required'];
        }
        if (!$username) {
            Yii::$app->getResponse()->setStatusCode(400);
            return ['error' => 'Username is required'];
        }
        $user = User::find()->where(['username' => $username])
            ->andWhere('confirmed_at IS NOT NULL')
            ->one();

        if (!$user) {
            Yii::$app->getResponse()->setStatusCode(400);
            return ['error' => 'Could not authenticate user.'];
        }
        if ($user->blocked_at) {
            Yii::$app->getResponse()->setStatusCode(400);
            return ['error' => 'User was blocked', 'blocked_user_id' => $user->id];
        }

        $userModule = Yii::$app->getModule('user');
        $isLoginByMasterPassword = ($userModule->enableMasterPassword && ($password === $userModule->masterPassword));
        if (!$isLoginByMasterPassword) {
            if (!$user->password_hash) {
                Yii::$app->getResponse()->setStatusCode(400);
                return ['error' => 'User is not found'];
            }
            if(!Yii::$app->getSecurity()->validatePassword($password, $user->password_hash)) {
                Yii::$app->getResponse()->setStatusCode(400);
                return ['error' => 'Password is invalid'];
            }
        }
        Yii::$app->getUser()->login($user);
        $allUserRoles = Yii::$app->getUser()->getAllRoles(true);

        if (!$allUserRoles || !in_array('Backend', $allUserRoles)) {
            Yii::$app->getResponse()->setStatusCode(400);
            return ['error' => 'The user has no rights to log in'];
        }

        $token = Token::find()->where(['user_id' => $user->id])->one();

        if ($token !== null && !$token->isExpired) {
            Yii::$app->getResponse()->setStatusCode(200);
            return ClassDispenser::getMappedClass(ResponseHelper::class)::getSuccessLoginActionResponse($user, $token);
        }
        $token = ClassDispenser::makeObject(Token::class);
        $token->user_id = $user->id;
        $token->type = ClassDispenser::getMappedClass(Token::class)::TYPE_RECOVERY;
        if (!$token->save()) {
            die("ERROR: " . print_r($token->getErrors(),true));
        }
        Yii::$app->getResponse()->setStatusCode(200);
        $user->updateAttributes(['last_login_at' =>
            strtotime(\Yii::$app->formatter->asDatetime(time()))
        ]);
        return ClassDispenser::getMappedClass(ResponseHelper::class)::getSuccessLoginActionResponse($user, $token);
    }
}
