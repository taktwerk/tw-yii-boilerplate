<?php


namespace taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory;

use Da\User\Event\FormEvent;
use taktwerk\yiiboilerplate\modules\user\forms\LoginForm;
use yii\base\Action;
use Yii;
use yii\widgets\ActiveForm;
use yii\web\Response;
use taktwerk\yiiboilerplate\modules\user\plugins\LoginFlashMessage;
use yii\web\ForbiddenHttpException;
use yii\helpers\Html;

abstract class LoginActionAbstractFactory extends Action
{
    protected $viewSection = 'login-regular';

    public function run()
    {
        $this->beforeStartAction();
        if (!Yii::$app->user->isGuest) {
            $this->controller->goHome();
        }
        $this->setOrigin();
        $form = $this->getForm();
        $event = $this->controller->make(FormEvent::class, [$form]);
        $isLoadedData = $form->load(Yii::$app->request->post());

        if ($isLoadedData) {
            $afterLoadDataResponse = $this->afterLoadData($form, $event);
            if ($afterLoadDataResponse) {
                return $afterLoadDataResponse;
            }
        }

        return $this->getPageResponse($form);
    }

    protected function beforeStartAction()
    {
        ///
    }

    protected function getForm()
    {
        return $this->controller->make(LoginForm::class);
    }

    protected function afterLoadData($form, $event)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return $this->afterLoadDataAjax($form, $event);
        }

        return $this->afterLoadDataWeb($form, $event);
    }

    protected function afterLoadDataAjax($form, $event)
    {
        return ActiveForm::validate($form);
    }

    protected function afterLoadDataWeb($form, $event)
    {
        return $this->loginUser($form, $event);
    }

    protected function loginUser($form, $event)
    {
        $this->controller->trigger(FormEvent::EVENT_BEFORE_LOGIN, $event);
        $loginSuccess = $form->login();
        if (!$loginSuccess) {
            return null;
        }
        if ($loginSuccess === "no-roles") {
            LoginFlashMessage::save('danger', Yii::t('twbp','No role is set for user'));
            throw new ForbiddenHttpException(Yii::t('twbp','No role is set for user'));
            return $this->controller->redirect('/site/error');
        }
        if ($loginSuccess === "no-backend"){
            LoginFlashMessage::save('danger', Yii::t('twbp','User has no access'));
            throw new ForbiddenHttpException(Yii::t('twbp','User has no access'));
            return $this->controller->redirect('/site/error');
        }
        if(property_exists($this->controller->module,'showLoginSuccessMsg') && $this->controller->module->showLoginSuccessMsg==true){
        LoginFlashMessage::save(
                    'success',
                    Html::a(
                        Yii::t(
                            'twbp',
                            'Login successful. Welcome! Click here to go to your dashboard.'
                        ),
                        '/backend'
                    ));
        }
        $this->controller->trigger(FormEvent::EVENT_AFTER_LOGIN, $event);

        $form->getUser()->updateAttributes(
            [
                'last_login_at' => strtotime(\Yii::$app->formatter->asDatetime(time()))
            ]
        );

        return $this->redirectAfterLogin();
    }

    protected function redirectAfterLogin()
    {
        if (!Yii::$app->getUser()) {
            return $this->controller->goBack();
        }

        /* $user_tw_data = Yii::$app->getUser()->identity->userTwData;
        if($user_tw_data && isset($user_tw_data->login_redirect)){
            return $this->controller->redirect($user_tw_data->login_redirect);
        } */

        $returnUrl = Yii::$app->getUser()->getReturnUrl(null);

        if (!$returnUrl ||
            preg_match('/(forgot|recover|reset)/', Yii::$app->getUser()->getReturnUrl($returnUrl))
        ) {
            return $this->controller->goHome();
        }

        return $this->controller->goBack();
    }

    protected function getPageResponse($form)
    {
        return $this->controller->render($this->controller->loginView, [
            'section' => $this->viewSection,
            'model' => $form,
            'module' => $this->controller->module,
        ]);
    }

    /**
     * Set in a cookie our origin
     * @param string $origin
     */
    protected function setOrigin($origin = null)
    {
        if ($this->controller->module->enableRememberLoginPage) {
            if (empty($origin)) {
                $origin = Yii::$app->request->url;
            }
            $cookieName = $this->controller->module->originCookieName;

            $options = [
                'name' => $cookieName,
                'value' => base64_encode($origin),
                'expire' => time() + 86400 * 365
            ];
            $cookie = new \yii\web\Cookie($options);
            Yii::$app->getResponse()->cookies->add($cookie);
        }
    }
}
