<?php


namespace taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory\implementations;

use taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory\LoginActionAbstractFactory;
use taktwerk\yiiboilerplate\modules\user\forms\LoginForm;
use yii\web\HttpException;

class LoginUsernameAction extends LoginActionAbstractFactory
{
    protected $viewSection = '@taktwerk/yiiboilerplate/modules/user/views/security/login-username';

    protected function beforeStartAction()
    {
        if ($this->controller->module->enableUsernameOnlyLogin !== true) {
            throw new HttpException(404);
        }
    }

    protected function getForm()
    {
        $form = $this->controller->make(LoginForm::class);
        $form->setScenario(LoginForm::LOGIN_USERNAME);

        return $form;
    }
}
