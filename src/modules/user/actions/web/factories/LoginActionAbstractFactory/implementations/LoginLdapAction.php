<?php


namespace taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory\implementations;

use taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory\LoginActionAbstractFactory;
use Yii;
use taktwerk\yiiboilerplate\modules\user\plugins\LoginFlashMessage;

class LoginLdapAction extends LoginActionAbstractFactory
{
    public $viewSection = '@taktwerk-boilerplate/modules/user/views/security/login-regular';

    protected function afterLoadDataWeb($form, $event)
    {
        try {
            if (!Yii::$app->ldap ||
                !Yii::$app->ldap->authenticate($form->login, $form->password)
            ) {
                LoginFlashMessage::save('danger', Yii::t('twbp','Can not login via Ldap'));
                throw new \Exception("Can not login via Ldap");
            }
            return $this->goHome();
        } catch (\Exception $e) {
            LoginFlashMessage::save('danger', $e->getMessage());
        }
    }
}
