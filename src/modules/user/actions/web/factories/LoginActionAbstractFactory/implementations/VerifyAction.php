<?php


namespace taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory\implementations;

use app\models\User;
use Da\User\Event\FormEvent;
use taktwerk\yiiboilerplate\modules\user\forms\LoginForm;
use Yii;
use taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory\LoginActionAbstractFactory;
use taktwerk\yiiboilerplate\modules\user\models\UserAuthCode;

class VerifyAction extends LoginActionAbstractFactory
{
    public function run()
    {
        if (!Yii::$app->user->isGuest) {
            $this->controller->goHome();
        }
        $this->setOrigin();

        $code = Yii::$app->request->get('code');

        if (!$code) {
            return $this->controller->render(
                '@taktwerk-boilerplate/modules/user/views/security/error/verify',
                ['text' => Yii::t('twbp', 'Wrong verification code')]
            );
        }

        $userAuthCode = UserAuthCode::find()
            ->andWhere(['code' => $code])
            ->one();

        if (!$userAuthCode) {
            return $this->controller->render(
                '@taktwerk-boilerplate/modules/user/views/security/error/verify',
                ['text' => Yii::t('twbp', 'Wrong verification code')]
            );
        }

        $userAuthCode->status = 1;
        $isVerified = $userAuthCode->save();

        if (!$isVerified) {
            return $this->controller->render(
                '@taktwerk-boilerplate/modules/user/views/security/error/verify',
                ['text' => Yii::t('twbp', 'Something went wrong')]
            );
        }

        $user = User::find()->where(['id' => $userAuthCode->user_id])->one();
        if (!$user) {
            return $this->controller->render(
                '@taktwerk-boilerplate/modules/user/views/security/error/verify',
                ['text' => Yii::t('twbp', 'User does not exist')]
            );
        }

        
        $form = $this->getForm();
        $form->setScenario(LoginForm::TWO_WAY);
        $event = $this->controller->make(FormEvent::class, [$form]);
        $isLoadedData = $form->load(['login' => $user->email]);

        if (!$isLoadedData) {
            return $this->controller->render(
                '@taktwerk-boilerplate/modules/user/views/security/error/verify',
                ['text' => Yii::t('twbp', 'Something went wrong')]
            );
        }
        $afterLoadDataResponse = $this->afterLoadData($form, $event);
        if ($afterLoadDataResponse) {
            return $afterLoadDataResponse;
        }

        return $this->controller->render(
            '@taktwerk-boilerplate/modules/user/views/security/error/verify',
            ['text' => Yii::t('twbp', 'Something went wrong')]
        );
    }
}
