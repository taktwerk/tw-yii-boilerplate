<?php


namespace taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory\implementations;

use taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory\LoginActionAbstractFactory;
use taktwerk\yiiboilerplate\modules\user\forms\LoginForm;
use yii\web\HttpException;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;
use Da\User\Event\FormEvent;
use Yii;

class LoginSelectionAction extends LoginActionAbstractFactory
{
    protected $viewSection = '@taktwerk/yiiboilerplate/modules/user/views/security/login-selection';

    protected function beforeStartAction()
    {
        if ($this->controller->module->enableSelectionLogin !== true) {
            throw new HttpException(404);
        }
    }

    protected function getForm()
    {
        $form = $this->controller->make(LoginForm::class);
        $form->setScenario(LoginForm::USER_SELECTION);

        return $form;
    }

    protected function afterLoadDataAjax($form, $event)
    {
        $parentAfterLoadDataAjaxResponse = parent::afterLoadDataAjax($form, $event);

        if ($parentAfterLoadDataAjaxResponse) {
            foreach ($parentAfterLoadDataAjaxResponse as $errorMessage) {
                return ['success' => false, 'message' => $errorMessage];
            }
        }
        $this->controller->trigger(FormEvent::EVENT_BEFORE_LOGIN, $event);
        $loginSuccess = $form->login();
        if ($loginSuccess){
            if ($loginSuccess === "no-roles"){
                return ['success' => false, 'message' => Yii::t('twbp','No role is set for user')];
            }
            if ($loginSuccess === "no-backend"){
                return ['success' => false, 'message' => Yii::t('twbp','User has no access')];
            }
            $this->controller->trigger(FormEvent::EVENT_AFTER_LOGIN, $event);

            $form->getUser()->updateAttributes(['last_login_at' => time()]);

            return['success' => true];
        }

        return [
            'success' => false,
            'message' => \Yii::t('twbp', 'There was an error logging in. Try again.')
        ];
    }

    protected function getPageResponse($form)
    {
        $query = User::find()->andWhere(['blocked_at' => null])->andWhere('confirmed_at IS NOT NULL');
        $dataProvider = new ActiveDataProvider(['query' => $query, 'pagination' => false]);

        return $this->controller->render($this->controller->loginView, [
            'section' => '@taktwerk/yiiboilerplate/modules/user/views/security/login-selection',
            'dataProvider' => $dataProvider,
        ]);
    }
}
