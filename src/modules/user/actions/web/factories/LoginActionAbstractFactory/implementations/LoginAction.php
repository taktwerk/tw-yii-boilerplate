<?php


namespace taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory\implementations;

use taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory\LoginActionAbstractFactory;
use taktwerk\yiiboilerplate\modules\user\models\TwoWay;

class LoginAction extends LoginActionAbstractFactory
{
    protected function afterLoadDataWeb($form, $event)
    {
        $twoWay = new TwoWay($form->login);
        if (!$twoWay->isEnabled()) {
            return $this->loginUser($form, $event);
        }
        if ($twoWay->validateUser()) {
            return $this->controller->render($this->controller->loginView, [
                'section' => $twoWay->view,
                'model' => $twoWay,
                'module' => $this->controller->module,
                'qr' => $twoWay->getCode(),
            ]);
        }
    }
}
