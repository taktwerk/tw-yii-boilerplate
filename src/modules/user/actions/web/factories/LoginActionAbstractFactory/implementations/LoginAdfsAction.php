<?php


namespace taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory\implementations;

use taktwerk\yiiboilerplate\modules\user\actions\web\factories\LoginActionAbstractFactory\LoginActionAbstractFactory;
use yii\web\HttpException;
use Yii;

class LoginAdfsAction extends LoginActionAbstractFactory
{
    public function run()
    {
        if (!Yii::$app->adfs) {
            throw new HttpException(404);
        }
        $auth0 = Yii::$app->adfs->getAuth0();
        $connection = getenv('AUTH0_CONNECTION') ? getenv('AUTH0_CONNECTION') : null;
        $auth0->login('STATE', $connection);
    }
}
