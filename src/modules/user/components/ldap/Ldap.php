<?php

namespace taktwerk\yiiboilerplate\modules\user\components\ldap;

use app\models\User;
use Da\User\Helper\SecurityHelper;
use Edvlerblog\Adldap2\Adldap2Wrapper;
use Yii;
use Adldap\Exceptions\AdldapException;


class Ldap extends Adldap2Wrapper
{

    /**
     * @var string class of user on app, which often equal to identityClass
     */
    public $userClass;

    /**
     * @var string class of profile on app, which need to implement AdfsProfileInterface
     */
    public $profileClass;

    /**
     * @var SecurityHelper
     */
    protected $securityHelper;

    public function __construct(SecurityHelper $securityHelper, $config = [])
    {
        parent::__construct($config);
        $this->securityHelper = $securityHelper;
    }

    public function init()
    {
        parent::init();

        if (!$this->profileClass) {
            throw new \Exception("property `profileClass` must be set");
        }

        $this->userClass = Yii::$app->user->identityClass;
    }


    /**
     * @param string $username
     * @param string $password
     * @return bool
     * @throws LdapLoginException
     */
    public function authenticate($username = '', $password = 'password')
    {
        // Authenticate user
        $auth = getenv('LDAP_LOGIN_ATTRIBUTE') ? getenv('LDAP_LOGIN_ATTRIBUTE') : 'cn';
        $dn = getenv('LDAP_DN') ? ',' . getenv('LDAP_DN') : '';

        // If we are login using something else than the cn, we need to get the cn first for the auth to work.
        if ($auth != 'cn') {
            $results = $this->search()->where($auth, '=', $username)->get();
            foreach ($results as $result) {
                $username = $result->getAttribute('cn')[0];
                continue;
            }
        }


        $authUsername = "cn=$username" . $dn;
        if ($this->auth()->attempt($authUsername, $password) === false) {
            throw new LdapLoginException(Yii::t('twbp', "Couldn't authenticate user {user}.", ['user' => $username]));
        }

        // Get the user info
        $results = $this->search()->where('cn', '=', $username)->get();

        foreach ($results as $result) {
            // Found the user, get his info
            $fullnameAttribute = getenv('LDAP_FULLNAME_ATTRIBUTE') ? getenv('LDAP_FULLNAME_ATTRIBUTE') : 'displayname';
            $fullname = $result->getAttribute($fullnameAttribute);
            if (is_array($fullname)) {
                $fullname = $fullname[0];
            }
            $email = $result->getAttribute('mail')[0];
            $group = $this->getGroupName($result->groupmembership);

            // Log user in
            $this->logUser($email, $fullname, $password, $group);

            return true;
        }

        return false;
    }

    /**
     * Log in a user
     * @param $email
     * @param string $fullname
     * @param string $password
     * @return string|bool $group
     * @throws LdapLoginException
     */
    protected function logUser($email, $fullname = '', $password = '', $group = '')
    {
        // Check if the user exists
        $identity = $this->userClass::findOne(['email' => $email]);

        if (empty($identity) && $group !== false) {
            // Set up the account
            $identity = $this->setupAccount($email, $fullname, $password, $group);
        }

        // Identity still null? Obviously we have an error and need to check what it is.
        if (empty($identity)) {
            throw new LdapLoginException(Yii::t('twbp', "Couldn't set up or connect account identity."));
        }

        // Log in the user
        Yii::$app->getUser()->login($identity);

        return true;
    }

    /**
     * Set up a new account
     *
     * @param $email
     * @param $fullname
     * @param string $password
     * @return mixed
     * @throws LdapLoginException
     */
    protected function setupAccount($email, $fullname, $password = '', $group = '')
    {
        // Account identity
        $username = str_replace(' ', '.', strtolower($fullname)); // Can't have spaces in usernames
        $password = $this->securityHelper->generatePassword(12); //!empty($password) ? Password::hash($password) : Password::generate(6);

        $user = Yii::createObject([
            'class' => $this->userClass,
            'scenario' => 'connect',
            'username' => $username,
            'email' => $email,
            'password_hash' => $password,
            'auth_key' => 'fake', // Is replaced in the beforeSave method.
        ]);

        if ($user->save(false)) {
            // LDAP users are by default Viewers.
            $role = Yii::$app->getAuthManager()->getRole($group);
            if ($role) {
                Yii::$app->getAuthManager()->assign($role, $user->id);
            }

            $user = User::findOne(['email' => $email]);
            $user->profile->name = $fullname;
            $user->profile->save();

            $currentProfile = $user->getCurrentProfile()->one();

            if (!$currentProfile) {
                $currentProfile = new $this->profileClass();//new Yii::$app->adfs->profileClass();
            }
            $currentProfile->firstname = $fullname;
            $currentProfile->save(false);
            $currentProfile->refresh();

            if ($currentProfile && $user->userTwData) {
                $userTwData = $user->userTwData;
                $userTwData->profile_id = $currentProfile->id;
                $userTwData->save(false);
            }

            return $user;
        } else {
            throw new LdapLoginException(Yii::t('twbp', "Couldn't create an account for {email}.", ['email' => $email]));
        }
    }


    /**
     * Get the default group name
     * @param $groups array
     * @return string
     */
    protected function getGroupName($groups = [])
    {
        if (empty($groups)) {
            return getenv('LDAP_DEFAULT_ROLE');
        }

        $mappings = $this->getRoleMapping();
        foreach ($groups as $group) {
            $segments = explode(',', $group);
            $group = str_replace('cn=', '', $segments[0]);

            if (!empty($mappings[$group])) {
                return $mappings[$group];
            }
        }

        return getenv('LDAP_DEFAULT_ROLE');
    }

    protected function getRoleMapping()
    {
        return [
            'admin_staff' => 'Administrator',
            'ship_crew' => 'Viewer',
        ];
    }
}
