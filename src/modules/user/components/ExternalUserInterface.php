<?php


namespace taktwerk\yiiboilerplate\modules\user\components;


interface ExternalUserInterface
{
    public function fillProfile(ProfileInterface $profile);
}
