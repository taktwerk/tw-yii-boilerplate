<?php


namespace taktwerk\yiiboilerplate\modules\user\components;


interface ProfileInterface
{
    public function setExternalProfile(array $attributes, $user = null, $attributesForUpdate = []);
    public function uploadExternalFileToProfile(array $attributes, $user = null, $attributesForSave = []);
}
