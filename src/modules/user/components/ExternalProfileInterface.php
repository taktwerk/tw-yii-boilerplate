<?php


namespace taktwerk\yiiboilerplate\modules\user\components;


interface ExternalProfileInterface
{
    public function getGivenNameAttribute();

    public function getFamilyNameAttribute();

    public function getEmailAttribute();

    public function getPhoneAttribute();

    public function getStreetAttribute();

    public function getZipAttribute();

    public function getCityAttribute();

    public function getFaxAttribute();

}