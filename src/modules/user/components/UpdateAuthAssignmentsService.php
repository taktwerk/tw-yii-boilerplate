<?php 
namespace taktwerk\yiiboilerplate\modules\user\components;

use Da\User\Service\UpdateAuthAssignmentsService as UAAS;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use taktwerk\yiiboilerplate\modules\sync\commands\SyncIndexUserJob;
use taktwerk\yiiboilerplate\modules\user\models\User;

class UpdateAuthAssignmentsService extends UAAS{
    
    public function run()
    {
        $return = parent::run();
        if($return && \Yii::$app->hasModule('sync')){
            if(is_array(\Yii::$app->getModule('sync')->models) && count(\Yii::$app->getModule('sync')->models)>0){
            $job = new QueueJob();
            $modelTitle = \yii\helpers\StringHelper::basename(get_class($this));
            $job->queue("Sync Indexing for ".$modelTitle." ".$this->model->user_id,
                ClassDispenser::getMappedClass(SyncIndexUserJob::class), [
                    'model' => User::class,
                    'id' => $this->model->user_id
                ], true);
            }
        }
        return $return;
    }
    protected function jobExists(){
        $query = QueueJob::find()->andWhere([
            'command' => ClassDispenser::getMappedClass(SyncIndexUserJob::class),
            'parameters' => json_encode([
                'model' => User::class,
                'id' => $this->model->user_id,
            ]),
            'status' => QueueJob::STATUS_QUEUED
        ]);
        return $query->exists();
    }
}