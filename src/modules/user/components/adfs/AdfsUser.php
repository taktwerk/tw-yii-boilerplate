<?php


namespace taktwerk\yiiboilerplate\modules\user\components\adfs;

use Auth0\SDK\Auth0;
use taktwerk\yiiboilerplate\modules\user\components\ProfileInterface;
use Yii;
use yii\base\UnknownPropertyException;

class AdfsUser
{
    public $sub;

    public $given_name;

    public $family_name;

    public $nickname;

    public $name;

    public $picture;

    public $groups;

    public $phone;

    public $street;

    public $zip;

    public $city;

    public $updated_at;

    public $email;

    public $email_verified;

    public $client_id;
    
    private $_divisionAtr=[];
    
    protected $attributes = [];

    /**
     * @var string prepend for custom properties in auth0
     */
    protected $claim_domain;

    protected $fillDataMode = AdfsFillDataEnum::ALLWAYS;

    /**
     * @var Auth0
     */
    private $auth0;

    /**
     * AdfsUser constructor.
     * @param Auth0 $auth0
     * @throws \Auth0\SDK\Exception\ApiException
     * @throws \Auth0\SDK\Exception\CoreException
     */
    public function __construct(Auth0 $auth0)
    {
        $this->claim_domain = getenv('AUTH0_CLAIM_DOMAIN') ? getenv('AUTH0_CLAIM_DOMAIN') : '';
        $this->fillDataMode = Yii::$app->adfs->fillDataMode;
        $this->auth0 = $auth0;

        $userInfo = $this->auth0->getUser();
        $divAttrName = getenv('DIVISION_ATTRIBUTE_NAME');
        if(strlen($divAttrName)>0){
            $this->_divisionAtr[$divAttrName] = NULL;
        }
        $hasDivProperty = false;
        if ($userInfo) {
            foreach ($userInfo as $key => $value) {
                $property = str_replace($this->claim_domain, '', $key);
                if($hasDivProperty==false && $property && $divAttrName && $property==$divAttrName){
                    $hasDivProperty = true;
                }
                if ($this->hasProperty($property)){
                    $this->$property = $value;
                    $this->attributes[$property] = $value;
                }
            }
            $this->attributes = $this->removeSkipAttributes($this->attributes);
        }
        //If the division attribute is not provided from the adfs then we set the default value for the divison
        if($hasDivProperty==false && $divAttrName){
            $this->{$divAttrName} = getenv('DIVISION_ATTRIBUTE_DEFAULT_VALUE');
        }
    }
    public function __get($name)
    {
        if (isset($this->_divisionAtr[$name]) || array_key_exists($name, $this->_divisionAtr)) {
            return $this->_divisionAtr[$name];
        }
        
        throw new UnknownPropertyException('Getting unknown property: ' . get_class($this) . '::' . $name);
    }

    /**
     * @return bool
     */
    public function doesExist()
    {
        return ($this->sub) ? true : false;
    }


    /**
     * @return string|null
     */
    public function getEmail()
    {
        if (!$this->doesExist()) {
            return null;
        }

        if ($this->email) {
            return $this->email;
        }

        $subArray = explode('|', $this->sub);
        if (isset($subArray[2])) {
            return $subArray[2];
        }
        return null;
    }

    public function getFullname()
    {
        if (!$this->doesExist()) {
            return null;
        }

        if ($this->given_name && $this->family_name) {
            return $this->given_name . ' ' . $this->family_name;
        }
        else {
            return $this->name;
        }

        return null;
    }

    /**
     * @param ProfileInterface $profile
     * @return ProfileInterface|null
     */
    public function fillProfile(ProfileInterface $profile, $user = null, $isCreate = false)
    {
        $isNewProfile = $isCreate;
        if ($this->fillDataMode === AdfsFillDataEnum::WHEN_CREATE && !$isNewProfile) {
            return null;
        }
        if (!$this->doesExist()) {
            return null;
        }

        $attributes = $this->attributes;
        $attributesForSave = [];
        if ($this->fillDataMode === AdfsFillDataEnum::WHEN_EMPTY_FIELD && !$isNewProfile) {
            foreach ($profile->getAttributes() as $key => $value) {
                if (null === $value || '' === $value) {
                    $attributesForSave[] = $key;
                }
            }
        } else {
            $attributesForSave = $profile->attributes();
        }

        if (isset($attributes['email'])){
            $attributes['email'] = $this->getEmail();
        }
        $profile->setExternalProfile($attributes, $user, $attributesForSave);

        return $profile;
    }

    public function uploadFileToProfile(ProfileInterface $profile, $user = null, $isCreate = false)
    {
        $isNewProfile = $isCreate;
        if ($this->fillDataMode === AdfsFillDataEnum::WHEN_CREATE && !$isNewProfile) {
            return null;
        }
        if (!$this->doesExist()) {
            return null;
        }

        $attributes = $this->attributes;
        $attributesForSave = [];

        if ($this->fillDataMode === AdfsFillDataEnum::WHEN_EMPTY_FIELD && !$isNewProfile) {
            foreach ($profile->getAttributes() as $key => $value) {
                if (null === $value || '' === $value) {
                    $attributesForSave[] = $key;
                }
            }
        } else {
            $attributesForSave = $profile->attributes();
        }

        $profile->uploadExternalFileToProfile($attributes, $user, $attributesForSave);

        return $profile;
    }
    public function removeSkipAttributes($attributes){
        if(getenv('ADFS_PROFILE_FILL_SKIP_ATTRIBUTES')){
            $skipAttributes = explode(',', getenv('ADFS_PROFILE_FILL_SKIP_ATTRIBUTES'));
            foreach ($skipAttributes as $skipAtr){
                unset($attributes[trim($skipAtr)]);
            }
        }
        return $attributes;
    }
    public function hasProperty($name){ 
        if (property_exists($this, $name) || array_key_exists($name,$this->_divisionAtr)) {
            return true;
        }
        return false;
    }
}
