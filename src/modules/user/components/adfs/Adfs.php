<?php


namespace taktwerk\yiiboilerplate\modules\user\components\adfs;


use Auth0\SDK\Auth0;
use Auth0\SDK\API\Authentication;
use Firebase\JWT\JWT;
use taktwerk\yiiboilerplate\modules\user\components\ProfileInterface;
use taktwerk\yiiboilerplate\modules\user\models\Profile;
use yii\base\Component;
use yii\helpers\Url;
use taktwerk\yiiboilerplate\modules\user\components\ExternalProfileInterface;
use taktwerk\yiiboilerplate\modules\user\models\User;
use Yii;

class Adfs extends Component
{
    /**
     * @var string class of profile on app, which need to implement ExternalProfileInterface
     */
    public $profileClass;

    /**
     * @var string class of user on app, which often equal to identityClass
     */
    public $userClass;
    public $overwriteClientId = OverwrightClientIdEnum::ALLWAYS;
    public $fillDataMode = AdfsFillDataEnum::ALLWAYS;

    private $domain;
    private $client_id;
    private $client_secret;
    private $redirect_uri;
    private $audience;
    private $scope = 'openid profile email phone';

    /**
     * @var Auth0
     */
    private $auth0;

    public function init()
    {
        parent::init();
        if(!$this->userClass){
        $this->userClass = User::class;
        }
        if (!$this->profileClass) {
            $this->profileClass = Profile::class;
        }
        
        $interfaces = class_implements($this->profileClass);

        if (!isset($interfaces[ProfileInterface::class])) {
            throw new \Exception("property `profileClass` must be set and implemented ProfileInterface");
        }

        $this->domain = getenv('AUTH0_DOMAIN');
        $this->client_id = getenv('AUTH0_CLIENT_ID');
        $this->client_secret = getenv('AUTH0_CLIENT_SECRET');
        $this->redirect_uri = getenv('AUTH0_CALLBACK_URL');
        $this->audience = getenv('AUTH0_AUDIENCE');

        if ($this->audience == '') {
            $this->audience = 'https://' . $this->domain . '/userinfo';
        }

        /**
         * If you handle next exception:
         * @see https://github.com/auth0/auth0-PHP/issues/56
         *
         * You can add a leeway to account for when there is a clock skew times between
         * the signing and verifying servers. It is recommended that this leeway should
         * not be bigger than a few minutes.
         */
        JWT::$leeway = 60; // $leeway in seconds (uncomment if throwing Exception)

        $this->auth0 = new Auth0([
            // The values below are found on the Application settings tab.
            'domain' => $this->domain,
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
            'audience' => $this->audience,

            // This is your application URL that will be used to process the login.
            // Save this URL in the "Allowed Callback URLs" field on the Application settings tab
            'redirect_uri' => $this->redirect_uri,
            'scope' => $this->scope,
            'persist_id_token' => true,
            'persist_access_token' => true,
            'persist_refresh_token' => true,
        ]);
    }

    /**
     * @return Auth0
     */
    public function getAuth0()
    {
        return $this->auth0;
    }

    /**
     * @return Authentication
     */
    public function getAuthentication()
    {
        return new Authentication($this->domain, $this->client_id, $this->client_secret, $this->audience, $this->scope);
    }

    /**
     * Get Auth0 logout url
     * @return string
     */
    public function getLogoutUrl()
    {
        $authentication = $this->getAuthentication();
        $returnUrl = getenv('AUTH0_LOGOUT_RETURN_URL') ?
            getenv('AUTH0_LOGOUT_RETURN_URL') :
            Url::base(true);
        
        return $authentication->get_logout_link($returnUrl, $this->client_id, true);
    }
}
