<?php

namespace taktwerk\yiiboilerplate\modules\user\components\adfs;

use Da\User\Helper\SecurityHelper;
use taktwerk\yiiboilerplate\modules\customer\models\base\Client;
use taktwerk\yiiboilerplate\modules\user\components\ProfileInterface;
use yii\base\Model;
use Exception;
use Yii;

class LoginAdfsForm extends Model
{
    const EVENT_IDENTITY_RETRIEVED = 'IDENTITY_RETRIEVED';
    
    /**
     * @var AdfsUser|null
     */
    private $_adfsUser;

    protected $overwriteClientId = OverwrightClientIdEnum::ALLWAYS;
    protected $fillDataMode = AdfsFillDataEnum::ALLWAYS;

    /**
     * @var string
     */
    private $_userClass;

    public function __construct($config = [])
    {
        $auth0 = Yii::$app->adfs->getAuth0();
        $this->fillDataMode = Yii::$app->adfs->fillDataMode;
        $this->overwriteClientId = Yii::$app->adfs->overwriteClientId;
        $this->_adfsUser = new AdfsUser($auth0);
        $this->_userClass = Yii::$app->adfs->userClass;
        
        parent::__construct($config);
    }

    public function login()
    {
        if (!$this->_adfsUser->doesExist()) {
            throw new Exception(Yii::t('twbp', 'User does not exist on ADFS server'));
        }

        $identity = $this->getIdentity();
        
        if (!$identity) {
            throw new Exception(Yii::t('usuario', 'Invalid login or password'));
        }

        $this->trigger(self::EVENT_IDENTITY_RETRIEVED);
        
        return Yii::$app->user->login($identity);
    }

    private function getIdentity()
    {
        $email = $this->_adfsUser->getEmail();
        $identity = $this->findUserByEmail($email);

        // development hack, while using +import emails
        if (!$identity) {
            $identity = $this->findUserByEmail(str_replace('@', '+import@', $email));
        }

        // domain mapping
        if (!$identity) {
            $identity = $this->findUserByEmail(str_replace('@sg.hcare.ch', '@kssg.ch', $email));
        }

        // hack + mapping
        if (!$identity) {
            $identity = $this->findUserByEmail(str_replace(['@', '@sg.hcare.ch'], ['+import@', '@kssg.ch'], $email));
        }
        
        if (!$identity) {
            $identity = $this->setupAccountByAdfs();
        } elseif ($this->fillDataMode !== AdfsFillDataEnum::WHEN_CREATE) {
            $this->updateAccountByAdfs($identity);
        }

        return $identity;
    }

    /**
     * @param string $email
     * @return mixed
     */
    private function findUserByEmail(string $email)
    {
        return $this->_userClass::findOne(['email' => $email]);
    }

    /**
     * Create new user account based on Adfs data
     *
     * @return object
     * @throws \yii\base\InvalidConfigException
     */
    protected function setupAccountByAdfs()
    {
        $securityHelper = new SecurityHelper(Yii::$app->getSecurity());

        // Account identity
        $password = $securityHelper->generatePassword(12); //!empty($password) ? Password::hash($password) : Password::generate(6);

        $user = Yii::createObject([
            'class' => $this->_userClass,
            'scenario' => 'connect',
            'username' => $this->_adfsUser->getEmail(),
            'email' => $this->_adfsUser->getEmail(),
            'fullname' => $this->_adfsUser->getFullname(),
            'password_hash' => $password,
            'auth_key' => 'fake', // Is replaced in the beforeSave method.
        ]);
        
        $user = new $this->_userClass(['scenario'=>'connect']);
        $user->username = $this->_adfsUser->getEmail();
        $user->email = $this->_adfsUser->getEmail();
        $user->fullname = $this->_adfsUser->getFullname();
        $user->password_hash = $password;
        $user->auth_key = 'fake';
        if ($this->_adfsUser->client_id &&
            !($this->overwriteClientId === OverwrightClientIdEnum::ALLWAYS || $this->overwriteClientId === OverwrightClientIdEnum::WHEN_FIRST_LOGIN) &&
            Client::find()->where(['id' => $this->_adfsUser->client_id])->exists()
        ) {
            $user->client_id = $this->_adfsUser->client_id;
        } else if (strlen(getenv('CLIENT_ID'))>0) {
            $user->client_id = getenv('CLIENT_ID');
        }
        
        $isExistUserWithUsername = (boolean) $this->_userClass::find()
                        ->where(['username' => $user->username])
                        ->count();
        if ($isExistUserWithUsername) {
            throw new Exception(Yii::t('twbp', 'User with this username already exists'));
        }
        if ($user->save(false)) {
            $user->refresh();
            $this->assignRoleToUser($user);
            $user->refresh();
            // create profile
            $userTwData = $user->userTwData;
            if ($userTwData && $userTwData->hasProperty('profile_id')) {
                $profile = Yii::$app->adfs->profileClass::findOne(
                    [Yii::$app->adfs->profileClass::primaryKey()[0] => $userTwData->profile_id]
                );
            } else {
                $profile = Yii::$app->adfs->profileClass::findOne(
                    [Yii::$app->adfs->profileClass::primaryKey()[0] => $user->id]
                );
            }
            if (!$profile || !$profile->{$profile::primaryKey()[0]}) {
                $profile = new Yii::$app->adfs->profileClass();
            }
            $profile = $this->createOrUpdateProfile($profile, $user, true);
            if ($profile && $user->userTwData) {
                $userTwData = $user->userTwData;
                $profilePrimaryKey = $profile::primaryKey()[0];
                if ($userTwData->hasProperty('profile_id')) {
                    $userTwData->profile_id = $profile->{$profilePrimaryKey};
                }
                $userTwData->save(false);
            }

            return $user;
        } else {
            throw new Exception(
                Yii::t('twbp', "Couldn't create an account for {email}.",
                ['email' => $this->_adfsUser->getEmail()])
            );
        }
    }

    /**
     * Update existing user account based on Adfs data
     *
     * @param $user
     * @return ProfileInterface|null
     * @throws Exception
     */
    protected function updateAccountByAdfs($user)
    {
        $this->assignRoleToUser($user);

        // update user and default profile
        $userUpdateAttributes = [];
        if ($this->fillDataMode === AdfsFillDataEnum::WHEN_EMPTY_FIELD) {
            if (null === $user->username || '' === $user->username) {
                $userUpdateAttributes['username'] = $this->_adfsUser->getEmail();
            }
            if (null === $user->email || '' === $user->email) {
                $userUpdateAttributes['email'] = $this->_adfsUser->getEmail();
            }
            if (null === $user->fullname || '' === $user->fullname) {
                $userUpdateAttributes['fullname'] = $this->_adfsUser->getFullname();
            }
        } else {
            $userUpdateAttributes = [
                'username' => $this->_adfsUser->getEmail(),
                'email' => $this->_adfsUser->getEmail(),
                'fullname' => $this->_adfsUser->getFullname(),
            ];
        }
        $isExistUserWithUsername = (boolean) $this->_userClass::find()
                        ->where(['username' => $userUpdateAttributes['username']])
                        ->andWhere(['<>','email', $userUpdateAttributes['email']])
                        ->count();

        if ($isExistUserWithUsername) {
            throw new Exception(Yii::t('twbp', 'User with this username already exists'));
        }
        if(!$user->client_id){
            if ($this->_adfsUser->client_id &&
                !($this->overwriteClientId === OverwrightClientIdEnum::ALLWAYS)
            ) {
                $userUpdateAttributes['client_id'] = $this->_adfsUser->client_id;
                if (!Client::find()->where(['id' => $this->_adfsUser->client_id])->exists()) {
                    $userUpdateAttributes['client_id'] = getenv('CLIENT_ID');
                }
            } else if (
                strlen(getenv('CLIENT_ID')) > 0 &&
                ($this->overwriteClientId === OverwrightClientIdEnum::ALLWAYS || $this->overwriteClientId === OverwrightClientIdEnum::WHEN_EMPTY_FIELD)
            ) {
                $userUpdateAttributes['client_id'] = getenv('CLIENT_ID');
            }
        }
        $userUpdateAttributes['last_login_at'] = time();
        $user->updateAttributes($userUpdateAttributes);

        $currentProfile = null;
        $userTwData = $user->getUserTwData()->one();
        if ($userTwData && $userTwData->hasProperty('profile_id')) {
            $currentProfile = Yii::$app->adfs->profileClass::findOne(
                [Yii::$app->adfs->profileClass::primaryKey()[0] => $userTwData->profile_id]
            );
        } else {
            $currentProfile = Yii::$app->adfs->profileClass::findOne(
                [Yii::$app->adfs->profileClass::primaryKey()[0] => $user->id]
            );
        }
        if (!$currentProfile || !$currentProfile->{$currentProfile::primaryKey()[0]}) {
            $currentProfile = new Yii::$app->adfs->profileClass();
        }
        $currentProfile = $this->createOrUpdateProfile($currentProfile, $user);
        if ($currentProfile && $user->userTwData) {
            $userTwData = $user->userTwData;
            $profilePrimaryKey = $currentProfile::primaryKey()[0];
            if ($userTwData->hasProperty('profile_id')) {
                $userTwData->profile_id = $currentProfile->{$profilePrimaryKey};
            }
            $userTwData->save(false);
        }
        return $currentProfile;
    }

    /**
     * @param ProfileInterface $sourceProfile
     * @return ProfileInterface|null
     */
    private function createOrUpdateProfile(ProfileInterface $sourceProfile, $user = null, $isCreate = false)
    {
        $profile = $this->_adfsUser->fillProfile($sourceProfile, $user, $isCreate);
        $profile->save(false);
        $profile = $this->_adfsUser->uploadFileToProfile($sourceProfile, $user, $isCreate);

        return $profile;
    }
    
    public function getAdfsUser(){
        return $this->_adfsUser;
    }

    /**
     * @param $user
     * @throws Exception
     */
    protected function assignRoleToUser($user)
    {
        $defaultRole = getenv('AUTH0_DEFAULT_USER_ROLE_AFTER_SIGN_UP');
        if (!$defaultRole) {
            $defaultRole = 'Viewer';
        }
        $role = Yii::$app->getAuthManager()->getRole($defaultRole);
        if (!Yii::$app->authManager->getAssignment($defaultRole, $user->id)) {
            Yii::$app->getAuthManager()->assign($role, $user->id);
        }
    }
}
