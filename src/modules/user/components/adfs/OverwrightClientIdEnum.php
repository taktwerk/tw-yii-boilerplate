<?php


namespace taktwerk\yiiboilerplate\modules\user\components\adfs;


class OverwrightClientIdEnum
{
    const ALLWAYS = 0;
    const WHEN_FIRST_LOGIN = 1;
    const WHEN_EMPTY_FIELD = 2;
}
