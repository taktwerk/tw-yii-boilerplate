<?php


namespace taktwerk\yiiboilerplate\modules\user\components\adfs;


class AdfsFillDataEnum
{
    const ALLWAYS = 0;
    const WHEN_CREATE = 1;
    const WHEN_EMPTY_FIELD = 2;
}
