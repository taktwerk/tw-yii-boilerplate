<?php


namespace taktwerk\yiiboilerplate\modules\user\service;

use Da\User\Contracts\ServiceInterface;
use Da\User\Event\UserEvent;
use Da\User\Helper\SecurityHelper;
use Da\User\Model\User;
use Da\User\Traits\MailAwareTrait;
use Da\User\Traits\ModuleAwareTrait;
use Exception;
use Yii;
use yii\base\InvalidCallException;
use taktwerk\yiiboilerplate\modules\user\factory\TokenFactory;

class PasswordSetService extends \Da\User\Service\PasswordRecoveryService
{

    /**
     * @throws InvalidCallException
     * @throws \yii\db\Exception
     * @return bool
     *
     */
    public function run()
    {
        try {
            if ($this->getModule()->enableFlashMessages == true) {
                Yii::$app->session->setFlash(
                    'info',
                    Yii::t('usuario', 'An email with instructions to set password has been sent to {email} if it is associated with an {appName} account. Your existing password has not been changed.', ['email' => $this->email, 'appName' => Yii::$app->name])
                    );
            }
            
            /** @var User $user */
            $user = $this->query->whereEmail($this->email)->one();
            
            if ($user === null) {
                throw new \RuntimeException('User not found.');
            }
            
            $token = TokenFactory::makeSetPasswordToken($user->id);
            
            if (!$token) {
                return false;
            }
            
            $this->mailService->setViewParam('user', $user);
            $this->mailService->setViewParam('token', $token);
            if (!$this->sendMail($user)) {
                return false;
            }
            
            return true;
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'usuario');
            
            return false;
        }
    }
}
