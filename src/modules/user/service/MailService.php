<?php

namespace taktwerk\yiiboilerplate\modules\user\service;

use Da\User\Contracts\ServiceInterface;
use Yii;
use yii\mail\BaseMailer;
use yii\mail\MailerInterface;

class MailService extends \Da\User\Service\MailService
{
    protected $viewPath = '@taktwerk-boilerplate/modules/user/mail';
}
