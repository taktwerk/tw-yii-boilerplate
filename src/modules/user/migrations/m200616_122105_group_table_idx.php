<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200616_122105_group_table_idx extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_group_deleted_at_client_id', '{{%group}}', ['deleted_at','client_id']);
    }

    public function down()
    {
        echo "m200616_122105_group_table_idx cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
