<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200911_191815_add_to_table_user_tw_data_indexes extends TwMigration
{
    public function up()
    {
        $table = Yii::$app->db->schema->getTableSchema('{{%user_tw_data}}');
        if(isset($table->columns['profile_id'])) {
            $this->createIndex('idx_user_tw_data_deleted_at_profile_id', '{{%user_tw_data}}', ['deleted_at','profile_id']);
        }
        $this->createIndex('idx_user_tw_data_deleted_at_user_id', '{{%user_tw_data}}', ['deleted_at','user_id']);
    }

    public function down()
    {
        echo "m200911_191815_add_to_table_user_tw_data_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
