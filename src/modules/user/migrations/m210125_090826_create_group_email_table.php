<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%group_email}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%group}}`
 */
class m210125_090826_create_group_email_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%group_email}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string(255)->null(),
            'user_id' => $this->integer(11)->null(),
            'group_id' => $this->integer(11),
        ]);
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\user"}';
        $this->addCommentOnTable('{{%group_email}}', $comment);
        // creates index for column `user_id`
        $this->createIndex(
            'idx-group_email-user_id',
            '{{%group_email}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            'fk-group_email-user_id',
            '{{%group_email}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `group_id`
        $this->createIndex(
            'idx-group_email-group_id',
            '{{%group_email}}',
            'group_id'
        );

        // add foreign key for table `{{%group}}`
        $this->addForeignKey(
            'fk-group_email-group_id',
            '{{%group_email}}',
            'group_id',
            '{{%group}}',
            'id',
            'CASCADE'
        );
        $this->createIndex('idx_group_email_deleted_at_user_id', '{{%group_email}}', ['deleted_at','user_id']);
        $this->createIndex('idx_group_email_deleted_at_group_id', '{{%group_email}}', ['deleted_at','group_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    $this->dropIndex('idx_group_email_deleted_at_user_id', '{{%group_email}}');
    $this->dropIndex('idx_group_email_deleted_at_group_id', '{{%group_email}}');
            // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            'fk-group_email-user_id',
            '{{%group_email}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-group_email-user_id',
            '{{%group_email}}'
        );

        // drops foreign key for table `{{%group}}`
        $this->dropForeignKey(
            'fk-group_email-group_id',
            '{{%group_email}}'
        );

        // drops index for column `group_id`
        $this->dropIndex(
            'idx-group_email-group_id',
            '{{%group_email}}'
        );

        $this->dropTable('{{%group_email}}');
    }
}
