<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200827_131120_add_namespace_to_user_auth_log extends TwMigration
{
    public function up()
    {
        $this->addCommentOnTable('{{%user_auth_log}}', '{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\user"}');
    }

    public function down()
    {
        echo "m200827_131120_add_namespace_to_user_auth_log cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
