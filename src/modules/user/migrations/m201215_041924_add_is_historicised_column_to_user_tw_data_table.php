<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%user_tw_data}}`.
 */
class m201215_041924_add_is_historicised_column_to_user_tw_data_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_tw_data}}', 'is_historicised', $this->boolean()->null()->defaultValue(null));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            $this->dropColumn('{{%user_tw_data}}', 'is_historicised');
    }
}
