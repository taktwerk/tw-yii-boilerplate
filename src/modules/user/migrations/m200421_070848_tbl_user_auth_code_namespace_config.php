<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200421_070848_tbl_user_auth_code_namespace_config extends TwMigration
{
    public function up()
    {
        $tbl = "{{%user_auth_code}}";
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\user"}';
        $this->addCommentOnTable($tbl, $comment);
    }

    public function down()
    {
        echo "m200421_070848_tbl_user_auth_code_namespace_config cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
