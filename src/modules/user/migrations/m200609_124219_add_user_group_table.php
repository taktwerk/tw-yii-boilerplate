<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200609_124219_add_user_group_table extends TwMigration
{
    public function up()
    {
        $tbl = "{{%user_group}}";
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\user"}';
        
        $this->createTable($tbl, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11)->notNULL(),
            'group_id' => $this->integer(11)->notNULL(),
        ]);
        $this->createIndex('unique_user_group_user_id_group_id', $tbl, ['user_id','group_id'],true);
        $this->addForeignKey('fk_user_group_user_id', $tbl, 'user_id', '{{%user}}', 'id');
        $this->createIndex('idx_user_group_deleted_at_user_id', $tbl, ['deleted_at','user_id']);
        $this->addForeignKey('fk_user_group_group_id', $tbl, 'group_id', '{{%group}}', 'id');
        $this->createIndex('idx_user_group_deleted_at_group_id', $tbl, ['deleted_at','group_id']);
        
        $this->addCommentOnTable($tbl, $comment);
    }

    public function down()
    {
        echo "m200609_124219_add_user_group_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
