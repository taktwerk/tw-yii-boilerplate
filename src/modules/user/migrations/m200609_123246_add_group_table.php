<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200609_123246_add_group_table extends TwMigration
{
    public function up()
    {
        $tbl = "{{%group}}";
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\user"}';
        
        $this->createTable($tbl, [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNULL()->unique()
        ]);
       
        $this->addCommentOnTable($tbl, $comment);
    }

    public function down()
    {
        echo "m200609_123246_add_group_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
