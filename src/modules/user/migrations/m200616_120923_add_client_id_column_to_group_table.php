<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%group}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%client}}`
 */
class m200616_120923_add_client_id_column_to_group_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%group}}', 'client_id', $this->integer(11)->after('name'));

        // creates index for column `client_id`
        $this->createIndex(
            '{{%idx-group-client_id}}',
            '{{%group}}',
            'client_id'
        );

        // add foreign key for table `{{%client}}`
        $this->addForeignKey(
            '{{%fk-group-client_id}}',
            '{{%group}}',
            'client_id',
            '{{%client}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%client}}`
        $this->dropForeignKey(
            '{{%fk-group-client_id}}',
            '{{%group}}'
        );

        // drops index for column `client_id`
        $this->dropIndex(
            '{{%idx-group-client_id}}',
            '{{%group}}'
        );

        $this->dropColumn('{{%group}}', 'client_id');
    }
}
