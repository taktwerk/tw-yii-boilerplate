<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200207_171227_add_foreign_key_to_device extends TwMigration
{
    public function up()
    {
        $this->addForeignKey(
            'fk_sync_progress_device_id',
            '{{%sync_process}}',
            'device_id',
            '{{%user_device}}',
            'id'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk_sync_process_device_id', '{{%sync_process}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
