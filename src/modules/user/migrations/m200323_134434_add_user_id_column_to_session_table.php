<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%session}}`.
 */
class m200323_134434_add_user_id_column_to_session_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%session}}', 'user_id', 'INT(11) AFTER id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%session}}', 'user_id');
    }
}
