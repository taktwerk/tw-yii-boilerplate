<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200827_133042_add_index_to_user_auth_log extends TwMigration
{
    public function up()
    {
        $this->addForeignKey('fk_user_auth_log_user_id', '{{%user_auth_log}}', 'user_id', '{{%user}}', 'id');
        $this->createIndex('idx_user_auth_log_deleted_at_user_id', '{{%user_auth_log}}', ['deleted_at','user_id']);

    }

    public function down()
    {
        echo "m200827_133042_add_index_to_user_auth_log cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
