<?php

use taktwerk\yiiboilerplate\TwMigration;
use yii\db\Schema;

/**
 * Handles the creation of table `{{%user_device}}`.
 */
class m200207_165554_create_user_device_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%user_device}}', [
            'id' => $this->primaryKey(),
            'uuid' => Schema::TYPE_TEXT,
            'user_id' => Schema::TYPE_INTEGER . "(11)",
            'device_information' => Schema::TYPE_TEXT,
        ]);

        $this->addForeignKey(
            'fk_device_user_id',
            '{{%user_device}}',
            'user_id',
            '{{%user}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropForeignKey('fk_device_user_id', '{{%user_device}}');
        $this->dropTable('{{%user_device}}');
    }
}
