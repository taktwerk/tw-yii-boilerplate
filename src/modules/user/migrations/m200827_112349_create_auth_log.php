<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200827_112349_create_auth_log extends TwMigration
{
    public function up()
    {
        $this->createTable('{{%user_auth_log}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'date' => $this->integer(),
            'cookie_based' => $this->boolean(),
            'duration' => $this->integer(),
            'error' => $this->string(),
            'ip' => $this->string(),
            'host' => $this->string(),
            'url' => $this->string(),
            'user_agent' => $this->string(),
        ]);
    }

    public function down()
    {
        echo "m200827_112349_create_auth_log cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
