<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200421_071117_tbl_user_device_namespace_config extends TwMigration
{
    public function up()
    {
        $tbl = "{{%user_device}}";
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\user"}';
        $this->addCommentOnTable($tbl, $comment);
    }

    public function down()
    {
        echo "m200421_071117_tbl_user_device_namespace_config cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
