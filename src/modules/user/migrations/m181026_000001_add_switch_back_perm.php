<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m181026_000001_add_switch_back_perm extends TwMigration
{
    public function up()
    {
        $this->createPermission('user_admin_switch-identity', 'User Switch Back', ['Viewer']);
    }

    public function down()
    {
        $this->removePermission('user_admin_switch-identity', ['Viewer']);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
