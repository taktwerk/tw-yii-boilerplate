<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200827_192905_add_auth_log_permission_to_authority extends TwMigration
{
    public function up()
    {
        // Init the permission cascade
        $this->createCrudControllerPermissions('user_user-auth-log');
        // Administration permission assignment
        $this->addAdminControllerPermission('user_user-auth-log','Authority');
    }

    public function down()
    {
        echo "m200827_192905_add_auth_log_permission_to_authority cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
