<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200214_001231_add_column_user_identifier extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'identifier', 'varchar(16) AFTER id');
        $this->createIndex('identifier_uk', '{{%user}}', 'identifier', true);
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'identifier');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
