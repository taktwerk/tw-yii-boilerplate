<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%user_auth_log}}`.
 */
class m210630_054419_add_user_env_column_to_user_auth_log_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_auth_log}}', 'user_env', $this->text()->null()->after('user_agent'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            $this->dropColumn('{{%user_auth_log}}', 'user_env');
    }
}
