<?php

use taktwerk\yiiboilerplate\modules\user\models\User;
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200214_003922_set_user_identifier_columns extends TwMigration
{
    public function up()
    {
        $usersWithoutIdentifier = User::find(['is', 'identifier', new \yii\db\Expression('null')])->all();

        foreach ($usersWithoutIdentifier as $user) {
            $user->identifier = Yii::$app->security->generateRandomString(User::IDENTIFIER_STRING_LENGTH);
            $user->save();
        }
    }

    public function down()
    {
        echo "m200214_003922_set_user_identifier_columns cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
