<?php

namespace taktwerk\yiiboilerplate\modules\user\helpers;

use Yii;
use taktwerk\yiiboilerplate\rest\Token;
use taktwerk\yiiboilerplate\modules\user\models\User;
use taktwerk\yiiboilerplate\modules\user\models\Group;

class ResponseHelper
{
    public static function getSuccessLoginActionResponse(User $user, Token $token)
    {
        $userRoles = Yii::$app->getUser()->getAllRoles(true);
        $userPermissions = Yii::$app->getUser()->getPermissions(true);

        return [
            'access_token' => $token->code,
            'user_id' => $user->id,
            'client_id' => $user->client_id,
            'username' => $user->username,
            'lastAuthItemChangedAt' => max($user->password_changed_at, $user->role_changed_at),
            'isAuthority' => Yii::$app->getUser()->can('Authority'),
            'groups'=>$user->groups,
            'additionalInfo' => [
                'fullname' => $user->fullname,
                'last_login_at' => date('Y-m-d G:i:s', $user->last_login_at),
                'email' => $user->email,
                'roles' => $userRoles,
                'permissions' => $userPermissions,
                'clientName' => $user->getClient() ? $user->getClient()->one()->toString : null,
            ],
        ];
    }
}
