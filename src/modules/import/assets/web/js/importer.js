/*
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by Nikola on 3/6/2017.
 */
pageReloaded = true;
var importOverlay = $('.import-overlay');
initImport = function () {
    Array.prototype.diff = function (a) {
        return this.filter(function (i) {
            if(typeof a =='string'){
                return a.indexOf(i) < 0;
            }
        });
    };
    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function camel2Id(text) {
        if (text !== null && !isNumber(text)) {
            text = text.replace(new RegExp(' ', 'g'), '_');
            text = text.toLowerCase();
        }
        return text;
    }

    function getShortModelName(model) {
        var parts, modelName;
        parts = model.split('\\');
        modelName = parts[parts.length - 1].split('.');
        return modelName[0];
    }

    progress = $('.progress'),
        progressBar = $(progress).children('.progress-bar'),
        progressText = $(progressBar).children('span'),
        importResult = $('#import-result'),
        assignSelectTemplate =
            '<select class="select2-assign" name="assign[{name}]" style="width: 100%">' +
            '<option value="-">-</option>' +
            '<option value="=">=</option>' +
            '</select>',
        elementTemplate =
            '<div class="row form-group{required}">' +
            '<div class="col-sm-2">' +
            '<div class="row">' +
            '<div class="col-md-12">' +
            '<label class="control-label" data-name="attributes[{name}]">{real-label}</label>' +
            '</div> ' +
            '{authority-label}'+
            '</div>' +
            '</div>'+
            '<div class="col-sm-1 assign" style="display: none;">' +
            '{assign}' +
            '</div>' +
            '<div class="col-sm-3" >' +
            '{element}' +
            '</div>' +
            '<div class="col-sm-3" >' +
            '{default}' +
            '</div>' +
            '<div class="col-sm-3">' +
            '{foreignElement}' +
            '</div>' +
            '</div>';

    prepareForm = function () {
        return new Promise(function (resolve, reject) {
            var div = $('#mappingPart'),
                url = div.attr('data-url'),
                delimiter = $('#delimiter-div'),
                firstRow = $('#firstRow-div'),
                header = $('#header-row-div'),
                helper = $('#helper-div'),
            showFldMapBtnSec = $('#show-field-map-btn-sec');
            div.html('');
            $.ajax({
                url: url + '?model=' + importModel,
                type: 'get'
            }).done(function (response) {
            	var data = $.parseJSON(response);
                prepareFormParse(data);
                resolve('Success');
            }).fail(function () {
                reject('Failure');
            });
        });
    };

    prepareFormParse = function (data) {
    	var div = $('#mappingPart'),
            url = div.attr('data-url'),
            delimiter = $('#delimiter-div'),
            firstRow = $('#firstRow-div'),
            header = $('#header-row-div'),
            helper = $('#helper-div'),
            showFldMapBtnSec = $('#show-field-map-btn-sec');
        div.html('');
        var is_authority = $('#user-role').attr('data-id');
        var methodVal = $('#method').val();
        $.each(data, function (key, value) {
            var element_label = value.label;
            var element = elementTemplate.replace('{real-label}', element_label),
                select = '<select class="select2-mapping" {required} name="attributes[{name}]" data-label="{label}" style="width: 100%">' +
                    '</select>';
            if(is_authority){
                var auth_label = '<div class="col-md-12">' +
                    '<small class="control-label">'+ value.name +'</small>' +
                    '</div>';
                element = element.replace('{authority-label}', auth_label);
            }else{
                element = element.replace('{authority-label}', '');
            }
            select = select.replace("{label}", element_label);
            select = select.replace("{name}", value.name);
            if (value.foreign == true) {
                var foreignSelect = '<select class="select2-foreign" name="foreign[{name}]" data-label="{label}" style="width: 100%">' +
                    '</select>';
                foreignSelect = foreignSelect.replace('{name}', value.name);
                foreignSelect = foreignSelect.replace('{label}', element_label);
                element = element.replace('{foreignElement}', foreignSelect);
            } else {
                element = element.replace('{foreignElement}', '');
            }
            element = element.replace('{element}', select);
            element = element.replace('{name}', value.name);
            element = element.replace("{label}", element_label);
            if (value.required == true) {
                element = element.replace('{required}', ' required');
            } else {
                element = element.replace('{required}', '');
            }
            var assignSelect = assignSelectTemplate.replace('{name}', value.name);
            element = element.replace('{assign}', assignSelect);
            element = element.replace('{label}', element_label);
            if(value.type=="datetime" || value.type=="date" || value.type=="time"){
                var default_text = '<div class="checkbox"><label><input type="checkbox" name="default[{name}]" value="1">Use current '+value.type+'</label></div>';
                if(methodVal==='insert' && (value.name=='created_at' || value.name=='updated_at')){
                	setTimeout(function(){
                		var selEl = $('#mappingPart [name="attributes['+value.name+']"]');
                    	if(selEl.val()=='' || selEl.val()==null){
                    		var defaultEl = $('#mappingPart input[name="default['+value.name+']"]');
                    		if(defaultEl.data('hasDefaultConfig')==undefined || defaultEl.data('hasDefaultConfig')==false){
                    			$('#mappingPart input[name="default['+value.name+']"]').prop('checked',true);
                    		}
                    	}
                	},250);
                }
            }else{
                var default_text = '<input class="form-control" type="text" name="default[{name}]">';
            }
            default_text = default_text.replace('{name}', value.name);
            element = element.replace('{default}', default_text);
            div.append(element);
        });

        $('.select2-mapping').select2({
            placeholder: "Select a option",
            //minimumResultsForSearch: Infinity,
            allowClear: true,
            theme: 'krajee'
        });
        $('.select2-assign').select2({
            placeholder: "Select a option",
            //minimumResultsForSearch: Infinity,
            theme: 'krajee'
        });
        fillForeign(data);
        firstRow.show();
        header.show();
        helper.show();
        showFldMapBtnSec.show();
        $('.help-popover').popover();
    };
    prepareForeign = function (data) {

    };
    fillForeign = function (data) {
        var selectors = $('.select2-foreign');
        $.each(selectors, function (key, selector) {
            $.each(data, function (key, model) {
                if (model.foreign == true && ('foreign[' + model.name + ']') == $(selector).attr('name')) {
                    $(selector).append(('<optgroup label="' + getShortModelName(model.foreignAttributes[0].name) + '"'));
                    $.each(model.foreignAttributes, function (key, attribute) {
                        $(selector).append($('<option />', {value: attribute.name}).text(attribute.label))
                            .val(model.foreignAttributes[0].name);
                    })
                }
            });
        });
        $(selectors).select2({
            allowClear: true,
            //minimumResultsForSearch: Infinity,
            theme: 'krajee'
        });
    };

    parseForm = function (data) {
    	var config = data.pre_config;
        var config_attr = [];
        var config_foreign = [];
        var config_default = [];
        var config_assign = [];
        if(config){
        	var config_attr = config.attributes;
            var config_foreign = config.foreign;
            var config_default = config.default;
            var config_assign = config.assign;
            $('#start_row').val(config.firstRow);
            $('#method').val(config.method).trigger('change');
            $('#validation_type').val(config.validation_type).trigger('change');
            $('#first_100_row').val(config.first_100_rows);
            $('#disable-history').prop('checked', (config.disable_history=="1" || config.disable_history==true));
            $('#imp_reset_value').val(config.reset_value);
        }
        var totalRows = data.totalRows;

        if(totalRows>100){
            $('.first_100').show();
        }else{
            $('.first_100').hide();
        }

        $('#start_row').attr('max',totalRows);

        var attributes = $('#mappingPart').find('.select2-mapping'),
            selectors = [],
            processedData = [],
            parsedData = [],
            allAttributes = [],
            unprocessedData,
            allData = [];
        $.each(attributes, function (key, selector) {
            selectors.push($(selector).attr('name'));
            $.each(data.data, function (index, value) {
                if (isNumber(value)) {
                    value = value + ' (Column ' + index + ')';
                }
                $(selector).append($('<option />', {value: index}).text(value))
                    .val(index);
            });
        });
        var is_match = $('#disable_detection').prop('checked');
        $.each(data.data, function (key, value) {
            allData.push(key);
            if(!is_match){
                if (value != null) {
                	value = value.toLowerCase();
                    var element_select_label = $('label[data-name="attributes['+value+']"]');
                    if(!element_select_label.text()){
                        var element_select_label = $('select[data-label="'+value+'"]').parent().siblings('label:first');
                    }
                    var element_name = $('select[data-label="'+value+'"]').attr('name');
                    if(!element_select_label.text()){
                        var element_select_label = $('label[data-name="'+element_name+'"]');
                    }        
                    var element_select = element_select_label.text();
                    var element_select_name = element_select_label.attr('data-name');
                    if (element_select === value) {
                        $('select[name="' + element_name + '"]').val(key).change();
                        parsedData.push(element_name);
                    }else if(element_select_name === 'attributes['+value+']'){
                        $('select[name="' + 'attributes['+value+']' + '"]').val(key).change();
                        parsedData.push('attributes['+value+']');
                    }else if(element_select_name === 'attributes['+element_select+']'){
                        $('select[name="' + element_select_name + '"]').val(key).change();
                        parsedData.push(element_select_name);
                    }
                }
            }
        });
        
        $.each($(selectors).not(parsedData).get(), function (key, value) {
            $('select[name="' + value + '"]').val([]).change();
        });

        var is_match = $('#disable_detection').prop('checked');
        if(is_match){
            $.each(allData, function (key, value) {
                $('select[name="' + selectors[key] + '"]').val(key).change();
            });
        }
        if(config_attr){
            if( Object.keys(config_attr).length>0){
                configData = [];
                allAttributes = [];
                $.each(config_attr, function (key, value) {
                    $('select[name="attributes[' + key + ']"]').val(value).change();
                });
                var keys = Object.keys(config_attr);
                $.each(selectors, function (key,value ) {
                    if(selectors[key]){
                        var attribute_push = '';
                        attribute_push = selectors[key];
                        attribute_push = attribute_push.replace('attributes[','');
                        attribute_push = attribute_push.replace(']','');
                        allAttributes.push(attribute_push);
                    }
                });
                configData =$(allAttributes).not(keys).get();
                $.each(configData, function (key,value ) {
                    $('select[name="' + 'attributes['+value+']' + '"]').val([]).change();
                });
            }

        }
        if(config_assign){
            $.each(config_assign, function (key, value) {
                $('select[name="assign[' + key + ']"]').val(value).change();
            });
        }        
        if(config_foreign){
            $.each(config_foreign, function (key, value) {
                $('select[name="foreign[' + key + ']"]').val(value).change();
            });
        }       
        if(config_default){
        	$.each(config_default, function (key, value) {
            	var el =  $('#mappingPart input[name="default[' + key + ']"]');
            	if(el.prop('type')=='checkbox'){
            		el.prop('checked',(value=='1'));
            		el.data('hasDefaultConfig',true);
            	}else{
            		el.val(value);
            	}
            });
        }

        $(importOverlay).hide();
    };

    removeForm = function () {
        var div = $('#mappingPart'),
            firstRow = $('#firstRow-div'),
            header = $('#header-row-div'),
            helper = $('#helper-div')
            showFldMapBtnSec = $('#show-field-map-btn-sec');
        div.html('');
        firstRow.hide();
        helper.hide();
        header.hide();
        showFldMapBtnSec.hide();
        hideResult();
        $('.attribute-container.collapse').collapse('hide');
    };

    updateProgress = function (fromModal) {
        var pjaxContainer = gridViewKey + '-pjax-container';
        $.ajax({
                type: "POST",
                url: statusUrl,
                data: {'model': importModel, 'pageReloaded': pageReloaded},
                async: true
            })
            .done(function (data) {
                pageReloaded = false;
                data = $.parseJSON(data);
                if (data.running == true) {
                    if (data.status == 'Running') {
                        $(importOverlay).show();
                        $(progress).show();
                        $(progressBar).css('width', data.progress + '%').attr('aria-valuenow', data.progress);
                        $(progressText).html(data.progress + '%');
                        setTimeout(updateProgress, interval);
                    } else if (data.status == 'Error') {
                        $(progress).hide();
                        $(importOverlay).hide();
                        var cls = 'alert alert-danger';
                        $(importResult).show();
                        $(importResult).html(data.result);
                        $(importResult).addClass(cls);
                    } else if (data.status == 'Finished') {
                        location.reload(true)
                        $('#' + importModal).modal('hide');
                        removeForm();
                        pageReloaded = true;
                        $(progress).hide();
                        $(importOverlay).hide();
                    }
                } else {
                    $(progress).hide();
                    $(importOverlay).hide();
                }
            })
        ;
    };

    hideResult = function () {
        $(importResult).hide();
        $(importResult).removeClass();
    };

    $('#form-import').on('submit', function (event) {
        event.preventDefault();
        var form = $(this),
            button = $(document.activeElement).attr('name');
        if (button == 'background') {
            var clickedButton = $('button[name="background"]'),
                url = $(clickedButton).attr('data-url');
            $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(),
                    async: true
                })
                .done(function (data) {
                    $('#form-import').trigger("reset");
                    $('#' + importModal).modal('hide');
                    removeForm();
                    pageReloaded = true;
                    $(progress).hide();
                    $(importOverlay).hide();

                });
        } else {
            hideResult();
            importOverlay.show();
            progress.show();
            $.ajax({
                    type: "POST",
                    url: form.attr("action"),
                    data: form.serialize(),
                    async: true
                })
                .done(function (data) {
                    setTimeout(updateProgress, interval);
                });
        }
        return false;
    });

    $('#' + importModal).on('show.bs.modal', function (e) {
        if (e.target.id == $(this)[0].id) {
            // Only if is fired from outer modal
            updateProgress();
        }
    });

    $('#prepare-google').on('click', function(e) {
        $(importOverlay).show();
        removeForm();
        $("#input-file").fileinput("clear");
        var url = $(this).attr('data-url'),
            self = $(this);
        $.ajax({
                type: "POST",
                url: url,
                data: {'file_name': $('#google_url').val(), 'type': 'google'},
                async: true
            })
            .done(function (response) {
                data = $.parseJSON(response);
                prepareForm().then(function(response){
                    parseForm(data);
                });
                $(self).closest("form").find(".btn-success").prop("disabled", false);
                $(importOverlay).hide();
            });
    });
    $('#google_url').on('change', function(){
        removeForm();
        $("#input-file").fileinput("clear");
        $('#google_import').val('true');
        if ($(this).val() != '') {
            $('#prepare-google').prop('disabled', false);
        } else {
            $('#prepare-google').prop('disabled', true);
        }
    });

    $('.imported-url').on('click', function(e){
        e.preventDefault();
        $('#google_url').val($(this).attr('href'));
        $('#google_url').trigger('change');
    });
};

$(window).on("ready load",function(){
    initImport();

    $('.reset-import-btn').on('click',function(event){
        console.log('in');
        event.preventDefault();
        $('.import-overlay').show();
        $.ajax({
            type: "POST",
            url: cancelUrl,
            data: {
                model: importModel,
            },
            success: function (data) {
                $('.import-overlay').hide();
                location.reload(true);
                pageReloaded = true;
            }
        });
    });
})

$(document).on('pjax:success', function () {
    initImport();
    pageReloaded = true;

    $('.reset-import-btn').on('click',function(event){
        event.preventDefault();
        $('.import-overlay').show();
        $.ajax({
            type: "POST",
            url: cancelUrl,
            data: {
                model: importModel,
            },
            success: function (data) {
                $('.import-overlay').hide();
                location.reload(true);
                pageReloaded = true;
            }
        });
    });
});