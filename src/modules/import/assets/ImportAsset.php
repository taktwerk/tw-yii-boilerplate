<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 3/6/2017
 * Time: 12:09 PM
 */

namespace taktwerk\yiiboilerplate\modules\import\assets;

use yii\helpers\FileHelper;
use yii\web\AssetBundle;

class ImportAsset extends AssetBundle
{
    public $sourcePath = '@taktwerk-boilerplate/modules/import/assets/web';

    public $css = [
        'less/importer.less',
    ];
    public $js = [
        'js/importer.js',
    ];
    public $depends = [
        'taktwerk\yiiboilerplate\assets\TwAsset',
    ];

    /**
     *
     */
    public function init()
    {
        parent::init();
        // we recompile the less files from 'yii\bootstrap\BootstrapAsset' and include the css in app.css
        // therefore we set bundle to false
        \Yii::$app->getAssetManager()->bundles['yii\bootstrap\BootstrapAsset'] = false;

        // /!\ CSS/LESS development only setting /!\
        // Touch the asset folder with the highest mtime of all contained files
        // This will create a new folder in web/assets for every change and request
        // made to the app assets.
        if (getenv('APP_ASSET_FORCE_PUBLISH')) {
            $files = FileHelper::findFiles(\Yii::getAlias($this->sourcePath));
            $mtimes = [];
            foreach ($files as $file) {
                $mtimes[] = filemtime($file);
            }
            touch(\Yii::getAlias($this->sourcePath), max($mtimes));
        }
    }
}
