<?php
//Generation Date: 11-Sep-2020 02:51:03pm
namespace taktwerk\yiiboilerplate\modules\import\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * ImportController implements the CRUD actions for Import model.
 */
class ImportController extends TwCrudController
{
}
