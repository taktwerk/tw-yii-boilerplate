<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200911_192427_add_to_table_import_progress_indexes extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_import_progress_deleted_at_user_id', '{{%import_progress}}', ['deleted_at','user_id']);
    }

    public function down()
    {
        echo "m200911_192427_add_to_table_import_progress_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
