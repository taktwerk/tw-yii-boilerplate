<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200414_115913_add_basenamespace_to_import extends TwMigration
{
    public function up()
    {
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\import"}';
        $this->addCommentOnTable('{{%import_progress}}', $comment);
        $this->addCommentOnTable('{{%import}}', $comment);
    }

    public function down()
    {
        echo "m200414_115913_add_basenamespace_to_import cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
