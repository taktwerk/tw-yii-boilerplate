<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m170817_103336_importer_acl extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $permission = $auth->createPermission('import_index');
        $permission->description = 'Import module';
        $auth->add($permission);

        $user = $auth->getRole('Public');
        $auth->addChild($user, $permission);
    }

    public function down()
    {
        echo "m170817_103336_importer_acl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
