<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m161128_103812_import extends TwMigration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%import}}',
            [
                'id'=> Schema::TYPE_PK."",
                'model' => Schema::TYPE_STRING . '(255) NOT NULL',
                'log'=> Schema::TYPE_TEXT,
            ],
            $tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%import}}');
    }
}
