<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 6/1/2017
 * Time: 2:54 PM
 */

namespace taktwerk\yiiboilerplate\modules\import\commands;

use taktwerk\yiiboilerplate\modules\import\models\Import;
use taktwerk\yiiboilerplate\modules\notification\models\Notification;
use taktwerk\yiiboilerplate\modules\queue\commands\BackgroundCommandInterface;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;

class ImportCommand implements BackgroundCommandInterface
{
    /**
     * @param QueueJob $queue
     * @return mixed|void
     */
    public function run(QueueJob $queue)
    {
        $import = new Import();
        $params = json_decode($queue->parameters, true);
        $import->userId = $params['user_id'];
        $import->setPostData($params);

        $import_array = [];
        $fileName = isset($params['upload-file-name'])&&strlen($params['upload-file-name'])>1?trim($params['upload-file-name']):$params['file_name'];
        if($import_data = \Yii::$app->get('userUi')->get('Import', $fileName)){
            $import_array = ArrayHelper::merge($import_array,$import_data);
        }

        $import_array['attributes'] = $params['attributes'];
        $import_array['foreign'] = isset($params['foreign']) ? $params['foreign'] : [];
        $import_array['default'] = isset($params['default']) ? $params['default'] : [];
        $import_array['method'] =  $params['method'];
        $import_array['validation_type'] = isset($params['validation_type'])?$params['validation_type']:Import::VALIDATION_TYPE_FULL;
        $import_array['assign'] =isset($params['assign']) ? $params['assign'] : [];
        $import_array['firstRow'] = isset($params['start_row']) ? $params['start_row'] : false;
        $import_array['first_100_rows'] = isset($params['first_100_row']) ? true : false;
        $import_array['disable_history'] = isset($params['disable_history']) ? $params['disable_history'] : false;
        $import_array['reset_value'] = isset($params['reset_value']) ? $params['reset_value'] : '_reset';
        $stored = \Yii::$app->get('userUi')->set('Import', $fileName, $import_array,  $params['user_id']);
        
        try {
            $result = $import->process($queue);
            if ($result['success'] == 'success') {
                $queue->status = QueueJob::STATUS_FINISHED;
            } else {
                $queue->status = QueueJob::STATUS_FAILED;
                $queue->error = $result['result'];
            }
            $queue->save();
        } catch (\Exception $e) {
            $queue->status = QueueJob::STATUS_FAILED;
            $queue->error = $e->getMessage();
            Notification::send($params['user_id'], 'Import process failed');
        }
       
    }
}