<?php
/**
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 */

/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 3/14/2017
 * Time: 9:30 AM
 */
/**
 * @var string $pjaxContainer
 * @var string $modelName
 * @var string $importValidateMethod
 * @var string $returnUrl
 * @var $this \yii\web\View
 */
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\file\FileInput;
use taktwerk\yiiboilerplate\widget\Select2;
use dmstr\bootstrap\Tabs;
use taktwerk\yiiboilerplate\modules\import\models\Import;

?>
    <div class="row">
        <div class="col-md-12">
            <div class="progress" style="display: none;">
                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="0"
                     aria-valuemin="1" aria-valuemax="100" style="min-width: 2em;">
                    <span>0%</span>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div id="import-result" style="display: none"></div>
        </div>
        <div class="col-md-12">
            <div class="alert alert-danger" style="display: none" id="upload-error"></div>
            <div class="alert alert-success" style="display: none" id="upload-success"></div>
        </div>
        <div class="col-md-6">
            <?= Html::tag(
                'p',
                Html::tag('strong', \Yii::t('twbp', 'Import data from .csv, .txt or .xlsx files')),
                ['class' => 'h5']
            ) ?>
        </div>
        <div class="col-md-6">
            <?= Html::a( \Yii::t('twbp', 'Reset Import' ),'#',['class'=>'btn-danger btn pull-right reset-import-btn','id'=>'reset-import-btn']) ?>
        </div>
    </div>
    <hr>
    <div class="container-fluid">

        <div class="row" id="import-form">
            <?= Html::beginForm(
                [Url::to('/import/index/import')],
                'POST',
                ['role' => 'form', 'class' => 'form-horizontal', 'id' => 'form-import']
            ) ?>
            <?php $this->beginBlock('upload-form'); ?>
            <br>
            <div class="form-group">
                <div class="import-overlay kv-grid-loading"><div class="loading"></div></div>
                <label class="control-label col-sm-3" for="input-file"><?= \Yii::t('twbp', 'Import File') ?></label>
                <div class="col-sm-7">
                    <?= FileInput::widget([
                        'name' => 'input-file',
                        'pjaxContainerId' => $pjaxContainer,
                        'id' => 'input-file',
                        'options' => [
                            'class' => 'form-control'
                        ],
                        'pluginOptions' => [
                            'uploadUrl' => Url::to(['/import/index/upload']),
                            'allowedPreviewTypes' => false,
                            'allowedPreviewMimeTypes' => false,
                            'previewSettings' => false,
                            'showPreview' => false,
                            'showRemove' => false,
                            'showUpload' => true,
                            'showCancel' => false,
                            'uploadAsync' => true,
                            'allowedFileExtensions' => [
                                'csv',
                                'xlsx',
                                'xls',
                                'txt',
                            ],
                            'uploadExtraData' => new \yii\web\JsExpression("function (previewId, index) {
                    return {
                        delimiter: $('select[name=\"delimiter\"]').val(),
                    };
                }"),
                        ],
                        'pluginEvents' => [
                            'fileuploaded' => 'function(event, data, previewId, index) {
                            // Call function from assets to process form by showing additional fields
                            if (data.response.data) {
                                prepareForm().then(function(response){
                                    parseForm(data.response);
                                });
                            } else {
                                $(importOverlay).hide();
                                $("#upload-error").html(data.response.response);
                                $("#upload-error").show();
                                removeForm();
                                $("#input-file").fileinput("clear");
                            }
                            $("#prepare-google").prop("disabled", true);
                            $("#google_url").val(null);
                            $("#google_import").val(null);
                            $(this).closest("form").find(".btn-success").prop("disabled", false);
                            $("input[name=\"file_name\"]").val(data.files[0].name.replace(/[-)(]/g, "_"));
                        }',
                            'change' => 'function(event, data, previewId, index) {
                            // Call function from assets to process form by showing additional fields
                            $(this).closest("form").find(".btn-success").prop("disabled", true);
                            removeForm();
                        }',
                            'filepreupload' => 'function(event, data, previewId, index) {
                            $(importOverlay).show();
                        }',
//                        'filebatchselected' => 'function(event, data, previewId, index) {
//                            $(this).fileinput("upload");
//                        }',
                        ],
                    ]) ?>
                </div>
            </div>
            <h5 class="text-bold text-center">
                <?= \Yii::t('twbp', 'OR') ?></h5>
            <div class="form-group no-gutters">
                <label class="control-label col-sm-3" for="upload-file-name">
                    <?= \Yii::t('twbp', 'Upload file from flysystem') ?>
                </label>
                <div class="col-sm-6">
                    <?php echo \taktwerk\yiiboilerplate\widget\ElfinderInputFile::widget([
                            'name'=>'el-input',
                            'id'=>'el-input',
                            'return'=>'path',
                            'path'=>'uploads/Importer'
                    ]);
                    ?>
                </div>
                <div class="col-sm-1">
                    <?= Html::button('Process', ['id' => 'el-file-btn', 'class'=>'btn btn-secondary', 'style'=>'width:100%; display:none;']) ?>
                </div>
            </div>
            <div class="form-group required" id="delimiter-div">
                <label class="control-label col-sm-3" for="delimiter">
                    <?= \Yii::t('twbp', 'Delimiter') ?>
                </label>
                <div class="col-sm-7">
                    <?= Select2::widget([
                        'id' => 'delimiter',
                        'pjaxContainerId' => $pjaxContainer,
                        'name' => 'delimiter',
                        'data' => [
                            'auto' => Yii::t('twbp', 'Auto detect'),
                            ';' => ';',
                            '|' => '|',
                            ',' => ',',
                            '\t' => '\t (Tab)'
                        ],
                        'pluginOptions' => [
                            'minimumResultsForSearch' => 'Infinity',
                        ]
                    ]) ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="checkbox">
                        <label for="disable_detection">
                            <?= Html::checkbox('disable_detection', false, ['id' => 'disable_detection']) ?>
                            <?= \Yii::t('twbp', 'Disable Column Detection') ?>
                        </label>
                    </div>
                </div>
            </div>

            <?php $this->endBlock() ?>
            <?php $this->beginBlock('google-form') ?>
            <?= $this->render('_google_spreadsheet') ?>
            <?php $this->endBlock() ?>
            <?= Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                        'label' => Yii::t('twbp', 'Upload file'),
                        'content' => $this->blocks['upload-form'],
                        'active' => true,
                    ],
                    [
                        'label' => Yii::t('twbp', 'Import Google Spreadsheet'),
                        'content' => $this->blocks['google-form'],
                        'active' => false,
                        'visible' => \taktwerk\yiiboilerplate\modules\import\models\Import::isGoogleEnabled(),
                    ],
                ]
            ])
            ?>
            <div id="firstRow-div" style="display:none;">
                <div class="form-group required">
                    <label class="control-label col-sm-3">
                        <?= \Yii::t('twbp', 'Method') ?>
                    </label>
                    <div class="col-sm-7">
                        <?= Select2::widget([
                            'id' => 'method',
                            'pjaxContainerId' => $pjaxContainer,
                            'name' => 'method',
                            'data' => [
                                'insert' => Yii::t('twbp', 'Insert'),
                                'update' => Yii::t('twbp', 'Update'),
                                'upsert' => Yii::t('twbp', 'Insert or Update'),
                            ],
                            'pluginOptions' => [
                                'minimumResultsForSearch' => 'Infinity',
                            ],
                            'pluginEvents' => [
                                'change' => 'function(){
                                if ($(this).val() == "insert") {
                                    $(".assign-info").hide();
                                    $(".assign").hide();
                                } else {
                                    $(".assign-info").show();
                                    $(".assign").show();
                                }
                            }',
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="checkbox">
                        <label for="halt-on-error">
                            <?= Html::checkbox('halt_on_error', false, ['id' => 'halt-on-error','uncheck'=>0]) ?>
                            <?= \Yii::t('twbp', 'Halt on error') ?>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group" id="rb-error-sec">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="checkbox">
                        <label for="rb-on-error">
                            <?= Html::checkbox('rb_on_error', false, 
                                ['id' => 'rb-on-error','uncheck'=>0]) ?>
                            <?= \Yii::t('twbp','Rollback on error') ?>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="checkbox">
                        <label for="disable-history">
                            <?= Html::checkbox('disable_history', false, ['uncheck'=>0,'id'=>'disable-history']) ?>
                            <?= \Yii::t('twbp', 'Do not create history entries') ?>
                        </label>
                    </div>
                </div>
            </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3">
                        <?= \Yii::t('twbp', 'Import Validation Type') ?>
                    </label>
                    <div class="col-sm-7">
                        <?= Select2::widget([
                            'id' => 'validation_type',
                            'pjaxContainerId' => $pjaxContainer,
                            'name' => 'validation_type',
                            'data' => [
                                Import::VALIDATION_TYPE_FULL => Yii::t('twbp', 'Validate All Attributes'),
                                Import::VALIDATION_TYPE_IMPORTED => Yii::t('twbp', 'Validate Only Imported Attributes'),
                                Import::VALIDATION_TYPE_OFF => Yii::t('twbp', 'No Validation'),
                            ],
                            'pluginOptions' => [
                                'minimumResultsForSearch' => 'Infinity',
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="imp_reset_value">
                        <?= \Yii::t('twbp', 'Reset Column Value if attribute has this value') ?>
                    </label>
                    <div class="col-sm-7">
                        <?= Html::textInput('reset_value', '_reset', ['id' => 'imp_reset_value',
                            'class'=>'form-control']) ?>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="start_row">
                        <?= \Yii::t('twbp', 'Start process from row') ?>
                    </label>
                    <div class="col-sm-7">
                        <?= Html::textInput('start_row', 2, ['id' => 'start_row','type'=>'number','min'=>1, 'class'=>'form-control']) ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6 col-sm-offset-3 first_100" style="display: none">
                        <div class="checkbox">
                            <label for="first_100_row">
                                <?= Html::checkbox('first_100_row', false, ['id' => 'first_100_row']) ?>
                                <?= \Yii::t('twbp', 'Process first 100 row') ?>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div id="show-field-map-btn-sec" class="container" style="display: none;margin-bottom: 20px;margin-top: 30px;">
<button class="btn btn-warning" data-toggle="collapse" data-target=".attribute-container"

aria-expanded="false" type="button">View File Columns Mapping</button>
</div>
<div class="attribute-container collapse">
            <div id="header-row-div" style="display: none;">
                <div class="form-group row text-right">
                    <div class="col-sm-2">
                        <strong><?= \Yii::t('twbp', 'Attributes') ?></strong>
                        <?= Html::tag(
                            'a',
                            '?',
                            [
                                'class' => 'btn btn-default btn-sm pull-right help-popover',
                                'style' => 'border-bottom-right-radius: 3px; border-top-right-radius: 3px; margin-top: -5px; margin-left: 1px',
                                'role' => 'button',
                                'data-toggle' => 'popover',
                                'data-trigger' => 'focus',
                                'tabindex' => 0,
                                'data-container' => 'body',
                                'data-placement' => 'top',
                                'data-title' => \Yii::t('twbp', 'Attributes'),
                                'data-content' => \Yii::t('twbp', 'The following rows represent the fields of the model that the data will be imported into. Map as many fields as possible for the best results.'),
                            ]
                        ) ?>
                    </div>
                    <div class="col-sm-1 assign-info" style="display: none;">
                        <strong><?= \Yii::t('twbp', 'Operator') ?></strong>
                        <?= Html::tag(
                            'a',
                            '?',
                            [
                                'class' => 'btn btn-default btn-sm pull-right help-popover',
                                'style' => 'border-bottom-right-radius: 3px; border-top-right-radius: 3px; margin-top: -5px; margin-left: 1px',
                                'role' => 'button',
                                'data-toggle' => 'popover',
                                'data-trigger' => 'focus',
                                'tabindex' => 0,
                                'data-container' => 'body',
                                'data-placement' => 'top',
                                'data-title' => \Yii::t('twbp', 'Columns from file'),
                                'data-content' => \Yii::t('twbp', 'Map the columns from the imported file to the fields of the model.'),
                            ]
                        ) ?>
                    </div>
                    <div class="col-sm-3">
                        <strong><?= \Yii::t('twbp', 'Columns from file') ?></strong>
                        <?= Html::tag(
                            'a',
                            '?',
                            [
                                'class' => 'btn btn-default btn-sm pull-right help-popover',
                                'style' => 'border-bottom-right-radius: 3px; border-top-right-radius: 3px; margin-top: -5px; margin-left: 1px',
                                'role' => 'button',
                                'data-toggle' => 'popover',
                                'data-trigger' => 'focus',
                                'tabindex' => 0,
                                'data-container' => 'body',
                                'data-placement' => 'top',
                                'data-title' => \Yii::t('twbp', 'Columns from file'),
                                'data-content' => \Yii::t('twbp', 'Map the columns from the imported file to the fields of the model.'),
                            ]
                        ) ?>
                    </div>
                    <div class="col-sm-3">
                        <strong><?= \Yii::t('twbp', 'Default value') ?></strong>
                        <?= Html::tag(
                            'a',
                            '?',
                            [
                                'class' => 'btn btn-default btn-sm pull-right help-popover',
                                'style' => 'border-bottom-right-radius: 3px; border-top-right-radius: 3px; margin-top: -5px; margin-left: 1px',
                                'role' => 'button',
                                'data-toggle' => 'popover',
                                'data-trigger' => 'focus',
                                'tabindex' => 0,
                                'data-container' => 'body',
                                'data-placement' => 'top',
                                'data-title' => \Yii::t('twbp', 'Default value'),
                                'data-content' => \Yii::t('twbp', "Replaces the columns from the imported file with the field's value."),
                            ]
                        ) ?>
                    </div>
                    <div class="col-sm-3">
                        <strong><?= \Yii::t('twbp', 'Foreign Lookup') ?></strong>
                        <?= Html::tag(
                            'a',
                            '?',
                            [
                                'class' => 'btn btn-default btn-sm pull-right help-popover',
                                'style' => 'border-bottom-right-radius: 3px; border-top-right-radius: 3px; margin-top: -5px; margin-left: 1px',
                                'role' => 'button',
                                'data-toggle' => 'popover',
                                'data-trigger' => 'focus',
                                'tabindex' => 0,
                                'data-container' => 'body',
                                'data-placement' => 'top',
                                'data-title' => \Yii::t('twbp', 'Foreign Lookup'),
                                'data-content' => \Yii::t('twbp', 'In case the model\'s property is an external model and your data doesn\'t have the ID of the foreign data but something else that can identify it, use these options to help us know what we are looking for. By default, \'Auto ID\' assumes the ID of the foreign model'),
                            ]
                        ) ?>
                    </div>
                </div>
            </div>
            <div id="mappingPart" data-url="<?= Url::toRoute(['/import/index/attributes']) ?>">
            </div>
</div>
            <?= Html::input('hidden', 'returnUrl', $returnUrl) ?>
            <?= Html::input('hidden', 'model', $modelName) ?>
            <?= Html::input('hidden', 'importValidateMethod', $importValidateMethod) ?>
            <?= Html::input('hidden', 'file_name') ?>

            <hr>

            <div class="col-sm-7 col-sm-offset-5">
                <div class="pull-right">
                    <?= Html::button(
                        '<span class="glyphicon glyphicon-check"></span> ' . \Yii::t('twbp', 'Import'),
                        ['type' => 'submit', 'class' => 'btn btn-success btn-import', 'disabled' => true]
                    ) ?>
                    <?= Html::button(
                        '<span class="glyphicon glyphicon-check"></span> ' . \Yii::t('twbp', 'Import in background'),
                        [
                            'type' => 'submit',
                            'class' => 'btn btn-success btn-import',
                            'name' => 'background',
                            'disabled' => true,
                            'data-url' => Url::toRoute(['/import/index/background'])
                        ]
                    ) ?>
                </div>
            </div>
            <span style="display: none" data-id="<?=Yii::$app->user->can('Authority');?>" id="user-role"></span>
            <?= Html::endForm() ?>
        </div>
    </div>
<?php
$css = <<<CSS
#import-form .btn,  #import-form .btn-group, #import-form .input-group {
    float: none !important;
}
.loading{
    z-index: 10000;
    width:160px;
    height:160px;
    border-radius:150px;
    border:10px solid #fff;
    border-top-color:#3c8dbc;
    box-sizing:border-box;
    position:absolute;
    top:50%;left:50%;
    margin-top:-80px;
    margin-left:-80px;
    animation:loading 1.2s linear infinite;
    -webkit-animation:loading 1.2s linear infinite;
}
@keyframes loading{
  0%{transform:rotate(0deg)}
  100%{transform:rotate(360deg)}
}
@-webkit-keyframes loading{
  0%{-webkit-transform:rotate(0deg)}
  100%{-webkit-transform:rotate(360deg)}
}
.import-overlay {
    z-index: 999;
	background: rgba(255,255,255,0.66);
	position: absolute;
	opacity: 0.4;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	display: none;
	margin-top: 20vh;
}
CSS;
$this->registerCss($css);
$url = Url::to(['/import/index/upload']);
$js = <<<JS
    $('#halt-on-error').on('change',function(){
        if($(this).prop('checked')==true){
            $('#rb-error-sec').show();
        }else{
            $('#rb-error-sec').hide();
        }
    });
    $('#halt-on-error').trigger('change');
    $('#el-input').on('change',function(){
        $("#el-file-btn").show();
    }); 
    $('#el-file-btn').on('click',function(){
        $('.import-overlay').show();
        $.ajax({
            type: "POST",
            url: "$url",        
            data: {
                el_upload: 1,
                input_file: $("#el-input").val(),
            },
            success: function (data) {
                response_text = $.parseJSON(data);
                if(response_text.data===false){
                    var cls = 'alert alert-danger';
                    $('#upload-error').show();
                    $('#upload-error').html(response_text.response);
                    $('#upload-error').addClass(cls);
                }else{
                    $("input[name='file_name']").val(response_text.file_name);
                    $('#upload-error').hide();
                    data = $.parseJSON(data);
                    prepareForm().then(function(response){
                        parseForm(data);
                    });
                    $(".btn-import").prop("disabled", false);
                }
                     $('.import-overlay').hide();
            }
        });
        
    });
    
$(window).on("pjax:success",function(){
    $('#el-file-btn').on('click',function(){
        $('.import-overlay').show();
        $.ajax({
            type: "POST",
            url: "$url",        
            data: {
                el_upload: 1,
                input_file: $("#el-input").val(),
            },
            success: function (data) {
                response_text = $.parseJSON(data);
                if(response_text.data===false){
                    var cls = 'alert alert-danger';
                    $('#upload-error').show();
                    $('#upload-error').html(response_text.response);
                    $('#upload-error').addClass(cls);
                }else{
                    $("input[name='file_name']").val(response_text.file_name);
                    $('#upload-error').hide();
                    data = $.parseJSON(data);
                    prepareForm().then(function(response){
                        parseForm(data);
                    });
                    $(".btn-import").prop("disabled", false);
                }
                     $('.import-overlay').hide();
            }
        });
    });
});

JS;
$this->registerJs($js);
