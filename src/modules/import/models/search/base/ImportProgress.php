<?php
//Generation Date: 11-Sep-2020 02:51:03pm
namespace taktwerk\yiiboilerplate\modules\import\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\import\models\ImportProgress as ImportProgressModel;

/**
 * ImportProgress represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\import\models\ImportProgress`.
 */
class ImportProgress extends ImportProgressModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'user_id',
                    'model',
                    'pid',
                    'total_rows',
                    'current_row',
                    'message',
                    'status',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ImportProgressModel::find();

        $this->parseSearchParams(ImportProgressModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(ImportProgressModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(ImportProgressModel::class),
                'params' => [
                    'page' => $this->parsePageParams(ImportProgressModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('user_id', $query);
        $this->applyHashOperator('pid', $query);
        $this->applyHashOperator('total_rows', $query);
        $this->applyHashOperator('current_row', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('model', $query);
        $this->applyLikeOperator('message', $query);
        $this->applyLikeOperator('status', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(ImportProgressModel::class);
    }
}
