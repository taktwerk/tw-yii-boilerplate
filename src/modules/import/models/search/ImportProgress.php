<?php

namespace taktwerk\yiiboilerplate\modules\import\models\search;

use taktwerk\yiiboilerplate\modules\import\models\search\base\ImportProgress as ImportProgressSearchModel;

/**
* ImportProgress represents the model behind the search form about `taktwerk\yiiboilerplate\modules\import\models\ImportProgress`.
*/
class ImportProgress extends ImportProgressSearchModel{

}