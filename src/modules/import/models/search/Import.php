<?php
//Generation Date: 11-Sep-2020 02:51:03pm
namespace taktwerk\yiiboilerplate\modules\import\models\search;

use taktwerk\yiiboilerplate\modules\import\models\search\base\Import as ImportSearchModel;

/**
* Import represents the model behind the search form about `taktwerk\yiiboilerplate\modules\import\models\Import`.
*/
class Import extends ImportSearchModel{

}