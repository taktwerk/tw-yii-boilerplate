<?php
/**
 * @property integer $id
 * @property string $model
 * @property string $log
 * @property integer $deleted_by
 * @property string $deleted_at
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property string $toString
 * @property string $entryDetails
 *
 * Copyright (c) 2017.
 * @author Nikola Tesic (nikolatesic@gmail.com)
 *
 * Created by PhpStorm.
 * User: Nikola
 * Date: 3/7/2017
 * Time: 11:17 AM
 */
namespace taktwerk\yiiboilerplate\modules\import\models;

use \taktwerk\yiiboilerplate\models\TwActiveRecord;
use taktwerk\yiiboilerplate\models\GoogleSpreadsheet;
use taktwerk\yiiboilerplate\modules\import\models\ImportProgress;
use taktwerk\yiiboilerplate\modules\import\widget\ImportResult;
use taktwerk\yiiboilerplate\modules\notification\models\Notification;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use Yii;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use taktwerk\yiiboilerplate\modules\import\models\base\Import as BaseImport;

class Import extends BaseImport
{

    public $userId;

    /**
     *
     * @var array
     */
    protected $postData;

    /**
     *
     * @var array
     */
    protected $processAttributes = [];

    /**
     *
     * @var string
     */
    protected $fileName;
    /**
     *
     * @var string
     */
    protected $validationType = 'off';
    /**
     *
     * @var string
     */
    protected $importValidateMethod = null;
    /**
     *
     * @var bool
     */
    protected $firstRow = 2;
    /**
     *
     * @var bool
     */
    protected $first_100_rows = false;

    /**
     *
     * @var string
     */
    protected $delimiter = ';';

    /**
     *
     * @var string
     */
    protected $fileType;

    /**
     *
     * @var string
     */
    protected $successLog;

    /**
     *
     * @var string
     */
    protected $errorLog;

    /**
     *
     * @var array
     */
    protected $modelPks;

    /**
     *
     * @var array
     */
    protected $foreignAttributes = [];

    /**
     *
     * @var array
     */
    protected $default = [];

    /**
     *
     * @var string
     */
    protected $type;

    /**
     *
     * @var string
     */
    protected $method = 'insert';

    protected $assign = [];

    protected $reader;

    protected $chunkFilter;

    protected $haltOnError = true;

    protected $rollbackOnError = true;
    /**
    * Enables/Dislabes the arhistory behaviour in case of upsert or update method
    */
    protected $disableHistory = false;

    /**
     * Used to reset a field to its default DB value if import file has the value of this property
     */
    protected $resetValue = '_reset';

    /**
     *
     * @var \Google_Service_Sheets
     */
    protected $googleService;

    protected $spreadsheetId;

    public $google_import = null;

    protected $googleSheetTitle;

    public static $total = 0;
    public static $totalInsertion = 0;
    public static $totalRows;

    public static $currentRow = 0;

    public static $firstRowValues = null;
    public const VALIDATION_TYPE_OFF = 'off';
    public const VALIDATION_TYPE_IMPORTED ='imported';
    public const VALIDATION_TYPE_FULL ='full';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'import';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'model'
                ],
                'required'
            ],
            [
                [
                    'model',
                    'log'
                ],
                'string'
            ],
            [
                [
                    'deleted_by'
                ],
                'integer'
            ],
            [
                [
                    'deleted_at'
                ],
                'safe'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('twbp', 'ID'),
            'model' => Yii::t('twbp', 'Model'),
            'log' => Yii::t('twbp', 'Log'),
            'created_by' => Yii::t('twbp', 'Created By'),
            'created_at' => Yii::t('twbp', 'Created At'),
            'updated_by' => Yii::t('twbp', 'Updated By'),
            'updated_at' => Yii::t('twbp', 'Updated At'),
            'deleted_by' => Yii::t('twbp', 'Deleted By'),
            'deleted_at' => Yii::t('twbp', 'Deleted At')
        ];
    }

    /**
     * Auto generated method, that returns a human-readable name as string
     * for this model.
     * This string can be called in foreign dropdown-fields or
     * foreign index-views as a representative value for the current instance.
     *
     * @return String
     * @author : taktwerk
     *         This method is auto generated with a modified Model-Generator by taktwerk.com
     */
    public function toString()
    {
        return $this->model; // this attribute can be modified
    }

    /**
     * Getter for toString() function
     * 
     * @return String
     */
    public function getToString()
    {
        return $this->toString();
    }

    /**
     *
     * @param array $post
     */
    public function setPostData(array $post)
    {
        $this->postData = $post;
        $this->model = $post['model'];
        $this->fileName = $post['file_name'];
        $this->processAttributes = $post['attributes'];
        $this->foreignAttributes = isset($post['foreign']) ? $post['foreign'] : [];
        $this->default = isset($post['default']) ? $post['default'] : [];
        $this->method = isset($post['method'])?$post['method']:'insert';
        $this->validationType = isset($post['validation_type']) ? $post['validation_type'] : self::VALIDATION_TYPE_FULL;
        $this->rollbackOnError = isset($post['rb_on_error']) ? $post['rb_on_error'] : false;
        $this->haltOnError = isset($post['halt_on_error']) ? $post['halt_on_error'] : false;
        $this->importValidateMethod = isset($post['importValidateMethod']) ? $post['importValidateMethod'] : '';
        $this->assign = isset($post['assign']) ? $post['assign'] : [];
        $this->firstRow = isset($post['start_row']) ? $post['start_row'] : false;
        $this->first_100_rows = isset($post['first_100_row']) ? true : false;
        $this->disableHistory = isset($post['disable_history']) ? $post['disable_history'] : false;
        $this->resetValue = isset($post['reset_value']) ? $post['reset_value'] : false;
        self::$currentRow = $this->firstRow;
        if (isset($post['type'])) {
            $this->type = $post['type'];
        }
        if (isset($post['google_import']) && $post['google_import'] == true) {
            $this->google_import = true;
            $this->fileName = $post['google_url'];
        } else {
            $this->delimiter = $post['delimiter'] === 'auto' ? $this->detectDelimiter() : $post['delimiter'];
        }
    }

    /**
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function process($queueJob = null)
    {
        $this->updateProgress($queueJob);
        $this->parseFile();
        
        if ($this->google_import) {
            $this->importGoogle($queueJob);
        } else {
            /**
             *
             * @var ActiveRecord $model ;
             * @var array $attributes
             */
            $total_rows = self::$totalRows;
            $chunkSize = 100;
            if($this->first_100_rows!==false){
                $total_rows = 100;
            }else{
                $total_rows = self::$totalRows;
            }
            for ($startRow = $this->firstRow ? $this->firstRow : 2; $startRow <= $total_rows; $startRow += $chunkSize) {
                /**
                 * Tell the Read Filter, the limits on which rows we want to read this iteration *
                 */
                $this->chunkFilter->setRows($startRow, $chunkSize);
                
                /**
                 * Load only the rows that match our filter from $inputFileName to a PHPExcel Object *
                 */
                try {
                    $objPHPExcel = $this->reader->load($this->getFilePath());
                    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, false);

                    $startIndex = ($startRow == 1) ? $startRow : $startRow - 1;
                    if (! empty($sheetData) && $startRow <= self::$totalRows) {
                        $data = array_slice($sheetData, $startIndex, $chunkSize);
                        $allowDbTransaction = ($this->haltOnError && $this->rollbackOnError);
                        if($allowDbTransaction){
                            $dbTransaction = \Yii::$app->db->beginTransaction();
                        }
                        foreach ($data as $key=>$row) {
                            if($this->isEmptyRow($row)) { continue; }
                            /** @var $modelName TwActiveRecord
                             */
                            if ($this->method == 'insert') {
                                // For insert method we always use new models
                                $modelName = $this->model;
                                $conditions = [];
                                $assignAttributes = [];
                                $index = 0;
                                foreach ($modelName::primaryKey() as $attribute) {
                                        $assignAttributes[$index] = $attribute;
                                        $index ++;
                                }
                                foreach ($assignAttributes as $attribute) {
                                    $mappedValue = $this->getMappedValue($row, $attribute);
                                    $conditions[$attribute] = $mappedValue;
                                }
                                if (! empty($conditions)) {
                                    $model = $modelName::find()->andFilterWhere($conditions)->one();
                                }

                                if (empty($model)) {
                                    // If we didn't find corresponding model, create new one
                                    $model = new $this->model();
                                    foreach ($model::primaryKey() as $key){
                                        $model->$key = $this->getMappedValue($row, $key);
                                    }
                                    $model->created_at =  new Expression('NOW()');
                                    $model->created_by = $this->created_by;
                                }
                                if($model && $this->disableHistory==true){
                                    if ($model->getBehavior('arhistory')){
                                        $model->detachBehavior('arhistory');
                                    }
                                }
                            } elseif ($this->method == 'update' || $this->method == 'upsert' ) {

                                // Find models for update method
                                $conditions = [];
                                $assignAttributes = [];
                                $index = 0;
                                foreach ($this->assign as $attribute => $value) {
                                    if ($value == '=') {
                                        $assignAttributes[$index] = $attribute;
                                    }
                                    $index ++;
                                }
                                foreach ($assignAttributes as $attribute) {
                                    $mappedValue = $this->getMappedValue($row, $attribute);
                                    $conditions[$attribute] = $mappedValue;
                                }

                                $modelName = $this->model;
                                if (! empty($conditions)) {
                                    $model = $modelName::find()->andFilterWhere($conditions)->all();
                                }
                                if (empty($model) && $this->method == 'upsert') {
                                    // If we didn't find corresponding model, create new one
                                    $model = [new $this->model()];
                                }
                                if(!empty($model)){
                                    if($this->method == 'update'){
                                        $removeModelKeys = [];
                                        foreach ($model as $x => $m){
                                            $isMChanged = false;
                                            foreach($this->processAttributes as $at=>$k){
                                                if($at!='updated_at' && $m->{$at} != $row[$k]){
                                                    $isMChanged = true;break;
                                                }
                                            }
                                            if($isMChanged==false){
                                                $removeModelKeys[] = $x;
                                            }
                                        }
                                        foreach( $removeModelKeys as $k){
                                            unset($model[$k]);
                                        }
                                    }
                                    $model = array_values($model);
                                    foreach($model as $m){
                                        if($this->disableHistory==true || $m->isNewRecord){
                                            if ($m->getBehavior('arhistory')){
                                                $m->detachBehavior('arhistory');
                                            }
                                        }
                                        $m->updated_by = $this->updated_by;
                                        $m->updated_at = $this->updated_at;
                                    }
                                }
                            }
                            if ($this->updateModels($model, $row, self::$currentRow) == false) {
                                if($this->haltOnError){
                                    if($allowDbTransaction){
                                       $dbTransaction->rollBack();
                                    }
                                    break (2);
                                }
                            }
                            ++ self::$currentRow;
                            $this->updateProgress($queueJob);
                        }
                        if(isset($allowDbTransaction) && $allowDbTransaction){
                            $dbTransaction->rollBack();
                        }
                    }
                    $objPHPExcel->disconnectWorksheets();
                    unset($objPHPExcel, $sheetData);
                } catch (\Exception $e) {
                    if(isset($allowDbTransaction) && $allowDbTransaction){
                        $dbTransaction->rollBack();
                    }
                    $this->stopProgress($e->getTraceAsString(), ImportProgress::STATUS_ERROR, $queueJob);
                    Notification::send($this->userId, Yii::t('twbp', 'Import failed'));
                    return [
                        'success' => 'danger',
                        'result' => $e->getMessage()
                    ];
                }
            }
        }
        $this->log = empty($this->errorLog) ? $this->successLog : $this->errorLog;
        if (Yii::$app->getModule('import')->keepLogs) {
            $this->save();
        }
        if (empty($this->errorLog)) {
            $msg = Yii::t('twbp', 'Total imported') . ': ' . self::$total;
            if($this->method == 'upsert'){
                $msg = Yii::t('twbp', 'Total imported') . ': ' . self::$total. ', '.Yii::t('twbp', 'Inserted').': '.self::$totalInsertion.', '.Yii::t('twbp', 'Updated').': '.(self::$total-self::$totalInsertion);
            }
            $this->stopProgress($msg, ImportProgress::STATUS_FINISHED, $queueJob);
            Notification::send($this->userId, Yii::t('twbp', 'Import finished') . '. ' . $msg);

            if(file_exists($this->getFilePath())){
                unlink($this->getFilePath());
            }

            if ($this->google_import) {
                $googleSpreadsheet = new GoogleSpreadsheet();
                $googleSpreadsheet->title = $this->googleSheetTitle;
                $googleSpreadsheet->url = $this->fileName;
                $googleSpreadsheet->type = GoogleSpreadsheet::TYPE_IMPORT;
                $googleSpreadsheet->user_id = $this->userId;
                $googleSpreadsheet->save();
            }
        } else {
            $this->stopProgress($this->errorLog, ImportProgress::STATUS_ERROR, $queueJob);
            Notification::send($this->userId, Yii::t('twbp', 'Import failed'));
        }
        // Need to reset $total counter for tests before we return it
        $totalReturn = self::$total;
        self::$total = 0;
        return [
            'success' => empty($this->errorLog) ? 'success' : 'danger',
            'total' => $totalReturn,
            'result' => empty($this->errorLog) ? (Yii::t('twbp', 'Total imported') . ': ' . $totalReturn) : nl2br($this->errorLog)
        ];
    }

    /**
     * Return first row of uploaded file
     * 
     * @return mixed
     */
    public function getFirstRow()
    {
        if ($this->type == 'google') {
            $this->initGoogle();
            return $this->readGoogleSingleRow(1);
        }
        $this->parseFile(true);
        return array_filter($this->readFirstRow()[0]);
    }

    /**
     *
     * @return mixed
     */
    protected function readFirstRow()
    {
        $objPHPExcel = $this->reader->load($this->getFilePath());
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, false);
        return $sheetData;
    }

    /**
     *
     * @return string
     */
    public function getFileType()
    {
        return $this->type;
    }

    /**
     *
     * @return int
     */
    protected function countCSVRows()
    {
        $c = 0;
        $fp = fopen($this->getFilePath(), "r");
        if ($fp) {
            while (! feof($fp)) {
                $content = fgets($fp);
                if ($content) {
                    $c ++;
                }
            }
        }
        fclose($fp);
        return $c;
    }

    /**
     *
     * @return array|mixed
     */
    protected function parseFile($is_first_row=false)
    {
        if ($this->google_import) {
            $this->initGoogle();
            self::$totalRows = $this->getGoogleTotalRows();
        } else {
            $inputFileName = $this->getFilePath();
            /**
             * Create a new Reader of the type that has been identified *
             */
            $this->reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader(\PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName));
            /**
             * Identify the type of $inputFileName *
             */
            $this->type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
            if (stripos($this->type,'CSV')!==false) {
                self::$totalRows = $this->countCSVRows();
                $this->reader->setDelimiter($this->delimiter);
            } else {
                $spreadsheetInfo = $this->reader->listWorksheetInfo($inputFileName);
                self::$totalRows = $spreadsheetInfo[0]['totalRows'];
            }
            /**
             * Create a new Instance of our Read Filter *
             */
            $this->chunkFilter = new ChunkReadFilter();
            if($is_first_row){
                $this->chunkFilter->setRows(0, 1);
            }
            /**
             * Tell the Reader that we want to use the Read Filter that we've Instantiated *
             */
            $this->reader->setReadFilter($this->chunkFilter);
            $this->reader->setReadDataOnly(true);
            $this->reader->setLoadSheetsOnly(null);
        }
    }

    /**
     *
     * @param
     *            $message
     */
    protected function appendSuccess($message)
    {
        if ($this->successLog == '') {
            $this->successLog = $message;
        } else {
            $this->successLog .= $message;
        }
        $this->successLog .= "\n";
    }

    /**
     *
     * @param
     *            $message
     */
    protected function appendError($message)
    {
        if ($this->errorLog == '') {
            $this->errorLog = $message;
        } else {
            $this->errorLog .= $message;
        }
        $this->errorLog .= "\n";
    }

    /**
     *
     * @param $model ActiveRecord
     * @return string
     */
    protected function getModelPks($model)
    {
        $pks = '';
        if (count($this->modelPks) > 1) {
            foreach ($this->modelPks as $pk) {
                $pks .= $model->$pk . '-';
            }
            $pks = rtrim($pks, '-');
        } else {
            $pks = $model->{$this->modelPks[0]};
        }
        return $pks;
    }

    /**
     *
     * @param string|int $input
     * @return int
     */
    protected function parseBoolean($input)
    {
        switch (strtolower($input)) {
            case 'yes':
            case 'true':
            case true:
            case 1:
                return 1;
                break;
            case 'no':
            case 'false':
            case false:
            case 0:
                return 0;
                break;
        }
        return false;
    }

    /**
     *
     * @param
     *            $input
     * @param string $type
     * @return bool|string
     */
    protected function parseTime($input, $type = 'datetime')
    {
        $input = str_replace(".","-",$input);
        $input = str_replace(";","-",$input);
        switch ($type) {
            case 'time':
                $format = "H:i:s";
                break;
            case 'date':
                $format = "Y-m-d";
                break;
            case 'datetime':
            default:
                $format = "Y-m-d H:i:s";
        }
        return date($format, strtotime($input));
    }

    /**
     *
     * @param ActiveRecord $model
     */
    protected function parseModelErrors(ActiveRecord $model)
    {
        foreach ($model->errors as $attribute => $errors) {

            if(Yii::$app->authManager->getAssignment('Authority', $this->userId)){
                $attribute_error = $model->getAttributeLabel($attribute).' ('.$attribute.')';
            }else{
                $attribute_error = $model->getAttributeLabel($attribute);
            }

            $this->appendError(str_repeat('&nbsp', 4) . "Field <strong>$attribute_error</strong> has following errors:");
            foreach ($errors as $error) {
                if(Yii::$app->authManager->getAssignment('Authority', $this->userId)){
                    $error = str_replace($model->getAttributeLabel($attribute),  $model->getAttributeLabel($attribute).' ('.$attribute.')', $error);
                }
                $this->appendError(str_repeat('&nbsp', 8).'- ' . $error);

            }
        }
        $this->appendError('<hr>');
    }

    public function getFilePath()
    {
        @mkdir(\Yii::getAlias('@root/runtime/upload/' . $this->userId));
        $fileName = str_replace(['(', ')', '-'], '_', $this->fileName);
        return \Yii::getAlias('@root/runtime/upload/' . $this->userId . '/' . md5($fileName));
    }

    /**
     *
     * @param
     *            $file
     * @return bool|string
     */
    public static function getFile($file)
    {
        $chmod = "0777";
        chmod(\Yii::getAlias('@app/../runtime/'), octdec($chmod));
        $filename = \Yii::getAlias('@root/runtime/upload/' . Yii::$app->user->id);
        if(!file_exists($filename)){
            FileHelper::createDirectory($filename,0777);
        }
        return $filename . '/' . md5($file);
    }

    /**
     *
     * @param string $model
     * @return array
     */
    public static function getModelAttributes($model, $showPk = false, $mainModel = true, $show_default_attributes = false)
    {
        /**
         *
         * @var $class ActiveRecord
         */
        $class = new $model();
        $avoidAttributes = [
            'deleted_at',
            'deleted_by',
            'updated_at',
            'updated_by',
            'created_at',
            'created_by'
        ];
        $attributes = [];
        $i = 0;
        if (! $mainModel) {
            $attributes[] = [
                'name' => $class::className() . '.toString',
                'label' => Yii::t('twbp', 'Auto detect'),
                'required' => false
            ];
            $i = 1;
        } else {
            $i = 0;
        }
        foreach ($class->attributes as $attribute => $value) {
            if (( !$show_default_attributes && ! in_array($attribute, $avoidAttributes)) || $show_default_attributes ) {
                if ($showPk || ! in_array($attribute, (array) $class->primaryKey())) {
                    if (! $mainModel) {
                        $attributes[$i]['name'] = $class::className() . '.' . $attribute;
                    } else {
                        $attributes[$i]['name'] = $attribute;
                    }
                    if (isset($class->attributeLabels()[$attribute])) {
                        $attributes[$i]['label'] = $class->attributeLabels()[$attribute];
                    } else {
                        $attributes[$i]['label'] = $attribute;
                    }
                    if (self::isForeignAttribute($attribute) && $mainModel) {
                        $attributes[$i]['foreign'] = true;
                        // Try to fetch foreign attributes
                        $relation = 'get' . Inflector::camelize(str_replace('_id', '', $attribute));
                        $foreignModel = $class->$relation()->modelClass;
                        $attributes[$i]['foreignAttributes'] = self::getModelAttributes($foreignModel, true, false, true);
                    }
                    $attributes[$i]['type'] = $class->getTableSchema()->getColumn($attribute)->type;
                    $attributes[$i]['required'] = $class->isAttributeRequired($attribute);
                    $i ++;
                }
            }
        }
        return $attributes;
    }

    /**
     *
     * @param
     *            $attribute
     * @return bool
     */
    public static function isForeignAttribute($attribute)
    {
        if (substr($attribute, - 3) == '_id') {
            return true;
        }
        return false;
    }

    /**
     *
     * @param
     *            $model
     * @return array
     */
    public static function getForeignModels($model)
    {
        /**
         *
         * @var ActiveRecord $class
         */
        $class = new $model();
        $reflection = new \ReflectionClass($class);
        $relationModels = [];
        foreach ($reflection->getMethods() as $method) {
            // Look only for methods starting with 'get'
            if (substr($method->name, 0, 3) !== 'get') {
                continue;
            }
            if ($model->hasMethod($method->name)) {
                $reflectionMethod = new \ReflectionMethod($model, $method->name);
                $checkNumArgs = $method->getNumberOfParameters();
                if ($checkNumArgs > 0) {
                    continue;
                } else {
                    $getMethod = $model->{$method->name}();
                    if (! ($getMethod instanceof ActiveQuery)) {
                        continue;
                    }
                }
            }
            // Don't use static methods
            $reflection = new \ReflectionMethod($class, $method->name);
            if ($reflection->isStatic()) {
                continue;
            }
            
            /**
             *
             * @var $relation ActiveQuery
             */
            $relation = @call_user_func([
                $class,
                $method->name
            ]);
            if ($relation instanceof ActiveQuery) {
                $relationReflection = new \ReflectionClass(new $relation->modelClass());
                if (! in_array($relationReflection->getName(), $relationModels)) {
                    $relationModels[] = $relationReflection->getName();
                }
            }
        }
        sort($relationModels);
        $relationAttributes = [];
        foreach ($relationModels as $relationModel) {
            /**
             *
             * @var $attributes array
             */
            $attributes = self::getModelAttributes($relationModel, true, true, false);
            sort($attributes);
            $foreignAttributes = [];
            $foreignAttributes[] = [
                'name' => $relationModel . '.toString',
                'label' => Yii::t('twbp', 'Auto detect'),
                'foreign' => false,
                'required' => false
            ];
            foreach ($attributes as $foreignAttribute) {
                $foreignAttribute['name'] = $relationModel . '.' . $foreignAttribute['name'];
                $foreignAttributes[] = $foreignAttribute;
            }
            $relationModel = explode('\\', $relationModel);
            $relationAttributes[$relationModel[count($relationModel) - 1]] = $foreignAttributes;
        }
        return $relationAttributes;
    }

    /**
     *
     * @return string Delimiter
     */
    protected function detectDelimiter()
    {
        $delimiters = array(
            ';' => 0,
            ',' => 0,
            "\t" => 0,
            "|" => 0
        );
        
        $handle = fopen($this->getFilePath(), "r");
        $firstLine = fgets($handle);
        fclose($handle);
        foreach ($delimiters as $delimiter => &$count) {
            $count = count(str_getcsv($firstLine, $delimiter));
        }
        return array_search(max($delimiters), $delimiters);
    }

    /**
     *
     * @param
     *            $row
     * @param
     *            $attribute
     * @return bool|string
     * @throws \yii\base\InvalidConfigException
     */
    protected function  getMappedValue($row, $attribute)
    {
        if (is_array($attribute)) {
            $attribute = $attribute['name'];
        }
        /**
         *
         * @var $model ActiveRecord
         */
        $model = new $this->model();
        $this->modelPks = $model::primaryKey();
        $schema = $model->getTableSchema();

        if(array_key_exists($attribute, $this->default) && isset($this->default[$attribute]) && !empty($this->default[$attribute])){
            if(in_array($schema->getColumn($attribute)->type,['time','datetime','date']) && $this->default[$attribute]=='1'){
                return $this->parseTime(date('Y-m-d H:i:s'), $schema->getColumn($attribute)->type);
            }else{
                return $this->default[$attribute];
            }
        }else{
            if (array_key_exists($attribute, $this->processAttributes) && isset($row[$this->processAttributes[$attribute]])) {
                if(stripos($row[$this->processAttributes[$attribute]],'NULL')!==false || stripos($row[$this->processAttributes[$attribute]],'null')!==false){
                    $row[$this->processAttributes[$attribute]]=null;
                }
                $tableColumn = $schema->getColumn($attribute);
                if (isset($this->foreignAttributes[$attribute]) || ! empty($this->foreignAttributes[$attribute])) {
                    $parts = explode('.', $this->foreignAttributes[$attribute]);
                    /**
                     *
                     * @var ActiveRecord $foreignModel
                     * @var ActiveRecord $foreignClass
                     */
                    $foreignClass = $parts[0];
                    $column = $parts[1];
                    $fileValue = $row[$this->processAttributes[$attribute]];
                    if ($column == 'toString') {
                        $foreignModel = null;
                        if (is_numeric($fileValue)) {
                            // If is numeric value find by primary key
                            $foreignModel = $foreignClass::findOne($fileValue);
                        }
                        // If there is no found foreign try to lookup by custom lookup method
                        if ($foreignModel == null || ! $foreignModel) {
                            $foreignModel = $foreignClass::fetchCustomImportLookup($row[$this->processAttributes[$attribute]])->one();
                        }
                    } else {
                        $foreignModel = $foreignClass::find()->andWhere([
                            $column => $row[$this->processAttributes[$attribute]]
                        ])->one();
                    }
                    // @TODO possible array return for composite key
                    if ($foreignModel) {
                        $mappedValue = $foreignModel->primaryKey;
                    } else {
                        $mappedValue = $row[$this->processAttributes[$attribute]];
                    }
                } else {
                    $mappedValue = $row[$this->processAttributes[$attribute]];
                }
                // Try to parse input data
                if ($tableColumn->phpType === 'boolean' || $tableColumn->dbType === 'tinyint(1)' || substr($tableColumn->name, 0, 3) == 'is_' || substr($tableColumn->name, 0, 4) == 'has_') {
                    // Parse boolean
                    $value = $this->parseBoolean($mappedValue);
                } elseif ($tableColumn->dbType === 'date' || $tableColumn->dbType === 'datetime' || $tableColumn->dbType === 'time') {
                    // Parse date, datetime or time
                    $value = $this->parseTime($mappedValue, $tableColumn->dbType);
                } else {
                    // Leave as is
                    $value = $mappedValue;
                }
                // Cast numbers to string since PHPExcel want to cast them to float
                return (string) $value;
            }
        }

        return false;
    }

    /**
     *
     * @param
     *            $models
     * @param
     *            $row
     * @param
     *            $rowNumber
     * @return bool
     * @throws \yii\db\Exception
     */
    protected function updateModels($models, $row, $rowNumber)
    {
        if (! is_array($models)) {
            $inputModels[] = $models;
        } else {
            $inputModels = $models;
        }
        $inputModels = array_filter($inputModels);
        $reflection = new \ReflectionClass(new $this->model());
        $shortModel = $reflection->getShortName();
        $attributes = $this->getModelAttributes($this->model, false, true, true);
        $error = 0;
        foreach ($inputModels as $model) {
            $attributeToValueMap = [];
            if($model==null){
                continue;
            }
            $inserting = $model->isNewRecord;
            /**@var $model ActiveRecord*/
            foreach ($attributes as $attribute) {
                $atrName = $attribute['name'];
                if ($mappedValue = $this->getMappedValue($row, $atrName)){
                    $resetValueSet = false;
                    if($mappedValue == $this->resetValue){
                        $col = $model->getTableSchema()->getColumn($atrName);
                        if($col){
                            if(is_string($col->defaultValue) || ($col->allowNull==true && $col->defaultValue===null)){
                                $mappedValue = $col->defaultValue;
                                $resetValueSet = true;
                                $model->{$atrName} = $mappedValue;
                            }
                        }
                    }
                    if($resetValueSet==false){
                        $model->{$atrName} = (string) $mappedValue;
                        if(empty($model->{$atrName})){
                            $columns_data = $model->getTableSchema()->columns[$atrName];
                            $type = $columns_data->type;
                            if($columns_data->allowNull){
                                $model->{$atrName} = null;
                            }elseif(in_array($type, ['integer','decimal','boolean'])){
                                $model->{$atrName} = 0;
                            }elseif(in_array($type, ['string'])){
                                $model->{$atrName} = ' ';
                            }elseif(in_array($type, ['datetime'])){
                                $model->{$atrName} = '0000-00-00 00:00:00';
                            }elseif(in_array($type, ['date'])){
                                $model->{$atrName} = '0000-00-00';
                            }elseif(in_array($type, ['time'])){
                                $model->{$atrName} = '00-00-00';
                            }
                        }
                    }

                    $attributeToValueMap[$atrName] = $model->{$atrName};
                }

            }
            try {
                if($this->importValidateMethod && method_exists($model, $this->importValidateMethod)){
                    $sheetRow = array_combine($this->getFirstRow(),$row);
                    $res = $model->{$this->importValidateMethod}($attributeToValueMap,$sheetRow,$this);
                    if($res===false){
                        $this->appendError("Error occurred on <strong>line " . ($rowNumber) . "</strong>: ");
                        $this->appendError("Row Data: ".implode(', ', $row));
                        if($this->haltOnError){
                            return false;
                        }
                        $error++;
                    }
                }
                if($model->hasErrors() 
                    || (($this->validationType === self::VALIDATION_TYPE_FULL || $this->validationType === '') && !$model->save()) 
                    || ($this->validationType === self::VALIDATION_TYPE_IMPORTED && (!$model->validate(array_keys($this->processAttributes)) || !$model->save(false))) 
                    || ($this->validationType === self::VALIDATION_TYPE_OFF && !$model->save(false))
                  ){
                    $this->appendError("Error occurred on <strong>line " . ($rowNumber) . "</strong>: ");
                    $this->appendError("Row Data: ".implode(', ', $row));
                    $this->parseModelErrors($model);
                    if($this->haltOnError){
                        return false;
                    }
                    $error++;
                } else {
                    $this->appendSuccess("Added new $shortModel under id(s): " . $this->getModelPks($model));
                    self::$total ++;
                    if($inserting){
                        self::$totalInsertion++;
                    }
                }
            } catch (\Exception $e) {
                $this->appendError("Error occurred on line " . ($rowNumber) . ": " . $e->getMessage());
                $this->appendError("Row Data: ".implode(', ', $row));
                if($this->haltOnError){
                    return false;
                }
                $error++;
            } catch (\Throwable $e) {
                $this->appendError("Error occurred on line " . ($rowNumber) . ": " . $e->getMessage());
                $this->appendError("Row Data: ".implode(', ', $row));
                if($this->haltOnError){
                    return false;
                }
                $error++;
            }
        }
        // TODO is rollback needed? or could it be an optional configuration?
        if($error) {
            if($this->haltOnError){
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @return mixed
     */
    public function getIsRunning()
    {
        $progress = ImportProgress::find()->andWhere([
            'user_id' => $this->userId,
            'model' => $this->model,
            'status' => ImportProgress::STATUS_RUNNING
        ])
            ->orderBy([
            'created_at' => SORT_DESC
        ])
            ->one();
        return $progress;
    }

    /**
     *
     * @param
     *            $queueJob
     */
    public function updateProgress($queueJob)
    {
        if ($queueJob) {
            $progress = self::$totalRows == 0 ? 0 : floor((self::$currentRow / self::$totalRows) * 100);
            $queueJob->progress = $progress;
            $queueJob->save();
        } else {
            $progress = $this->getProgress();
            if (! $progress) {
                $progress = new ImportProgress();
                $progress->user_id = $this->userId;
                $progress->model = $this->model;
                $progress->status = ImportProgress::STATUS_RUNNING;
                $progress->pid = getmypid();
            }
            $progress->total_rows = self::$totalRows;
            $progress->current_row = self::$currentRow + 1;
            $progress->save(false);
        }
    }

    /**
     *
     * @param
     *            $message
     * @param string $status
     * @param null $queueJob
     */
    public function stopProgress($message, $status = ImportProgress::STATUS_FINISHED, $queueJob = null)
    {
        if ($queueJob) {} else {
            $progress = $this->getProgress();
            $progress->status = $status;
            $progress->message = $message;
            $progress->save();
        }
    }

    /**
     *
     * @param bool $frontend
     * @return mixed
     */
    public function getProgress($frontend = false)
    {
        $progress = ImportProgress::find()->andWhere([
            'user_id' => $this->userId,
            'model' => $this->model,
            'status' => ($frontend ? [] : [
                ImportProgress::STATUS_RUNNING
            ])
        ])
            ->orderBy([
            'created_at' => SORT_DESC
        ])
            ->one();
        return $progress;
    }

    /**
     *
     * @return boolean
     */
    public static function isGoogleEnabled()
    {
        return file_exists(\Yii::getAlias(\Yii::$app->params['GoogleCredentials']));
    }

    /**
     */
    protected function initGoogle()
    {
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . \Yii::getAlias(\Yii::$app->params['GoogleCredentials']));
        $client = new \Google_Client();
        $client->useApplicationDefaultCredentials();
        $client->addScope(\Google_Service_Sheets::SPREADSHEETS_READONLY);
        $this->googleService = new \Google_Service_Sheets($client);
        preg_match('/\/([\w-_]{15,})\/(.*?gid=(\d+))?/', $this->fileName, $matches);
        if (! isset($matches[1]) || empty($matches[1])) {
            throw new Exception(Yii::t('twbp', 'Url is not good'));
        }
        $this->spreadsheetId = $matches[1];
        $this->googleSheetTitle = $this->googleService->spreadsheets->get($this->spreadsheetId)
            ->getProperties()
            ->getTitle();
    }

    /**
     *
     * @param $row integer
     * @return array
     */
    protected function readGoogleSingleRow($row)
    {
        $range = 'A' . $row . ':Z' . $row;
        return $this->googleService->spreadsheets_values->get($this->spreadsheetId, $range)->getValues()[0];
    }

    /**
     *
     * @return array
     */
    protected function readGoogleSheet()
    {
        $totalRows = $this->getGoogleTotalRows();
        $range = 'A' . ($this->firstRow ? 1 : 2) . ':Z' . $totalRows;
        return $this->googleService->spreadsheets_values->get($this->spreadsheetId, $range)->getValues();
    }

    /**
     *
     * @return integer
     */
    protected function getGoogleTotalRows()
    {
        return $this->googleService->spreadsheets->get($this->spreadsheetId)->getSheets()[0]->getProperties()
            ->getGridProperties()
            ->getRowCount();
    }

    /**
     */
    protected function importGoogle($queueJob)
    {
        $this->initGoogle();
        try {
            $data = $this->readGoogleSheet();
            foreach ($data as $key => $row) {
                if ($this->method == 'insert') {
                    // For insert method we always use new models
                    $model = new $this->model();
                } elseif ($this->method == 'update' || $this->method == 'upsert' ) {
                    // Find models for update method
                    /**
                     *
                     * @var $modelName ActiveRecord
                     */
                    $conditions = [];
                    $assignAttributes = [];
                    $index = 0;
                    foreach ($this->assign as $attribute => $value) {
                        if ($value == '=') {
                            $assignAttributes[$index] = $attribute;
                        }
                        $index ++;
                    }
                    foreach ($assignAttributes as $attribute) {
                        $mappedValue = $this->getMappedValue($row, $attribute);
                        $conditions[$attribute] = $mappedValue;
                    }
                    $modelName = $this->model;
                    if (! empty($conditions)) {
                        $model = $modelName::find()->andFilterWhere($conditions)->all();
                    }
                    if (empty($model) && $this->method == 'upsert') {
                        // If we didn't find corresponding model, create new one
                        $model = new $this->model();
                    }
                }
                if ($this->updateModels($model, $row, self::$currentRow) == false) {
                    break;
                }
                ++ self::$currentRow;
                $this->updateProgress($queueJob);
            }
        } catch (\Exception $e) {
            $this->stopProgress($e->getTraceAsString(), ImportProgress::STATUS_ERROR, $queueJob);
            Notification::send($this->userId, Yii::t('twbp', 'Import failed'));
            return [
                'success' => 'danger',
                'result' => $e->getMessage()
            ];
        }
    }

    public static function isEmptyRow($row) {
        foreach($row as $cell){
            if ($cell !== null || strlen(trim($cell))>=1){
                return false;
            }
        }
        return true;
    }
}
