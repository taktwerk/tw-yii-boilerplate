<?php
namespace taktwerk\yiiboilerplate\modules\webshell\assets;

use yii\web\AssetBundle;

/**
 * WebshellAsset is an asset bundle used to include custom overrides for terminal into the page.
 *
 * @author Alexander Makarov <sam@rmcreative.ru>
 */
class Assets extends AssetBundle
{
    public $sourcePath = '@taktwerk-boilerplate/modules/webshell/assets/web/css';

    public $css = [
        'webshell.css',
    ];

    public $depends = [
        'samdark\webshell\JqueryTerminalAsset',
    ];
}
