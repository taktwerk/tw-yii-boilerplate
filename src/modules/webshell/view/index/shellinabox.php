<?php
use yii\helpers\Html;

$this->title = \Yii::t('twbp', "Shell In A Box");
?>
<section class="content">
	<div class="box">
		<div class="box-body">

			<div>
				<?php
    if ($installed) {
        ?>
<div class="row">
<?php if($isRunning){?>
	<div class="col-md-4">
        <div class="input-group">
         <?php
         echo Html::textInput('port','4200',['title'=>'The port number at which terminal can be accessed', 'id'=>'port-input', 'class'=>'form-control','placeholder'=>'Port Number']);
         echo Html::tag('div',Html::button('Open Shell',['id'=>'open-shell-btn','class'=>'btn btn-success']),['class'=>'input-group-btn']);
        ?>
        </div>
    </div><?php }else{?>
    <div class="col-md-12">
    <p class="text-danger">It looks like the <b>Shell In A Box</b> program is installed but it's service is not running yet, Please start that to access the terminal.</p>
    </div>
    <?php }?>
</div>
<?php 
$js = <<<EOT
$('button#open-shell-btn').click(function(){
    var port = $('#port-input').val();
    window.open('{$ip}:'+port, '_blank');
});
EOT;
$this->registerJs($js);    
}else { ?>
<p class="text-danger">It looks like the <b>Shell In A Box</b> program is not installed on this server yet, Please follow the below instructions to install the program & then refresh this page</span>
<?php }?>
<?php if($isRunning==false){?>
<h4>Shell In A Box - Installation Steps</h4>
<pre>
# install
sudo apt-get install openssl shellinabox

# configure
sudo nano /etc/default/shellinabox

# start
sudo service shellinabox start | stop | restart
# or
sudo /etc/init.d/shellinabox start

# check status
sudo service shellinabox status
# or
/etc/init.d/shellinabox status
# or
ss -ltp | grep shellinabox
</pre>
				
				<p>
				Reference Links: 
					<a href="https://github.com/shellinabox/shellinabox/" target="_blank"
						rel="nofollow">Shellinabox Github</a>
				</p>
			</div>
		</div>
	</div>
	<?php }?>
</section>