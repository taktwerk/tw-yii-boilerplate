<?php
namespace taktwerk\yiiboilerplate\modules\webshell\controllers;

use Yii;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Response;
use taktwerk\yiiboilerplate\components\Helper;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * DefaultController
 *
 * @author Alexander Makarov <sam@rmcreative.ru>
 *
 * @property \samdark\webshell\Module $module
 */
class IndexController extends \samdark\webshell\controllers\DefaultController
{
    /**
     * Displays initial HTML markup
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = "@taktwerk-backend-views/layouts/main";
        return $this->render('index', [
            'quitUrl' => $this->module->quitUrl ? Url::toRoute($this->module->quitUrl) : null,
            'greetings' => $this->module->greetings
        ]);
    }

    /**
     * RPC handler
     * @return array
     */
    public function actionRpc()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $options = Json::decode(Yii::$app->request->getRawBody());

        switch ($options['method']) {
            case 'yii':
                list ($status, $output) = $this->runConsole(implode(' ', $options['params']));
                return ['result' => $output];
        }
    }
    public function actionShellBox(){
        $this->layout = "@taktwerk-backend-views/layouts/main";
        $program = 'shellinabox';
        $serverIp = $_SERVER['SERVER_ADDR'];
        if(\Yii::$app->request->serverName){
            $serverIp =  \Yii::$app->request->serverName;
        }
        $serverIp = 'https://'.$serverIp;
        $installed = false;
        $isRunning = false;
        $helperClass = ClassDispenser::getMappedClass(Helper::class);
        if($helperClass::shellProgramExists($program) || $helperClass::shellProgramExists('shellinaboxd')){
          $installed = true;
          exec("pgrep shellinabox", $output, $return);
          if ($return == 0) {
              $isRunning = true;
          }
        }
        return  $this->render('shellinabox',[
            'installed'=>$installed,
            'ip'=>$serverIp,
            'isRunning'=>$isRunning
        ]);
        //return $this->redirect('https://127.0.0.1:4200/');
    }

    /**
     * Runs console command
     *
     * @param string $command
     *
     * @return array [status, output]
     */
    private function runConsole($command)
    {
        $cmd = '"'.Yii::getAlias($this->module->yiiScript).'" ' . $command . ' 2>&1';

        $handler = popen($cmd, 'r');
        $output = '';
        while (!feof($handler)) {
            $output .= fgets($handler);
        }

        $output = trim($output);
        $status = pclose($handler);

        return [$status, $output];
    }
}