<?php

namespace taktwerk\yiiboilerplate\modules\analytics\commands;

use yii\console\Controller;
use Yii;
use taktwerk\yiiboilerplate\modules\analytics\models\AnalyticsPageview;
use \taktwerk\yiiboilerplate\modules\analytics\components\GoogleAnalytics;
use \taktwerk\yiiboilerplate\modules\analytics\widgets\ModelPageAnalyticsStatisticWidget;
use taktwerk\yiiboilerplate\models\Language;
use yii\helpers\Inflector;

/**
 * Task runner command for development.
 *
 * @author Jeremy Payne <j.payme@taktwerk.ch>
 */
class CacheDataController extends Controller
{
    protected $cache;

    public function actionIndex($truncateAnalyticsPageviewsTable = false)
    {
        $this->cache = Yii::$app->cache;

        if ($truncateAnalyticsPageviewsTable) {
            Yii::$app->db->createCommand('TRUNCATE TABLE `analytics_pageview`')->execute();
        }

        $this->savePageviews();
        $this->cacheFrontendPageviews();
        $this->cacheBackendEvents();
    }

    protected function savePageviews()
    {
        $googleAnalyticsOptParams = [
            'dimensions' => 'ga:pageTitle,ga:pagePath,ga:dimension1,ga:dimension2,ga:dimension3,ga:dimension4',
        ];

        $reportsData = Yii::$app->analytics->getReport('ga:pageviews', $googleAnalyticsOptParams);
        if (empty($reportsData['results']) || empty($reportsData['totalResultsCount'])) {
            return $dataProvider;
        }
        $reports = $reportsData['results'];

        $pageViewModels = [];
        $executedPages = [];
        $executedModels = [];
        $pageviewsToIds = [];

        foreach ($reports as $resultKey => $resultValue) {
            $pagePath = trim($resultValue['ga:pagePath']);
            $pagePathParts = explode('/', $pagePath);
            if (!empty($pagePathParts[1]) && in_array($pagePathParts[1], ['de', 'en'])) {
                unset($pagePathParts[1]);
            }
            $pagePath = implode('/', $pagePathParts);
            $pageViewModel = AnalyticsPageview::find()
                ->andWhere(['reference_model' => trim($resultValue['ga:dimension1'])])
                ->andWhere([
                    'or',
                    ['reference_model_identifire' => trim($resultValue['ga:dimension2'])],
                    ['is', 'reference_model_identifire', null]
                ])
                ->andWhere(['site_place' => trim($resultValue['ga:dimension3'])])
                ->andWhere(['controller_action' => trim($resultValue['ga:dimension4'])])
                ->one();

            if (!$pageViewModel) {
                $pageViewModel = AnalyticsPageview::find()
                    ->where(['uri' => $pagePath])
                    ->one();
            }

            if (!$pageViewModel) {
                $pageViewModel = new AnalyticsPageview();
            }

            if ($pageViewModel->id) {
                $previousValue = empty($pageviewsToIds[$pageViewModel->id]) ? 0 : $pageviewsToIds[$pageViewModel->id];
                $pageviewsToIds[$pageViewModel->id] = $previousValue + $resultValue['ga:pageviews'];
            } else {
                $pageViewModel->title = $resultValue['ga:pageTitle'];
                $pageViewModel->uri = $pagePath;
                $pageViewModel->reference_model = $resultValue['ga:dimension1'];
                if (trim($resultValue['ga:dimension2']) !== 'none') {
                    $pageViewModel->reference_model_identifire = $resultValue['ga:dimension2'];
                }
                $pageViewModel->site_place = $resultValue['ga:dimension3'];
                $pageViewModel->controller_action = $resultValue['ga:dimension4'];
                $pageViewModel->save();
                if ($pageViewModel->save()) {
                    $pageviewsToIds[$pageViewModel->id] = $resultValue['ga:pageviews'];
                }
            }
        }
        foreach ($pageviewsToIds as $id => $pageviews) {
            $analyticPageviewModel = AnalyticsPageview::find()->where(['id' => $id])->one();
            $analyticPageviewModel->pageviews = $pageviews;
            $analyticPageviewModel->save();
        }
    }

    protected function cacheFrontendPageviews()
    {
        $this->cacheFrontendTopEntitiesPageviews();
        $this->cacheFrontendEntitiesPageviewsByYear();
        $this->cacheFrontendEntitiesPageviewsByMonth();
        $this->cacheFrontendAllPageviewsByYear();
        $this->cacheNewsletterPageviewsByMonthImage();
    }

    protected function cacheFrontendTopEntitiesPageviews()
    {
        $entities = $this->module->referenceModels;

        $entitiesData = [];

        foreach ($entities as $entity) {
            $reportsData = Yii::$app->analytics->getModelPageviewsReport(
                $entity,
                null,
                'frontend',
                'view',
                '2005-01-01',
                'today',
                'ga:dimension2,ga:pageTitle',
                [],
                '-ga:pageviews',
                'ga:pageviews',
                $countOfResults
            );
            if (empty($reportsData['results'])) {
                continue;
            }
            $reportResults = $reportsData['results'];
            $resultsForSave = [];
            
            foreach ($reportResults as $report) {
                $isSavedPage = false;
                foreach ($resultsForSave as &$savedEntity) {

                    if ($savedEntity['ga:dimension2'] === $report['ga:dimension2']) {
                        $savedEntity['ga:pageviews'] += $report['ga:pageviews'];
                        $isSavedPage = true;
                    }
                }
                if (!$isSavedPage) {
                    $resultsForSave[] = $report;
                }
            }

            $pageviewsColumnData = array_column($resultsForSave, 'ga:pageviews');

            array_multisort($pageviewsColumnData, SORT_DESC, $resultsForSave);
            $entitiesData[$entity] = $resultsForSave;
        }

        if (!empty($entitiesData)) {
            Yii::$app->cache->set('analytics_top_entities', $entitiesData, null);
        }
    }

    protected function cacheFrontendEntitiesPageviewsByYear()
    {
        $entities = $this->module->referenceModels;

        foreach ($entities as $entity) {
            $reportsData = Yii::$app->analytics->getModelPageviewsReport(
                $entity,
                null,
                'frontend',
                'view',
                date('Y-m-d', strtotime('-1 year', time())),
                date('Y-m-d'),
                'ga:yearMonth',
                ['ga:year==' . date('Y')],
                '',
                'ga:pageviews'
            );
            if (empty($reportsData['results'])) {
                continue;
            }
            Yii::$app->cache->set(
                'analytics_pageviews_year_' . $entity,
                $reportsData['results']
            );
        }
    }

    protected function cacheFrontendAllPageviewsByYear()
    {
        $reportsData = Yii::$app->analytics->getModelPageviewsReport(
            $this->module->referenceModels,
            null,
            'frontend',
            'view',
            date('Y-m-d', strtotime('-1 year', time())),
            date('Y-m-d'),
            'ga:dimension1,ga:yearMonth',
            ['ga:year==' . date('Y')],
            '',
            'ga:pageviews'
        );
        if (empty($reportsData['results'])) {
            return;
        }
        Yii::$app->cache->set(
            "analytics_pageviews_year_all",
            $reportsData['results']
        );
    }

    protected function cacheFrontendEntitiesPageviewsByMonth()
    {
        $entities = $this->module->referenceModels;
        $entitiesData = [];

        foreach ($entities as $entity) {
            $reportsData = Yii::$app->analytics->getModelPageviewsReport(
                $entity,
                null,
                'frontend',
                'view',
                date('Y-m-01'),
                date('Y-m-t'),
                'ga:day',
                [],
                '',
                'ga:pageviews'
            );
            if (empty($reportsData['results'])) {
                continue;
            }
            Yii::$app->cache->set(
                'analytics_pageviews_month_' . $entity,
                $reportsData['results']
            );
        }
    }

    protected function cacheNewsletterPageviewsByMonthImage()
    {
        $languages = Language::getLanguages(true, true);
        $googleAnalyticsComponent = new GoogleAnalytics();
        $googleAnalyticsComponent->on(GoogleAnalytics::SAVE_IMAGE_CHART_EVENT, function($event) {
            $languageId = \Yii::$app->language;
            if ($event->imagePath) {
                Yii::$app->cache->set(
                    'newsletter_pageviews_by_month_image_' . $languageId,
                    $event->imagePath
                );
            }
        });
        foreach ($languages as $language) {
            \Yii::$app->language = $language['language_id'];
            
            $chartEntities = [
                [
                    'referenceModel' => $this->module->referenceModels,
                    'chartName' => 'Year pageviews statistic',
                    'chartLabelName' => 'Year pageviews statistic',
                    'multipleColumns' => [
                        'ga:dimension1',
                    ],
                    'labelCallback' => function (string $label) {
                        $label = Inflector::camel2words(Inflector::pluralize(\yii\helpers\StringHelper::basename($label)));
                        return $label;
                    },
                    'colorCallback' => function (array $chartData) {
                        if (empty($chartData) || empty($chartData['label'])) {
                            return null;
                        }
                        $chartData['label'];
                        $clrs = [
                            "rgba(165,194,0,1)",
                            "rgba(0,82,123,1)",
                            "rgba(128,56,109,1)",
                            "rgba(255,166,0,1)",
                            "rgba(179,181,198,1)"
                        ];
                        return $clrs[strlen($chartData['label'])%count($clrs)];
                    },
                ],
            ];

            foreach ($chartEntities as $chartEntity) {
                $widgetParams = [
                        'referenceModel' => $chartEntity['referenceModel'],
                        'outputFormat' => ModelPageAnalyticsStatisticWidget::IMAGE_OUTPUT_FORMAT,
                        'byMonth' => false,
                        'createChartColumnsByDimension' => true,
                        'chartColorCallback' => !empty($chartEntity['colorCallback']) ?
                                $chartEntity['colorCallback'] :
                                null,
                        'chartLabelCallback' => !empty($chartEntity['labelCallback']) ?
                            $chartEntity['labelCallback'] :
                            null,
                        'googleAnalyticsObject' => $googleAnalyticsComponent,
                        'cacheKey' => 'analytics_pageviews_year_all',
                ];
                if (!empty($chartEntity['multipleColumns'])) {
                    $widgetParams['dimensions'] = $chartEntity['multipleColumns'];
                }

                ModelPageAnalyticsStatisticWidget::widget($widgetParams);
            }
        }
    }

    protected function cacheBackendEvents()
    {
        $periodTypes = ['year', 'month'];
        $actionTypes = ['Create', 'Update', 'Delete'];
        
        foreach ($periodTypes as $periodType) {
            $dimensions = 'ga:eventCategory';
            $filters = [];
            $fromDate = null;
            $toDate = null;
            if ($periodType === 'year') {
                $fromDate = date('Y-m-d', strtotime('-1 year', time()));
                $toDate = date('Y-m-d');
                $dimensions .= ',ga:yearMonth';
                $filters = ['ga:year==' . date('Y')];
            } elseif ($periodType === 'month') {
                $fromDate = date('Y-m-01');
                $toDate = date('Y-m-t');
                $dimensions .= ',ga:day';
            }

            foreach ($actionTypes as $actionType) {
                $reportsData = Yii::$app->analytics->getModelPageviewsReport(
                    $this->module->referenceModels,
                    null,
                    'frontend',
                    $actionType,
                    $fromDate,
                    $toDate,
                    $dimensions,
                    $filters,
                    '',
                    'ga:totalEvents'
                );
                if (empty($reportsData['results'])) {
                    continue;
                }
                Yii::$app->cache->set(
                    "analytics_backend_events_{$periodType}_$actionType",
                    $reportsData['results']
                );
            }
        }
    }
}
