<?php

namespace taktwerk\yiiboilerplate\modules\analytics\events;

use yii\base\Component;
use yii\base\Event;

class SaveChartImageEvent extends Event
{
    public $imagePath;
    public $imageTag;
}
