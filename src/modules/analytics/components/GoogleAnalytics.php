<?php

namespace taktwerk\yiiboilerplate\modules\analytics\components;

use yii\base\Component;
use Yii;

use Google_Client;
use Google_Service_Analytics;
use Google_Service_AnalyticsReporting;
use Google_Service_AnalyticsReporting_DateRange;
use Google_Service_AnalyticsReporting_Dimension;
use Google_Service_AnalyticsReporting_GetReportsRequest;
use Google_Service_AnalyticsReporting_Metric;
use Google_Service_AnalyticsReporting_ReportData;
use Google_Service_AnalyticsReporting_ReportRequest;
use Google_Service_AnalyticsReporting_ReportRow;

use taktwerk\yiiboilerplate\modules\analytics\exceptions\AnalyticsException;

use \yii\web\JsExpression;
use taktwerk\yiiboilerplate\modules\analytics\helpers\factories\AnalyticsHelperFactory\AnalyticsHelperFactory;

class GoogleAnalytics extends Component
{
    protected $analytics;
    protected $reports;
    protected $client;

    public $analyticsProperty;
    public $serviceAccountCredantialsPath;
    public $viewId;
    public $helperClass;

    public $helper;

    protected $chartOptions;
    protected $basePlugins = "[{
            \"beforeDraw\": function(chartInstance, easing) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = 'white';

              var chartArea = chartInstance.chartArea;
              ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
            }
        }]";

    const SAVE_IMAGE_CHART_EVENT = 'SAVE_IMAGE_CHART_EVENT';

    public function init()
    {
        parent::init();

        $this->client = new Google_Client();
        $this->client->setApplicationName("Hello Analytics Reporting");
        $this->client->setAuthConfig($this->serviceAccountCredantialsPath);
        $this->client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        if ($this->helperClass && class_exists($this->helperClass)){
          $this->helper = new $this->helperClass();
          if(!($this->helper instanceof AnalyticsHelperFactory)){
              throw new \Exception('The analytics helper '.get_class($this->helper).' must implement AnalyticsHelperFactory interface');
          }
        }
    }

    function getReport($metrica = 'ga:pageviews', $optParams = [], $fromDate = '2005-01-01', $toDate = 'today'): array
    {
      $analytics = new Google_Service_Analytics($this->client);

      try {
          $reports = $analytics->data_ga->get(
               'ga:' . $this->viewId,
               $fromDate, // start date
               $toDate, // end date
               $metrica,
               $optParams
          );
        } catch (\Google_Service_Exception $e) {
          $message = $e->getMessage();
          $parsedMessage = json_decode($message, true);
          $throwMessage = '';
          if (!empty($parsedMessage['error']) && !empty($parsedMessage['error']['message'])) {
            $throwMessage = $parsedMessage['error']['message'];
          }
          throw new AnalyticsException($throwMessage);
        }
      

      $results = [];
      $rows = $reports->getRows();
      foreach ($rows as $key => $rowValue) {
          $resultsTemporary = [];
          foreach ($reports->getColumnHeaders() as $key => $value) {
              $resultsTemporary[$value->getName()] = $rowValue[$key];
          }
          $results[] = $resultsTemporary;
      }

      return [
        'totalResultsCount' => $reports->getTotalResults(),
        'results' => $results,
      ];
    }

    public function getModelPageviewsReport(
      $referenceModel = null,
      $modelId = null,
      $siteType = 'frontend',
      $actionId = 'view',
      $fromDate = 'today',
      $toDate = 'today',
      $dimesnsions = '',
      $additionalFilters = [],
      $sort = '',
      $metrics = 'ga:pageviews',
      $limit = null
    )
    {
      $googleAnalyticsOptParams = [];
      $filters = [];

      switch ($metrics) {
        case 'ga:totalEvents':
          if ($referenceModel) {
            if (is_array($referenceModel)) {
              $referenceModelQueryArray = [];
              foreach ($referenceModel as $model) {
                $referenceModelQueryArray[] = 'ga:eventCategory==' . $model;
              }
              $filters[] = implode(',', $referenceModelQueryArray);
            } else {
              $filters[] = 'ga:eventCategory==' . $referenceModel;
            }
          }
          if ($modelId) {
              $filters[] = 'ga:eventLabel==' . $modelId;
          }
          if ($actionId) {
              $filters[] = 'ga:eventAction==' . $actionId;
          }
          break;
        case 'ga:pageviews':
        default:
          if ($referenceModel) {
            if (is_array($referenceModel)) {
              $referenceModelQueryArray = [];
              foreach ($referenceModel as $model) {
                $referenceModelQueryArray[] = 'ga:dimension1==' . $model;
              }
              $filters[] = implode(',', $referenceModelQueryArray);
            } else {
              $filters[] = 'ga:dimension1==' . $referenceModel;
            }
          }
          if ($modelId) {
              $filters[] = 'ga:dimension2==' . $modelId;
          }
          if ($siteType) {
              $filters[] = 'ga:dimension3==' . $siteType;
          }
          if ($actionId) {
              $filters[] = 'ga:dimension4==' . $actionId;
          }
          break;
      }
      
      $filters = array_merge($filters, $additionalFilters);

      if ($filters) {
        $googleAnalyticsOptParams['filters'] = implode(';', $filters);
      }
      if ($dimesnsions) {
        $googleAnalyticsOptParams['dimensions'] = $dimesnsions;
      }
      if ($sort) {
        $googleAnalyticsOptParams['sort'] = $sort;
      }
      if ($limit) {
        $googleAnalyticsOptParams['max-results'] = $limit;
      }
      
      return $this->getReport(
          $metrics,
          $googleAnalyticsOptParams,
          $fromDate,
          $toDate
      );
    }

    public function getChartStatisticOptions(
      $data,
      $labels,
      $type = 'bar',
      JsExpression $clickBarCallback = null,
      \Closure $collorCallback = null,
      $stacked = false,
      $showLabels = false
    )
    {
      $chartWidgetStardardOptions = [];
      switch ($type) {
        case 'horizontalBar':
          $chartWidgetStardardOptions = [
              'type' => 'horizontalBar',
              'clientOptions' => [
                  'responsive' => true,
                  'chartArea' => [
                    'backgroundColor' => 'rgba(255, 255, 255, 1)',
                  ],
                  'maintainAspectRatio' => false,
                  'legend' => [
                    'display' => $showLabels,
                  ],
                  'scales' => [
                    'yAxes' => [
                      [
                        'stacked' => $stacked,
                        'ticks' => [
                          'z' => 1000000,
                          'mirror' => true,
                          'padding' => -10,
                          'min' => 0,
                          'stepSize' => 1,
                          'precision' => 0,
                        ],
                      ],
                    ],
                    'xAxes' => [
                      [
                        'stacked' => $stacked,
                        'ticks' => [
                          'min' => 0,
                          'stepSize' => 1,
                          'precision' => 0,
                          'callback' => new JsExpression(
                            'function(value) {
                              var maxSymbols = 20;
                              if (value.length > maxSymbols) {
                                return (""+value).substr(0, maxSymbols) + "...";
                              }
                              
                              return value;
                            }'
                          ),
                        ],
                      ],
                    ],
                  ],
              ],
              'data' => []
          ];
          break;
        default:
          $chartWidgetStardardOptions = [
              'type' => 'bar',
              'clientOptions' => [
                  'responsive' => true,
                  'maintainAspectRatio' => false,
                  'chartArea' => [
                    'backgroundColor' => 'rgba(255, 255, 255, 1)',
                  ],
                  'legend' => [
                    'display' => $showLabels,
                  ],
                  'scales' => [
                    'yAxes' => [
                      [
                        'stacked' => $stacked,
                        'ticks'=>['stepSize' => 1],
                      ],
                    ],
                    'xAxes' => [
                      [
                        'stacked' => $stacked,
                        'ticks' => [
                          'callback' => new JsExpression(
                            'function(value) {
                              var maxSymbols = 20;
                              if (value.length > maxSymbols) {
                                return (""+value).substr(0, maxSymbols) + "...";
                              }
                              
                              return value;
                            }'
                          ),
                        ],
                      ],
                    ],
                  ],
              ],
              'data' => []
          ];
      }

      $chartWidgetStardardOptions['plugins'] = new \yii\web\JsExpression($this->basePlugins);

      if ($clickBarCallback) {
        $chartWidgetStardardOptions['clientOptions']['onClick'] = $clickBarCallback;
      }
      $chartWidgetStardardOptions['data']['labels'] = $labels;
      if ($data) {
        $i = 0;
        foreach ($data as $chartValue) {
          $rgbaString = "rgba(179,181,198,1)";
          if ($collorCallback) {
            $rgbaString = $collorCallback($chartValue);
          } else if (count($data) > 1) {
              $i++;
              $maxColor = 255 * 255 * 255;
              $colorStep = ceil($maxColor / count($data));
              $currentColorNumber = $i * $colorStep;
              $currentHashColor = md5('color' . $currentColorNumber);
              $colors = [
                'r' => hexdec(substr($currentHashColor, 0, 2)), // r
                'g' => hexdec(substr($currentHashColor, 2, 2)), // g
                'b' => hexdec(substr($currentHashColor, 4, 2))
              ];
              $rgbaString = "rgba({$colors['r']},{$colors['g']},{$colors['b']},1)";
            }
          
            $chartWidgetStardardOptions['data']['datasets'][] = [
                'backgroundColor' => $rgbaString,
                'borderColor' => $rgbaString,
                'pointBackgroundColor' => $rgbaString,
                'pointBorderColor' => "#fff",
                'pointHoverBackgroundColor' => "#fff",
                'pointHoverBorderColor' => $rgbaString,
                'data' => $chartValue['value'],
                'label' => $chartValue['label'],
            ];

            if ($stacked) {
              // $chartWidgetStardardOptions['data']['datasets']['stack'] = 'Stack' . $i;
            }
          }
      }

      $this->chartOptions = $chartWidgetStardardOptions;

      return $chartWidgetStardardOptions;
    }

    public function toJsonChartOptions(array $chartOptions)
    {
      unset($chartOptions['plugins']);
      $chartOptionsJSON = json_encode($chartOptions);

      return $chartOptionsJSON;
    }

    public function randomColor(){
        $result = array('rgb' => '', 'hex' => '');
        foreach(array('r', 'b', 'g') as $col){
            $rand = mt_rand(0, 255);
            $result['rgb'][$col] = $rand;
            $dechex = dechex($rand);
            if(strlen($dechex) < 2){
                $dechex = '0' . $dechex;
            }
            $result['hex'] .= $dechex;
        }
        return $result;
    }

    public function getEvents($eventCategory = '')
    {
      $analytics = new Google_Service_Analytics($this->client);

      $reports = $analytics->data_ga->get(
           'ga:' . $this->viewId,
           '2005-01-01', // start date
           'today', // end date
           'ga:totalEvents',
           [
              'dimensions' => 'ga:eventCategory,ga:eventAction',
              // 'filters' => 'ga:eventCategory==' . $eventCategory
           ]
      );

      $results = [];
      $rows = $reports->getRows();
      foreach ($rows as $key => $rowValue) {
          $resultsTemporary = [];
          foreach ($reports->getColumnHeaders() as $key => $value) {
              $resultsTemporary[$value->getName()] = $rowValue[$key];
          }
          $results[] = $resultsTemporary;
      }

      return [
        'totalResultsCount' => $reports->getTotalResults(),
        'results' => $results,
      ];
    }

    public function trackEvent($eventCategory, $eventLabel, $eventAction)
    {
      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL,"http://www.google-analytics.com/collect");
      curl_setopt($ch, CURLOPT_POST, 1);
      $queryData = [
        'v' => 1,
        't' => 'event',
        'tid' => $this->analyticsProperty,
        'cid' => '555',
        'ec' => $eventCategory,
        'el' => $eventLabel,
        'ea' => $eventAction,
      ];

      curl_setopt(
        $ch,
        CURLOPT_POSTFIELDS,
        http_build_query($queryData)
      );

      // In real life you should use something like:
      // curl_setopt($ch, CURLOPT_POSTFIELDS, 
      //          http_build_query(array('postvar1' => 'value1')));

      // Receive server response ...
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      $server_output = curl_exec($ch);

      curl_close ($ch);
    }

    public function trackPageviews($class, $id, $pageTitle, $action, $sitePlace = 'frontend')
    {
      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL,"http://www.google-analytics.com/collect");
      curl_setopt($ch, CURLOPT_POST, 1);
      $queryData = [
        'v' => 1,
        't' => 'pageview',
        'tid' => $this->analyticsProperty,
        'cid' => '555',
        'dt' => $pageTitle,
        'dl' => \yii\helpers\Url::base(true) . Yii::$app->request->url,
        'cd1' => $class,
        'cd2' => $id,
        'cd3' => $sitePlace,
        'cd4' => $action
      ];

      curl_setopt(
        $ch,
        CURLOPT_POSTFIELDS,
        http_build_query($queryData)
      );

      // In real life you should use something like:
      // curl_setopt($ch, CURLOPT_POSTFIELDS, 
      //          http_build_query(array('postvar1' => 'value1')));

      // Receive server response ...
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      $server_output = curl_exec($ch);

      curl_close ($ch);
    }
}
