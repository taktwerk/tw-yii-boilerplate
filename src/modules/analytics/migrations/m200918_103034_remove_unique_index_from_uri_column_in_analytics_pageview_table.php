<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200918_103034_remove_unique_index_from_uri_column_in_analytics_pageview_table extends TwMigration
{
    public function up()
    {
        // drop not working?
        $this->dropIndex('uri', '{{%analytics_pageview}}');
        $this->alterColumn('{{%analytics_pageview}}', 'uri', $this->string(2048));
        $this->alterColumn('{{%analytics_pageview}}', 'title', $this->string(2048));
    }

    public function down()
    {
        echo "m200918_103034_remove_unique_index_from_uri_column_in_analytics_pageview_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
