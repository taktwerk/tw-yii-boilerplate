<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200722_101600_add_base_namespace_to_table_analytics_pageviews extends TwMigration
{
    public function up()
    {
        $query = <<<EOF
        
            ALTER TABLE `analytics_pageview`
                COMMENT='';
                
            ALTER TABLE `analytics_pageview`
                COMMENT='{"base_namespace":"taktwerk\\\\\\\\yiiboilerplate\\\\\\\\modules\\\\\\\\analytics"}';       
       
EOF;

        $this->execute($query);
    }

    public function down()
    {
        echo "m200722_101600_add_base_namespace_to_table_analytics_pageviews cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
