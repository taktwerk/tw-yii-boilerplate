<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200727_135005_add_metadata_columns_to_table_analytics_pageviews extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%analytics_pageview}}', 'reference_model', $this->string(255)->null());
        $this->addColumn('{{%analytics_pageview}}', 'reference_model_id', $this->integer()->null());
        $this->addColumn('{{%analytics_pageview}}', 'site_place', $this->string(255)->null());
        $this->addColumn('{{%analytics_pageview}}', 'controller_action', $this->string(255)->null());
    }

    public function down()
    {
        echo "m200727_135005_add_metadata_columns_to_table_analytics_pageviews cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
