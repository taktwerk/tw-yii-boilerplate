<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200827_124128_rename_reference_model_id_in_analytics_pageview_table extends TwMigration
{
    public function up()
    {
        $this->renameColumn('{{%analytics_pageview}}', 'reference_model_id', 'reference_model_identifire');
    }

    public function down()
    {
        echo "m200827_124128_rename_reference_model_id_in_analytics_pageview_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
