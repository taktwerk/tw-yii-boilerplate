<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200917_174030_increase_title_and_uri_columns_in_analytics_pageview_table extends TwMigration
{
    public function up()
    {
        $this->alterColumn('{{%analytics_pageview}}', 'uri', $this->string(765));
        $this->alterColumn('{{%analytics_pageview}}', 'title', $this->string(765));
    }

    public function down()
    {
        echo "m200917_174030_increase_title_and_uri_columns_in_analytics_pageview_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
