<?php

use taktwerk\yiiboilerplate\TwMigration;

class m200722_092440_add_table_analytics_pageview extends TwMigration
{
    public function up()
    {
        $this->createTable('{{%analytics_pageview}}', [
            'id' => $this->primaryKey(),
            'uri' => $this->string(255)->notNull()->unique(),
            'title' => $this->string(255)->null(),
            'pageviews' => $this->integer(255)->null(),
            'last_visited' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        echo "m200722_092440_add_table_analytics_pageview cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
