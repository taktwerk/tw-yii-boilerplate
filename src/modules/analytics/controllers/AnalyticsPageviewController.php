<?php

namespace taktwerk\yiiboilerplate\modules\analytics\controllers;

use taktwerk\yiiboilerplate\grid\GridView;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;

/**
 * This is the class for controller "AnalyticsPageviewController".
 */
class AnalyticsPageviewController extends \taktwerk\yiiboilerplate\modules\analytics\controllers\base\AnalyticsPageviewController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\analytics\models\AnalyticsPageview';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\analytics\models\search\AnalyticsPageview';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'frontend-pageviews-charts',
                            'backend-events-charts',
                        ],
                        'roles' => ['@']
                    ]
                ]
            ]
        ]);
    }

    public function init()
    {
        $this->crudColumnsOverwrite = [
            'index' => [
                'id' => false,
                'uri' => [
                    'attribute' => 'uri',
                    'content' => function ($model) {
                        $uri = mb_strimwidth($model->uri, 0, 50, "...");
                        return "<a href=\"{$model->uri}\" target=\"_blank\" data-pjax=\"0\">{$uri}</a>";
                    },
                ],
                'title' => [
                    'attribute' => 'title',
                    'content' => function ($model) {
                        $modelName = str_replace('/', '\\', $model->reference_model);

                        if ($modelName &&
                            $model->reference_model_identifire &&
                            class_exists($modelName)
                        ) {
                            $referenceModel = $modelName::find()
                                ->where(['id' => $model->reference_model_identifire])
                                ->one();
                            if ((boolean) $referenceModel) {
                                return $referenceModel->toString();
                            }

                    return '(' . Yii::t('twbp', 'Entry not found') . ')';
                  }
                },
              ],
              'site_place' => false,
              'pageviews' => [
                  'attribute' => 'pageviews',
                  'filter' => false,
              ],
              'controller_action' => false,
              'reference_model' => false,
              'reference_model_identifire' => false,
              'last_visited' => false,
              'after#pageviews' => [
                  'class' => '\kartik\grid\DataColumn',
                  'attribute' => 'reference_model',
                  'label' => Yii::t('twbp', 'Reference'),
                  'format' => 'html',
                  'value' => function ($model) {

                      $model_name = explode('models\\',$model->reference_model)[1];
                      if (!$model_name) {
                        $model_name = explode('models/',$model->reference_model)[1];
                      }
                      $linkTitle = $this->getModelLinkTitle($model_name, $model);
                      // $linkTitle = $model_name;
                      if ($model->controller_action === 'pdf') {
                        $linkTitle .= ' PDF';
                      }

                        if($model->reference_model_identifire){
                            if (class_exists(strtolower(explode('models\\',$model->reference_model)[0])).$model_name) {
                                $controller_name = strtolower(Inflector::slug(Inflector::camel2words($model_name), '-'));
                                $module_name = explode("modules\\",substr($model->reference_model, 0, strrpos($model->reference_model, '\\models')))[1];
                                $module_name = strtolower(str_replace(" ", "",ucwords(str_replace("-"," ",$module_name))));
                                if (!$module_name) {
                                    $module_name = 'backend';
                                }
                                if ($controller_name) {
                                    $url = \Yii::$app->urlManager->createAbsoluteUrl(
                                        "$module_name/$controller_name/view?id=$model->reference_model_identifire"
                                    );

                                    $link = Html::a($linkTitle, $url, ['data-pjax' => 0]);

                                    if ($link) {
                                        return $link;
                                    }
                                }
                            }
                        }
                        return $linkTitle;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'data' => (Yii::$app->analytics->helper)?Yii::$app->analytics->helper->getModelFilterOptions()
                        :$this->module->referenceModels,
                        'initValueText' => null,
                        'options' => [
                            'placeholder' => '',
                            'multiple' => true,
                        ],
                    ]
                ],
            ],

        ];
    }

    protected function getModelLinkTitle($modelName, $model)
    {
        if (!(class_exists(strtolower(explode('models\\',$model->reference_model)[0])).$modelName)) {
            return '';
        }
        $modelName = ucwords(Inflector::slug(Inflector::camel2words($modelName), ' '));

        if (!$modelName) {
            return '';
        }
        return Yii::t('app', $modelName);
  }

    public function actionFrontendPageviewsCharts()
    {
        return $this->render('frontend-pageviews-charts');
    }

    public function actionBackendEventsCharts()
    {
        return $this->render('backend-events-charts');
    }
}
