<?php
//Generation Date: 11-Sep-2020 04:35:51pm
namespace taktwerk\yiiboilerplate\modules\analytics\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * AnalyticsPageviewController implements the CRUD actions for AnalyticsPageview model.
 */
class AnalyticsPageviewController extends TwCrudController
{
}
