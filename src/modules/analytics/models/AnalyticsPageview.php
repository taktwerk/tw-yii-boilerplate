<?php

namespace taktwerk\yiiboilerplate\modules\analytics\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\analytics\models\base\AnalyticsPageview as BaseAnalyticsPageview;

/**
 * This is the model class for table "analytics_pageview".
 */
class AnalyticsPageview extends BaseAnalyticsPageview
{
   /**
    * List of additional rules to be applied to model, uncomment to use them
    * @return array
    */
   public function rules()
   {
       return array_merge(parent::rules(), [
           [['uri'], 'unique'],
       ]);
   }

	public function deletable()
	{
		return false;
	}

	public function editable()
	{
		return false;
	}

	public function readable()
	{
		return (boolean) self::find()->count() && $this->id;
	}

	public static function primaryKey()
	{
		return ['id'];
	}

}
