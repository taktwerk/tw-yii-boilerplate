<?php
//Generation Date: 28-May-2021 06:11:30am
namespace taktwerk\yiiboilerplate\modules\analytics\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\analytics\models\AnalyticsPageview as AnalyticsPageviewModel;

/**
 * AnalyticsPageview represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\analytics\models\AnalyticsPageview`.
 */
class AnalyticsPageview extends AnalyticsPageviewModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'uri',
                    'title',
                    'pageviews',
                    'last_visited',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'reference_model',
                    'reference_model_identifire',
                    'site_place',
                    'controller_action',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$showDeleted = (property_exists(\Yii::$app->controller, 'model')
            &&
            (is_subclass_of(AnalyticsPageviewModel::class, \Yii::$app->controller->model) 
            	||
            	is_subclass_of(\Yii::$app->controller->model, AnalyticsPageviewModel::class))
            &&
              (Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model)
                ||
                Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model)))
            );
        $query = AnalyticsPageviewModel::find((Yii::$app->get('userUi')->get('show_deleted', AnalyticsPageviewModel::class) === '1'|| $showDeleted)?false:true);

        $this->parseSearchParams(AnalyticsPageviewModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(AnalyticsPageviewModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(AnalyticsPageviewModel::class),
                'params' => [
                    'page' => $this->parsePageParams(AnalyticsPageviewModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('pageviews', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyHashOperator('reference_model_identifire', $query);
        $this->applyLikeOperator('uri', $query);
        $this->applyLikeOperator('title', $query);
        $this->applyLikeOperator('reference_model', $query);
        $this->applyLikeOperator('site_place', $query);
        $this->applyLikeOperator('controller_action', $query);
        $this->applyDateOperator('last_visited', $query, true);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(AnalyticsPageviewModel::class);
    }
}
