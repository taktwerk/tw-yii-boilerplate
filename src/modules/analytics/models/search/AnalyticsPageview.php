<?php

namespace taktwerk\yiiboilerplate\modules\analytics\models\search;

use taktwerk\yiiboilerplate\modules\analytics\models\search\base\AnalyticsPageview as AnalyticsPageviewModel;
use yii\data\ActiveDataProvider;
use Yii;
use taktwerk\yiiboilerplate\modules\analytics\models\AnalyticsPageview as APM;
/**
 * AnalyticsPageview represents the model behind the search form about `taktwerk\yiiboilerplate\modules\analytics\models\AnalyticsPageview`.
 */
class AnalyticsPageview extends AnalyticsPageviewModel
{
    public function search($params)
    {
        $isExistAnalyticsRows = (boolean) AnalyticsPageviewModel::find()->count();

        if ($isExistAnalyticsRows) {
            return $this->baseSearch($params);
        }

        return $this->searchOnline($params);
    }

    public function baseSearch($params)
    {
        $query = AnalyticsPageviewModel::find();

        $this->parseSearchParams(\taktwerk\yiiboilerplate\modules\analytics\models\AnalyticsPageview::class, $params);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(APM::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(APM::class),
                'params' => [
                    'page' => $this->parsePageParams(APM::class),
                ]
            ],
        ]);

        $referenceModelFilter = !empty($params[$this->formName()]['reference_model']) ?
            $params[$this->formName()]['reference_model'] :
            [];

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('pageviews', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyHashOperator('reference_model_identifire', $query);
        $this->applyLikeOperator('uri', $query);
        $this->applyLikeOperator('title', $query);
        if ($referenceModelFilter && Yii::$app->analytics->helperClass) {
            $query = Yii::$app->analytics->helper->getSearchQueryReferenceModelByCategory(
                [
                    'type' => 'database',
                    'query' => $query,
                    'filter' => $referenceModelFilter,
                ]
            );
        }
        $query->andWhere(['site_place' => 'frontend']);
        $query->andWhere(['not', ['reference_model_identifire' => null]]);
        $this->applyLikeOperator('site_place', $query);
        $this->applyLikeOperator('controller_action', $query);
        $this->applyDateOperator('last_visited', $query, true);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        $query->select([
                'id',
                'reference_model',
                'reference_model_identifire',
                'title',
                'pageviews',
                "REPLACE(REPLACE(uri, '/en', ''), '/de', '') as uri",
                "SUM(pageviews) as pageviews",
                'controller_action']
        );
        $query->groupBy(['id', 'reference_model', 'reference_model_identifire', 'controller_action', 'uri']);

        return $dataProvider;
    }

    public function searchOnline($params)
    {
        $currentPage = 0;
        if (!empty($params) && !empty($params['page'])) {
            $currentPage = $params['page'] - 1;
        }
        $query = AnalyticsPageviewModel::find();

        $this->parseSearchParams(AnalyticsPageviewModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(AnalyticsPageviewModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(AnalyticsPageviewModel::class),
                'params' => [
                    'page' => $this->parsePageParams(AnalyticsPageviewModel::class),
                ]
            ],
        ]);

        $referenceModelFilter = !empty($params[$this->formName()]['reference_model']) ?
            $params[$this->formName()]['reference_model'] :
            [];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyHashOperator('pageviews', $query);
        $this->applyHashOperator('reference_model_identifire', $query);
        $this->applyLikeOperator('uri', $query);
        $this->applyLikeOperator('title', $query);
        $query->andFilterWhere(['in','reference_model',implode(',',$this->reference_model)]);
        $this->applyLikeOperator('site_place', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        $this->applyDateOperator('last_visited', $query, true);

        if (Yii::$app->hasModule('analytics') && !empty(Yii::$app->analytics)) {
            $filters = [];
            $filtersQuery = '';
            $maxRegularExpressionWidth = 127;
            if (!empty($this->title)) {
                $filters[] = 'ga:pageTitle=~' . substr($this->title, 0, $maxRegularExpressionWidth);
            }
            if (!empty($this->uri)) {
                $filters[] = 'ga:pagePath=~' . substr($this->uri, 0, $maxRegularExpressionWidth);
            }
            if (!empty($this->pageviews)) {
                $filters[] = 'ga:pageviews==' . substr($this->pageviews, 0, $maxRegularExpressionWidth);
            }
            if (!empty($referenceModelFilter)) {
                if (Yii::$app->analytics->helperClass) {
                    $filters = Yii::$app->analytics->helper->getSearchQueryReferenceModelByCategory(
                        [
                            'type' => 'online',
                            'query' => $filters,
                            'filter' => $referenceModelFilter,
                        ]
                    );
                }
            }
            if (!empty($this->reference_model_identifire)) {
                $filters[] = 'ga:dimension2=~' . substr($this->reference_model_identifire, 0, $maxRegularExpressionWidth);
            } else {
                $filters[] = 'ga:dimension2!=none';
            }
            // $this->site_place = 'frontend';
            if (!empty($this->site_place)) {
                $filters[] = 'ga:dimension3=~' . substr($this->site_place, 0, $maxRegularExpressionWidth);
            } else {
                $filters[] = 'ga:dimension3==frontend';
            }
            $filtersQuery = implode(';', $filters);
            $allPages = $countOfAllViewedPagesReport[0]['ga:exits'];
            $googleAnalyticsOptParams = [
                'start-index' => $currentPage * $this->getPageSize() + 1,
                'max-results' => $this->getPageSize(),
                'dimensions' => 'ga:pageTitle,ga:pagePath,ga:dimension1,ga:dimension2,ga:dimension3,ga:dimension4',
            ];
            if (!empty($params['sort'])) {
                if ($params['sort'] === 'uri') {
                    $googleAnalyticsOptParams['sort'] = 'ga:pagePath';
                }
                if ($params['sort'] === '-uri') {
                    $googleAnalyticsOptParams['sort'] = '-ga:pagePath';
                }
                if ($params['sort'] === 'title') {
                    $googleAnalyticsOptParams['sort'] = 'ga:pageTitle';
                }
                if ($params['sort'] === '-title') {
                    $googleAnalyticsOptParams['sort'] = '-ga:pageTitle';
                }
                if ($params['sort'] === 'reference_model') {
                    $googleAnalyticsOptParams['sort'] = 'ga:dimension1';
                }
                if ($params['sort'] === '-reference_model') {
                    $googleAnalyticsOptParams['sort'] = '-ga:dimension1';
                }
                if ($params['sort'] === 'reference_model_identifire') {
                    $googleAnalyticsOptParams['sort'] = 'ga:dimension2';
                }
                if ($params['sort'] === '-reference_model_identifire') {
                    $googleAnalyticsOptParams['sort'] = '-ga:dimension2';
                }
                if ($params['sort'] === 'site_place') {
                    $googleAnalyticsOptParams['sort'] = 'ga:dimension3';
                }
                if ($params['sort'] === '-site_place') {
                    $googleAnalyticsOptParams['sort'] = '-ga:dimension3';
                }
                if ($params['sort'] === 'pageviews') {
                    $googleAnalyticsOptParams['sort'] = 'ga:pageviews';
                }
                if ($params['sort'] === '-pageviews') {
                    $googleAnalyticsOptParams['sort'] = '-ga:pageviews';
                }
            } else {
                $googleAnalyticsOptParams['sort'] = '-ga:pageviews';
            }
            if (!empty($filtersQuery)) {
                $googleAnalyticsOptParams['filters'] = $filtersQuery;
            }
            $reportsData = Yii::$app->analytics->getReport('ga:pageviews', $googleAnalyticsOptParams);
            if (empty($reportsData['results']) || empty($reportsData['totalResultsCount'])) {
                return $dataProvider;
            }
            $reports = $reportsData['results'];

            $pageViewModels = [];
            // echo '<pre>';
            // var_dump($reports);die;
            foreach ($reports as $resultKey => $resultValue) {
                $pageViewModel = new AnalyticsPageviewModel();
                $pageViewModel->title = $resultValue['ga:pageTitle'];
                $pageViewModel->uri = $resultValue['ga:pagePath'];
                $pageViewModel->pageviews = $resultValue['ga:pageviews'];
                $pageViewModel->reference_model = $resultValue['ga:dimension1'];
                if ($resultValue['ga:dimension2'] !== 'none') {
                    $pageViewModel->reference_model_identifire = $resultValue['ga:dimension2'];
                }
                $pageViewModel->site_place = $resultValue['ga:dimension3'];
                $pageViewModel->controller_action = $resultValue['ga:dimension4'];
                /*$pageViewModel->last_visited = substr($resultValue['ga:dateHourMinute'], 0, 4) .
                    '-' . substr($resultValue['ga:dateHourMinute'], 4, 2) .
                    '-' . substr($resultValue['ga:dateHourMinute'], 6, 2) .
                    ' ' . substr($resultValue['ga:dateHourMinute'], 8, 2) .
                    ':' . substr($resultValue['ga:dateHourMinute'], 10, 2) .
                    ':00';*/

                $pageViewModels[] = $pageViewModel;
            }
            if ($pageViewModels) {
                $dataProvider->setModels($pageViewModels);
            }
            $dataProvider->setTotalCount($reportsData['totalResultsCount']);
            $pages = new yii\data\Pagination(['totalCount' => $reportsData['totalResultsCount']]);
            $pages->setPage($currentPage);
            $dataProvider->setPagination($pages);
        }

        return $dataProvider;
    }
}
