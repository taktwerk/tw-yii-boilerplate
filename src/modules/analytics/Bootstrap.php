<?php

namespace taktwerk\yiiboilerplate\modules\analytics;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        if ($app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'taktwerk\yiiboilerplate\modules\analytics\commands';
        }
    }
}
