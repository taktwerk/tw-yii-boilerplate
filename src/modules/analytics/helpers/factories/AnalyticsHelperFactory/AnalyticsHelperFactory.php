<?php

namespace taktwerk\yiiboilerplate\modules\analytics\helpers\factories\AnalyticsHelperFactory;

interface AnalyticsHelperFactory
{
    public function getModelFilterOptions():array;
	public function getSearchQueryReferenceModelByCategory(array $searchOptions);
}