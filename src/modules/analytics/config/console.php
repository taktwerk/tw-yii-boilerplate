<?php

return [
    'bootstrap' => [],
    'controllerMap' => [
        'cache-data' => [
            'class' => 'taktwerk\yiiboilerplate\modules\analytics\commands\CacheDataController',
        ],
    ],
];
