<?php

namespace taktwerk\yiiboilerplate\modules\analytics;
use Yii;

class Module extends \taktwerk\yiiboilerplate\components\TwModule
{
	public $bootstrap;
    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\analytics\controllers';
    public $referenceModels = [];
    public function init()
    {
        parent::init();
        Yii::configure($this, require(__DIR__ . '/config/console.php'));
        // custom initialization code goes here
    }

    public function bootstrap($app)
    {
        if ($app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'app\modules\analytics\commands';
        }
    }
}
