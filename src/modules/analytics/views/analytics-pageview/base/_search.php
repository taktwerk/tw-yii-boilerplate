<?php
//Generation Date: 17-Sep-2020 05:48:07pm
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\analytics\models\search\AnalyticsPageview $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="analytics-pageview-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'uri') ?>

        <?= $form->field($model, 'title') ?>

        <?= $form->field($model, 'pageviews') ?>

        <?= $form->field($model, 'last_visited') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

        <?php // echo $form->field($model, 'reference_model') ?>

        <?php // echo $form->field($model, 'reference_model_identifire') ?>

        <?php // echo $form->field($model, 'site_place') ?>

        <?php // echo $form->field($model, 'controller_action') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
