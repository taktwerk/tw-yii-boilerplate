<?php

namespace taktwerk\yiiboilerplate\modules\analytics\widgets;

use Yii;
use dosamigos\chartjs\ChartJs;
use yii\base\Widget;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use taktwerk\yiiboilerplate\components\Helper;
use yii\helpers\Url;
use \taktwerk\yiiboilerplate\modules\analytics\components\GoogleAnalytics;
use yii\base\Event;
use taktwerk\yiiboilerplate\modules\analytics\events\SaveChartImageEvent;
use taktwerk\yiiboilerplate\modules\analytics\exceptions\AnalyticsException;
use yii\helpers\Html;

class ModelPageAnalyticsStatisticWidget extends Widget
{
	const JS_OUTPUT_FORMAT = 'js';
	const IMAGE_OUTPUT_FORMAT = 'image';
	const BASE64_OUTPUT_FORMAT = 'base_64';

	public $cacheKey;
    public $referenceModel = null;
    public $modelId = null;
    public $byYear = true;
    public $byMonth = true;
    public $filters = [];
    public $dimensions = [];
    public $siteType = 'frontend';
    public $metrics = 'ga:pageviews';
    public $action = 'view';
    public $createChartColumnsByDimension = false;
    public $chartColorCallback = null;
    public $chartLabelCallback = null;
    public $stacked = false;
    public $displayLegend = false;
    public $googleAnalyticsObject;
    public $fromDate;
    public $toDate;
    public $chartClass = 'col-lg-6';
    public $title = '';

    public $outputFormat = self::JS_OUTPUT_FORMAT;

    protected $demensionsByType;
    protected $filtersByType;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        try{
    	    if ($this->byYear) {
    	        if(!$this->title) {
                    $this->title = Yii::t('twbp', 'Per year');
                }
    	    	return $this->getContent('year', $this->title);
    	    }
    
    	    if ($this->byMonth) {
                if(!$this->title) {
                    $this->title = Yii::t('twbp', 'Per month');
                }
    	    	return $this->getContent('month', $this->title);
    	    }
        } catch (AnalyticsException $e) {
            return '<span class="text-danger">Error getting the statistics: '. Html::encode($e->getMessage()).'</span>';
        }
	    return '';
	}

	protected function getContent($periodDemensionType, $title)
	{
		$reports = $this->getReports($periodDemensionType);
		if (!$reports) {
			return '';
		}
	    
	    $chartData = $this->parseReports($reports, $periodDemensionType);

	    return $this->drawChart(
	    	$chartData['datasets'],
	    	$chartData['labels'],
	    	$title
	    );
	}

	protected function parseReports($reports, $type = null)
	{
		$labels = [];
	    $datasets = [];
	    $datasetsTemplate = [];
	    $dataMainKey = '';
	    $resultLabelCallback = null;
	    $labelTitle = '';

	    switch ($type) {
	    	case 'year':
	    		$labelTitle = Yii::t('twbp', 'Per month');
	    		$dataMainKey = 'ga:yearMonth';
	    		$resultLabelCallback = function ($labelValue) {
	    			return "" . substr($labelValue, 0, 4) .
			            '-' .
			            substr($labelValue, 4, 2);
	    		};
				$ts1 = strtotime($this->fromDate);
				$ts2 = strtotime($this->toDate);
				$year1 = date('Y', $ts1);
				$year2 = date('Y', $ts2);
				$month1 = date('m', $ts1);
				$month2 = date('m', $ts2);
				$diff = (($year2 - $year1) * 12) + ($month2 - $month1);

				if (!$ts1 || !$ts2) {
					$diff = 11;
				}
				
	    		for ($i = $diff; $i >= 0; $i--) {
	    			$lastTime = $this->toDate ? strtotime($this->toDate) : time();
			    	$labels[] = date('Y-m', strtotime("-{$i} month", $lastTime));
			    	$datasetsTemplate[] = null;
			    }
			    break;
	    	case 'month':
	    		$labelTitle = Yii::t('twbp', 'Daily statistics');
	    		$dataMainKey = 'ga:day';

	    		$lastDayOfTheMonth = (int) date("t");
	    		for ($i = 1; $i <= $lastDayOfTheMonth; $i++) {
			    	$labels[] = $i;
			    	$datasetsTemplate[] = null;
			    }
			    break;
	    	default:
	    		return [
			    	'datasets' => [],
			    	'labels' => [],
			    ];
	    }

	    foreach ($reports as $resultKey => $resultValue) {
	    	if ($resultLabelCallback) {
	    		$resultLabel = $resultLabelCallback($resultValue[$dataMainKey]);
	    	} else {
	    		$resultLabel = $resultValue[$dataMainKey];
	    	}
	        $value = $resultValue[$this->metrics];
        	unset($resultValue[$dataMainKey]);
        	unset($resultValue[$this->metrics]);
        	$label = $this->dimensions ? implode('_', $resultValue) : 0;
        	if (empty($datasets[$label]) || empty($datasets[$label]['label'])) {
        		$datasets[$label]['label'] = $this->dimensions ?
        			$label :
        			$labelTitle;
        		if ($this->chartLabelCallback) {
        			$chartLabelCallback = $this->chartLabelCallback;
        			$datasets[$label]['label'] = $chartLabelCallback(
        				$datasets[$label]['label']
        			);
        		}
        	}
        	if (empty($datasets[$label]) || empty($datasets[$label]['value'])) {
        		$datasets[$label]['value'] = $datasetsTemplate;
        	}
        	$datasets[$label]['value'][array_search($resultLabel, $labels)] = $value;
	    }

	    return [
	    	'datasets' => $datasets,
	    	'labels' => $labels,
	    ];
	}

	protected function getReports($type = null)
	{
		if ($this->cacheKey && Yii::$app->cache->get($this->cacheKey)) {
			return $this->getCacheReports();
		}

		switch ($type) {
			case 'year':
				$this->demensionsByType = array_merge(['ga:yearMonth'], $this->dimensions);
		    	$this->filtersByType = array_merge(['ga:year==' . date('Y')], $this->filters);
		    	if (!$this->fromDate) {
		    		$this->fromDate = date('Y-m-d', strtotime('-1 year', time()));
		    	}
		    	if (!$this->toDate) {
		    		$this->toDate = date('Y-m-d');
		    	}
				break;
			case 'month':
				$this->demensionsByType = array_merge(['ga:day'], $this->dimensions);
				$this->filtersByType = $this->filters;
		    	$this->fromDate = date('Y-m-01');
		    	$this->toDate = date('Y-m-t');
				break;
			default:
				return [];
		}

		return $this->getReportsOnline();
	}

	protected function getCacheReports()
	{
		if (!$this->cacheKey) {
			return [];
		}

		$dataByCacheKey = Yii::$app->cache->get($this->cacheKey);

		if (!$dataByCacheKey) {
			return [];
		}

		return $dataByCacheKey;
	}

	protected function getReportsOnline()
	{
		$reportsData = Yii::$app->analytics->getModelPageviewsReport(
	        $this->referenceModel,
	        $this->modelId,
	        $this->siteType,
	        $this->action,
	        $this->fromDate,
	        $this->toDate,
	        implode(',', $this->demensionsByType),
	        $this->filtersByType,
	        '',
	        $this->metrics
	    );

	    if (!empty($reportsData['results'])) {
	        return $reportsData['results'];
	    }

	    return [];
	}

	protected function drawChart($datasets, $labels, $title)
	{
		$chartOptions = Yii::$app->analytics->getChartStatisticOptions(
	        $datasets,
	        $labels,
	        'bar',
	        null,
	        $this->chartColorCallback,
	        $this->stacked,
	        $this->displayLegend
	    );
	    $yearStatisticTitle = $title;
	    $chart = '';
	    if ($this->outputFormat === self::JS_OUTPUT_FORMAT) {

            $chart .= '<div class="' . $this->chartClass . '" style="height: 400px; margin-bottom: 40px;">';
            $chart .= "<h4 style='text-align: center;'>{$yearStatisticTitle}</h4>";
            $chart .= ChartJs::widget($chartOptions);
	    	$chart .= "</div>";
	    }
	    $chartImageContent = '';
	    if ($this->outputFormat === self::IMAGE_OUTPUT_FORMAT ||
	    	$this->outputFormat === self::BASE64_OUTPUT_FORMAT
	    ) {
	    	$jsonChartOptions = Yii::$app->analytics->toJsonChartOptions($chartOptions);

	    	$url = 'https://quickchart.io/chart?bkg=white&c=' . urlencode($jsonChartOptions);

	    	$chartImageContent .= "<h4 style='text-align: center;'>{$monthStatisticTitle}</h4>";
	    	if ($this->outputFormat === self::BASE64_OUTPUT_FORMAT) {
	    		$monthChartImageContent .= '<div><img style="width: 100%;"
					src="data:image/jpeg;base64,' . base64_encode(file_get_contents($url)) . '"></div>';
			} else {
				$imageUrl = $this->getImageChartPath($url);
				$imageId = 'chart_image_' . microtime();
				$imageTag = '<img style="width: 100%;" id="' . $imageId . '" src="' . $imageUrl . '">';
				$chartImageContent .= "<div>$imageTag</div>";
				if ($this->googleAnalyticsObject) {
            		$saveChartImageEvent = new SaveChartImageEvent();
	            	$saveChartImageEvent->imagePath = $imageUrl;
	            	$saveChartImageEvent->imageTag = $imageTag;
	            	$this->googleAnalyticsObject
	            		->trigger(GoogleAnalytics::SAVE_IMAGE_CHART_EVENT, $saveChartImageEvent);
            	}
			}
	    }

	    return "{$chart}{$chartImageContent}";
	}

	protected function getImageChartPath($url)
	{
		$prictureStream = fopen($url, 'r');
        if ($prictureStream) {
            $newPathName = '/' .
            	'analytics/' .
            	'pageviews_chart/' .
            	date('Y') . '/' .
            	date('m') . '/' .
            	date('d') . '/' .
            	str_replace(' ', '', microtime()) . '.png';
            $isCreatedFile = \Yii::$app->fs->putStream($newPathName, $prictureStream);

            if ($isCreatedFile) {
            	$url = '/site/image?path=' . $newPathName;
            	$fullUrl = getenv('APP_BASE_URL') . $url;

	            return $fullUrl;
            }
        }

        return null;
	}
}
