<?php

namespace taktwerk\yiiboilerplate\modules\analytics\widgets;

use Yii;
use dosamigos\chartjs\ChartJs;
use yii\base\Widget;
use \yii\web\JsExpression;

class ModelTopEntitiesStatisticWidget extends Widget
{
	public $modelName;
	public $labelName;
	public $chartName;
	public $filters = [];
	public $countOfResults = 15;
	public $reports = [];

    public function init()
    {
        parent::init();
    }

    public function run()
    {
    	if (!$this->modelName) {
			return '';
		}
		$labels = [];
		$datasets = [
			['label' => $this->labelName, 'value' => []]
		];
		$urls = [];

		$reports = $this->getAnalyticsReport();

		if (!$reports) {
			return '';
		}

		foreach ($reports as $resultKey => $resultValue) {
			$className = str_replace('/', '\\', $this->modelName);
			$model = $className::findOne($resultValue['ga:dimension2']);
			$label = '';
			if ($model) {
				$label = method_exists($model, 'toString') ?
		    	$model->id . ' - ' . $model->toString() :
		    	$model->id;
			    $urls[] = method_exists($model, 'getFrontendUrl') ?
			    	$model->getFrontendUrl() :
			    	'';
			} else {
				$label = $resultValue['ga:dimension2'];
				if (!empty($resultValue['ga:pageTitle'])) {
					$label .= ' - ' .  $resultValue['ga:pageTitle'];
				}
			}
			
		    $labels[] = $label;
		    
		    $datasets[0]['value'][] = $resultValue['ga:pageviews'];
		}

		$jsUrlsArray = $this->toJsArray($urls);
		$topChartsBarClickCallback = new JsExpression(
	            "function(event, array) {
	              if (array && array[0] && array[0]._index > -1) {
	                var urls = {$jsUrlsArray};
	                window.open(urls[array[0]._index], '_blank');
	              } 
	            }"
	          );
		$chartOptions = Yii::$app->analytics->getChartStatisticOptions(
		    $datasets,
		    $labels,
		    'horizontalBar',
		    $topChartsBarClickCallback
		);
		$chart = ChartJs::widget($chartOptions);

	    $content = '<div class="chart-block">';
	    if ($this->chartName) {
	    	$content .= '<h4 style="text-align: center;">' . $this->chartName . '</h4>';
	    }
	    $content .= '<div class="" style="height: 400px;">' .
		    $chart .
	        '</div>' .
	        '</div>';

	    return $content;
	}

	protected function getAnalyticsReport()
	{
		if ($this->reports) {
			if (is_integer($this->countOfResults)) {
				return array_slice($this->reports, 0, $this->countOfResults);
			}
			
			return $this->reports;
		}

        $reportsData = Yii::$app->analytics->getModelPageviewsReport(
            $this->modelName,
            null,
            'frontend',
            'view',
            '2005-01-01',
            'today',
            'ga:dimension2',
            $this->filters,
            '-ga:pageviews',
            'ga:pageviews',
            $this->countOfResults
        );
        if (empty($reportsData['results'])) {
            return [];
        }
        
        return $reportsData['results'];
	}

	protected function toJsArray($array)
	{
		$temp = array_map([$this, 'toJsStrring'], $array);

    	return '[' . implode(',', $temp) . ']';
	}

	protected function toJsStrring($string)
	{
		return '"' . addcslashes($string, "\0..\37\"\\") . '"';
	}
}
