<style>
    .hidden {
        display: none;
    }
    .client-name {
        cursor: pointer;
        padding: 5px 5px 5px 5px;
        font-size: 18px;
    }
    .sync-statistic-info {
        padding: 5px 5px 5px 20px;
        font-size: 16px;
    }
    .device-info {
        padding: 15px 5px 5px 15px;
    }
    .device-section {
        cursor: pointer;
    }
    .guide-title {
        width: 70%;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        display: inline-block;
    }
</style>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?=$guideStepsCount?:0;?></h3>
                <p><?=Yii::t('twbp', 'Guide Steps');?></p>
            </div>
            <div class="inner text-left">
                <?php foreach ($guides as $guide) { ?>
                    <a href="<?=\yii\helpers\Url::toRoute('/guide/guide/view?id=' . $guide->id . '#tab_relation-tabs-tab5')?>"
                       class="small-box-footer"
                       style="color: white;"
                    >
                        <div class="client-name">
                            <i class="fa fa-book"></i>
                            <span class="guide-title" title="<?=$guide->title?>"><?=$guide->title?></span>
                            <span class="pull-right-container">
                                <span class="pull-right">
                                    <span><?=$guide->getGuideSteps()->count()?></span>
                                    <i class="fa fa-angle-left"></i>
                                </span>
                            </span>
                        </div>
                    </a>
                <?php } ?>
            </div>
            <a href="<?=\yii\helpers\Url::toRoute('/guide/guide-step')?>" class="small-box-footer">
                <?=Yii::t('twbp', 'More Info');?> <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>


