<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="box box-widget widget-user-2">
        <div class="widget-icon-list bg-blue">
            <div class="icon">
                <i class="fa fa-arrows"></i>
            </div>
            <h3 class="widget-user-username"><?=Yii::t('twbp', 'Guider Statistics');?></h3>
        </div>
        <div class="box-footer no-padding">
            <ul class="nav nav-stacked">
                <li><a href="<?=\yii\helpers\Url::toRoute("/guide/guide")?>"><?=Yii::t('twbp', 'Guides');?> <span class="pull-right badge bg-blue"><?=$guideCount?:0;?></span></a></li>
                <li><a href="<?=\yii\helpers\Url::toRoute("/guide/guide")?>"><?=Yii::t('twbp', 'Guide Steps');?> <span class="pull-right badge bg-aqua"><?=$guideStepsCount?:0;?></span></a></li>
            </ul>
        </div>
    </div>
</div>
