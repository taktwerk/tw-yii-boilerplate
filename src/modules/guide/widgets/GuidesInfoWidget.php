<?php

namespace taktwerk\yiiboilerplate\modules\guide\widgets;

use taktwerk\yiiboilerplate\modules\guide\models\Guide;
use taktwerk\yiiboilerplate\modules\guide\models\GuideStep;
use yii\base\Widget;

class GuidesInfoWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $guideCount = Guide::find()->count();
        $guideStepsCount = GuideStep::find()->joinWith('guide')->count();

        return $this->render('guides-statistic-info', ['guideCount' => $guideCount, 'guideStepsCount' => $guideStepsCount]);
    }
}
