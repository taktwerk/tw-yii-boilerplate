<?php

return [
    'undo',
    'redo',"|",
    'heading',"|",
    'bold',
    'italic',
    'strikethrough',
    'underline',
    "|",
    'fontSize',
    'fontFamily',
    'fontColor',
    'fontBackgroundColor',"|", 'numberedList','bulletedList',"|",'indent','outdent',"|",
    'alignment',"|",
   'insertTable'
];
