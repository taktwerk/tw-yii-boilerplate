<?php

namespace taktwerk\yiiboilerplate\modules\guide\widgets;

use taktwerk\yiiboilerplate\modules\guide\models\Guide;
use taktwerk\yiiboilerplate\modules\guide\models\GuideStep;
use taktwerk\yiiboilerplate\modules\sync\enums\SyncProcessStatusEnum;
use taktwerk\yiiboilerplate\modules\customer\models\Client;
use taktwerk\yiiboilerplate\modules\user\models\UserDevice;
use yii\base\Widget;
use \yii\db\Query;
use Yii;
use yii\web\View;

class GuideStepsInfoWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $guideStepsCount = GuideStep::find()->joinWith('guide')->count();
        $guides = Guide::find()->all();

        return $this->render(
            'guide-steps-statistic-info',
            ['guideStepsCount' => $guideStepsCount, 'guides' => $guides]
        );
    }
}
