<?php

namespace taktwerk\yiiboilerplate\modules\guide\components;

/*
 * @link http://www.diemeisterei.de/
 *
 * @copyright Copyright (c) 2015 diemeisterei GmbH, Stuttgart
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use taktwerk\yiiboilerplate\modules\guide\models\Guide;
use Yii;
use taktwerk\yiiboilerplate\modules\guide\models\GuideSetting;

class PdfHelper extends \taktwerk\yiiboilerplate\components\Helper
{
    /**
     * @param $guide
     * @param string $language
     * @return array
     * @throws \Exception
     */
    public static function renderPdf($guide, $language = 'de'){
        Yii::$app->params['clean_up'] = [];
        $pdf_guide = [];
        /**@var $guide Guide*/
        Yii::$app->language = $language;
        $pdfLayout = '1-column';
       
        $gConfig = GuideSetting::clientEntries($guide->client_id, true);
        if($gConfig && $gConfig->pdf_template){
            $pdfLayout = $gConfig->pdf_template;
        }

        $pdf_guide['content'] = Yii::$app->controller
        ->renderPartial('@taktwerk-boilerplate/modules/guide/views/pdf/'.$pdfLayout,compact('guide'));
        $pdf_guide['directory_path'] = 'pdf/'.$guide->client->id."/";
        $pdf_guide['header'] = $guide->short_name . ' - ' . $guide->title . '|' . Yii::t('twbp', 'Rev_') . $guide->getRevision() . '|' . $guide->getClientLogoFile();
        $pdf_guide['footer'] = Yii::t('twbp', 'Copyright') . ' '.date('Y').' © '.$guide->client->name.' | | '.Yii::t('twbp', 'Page').' {PAGENO} '.Yii::t('twbp', 'of').' {nb}';
        $pdf_guide['file_name'] = $guide->getPdfName();

        return $pdf_guide;
    }
}
