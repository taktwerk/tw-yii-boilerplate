<?php
namespace taktwerk\yiiboilerplate\modules\guide\components;

use taktwerk\yiiboilerplate\modules\digisign\components\DigiSignAbstract;

class ProductionLineDigiSign extends DigiSignAbstract
{
    const EVENT_AFTER_TITLE = 'afterTitle';

    public function getDigiSignHtml():string{
        \Yii::$app->controller->layout = '@taktwerk-boilerplate/modules/guide/views/layouts/mainscreen-dark';
        return \Yii::$app->controller
        ->render('@taktwerk-boilerplate/modules/guide/views/digi-sign-template/view-dark',['model'=>$this]);
    }
    public function getDigiSignTitle():string{
        return \Yii::t('twbp','Production Line');
    }
    public function autoRefreshEnabled():bool{
        return false;
    }
    public function getScreenTitle() {
        return $this->getDigiScreenModel()->name;
    }
    public function getIframeUrl() {
        return false;
    }
}