<?php
//Generation Date: 09-Oct-2020 12:53:15pm
namespace taktwerk\yiiboilerplate\modules\guide\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use taktwerk\yiiboilerplate\grid\GridView;
use Yii;
use taktwerk\yiiboilerplate\modules\guide\models\Guide;
use yii\helpers\Json;
use taktwerk\yiiboilerplate\assets\TwAsset;
/**
 * This is the class for controller "GuideViewHistoryController".
 */
class GuideViewHistoryController extends \taktwerk\yiiboilerplate\modules\guide\controllers\base\GuideViewHistoryController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\guide\models\GuideViewHistory';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\guide\models\search\GuideViewHistory';

   /**
    * Additional actions for controllers, uncomment to use them
    * @inheritdoc
    */
   public function behaviors()
   {
       return ArrayHelper::merge(parent::behaviors(), [
           'access' => [
               'class' => AccessControl::class,
               'rules' => [
                   [
                       'allow' => true,
                       'actions' => [
                           'progress',
                       ],
                       'roles' => ['@']
                   ]
                ]
            ]
        ]);
    }

    /**
     * Init controller
     *
     * @throws \Exception
     */
    public function init()
    {
        /*
         * $this->crudRelations = [
         * 'GuideSteps' => true,
         * 'GuideAssetPivots' => true,
         * 'GuideChildren0' => true
         * ];
         */
        
        $guideIcon = '<i class="fa fa-file-text-o"></i> ';
        $collectionIcon = '<i class="fa fa-book"></i> ';
        
        $this->crudColumnsOverwrite = [
            'crud' => [
                'data' => false,
                'user_id'=>false,   
                //'guide_id'=>false,
                'guide_id'=>[
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'guide_id',
                    'label'=>\Yii::t('twbp','Guide/Collection'),
                    'format'=>'html',
                    'value'=> function ($model)use ($guideIcon,$collectionIcon) {
                        $html = '';
                    if ($model->guide) {
                        $title = $model->guide->toString;
                        if(trim($title)==''){
                            $title = $model->guide->short_name .' - '.$model->guide->title;
                        }
                        $title = Html::encode($title);
                        if (Yii::$app->getUser()->can('app_guide_view') && $model->guide->readable()) {
                            $html=  Html::a($title, ['guide/view', 'id' => $model->guide_id], ['data-pjax' => 0]);
                        } else {
                            $html = $title;
                        }
                        if($model->guide->isCollection()){
                            return '<b>'.$collectionIcon.$html.'</b>';
                        }
                        //$html .= "(Steps:". $model->guide->stepCount().")";
                        return $guideIcon.$html;
                    }
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'data' => Guide::find()->groupBy('guide.id')->count() > 50 ? null : \yii\helpers\ArrayHelper::map(Guide::find()->groupBy('guide.id')->all(), 'id', 'toString'),
                        'initValueText' => Guide::find()->groupBy('guide.id')->count() > 50 ? \yii\helpers\ArrayHelper::map(Guide::find()->groupBy('guide.id')->andWhere(['guide.id' => $searchModel->guide_id])->all(), 'id', 'toString') : '',
                        'options' => [
                            'placeholder' => '',
                            'multiple' => true,
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            (Guide::find()->groupBy('guide.id')->count() > 50 ? 'minimumInputLength' : '') => 3,
                            (Guide::find()->groupBy('guide.id')->count() > 50 ? 'ajax' : '') => [
                                'url' => \yii\helpers\Url::toRoute(['/guide/guide-view-history/list']),
                                'dataType' => 'json',
                                'data' => new \yii\web\JsExpression('function(params) {
                        return {
                            q:params.term, m: \'Guide\'
                        };
                    }')
                            ],
                        ]
                    ],
                    ],
               'before#guide_id'=>[
                    'class' => '\kartik\grid\DataColumn',
                    'label'=>\Yii::t('twbp','User'),
                    'attribute' => 'user_id',
                    'format' => 'html',
                    'value' => function ($model) {
                        $cb = $model->user;
                        if($cb){
                            return $cb->toString;
                        }
                    },
                    'filter' => false
                ],
                'after#guide_id' => [
                    'label' => \Yii::t('twbp', 'Progress'),
                    'attribute'=>'guide_id',
                    'format'=>'html',
                    'value' => function ($model) use ($guideIcon,$collectionIcon) {
                        if ($model->guide) {
                            $html = '';
                            try {
                                $vData = Json::decode($model->data);
                            } catch (\Exception $e) {
                                $vData = [];
                            }
                            if ($model->guide->isCollection()) {
                                $childGuide = null;
                                if (isset($vData['child_guide_id'])) {
                                    $childGuide = Guide::findOne([
                                        Guide::tableName().'.id' => $vData['child_guide_id']
                                    ]);
                                    if ($childGuide && $childGuide->readable()) {
                                        $cTitle = $childGuide->toString;
                                        if(trim($cTitle)==''){
                                            $cTitle = $model->guide->short_name .' - '.$model->guide->title;
                                        }
                                        $cTitle = Html::encode($cTitle);
                                        
                                        $guideHtml = '<br>'.\Yii::t('twbp','Of Guide - ') .'<b>'. Html::a($cTitle, [
                                            'guide/view',
                                            'id' => $childGuide->id
                                        ], [
                                            'data-pjax' => 0
                                        ]). "</b> (Total Steps:".$childGuide->stepCount().")";
                                        $guideHtml = '<b>('.$collectionIcon.$model->guide->stepCount().')</b>';
                                    }
                                }
                                if (isset($vData['step_order_number'])) {
                                    if ($childGuide) {
                                        $step = $childGuide->getGuideSteps()
                                            ->where([
                                            'order_number' => $vData['step_order_number']
                                        ])
                                            ->one();
                                        if ($step) {
                                            $html = $vData['step_order_number'].' / '.$childGuide->getGuideSteps()->count().' '.$guideHtml. ' - '. Html::a($step->toString, [
                                                'guide-step/view',
                                                'id' => $step->id
                                            ], [
                                                'data-pjax' => 0
                                            ]);
                                            
                                        }
                                    }
                                }
                            } else {
                                if (isset($vData['step_order_number'])) {
                                    
                                    if ($model->guide) {
                                        $step = $model->guide->getGuideSteps()
                                            ->where([
                                            'order_number' => $vData['step_order_number']
                                        ])
                                            ->one();
                                        if ($step) {
                                            $html = $vData['step_order_number'].' / '.$model->guide->getGuideSteps()->count(). ' - '. Html::a($step->toString, [
                                                'guide-step/view',
                                                'id' => $step->id
                                            ], [
                                                'data-pjax' => 0
                                            ]);
                                        }
                                    }
                                }
                            }
                            
                            
                            return $html;
                        }
                        }
                        ],
                        'id'=>false,
            ],
            //'view' => ['data' => false]
        ];
        return parent::init();
    }
}
