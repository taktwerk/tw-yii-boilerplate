<?php

namespace taktwerk\yiiboilerplate\modules\guide\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\modules\customer\models\ClientModelType;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use yii\helpers\Url;
use taktwerk\yiiboilerplate\helpers\CrudHelper;
use yii\helpers\Inflector;
use yii\db\Query;

/**
 * This is the class for controller "GuideDataController".
 */
class GuideDataController extends \taktwerk\yiiboilerplate\modules\guide\controllers\base\GuideDataController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\guide\models\GuideData';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\guide\models\search\GuideData';

    /**
     * Additional actions for controllers, uncomment to use them
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'type-list',
                        ],
                        'roles' => ['@']
                    ]
                ]
            ]
        ]);
    }
    public function actionTypeList($id){
        $model = ClientModelType::findOne($id);
        $q = \Yii::$app->request->get('q');

        if($model){
            $tblSchema  = \Yii::$app->db->schema->getTableSchema($model->type);
            if($tblSchema==null){
                return [];
            }
            
            $modelName = Inflector::camelize($tblSchema->name);
            
            $tableName = $tblSchema->name;
            
            $modelName = CrudHelper::findModelClass($modelName,$tableName);
            
            if($modelName){
                if(method_exists($modelName, 'filter')){
                    //return $modelName::filter($q);
                }
                $nameCol = $tblSchema->primaryKey[0];
                $primaryCol =  $tblSchema->primaryKey[0];
                //prd($tblSchema->columns);
                $secondCol = '';
                foreach($tblSchema->columns as $col){
                    if($secondCol=='' && $primaryCol!=$col->name){
                        $secondCol = $col->name;//working herter
                    }
                    if($col->type=='string'){
                        $nameCol  = $col->name;
                        break;
                    }
                }
                
                if($nameCol==''){
                    $nameCol = $secondCol;
                }
                
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $out = ['results' => ['id' => '', 'text' => '']];
                if (!is_null($q)) {
                    $query = new Query();
                    $query->select($tblSchema->primaryKey[0].' , '.$nameCol.' AS text')
                    ->from($modelName::tableName())
                    ->andWhere([$modelName::tableName() . '.deleted_at' => null])
                    ->andFilterWhere(['like', $nameCol, $q])
                    ->limit(20);
                    //prd($query->createCommand()->rawSql,$primaryCol,$nameCol);
                    $command = $query->createCommand();
                    $data = $command->queryAll();
                    $out['results'] = array_values($data);
                }
                return $out;
                
            }
            
            
            
        }//WORKING HERE
        
    }
    
    public function formFieldsOverwrite($model, $form,$owner=null){
        //WORKING HERE
        $initialRefValue = $model->type_ref;
        $refModel = $model->getTypeRefModel();
        if($refModel){
            $initialRefValue = method_exists($refModel, 'getToString')?$refModel->toString:$initialRefValue;
        }

        $typeRefId = 'type_ref' . ($ajax || $useModal ? '_ajax_' . $owner : '');

        $this->formFieldsOverwrite = [
            'client_model_type_id' =>
            $form->field(
                $model,
                'client_model_type_id'
                )
            ->widget(
                Select2::class,
                [
                    'data' => ClientModelType::find()->count() > 50 ? null : ArrayHelper::map(ClientModelType::find()->all(), 'id', 'toString'),
                    'initValueText' => ClientModelType::find()->count() > 50 ? \yii\helpers\ArrayHelper::map(ClientModelType::find()->andWhere(['id' => $model->client_model_type_id])->all(), 'id', 'toString') : '',
                    'options' => [
                        'placeholder' => \Yii::t('twbp', 'Select a value...'),
                        'id' => 'client_model_type_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        (ClientModelType::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                        (ClientModelType::find()->count() > 50 ? 'ajax' : '') => [
                            'url' => \yii\helpers\Url::to(['list']),
                            'dataType' => 'json',
                            'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'ClientModelType\'
                                        };
                                    }')
                        ],
                    ],
                    'pluginEvents' => [
                        "change" => "function() {
                                    $('#$typeRefId').val('').trigger('change');
                                    var data_id = $(this);
                                $.post('" .
                        Url::toRoute('/customer/client-model-type/system-entry?id=', true) .
                        "' + data_id.val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if(!json.success){
                                                if ((data_id.val() != null) && (data_id.val() != '')) {
                                        // Enable edit icon
                                        $(data_id.next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
                        Url::toRoute('/customer/client-model-type/entry-details?id=', true) .
                        "' + data_id.val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#client_model_type_id_well').html(json.data);
                                                //$('#client_model_type_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $(data_id.next().next().children('button')[0]).prop('disabled', true);
                                    }
                                            }else{
                                        // Disable edit icon and remove sub-form
                                        $(data_id.next().next().children('button')[0]).prop('disabled', true);
                                            }
                                        });
                                }",
                    ],
                    'addon' => (\Yii::$app->getUser()->can('modules_client-model-type_create') && (!$relatedForm || ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL && !$useModal))) ? [
                        'append' => [
                            'content' => [
                                ClassDispenser::getMappedClass(RelatedForms::class)::widget([
                                    'relatedController' => '/customer/client-model-type',
                                    'type' => $relatedTypeForm,
                                    'selector' => 'client_model_type_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                    'primaryKey' => 'id',
                                ]),
                            ],
                            'asButton' => true
                        ],
                    ] : []
                ]
                ) . '
            
            
                <div id="client_model_type_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' .
            (ClientModelType::findOne(['id' => $model->client_model_type_id])
                ->entryDetails == '' ?
                ' display:none;' :
                '') . '
                    ">' .
            (ClientModelType::findOne(['id' => $model->client_model_type_id])->entryDetails != '' ?
                ClientModelType::findOne(['id' => $model->client_model_type_id])->entryDetails :
                '') . '
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>',
            
            
            
            'type_ref' =>
            $form->field(
                $model,
                'type_ref'
                )
            ->widget(
                Select2::class,
                [
                    'data' => [],
                    'initValueText' =>$initialRefValue,
                    'options' => [
                        'placeholder' => \Yii::t('twbp', 'Select a value...'),
                        'id' => $typeRefId,
                        'multiple' => false
                    ],
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'tags' => false,
                        'maximumInputLength' => 45,
                        'ajax'=> [
                            'url' => \yii\helpers\Url::to(['type-list']),
                            'dataType' => 'json',
                            'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'ClientModelType\',id: $("#client_model_type_id'. ($ajax || $useModal ? '_ajax_' . $owner : '').'").val()
                                        };
                                    }')
                        ],
                        
                    ]
                ]
                )
            ->hint($model->getAttributeHint('type_ref')),
        ];
        
        return $this->formFieldsOverwrite;
    }
}
