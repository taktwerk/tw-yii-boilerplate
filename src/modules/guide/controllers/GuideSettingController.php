<?php

namespace taktwerk\yiiboilerplate\modules\guide\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "GuideSettingController".
 */
class GuideSettingController extends \taktwerk\yiiboilerplate\modules\guide\controllers\base\GuideSettingController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\guide\models\GuideSetting';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\guide\models\search\GuideSetting';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
    public function formFieldsOverwrite($model, $form,$owner=null){
        $this->formFieldsOverwrite = [
            'is_historicised' =>
            $form->field(
                $model,
                'is_historicised')
                ->dropDownList([
                null => \Yii::t('twbp', 'Default'),
                0 => \Yii::t('twbp', 'No'),
                1 => \Yii::t('twbp', 'Yes')
            ])
                ->hint($model->getAttributeHint('is_historicised')),
        ];
        
        return $this->formFieldsOverwrite;
    }
}
