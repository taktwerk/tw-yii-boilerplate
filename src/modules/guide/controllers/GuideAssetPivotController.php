<?php

namespace taktwerk\yiiboilerplate\modules\guide\controllers;

use taktwerk\yiiboilerplate\modules\guide\models\GuideAssetPivot;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "GuideAssetPivotController".
 */
class GuideAssetPivotController extends \taktwerk\yiiboilerplate\modules\guide\controllers\base\GuideAssetPivotController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\guide\models\GuideAssetPivot';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\guide\models\search\GuideAssetPivot';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }


    /**
     * @param GuideAssetPivot $model
     * @return \yii\web\Response
     */
    protected function workflowCloseAction($model)
    {
        return $this->redirect(['/guide/guide/view', 'id' => $model->guide_id]);
    }
}
