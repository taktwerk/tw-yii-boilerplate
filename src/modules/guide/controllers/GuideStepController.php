<?php

namespace taktwerk\yiiboilerplate\modules\guide\controllers;

use taktwerk\yiiboilerplate\modules\guide\actions\GuideStepSortAction;
use taktwerk\yiiboilerplate\modules\guide\models\GuideStep;
use kartik\select2\Select2;
use Yii;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the class for controller "GuideStepController".
 */
class GuideStepController extends \taktwerk\yiiboilerplate\modules\guide\controllers\base\GuideStepController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\guide\models\GuideStep';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\guide\models\search\GuideStep';

    /**
     * Additional actions for controllers, uncomment to use them
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'order-number',
                        ],
                        'roles' => ['@']
                    ]
                ]
            ]
        ]);
    }

    /**
     * Init controller
     * @throws \Exception
     */
    public function init()
    {
        $this->crudColumnsOverwrite = [
            'index' => [
                // index
                //'description_html' => false,
            ],
            'crud' => [
                // index
                'attached_file' => false,
                'design_canvas' => false,
                'before#title' => [
                    'attribute' => 'attached_file',
                    'format' => 'html',
                    'value' => function ($model) {
                        return Html::a($model->showFilePreview('design_canvas'), ['guide-step/update', 'id' => $model->id, 'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))], ['data-pjax' => 0]);
                    },
                ],
            ],
            'tab' => [
                'id' => false,
                //'description_html' => false,
            ]
        ];

        return parent::init();
    }

    /**
     * @return array
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'sort-item' => [
                'class' => GuideStepSortAction::class,
                'activeRecordClassName' => GuideStep::class,
                'orderColumn' => 'order_number'
            ],
        ]);
    }

    /**
     * @param $model
     * @return \yii\web\Response
     */
    protected function workflowCloseAction($model)
    {
        return $this->redirect(['/guide/guide/view', 'id' => $model->guide_id]);
    }

    public function formFieldsOverwrite($model, $form)
    {
        if(($model->guide_id || Yii::$app->request->get('GuideStep')['guide_id']) && !$model->order_number){
            if(Yii::$app->request->get('GuideStep')['guide_id']){
                $guide_id = Yii::$app->request->get('GuideStep')['guide_id'];
            }else{
                $guide_id = $model->guide_id;
            }
            $lastStep = GuideStep::find()->where(['guide_id' => $guide_id])->orderBy(['order_number' => SORT_DESC])->one();
            $order_no =  !empty($lastStep) ? ((int)$lastStep->order_number) + 1 : 1;
            $model->order_number=$order_no;
        }

        $guide_url = Url::toRoute('guide-step/order-number');
        $guideQuery = \taktwerk\yiiboilerplate\modules\guide\models\Guide::find()->where(['guide.is_collection'=>0])->groupBy('guide.id');
        $this->formFieldsOverwrite = [
            'guide_id'=>        $form->field(
                    $model,
                    'guide_id'
                )
                    ->widget(
                        Select2::class,
                        [
                            'data' => $guideQuery->count() > 50 ? null : ArrayHelper::map($guideQuery->all(), 'id', 'toString'),
                            'initValueText' => $guideQuery->count() > 50 ? \yii\helpers\ArrayHelper::map(\taktwerk\yiiboilerplate\modules\guide\models\Guide::find()->where(['guide.id' => $model->guide_id])->andWhere(['guide.is_collection'=>0])->all(), 'id', 'toString') : '',
                            'options' => [
                                'placeholder' => \Yii::t('twbp', 'Select a value...'),
                                'id' => 'guide_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                            ],
                            'pluginOptions' => [
                                'allowClear' => false,
                                ($guideQuery->count() > 50 ? 'minimumInputLength' : '') => 3,
                                ($guideQuery->count() > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'Guide\'
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                                    var data_id = $(this);
                                    $.post('".$guide_url."', { id: data_id.val() } ).done(
                                    function( data ) {
                                    if(!$( '#guidestep-order_number' ).val()){
                                        $( '#guidestep-order_number' ).val( data );
                                        }
                                    }
                                );
                                $.post('" .
                                    Url::toRoute('/guide/guide/system-entry?id=', true) .
                                    "' + data_id.val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if(!json.success){
                                                if ((data_id.val() != null) && (data_id.val() != '')) {
                                        // Enable edit icon
                                        $(data_id.next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
                                    Url::toRoute('/guide/guide/entry-details?id=', true) .
                                    "' + data_id.val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#guide_id_well').html(json.data);
                                                //$('#guide_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $(data_id.next().next().children('button')[0]).prop('disabled', true);
                                    }
                                            }else{
                                        // Disable edit icon and remove sub-form
                                        $(data_id.next().next().children('button')[0]).prop('disabled', true);
                                            }
                                        });
                                }",
                            ],
                            'addon' => (\Yii::$app->getUser()->can('guide_guide_create') && (!$relatedForm || ($relatedType != RelatedForms::TYPE_MODAL && !$useModal))) ? [
                                'append' => [
                                    'content' => [
                                        RelatedForms::widget([
                                            'relatedController' => '/guide/guide',
                                            'type' => $relatedTypeForm,
                                            'selector' => 'guide_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'primaryKey' => 'id',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    ) . '
                    
                
                <div id="guide_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' .
                (\taktwerk\yiiboilerplate\modules\guide\models\Guide::findOne(['guide.id' => $model->guide_id])
                    ->entryDetails == '' ?
                    ' display:none;' :
                    '') . '
                    ">' .
                (\taktwerk\yiiboilerplate\modules\guide\models\Guide::findOne(['guide.id' => $model->guide_id])->entryDetails != '' ?
                    \taktwerk\yiiboilerplate\modules\guide\models\Guide::findOne(['guide.id' => $model->guide_id])->entryDetails :
                    '') . ' 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>','order_number' =>
                $form->field(
                    $model,
                    'order_number',
                    [
                        'selectors' => [
                            'input' => '#' .
                                Html::getInputId($model, 'order_number') . $owner
                        ]
                    ]
                )
                    ->textInput(
                        [
                            'type' => 'number',
                            'step' => '1',
                            'placeholder' => $model->getAttributePlaceholder('order_number'),

                            'id' => Html::getInputId($model, 'order_number') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('order_number')),
        ];


        return parent::formFieldsOverwrite($model, $form); // TODO: Change the autogenerated stub
    }

    public function actionOrderNumber(){
        $id = \Yii::$app->request->post('id');
        $tbl = GuideStep::tableName();
        $lastStep = GuideStep::find()->where([$tbl.'.guide_id' => $id])
        ->orderBy([$tbl.'.order_number' => SORT_DESC])->one();
        return !empty($lastStep) ? ((int)$lastStep->order_number) + 1 : 1;
    }

}
