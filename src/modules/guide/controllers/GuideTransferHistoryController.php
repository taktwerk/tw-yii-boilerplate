<?php
//Generation Date: 21-Oct-2020 08:55:22am
namespace taktwerk\yiiboilerplate\modules\guide\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "GuideTransferHistoryController".
 */
class GuideTransferHistoryController extends \taktwerk\yiiboilerplate\modules\guide\controllers\base\GuideTransferHistoryController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\guide\models\GuideTransferHistory';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\guide\models\search\GuideTransferHistory';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }
}
