<?php

namespace taktwerk\yiiboilerplate\modules\guide\controllers;

use taktwerk\yiiboilerplate\modules\guide\models\Guide;
use taktwerk\yiiboilerplate\modules\guide\models\GuideCategory;
use taktwerk\yiiboilerplate\modules\guide\models\GuideTag;
use taktwerk\yiiboilerplate\modules\search\components\SearchBuilder;
use yii\base\InvalidRouteException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;
use Http\Client\Common\Exception\HttpClientNotFoundException;
use yii\web\NotFoundHttpException;
use taktwerk\yiiboilerplate\modules\guide\models\GuideChild;
use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\modules\guide\models\base\GuideViewHistory;
use yii\helpers\Json;
use taktwerk\yiiboilerplate\modules\guide\models\GuideTransferHistory;
use taktwerk\yiiboilerplate\modules\guide\models\GuideSetting;
use taktwerk\yiiboilerplate\modules\guide\models\GuideCategoryBinding;
use taktwerk\yiiboilerplate\modules\guide\models\GuideCategoryGroup;
use taktwerk\yiiboilerplate\modules\user\models\UserGroup;


/**
* This is the class for controller "ExploreController".
*/
class ExploreController extends Controller
{
    /**
     * @var SearchBuilder
     */
    protected $search;

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->layout = '@taktwerk-boilerplate/modules/guide/views/explore/layout.php';
        $this->search = new SearchBuilder();
    }

    /**
     * Additional actions for controllers, uncomment to use them
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index', 'guides', 'guide', 'search', 'error','save-history','assign-guide'
                        ],
                        'roles' => ['@']
                    ]
                ]
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'assign-guide'  => ['POST'],
                ],
            ],
        ]);
    }
    private function guideWithoutCategoryQuery(){
        $query = Guide::find();
        $gcbTbl = GuideCategoryBinding::tableName();
        $query->leftJoin(['egcb'=>$gcbTbl],
            '`'.Guide::tableName().'`.`id` = `egcb`.`guide_id` AND `egcb`.`deleted_at` IS NULL');
        $query->andWhere([
                'egcb.id'=>null
            ]);
        $query = $query
        ->joinWith(
            [
                'guideChildren' => function($query) {
                 $query->onCondition([
                    'is',
                    'guide_child.deleted_at',
                    new \yii\db\Expression('null')
                ]);
                },
            ]
            )
            ->andWhere([
                'is',
                'guide_child.guide_id',
                new \yii\db\Expression('null')
            ])->groupBy('guide.id');
           return $query;
    }
    /**
     * @return string
     */
    public function actionIndex($pageContentType = 'category')
    {
        $tags = [];
        $guidesWithoutCategory = [];
        $guidesWithoutCategory = $this->guideWithoutCategoryQuery()
            ->orderBy('guide.id')->all();

        if ($pageContentType === 'category') {
            $tags = GuideCategory::find()->joinWith('guideCategoryGroups')->orderBy('name')->all();
        }
        
        return $this->render('index', compact('tags', 'pageContentType', 'guidesWithoutCategory'));
    }

    /**
     * @return string
     */
    public function actionGuides($guideCategoryId = null, $collectionId=null)
    {
        $collection = null;
        $guideCategory = null;
        $guides = [];
        if ($guideCategoryId) {
            $guideCategory = GuideCategory::find()->joinWith('guideCategoryGroups')
                ->where([
                    GuideCategory::getTableSchema()->fullName.'.id' => $guideCategoryId]
                )
                ->one();
            $guides = $guideCategory->guideCategoryBindings;
        } else {
            $guideCategory = new GuideCategory();
            $guideCategory->name = 'No category';
            $guides =  $this->guideWithoutCategoryQuery()->orderBy('guide.id')
            ->all();
        }
        
            /*
         * if(strlen($collectionId)>1){
         * $collection = Guide::findOne($collectionId);
         * if (!$collection || $collection->is_collection==0){
         * throw new NotFoundHttpException("Guide Collection not found",400);
         * }
         * }
         */
        if (! $guideCategory) {
            throw new InvalidRouteException("Guides not found by guide category", 400);
        }
        return $this->render('guides', compact('guideCategory', 'collection', 'guides'));
    }

    /**
     *
     * @param
     *            $id
     * @param null $step
     * @return string
     * @throws Yii\base\InvalidRouteException
     */
    public function actionGuide($id, $step = null, $parent = null, $resume = 1)
    {
        $tbl = Guide::tableName();
        $guide = Guide::findOne([
            $tbl.'.id' => $id
        ]);
        $parentGuide = null;
        if ($parent != null) {
            $guideChild = GuideChild::find()->where([
                'guide_id' => $id,
                'parent_guide_id' => $parent
            ])->one();
            if ($guideChild == null && $guideChild->parentGuide->is_collection == 0) {
                throw new InvalidRouteException("Guide not found", 404);
            }
            $parentGuide = $guideChild->parentGuide;
        }

        if ($guide) {
            $gViewHistory = $guide->getGuideViewHistoryOfUser(\Yii::$app->user->id);
            if ($resume == 0){
                
                if($parentGuide){
                    $pgViewHistory = $parentGuide->getGuideViewHistoryOfUser(\Yii::$app->user->id);
                    if($pgViewHistory){
                        $pgViewHistory->delete();
                    }
                }else{
                //If Collection is resumed, We delete the history data and the refresh the page
                    if ($gViewHistory) {
                        $gViewHistory->delete();
                    }
                }
                return $this->redirect([
                    'explore/guide',
                    'id' => $id,
                    'step' => $step,
                    'parent' => $parent
                ]);
            }
            //If current guide is a collection and has view history data, then we redirect it to the last visited child guide of the collection
            if ($gViewHistory && $guide->isCollection() && $gViewHistory->data) {
                $gViewData = Json::decode($gViewHistory->data);
                if (isset($gViewData['child_guide_id'])) {
                    $gViewData['child_guide_id'];
                    $hasChild = $guide->getGuideChildren0()
                        ->where([
                        'guide_id' => $gViewData['child_guide_id']
                    ])
                        ->exists();
                    if ($hasChild){
                            return $this->redirect(['explore/guide','id'=>$gViewData['child_guide_id'],'parent'=>$guide->id]);
                        }
                    }
            }
            
            $stepModel = !empty($step) ?
                $guide->getGuideSteps()->where(['order_number' => $step])->one() :
                $guide->getGuideSteps()->orderBy(['order_number' => SORT_ASC])->one();
            $step = $stepModel;
            return $this->render('guide', compact('guide','parentGuide', 'step'));
        } else{
            throw new InvalidRouteException("Guide not found",400);
        }
    }
    public function actionSaveHistory($guide_id, $step=null, $parent_guide_id=null)
    {
        if(!\Yii::$app->request->isAjax){
            return;
        }
        $isHistoricised = GuideSetting::isHistoricised();
        if($isHistoricised){
            $guide = Guide::findOne([Guide::tableName().'.id' => $guide_id]);
            if($guide){
                $guide->saveGuideViewHistory($step,$parent_guide_id);
            }
        }
    }
    public function actionAssignGuide($id, $parent_id=null)
    {
        $userId = \Yii::$app->request->post('user_id');
        $guide = Guide::findOne([Guide::tableName().'.id'  => $parent_id?$parent_id:$id]);
        if($guide){
            $gViewHistory = $guide->getGuideViewHistoryOfUser();
            if($gViewHistory && $gViewHistory->transferToUser($userId)){
                \Yii::$app->session->setFlash('success',\Yii::t('twbp','Guide progress transfered to the user'));
                return $this->redirect(['explore/index']);
            }
            \Yii::$app->session->setFlash('error',\Yii::t('twbp','There were some problems in assigning this guide to the user'));
        }else{
            \Yii::$app->session->setFlash('error',\Yii::t('twbp','Guide not found'));
        }
        return $this->redirect(['explore/guide','id'=>$id,'parent'=>$parent_id]);

    }
    /**
     * @return string|\yii\web\Response
     * @throws \Exception
     */
    public function actionSearch()
    {
        $query = Yii::$app->getRequest()->get('term');
        $results = $this->search->search($query, \taktwerk\yiiboilerplate\modules\guide\models\Guide::class);

        if (Yii::$app->getRequest()->isAjax) {
            return $this->asJson($results);
        }

        return $this->render('search', compact('results'));
    }
}
