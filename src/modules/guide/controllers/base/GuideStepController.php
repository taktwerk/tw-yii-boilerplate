<?php
//Generation Date: 16-Sep-2020 03:54:53pm
namespace taktwerk\yiiboilerplate\modules\guide\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * GuideStepController implements the CRUD actions for GuideStep model.
 */
class GuideStepController extends TwCrudController
{
	public function actions(){
        return array_merge(parent::actions(), [
            'sort-item' => [
                'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\actions\SortableAction::className()),
                'activeRecordClassName' => \taktwerk\yiiboilerplate\modules\guide\models\GuideStep::className(),
                'orderColumn' => 'order_number',
            ],
            // your other actions
        ]);
    }
    /**
    * Additional actions for controllers, uncomment to use them
    * @inheritdoc
    */
   public function behaviors()
   {
       return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
           'access' => [
               'class' => \yii\filters\AccessControl::class,
               'rules' => [
                   [
                       'allow' => true,
                       'actions' => [
                           'sort-item'
                       ],
                       'roles' => ['@']
                   ]
               ]
           ]
       ]);
   }
}
