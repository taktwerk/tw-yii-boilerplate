<?php
//Generation Date: 10-Sep-2020 06:26:01am
namespace taktwerk\yiiboilerplate\modules\guide\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * ProductionLineHistoryController implements the CRUD actions for ProductionLineHistory model.
 */
class ProductionLineHistoryController extends TwCrudController
{
}
