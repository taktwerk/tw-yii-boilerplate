<?php
//Generation Date: 16-Sep-2020 03:54:51pm
namespace taktwerk\yiiboilerplate\modules\guide\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * GuideAssetController implements the CRUD actions for GuideAsset model.
 */
class GuideAssetController extends TwCrudController
{
}
