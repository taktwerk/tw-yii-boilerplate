<?php
//Generation Date: 16-Sep-2020 03:54:54pm
namespace taktwerk\yiiboilerplate\modules\guide\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * GuideTemplateController implements the CRUD actions for GuideTemplate model.
 */
class GuideTemplateController extends TwCrudController
{
}
