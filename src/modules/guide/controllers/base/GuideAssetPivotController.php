<?php
//Generation Date: 16-Sep-2020 03:54:52pm
namespace taktwerk\yiiboilerplate\modules\guide\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * GuideAssetPivotController implements the CRUD actions for GuideAssetPivot model.
 */
class GuideAssetPivotController extends TwCrudController
{
}
