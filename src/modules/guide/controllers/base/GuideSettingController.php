<?php
//Generation Date: 16-Sep-2020 03:54:53pm
namespace taktwerk\yiiboilerplate\modules\guide\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * GuideSettingController implements the CRUD actions for GuideSetting model.
 */
class GuideSettingController extends TwCrudController
{
}
