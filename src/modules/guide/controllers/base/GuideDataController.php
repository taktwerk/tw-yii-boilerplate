<?php
//Generation Date: 03-Sep-2020 07:03:45am
namespace taktwerk\yiiboilerplate\modules\guide\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * GuideDataController implements the CRUD actions for GuideData model.
 */
class GuideDataController extends TwCrudController
{
}
