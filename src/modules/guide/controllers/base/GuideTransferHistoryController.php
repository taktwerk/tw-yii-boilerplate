<?php
//Generation Date: 21-Oct-2020 08:55:22am
namespace taktwerk\yiiboilerplate\modules\guide\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * GuideTransferHistoryController implements the CRUD actions for GuideTransferHistory model.
 */
class GuideTransferHistoryController extends TwCrudController
{
}
