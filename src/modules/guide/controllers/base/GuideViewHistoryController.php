<?php
//Generation Date: 09-Oct-2020 12:53:15pm
namespace taktwerk\yiiboilerplate\modules\guide\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * GuideViewHistoryController implements the CRUD actions for GuideViewHistory model.
 */
class GuideViewHistoryController extends TwCrudController
{
}
