<?php
//Generation Date: 16-Sep-2020 03:54:49pm
namespace taktwerk\yiiboilerplate\modules\guide\controllers\base;

use taktwerk\yiiboilerplate\controllers\TwCrudController;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * GuideController implements the CRUD actions for Guide model.
 */
class GuideController extends TwCrudController
{
}
