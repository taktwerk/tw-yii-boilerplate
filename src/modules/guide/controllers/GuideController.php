<?php

namespace taktwerk\yiiboilerplate\modules\guide\controllers;

use taktwerk\yiiboilerplate\modules\guide\commands\GuidePdfController;
use taktwerk\yiiboilerplate\modules\guide\commands\ZipController;
use taktwerk\yiiboilerplate\modules\guide\models\Guide;
use taktwerk\yiiboilerplate\components\Helper;
use taktwerk\yiiboilerplate\grid\GridView;
use taktwerk\yiiboilerplate\widget\Select2;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use taktwerk\yiiboilerplate\modules\guide\models\GuideChild;

/**
 * This is the class for controller "GuideController".
 */
class GuideController extends \taktwerk\yiiboilerplate\modules\guide\controllers\base\GuideController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\guide\models\Guide';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\guide\models\search\Guide';

    /**
     * Additional actions for controllers, uncomment to use them
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'generate-pdf',
                            'generate-zip'
                        ],
                        'roles' => ['@']
                    ]
                ]
            ]
        ]);
    }

    /**
     * Init controller
     * @throws \Exception
     */
    public function init()
    {
        $this->customActions = [
            'pdf' => function ($url, $model, $key) {
                /** @var  $model Guide */
                $pdf_file = '/pdf/'.$model->client->id.'/'.$model->getPdfName();
                if(Yii::$app->user->can('guide_guide_generate-pdf')){
                    if(\Yii::$app->fs->has($pdf_file)){
                        $download_url = Helper::getFile("elfinder", "fs", $pdf_file);
                        $title = \Yii::t('twbp', 'Download PDF');
                        return Html::a('<span class="fa fa-download"></span> ' . $title, $download_url, [
                            'title' => $title,
                            'data-toggle' => 'modal',
                            'data-url' => $download_url,
                            'data-pjax' => 1,
                            'target'=>'_blank'
                        ]);
                    }else{
                        $title = \Yii::t('twbp', 'PDF not available yet');
                        return Html::a('<span class="fa fa-file-o"></span> ' . $title, "#", [
                            'title' => $title,
                            'class' => "generate-file",
                            'id' => "generate-file-".$model->id,
                            'data-value' => "pdf",
                            'data-pjax' => 1,
                        ]);
                    }
                }else{
                    return false;
                }

            },
            'zip' => function ($url, $model, $key) {
                /** @var  $model Guide */
                $zip_file = '/zip/'.$model->client->id.'/'.$model->getZipName();
                if(Yii::$app->user->can('guide_guide_generate-zip')){
                    if(\Yii::$app->fs->has($zip_file)){
                        $download_url = Helper::getFile("elfinder", "fs", $zip_file);

                        $title = \Yii::t('twbp', 'Download ZIP');
                        return Html::a('<span class="fa fa-download"></span> ' . $title, $download_url, [
                            'title' => $title,
                            'data-toggle' => 'modal',
                            'data-url' => $download_url,
                            'data-pjax' => 1,
                            'target'=>'_blank'
                        ]);
                    }else{
                        $title = \Yii::t('twbp', 'ZIP not available yet');
                        return Html::a('<span class="fa fa-archive"></span> ' . $title, "#", [
                            'title' => $title,
                            'class' => "generate-file",
                            'id' => "generate-file-".$model->id,
                            'data-value' => "zip",
                            'data-pjax' => 1,
                        ]);
                    }
                }else{
                    return false;
                }

            },
        ];

        $this->crudRelations = [
            'GuideSteps' => true,
            'GuideAssetPivots' => true,
            'GuideChildren0' => true
        ];

        $this->crudColumnsOverwrite = [
            'crud' => [
                'after#duration' => [
                    'attribute' => 'id', // why is fake attribute needed?
                    'label' => Yii::t('twbp', 'Guide Steps'),
                    'value' => function ($model) {
                        return $model->stepCount();
                    }
                ]
            ],
            'index' => [
                // index
                'duration'=>false,
                'after#revision_counter' => [
                    'attribute' => 'duration',
                    'content' => function ($model) {
                        return $model->durationTime(false);
                    },
                ],
                'preview_file' => false,
                'before#short_name' => [
                    'attribute' => 'preview_file',
                    'format' => 'html',
                    'content' => function ($model) {
                        return Html::a($model->showFilePreview('preview_file'), ['guide/view', 'id' => $model->id], ['data-pjax' => 0]);
                    },
                ],
                'after#description' => [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'guide_category_id',
                    'format' => 'html',
                    'content' => function ($model) {
                        $categories = [];
                        foreach ($model->categories as $category) {
                            if (Yii::$app->getUser()->can('app_guide-category_view') && $category->readable()) {
                                $categories[]= Html::a($category->toString, [
                                    'guide-category/view',
                                    'id' => $category->id,
                                ]);
                            } else {
                                $categories[] = $category->toString;
                            }

                        }
                        return count($categories) > 0 ? implode('<br>', $categories) : null;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'data' => \taktwerk\yiiboilerplate\modules\guide\models\GuideCategory::find()->count() > 50 ? null : \yii\helpers\ArrayHelper::map(\taktwerk\yiiboilerplate\modules\guide\models\GuideCategory::find()->all(), 'id', 'toString'),//\taktwerk\yiiboilerplate\modules\guide\models\GuideCategory::getDistinctValues(),
                        'initValueText' => \taktwerk\yiiboilerplate\modules\guide\models\GuideCategory::find()->count() > 50 ? \yii\helpers\ArrayHelper::map(\taktwerk\yiiboilerplate\modules\guide\models\GuideCategory::find()->andWhere(['guide_category.id' => $searchModel->guide_category_id])->all(), 'id', 'toString') : '',
                        'options' => [
                            'placeholder' => Yii::t('twbp', 'Select a value...'),
                            'multiple' => true,
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            (\taktwerk\yiiboilerplate\modules\guide\models\GuideCategory::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
                            (\taktwerk\yiiboilerplate\modules\guide\models\GuideCategory::find()->count() > 50 ? 'ajax' : '') => [
                                'url' => \yii\helpers\Url::to(['/guide/guide-category/list']),
                                'dataType' => 'json',
                                'data' => new \yii\web\JsExpression('function(params) {
                        return {
                            q:params.term, m: \'GuideCategory\'
                        };
                    }')
                            ],
                        ]
                    ],
                ],
                'protocol_template_id' => false,
                'template_id' => false,
                'all_step_files_size' => [
                    'attribute'=>'all_step_files_size',
                    'format'=>'html',
                    'contentOptions'=>[
                        'style'=>'white-space:pre;width:200px;'
                    ],
                ]
            ],
            'view' => [
                // view
                'preview_file' => false,
                'before#short_name' => [
                    'attribute' => 'preview_file',
                    'format' => 'html',
                    'value' => function ($model) {
                        return Html::a($model->showFilePreview('preview_file'), ['guide/update', 'id' => $model->id], ['data-pjax' => 0]);
                    },
                ],
                'duration' => [
                    'attribute' => 'duration',
                    'value' => function ($model) {
                        $duration = $model->durationTime(false);
                        return ($duration)?$duration . ' (' . $model->durationTime() . ')':'';
                    },
                ],
                'all_step_files_size' => [
                    'attribute'=>'all_step_files_size',
                    'format'=>'html',
                    'contentOptions'=>[
                        'style'=>'white-space:pre;width:200px;'
                    ],
                ]
            ]
        ];

        return parent::init();
    }

    public function actionGeneratePdf()
    {
        $guide_id = \Yii::$app->request->post('guide_id');
        $generate_type = \Yii::$app->request->post('generate_type');
        $commandParams = ['type' => $generate_type];
        $id = null;
        if($generate_type=="SPECIFIC"){
            $commandParams['id'] = $guide_id;
            $id = $guide_id;
        }
        if(!defined('STDIN'))  define('STDIN',  fopen('php://stdin',  'rb'));
        if(!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'wb'));
        if(!defined('STDERR')) define('STDERR', fopen('php://stderr', 'wb'));
        $consoleController = new GuidePdfController('guide-pdf', Yii::$app);
        $consoleController->actionGenerateGuidePdf($generate_type, $id);

        return true;
    }

    public function actionGenerateZip()
    {
        $guide_id = \Yii::$app->request->post('guide_id');

        if(!defined('STDIN'))  define('STDIN',  fopen('php://stdin',  'rb'));
        if(!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'wb'));
        if(!defined('STDERR')) define('STDERR', fopen('php://stderr', 'wb'));
        $consoleController = new ZipController('zip', Yii::$app);
        $consoleController->actionGenerate($guide_id);

        return true;
    }

    public function beforeAction($action)
    {
        $this->getCustomTab();
        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }

    public function formFieldsOverwrite($model, $form)
    {
        $guideSecId ='guide-children-sec';
        if($model->is_collection==1){
            $js = <<<EOT
$('#{$guideSecId}').show();

EOT;
        }

        if($model->getGuideSteps()->count()>0 || $model->getGuideChildren0()->count()>0){
            $js .= <<<EOT
$('div.field-guide-is_collection').addClass("disable-section");
EOT;
        }
        $this->view->registerJs($js);
        $childGuideQuery = \taktwerk\yiiboilerplate\modules\guide\models\Guide::find();//->where(['is_collection'=>0]);
        if(!$model->isNewRecord){
            $childGuideQuery->andWhere(['!=','guide.id',$model->id])->groupBy('guide.id');
        }
        $this->formFieldsOverwrite = [
            'total_step_file_size'=>false,
            'all_step_files_size'=>false,
            'protocol_template_id'=>Yii::$app->user->can('ProtocolAdmin'),
            'template_id'=>Yii::$app->user->can('Authority'),
            'after#duration' =>
                $form->field(
                    $model,
                    'tags'
                )
                    ->widget(
                        Select2::class,
                        [
                            'data' => \taktwerk\yiiboilerplate\modules\guide\models\GuideCategory::getDistinctValues(),
                            'options' => [
                                'placeholder' => Yii::t('twbp', 'Select a value...'),
                                'id' => 'tags' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                'multiple' => true
                            ],
                            'maintainOrder' => true,
                            'pluginOptions' => [
                                'tags' => false,
                                'maximumInputLength' => 45
                            ]
                        ]
                    )
                    ->hint($model->getAttributeHint('tags')),
            'after#is_collection'=>
                $form->field(
                    $model,
                    'childrenIds', ['options'=>['id'=>'guide-children-sec',
                        //'style'=>'display:none'
                    ]]
                )
                    ->widget(
                        Select2::class,
                        [
                            'data' => $childGuideQuery->count() > 50 ? null : ArrayHelper::map($childGuideQuery->all(), 'id', 'toString'),
                            'initValueText' =>  ArrayHelper::map($model->guideChildren0,'guide_id','guide.title'),
                            'options' => [
                                'placeholder' => Yii::t('twbp', 'Select a value...'),
                                'id' => 'childrenIds' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                'value'=>$model->children
                            ],
                            'pluginOptions' => [
                                'multiple' => true,
                                'allowClear' => false,
                                ($childGuideQuery->count() > 50 ? 'minimumInputLength' : '') => 3,
                                ($childGuideQuery->count() > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'children\'
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents'=>[
                                "change" => "function() { if(this.selectedOptions.length>0){
                        $('div.field-guide-is_collection').addClass(\"disable-section\")
                    }else{
                        $('div.field-guide-is_collection').removeClass(\"disable-section\")
}
                       
}",
                            ]

                        ]
                    )
                    ->hint($model->getAttributeHint('childrenIds'))
        ];

        return $this->formFieldsOverwrite;
    }

    public function getCustomTab(){

        $this->view->beginBlock('custom-actions');

        \yii\bootstrap\Modal::begin([
            'header' => '<h4 class="modal-title">' . Yii::t('twbp', 'PDF Generation') . '</h4>',
            'id' => 'generate-pdf'
        ]);
        ?>
        <p><?=Yii::t("twbp",'This PDF needs to be generated before you can download it. This can take up to 1 minute. Reload this page after generation to see the download button.')?></p>
        <?=Html::button(Yii::t('twbp', 'All PDF'),['class'=>'generate-pdf-btn btn btn-primary','data-value'=>'ALL'])?>
        <?=Html::button(Yii::t('twbp', 'All changed PDF'),['class'=>'generate-pdf-btn btn btn-primary','data-value'=>'HOURLY'])?>
        <?=Html::button(Yii::t('twbp', 'Only this PDF'),['class'=>'generate-pdf-btn btn btn-primary','data-value'=>'SPECIFIC'])?>

        <?php \yii\bootstrap\Modal::end();
        ?>

        <?php \yii\bootstrap\Modal::begin([
            'header' => '<h4 class="modal-title">' . Yii::t('twbp', 'ZIP Generation') . '</h4>',
            'id' => 'generate-zip'
        ]);
        ?>
        <p><?=Yii::t("twbp",'This ZIP needs to be generated before you can download it. This can take up to 1 minute. Reload this page after generation to see the download button.')?></p>
        <?=Html::button(Yii::t('twbp', 'Generate ZIP'),['class'=>'generate-zip-btn btn btn-primary'])?>

        <?php \yii\bootstrap\Modal::end();

        $generate_pdf_url = Url::toRoute('generate-pdf');
        $generate_zip_url = Url::toRoute('generate-zip');
        $js = <<<JS
$(document).ready(function () {

    $('.generate-file').on('click', function(e){
        var generate_id = $(this).attr('id');
        var id = generate_id.replace("generate-file-","");
        var type = $(this).attr('data-value');
        e.preventDefault();
        if(type=='pdf'){
            $('#generate-pdf').modal('show');
            $('.generate-pdf-btn').attr("data-id",id);
        }else{
            $('#generate-zip').modal('show');
            $('.generate-zip-btn').attr("data-id",id);
        }
    });
    
    $('.generate-pdf-btn').on('click', function(e){
        var guide_id = $(this).attr('data-id');
        var generate_type = $(this).attr('data-value');
        e.preventDefault();
        $.ajax({
          type: 'POST',
          url: "$generate_pdf_url",
          data: {
              guide_id: guide_id,
              generate_type: generate_type,
          },
          success: function(resultData) {
              //btn.button('reset');
              $('.generate-pdf-btn').removeAttr("data-id");
              $('#generate-pdf').modal('hide'); 
          }
        });
    });    
    $('.generate-zip-btn').on('click', function(e){
        var guide_id = $(this).attr('data-id');
        e.preventDefault();
        $.ajax({
          type: 'POST',
          url: "$generate_zip_url",
          data: {
              guide_id: guide_id,
          },
          success: function(resultData) {
              $('.generate-zip-btn').removeAttr("data-id");
              $('#generate-zip').modal('hide'); 
          }
        });
    });
});
JS;
        $this->view->registerJs($js);

        $this->view->endBlock();
    }
}
