<?php
namespace taktwerk\yiiboilerplate\modules\guide\controllers;

use Yii;
use taktwerk\yiiboilerplate\grid\GridView;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\modules\user\models\Group;
use yii\helpers\Html;

/**
 * This is the class for controller "GuideCategoryController".
 */
class GuideCategoryController extends \taktwerk\yiiboilerplate\modules\guide\controllers\base\GuideCategoryController
{

    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\guide\models\GuideCategory';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\guide\models\search\GuideCategory';

    // /**
    // * Additional actions for controllers, uncomment to use them
    // * @inheritdoc
    // */
    // public function behaviors()
    // {
    // return ArrayHelper::merge(parent::behaviors(), [
    // 'access' => [
    // 'class' => AccessControl::class,
    // 'rules' => [
    // [
    // 'allow' => true,
    // 'actions' => [
    // 'list-of-additional-actions',
    // ],
    // 'roles' => ['@']
    // ]
    // ]
    // ]
    // ]);
    // }

    /**
     * Init controller
     *
     * @throws \Exception
     */
    public function init()
    {
        $this->crudRelations = [
            'GuideCategoryBindings' => true
        ];
        $this->crudColumnsOverwrite = [
            'view' => [
                // view
                'after#name' => [
                    'attribute' => 'guide_category_group_id',
                    'format' => 'html',
                    'value' => function ($model) {
                    $categories = [];
                    foreach ($model->guideCategoryGroups as $cg) {
                        $n = $cg->group->name;
                        if (Yii::$app->getUser()->can('app_guide-category-group_view') && $cg->readable()) {
                            if($n){
                                $categories[] = Html::a($cg->group->name, [
                                    '/user/group/view',
                                    'id' => $cg->group_id
                                ]);}
                        } else {
                            if($n){
                                $categories[] = $n;
                            }
                        }
                    }
                    return count($categories) > 0 ? implode(', ', $categories) : null;
                    }
                    ],
                    
            ]
        ];
        return parent::init();
    }

    public function formFieldsOverwrite($model, $form)
    {
        $this->formFieldsOverwrite = [
            'after#name' => $form->field(
                $model,
                'group_ids'
                )
                ->widget(Select2::class, [
                'data' => ArrayHelper::map(Group::find()->all(), 'id', 'name'),
                'options' => [
                    'placeholder' => Yii::t('twbp', 'Select a value...'),
                    'id' => 'groups' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                    'multiple' => true
                ],
                'maintainOrder' => true,
                'pluginOptions' => [
                    'tags' => false,
                    'maximumInputLength' => 45
                ]
            ])
                ->hint($model->getAttributeHint('group_ids'))
        ];

        return $this->formFieldsOverwrite;
    }
}
