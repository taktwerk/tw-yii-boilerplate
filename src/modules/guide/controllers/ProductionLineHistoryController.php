<?php
//Generation Date: 10-Sep-2020 06:26:01am
namespace taktwerk\yiiboilerplate\modules\guide\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use taktwerk\yiiboilerplate\modules\guide\models\Guide;
use taktwerk\yiiboilerplate\modules\guide\models\ProductionLineHistory;
use yii\helpers\Html;

/**
 * This is the class for controller "ProductionLineHistoryController".
 */
class ProductionLineHistoryController extends \taktwerk\yiiboilerplate\modules\guide\controllers\base\ProductionLineHistoryController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\guide\models\ProductionLineHistory';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\guide\models\search\ProductionLineHistory';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [ 
                        'allow' => true,
                        'actions' => [
                            'scan-guide',
                      'roles' => ['@']
                  ]
                ]
            ]
        ],
        'verbs' => [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'scan-guide'  => ['POST'],
            ],
        ],
        ]);
    }
    public function init()
    {
        $this->customBlocks = [
            'index' => [
                'custom-block' => self::POS_MAIN
            ],
            'custom-block'=>self::POS_MAIN
        ];
        return parent::init();
    }
    public function actionScanGuide(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $short_name = \Yii::$app->request->post('q');
        $prod_line_id = \Yii::$app->request->post('prod_line_id');
        if($short_name){
            $guide = Guide::findOne(['short_name'=>$short_name]);
            if($guide){
                $model = new ProductionLineHistory();
                $model->production_line_id = $prod_line_id;
                $model->guide_id = $guide->id;
                $model->reference = (string)$guide->id;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    $errors =[];
                    foreach($model->errors as $atr){
                        foreach($atr as $er){
                            $errors[]= $er;
                        }
                    }
                    return ['success'=>false,'errors'=> $errors];
                }
            }else{
                return ['success'=>false,'errors'=>[\Yii::t('twbp','Guide not found')]];
            }
        }
        return ['success'=>false,'errors'=>[\Yii::t('twbp','Short name required')]];;
    }
}
