<?php

namespace taktwerk\yiiboilerplate\modules\guide\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * This is the class for controller "GuideAssetController".
 */
class GuideAssetController extends \taktwerk\yiiboilerplate\modules\guide\controllers\base\GuideAssetController
{
    /**
     * Model class with namespace
     */
    public $model = 'taktwerk\yiiboilerplate\modules\guide\models\GuideAsset';

    /**
     * Search Model class
     */
    public $searchModel = 'taktwerk\yiiboilerplate\modules\guide\models\search\GuideAsset';

//    /**
//     * Additional actions for controllers, uncomment to use them
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return ArrayHelper::merge(parent::behaviors(), [
//            'access' => [
//                'class' => AccessControl::class,
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => [
//                            'list-of-additional-actions',
//                        ],
//                        'roles' => ['@']
//                    ]
//                ]
//            ]
//        ]);
//    }

    /**
     * Init controller
     * @throws \Exception
     */
    public function init()
    {
        $this->crudColumnsOverwrite = [
            'crud' => [
                // index
                'pdf_image' => false,
            ]
        ];

        $this->crudRelations = [
            'GuideAssetPivots' => true
        ];

        return parent::init();
    }

    public function formFieldsOverwrite($model, $form)
    {
        $this->formFieldsOverwrite = [
           // 'pdf_image' => false
        ];

        return $this->formFieldsOverwrite;
    }
}
