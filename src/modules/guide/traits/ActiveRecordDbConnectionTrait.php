<?php

namespace taktwerk\yiiboilerplate\modules\guide\traits;

trait ActiveRecordDbConnectionTrait
{
    public static function getDb()
    {
        return \Yii::$app->db;
    }
}
