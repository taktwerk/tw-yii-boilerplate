<?php

namespace taktwerk\yiiboilerplate\modules\guide\actions;

use taktwerk\yiiboilerplate\modules\guide\models\Guide;
use taktwerk\yiiboilerplate\modules\guide\models\GuideStep;
use taktwerk\yiiboilerplate\actions\SortableAction;
use Yii;
use yii\db\Expression;
use yii\web\HttpException;

class GuideStepSortAction extends SortableAction
{
    /**
     *
     */
    public function run()
    {
        if (! Yii::$app->request->isAjax) {
            throw new HttpException(404);
        }

        $items = \Yii::$app->request->post('items');

        if (isset($items) && is_array($items)) {
            $activeRecordClassName = $this->activeRecordClassName;
            $tbl = $activeRecordClassName::tableName();
            $pk = $activeRecordClassName::primaryKey()[0];
            $oc = $this->orderColumn;
            $records = $activeRecordClassName::find()->select([
                $tbl.'.'.$pk,
                $tbl.'.'.$oc,
                $tbl.'.'.'guide_id'
            ])
                ->where([
                    $tbl.'.'.$pk => $items
            ])
            ->orderBy($tbl.'.'.$oc . ' ASC')
                ->all();
            $newOrder = [];
            foreach ($items as $i => $item) {
                $newOrder[$i] = $records[$i]->$oc;
            }
            $previousOrder = null;
            foreach ($newOrder as $i => $order) {
                $id = $items[$i];
                if ($previousOrder == $order) {
                    $order++;
                } else if ($previousOrder > $order) {
                    $order = $previousOrder + 1;
                }
                foreach ($records as $record) {
                    if ($record->{$pk} == $id) {
                        $changedAttributes = [
                            $oc => $order,
                            'updated_at' => new Expression('NOW()')
                        ];
                        $record->updateAttributes($changedAttributes);
                        if ($record->guide) {
                            $record->guide->afterSave(false, []);
                        }
                        break;
                    }
                }

                $previousOrder = $order;
            }
        }
    }
}
