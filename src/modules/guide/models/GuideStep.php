<?php

namespace taktwerk\yiiboilerplate\modules\guide\models;

use taktwerk\yiiboilerplate\modules\guide\controllers\GuideStepController;
use taktwerk\yiiboilerplate\modules\guide\Module;
use taktwerk\yiiboilerplate\components\Helper;
use taktwerk\yiiboilerplate\modules\backend\models\RelationModelInterface;
use taktwerk\yiiboilerplate\widget\Viewer3DModel;
use Yii;
use \taktwerk\yiiboilerplate\modules\guide\models\base\GuideStep as BaseGuideStep;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use taktwerk\yiiboilerplate\modules\sync\traits\UserAppDataVersionTrait;
use taktwerk\yiiboilerplate\components\ImageHelper;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use taktwerk\yiiboilerplate\commands\FileCompressor;

/**
 * This is the model class for table "guide_step".
 */
class GuideStep extends BaseGuideStep implements RelationModelInterface
{
    use UserAppDataVersionTrait;
    public static function find($removedDeleted = true){
        $query = parent::find($removedDeleted);
        if(array_key_exists('REQUEST_METHOD', $_SERVER) && !Yii::$app->user->can('Authority')){
            $query->joinWith('guide');
            if(\Yii::$app->user->can('GuiderGroup'))
            {
                $query->groupBy('guide_step.id');
            }
        }
        return $query;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                ['guide_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => \taktwerk\yiiboilerplate\modules\guide\models\Guide::className(),
                'targetAttribute' => ['guide_id' => 'id']
            ],
            ['guide_id', 'validateGuide'],
        ]);
    }
    public function validateGuide($attribute, $params, $validator)
    {
        $guide = Guide::findOne($this->$attribute);
        if($guide && $guide->is_collection==1){
            $this->addError($attribute, \Yii::t('twbp', 'Selected Guide is a collection'));
        }           
    }
    /**
     * @param $attribute
     * @return bool
     */
    public function isFileVideo($attribute = 'attached_file')
    {
        return strpos($this->getFileMimeType($attribute), 'video/') !== false;
    }

    public function isFileAudio($attribute = 'attached_file')
    {
        return strpos($this->getFileMimeType($attribute), 'audio/') !== false;
    }

    public function isFilePdf($attribute = 'attached_file')
    {
        return strpos($this->getFileMimeType($attribute), 'application/pdf') !== false;
    }

    /**
     * @param string $attribute
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function getFileMimeType($attribute = 'attached_file', $compressedFileMimeType=false)
    {
        $path = $this->getUploadPath() . $this->{$attribute};
        if($compressedFileMimeType){
            $paths = Yii::$app->fs->listContents($this->getUploadPath());
            $pathInfo = pathinfo($path);
            $fileName = $pathInfo['filename'];
            $fileType = $this->getFileType($attribute);
            $isVideo = ($fileType == 'video');
            $isAudio = ($fileType == 'audio');

            if (($isVideo||$isAudio) && ! empty($paths)) {
                $ext = 'mp4';
                $ext =($isAudio)?(strlen(getenv('compressed_audio_format')>0)?getenv('compressed_audio_format'):'mp3'):$ext;
                foreach ($paths as $file) {
                    if(preg_match("/{$fileName}/i", $file['filename']) ){
                        if ($file['extension'] == $ext) {
                            if (strpos($file['basename'],ClassDispenser::getMappedClass(FileCompressor::class)::COMPRESSED_FILE_POSTFIX . "." . $ext)> 0){
                                $path = $file['path'];
                                break;
                            }
                        }
                    }
                }
            }
        }
        if (Yii::$app->get('fs')->has($path)) {
            return Yii::$app->get('fs')->getMimetype($path);
        }
        return false;
    }

    /**
     * @param string $attribute
     * @return int
     * @throws \yii\base\InvalidConfigException
     */
    public function getFileSize($attribute = 'attached_file')
    {
        $path = $this->getUploadPath() . $this->$attribute;
        if (Yii::$app->get('fs')->has($path)) {
            return Yii::$app->get('fs')->getSize($path);
        }
        return 0;
    }

    /**
     * @param bool $first
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getAttachedFileHtml($first = false)
    {
        // Empty
        if (empty($this->attached_file) && empty($this->design_canvas)) {
            return false;
        }

        $image = $this->getCanvasAttribute('attached_file');
        $attribute = 'attached_file';

        // Video
        if ($this->isFileVideo($attribute)) {
            $mType = $this->getFileMimeType($attribute,true);
            if($mType === 'video/quicktime')
            {
                $mType = 'video/mp4';
            }
            return '<div class="embed-responsive embed-responsive-16by9">
                <video ' . (($imageSrc = $this->getFileUrl($image)) ? 'poster="' . $imageSrc . '"':'') . ' class="afterglow embed-responsive-item" webkit-playsinline playsinline ' . ($first && false ? 'autoplay' : '') . '>
                    <source src="' . $this->getFileUrl($attribute) . '" type="' . $mType . '">
                </video>
            </div>';
        } else if ($this->isFileAudio($attribute)) {
            return '<div class="">
                <audio controls controlsList="nodownload">
                  <source src="'.$this->getFileUrl($attribute).'" type="'.$this->getFileMimeType($attribute,true).'">
                </audio>
            </div>';
        } else if ($this->isFilePdf($attribute)) {
            return '<div class="">
                <a style="cursor: pointer;display: table;margin: auto;"
                   href="' . $this->getFileUrl($attribute) . '"
                   target="_blank"
                >
                    <i class="material-icons"
                       style="font-size: 14em;padding: 1rem;background-color: white;border-radius: 8rem;"
                    >
                       picture_as_pdf
                    </i>
                </a>
            </div>';
        } else if ($this->is3dModelFile($attribute)) {
            return
                '<div class="pinchSlider">
                    <div id="stepSlider' . $this->id . '"
                         class="slider"
                         data-elem="touchnswipe"
                         style="width:1px; height: 1px; overflow: hidden; position: absolute"
                    >
                        <div class="slideHolder"
                             data-elem="slides"
                             data-options="loop:false; slideOptions:{ maxZoom:3; allowMouseWheelScroll:true}"
                        >
                            <div class="3d-step-model-' . $this->id . ' slider-element"
                                 data-elem="slide"
                                 data-index="0"
                                 data-model-path="' . (!empty($this->getMinFilePath($attribute)) ? 
                                                $this->getMinFilePath($attribute) :
                                                $this->getFileUrl($attribute, [], true)) . '"
                                 data-id="' . $this->id . '"
                                 data-model-name="guideStep"
                                 style="width: 100%; height: 100%;"
                            >' . Viewer3DModel::widget(['modelPath' => (!empty($this->getMinFilePath($attribute)) ? 
                                                $this->getMinFilePath($attribute) :
                                                $this->getFileUrl($attribute, [], true))
                                ]) . '
                            </div>
                        </div>
                    </div>
                </div>
                <div class="3d-step-model-thumb h-100"
                     data-model-path="' . $this->getFileUrl($attribute, [], true) . '"
                     data-id="' . $this->id . '"
                     style="min-height: 150px; max-height: 500px;"
                 >' .
                Viewer3DModel::widget(['modelPath' => !empty($this->getMinFilePath($attribute)) ? 
                                                $this->getMinFilePath($attribute) :
                                                $this->getFileUrl($attribute, [], true)])
                 . '
                    <div class="btn btn-primary 3d-step-model-zoom"
                         data-model-path="' . (!empty($this->getMinFilePath($attribute)) ? 
                                                $this->getMinFilePath($attribute) :
                                                $this->getFileUrl($attribute, [], true)) . '"
                         data-id="' . $this->id . '"
                         data-model-name="guideStep"
                         data-elem="thumb"
                         data-options="sliderId:stepSlider'.$this->id.';
                         index:0; fullscreen:true; offCss:{alpha:1}"
                         style="top: 0;left: 15px;position: absolute;"
                    >
                        Zoom
                    </div>
                 </div>';
        } else {
            return
            '<div class="pinchSlider">
<div id="stepSlider'.$this->id.'" class="slider" data-elem="touchnswipe" style="width:1px; height: 1px; overflow: hidden; position: absolute">
    <div class="slideHolder" data-elem="slides" data-options="loop:true; slideOptions:{ maxZoom:2; allowMouseWheelScroll:true}">
        <div data-elem="slide" > <img data-elem="bg" data-src="' . $this->getFileUrl($image, ['width' => 1280, 'height' => 1024], true) . '" ></div>
    </div>
</div>
</div>
<img class="d-block img-thumbnail img-fluid rounded" src="' . $this->getFileUrl($image, ['width' => 1280, 'height' => 1024], true) . '" data-elem="thumb" data-options="sliderId:stepSlider'.$this->id.'; index:0; fullscreen:true; offCss:{alpha:1}">';
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return array_merge(parent::attributeHints(), [
            'attached_file' => Yii::t('twbp', 'Choose an image, video, audio, pdf or 3D file.'),
            'design_canvas' => Yii::t('twbp', 'Add a design layer to image, like arrows, text, squares, etc.')
        ]);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        // If we are dealing with a new record
        if ($insert === true) {
            if (empty($this->order_number) && empty($this->id)) {
                $lastStep = GuideStep::find()->where([
                    'guide_id' => $this->guide_id
                ])
                    ->orderBy([
                    'order_number' => SORT_DESC
                ])
                    ->one();
                $this->order_number = ! empty($lastStep) ? ((int) $lastStep->order_number) + 1 : 1;
            }
        }
        $this->guide->save();
        return parent::beforeSave($insert);
    }

    /**
     * Get step image for pdf
     *
     * @return string
     * @throws \yii\base\Exception
     */
    public function getStepPicture($pictureStyle = '', $showCompressImg = true)
    {
        $file = $this->getImageTmpPath('attached_file', ['width' => getenv('GUIDER_PDF_IMAGE_WIDTH'), 'ratio' => '1_1'], $showCompressImg);
        if(!$file && $this->isFileVideo()) {
            $file = Yii::getAlias('@web/img/videooverlay.png');
        }
        if($file){
            return Html::img($file, [
                'class' => 'guide-step-img',
                'style' => $pictureStyle
            ]);
       }
        return null;
    }

    public function getDescription() {
        return str_replace('<p><br data-cke-filler="true"></p>', '', $this->description_html);
    }

    /**
     * @inheritDoc
     */
    public function getControllerInstance()
    {
        return new GuideStepController("guide-step", Module::getInstance());
    }

    /**
     * @param $guide_id
     * @return array
     */
    public static function getAllFileSizes($guide_id){
        $all_guide_steps = self::find()->where(['guide_id'=>$guide_id])->all();
        $sizes['file'] = null;
        $sizes['video'] = null;
        $sizes['audio'] = null;
        $sizes['3d'] = null;
        $sizes['image'] = null;

        foreach ($all_guide_steps as $guide_step){
            $attribute = 'attached_file';

            // Video
            if ($guide_step->isFileVideo($attribute)) {
                $sizes['video'] += $guide_step->getFileSize($attribute);
            } else if ($guide_step->isFileAudio($attribute)) {
                $sizes['audio'] += $guide_step->getFileSize($attribute);
            } else if ($guide_step->isFilePdf($attribute)) {
                $sizes['file'] += $guide_step->getFileSize($attribute);
            } else if ($guide_step->is3dModelFile($attribute)) {
                $sizes['3d'] += $guide_step->getFileSize($attribute);
            } else {
                $sizes['image'] += $guide_step->getFileSize($attribute);
            }
        }

        return $sizes;
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $old_attribute = null;
        if (!empty($this->oldAttributes['attached_file'])) {
            $old_attribute = $this->oldAttributes['attached_file'];
            
        }

        $save = parent::save($runValidation, $attributeNames);
        
        if($old_attribute!=$this->attributes['attached_file']){
            $this->guide->updateSize();
        }
        
        return $save;
    }
}

