<?php

namespace taktwerk\yiiboilerplate\modules\guide\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\guide\models\base\GuideData as BaseGuideData;
use yii\helpers\Inflector;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use taktwerk\yiiboilerplate\helpers\CrudHelper;
use taktwerk\yiiboilerplate\modules\customer\models\ClientModelType;

/**
 * This is the model class for table "guide_data".
 */
class GuideData extends BaseGuideData
{
    public static function find($removedDeleted = true){
        $query = parent::find($removedDeleted);
        if(array_key_exists('REQUEST_METHOD', $_SERVER) && !Yii::$app->user->can('Authority')){
            $query->joinWith('guide');
        }
        return $query;
    }
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                ['type_ref'],
                'validateTypeRef'
            ],
        ]);
    }
    public function validateTypeRef($attribute, $params, $validator)
    {
            $refClass = $this->getTypeRefClass();
            if($refClass){
                $check = $refClass::findOne($this->$attribute);
                if($check == null){
                    $this->addError($attribute, \Yii::t('twbp', '{0} is invalid.',[
                        $this->getAttributeLabel($attribute)
                    ]));
                }
            }
    }
    public function getTypeRefModel($filterClient=false){
        $clientModelType = ($filterClient==true)?$this->clientModelType:ClientModelType::find(['filterClient'=>false])
        ->where([ClientModelType::tableName().'.id'=>$this->client_model_type_id])->one();
        if($clientModelType){
            $modelName = Inflector::camelize($clientModelType->type);
            $tableName = $clientModelType->type;
            $modelName = ClassDispenser::getMappedClass(CrudHelper::class)::findModelClass($modelName,$tableName);
            if($modelName){
                return $modelName::findOne($this->type_ref);
            }
        }
        return null;
    }
    public function getTypeRefClass($filterClient=false){
        $clientModelType = ($filterClient==true)?$this->clientModelType:ClientModelType::find(['filterClient'=>false])
        ->where([ClientModelType::tableName().'.id'=>$this->client_model_type_id])->one();
        if($clientModelType){
            $modelName = Inflector::camelize($clientModelType->type);
            $tableName = $clientModelType->type;
            $modelName = ClassDispenser::getMappedClass(CrudHelper::class)::findModelClass($modelName,$tableName);
            return $modelName;
        }
        return null;
    }
    
    /* public function getTypeRefModelQuery(){
        if($this->clientModelType){
            $modelName = Inflector::camelize($this->clientModelType->type);
            $tableName = $this->clientModelType->type;
            $modelName = ClassDispenser::getMappedClass(CrudHelper::class)::findModelClass($modelName,$tableName);
            if($modelName){
                return $modelName::findOne($this->type_ref);
            }
        }
        return null;
    } */
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels =  parent::attributeLabels();
        $labels['type_ref'] = Yii::t('twbp', 'Ref Model');
        return $labels;
    }
}
