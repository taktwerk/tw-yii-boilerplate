<?php
//Generation Date: 09-Oct-2020 12:52:45pm
namespace taktwerk\yiiboilerplate\modules\guide\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\guide\models\base\GuideViewHistory as BaseGuideViewHistory;
use taktwerk\yiiboilerplate\modules\customer\models\User;
use yii\helpers\Json;
use \taktwerk\yiiboilerplate\modules\user\models\UserGroup;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "guide_view_history".
 */
class GuideViewHistory extends BaseGuideViewHistory
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }
    public function getCreatedBy(){
        return $this->hasOne(User::class,['id'=>'created_by']);
    }
    public static function getAssignableUserListQuery(){
        $q = User::find()->where(['!=',User::getDb()->tablePrefix. User::tableName().'.id',\Yii::$app->user->identity->id]);
        if(!\Yii::$app->user->can('Authority')){
            if(\Yii::$app->user->can('guide_explore_user_group')){
                $groupIds = ArrayHelper::map(UserGroup::find()->select('group_id')
                    ->where([
                        'user_id' => \Yii::$app->user->identity->id
                    ])
                    ->asArray()
                    ->all(), 'group_id', 'group_id');
                if (count($groupIds) > 0) {
                    $q->join(
                        'LEFT JOIN',
                        UserGroup::tableFullName(),
                        UserGroup::tableFullName() . '.`user_id`=' .User::getDb()->tablePrefix. User::tableName() . '.`created_by`');
                    $q->andWhere([
                        UserGroup::tableFullName() . '.`group_id`' => $groupIds
                    ]);
                }
            }else{
                $q->andWhere(['user.client_id'=>\Yii::$app->user->identity->client_id]);
            }
        }
        return $q;
    }
    public function transferToUser($userId)
    {
        $userValid = self::getAssignableUserListQuery()->andWhere(['id'=>$userId])->exists();
        if(!$userValid){
            throw new \Exception('Invalid User Selected');
        }
        $transaction = \Yii::$app->db->beginTransaction();
        self::deleteAll(['user_id'=>$userId,'guide_id'=>$this->guide_id]);
        
        $this->user_id = $userId;
        if ($this->save()) {
            $model = new GuideTransferHistory();
            $model->to_user_id = $userId;
            $model->guide_view_history_id = $this->id;
            try {
                $viewHistoryData = Json::decode($this->data);
                if (isset($viewHistoryData['step_order_number'])) {
                    $model->step = (string)$viewHistoryData['step_order_number'];
                }
            } catch (\Exception $e) {
                $viewHistoryData = [];
            }
            if ($model->save()) {
                $transaction->commit();
                return $model;
            }
        }
        $transaction->rollBack();
        return false;
    }
}
