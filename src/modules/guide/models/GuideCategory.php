<?php

namespace taktwerk\yiiboilerplate\modules\guide\models;

use taktwerk\yiiboilerplate\modules\guide\models\base\GuideCategory as BaseGuideCategory;
use taktwerk\yiiboilerplate\modules\sync\traits\UserAppDataVersionTrait;
use taktwerk\yiiboilerplate\modules\user\models\Group;
use taktwerk\yiiboilerplate\widget\Select2;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use taktwerk\yiiboilerplate\modules\guide\models\base\GuideCategoryGroup;

/**
 * This is the model class for table "guide_category".
 */
class GuideCategory extends BaseGuideCategory
{

    public $guide_category_group_id;

    public $group_ids;
    use UserAppDataVersionTrait;
    public static function populateRecord($record, $row){
        parent::populateRecord($record, $row);
        foreach($record->guideCategoryGroups as $tag){
            $record->group_ids[] = $tag->group_id;
        }
    }
    /**
     * List of additional rules to be applied to model, uncomment to use them
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['guide_category_group_id'], 'safe'],
            [
                'group_ids',
                'required'
            ],
            [
                'group_ids',
                'each',
                'skipOnEmpty'=>false,
                'message'=>\Yii::t('twbp','Please select a group'),
                'rule'=>[
                    'exist',
                    'skipOnError' => true,
                    'targetClass' => \taktwerk\yiiboilerplate\modules\user\models\Group::class,
                    'targetAttribute' => ['group_ids' => 'id']
                ]
            ]
        ]);
    }

    /**
     * @param string $field
     * @return array
     */
    public static function getDistinctValues($field = 'name')
    {
        $tags = [];
        foreach (self::find()->joinWith('guideCategoryGroups')->all() as $tag) {
            $tags[$tag->$field] = $tag->$field;
        }
        return $tags;
    }

    /**
     * @return false|int|void
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function delete()
    {
        foreach ($this->guideCategoryBindings as $b) {
            $b->forceDelete();
        }
        parent::delete();
    }
    /**
     * Get the Selected Groups
     */
    public function getSelectedGroups()
    {
        $groups = [];
        foreach ($this->guideCategoryGroups as $tag) {
            $groups[$tag->group_id] = $tag->group_id;
        }
        return $groups;
    }
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return array_merge(parent::attributeHints(), [
            'selectedGroups' => Yii::t('twbp', 'Choose the related groups of the category'),
            'group_ids' => Yii::t('twbp', 'Choose the related groups of the category'),
        ]);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'selectedGroups' => Yii::t('twbp', 'Groups'),
            'guide_category_group_id'=> Yii::t('twbp', 'Groups'),
            'group_ids'=> Yii::t('twbp', 'Groups'),
        ]);
    }
    /**
     * After Save event
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveGuideCategoryGroups();
        $this->changeUserAppDataVersion($insert, $changedAttributes);
    }
    /**
     * Manage category bindings after a guide has been saved
     */
    protected function saveGuideCategoryGroups()
    {
        // Manage Groups
        if (Yii::$app instanceof \yii\web\Application) {

            $request = Yii::$app->getRequest()->post('GuideCategory');
            if ($request && key_exists('group_ids',$request)) {

                $existingGroups = [];
                foreach ($this->guideCategoryGroups as $guideCategoryGroup) {
                    $existingGroups[$guideCategoryGroup->group_id] = $guideCategoryGroup;
                }
                $submittedGroups = ! empty($request['group_ids']) ? $request['group_ids'] : [];

                // Loop submitted Groups to see if they are new or existing (and don't require deleting)
                foreach ($submittedGroups as $groupId) {
                    if (! empty($existingGroups[$groupId])) {
                        unset($existingGroups[$groupId]);
                    } else {
                        $group = Group::find()->where([
                            'id' => $groupId
                        ])->one();
                        if($group==null){
                            continue;
                        }
                        // Already have this binding?
                        $guideCatGroup = GuideCategoryGroup::find()->where([
                            'group_id' => $groupId,
                            'guide_category_id' => $this->id
                        ])->one();
                        if (empty($guideCatGroup)) {
                            $tagModel = new GuideCategoryGroup();
                            $tagModel->guide_category_id = $this->id;
                            $tagModel->group_id = $groupId;
                            $tagModel->save();
                        }
                    }
                }

                // Remove old Groups
                foreach ($existingGroups as $groupId => $tag) {
                    // We don't want to keep soft-deletes of Groups so force delete.
                    $tag->forceDelete();
                }
            }
        }
    }

    /**
     * @param $model
     * @param $pjaxContainer
     * @param bool $is_search_form
     * @return array|void
     * @throws \ReflectionException
     */
    public function getCustomFilters($model, $pjaxContainer=null, $is_search_form=false){
        $reflection = new \ReflectionClass($model);
        $shortModelName = $reflection->getShortName();
        $suffix = ($is_search_form?"-form":"");
        $isGuideAdmin = \Yii::$app->user->can('GuiderAdmin');
        $group_name = Html::hiddenInput("{$shortModelName}[guide_category_group_id]");
        $group_name .= Select2::widget([
            'pjaxContainerId' => $pjaxContainer,
            'data' => ArrayHelper::merge(
                ArrayHelper::map($isGuideAdmin?\taktwerk\yiiboilerplate\modules\user\models\base\Group::find()->all():Group::find()->all(),'id','toString'),
                [
                    'none'=>Yii::t('twbp','None')
                ]
            ),
            'value'=> $model->guide_category_group_id,
            'name' => "{$shortModelName}[guide_category_group_id]",
            'id'=> "{$shortModelName}-guide_category_group_id".$suffix,
            'options' => [
                'placeholder' => Yii::t('twbp', 'Select a value...'),
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true,
            ],
        ]);

        return [
            [
                'attribute' => 'guide_category_group_id',
                'value' => function ($model){
                    $isGuideAdmin = \Yii::$app->user->can('GuiderAdmin');
                    if($isGuideAdmin){
                        $allGroupIds = (new \yii\db\Query())
                        ->select(['group_id'])
                        ->from('guide_category_group')
                        ->where(['AND',['guide_category_group.deleted_at'=>null],['guide_category_id'=>$model->id]])
                        ->column();
                    }else{
                        $allGroupIds = ArrayHelper::getColumn(
                            GuideCategoryGroup::find()
                            ->where(['guide_category_id'=>$model->id])
                            ->all(),'group_id');
                    }
                    $groupQ = Group::find();
                    if($isGuideAdmin){
                        $groupQ = \taktwerk\yiiboilerplate\modules\user\models\base\Group::find();
                    }
                    $groups = ArrayHelper::getColumn($groupQ
                        ->where(['in','id',$allGroupIds])
                        ->all(),'toString');

                    if($groups){
                        return implode(", ",$groups);
                    }
                },
                'format' => 'raw',
                'label' => Yii::t('twbp','Groups'),
                'filter' => $group_name,
            ]
        ];
    }
}
