<?php

namespace taktwerk\yiiboilerplate\modules\guide\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\guide\models\base\GuideChild as BaseGuideChild;
use taktwerk\yiiboilerplate\modules\sync\traits\UserAppDataVersionTrait;

/**
 * This is the model class for table "guide_child".
 */
class GuideChild extends BaseGuideChild
{
    use UserAppDataVersionTrait;
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['parent_guide_id', 'validateParentGuide'],
            ['parent_guide_id','compare', 'compareAttribute' => 'guide_id','operator'=>'!=',
                'message'=>\Yii::t('twbp','Both guides are same')],
            [['parent_guide_id'],
            'unique', 'targetAttribute' =>
            ['parent_guide_id', 'guide_id'],'message'=>\Yii::t('twbp','This combination already exist')],
          //  ['guide_id', 'validateGuide'],
        ]);
    }
    public function validateParentGuide($attribute, $params, $validator)
    {
        $guide = Guide::findOne($this->$attribute);
        if($guide->is_collection==0){
            $this->addError($attribute, \Yii::t('twbp', 'Parent Guide must be a collection'));
        }
        $exist = static::find()->where([
            'guide_id' => $this->$attribute,'parent_guide_id'=>$this->guide_id])
        ->exists();
        if($exist){
            $this->addError($attribute, \Yii::t('twbp', 'Invalid Parent-Child combination selected'));
        }
        
    }

    
    public function clientsForChangeAppDataVersion()
    {
        $clientIds = [];
        $clientIds[] = $this->parentGuide->client_id;
        $clientIds[] = $this->guide->client_id;

        return $clientIds;
    }
    /* uncomment if you want child guide to not be a collection
    public function validateGuide($attribute, $params, $validator)
    {
         
        $guide = Guide::findOne($this->$attribute);
        if($guide->is_collection==1){
            $this->addError($attribute, \Yii::t('twbp','Child Guide must be not a collection'));
        } 
    }
    */
}
