<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\search;

use taktwerk\yiiboilerplate\modules\guide\models\search\base\GuideData as GuideDataSearchModel;

/**
* GuideData represents the model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\GuideData`.
*/
class GuideData extends GuideDataSearchModel{

}