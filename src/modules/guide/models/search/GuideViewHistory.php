<?php
//Generation Date: 09-Oct-2020 12:53:15pm
namespace taktwerk\yiiboilerplate\modules\guide\models\search;

use taktwerk\yiiboilerplate\modules\guide\models\search\base\GuideViewHistory as GuideViewHistorySearchModel;
use taktwerk\yiiboilerplate\modules\guide\models\GuideViewHistory as GuideViewHistoryModel;
use yii\data\Sort;
/**
* GuideViewHistory represents the model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\GuideViewHistory`.
*/
class GuideViewHistory extends GuideViewHistorySearchModel{
    public function search($params){
        $dataProvider = parent::search($params);
        $dataProvider->sort->defaultOrder = ['updated_at' => SORT_DESC];
        return $dataProvider;
    }
}