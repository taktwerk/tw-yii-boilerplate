<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\search;

use taktwerk\yiiboilerplate\modules\guide\models\search\base\GuideChild as GuideChildSearchModel;

/**
* GuideChild represents the model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\GuideChild`.
*/
class GuideChild extends GuideChildSearchModel{

}