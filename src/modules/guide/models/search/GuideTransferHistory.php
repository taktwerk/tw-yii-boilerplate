<?php
//Generation Date: 21-Oct-2020 08:55:22am
namespace taktwerk\yiiboilerplate\modules\guide\models\search;

use taktwerk\yiiboilerplate\modules\guide\models\search\base\GuideTransferHistory as GuideTransferHistorySearchModel;

/**
* GuideTransferHistory represents the model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\GuideTransferHistory`.
*/
class GuideTransferHistory extends GuideTransferHistorySearchModel{

}