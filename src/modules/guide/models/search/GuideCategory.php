<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\search;

use taktwerk\yiiboilerplate\modules\guide\models\search\base\GuideCategory as GuideCategorySearchModel;
use Yii;
use taktwerk\yiiboilerplate\modules\guide\models\GuideCategoryGroup;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * GuideCategory represents the model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\GuideCategory`.
 */
class GuideCategory extends GuideCategorySearchModel{

    /**
     * List of additional rules to be applied to model, uncomment to use them
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['guide_category_group_id'], 'safe'],
        ]);
    }

    public function search($params)
    {
        $dataProvider = parent::search($params);
        $query = $dataProvider->query;
        if(is_array($this->guide_category_group_id) && count($this->guide_category_group_id)>=1){
            $ids = ArrayHelper::getColumn(
                GuideCategoryGroup::find()
                    ->andWhere(['in', 'group_id',$this->guide_category_group_id])
                    ->all(),'guide_category_id');

            if(in_array("none", $this->guide_category_group_id, true)) {
                $ids = ArrayHelper::merge($ids,ArrayHelper::getColumn(self::find()
                    ->where(['not in', static::tableName() . '.id', ArrayHelper::getColumn(GuideCategoryGroup::find()->all(), 'guide_category_id')])
                    ->all(), 'id'));
            }
            $query->andFilterWhere(['in',static::tableName().'.id', $ids]);
        }

        if(in_array("none", $this->guide_category_group_id, true) && count($this->guide_category_group_id)<2){
            $ids = ArrayHelper::getColumn(self::find()
                ->where(['not in',static::tableName().'.id', ArrayHelper::getColumn(GuideCategoryGroup::find()->all(),'guide_category_id')])->all(),
                'id');
            $query->andFilterWhere(['in',static::tableName().'.id',$ids]);
        }

        return $dataProvider;
    }

}