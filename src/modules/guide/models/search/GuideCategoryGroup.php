<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\search;

use taktwerk\yiiboilerplate\modules\guide\models\search\base\GuideCategoryGroup as GuideCategoryGroupSearchModel;

/**
* GuideCategoryGroup represents the model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\GuideCategoryGroup`.
*/
class GuideCategoryGroup extends GuideCategoryGroupSearchModel{

}