<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\search;

use taktwerk\yiiboilerplate\modules\guide\models\search\base\GuideTag as GuideTagSearchModel;

/**
* GuideTag represents the model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\GuideTag`.
*/
class GuideTag extends GuideTagSearchModel{

}