<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\search;

use taktwerk\yiiboilerplate\modules\guide\models\search\base\GuideAssetPivot as GuideAssetPivotSearchModel;

/**
* GuideAssetPivot represents the model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\GuideAssetPivot`.
*/
class GuideAssetPivot extends GuideAssetPivotSearchModel{

}