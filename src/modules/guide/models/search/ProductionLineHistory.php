<?php
//Generation Date: 10-Sep-2020 06:26:01am
namespace taktwerk\yiiboilerplate\modules\guide\models\search;

use taktwerk\yiiboilerplate\modules\guide\models\search\base\ProductionLineHistory as ProductionLineHistorySearchModel;

/**
* ProductionLineHistory represents the model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\ProductionLineHistory`.
*/
class ProductionLineHistory extends ProductionLineHistorySearchModel{

}