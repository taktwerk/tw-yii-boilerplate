<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\search;

use taktwerk\yiiboilerplate\modules\guide\models\search\base\GuideSetting as GuideSettingSearchModel;

/**
* GuideSetting represents the model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\GuideSetting`.
*/
class GuideSetting extends GuideSettingSearchModel{

}