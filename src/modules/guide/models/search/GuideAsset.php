<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\search;

use taktwerk\yiiboilerplate\modules\guide\models\search\base\GuideAsset as GuideAssetSearchModel;

/**
* GuideAsset represents the model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\GuideAsset`.
*/
class GuideAsset extends GuideAssetSearchModel{

}