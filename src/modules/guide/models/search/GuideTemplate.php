<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\search;

use taktwerk\yiiboilerplate\modules\guide\models\search\base\GuideTemplate as GuideTemplateSearchModel;

/**
* GuideTemplate represents the model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\GuideTemplate`.
*/
class GuideTemplate extends GuideTemplateSearchModel{

}