<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\search;

use taktwerk\yiiboilerplate\modules\guide\models\search\base\GuideCategoryBinding as GuideCategoryBindingSearchModel;

/**
* GuideCategoryBinding represents the model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\GuideCategoryBinding`.
*/
class GuideCategoryBinding extends GuideCategoryBindingSearchModel{

}