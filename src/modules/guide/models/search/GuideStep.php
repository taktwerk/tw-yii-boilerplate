<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\search;

use taktwerk\yiiboilerplate\modules\guide\models\search\base\GuideStep as GuideStepSearchModel;

/**
* GuideStep represents the model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\GuideStep`.
*/
class GuideStep extends GuideStepSearchModel{

}