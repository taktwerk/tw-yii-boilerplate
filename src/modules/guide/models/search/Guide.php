<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\search;

use Yii;
use taktwerk\yiiboilerplate\modules\guide\models\Guide as GuideModel;
use taktwerk\yiiboilerplate\modules\guide\models\GuideCategory;
use taktwerk\yiiboilerplate\modules\guide\models\search\base\Guide as GuideSearchModel;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
* Guide represents the model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\Guide`.
*/
class Guide extends GuideSearchModel{

    public $guide_category_id;
    public $total_info;

    /**
     * List of additional rules to be applied to model, uncomment to use them
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['guide_category_id'], 'safe'],
            [['group'], 'safe'],
            [['total_info'], 'safe'],
        ]);
    }

    public function search($params)
    {
        $dataProvider = parent::search($params);

        $query = $dataProvider->query;
        if(is_array($this->guide_category_id) || is_array($this->group)) {
            $query->joinWith('categories')->groupBy(GuideModel::tableName() . '.id');
        }

        if(is_array($this->guide_category_id)){

            $dataProvider->sort->attributes['guide_category_id'] = [
                'asc' => [GuideCategory::tableName().'.name' => SORT_ASC],
                'desc' => [GuideCategory::tableName().'.name' => SORT_DESC],
            ];

            $query->andFilterWhere([GuideCategory::tableName().'.id' => $this->guide_category_id]);
        }

        if(is_array($this->group)){

            $all_cat_ids = ArrayHelper::getColumn(GuideCategoryGroup::find()
                ->where(['in','group_id',$this->group])->all(),'guide_category_id');
            if(count($all_cat_ids)>=1){
                $query->andFilterWhere(['in', GuideCategory::tableName().'.id', $all_cat_ids]);
                if(in_array("none", $this->group, true)){
                    $query->orFilterWhere(['is', GuideCategory::tableName().'.id', new Expression('null')]);
                }
            }elseif(count($all_cat_ids)<1 || in_array("none", $this->group, true)){
                $query->andFilterWhere(['is', GuideCategory::tableName().'.id', new Expression('null')]);
            }
        }

        if($this->total_info){
            $new_models = [];
            $models = $dataProvider->models;
            foreach ($models as $model) {
              if(stripos($model->total_info, (string)$this->total_info)!==false){
                  $new_models[] = $model;
              }
            }
            $dataProvider->setModels($new_models);
        }

        return $dataProvider;
    }
    public function attributes()
    {
        $attributes = parent::attributes();
        $attributes[] = 'guide_category_id';
        $attributes[] = 'total_info';
        return $attributes;
    }

    public function attributeLabels()
    {
        $attributeLabels = parent::attributeLabels();
        $attributeLabels['guide_category_id'] = Yii::t('twbp', 'Tags');
        $attributeLabels['group'] = Yii::t('twbp', 'Group');
        $attributeLabels['total_info'] = Yii::t('twbp', 'Step Files Size');

        return $attributeLabels;
    }
}