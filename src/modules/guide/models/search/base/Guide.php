<?php
//Generation Date: 28-Mar-2021 04:27:19pm
namespace taktwerk\yiiboilerplate\modules\guide\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\guide\models\Guide as GuideModel;

/**
 * Guide represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\Guide`.
 */
class Guide extends GuideModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'client_id',
                    'short_name',
                    'title',
                    'description',
                    'preview_file',
                    'preview_file_filemeta',
                    'is_collection',
                    'revision_term',
                    'revision_counter',
                    'duration',
                    'template_id',
                    'protocol_template_id',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'local_created_at',
                    'local_updated_at',
                    'local_deleted_at',
                    'all_step_files_size',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$showDeleted = (property_exists(\Yii::$app->controller, 'model')
            &&
            (is_subclass_of(GuideModel::class, \Yii::$app->controller->model) 
            	||
            	is_subclass_of(\Yii::$app->controller->model, GuideModel::class))
            &&
              (Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model)
                ||
                Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model)))
            );
        $query = GuideModel::findWithOutSystemEntry((Yii::$app->get('userUi')->get('show_deleted', GuideModel::class) === '1'|| $showDeleted)?false:true);

        $this->parseSearchParams(GuideModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(GuideModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(GuideModel::class),
                'params' => [
                    'page' => $this->parsePageParams(GuideModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('client_id', $query);
        $this->applyHashOperator('is_collection', $query);
        $this->applyHashOperator('revision_counter', $query);
        $this->applyHashOperator('duration', $query);
        $this->applyHashOperator('template_id', $query);
        $this->applyHashOperator('protocol_template_id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('short_name', $query);
        $this->applyLikeOperator('title', $query);
        $this->applyLikeOperator('description', $query);
        $this->applyLikeOperator('preview_file', $query);
        $this->applyLikeOperator('preview_file_filemeta', $query);
        $this->applyLikeOperator('revision_term', $query);
        $this->applyLikeOperator('all_step_files_size', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        $this->applyDateOperator('local_created_at', $query, true);
        $this->applyDateOperator('local_updated_at', $query, true);
        $this->applyDateOperator('local_deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(GuideModel::class);
    }
}
