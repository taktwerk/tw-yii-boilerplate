<?php
//Generation Date: 27-May-2021 08:48:22am
namespace taktwerk\yiiboilerplate\modules\guide\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\guide\models\ProductionLineHistory as ProductionLineHistoryModel;

/**
 * ProductionLineHistory represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\ProductionLineHistory`.
 */
class ProductionLineHistory extends ProductionLineHistoryModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'production_line_id',
                    'guide_id',
                    'reference',
                    'reference_qrscan',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$showDeleted = (property_exists(\Yii::$app->controller, 'model')
            &&
            (is_subclass_of(ProductionLineHistoryModel::class, \Yii::$app->controller->model) 
            	||
            	is_subclass_of(\Yii::$app->controller->model, ProductionLineHistoryModel::class))
            &&
              (Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model)
                ||
                Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model)))
            );
        $query = ProductionLineHistoryModel::find((Yii::$app->get('userUi')->get('show_deleted', ProductionLineHistoryModel::class) === '1'|| $showDeleted)?false:true);

        $this->parseSearchParams(ProductionLineHistoryModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(ProductionLineHistoryModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(ProductionLineHistoryModel::class),
                'params' => [
                    'page' => $this->parsePageParams(ProductionLineHistoryModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('production_line_id', $query);
        $this->applyHashOperator('guide_id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyLikeOperator('reference', $query);
        $this->applyLikeOperator('reference_qrscan', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(ProductionLineHistoryModel::class);
    }
}
