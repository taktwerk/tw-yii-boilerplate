<?php
//Generation Date: 27-May-2021 08:48:18am
namespace taktwerk\yiiboilerplate\modules\guide\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\guide\models\GuideChild as GuideChildModel;

/**
 * GuideChild represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\GuideChild`.
 */
class GuideChild extends GuideChildModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'parent_guide_id',
                    'guide_id',
                    'order_number',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'local_created_at',
                    'local_updated_at',
                    'local_deleted_at',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$showDeleted = (property_exists(\Yii::$app->controller, 'model')
            &&
            (is_subclass_of(GuideChildModel::class, \Yii::$app->controller->model) 
            	||
            	is_subclass_of(\Yii::$app->controller->model, GuideChildModel::class))
            &&
              (Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model)
                ||
                Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model)))
            );
        $query = GuideChildModel::find((Yii::$app->get('userUi')->get('show_deleted', GuideChildModel::class) === '1'|| $showDeleted)?false:true);

        $this->parseSearchParams(GuideChildModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>['order_number'=>SORT_ASC],
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(GuideChildModel::class),
                'params' => [
                    'page' => $this->parsePageParams(GuideChildModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('parent_guide_id', $query);
        $this->applyHashOperator('guide_id', $query);
        $this->applyHashOperator('order_number', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        $this->applyDateOperator('local_created_at', $query, true);
        $this->applyDateOperator('local_updated_at', $query, true);
        $this->applyDateOperator('local_deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(GuideChildModel::class);
    }
}
