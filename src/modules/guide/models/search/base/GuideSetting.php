<?php
//Generation Date: 27-May-2021 08:48:19am
namespace taktwerk\yiiboilerplate\modules\guide\models\search\base;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use taktwerk\yiiboilerplate\traits\SearchTrait;
use taktwerk\yiiboilerplate\modules\guide\models\GuideSetting as GuideSettingModel;

/**
 * GuideSetting represents the base model behind the search form about `taktwerk\yiiboilerplate\modules\guide\models\GuideSetting`.
 */
class GuideSetting extends GuideSettingModel{

    use SearchTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'pdf_template',
                    'client_id',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted_by',
                    'deleted_at',
                    'apk_file',
                    'guide_qrscan',
                    'apk_file_filemeta',
                    'is_historicised',
                    'is_deleted'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
    	$showDeleted = (property_exists(\Yii::$app->controller, 'model')
            &&
            (is_subclass_of(GuideSettingModel::class, \Yii::$app->controller->model) 
            	||
            	is_subclass_of(\Yii::$app->controller->model, GuideSettingModel::class))
            &&
              (Yii::$app->get('userUi')->get('show_deleted', \Yii::$app->controller->model)
                ||
                Yii::$app->get('userUi')->get('show_deleted',get_parent_class(\Yii::$app->controller->model)))
            );
        $query = GuideSettingModel::findWithOutSystemEntry((Yii::$app->get('userUi')->get('show_deleted', GuideSettingModel::class) === '1'|| $showDeleted)?false:true);

        $this->parseSearchParams(GuideSettingModel::class, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>$this->parseSortParams(GuideSettingModel::class),
            ],
            'pagination' => [
                'pageSize' => $this->parsePageSize(GuideSettingModel::class),
                'params' => [
                    'page' => $this->parsePageParams(GuideSettingModel::class),
                ]
            ],
        ]);

        $this->load($params);

        $query->deleted($this->is_deleted, self::tableName());

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->applyHashOperator('id', $query);
        $this->applyHashOperator('pdf_template', $query);
        $this->applyHashOperator('client_id', $query);
        $this->applyHashOperator('created_by', $query);
        $this->applyHashOperator('updated_by', $query);
        $this->applyHashOperator('deleted_by', $query);
        $this->applyHashOperator('is_historicised', $query);
        $this->applyLikeOperator('apk_file', $query);
        $this->applyLikeOperator('guide_qrscan', $query);
        $this->applyLikeOperator('apk_file_filemeta', $query);
        $this->applyDateOperator('created_at', $query, true);
        $this->applyDateOperator('updated_at', $query, true);
        $this->applyDateOperator('deleted_at', $query, true);
        return $dataProvider;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->parsePageSize(GuideSettingModel::class);
    }
}
