<?php

namespace taktwerk\yiiboilerplate\modules\guide\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\guide\models\base\GuideTag as BaseGuideTag;

/**
 * This is the model class for table "guide_tag".
 */
class GuideTag extends BaseGuideTag
{
    public static function find($removedDeleted = true){
        $query = parent::find($removedDeleted);
        if(array_key_exists('REQUEST_METHOD', $_SERVER) && !Yii::$app->user->can('Authority')){
            $query->joinWith('guide');
        }
        return $query;
    }
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

    /**
     * Get a list of distinct values
     * @param string $field
     * @return array
     */
    public static function getDistinctTagValues($field = 'name')
    {
        $tags = [];
        foreach (self::find()->select($field)->distinct()->all() as $tag) {
            /**@var $tag GuideTag*/
            $tags[$tag->name] = $tag->name;
        }
        return $tags;
    }


    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getGuides()
    {
        // Build a list of similar tags
        $similarTagIds = [];
        foreach (self::find()->where(['name' => $this->name])->all() as $tag) {
            /**@var $tag GuideTag*/
            $similarTagIds[] = $tag->guide_id;
        }
        return Guide::find()->where(['in', 'id', $similarTagIds])->all();
    }
}
