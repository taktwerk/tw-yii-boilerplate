<?php

namespace taktwerk\yiiboilerplate\modules\guide\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\guide\models\base\GuideSetting as BaseGuideSetting;

/**
 * This is the model class for table "guide_setting".
 */
class GuideSetting extends BaseGuideSetting
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }
    public static function isHistoricised(){
        $isHistoricised = true;
        $guideHistoryEnabled = getenv('DEFAULT_GUIDE_RESUME_ENABLED');
        if($guideHistoryEnabled==='1'){
            $isHistoricised = true;
        }
        if($guideHistoryEnabled==='0'){
            $isHistoricised = false;
        }
        $user = \taktwerk\yiiboilerplate\modules\user\models\User::findOne(['id'=>\Yii::$app->user->identity->id]);

        if(!\Yii::$app->user->can('Authority')){
            $model = self::findOne(['client_id'=>$user->client_id]);
            if($model){
                if($model->is_historicised==1){
                    $isHistoricised = true;
                }else if($model->is_historicised===0){//$isHistoricised set to false only if is_historicised is strictly 0(not null)
                    $isHistoricised = false;
                }
            }
            if($twData = $user->userTwData){
                if($twData->is_historicised==1){
                    $isHistoricised = true;
                }else if($twData->is_historicised===0){//$isHistoricised set to false only if is_historicised is strictly 0(not null)
                    $isHistoricised = false;
                }
            }
        }
        return $isHistoricised;
    }
}
