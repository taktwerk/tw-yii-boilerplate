<?php

namespace taktwerk\yiiboilerplate\modules\guide\models;

use taktwerk\yiiboilerplate\modules\guide\commands\GeneratePdfJob;
use taktwerk\yiiboilerplate\modules\guide\commands\GenerateZipJob;
use taktwerk\yiiboilerplate\modules\guide\models\base\Guide as BaseGuide;
use Illuminate\Support\Arr;
use taktwerk\yiiboilerplate\components\Helper;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use taktwerk\yiiboilerplate\modules\search\models\interfaces\SearchableInterface;
use taktwerk\yiiboilerplate\modules\sync\traits\UserAppDataVersionTrait;
use taktwerk\yiiboilerplate\modules\user\models\Group;
use taktwerk\yiiboilerplate\widget\Select2;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use taktwerk\yiiboilerplate\helpers\CrudHelper;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use yii\helpers\Inflector;
use yii\helpers\Json;
use taktwerk\yiiboilerplate\behaviors\ZipBehavior;
use taktwerk\yiiboilerplate\modules\user\models\UserGroup;

/**
 * This is the model class for table "guide".
 * @property string $revision_term
 * @property integer $revision_counter
 * @property integer $duration
 *
 */
class Guide extends BaseGuide implements SearchableInterface
{
    use UserAppDataVersionTrait;

    /**
     * @var
     */
    public $group;
    public $childrenIds;
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['zippable'] = [
            'class'=>ZipBehavior::class,
            'zipJobClass'=>GenerateZipJob::class,
            'excludedRelationsListForZip'=>['client']
        ];
        return $behaviors;
    }
    /**
     * List of additional rules to be applied to model, uncomment to use them
     * @return array
     */
    public function rules()
    {
        $internalRules = array_merge(parent::rules(), [
            [
                'short_name',
                'match',
                'pattern' => '/^[a-z0-9-_.]+$/i',
                'message' => Yii::t("twbp",'Invalid characters in short name.')
            ],
            [
                ['short_name'],
                'validateShortName',
                'when' => function ($model) {
                    return strlen($model->short_name) > 0;
                },
            ]
        ]);
        
        return  array_merge($internalRules, $this->buildRequiredRulesFromFormDependentShowFields());
        
    }
    /**
     * @desc Provides the guide_data models according to diffrent type of models 
     * 
     * @return \yii\db\ActiveQuery[] | \yii\db\ActiveRecord[] 
    */
    public function getGuideDatasModels($returnQuery=false, $filterClient = true){
        $recordList = [];
        $classList = [];
        foreach($this->guideDatas as $gData){
            $c = $gData->getTypeRefClass($filterClient);
            if($c && class_exists($c)){
                
                if(!array_key_exists($c,$classList)){
                    $classList[$c] = [];
                }
                if($gData->type_ref){
                    $classList[$c][] = $gData->type_ref;
                }
            }
        }

        foreach($classList as $k=>$ids){
            if($returnQuery){
                $primaryKey = $k::primaryKey();
                $recordList[$k] = $k::find()->where([$primaryKey[0]=>$ids]);
            }else{
                $recordList[$k] = $k::findAll($ids);
            }
        }
        return $recordList;
    }
    
    /* public function getTypeRefModel(){
        if($this->clientModelType){
            $modelName = Inflector::camelize($this->clientModelType->type);
            $tableName = $this->clientModelType->type;
            $modelName = ClassDispenser::getMappedClass(CrudHelper::class)::findModelClass($modelName,$tableName);
            if($modelName){
                return $modelName::findOne($this->type_ref);
            }
        }
        return null;
    } */
    
    public function deletableRelationNames(){
        return [
            'guideSteps',
        ];
    }
    /**
     * @return array
     */
    public function formDependentShowFields()
    {
        $fName = $this->formName();
        // dependence project_type
        $dependentFields = [];
        /* $dependentFields = [
            'childrenIds' => $this->generateDependentFieldRule(
                'is_collection',
                Html::getInputId($this,'is_collection'),
                'childrenIds',
                'childrenIds',
                '1',true),
        ]; */
        $dependentFields['childrenIds'] = $this->generateDependentFieldRules('childrenIds', [
            "field" => $fName . "[childrenIds]",
            "container" => ".form-group-block",
            "rules" => [
                [
                    "name" => $fName . "[is_collection]",
                    "operator" => "is",
                    "value" => "1"
                ]
            ]
        ],true);
        return $dependentFields;
    }
    
    public function getModelUrlWithName(){
        if(!$this->isNewRecord){
            $data = [];
            $data['url'] = Url::toRoute(['/guide/guide/view','id'=>$this->id]);
            $data['name'] =$this->short_name .' - '. $this->toString();
            return $data;
        }
        return null;
    }
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return array_merge(parent::attributeHints(), [
            'short_name' => Yii::t('twbp', 'Leave empty for an auto-generated value.'),
            'tags' => Yii::t('twbp', 'Choose categories to group guides.'),
            'guide_category_id' => Yii::t('twbp', 'Choose categories to group guides.'),
            'is_collection'=>Yii::t('twbp', 'Pack multiple guides to a collection')
        ]);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'tags' => Yii::t('twbp', 'Tags'),
            'guide_category_id' => Yii::t('twbp', 'Tags'),
            'children'=>Yii::t('twbp', 'Collection Guides'),
            'childrenIds'=>Yii::t('twbp', 'Collection Guides'),
            'guideChildren0'=>Yii::t('twbp', 'Collection Guides'),
            'is_collection'=>Yii::t('twbp', 'Guide is a collection'),
        ]);
    }

    /**
     * Get the tags
     */
    public function getTags()
    {
        $tags = [];
        foreach ($this->guideCategoryBindings as $tag) {
            $tags[] = $tag->guideCategory->name;
        }
        return $tags;
    }
    public function getGuideViewHistoryOfUser($userId=null){
        $userId = $userId?$userId:\Yii::$app->user->id;
        if($userId){
            return GuideViewHistory::findOne([GuideViewHistory::tableFullName().'.guide_id'=>$this->id,
                GuideViewHistory::tableFullName().'.user_id'=>$userId]);
        }
        return null;
    }
    public function saveGuideViewHistory($step=null, $parent_guide_id=null){
        $guide_id = $this->id;
        $childGuideId = null;
        if($parent_guide_id)
        {
            $childGuideId = $guide_id;
            $guide_id = $parent_guide_id;
        }
        $guide = Guide::findOne([self::tableName().'.id' => $guide_id]);
        if($guide){
            $userId = \Yii::$app->user->id;
            $gViewHistory = $guide->getGuideViewHistoryOfUser($userId);
            if($gViewHistory==null){
                $gViewHistory = new GuideViewHistory();
            }
            $gViewHistory->guide_id = $guide->id;
            $gViewHistory->user_id = $userId;
            $guideWithStep = $guide;
            if($childGuideId){
                $childGuide = $guide->getGuideChildren0()->where(['guide_id' => $childGuideId])->one();
                if($childGuide && $childGuide->guide){
                    $guideWithStep = $childGuide->guide;
                }
            }
            $stepModel = !empty($step) ?
            $guideWithStep->getGuideSteps()->where(['order_number' => $step])->one() :
            $guideWithStep->getGuideSteps()->orderBy(['order_number' => SORT_ASC])->one();
            if($stepModel){
                $metaData = ['step_order_number' => $stepModel->order_number];
                if($childGuideId && isset($childGuide)){
                    if($childGuide){
                        $metaData['child_guide_id'] = $childGuideId;
                    }
                }
                $gViewHistory->data = Json::encode($metaData);
            }
            $gViewHistory->client_id = $this->client_id;

            $gViewHistory->save();
            return $gViewHistory;
        }
        return false;
    }
    /**
     * @param null $q
     * @return array
     */
    public static function childrenList($q = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, title AS text')
            ->from(self::tableName())
            ->andWhere([self::tableName() . '.deleted_at' => null])
            ->andWhere(['like', 'title', $q])
            ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }
    /**
     * Get the Children Guides Id
     */
    public function getChildren()
    {
        $tags = [];
        foreach ($this->guideChildren0 as $guide) {
            $tags[] = $guide->guide_id;
        }
        return $tags;
    }

    /**
     * @param string $attribute
     * @param bool $thumb
     * @return mixed|string|null
     * @throws \Exception
     */
    public function getPicture($attribute='preview_file', $thumb = false)
    {
        if($attribute){
            $width = $thumb ? 100 : 1280;
            $height = $thumb ? 100 : 1024;
            return !empty($this->$attribute) ? $this->getFileUrl($attribute, ['width' => $width, 'height' => $height], true) : "/img/placeholder.jpg";
        }
        return null;
    }

    /**
     * @param array $options
     * @return string
     * @throws \Exception
     */
    protected function showPreviewForPreviewFile(array $options = [])
    {
        $maxWidth = ArrayHelper::getValue($options, 'max-width', '200px');
        $maxHeight = ArrayHelper::getValue($options, 'max-height', '200px');

        return Html::img(
            $this->getPicture('preview_file'),
            [
                'class' => '',
                'style' => 'max-width:' . $maxWidth . ';max-height:' . $maxHeight . ''
            ]
        );
    }


    /**
     * @param null $q
     * @return array
     * @throws \yii\db\Exception
     */
    public static function guideCategoryList($q = null)
    {
        return \taktwerk\yiiboilerplate\modules\guide\models\GuideCategory::filter($q);
    }

    /**
     * After Save event
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveCategoryBindings();
        $this->saveChildren();
        $this->changeUserAppDataVersion($insert, $changedAttributes);
    }

    /**
     * Manage category bindings after a guide has been saved
     */
    protected function saveCategoryBindings()
    {
        // Manage tags
        if (Yii::$app instanceof \yii\web\Application) {

            $request = Yii::$app->getRequest()->post('Guide');

            if ($request) {
                $existingTags = [];
                foreach ($this->getGuideCategoryBindings()->joinWith('guideCategory.guideCategoryGroups')->all() as $tag) {
                    $existingTags[$tag->guideCategory->name] = $tag;
                }
                $submittedTags = ! empty($request['tags']) ? $request['tags'] : [];

                // Loop submitted tags to see if they are new or existing (and don't require deleting)
                foreach ($submittedTags as $tag) {
                    if (! empty($existingTags[$tag])) {
                        unset($existingTags[$tag]);
                    } else {
                        $category = GuideCategory::find()->where([
                            'name' => $tag
                        ])->joinWith('guideCategoryGroups')
                        ->one();
                        if (empty($category)) {
                            $category = new GuideCategory();
                            $category->client_id = $this->client_id;
                            $category->name = $tag;
                            $category->save();
                        }

                        // Already have this binding?
                        $binding = GuideCategoryBinding::find()->where([
                            'guide_id' => $this->id,
                            'guide_category_id' => $category->id
                        ])->one();
                        if (empty($binding)) {
                            $tagModel = new GuideCategoryBinding();
                            $tagModel->guide_id = $this->id;
                            $tagModel->guide_category_id = $category->id;
                            $tagModel->save();
                        }
                    }
                }

                // Remove old tags
                foreach ($existingTags as $tagName => $tag) {
                    // We don't want to keep soft-deletes of tags so force delete.
                    $tag->delete();
                }
            }
        }
    }
    

    /**
     * Manage Guide's children Guides after a guide has been saved
     */
    protected function saveChildren()
    {
        if(!$this->is_collection){
            // Delete guide children if this guide is changed to a non-collection guide
             foreach($this->guideChildren0 as $child){
                $child->forceDelete();
             }

            return;
        }
        /* Delete guide steps if this guide is changed to a collection
        foreach($this->guideSteps as $step){
            $step->delete();
        }
        */
        // Manage tags
        if (Yii::$app instanceof \yii\web\Application) {
            
            $request = Yii::$app->getRequest()->post('Guide');
            if ($request && key_exists('childrenIds',$request)) {
                $existingGuides = [];
                foreach ($this->guideChildren0 as $guide) {
                    $existingGuides[$guide->guide_id] = $guide;
                }
                $submittedGuides = ! empty($request['childrenIds']) ? $request['childrenIds'] : [];
                // Loop submitted Groups to see if they are new or existing (and don't require deleting)
                foreach ($submittedGuides as $guideId) {
                    if (! empty($existingGuides[$guideId])) {
                        unset($existingGuides[$guideId]);
                    } else {
                        $guide = self::find()->where([
                            self::tableName(). '.id' => $guideId
                        ])->one();
                        if($guide==null){
                            continue;
                        }
                        // Already have this binding?
                        $child = GuideChild::find()->where([
                            'guide_id' => $guideId,
                            'parent_guide_id'=>$this->id
                        ])->one();
                        if (empty($child)) {
                            $tagModel = new GuideChild();
                            $tagModel->guide_id = $guideId;
                            $tagModel->parent_guide_id = $this->id;
                            $tagModel->save();
                        }
                    }
                }

                // Remove old Groups
                foreach ($existingGuides as $childGuide) {
                    // We don't want to keep soft-deletes of Collection Guides so force delete.
                    $childGuide->forceDelete();
                }
            }
        }
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        $query = $this->hasMany(GuideCategory::class, ['id' => 'guide_category_id'])
            ->via('guideCategoryBindings');
        $query->modelClass = GuideCategory::class;
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::class, ['client_id' => 'client_id']);
    }

    /**
     * Get the previous step
     * @param $currentStep
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getPreviousStep(GuideStep $currentStep = null)
    {
        return $this->getGuideSteps()->where(['<', 'order_number', $currentStep->order_number])->orderBy(['order_number' => SORT_DESC])->one();
    }

    /**
     * Get the previous step
     * @param $currentStep
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getNextStep(GuideStep $currentStep = null)
    {
        return $this->getGuideSteps()->where(['>', 'order_number', $currentStep->order_number])->orderBy(['order_number' => SORT_ASC])->one();
    }


    /**
     * Return details for dropdown field
     * @return string
     */
    public function getEntryDetails()
    {
        return trim($this->short_name . ' ' . $this->title);
    }


    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        // If we are dealing with a new record
        if (empty($this->short_name)) {
            if ($insert) {
                $gTbl = Guide::tableName();
                $nextId = \taktwerk\yiiboilerplate\modules\guide\models\Guide::find()
                ->where([$gTbl.'.client_id' => $this->client_id])
                ->max($gTbl.'.short_name');
                if(is_numeric($nextId)){
                    $nextId += 1;
                }else{
                    $nextId = \taktwerk\yiiboilerplate\modules\guide\models\Guide::find()
                    ->where([$gTbl.'.client_id' => $this->client_id])
                    ->max($gTbl.'.id') + 1;
                }
            } else {
                $nextId = $this->id;
            }
            $this->short_name = str_pad($nextId, 4, "0", STR_PAD_LEFT);
        }
        if($insert==false){
            if($this->getGuideSteps()->count()>0 && $this->is_collection==1){
                $this->addError('is_collection',\Yii::t('twbp','Please remove all the guide steps under this guide first to make it a collection'));
                return false;
            }
            if($this->getGuideChildren0()->count()>0 && $this->is_collection==0){
                if (Yii::$app instanceof \yii\web\Application) {
                $request = \Yii::$app->request->post('Guide');
                $submittedGuides = ! empty($request['childrenIds']) ? $request['childrenIds'] : [];
                if(count($submittedGuides)>0){
                    $this->addError('is_collection',\Yii::t('twbp','Please remove all the guide children binding of this collection first to make it a guide'));
                    return false;
                }
                }
            }
        }
        if (!$insert) {
            $this->incrementRevisionCounter();
        }
        return parent::beforeSave($insert);
    }

    /**
     *
     * @return string
     */
    public function searchableKey()
    {
        return 'Guide';
    }

    /**
     *
     * @return array
     */
    public function searchableFields()
    {
        return [
            'short_name',
            'title',
            'description'
        ];
    }
    
    public function searchableCustom($term=''){
        
        $query = static::find()->joinWith('guideCategoryBindings',true,'INNER JOIN')
        ->groupBy('guide.id');
        
      //  $query->joinWith('guideChildren0.guide as g');
        
        
        
        $fields = $this->searchableFields();
        $filter1 =[];
        $filter1[] = 'OR';
        foreach($fields as $fieldName){
            $filter1[]= [
                'LIKE',
                static::tableFullName() . '.' . $fieldName,
                str_replace('*', $term, '%*%'),
                false
            ];
        }
        
       /*  $filter =[];
        $filter[] = 'OR';
        foreach($fields as $fieldName){
            $filter[]= [
                'LIKE',
                 'g.' . $fieldName,
                str_replace('*', $term, '%*%'),
                false
            ];
        } */
        $query->andFilterWhere($filter1);
        //prd($query->createCommand()->rawSql);
        return $query;
    }

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        /* $parentList = [];
        foreach($this->guideChildren as $parent){
            $data=[];
            $pg = $parent->parentGuide;
            if($pg && $pg->getGuideCategoryBindings()->count()>0){
                $data['title'] = $pg->title;
                $data['short_name'] = $pg->short_name;
                $data['id'] = $pg->id;
                $parentList[]=$data;
            }
        } */
        return [
            'id' => $this->id,
            'short_name' => $this->short_name,
            'title' => $this->title,
            'preview' => $this->showFilePreview('preview_file', [], true),
            'description' => substr($this->description, 0, 250),
            'url' => \yii\helpers\Url::to(['/guide/explore/guide', 'id' => $this->id]),
            /* 'parent_guides'=>$parentList */
        ];
    }

    /**
     * @return mixed|string
     * @throws \ImagickException
     */
    /* public function pdfImageUrl()
    {
        return $this->getFileUrl('pdf_file');

        $pdf = $this->getUploadPath() . $this->pdf_file;
        $pngFileName = Str::replaceLast('.pdf', '.png', strtolower($this->pdf_file));
        $png = $this->getUploadPath() . $pngFileName;

        if (!Yii::$app->fs->has($png)) {
            $tmpFile = Yii::getAlias('@runtime/convert/' . $this->pdf_file);
            $convertedFile = Yii::getAlias('@runtime/convert/' . $pngFileName);
            $contents = Yii::$app->fs->read($pdf);

            @mkdir(Yii::getAlias('@runtime/convert/'));
            file_put_contents($tmpFile, $contents);

            $imagick = new \Imagick();
            $imagick->setResolution(300, 300);
            $imagick->readImage($tmpFile. "[0]");
            $imagick->setImageFormat( "png" );
            $imagick->setCompressionQuality(90);
            $imagick->writeImage($convertedFile);

            $stream = fopen($convertedFile, 'r+');
            Yii::$app->fs->writeStream($png, $stream);
        }

        return Helper::getFile("elfinder", "fs", $png);
    } */

    /**
     * Before (soft) delete, delete assets and pivots
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        foreach ($this->guideSteps as $c) {
            $c->delete();
        }
        foreach ($this->guideAssetPivots as $c) {
            $c->delete();
        }
        foreach ($this->guideCategoryBindings as $c) {
            $c->delete();
        }
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

    /**
     * Returns a revision string based on guide modification date
     * @return string
     */
    public function getRevision(){
        $file_name='';
        if($this->revision_term){
            $file_name .= $this->revision_term.'_';
        }
        if($this->revision_counter){
            $file_name .= $this->revision_counter.'_';
        }
        return $file_name.date('Y-m-d', strtotime($this->updated_at?:$this->created_at));
    }

    /**
     * Returns name of guide pdf
     * @return string
     */
    public function getPdfName(){
        return Helper::sanitizeString($this->short_name."_".$this->getRevision().".pdf");
    }

    /**
     * Returns name of guide pdf
     * @return string
     */
    public function getZipName(){
        return Helper::sanitizeString($this->short_name."_".$this->getRevision().".zip");
    }

    /**
     * @param $guide Guide
     * @return QueueJob|null
     */
    public function findPdfGenerateQueueJobModel($guide)
    {
        if (($model = QueueJob::findOne([
                'command' => GeneratePdfJob::class,
                'parameters' => json_encode($this->generateParametersForJobQueue($guide)),
                'status' => QueueJob::STATUS_QUEUED
            ])) !== null) {
            return $model;
        }
        return null;
    }

    /**
     * @param $guide Guide
     * @return mixed
     */
    public function createPdfGenerateQueueJobModel($guide)
    {
        $job = new QueueJob();
        return $job->queue("Generate PDF for guide {$guide}", GeneratePdfJob::class, $this->generateParametersForJobQueue($guide), true);
    }

    /**
     * @param $guide_id
     * @return array
     */
    protected function generateParametersForJobQueue($guide_id)
    {
        return ['guide_id' => $guide_id];
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public function  getClientLogoFile(){
        if($this->client){
            $client_logo = Helper::getLocalTempFile($this->client->getUploadPath() . $this->client->logo_file);
            Yii::$app->params['clean_up'][] = $client_logo;
            return Html::img($client_logo,[
                'height'=>'50px',
                'style'=>'margin-bottom:5px'
            ]);
        }
    }

    /**
     * Guide preview for PDF
     * @return string
     * @throws \yii\base\Exception
     */
    public function  getGuidePreview($showCompressImg=true){
        
        $preview_file = $this->getImageTmpPath('preview_file',['width' => getenv('GUIDER_PDF_IMAGE_WIDTH'), 'ratio' => '1_1'],$showCompressImg);
        if($preview_file){
            return  Html::img($preview_file, ['class' => 'guide-img']);
        }
        return null;
    }

    /**
     * Increment revision counter
     * @return int
     */
    public function incrementRevisionCounter()
    {
        return $this->revision_counter++;
    }
    public static function childrenDuration(self $guide,$skipGuideIds=[]){
        $totalDuration = 0;
        /* echo '<pre>'; */
        foreach($guide->guideChildren0 as $child){
            /* echo $child->guide_id.' - ';
            print_R($skipGuideIds); */
            if(in_array($child->guide_id,$skipGuideIds)){
                continue;
            }
            if($child->guide){
                if($child->guide->duration){
                    $totalDuration +=$child->guide->duration;
                }
                elseif($child->guide->is_collection==1){
                    $totalDuration += self::childrenDuration($child->guide,array_merge($skipGuideIds,[$guide->id]));
                }
            }
        }
        return $totalDuration;
    }
    /**
     * @return bool|string
     */
    public function durationTime($humanReadableFormat=true)
    {
        if($this->duration){
            if($humanReadableFormat){
                return Helper::convertMinutesToHumanReadableTime($this->duration);
            }
            return $this->duration;
        }
        if($this->is_collection==1){
            $totalDuration = self::childrenDuration($this);
           // prd($totalDuration);
            if($totalDuration>0){
                if($humanReadableFormat){
                return Helper::convertMinutesToHumanReadableTime($totalDuration);
                }
                return $totalDuration;
            }
        }
        return false;
    }
    public static function childrenStepCount(self $guide,$skipGuideIds=[]){
        $count = 0;
        $count += $guide->getGuideSteps()->count();
        foreach($guide->guideChildren0 as $child){
            
            if(in_array($child->guide_id,$skipGuideIds))
            {
                continue;
            }
            if($child->guide){
                if($child->guide->is_collection==1){
                    $count += self::childrenStepCount($child->guide,array_merge($skipGuideIds,[$guide->id]));
                }else{
                    $count += $child->guide->getGuideSteps()->count();
                }
            }
        }
        return $count;
    }
    public function stepCount(){
        $count =0;
        if($this->is_collection==1){
            $count = self::childrenStepCount($this);
        }else{
            $count = $this->getGuideSteps()->count();
        }
        return $count;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuideStepsOrderBySteps()
    {
        return $this->getGuideSteps()->orderBy(['order_number' => SORT_ASC]);
    }

    /**
     * To get all the non relational additional files
     * 'cmd' key includes all the console to be exceutedbefore running the zip command to generate additional files if any
     * @return array
     */
    public function additionalFilesForZip(){

        $additional_files = [];
        $pdf_file = '/pdf/'.$this->client->id.'/'.$this->getPdfName();
        if(\Yii::$app->fs->has($pdf_file)) {
            $additional_files[] = $pdf_file;
        }else{
            $guide_id = $this->id;
            $cmd = "php yii guide/guide-pdf/generate-guide-pdf SPECIFIC $guide_id --job=0";
            $additional_files['cmd'][] = $cmd;
            $additional_files[] = $pdf_file;
        }
        return $additional_files;
    }

    public function validateShortName(){
        $exists = self::find()
            ->where(["guide.short_name" => trim($this->short_name)])
            ->andWhere(['guide.client_id' => $this->client_id]);

        if($this->id){
           $exists->andWhere(['!=', 'guide.id', $this->id]);
        }

        $exists = $exists->exists();

        if ($exists) {
            $this->addError("short_name", "Short Name {$this->short_name} has already been taken.");
        }
    }

    /**
     * @param $model
     * @param $pjaxContainer
     * @param bool $is_search_form
     * @return array|void
     * @throws \ReflectionException
     */
    public function getCustomFilters($model, $pjaxContainer=null, $isSearchForm=false){

        $reflection = new \ReflectionClass($model);
        $shortModelName = $reflection->getShortName();
        $suffix = $isSearchForm?"-form":"";
        $inputName = "{$shortModelName}[group]";

        $groupName = Html::hiddenInput($inputName);
        $isGuideAdmin = \Yii::$app->user->can('GuiderAdmin');
        $groupName .= Select2::widget([
            'pjaxContainerId' => $pjaxContainer,
            'data' => ArrayHelper::merge(
                ArrayHelper::map($isGuideAdmin?\taktwerk\yiiboilerplate\modules\user\models\base\Group::find()->all():Group::find()->all(),'id','toString'),
                [
                    'none'=>Yii::t('twbp','None')
                ]
            ),
            'value'=> $model->group,
            'name' => $inputName,
            'id'=> "{$shortModelName}-group".$suffix,
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true,
            ],
        ]);

        return [
            [
                'attribute' => 'group',
                'value' => function ($model){
                    $isGuideAdmin = \Yii::$app->user->can('GuiderAdmin');
                    $categories = [];
                    foreach ($model->categories as $category) {
                        $categories[] = $category->id;
                    }
                    if($isGuideAdmin){
                        $allGroupIds = (new \yii\db\Query())
                        ->select(['group_id'])
                        ->from('guide_category_group')
                        ->where(['AND',['guide_category_group.deleted_at'=>null],['in', 'guide_category_id', $categories]])
                        ->column();
                    }else{
                        $allGroupIds = GuideCategoryGroup::find()->select('group_id')
                        ->where(['in','guide_category_id',$categories])->column();
                    }
                    $groupQ = Group::find();
                    if($isGuideAdmin){
                        $groupQ = \taktwerk\yiiboilerplate\modules\user\models\base\Group::find();
                    }
                    $groups = ArrayHelper::getColumn($groupQ
                        ->where(['in','id',$allGroupIds])
                        ->all(),'toString');
                    if($groups){
                        return implode(", ",$groups);
                    }
                },
                'format' => 'raw',
                'label' => Yii::t('twbp','Groups'),
                'filter' => $groupName
            ]
        ];
    }

    /**
     * @return bool
     */
    public function isCollection(){
        return ($this->is_collection==1);
    }

    public function stepFilesInfo(){

        $sizes = GuideStep::getAllFileSizes($this->id);
        Yii::$app->formatter->nullDisplay = '-';
        Yii::$app->formatter->sizeFormatBase = 1000;

        $size = Yii::t('twbp','Total') . ': ' . Yii::$app->formatter->asShortSize(array_sum($sizes),2);
        $size .= '<br>';
        $size .= 'PDF' . ': ' . Yii::$app->formatter->asShortSize($sizes['file'],2);
        $size .= '<br>';
        $size .= Yii::t('twbp','Video') . ': ' . Yii::$app->formatter->asShortSize($sizes['video'],2);
        $size .= '<br>';
        $size .= Yii::t('twbp','Audio') . ': ' . Yii::$app->formatter->asShortSize($sizes['audio'],2);
        $size .= '<br>';
        $size .= Yii::t('twbp','Image') . ': ' . Yii::$app->formatter->asShortSize($sizes['image'],2);
        $size .= '<br>';
        $size .= Yii::t('twbp','3D') . ': ' . Yii::$app->formatter->asShortSize($sizes['3d'],2);

        return $size;
    }

    public function updateSize(){
        $this->all_step_files_size = $this->stepFilesInfo();
        $this->save(false);
    }
    public static function find($removedDeleted = true){
        $q = parent::find($removedDeleted);
        $webUser = \Yii::$app->has('user')?\Yii::$app->user:null;
        if(array_key_exists('REQUEST_METHOD', $_SERVER) && !$webUser->can('Authority')) {
            if($webUser->can('GuiderGroup')){
                //Filter for GuideGroup role to display 
                //1.guides having no Category;
                //     OR
                //2.related Category which has no group binding;
                //     OR
                //3.related Category which has group binding of the logged in user's group 

                $clients = [];
                $clientUsers = \taktwerk\yiiboilerplate\modules\customer\models\ClientUser::findAll(["user_id" => $webUser->id]);
                foreach ($clientUsers as $client) {
                    $clients[] = $client->client_id;
                }
                $gcbTbl = GuideCategoryBinding::tableName();
                $gcTbl = GuideCategory::tableName();
                $gcgTbl = GuideCategoryGroup::tableName();
                $q->leftJoin(['gcb'=>$gcbTbl], '`'.self::tableName().'`.`id` = `gcb`.`guide_id` AND `gcb`.`deleted_at` IS NULL');
                $q->leftJoin(['gc'=>$gcTbl], '`gcb`.`guide_category_id` = `gc`.`id` AND `gc`.`deleted_at` IS NULL');
                $q->leftJoin(['gcg'=>$gcgTbl], '`gc`.`id` = `gcg`.`guide_category_id` AND `gcg`.`deleted_at` IS NULL');
                
                $groupIds = ArrayHelper::map(UserGroup::find()->select('group_id')
                    ->where(['user_id' => $webUser->id])
                    ->asArray()->all(), 'group_id', 'group_id');
                $q->andWhere(['OR',
                                    [
                                        'gc.client_id'=>$clients,
                                        'gcg.group_id'=>$groupIds
                                    ],
                                    [
                                        'gcb.id'=>null
                                    ]
                                    //uncomment to show guides with categories which have no groups, 
                                    /* ,[
                                        'gc.client_id'=>$clients,
                                        'gcg.id'=>null
                                    ] */
                            ]);
               $groupByAvoidActions = [
                   ['guide','view'],
                   ['explore','guide'],
               ];
               $groupBy = true;
               foreach($groupByAvoidActions as $g){
                   if(\Yii::$app->controller->id==$g[0] && \Yii::$app->controller->action->id==$g[1]){
                       $groupBy = false;break;
                   }
               }
               if($groupBy){
                    $q->groupBy('guide.id');
                }
            }
        }
        return $q;
    }
}
