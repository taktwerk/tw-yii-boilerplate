<?php

namespace taktwerk\yiiboilerplate\modules\guide\models;

use taktwerk\yiiboilerplate\modules\customer\models\ClientUser;
use taktwerk\yiiboilerplate\modules\user\models\User;
use Yii;
use \taktwerk\yiiboilerplate\modules\guide\models\base\GuideCategoryBinding as BaseGuideCategoryBinding;
use taktwerk\yiiboilerplate\modules\sync\traits\UserAppDataVersionTrait;

/**
 * This is the model class for table "guide_category_binding".
 */
class GuideCategoryBinding extends BaseGuideCategoryBinding
{
    use UserAppDataVersionTrait;
    public static function find($removedDeleted = true){
        $query = parent::find($removedDeleted);
        if(array_key_exists('REQUEST_METHOD', $_SERVER) && !Yii::$app->user->can('Authority')){
            $query->joinWith('guideCategory');
        }
        return $query;
    }
    public function clientsForChangeAppDataVersion()
    {
        $clientIds = [];
        $clientIds[] = $this->guideCategory->client_id;
        $clientIds[] = $this->guide->client_id;

        return $clientIds;
    }
}
