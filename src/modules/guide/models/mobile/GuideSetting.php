<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\mobile;

use taktwerk\yiiboilerplate\traits\MobileDateTrait;
use taktwerk\yiiboilerplate\modules\guide\traits\MobileFileTrait;
use Yii;

class GuideSetting extends \taktwerk\yiiboilerplate\modules\guide\models\GuideSetting
{
    use MobileDateTrait,MobileFileTrait;

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        if (!\Yii::$app->user) {
            return [];
        }
        $apkFilePath = $this->getFilePathByAttribute('apk_file');

        return [
            'id' => $this->id,
            'pdf_template'=>$this->pdf_template,
            'client_id' => $this->client_id,
            'apk_file'=>$this->apk_file,
            'apk_file_path'=>$apkFilePath,
            'guide_qrscan'=>$this->guide_qrscan,
            //'apk_file_filemeta'=>$this->apk_file_filemeta,
            'is_historicised'=>$this->is_historicised,
            'created_by' => $this->created_by,
            'created_at' => $this->getIntCreatedAt(),
            'updated_by' => $this->updated_by,
            'updated_at' => $this->getIntUpdatedAt(),
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->getIntDeletedAt(),          
        ];
    }
}
