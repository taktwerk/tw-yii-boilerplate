<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\mobile;

use taktwerk\yiiboilerplate\models\User;
use taktwerk\yiiboilerplate\traits\MobileDateTrait;
use taktwerk\yiiboilerplate\modules\guide\traits\MobileFileTrait;
use Yii;

class Guide extends \taktwerk\yiiboilerplate\modules\guide\models\Guide
{
    use MobileDateTrait, MobileFileTrait;

    public function getPreviewFilePath()
    {
        return $this->getFilePathByAttribute('preview_file');
    }

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        if (!\Yii::$app->user) {
            return [];
        }

        $created_by = User::findOne($this->created_by);
        $updated_by = User::findOne($this->updated_by);

        return [
            'id' => $this->id,
            'client_id' => $this->client_id,
            'user_id' => \Yii::$app->user->id,
            'short_name' => $this->short_name,
            'revision' => $this->getRevision(),
            'title' => $this->title,
            'preview_file' => $this->preview_file ?: null,
            'preview_file_path' => $this->previewFilePath,
            'revision_term' => $this->revision_term,
            'revision_counter' => $this->revision_counter,
            'duration' => $this->duration,
            'template_id' => $this->template_id,
            'protocol_template_id' => $this->protocol_template_id,
            'description' => substr($this->description, 0, 250),
            'created_term' => $created_by->toString() . ' (' . \Yii::$app->formatter->asDatetime($this->created_at) .')',
            'updated_term' => $updated_by->toString() . ' (' . \Yii::$app->formatter->asDatetime($this->updated_at) .')',
            'deleted_at' => $this->getIntDeletedAt(),
            'updated_at' => $this->getIntUpdatedAt(),
            'created_at' => $this->getIntCreatedAt(),
            'local_deleted_at' => $this->getIntLocalDeletedAt(),
            'local_updated_at' => $this->getIntLocalUpdatedAt(),
            'local_created_at' => $this->getIntLocalCreatedAt(),
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ];
    }
}
