<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\mobile;

use taktwerk\yiiboilerplate\traits\MobileDateTrait;
use taktwerk\yiiboilerplate\modules\guide\traits\MobileFileTrait;
use Yii;

class GuideChild extends \taktwerk\yiiboilerplate\modules\guide\models\GuideChild
{
    use MobileDateTrait, MobileFileTrait;

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        return [
            'id' => $this->id,
            'parent_guide_id' => $this->parent_guide_id,
            'guide_id' => $this->guide_id,
            'order_number' => $this->order_number,
            'deleted_at' => $this->getIntDeletedAt(),
            'updated_at' => $this->getIntUpdatedAt(),
            'created_at' => $this->getIntCreatedAt(),
            'local_deleted_at' => $this->getIntLocalDeletedAt(),
            'local_updated_at' => $this->getIntLocalUpdatedAt(),
            'local_created_at' => $this->getIntLocalCreatedAt(),
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ];
    }
}
