<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\mobile;

use taktwerk\yiiboilerplate\traits\MobileDateTrait;
use Yii;
use yii\helpers\ArrayHelper;

class GuideCategoryBinding extends \taktwerk\yiiboilerplate\modules\guide\models\base\GuideCategoryBinding
{
    use MobileDateTrait;

    protected static $relatedModels = [
        'guide',
        'guideCategory',
    ];

    public static function find($removedDeleted = true)
    {
        $model = parent::find($removedDeleted);
        if (array_key_exists('REQUEST_METHOD', $_SERVER) && !Yii::$app->user->can('Authority')) {
            foreach (self::$relatedModels as $relatedModel){
                $model->joinWith($relatedModel, false);
            }
            if(\Yii::$app->user->can('GuiderGroup')){
                $model->groupBy(self::tableName().'.id');
            }
        }
        return $model;
    }

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        return [
            'id' => $this->id,
            'guide_id' => $this->guide_id,
            'guide_category_id' => $this->guide_category_id,
            'deleted_at' => $this->getIntDeletedAt(),
            'updated_at' => $this->getIntUpdatedAt(),
            'created_at' => $this->getIntCreatedAt(),
            'local_deleted_at' => $this->getIntLocalDeletedAt(),
            'local_updated_at' => $this->getIntLocalUpdatedAt(),
            'local_created_at' => $this->getIntLocalCreatedAt(),
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ];
    }
}
