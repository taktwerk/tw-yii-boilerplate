<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\mobile;

use taktwerk\yiiboilerplate\traits\MobileDateTrait;
use taktwerk\yiiboilerplate\modules\guide\traits\MobileFileTrait;
use Yii;

class GuideStep extends \taktwerk\yiiboilerplate\modules\guide\models\GuideStep
{
    use MobileDateTrait, MobileFileTrait;

    protected static $relatedModels = [
        'guide'
    ];

    public function scenarios()
    {
        $scenario = parent::scenarios();
        $scenario['update'] = [
           'design_canvas_meta'
         ];
        return $scenario;
    }
    public function load($data, $formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;

        if ($scope === '' && !empty($data)) {
            $this->setAttributes($data);

            return true;
        } elseif (isset($data[$scope])) {
            $this->setAttributes($data[$scope]);

            return true;
        }

        return false;
    }

    public static function find($removedDeleted = true)
    {
        $model = parent::find($removedDeleted);

        if (array_key_exists('REQUEST_METHOD', $_SERVER) && !Yii::$app->user->can('Authority')) {
            foreach (self::$relatedModels as $relatedModel) {
                $model->joinWith($relatedModel, false);
            }
            if(\Yii::$app->user->can('GuiderGroup')){
                $model->groupBy(self::tableName().'.id');
            }
        }

        return $model;
    }

    public function getAttachedFilePath()
    {
        return $this->getFilePathByAttribute('attached_file');
    }

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $thumbnailAttribute = 'design_canvas';
        $canvasAttribute = $this->getCanvasAttribute('attached_file');
        $thumbAttachedFilePath = null;
        $attachedFilePath = null;
        $isVideo = ($this->getFileType('attached_file') === 'video');
        $thumbFileName = $this->getThumbFileName('attached_file');

        if($thumbFileName){
            Yii::$app->urlManager->setBaseUrl('/');
            $previewFileUri = $this->getPreviewFile($thumbnailAttribute, ['width' => '300', 'height' => '300']);
            Yii::$app->urlManager->setBaseUrl(null);
            $thumbAttachedFilePath = $this->getAbsoluteUrl($previewFileUri);
        }

        if (!$isVideo || ($isVideo && $thumbAttachedFilePath)) {
            $fileAttribute = $isVideo ? 'attached_file' : $canvasAttribute;
            $attachedFilePath = $this->getFilePathByAttribute($fileAttribute);
        }
        $designCanvasFile = $this->getFilePathByAttribute('design_canvas',[],true,true);

        return [
            'id' => $this->id,
            'guide_id' => $this->guide_id,
            'order_number' => $this->order_number,
            'title' => $this->title,
            'description_html' => $this->description_html,
            'attached_file' => $this->attached_file ?: null,
            'attached_file_path' => $attachedFilePath,
            'thumb_attached_file' => $thumbFileName,
            'thumb_attached_file_path' => $thumbAttachedFilePath,
            'design_canvas_file' => $designCanvasFile,
            'design_canvas_meta'=>$this->design_canvas_meta,
            'deleted_at' => $this->getIntDeletedAt(),
            'updated_at' => $this->getIntUpdatedAt(),
            'created_at' => $this->getIntCreatedAt(),
            'local_deleted_at' => $this->getIntLocalDeletedAt(),
            'local_updated_at' => $this->getIntLocalUpdatedAt(),
            'local_created_at' => $this->getIntLocalCreatedAt(),
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ];
    }
}
