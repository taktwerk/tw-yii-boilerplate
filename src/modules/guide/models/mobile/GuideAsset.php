<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\mobile;

use app\components\Helper;
use taktwerk\yiiboilerplate\traits\MobileDateTrait;
use taktwerk\yiiboilerplate\modules\guide\traits\MobileFileTrait;
use taktwerk\yiiboilerplate\helpers\FileHelper;

class GuideAsset extends \taktwerk\yiiboilerplate\modules\guide\models\GuideAsset
{
    use MobileDateTrait, MobileFileTrait;

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        if (!\Yii::$app->user) {
            return [];
        }
        $thumbFile = $this->getThumbFileName('asset_file');
        $thumbFilePath = str_replace('api/api', 'api', $this->getAbsoluteUrl($this->getThumbFileUrl('asset_file')));
        return [
            'id' => $this->id,
            'client_id' => $this->client_id,
            'user_id' => \Yii::$app->user->id,
            'name' => $this->name,
            'asset_html' => $this->asset_html,
            'asset_file' => $this->asset_file ? : null,
            'asset_file_path' => $this->getFilePathByAttribute('asset_file'),
            'thumb_asset_file' => $thumbFile,
            'thumb_asset_file_path' => $thumbFilePath,
            'pdf_image' => $thumbFile,
            'pdf_image_path' => $thumbFilePath,
            'deleted_at' => $this->getIntDeletedAt(),
            'updated_at' => $this->getIntUpdatedAt(),
            'created_at' => $this->getIntCreatedAt(),
            'local_deleted_at' => $this->getIntLocalDeletedAt(),
            'local_updated_at' => $this->getIntLocalUpdatedAt(),
            'local_created_at' => $this->getIntLocalCreatedAt(),
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ];
    }
}
