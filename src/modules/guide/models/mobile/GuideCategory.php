<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\mobile;

use taktwerk\yiiboilerplate\traits\MobileDateTrait;

class GuideCategory extends \taktwerk\yiiboilerplate\modules\guide\models\GuideCategory
{
    use MobileDateTrait;

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        if (!\Yii::$app->user) {
            return [];
        }

        return [
            'id' => $this->id,
            'client_id' => $this->client_id,
            'user_id' => \Yii::$app->user->id,
            'name' => $this->name,
            'deleted_at' => $this->getIntDeletedAt(),
            'updated_at' => $this->getIntUpdatedAt(),
            'created_at' => $this->getIntCreatedAt(),
            'local_deleted_at' => $this->getIntLocalDeletedAt(),
            'local_updated_at' => $this->getIntLocalUpdatedAt(),
            'local_created_at' => $this->getIntLocalCreatedAt(),
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ];
    }
}
