<?php


namespace taktwerk\yiiboilerplate\modules\guide\models\mobile;


use taktwerk\yiiboilerplate\traits\MobileDateTrait;
use taktwerk\yiiboilerplate\modules\guide\traits\MobileFileTrait;
use DateTimeZone;
use taktwerk\yiiboilerplate\modules\sync\traits\UserAppDataVersionTrait;
use taktwerk\yiiboilerplate\rest\exceptions\MissingCurrentDatetimeException;
use taktwerk\yiiboilerplate\traits\UploadTrait;
use Yii;
use taktwerk\yiiboilerplate\modules\customer\models\ClientUser;

class Feedback extends \taktwerk\yiiboilerplate\modules\feedback\models\Feedback
{
    use UploadTrait, MobileDateTrait, MobileFileTrait;

    public function rules()
    {
        $parentRules = parent::rules();
        $mobileRules = [
            [
                ['local_deleted_at', 'local_updated_at', 'local_created_at'],
                'default',
                'value' => null,
            ],
            [
                ['local_deleted_at', 'local_updated_at', 'local_created_at'],
                'date',
                'format'=>'yyyy-M-d H:m:s'
            ]
        ];

        return array_merge($parentRules, $mobileRules);
    }

    protected function prepareData($data, $scope)
    {
        $clientUser = ClientUser::find()->where(['user_id' => $data['user_id']])->one();
        if ($clientUser) {
            $data['client_id'] = $clientUser->client_id;
        } else {
            $data['client_id'] = null;
        }

        return $data;
    }

    protected function getReferenceModelByAlias($referenceModelAlias)
    {
        switch ($referenceModelAlias) {
            case 'guide':
                return 'app\\modules\\guide\\models\\Guide';
            case 'guide_step':
                return 'app\\modules\\guide\\models\\GuideStep';
            default:
                return null;
        }
    }

    public function load($data, $formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;

        $data = $this->prepareData($data, $scope);

        if ($scope === '' && !empty($data)) {
            $this->setAttributes($data);

            return true;
        } elseif (isset($data[$scope])) {
            $this->setAttributes($data[$scope]);

            return true;
        }

        return false;
    }

    public function getAttachedFilePath()
    {
        return $this->getFilePathByAttribute('attached_file');
    }

    public function beforeSave($insert)
    {
        $this->beforeSaveForLocalDateAttributes($insert);

        return parent::beforeSave($insert);
    }

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        if (!\Yii::$app->user) {
            return [];
        }

        $this->refresh();
        $attachedFileAttribute = 'attached_file';
        $thumbFileName = $this->getThumbFileName($attachedFileAttribute);
        $thumbAttachedFilePath = str_replace('api/api', 'api', $this->getAbsoluteUrl($this->getPreviewFile($attachedFileAttribute)));
        $isVideo = ($this->getFileType($attachedFileAttribute) === 'video');
        $attachedFilePath = null;
        if (!$isVideo || ($isVideo && $thumbAttachedFilePath)) {
            $attachedFilePath = $thumbAttachedFilePath ? $this->getFilePathByAttribute($attachedFileAttribute) : null;
        }

        $feedbackResponse = [
            'id' => $this->id,
            'client_id' => $this->client_id,
            'user_id' => $this->created_by,
            'title' => $this->title,
            'reference_model' => $this->reference_model,
            'feedback_url' => $this->feedback_url,
            'status' => $this->status,
            'reference_id' => $this->reference,
            'attached_file' => $this->attached_file ?: null,
            'attached_file_path' => $attachedFilePath,
            'thumb_attached_file' => $thumbFileName,
            'thumb_attached_file_path' => $thumbAttachedFilePath,
            'description' => substr($this->description, 0, 250),
            'deleted_at' => $this->getIntDeletedAt(),
            'updated_at' => $this->getIntUpdatedAt(),
            'created_at' => $this->getIntCreatedAt(),
            'local_deleted_at' => $this->getIntLocalDeletedAt(),
            'local_updated_at' => $this->getIntLocalUpdatedAt(),
            'local_created_at' => $this->getIntLocalCreatedAt(),
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ];

        return $feedbackResponse;
    }
}
