<?php

namespace taktwerk\yiiboilerplate\modules\guide\models\mobile;

use taktwerk\yiiboilerplate\traits\MobileDateTrait;
use taktwerk\yiiboilerplate\modules\guide\traits\MobileFileTrait;
use Yii;
use yii\helpers\Json;

class GuideViewHistory extends \taktwerk\yiiboilerplate\modules\guide\models\GuideViewHistory
{
    use MobileDateTrait;

    public function beforeSave($insert)
    {
        $this->beforeSaveForLocalDateAttributes($insert);

        $request = Yii::$app->request;
        $post    = $request->post();

        $step = $post['step'];
        $parent_guide_id = $post['parent_guide_id'];
        $metaData = ['step_order_number' => $step];
        if($parent_guide_id){
            $metaData['child_guide_id'] = $parent_guide_id;
        }
        $this->data = Json::encode($metaData);

        return parent::beforeSave($insert);
    }

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        if (!\Yii::$app->user) {
            return [];
        }

        $this->refresh();
        $defaultData = [
        'id' => $this->id,
        'client_id' => $this->client_id,
        'user_id' => \Yii::$app->user->id,
        ];
        try{
            $metadata = Json::decode($this->data);
            $guideData = [];
            $guideData['guide_id'] = $this->guide_id;
            $guideData['step'] = $metadata['step_order_number'];
            $guideData['parent_guide_id'] = null;
            if(isset($metadata['child_guide_id'])){
                $guideData['guide_id'] = $metadata['child_guide_id'];
                $guideData['parent_guide_id'] = $this->guide_id;
            }
            $defaultData = array_merge($defaultData,$guideData);
        }catch(\Exception $e){
            
        }
        return array_merge($defaultData,[
            'deleted_at' => $this->getIntDeletedAt(),
            'updated_at' => $this->getIntUpdatedAt(),
            'created_at' => $this->getIntCreatedAt(),
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ]);
    }
}
