<?php

namespace taktwerk\yiiboilerplate\modules\guide\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\guide\models\base\GuideAsset as BaseGuideAsset;
use taktwerk\yiiboilerplate\modules\sync\traits\UserAppDataVersionTrait;
use taktwerk\yiiboilerplate\helpers\FileHelper;

/**
 * This is the model class for table "guide_asset".
 */
class GuideAsset extends BaseGuideAsset
{
    use UserAppDataVersionTrait;

//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }
    
    public function getPdf_image()
    {
        return $this->getThumbFileName('asset_file');
    }
    public function setPdf_image()
    {
        
    }
    
    /**
     * Description for the table/model to show on grid an form view
     * @return String
     */
    public static function tableHint()
    {
        return Yii::t('twbp', 'Assets can be defined once and attached to multiple guides. Fill either text or upload a file.');
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return array_merge(parent::attributeHints(), [
            'asset_file' => Yii::t('twbp', 'Choose an image, video, audio, pdf or 3D file.'),
            'asset_html' => Yii::t('twbp', 'Text is only shown, when no file is defined.'),
            'name' => Yii::t('twbp', 'Used as reference in administration backend. Is shown as button label.'),
        ]);
    }

    /**
     * Check if the asset a pdf
     * @return bool
     */
    public function isPdf()
    {
        return \yii\helpers\FileHelper::getMimeType($this->asset_file)=='application/pdf';
    }

    /**
     * @return bool
     */
    public function isFile()
    {
        return !empty($this->asset_file);
    }

    /**
     * @return bool
     */
    public function isText()
    {
        return !empty($this->asset_html);
    }

    /**
     * @param int $width
     * @param int $height
     * @return mixed|string
     * @throws \Exception
     */
    /* public function pdfImageUrl($width = 300, $height = 300)
    {
        if ($this->isPdf() and class_exists('Imagick')) {
            try {
                $pdf = $this->getUploadPath() . $this->asset_file;
                $pngFileName = Str::replaceLast('.pdf', '.png', strtolower($this->asset_file));
                $png = $this->getUploadPath() . str_replace('.png', '_' . $width.'_' . $height . '_png', $pngFileName);

                if (!Yii::$app->fs->has($png)) {
                    $tmpFile = Yii::getAlias('@runtime/convert/' . $this->asset_file);
                    $convertedFile = Yii::getAlias('@runtime/convert/' . $pngFileName);
                    $contents = Yii::$app->fs->read($pdf);

                    @mkdir(Yii::getAlias('@runtime/convert/'));
                    file_put_contents($tmpFile, $contents);

                    $imagick = new \Imagick();
                    $imagick->setResolution(300, 300);
                    $imagick->readImage($tmpFile . "[0]");
                    $imagick->setImageFormat("png");
                    $imagick->setCompressionQuality(90);
                    $imagick->setSize($width, $height);
                    $imagick->cropThumbnailImage($width, $height);
                    //$imagick->resizeImage($width, $height,\Imagick::FILTER_LANCZOS,0,false);
                    $imagick->writeImage($convertedFile);

                    // Save thumb
                    $stream = fopen($convertedFile, 'r+');
                    Yii::$app->fs->writeStream($png, $stream);
                }

                return Helper::getFile("elfinder", "fs", $png);
            }
            catch(\Exception $e) {

            }
        }
        return $this->getFileUrl('asset_file');
    } */

    /**
     * @return string
     */
    public function imagePath()
    {
        return $this->getUploadPath() . $this->asset_file;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if (array_key_exists('asset_file', $changedAttributes) &&
            !isset($changedAttributes['pdf_image']) &&
            $this->isPdf() &&
            class_exists('Imagick')
        ) {
            $this->refresh();
            $pdfImg = FileHelper::PdfToImageByModel($this,'asset_file');
            if($pdfImg && $pdfImg['success']){
                //$this->pdf_image = $pdfImg['name'];
                $this->save();
            }
            
        }
        $this->changeUserAppDataVersion($insert, $changedAttributes);
    }

    /**
     * Before deleting assets, delete all pivots linking them to guides
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        foreach ($this->guideAssetPivots as $c) {
            $c->delete();
        }
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }
}
