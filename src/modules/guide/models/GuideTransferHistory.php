<?php
//Generation Date: 21-Oct-2020 08:45:20am
namespace taktwerk\yiiboilerplate\modules\guide\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\guide\models\base\GuideTransferHistory as BaseGuideTransferHistory;

/**
 * This is the model class for table "guide_transfer_history".
 */
class GuideTransferHistory extends BaseGuideTransferHistory
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

}
