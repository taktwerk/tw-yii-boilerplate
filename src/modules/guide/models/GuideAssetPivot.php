<?php

namespace taktwerk\yiiboilerplate\modules\guide\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\guide\models\base\GuideAssetPivot as BaseGuideAssetPivot;
use taktwerk\yiiboilerplate\modules\sync\traits\UserAppDataVersionTrait;

/**
 * This is the model class for table "guide_asset_pivot".
 */
class GuideAssetPivot extends BaseGuideAssetPivot
{
    use UserAppDataVersionTrait;
    public static function find($removedDeleted = true){
        $query = parent::find($removedDeleted);
        if(array_key_exists('REQUEST_METHOD', $_SERVER) && !Yii::$app->user->can('Authority')){
            $query->joinWith('guide');
            if(\Yii::$app->user->can('GuiderGroup'))
            {
                $query->groupBy('guide_asset_pivot.id');
            }
        }
        return $query;
    }
    public function clientsForChangeAppDataVersion()
    {
        $clientIds = [];
        $clientIds[] = $this->guideAsset->client_id;
        $clientIds[] = $this->guide->client_id;

        return $clientIds;
    }
}
