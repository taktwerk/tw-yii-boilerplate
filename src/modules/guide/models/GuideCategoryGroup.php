<?php

namespace taktwerk\yiiboilerplate\modules\guide\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\guide\models\base\GuideCategoryGroup as BaseGuideCategoryGroup;
use taktwerk\yiiboilerplate\modules\user\models\Group;

/**
 * This is the model class for table "guide_category_group".
 */
class GuideCategoryGroup extends BaseGuideCategoryGroup
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['guide_category_id'],
                'unique', 'targetAttribute' => 
                ['guide_category_id', 'group_id'],'message'=>\Yii::t('twbp','This Group and Guide Category combination already exist')],
        ]);
    }
    /**
     * TODO: what is this function for? its the same as in base model
     * @param null $q
     * @return array
     */
    public static function groupList($q = null, $id=null, $page=0)
    {
        return Group::filter($q, $id, $page);
    }
}
