<?php
//Generation Date: 10-Sep-2020 06:25:23am
namespace taktwerk\yiiboilerplate\modules\guide\models;

use Yii;
use \taktwerk\yiiboilerplate\modules\guide\models\base\ProductionLine as BaseProductionLine;

/**
 * This is the model class for table "production_line".
 */
class ProductionLine extends BaseProductionLine
{
//    /**
//     * List of additional rules to be applied to model, uncomment to use them
//     * @return array
//     */
//    public function rules()
//    {
//        return array_merge(parent::rules(), [
//            [['something'], 'safe'],
//        ]);
//    }

}
