<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;
use dektrium\user\helpers\Password;

class m190314_000000_guide_elfinder_acl extends TwMigration
{
    public function safeUp()
    {
        $this->createPermission('app_site_image', 'App Site Image', ['Customer']);
        $this->createPermission('app_elfinder_connect ', 'App Site Image', ['Customer']);
    }

    public function safeDown()
    {
        $this->removePermission('app_site_image', ['Customer']);
        $this->removePermission('app_elfinder_connect', ['Customer']);
    }
}
