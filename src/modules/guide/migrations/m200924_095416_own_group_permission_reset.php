<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;
use yii\db\Query;
use yii\db\Expression;

class m200924_095416_own_group_permission_reset extends TwMigration
{
    public function up()
    {
       (new Query)
        ->createCommand()
        ->delete('auth_item_child', 
            ['OR',['like','parent',new Expression("'guide_%_own'")],['like','parent',new Expression("'guide_%_group'")]])
        ->execute();
        
        (new Query)
        ->createCommand()
        ->delete('auth_item_child', 
            ['OR',['like','child',new Expression("'guide_%_own'")],['like','child',new Expression("'guide_%_group'")]])
        ->execute();
        
        $this->createCrudControllerPermissions('guide_guide-child');
        $this->createCrudControllerPermissions('guide_guide-data');
        $this->createCrudControllerPermissions('guide_guide-category-group');
        $this->createCrudControllerPermissions('guide_guide-category');
        
        $this->createPermission('guide_guide-category-group_group','',['GuiderViewer']);
        $this->createPermission('guide_guide-category_group','',['GuiderViewer']);
    }

    public function down()
    {
        echo "m200924_095416_own_group_permission_reset cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
