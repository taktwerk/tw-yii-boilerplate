<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200425_102610_remove_guide_setting extends TwMigration
{
    public function up()
    {
        $this->removePermission('x_guide_guide-setting_see', ['GuiderViewer']);
        $this->removePermission('guide_guide-setting',['GuiderViewer']);
    }

    public function down()
    {
        echo "m200425_102610_remove_guide_setting cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
