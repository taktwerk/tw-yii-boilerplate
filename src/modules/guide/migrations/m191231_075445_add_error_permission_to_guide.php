<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191231_075445_add_error_permission_to_guide extends TwMigration
{
    public function up()
    {
        $this->createPermission('guide_explore_error', 'Guide Explore Error', ['GuiderAdmin']);
        $this->createPermission('guide_explore_error', 'Guide Explore Error', ['GuiderViewer']);
    }

    public function down()
    {
        echo "m191231_075445_add_error_permission_to_guide cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
