<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200204_135538_add_offline_change_date_columns extends TwMigration
{
    use \taktwerk\yiiboilerplate\modules\guide\traits\MobileMigrationTrait;

    public $tablesWithOfflineChangeDateColumns = [
        'guide',
        'guide_asset',
        'guide_asset_pivot',
        'guide_category',
        'guide_category_binding',
        'guide_step',
        'feedback',
    ];

    public function up()
    {
        foreach ($this->tablesWithOfflineChangeDateColumns as $table) {
            $this->addOfflineChangeDateColumns($table);
        }
    }

    public function down()
    {
        echo "m200204_135538_add_offline_change_date_columns cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
