<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%guide_setting}}`.
 */
class m201215_041853_add_is_historicised_column_to_guide_setting_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%guide_setting}}', 'is_historicised', $this->boolean()->null()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
            $this->dropColumn('{{%guide_setting}}', 'is_historicised');
    }
}
