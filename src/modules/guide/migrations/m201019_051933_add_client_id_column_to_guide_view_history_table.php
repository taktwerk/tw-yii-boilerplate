<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%guide_view_history}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%client}}`
 */
class m201019_051933_add_client_id_column_to_guide_view_history_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        
        $this->addColumn('{{%guide_view_history}}', 'client_id', $this->integer(11)->after('id')->notNull());
        // creates index for column `client_id`
        $this->createIndex(
            '{{%idx-guide_view_history-client_id}}',
            '{{%guide_view_history}}',
            'client_id'
        );
        try{
            // add foreign key for table `{{%client}}`
            $this->addForeignKey(
                '{{%fk-guide_view_history-client_id}}',
                '{{%guide_view_history}}',
                'client_id',
                '{{%client}}',
                'id',
                'CASCADE'
            );
        }catch(\Exception $e){
            // drops index for column `client_id`
            $this->dropIndex(
                '{{%idx-guide_view_history-client_id}}',
                '{{%guide_view_history}}'
                );
            
            $this->dropColumn('{{%guide_view_history}}', 'client_id');
            throw $e;
        }
        $this->createIndex('idx_guide_view_history_deleted_at_client_id', '{{%guide_view_history}}', ['deleted_at','client_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%client}}`
        $this->dropForeignKey(
            '{{%fk-guide_view_history-client_id}}',
            '{{%guide_view_history}}'
        );

        // drops index for column `client_id`
        $this->dropIndex(
            '{{%idx-guide_view_history-client_id}}',
            '{{%guide_view_history}}'
        );
        $this->dropIndex(
            '{{%idx_guide_view_history_deleted_at_client_id}}',
            '{{%guide_view_history}}'
            );

        $this->dropColumn('{{%guide_view_history}}', 'client_id');
    }
}
