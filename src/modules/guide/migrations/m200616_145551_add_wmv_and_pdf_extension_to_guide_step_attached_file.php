<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200616_145551_add_wmv_and_pdf_extension_to_guide_step_attached_file extends TwMigration
{
    public function up()
    {
        $comment = <<<EOT
{"allowedExtensions":["jpg","jpeg","png","webp","mp4","mov","avi","mpeg","mpg","m4v","mkv","ogv", "mp3", "ogg", "wav", "m4a", "m4p", "aac", "opus", "midi", "flac", "wmv", "pdf"]}
EOT;
        $this->addCommentOnColumn('{{%guide_step}}', 'attached_file', $comment);
    }

    public function down()
    {
        $comment = <<<EOT
{"allowedExtensions":["jpg","jpeg","png","webp","mp4","mov","avi","mpeg","mpg","m4v","mkv","ogv", "mp3", "ogg", "wav", "m4a", "m4p", "aac", "opus", "midi", "flac"]}
EOT;
        $this->addCommentOnColumn('{{%guide_step}}', 'attached_file', $comment);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
