<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200613_120453_create_guide_explore_guides_permission extends TwMigration
{
    public function up()
    {
        $this->createPermission('guide_explore_guides', 'Guider Explore By Category', ['GuiderViewer']);
    }

    public function down()
    {
        echo "m200613_120453_create_guide_explore_guides_permission cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
