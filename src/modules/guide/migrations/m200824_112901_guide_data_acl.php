<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200824_112901_guide_data_acl extends TwMigration
{
    public function up()
    {
        $this->createCrudPermissions('guide_guide-data', 'Guide Data', ['GuiderAdmin']);
    }

    public function down()
    {
        echo "m200824_112901_guide_data_acl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
