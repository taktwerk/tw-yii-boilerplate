<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;
use dektrium\user\helpers\Password;

class m190401_000001_guide_assets_acl extends TwMigration
{
    public function safeUp()
    {
        $this->createCrudPermissions('guide_guide-asset', 'Guide Assets', ['GuiderAdmin']);
        $this->createCrudPermissions('guide_guide-asset-pivot', 'Guide Assets Pivot', ['GuiderAdmin']);
    }

    public function safeDown()
    {
    }
}
