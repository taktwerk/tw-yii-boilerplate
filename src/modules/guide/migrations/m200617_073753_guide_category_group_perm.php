<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200617_073753_guide_category_group_perm extends TwMigration
{
    public function up()
    {
        $this->createCrudPermissions('guide_guide-category-group', 'guide Guide Categories Group');
        $auth = $this->getAuth();
        $viewer = $auth->getRole('GuiderViewer');
        $groupPerm = $auth->getPermission('guide_guide-category-group_group');
        if($groupPerm){
            $auth->addChild($viewer, $groupPerm);
        }
    }

    public function down()
    {
        echo "m200617_073753_guide_category_group_perm cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
