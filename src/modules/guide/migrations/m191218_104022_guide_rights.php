<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191218_104022_guide_rights extends TwMigration
{
    public function up()
    {
        $this->createCrudControllerPermissions('guide_guide');
        $this->createCrudControllerPermissions('guide_guide-step');
        $this->createCrudControllerPermissions('guide_guide-asset-pivot');
        $this->createCrudControllerPermissions('guide_guide-category-binding');

        $this->addAdminControllerPermission('guide_guide-step','GuiderAdmin');
        $this->addAdminControllerPermission('guide_guide-asset-pivot','GuiderAdmin');
        $this->addAdminControllerPermission('guide_guide-category-binding','GuiderAdmin');
    }

    public function down()
    {
        echo "m191218_104022_guide_rights cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
