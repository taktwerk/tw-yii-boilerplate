<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200205_110940_rename_guide_config_tbl extends TwMigration
{
    public function up()
    {
        $this->renameTable('{{%guide_config}}', '{{%guide_setting}}');
    }

    public function down()
    {
        echo "m200205_110940_rename_guide_config_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
