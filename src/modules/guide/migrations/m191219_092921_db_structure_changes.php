<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191219_092921_db_structure_changes extends TwMigration
{
    public function up()
    {
        $query = <<<EOF
            ALTER TABLE `guide`
	        ALTER `template_id` DROP DEFAULT;
            ALTER TABLE `guide`
                CHANGE COLUMN `preview_file` `preview_file` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `description`,
                CHANGE COLUMN `revision_term` `revision_term` VARCHAR(10) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `preview_file`,
                CHANGE COLUMN `revision_counter` `revision_counter` INT(11) NOT NULL DEFAULT 0 AFTER `revision_term`,
                CHANGE COLUMN `duration` `duration` INT(11) NULL DEFAULT NULL AFTER `revision_counter`,
                CHANGE COLUMN `template_id` `template_id` INT(11) NOT NULL AFTER `duration`,
                CHANGE COLUMN `protocol_template_id` `protocol_template_id` INT(11) NULL DEFAULT NULL AFTER `template_id`;
            
            ALTER TABLE `guide_template`
                CHANGE COLUMN `code` `script` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `name`;
            
            ALTER TABLE `guide_step`
            ALTER `step` DROP DEFAULT;
            ALTER TABLE `guide_step`
                CHANGE COLUMN `step` `order_number` INT(11) NOT NULL AFTER `guide_id`;
                
            ALTER TABLE `guide`
                COMMENT='{"base_namespace":"app\\\\modules\\\\guide"}';
            
            ALTER TABLE `guide_step`
                COMMENT='{"base_namespace":"app\\\\modules\\\\guide"}';
            
            ALTER TABLE `guide_asset`
                COMMENT='{"base_namespace":"app\\\\modules\\\\guide"}';
            
            ALTER TABLE `guide_asset_pivot`
                COMMENT='{"base_namespace":"app\\\\modules\\\\guide"}';
            
            ALTER TABLE `guide_category`
                COMMENT='{"base_namespace":"app\\\\modules\\\\guide"}';
            
            ALTER TABLE `guide_category_binding`
                COMMENT='{"base_namespace":"app\\\\modules\\\\guide"}';
            
            ALTER TABLE `guide_tag`
                COMMENT='{"base_namespace":"app\\\\modules\\\\guide"}';
            
            ALTER TABLE `guide_template`
                COMMENT='{"base_namespace":"app\\\\modules\\\\guide"}';
EOF;
        $this->execute($query);
    }

    public function down()
    {
        echo "m191219_092921_db_structure_changes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
