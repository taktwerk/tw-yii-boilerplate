<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200512_044926_add_zip_permission_to_guideAdmin extends TwMigration
{
    /**
     * @return bool|void|null
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->createPermission('guide_guide_generate-zip', 'Generate Guide Zip', ['GuiderAdmin']);
    }

    public function down()
    {
        echo "m200512_044926_add_zip_permission_to_guideAdmin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
