<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;
use dektrium\user\helpers\Password;

class m180807_010000_calculator_role extends TwMigration
{
    public function safeUp()
    {
        $this->createRole('Calculator', 'Customer');

        $this->removePermission('app_address', 'Customer');
        $this->removePermission('app_dfx_calculate', 'Customer');
        $this->removePermission('label_label-order', 'Customer');

        $this->createPermission('app_address', 'Address', 'Calculator');
        $this->createPermission('app_dfx_calculate', 'Dfx Calculator', 'Calculator');
        $this->createPermission('label_label-order', 'Label Order', 'Calculator');
    }

    public function safeDown()
    {
        $this->removeRole('Calculator');
    }
}
