<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;
use dektrium\user\helpers\Password;

class m190313_000000_guide_admin_home_acl extends TwMigration
{
    public function safeUp()
    {
        $auth = $this->getAuth();
        $perm = $this->getPermission('app_site');
        $role = $auth->getRole('GuiderAdmin');
        $auth->addChild($role, $perm);
        $perm = $this->getPermission('backend_default');
        $auth->addChild($role, $perm);
    }

    public function safeDown()
    {
        $auth = $this->getAuth();
        $perm = $this->getPermission('app_site');
        $role = $auth->getRole('GuiderAdmin');
        $auth->removeChild($role, $perm);
        $perm = $this->getPermission('backend_default');
        $auth->removeChild($role, $perm);
    }
}
