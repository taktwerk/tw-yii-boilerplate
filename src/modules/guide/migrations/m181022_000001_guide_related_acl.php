<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;
use dektrium\user\helpers\Password;

class m181022_000001_guide_related_acl extends TwMigration
{
    public function safeUp()
    {
        // Create the guide client
        //$this->createPermission('guide_guide_related-form', 'Guide Related Form', ['GuiderAdmin']);
        $this->createPermission('guide_explore_search', 'Guide Explore Search', ['GuiderViewer']);
        $this->createPermission('guide_guide-step_sort-item', 'Guide Step Sort Item', 'GuiderAdmin');
        $this->createCrudPermissions('guide_guide-category', 'Guider Categories', ['GuiderAdmin']);
    }

    public function safeDown()
    {
        $this->removePermission('guide_guide_related-form', ['GuiderAdmin']);
        $this->removePermission('guide_guide-step_sort-item', 'GuiderAdmin');
    }
}
