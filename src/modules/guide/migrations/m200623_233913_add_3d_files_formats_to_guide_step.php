<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200623_233913_add_3d_files_formats_to_guide_step extends TwMigration
{
    public function up()
    {
        $comment = <<<EOT
{"allowedExtensions":["jpg","jpeg","png","webp","mp4","mov","avi","mpeg","mpg","m4v","mkv","ogv", "mp3", "ogg", "wav", "m4a", "m4p", "aac", "opus", "midi", "flac", "wmv", "pdf", "stp", "gltf"]}
EOT;
        $this->addCommentOnColumn('{{%guide_step}}', 'attached_file', $comment);
    }

    public function down()
    {
        echo "m200623_233913_add_3d_files_formats_to_guide_step cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
