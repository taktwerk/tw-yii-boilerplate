<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;
use dektrium\user\helpers\Password;

class m180809_010000_guider_category extends TwMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{%guide_category}}',
            [
                'id' => $this->primaryKey(),
                'client_id' => $this->integer()->notNull(),
                'name' => $this->string(45)->notNull()
            ]
        );
        $this->addForeignKey('fk_guide_category_client_id', '{{%guide_category}}', 'client_id', '{{%client}}', 'id');

        $this->createTable(
            '{{%guide_category_binding}}',
            [
                'id' => $this->primaryKey(),
                'guide_id' => $this->integer()->notNull(),
                'guide_category_id' => $this->integer()->notNull(),
            ]
        );
        $this->addForeignKey('fk_guide_category_binding_guide_id', '{{%guide_category_binding}}', 'guide_id', '{{%guide}}', 'id');
        $this->addForeignKey('fk_guide_category_binding_guide_category_id', '{{%guide_category_binding}}', 'guide_category_id', '{{%guide_category}}', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('{{%guide_category_binding}}');
        $this->dropTable('{{%guide_category}}');
    }
}
