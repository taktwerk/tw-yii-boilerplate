<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;
use dektrium\user\helpers\Password;

class m181015_000002_guider_protocol_id extends TwMigration
{
    public function safeUp()
    {
        $this->addColumn('{{%guide}}', 'protocol_template_id', $this->integer()->null());
        $this->addForeignKey('guide_fk_protocol_template_id', '{{%guide}}', 'protocol_template_id', '{{%protocol_template}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('guide_fk_protocol_template_id', '{{%guide}}');
        $this->dropColumn('{{%guide}}', 'protocol_template_id');
    }
}
