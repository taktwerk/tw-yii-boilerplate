<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m210520_073024_guiderGroup_to_guideViewer_perm extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $guiderGroup = $auth->getRole('GuiderGroup');
        $guiderViewer = $auth->getRole('GuiderViewer');
        if($guiderGroup && $guiderViewer){
            $auth->addChild($guiderViewer, $guiderGroup);
        }
    }

    public function down()
    {
        echo "m210520_073024_guiderGroup_to_guideViewer_perm cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
