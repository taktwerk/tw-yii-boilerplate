<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200424_184408_add_syncAdmin_role_to_guiderAdmin extends TwMigration
{
    /**
     * @return bool|void|null
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $auth = $this->getAuth();
        $SyncAdmin = $auth->getRole('SyncAdmin');
        $guider = $auth->getRole('GuiderAdmin');
        $auth->addChild($guider, $SyncAdmin);
    }

    public function down()
    {
        echo "m200424_184408_add_syncAdmin_role_to_guiderAdmin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
