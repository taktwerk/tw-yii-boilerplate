<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m180802_010000_guide_tags extends TwMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{%guide_tag}}',
            [
                'id' => $this->primaryKey(),
                'guide_id' => $this->integer()->notNull(),
                'name' => $this->string(45)->notNull()
            ]
        );
        $this->addForeignKey('fk_guide_tag_guide_id', '{{%guide_tag}}', 'guide_id', '{{%guide}}', 'id');
        $this->createIndex('guide_tag_name_idx', '{{%guide_tag}}', ['name']);
    }

    public function safeDown()
    {
        $this->dropTable('{{%guide_tag}}');
    }
}