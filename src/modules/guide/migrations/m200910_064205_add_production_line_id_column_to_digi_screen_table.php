<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%digi_screen}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%production_line}}`
 */
class m200910_064205_add_production_line_id_column_to_digi_screen_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%digi_screen}}', 'production_line_id', $this->integer(11)->after('layout')->null());

        // creates index for column `production_line_id`
        $this->createIndex(
            '{{%idx-digi_screen-production_line_id}}',
            '{{%digi_screen}}',
            'production_line_id'
        );

        // add foreign key for table `{{%production_line}}`
        $this->addForeignKey(
            '{{%fk-digi_screen-production_line_id}}',
            '{{%digi_screen}}',
            'production_line_id',
            '{{%production_line}}',
            'id',
            'CASCADE'
        );
        $this->createIndex('idx_digi_screen_deleted_at_production_line_id', '{{%digi_screen}}', ['deleted_at','production_line_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%production_line}}`
        $this->dropForeignKey(
            '{{%fk-digi_screen-production_line_id}}',
            '{{%digi_screen}}'
        );

        // drops index for column `production_line_id`
        $this->dropIndex(
            '{{%idx-digi_screen-production_line_id}}',
            '{{%digi_screen}}'
        );

        $this->dropColumn('{{%digi_screen}}', 'production_line_id');
    }
}
