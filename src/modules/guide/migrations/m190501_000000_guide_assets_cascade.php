<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m190501_000000_guide_assets_cascade extends TwMigration
{
    public function safeUp()
    {
		$this->dropForeignKey('fk_guide_asset_pivot_client_id', '{{%guide_asset_pivot}}');
		$this->dropForeignKey('fk_guide_asset_pivot_template_id', '{{%guide_asset_pivot}}');

		$this->addForeignKey('fk_guide_asset_pivot_client_id', '{{%guide_asset_pivot}}', 'guide_id', '{{%guide}}', 'id', 'cascade');

    	$this->addForeignKey('fk_guide_asset_pivot_template_id', '{{%guide_asset_pivot}}', 'guide_asset_id', '{{%guide_asset}}', 'id', 'cascade');
    }

    public function safeDown()
    {
        // no down for this migration
    }
}