<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191212_131541_add_duration_column_to_guide extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%guide}}', 'duration', $this->integer()->null());
    }

    public function down()
    {
        echo "m191212_131541_add_duration_column_to_guide cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
