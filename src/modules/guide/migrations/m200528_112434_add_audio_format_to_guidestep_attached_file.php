<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200528_112434_add_audio_format_to_guidestep_attached_file extends TwMigration
{
    public function up()
    {
        $comment = <<<EOT
{"allowedExtensions":["jpg","jpeg","png","webp","mp4","mov","avi","mpeg","mpg","m4v","mkv","ogv","mp3","ogg","wav","aac","wma","midi"]}
EOT;
        $this->addCommentOnColumn('{{%guide_step}}', 'attached_file', $comment);
    }

    public function down()
    {
        echo "m200528_112434_add_audio_format_to_guidestep_attached_file cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
