<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200928_040944_guide_file_meta_columns extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%guide}}', 'preview_file_filemeta', $this->text()->after('preview_file'));
        $this->addColumn('{{%guide_asset}}', 'asset_file_filemeta', $this->text()->after('asset_file'));
        $this->addColumn('{{%guide_asset}}', 'pdf_image_filemeta', $this->text()->after('pdf_image'));
        $this->addColumn('{{%guide_step}}', 'attached_file_filemeta', $this->text()->after('attached_file'));
        $this->addColumn('{{%guide_step}}', 'design_canvas_filemeta', $this->text()->after('design_canvas'));
         
    }

    public function down()
    {
        $this->dropColumn('{{%guide}}', 'preview_file_filemeta');
        $this->dropColumn('{{%guide_asset}}', 'asset_file_filemeta');
        $this->dropColumn('{{%guide_asset}}', 'pdf_image_filemeta');
        $this->dropColumn('{{%guide_step}}', 'attached_file_filemeta');
        $this->dropColumn('{{%guide_step}}', 'design_canvas_filemeta');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
