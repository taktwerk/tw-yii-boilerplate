<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m210331_091044_guide_setting_perm_revoke_from_guideeradmin extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $role = $auth->getRole('GuiderAdmin');
        if($role){
            $perm = $auth->getPermission('x_guide_guide-setting_admin');
            if($perm){
                $auth->removeChild($role,$perm);
            }
            $seePerm = $auth->getPermission('x_guide_guide-setting_see');
            if($seePerm){
                $auth->removeChild($role,$seePerm);
            }
        }
    }

    public function down()
    {
        echo "m210331_091044_guide_setting_perm_revoke_from_guideeradmin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
