<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200114_124105_guide_step_comment_order_number extends TwMigration
{
    public function up()
    {
        $comment = <<<EOT
{ "sortRestrictAttributes":["guide_id"]}
EOT;
        $this->addCommentOnColumn('{{%guide_step}}', 'order_number', $comment);
    
    }

    public function down()
    {
        echo "m200114_124105_guide_step_comment_order_number cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
