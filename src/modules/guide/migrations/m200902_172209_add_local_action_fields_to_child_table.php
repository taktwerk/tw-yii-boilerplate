<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200902_172209_add_local_action_fields_to_child_table extends TwMigration
{
    use \taktwerk\yiiboilerplate\modules\guide\traits\MobileMigrationTrait;
    
    public function up()
    {
        $this->addOfflineChangeDateColumns('guide_child');
    }

    public function down()
    {
        echo "m200902_172209_add_local_action_fields_to_child_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
