<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%guide_view_history}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m201019_094511_add_user_id_column_to_guide_view_history_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%guide_view_history}}', 'user_id', $this
            ->integer(11)->after('guide_id')
            ->comment('User who is currently viewing this guide'));

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-guide_view_history-user_id}}',
            '{{%guide_view_history}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-guide_view_history-user_id}}',
            '{{%guide_view_history}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
        $this->createIndex('idx_guide_view_history_deleted_at_user_id', '{{%guide_view_history}}', ['deleted_at','user_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-guide_view_history-user_id}}',
            '{{%guide_view_history}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-guide_view_history-user_id}}',
            '{{%guide_view_history}}'
        );
        $this->dropIndex(
            '{{%idx_guide_view_history_deleted_at_user_id}}',
            '{{%guide_view_history}}'
            );

        $this->dropColumn('{{%guide_view_history}}', 'user_id');
    }
}
