<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%guide_view_history}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%guide}}`
 */
class m201009_103821_create_guide_view_history_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%guide_view_history}}', [
            'id' => $this->primaryKey(),
            'guide_id' => $this->integer(11),
            'data' => $this->text(),
        ]);
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\guide"}';
        $this->addCommentOnTable('{{%guide_view_history}}', $comment);
        
        // creates index for column `guide_id`
        $this->createIndex(
            '{{%idx-guide_view_history-guide_id}}',
            '{{%guide_view_history}}',
            'guide_id'
        );

        // add foreign key for table `{{%guide}}`
        $this->addForeignKey(
            '{{%fk-guide_view_history-guide_id}}',
            '{{%guide_view_history}}',
            'guide_id',
            '{{%guide}}',
            'id',
            'CASCADE'
        );
        $this->createIndex('idx_guide_view_history_deleted_at_guide_id', '{{%guide_view_history}}', ['deleted_at','guide_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    $this->dropIndex('idx_guide_view_history_deleted_at_guide_id', '{{%guide_view_history}}');
            // drops foreign key for table `{{%guide}}`
        $this->dropForeignKey(
            '{{%fk-guide_view_history-guide_id}}',
            '{{%guide_view_history}}'
        );

        // drops index for column `guide_id`
        $this->dropIndex(
            '{{%idx-guide_view_history-guide_id}}',
            '{{%guide_view_history}}'
        );

        $this->dropTable('{{%guide_view_history}}');
    }
}
