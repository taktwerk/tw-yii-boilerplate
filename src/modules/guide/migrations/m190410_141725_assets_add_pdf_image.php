<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m190410_141725_assets_add_pdf_image extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%guide_asset}}', 'pdf_image', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn('{{%guide_asset}}', 'pdf_image');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
