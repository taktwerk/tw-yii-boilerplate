<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m201016_105402_guiderAdmin_guide_view_history_perm extends TwMigration
{
    public function safeUp()
    {
        
        $this->createRole('GuiderGroup');
        $this->createCrudControllerPermissions('guide_guide-view-history');
        $this->createOwnGroupPermission('guide_guide-view-history');
        $perm = $this->getPermission('guide_guide-view-history_group');
        if($perm){
            $auth = $this->getAuth();
            $auth->addChild($auth->getRole('GuiderGroup'), $perm);
        }
        $this->addSeeControllerPermission('guide_guide-view-history', ['GuiderAdmin']);
    }

    public function safeDown()
    {
        echo "m201016_105402_guiderAdmin_guide_view_history_perm cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
