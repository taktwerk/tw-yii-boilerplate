<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%guide_data}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%guide}}`
 * - `{{%client_model_type}}`
 */
class m200824_104920_create_guide_data_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%guide_data}}', [
            'id' => $this->primaryKey(),
            'guide_id' => $this->integer(11)->notNull(),
            'client_model_type_id' => $this->integer(11)->notNull(),
            'type_ref' => $this->string(255)->notNull()->comment('Id of the record in model type table'),
            'amount' => $this->decimal(8,2)->null(),
        ]);
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\guide"}';
        
        $this->addCommentOnTable('{{%guide_data}}',$comment);
        // creates index for column `guide_id`
        $this->createIndex(
            '{{%idx-guide_data-guide_id}}',
            '{{%guide_data}}',
            'guide_id'
        );

        // add foreign key for table `{{%guide}}`
        $this->addForeignKey(
            '{{%fk-guide_data-guide_id}}',
            '{{%guide_data}}',
            'guide_id',
            '{{%guide}}',
            'id',
            'CASCADE'
        );

        // creates index for column `client_model_type_id`
        $this->createIndex(
            '{{%idx-guide_data-client_model_type_id}}',
            '{{%guide_data}}',
            'client_model_type_id'
        );

        // add foreign key for table `{{%client_model_type}}`
        $this->addForeignKey(
            '{{%fk-guide_data-client_model_type_id}}',
            '{{%guide_data}}',
            'client_model_type_id',
            '{{%client_model_type}}',
            'id',
            'CASCADE'
        );
        $this->createIndex('idx_guide_data_deleted_at_guide_id', '{{%guide_data}}', ['deleted_at','guide_id']);
        $this->createIndex('idx_guide_data_deleted_at_client_model_type_id', '{{%guide_data}}', ['deleted_at','client_model_type_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    $this->dropIndex('idx_guide_data_deleted_at_guide_id', '{{%guide_data}}');
    $this->dropIndex('idx_guide_data_deleted_at_client_model_type_id', '{{%guide_data}}');
            // drops foreign key for table `{{%guide}}`
        $this->dropForeignKey(
            '{{%fk-guide_data-guide_id}}',
            '{{%guide_data}}'
        );

        // drops index for column `guide_id`
        $this->dropIndex(
            '{{%idx-guide_data-guide_id}}',
            '{{%guide_data}}'
        );

        // drops foreign key for table `{{%client_model_type}}`
        $this->dropForeignKey(
            '{{%fk-guide_data-client_model_type_id}}',
            '{{%guide_data}}'
        );

        // drops index for column `client_model_type_id`
        $this->dropIndex(
            '{{%idx-guide_data-client_model_type_id}}',
            '{{%guide_data}}'
        );

        $this->dropTable('{{%guide_data}}');
    }
}
