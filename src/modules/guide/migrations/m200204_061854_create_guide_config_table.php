<?php

use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%guide_config}}`.
 */
class m200204_061854_create_guide_config_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%guide_config}}', [
            'id' => $this->primaryKey(),
            'pdf_template_path' => $this->string(255),
        ]);
        $this->addCommentOnTable('{{%guide_config}}', '{"base_namespace":"app\\\\modules\\\\guide"}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%guide_config}}');
    }
}
