<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200204_101310_alert_guide_config_template_path extends TwMigration
{

    public function up()
    {
        $tbl = '{{%guide_config}}';
        $this->alterColumn($tbl, 'pdf_template_path', $this->getDb()
            ->getSchema()
            ->createColumnSchemaBuilder("ENUM('@app/modules/guide/views/pdf/1-column','@app/modules/guide/views/pdf/2-column')"));
    }

    public function down()
    {
        echo "m200204_101310_alert_guide_config_template_path cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
