<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200908_083458_add_qr_scan_to_guide_settings extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%guide_setting}}', 'guide_qrscan', $this->string(255)->null());
    }

    public function down()
    {
        echo "m200908_083458_add_qr_scan_to_guide_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
