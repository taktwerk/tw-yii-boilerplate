<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m190328_000000_guide_assets extends TwMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{%guide_asset}}',
            [
                'id' => $this->primaryKey(),
                'client_id' => $this->integer()->null(),
                'name' => $this->string(255)->notNull(),
                'asset_file' => $this->string(255)->null(),
                'asset_html' => $this->text()->null(),
            ]
        );
        $this->addForeignKey('fk_guide_asset_client_id', '{{%guide_asset}}', 'client_id', '{{%client}}', 'id', 'cascade');
        $this->createIndex('guide_asset_name_idx', '{{%guide_asset}}', ['name']);


        $this->createTable(
            '{{%guide_asset_pivot}}',
            [
                'id' => $this->primaryKey(),
                'guide_id' => $this->integer()->notNull(),
                'guide_asset_id' => $this->integer()->notNull(),
            ]
        );
        $this->addForeignKey('fk_guide_asset_pivot_client_id', '{{%guide_asset_pivot}}', 'guide_id', '{{%guide}}', 'id', 'cascade');
        $this->addForeignKey('fk_guide_asset_pivot_template_id', '{{%guide_asset_pivot}}', 'guide_asset_id', '{{%guide_asset}}', 'id', 'cascade');

        $this->dropColumn('{{%guide}}', 'pdf_file');
    }

    public function safeDown()
    {
        $this->dropTable('{{%guide_asset_pivot}}');
        $this->dropTable('{{%guide_asset}}');

        $this->addColumn('{{%guide}}', 'pdf_file', $this->string(255));
    }
}