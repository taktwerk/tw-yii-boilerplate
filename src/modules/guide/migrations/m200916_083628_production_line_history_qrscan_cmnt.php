<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200916_083628_production_line_history_qrscan_cmnt extends TwMigration
{
    public function up()
    {
        $this->addCommentOnColumn('{{%production_line_history}}', 'reference_qrscan', '');
    }

    public function down()
    {
        echo "m200916_083628_production_line_history_qrscan_cmnt cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
