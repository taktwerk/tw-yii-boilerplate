<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m201106_060950_guide_setting_file_meta_column extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%guide_setting}}', 'apk_file_filemeta', $this->text());
    }

    public function down()
    {
        echo "m201106_060950_guide_setting_file_meta_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
