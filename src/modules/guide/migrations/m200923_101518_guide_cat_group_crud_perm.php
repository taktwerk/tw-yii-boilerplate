<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200923_101518_guide_cat_group_crud_perm extends TwMigration
{
    public function up()
    {
        $this->createCrudControllerPermissions('guide_guide-category-group');
        $this->addSeeControllerPermission('guide_guide-category-group','GuiderViewer');
    }

    public function down()
    {
        echo "m200923_101518_guide_cat_group_crud_perm cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
