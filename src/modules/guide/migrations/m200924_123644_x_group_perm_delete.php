<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;
use yii\db\Query;
use yii\db\Expression;

class m200924_123644_x_group_perm_delete extends TwMigration
{

    public function up()
    {
    (new Query())
            ->createCommand()
            ->delete('auth_item_child',
                    ['OR',
                        ['like','child',new Expression("'x\_guide\_%\_own%'")],
                        ['like','child',new Expression("'x\_guide\_%\_group%'")]])
                    ->execute();
        
    (new Query)
    ->createCommand()
    ->delete('auth_item_child',
            ['OR',['like','parent',new Expression("'x\_guide\_%\_own%'")],
                ['like','parent',new Expression("'x\_guide\_%\_group%'")]])
            ->execute();
    }

    public function down()
    {
        echo "m200924_123644_x_group_perm_delete cannot be reverted.\n";

        return false;
    }

    /*
     * // Use safeUp/safeDown to run migration code within a transaction
     * public function safeUp()
     * {
     * }
     *
     * public function safeDown()
     * {
     * }
     */
}
