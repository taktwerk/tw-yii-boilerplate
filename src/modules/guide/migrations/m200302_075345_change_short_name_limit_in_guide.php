<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200302_075345_change_short_name_limit_in_guide extends TwMigration
{
    public function up()
    {
        $query = <<<EOF
            ALTER TABLE `guide` CHANGE `short_name` `short_name` CHAR(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL; 
EOF;
        $this->execute($query);

    }

    public function down()
    {
        echo "m200302_075345_change_short_name_limit_in_guide cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
