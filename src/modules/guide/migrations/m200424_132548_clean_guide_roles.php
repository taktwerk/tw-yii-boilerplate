<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200424_132548_clean_guide_roles extends TwMigration
{
    /**
     * @return bool|void|null
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $auth = $this->getAuth();
        $guider_admin = $auth->getRole('GuiderAdmin');
        $guider_viewer = $auth->getRole('GuiderViewer');
        $backend = $auth->getRole('Backend');

        $auth->removeChildren($guider_admin);
        $auth->removeChildren($guider_viewer);

        $this->createPermission('guide_explore_search', 'Guide Explore Search', ['GuiderViewer']);
        $this->createCrudPermissions('guide_explore', 'Guider Explore', ['GuiderViewer']);
        $this->createPermission('guide_explore_guide', 'Guider Explore Guide', ['GuiderViewer']);

        $auth->addChild($guider_viewer, $backend);
        $auth->addChild($guider_admin, $guider_viewer);

        $this->createPermission('guide_guide-step_sort-item', 'Guide Step Sort Item', 'GuiderAdmin');
        $this->createPermission('guide_guide_generate-pdf', 'Generate Guide PDF', ['GuiderAdmin']);

        $this->createCrudControllerPermissions('guide_guide-asset');
        $this->createCrudControllerPermissions('guide_guide-category');
        $this->createCrudControllerPermissions('guide_guide-template');
        $this->addAdminControllerPermission('guide_guide','GuiderAdmin');
        $this->addAdminControllerPermission('guide_guide-step','GuiderAdmin');
        $this->addAdminControllerPermission('guide_guide-asset','GuiderAdmin');
        $this->addAdminControllerPermission('guide_guide-asset-pivot','GuiderAdmin');
        $this->addAdminControllerPermission('guide_guide-category-binding','GuiderAdmin');
        $this->addAdminControllerPermission('guide_guide-category','GuiderAdmin');
        $this->addAdminControllerPermission('guide_guide-setting','GuiderAdmin');
        $this->addAdminControllerPermission('guide_guide-template','GuiderAdmin');
        $this->addSeeControllerPermission('guide_guide','GuiderViewer');
        $this->addSeeControllerPermission('guide_guide-step','GuiderViewer');
        $this->addSeeControllerPermission('guide_guide-asset','GuiderViewer');
        $this->addSeeControllerPermission('guide_guide-asset-pivot','GuiderViewer');
        $this->addSeeControllerPermission('guide_guide-category-binding','GuiderViewer');
        $this->addSeeControllerPermission('guide_guide-category','GuiderViewer');
        $this->addSeeControllerPermission('guide_guide-setting','GuiderViewer');
        $this->addSeeControllerPermission('guide_guide-template','GuiderViewer');

    }

    public function down()
    {
        echo "m200424_132548_clean_GuideAdmin cannot be reverted.\n";

//        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
