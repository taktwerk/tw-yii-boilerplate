<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200528_112920_add_see_to_guider_admin extends TwMigration
{
    /**
     * @return bool|void|null
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->createCrudControllerPermissions('guide_guide-setting');
        $this->addAdminControllerPermission('guide_guide-setting','GuiderAdmin');
        $this->addSeeControllerPermission('guide_guide-setting','GuiderAdmin');
    }

    public function down()
    {
        echo "m200528_112920_add_see_to_guider_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
