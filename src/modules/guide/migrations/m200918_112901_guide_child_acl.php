<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200918_112901_guide_child_acl extends TwMigration
{
    public function up()
    {
        //$this->createCrudPermissions('guide_guide-child', 'Guide Data', ['GuiderAdmin']);
        $this->createCrudControllerPermissions('guide_guide-child');
        $this->addAdminControllerPermission('guide_guide-child','GuiderAdmin');
        $this->addSeeControllerPermission('guide_guide-child','GuiderViewer');
    }

    public function down()
    {
        echo "m200918_112901_guide_child_acl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
