<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200615_113545_guide_category_permission extends TwMigration
{
    public function up()
    {
        $this->createCrudPermissions('guide_guide-category', 'guide Guide Categories');
        $auth = $this->getAuth();
        $viewer = $auth->getRole('GuiderViewer');
        $groupPerm = $auth->getPermission('guide_guide-category_group');
        $auth->addChild($viewer, $groupPerm);
    }

    public function down()
    {
        echo "m200615_113545_guide_category_permission cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
