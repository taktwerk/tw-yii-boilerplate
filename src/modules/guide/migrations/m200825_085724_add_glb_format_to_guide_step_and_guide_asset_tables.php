<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200825_085724_add_glb_format_to_guide_step_and_guide_asset_tables extends TwMigration
{
    public function up()
    {
        $comment = <<<EOT
{"allowedExtensions":["jpg","jpeg","png","webp","mp4","mov","avi","mpeg","mpg","m4v","mkv","ogv", "mp3", "ogg", "wav", "m4a", "m4p", "aac", "opus", "midi", "flac", "wmv", "pdf", "stp", "gltf", "glb"]}
EOT;
        $this->addCommentOnColumn('{{%guide_step}}', 'attached_file', $comment);
    }

    public function down()
    {
        echo "m200825_085724_add_glb_format_to_guide_step_and_guide_asset_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
