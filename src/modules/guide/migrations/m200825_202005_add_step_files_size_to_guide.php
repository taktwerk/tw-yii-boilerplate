<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200825_202005_add_step_files_size_to_guide extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%guide}}', 'total_step_file_size',$this->string(255)->null());
        $this->addColumn('{{%guide}}', 'all_step_files_size',$this->string(255)->null());

    }

    public function down()
    {
        echo "m200825_202005_add_step_files_size_to_guide cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
