<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles adding columns to table `{{%guide}}`.
 */
class m200623_114700_add_is_collection_column_to_guide_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%guide}}', 'is_collection', $this->boolean()->after('preview_file')->defaultValue(0)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%guide}}', 'is_collection');
    }
}
