<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200219_095656_guide_setting_col_apk_file extends TwMigration
{
    public function up()
    {
        $tbl = '{{%guide_setting}}';
        $this->addColumn($tbl, 'apk_file', $this->string(255)
            ->null());
    }

    public function down()
    {
        echo "m200219_095656_guide_setting_col_apk_file cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
