<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191212_103314_add_revision_terms_and_counter_columns_to_guide extends TwMigration
{
    public function up()
    {
        $this->addColumn('{{%guide}}', 'revision_term', $this->string(10)->null());
        $this->addColumn('{{%guide}}', 'revision_counter', $this->integer()->notNull()->defaultValue(0));
    }

    public function down()
    {
        echo "m191212_103314_add_revision_terms_and_counter_columns_to_guide cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
