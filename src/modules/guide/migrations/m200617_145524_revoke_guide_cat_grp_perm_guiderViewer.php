<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200617_145524_revoke_guide_cat_grp_perm_guiderViewer extends TwMigration
{
    public function up()
    {
        $auth = $this->getAuth();
        $viewer = $auth->getRole('GuiderViewer');
        $groupPerm = $auth->getPermission('guide_guide-category_group');
        $auth->removeChild($viewer, $groupPerm);
    }

    public function down()
    {
        echo "m200617_145524_revoke_guide_cat_grp_perm_guiderViewer cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
