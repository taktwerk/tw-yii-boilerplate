<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;
use dektrium\user\helpers\Password;

class m180806_010000_guide_user extends TwMigration
{
    public function safeUp()
    {
        // Create role
        $role = $this->createRole('Customer', 'Viewer');

        // Create roles
        $this->createRole('GuiderViewer', 'Customer');
        $this->createRole('GuiderAdmin', 'GuiderViewer');

        $this->createCrudPermissions('guide_explore', 'Guider Explore', ['GuiderViewer']);
        $this->createPermission('guide_explore_guide', 'Guider Explore Guide', ['GuiderViewer']);
        $this->createCrudPermissions('guide_guide', 'Guider Guides', ['GuiderAdmin']);
        $this->createCrudPermissions('guide_guide-step', 'Guider Steps', ['GuiderAdmin']);
    }

    public function safeDown()
    {

        $this->removeRole('GuiderAdmin');
        $this->removeRole('GuiderViewer');
    }
}
