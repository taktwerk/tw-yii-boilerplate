<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200528_114213_add_indexes_to_guide_step extends TwMigration
{
    public function up()
    {
        $this->createIndex('guide_step_guide_id_deleted_at_idx', '{{%guide_step}}', ['deleted_at','guide_id']);
    }

    public function down()
    {
        echo "m200528_114213_add_indexes_to_guide_step cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
