<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m191218_211656_generate_pdf_permission extends TwMigration
{
    public function up()
    {
        $this->createPermission('guide_guide_generate-pdf', 'Generate Guide PDF', ['GuiderAdmin']);
    }

    public function down()
    {
        echo "m191218_211656_generate_pdf_permission cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
