<?php

use taktwerk\yiiboilerplate\modules\guide\models\Guide;
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200826_073142_remove_total_step_file_size_from_guide extends TwMigration
{
    public function up()
    {
        $this->dropColumn('{{%guide}}','total_step_file_size');

        foreach (Guide::find()->all() as $item) {
            /**@var $item Guide*/
            $item->updateSize();
        }
    }

    public function down()
    {
        echo "m200826_073142_remove_total_step_file_size_from_guide cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
