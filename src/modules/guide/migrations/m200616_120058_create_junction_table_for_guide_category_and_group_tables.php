<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%guide_category_group}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%guide_category}}`
 * - `{{%group}}`
 */
class m200616_120058_create_junction_table_for_guide_category_and_group_tables extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%guide_category_group}}', [
            'id' => $this->primaryKey(),
            'guide_category_id' => $this->integer(),
            'group_id' => $this->integer(),
        ]);
        $this->addCommentOnTable('{{%guide_category_group}}', '{"base_namespace":"app\\\\modules\\\\guide"}');
        // creates index for column `guide_category_id`
        $this->createIndex(
            '{{%idx-guide_category_group-guide_category_id}}',
            '{{%guide_category_group}}',
            'guide_category_id'
        );

        // add foreign key for table `{{%guide_category}}`
        $this->addForeignKey(
            '{{%fk-guide_category_group-guide_category_id}}',
            '{{%guide_category_group}}',
            'guide_category_id',
            '{{%guide_category}}',
            'id',
            'CASCADE'
        );

        // creates index for column `group_id`
        $this->createIndex(
            '{{%idx-guide_category_group-group_id}}',
            '{{%guide_category_group}}',
            'group_id'
        );

        // add foreign key for table `{{%group}}`
        $this->addForeignKey(
            '{{%fk-guide_category_group-group_id}}',
            '{{%guide_category_group}}',
            'group_id',
            '{{%group}}',
            'id',
            'CASCADE'
        );
        $this->createIndex('idx_guide_category_group_deleted_at_guide_category_id', '{{%guide_category_group}}', ['deleted_at','guide_category_id']);
        $this->createIndex('idx_guide_category_group_deleted_at_group_id', '{{%guide_category_group}}', ['deleted_at','group_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    $this->dropIndex('idx_guide_category_group_deleted_at_guide_category_id', '{{%guide_category_group}}');
    $this->dropIndex('idx_guide_category_group_deleted_at_group_id', '{{%guide_category_group}}');
            // drops foreign key for table `{{%guide_category}}`
        $this->dropForeignKey(
            '{{%fk-guide_category_group-guide_category_id}}',
            '{{%guide_category_group}}'
        );

        // drops index for column `guide_category_id`
        $this->dropIndex(
            '{{%idx-guide_category_group-guide_category_id}}',
            '{{%guide_category_group}}'
        );

        // drops foreign key for table `{{%group}}`
        $this->dropForeignKey(
            '{{%fk-guide_category_group-group_id}}',
            '{{%guide_category_group}}'
        );

        // drops index for column `group_id`
        $this->dropIndex(
            '{{%idx-guide_category_group-group_id}}',
            '{{%guide_category_group}}'
        );

        $this->dropTable('{{%guide_category_group}}');
    }
}
