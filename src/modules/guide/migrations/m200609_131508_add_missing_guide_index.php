<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200609_131508_add_missing_guide_index extends TwMigration
{
    public function up()
    {
        $this->createIndex('idx_guide_deleted_at_client_id', '{{%guide}}', ['deleted_at','client_id']);
        $this->createIndex('idx_guide_deleted_at_template_id', '{{%guide}}', ['deleted_at','template_id']);
        $this->createIndex('idx_guide_deleted_at_protocol_template_id', '{{%guide}}', ['deleted_at','protocol_template_id']);

        $this->createIndex('idx_guide_asset_deleted_at_client_id', '{{%guide_asset}}', ['deleted_at','client_id']);

        $this->createIndex('idx_guide_asset_pivot_deleted_at_guide_id', '{{%guide_asset_pivot}}', ['deleted_at','guide_id']);
        $this->createIndex('idx_guide_asset_pivot_deleted_at_guide_asset_id', '{{%guide_asset_pivot}}', ['deleted_at','guide_asset_id']);

        $this->createIndex('idx_guide_category_deleted_at_client_id', '{{%guide_category}}', ['deleted_at','client_id']);

        $this->createIndex('idx_guide_category_binding_deleted_at_guide_category_id', '{{%guide_category_binding}}', ['deleted_at','guide_category_id']);
        $this->createIndex('idx_guide_category_binding_deleted_at_guide_id', '{{%guide_category_binding}}', ['deleted_at','guide_id']);

        $this->createIndex('idx_guide_setting_deleted_at_client_id', '{{%guide_setting}}', ['deleted_at','client_id']);

        $this->createIndex('idx_guide_tag_deleted_at_guide_id', '{{%guide_tag}}', ['deleted_at','guide_id']);

        $this->createIndex('idx_guide_template_deleted_at_client_id', '{{%guide_template}}', ['deleted_at','client_id']);
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
