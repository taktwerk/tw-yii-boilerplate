<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200327_141853_add_design_canvas_field_to_guide_step extends TwMigration
{
    public function up()
    {
        $comment = '{"isAdvanceMode":true,"loadImageFromField":"attached_file","keepMetaData":true}';
        $table = '{{%guide_step}}';
        $column = 'design_canvas';
        $this->addColumn($table, $column, 'varchar(255) DEFAULT NULL');
        $this->addCommentOnColumn($table, $column, $comment);

        $column = 'design_canvas_meta';
        $this->addColumn($table, $column, 'LONGTEXT NULL DEFAULT NULL');
    }

    public function down()
    {
        echo "m200327_141853_add_design_canvas_field_to_guide_step cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
