<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m201012_110016_guide_resume_history_perm_to_guideViewer extends TwMigration
{
    public function up()
    {
        $this->createPermission('guide_explore_save-history','Saves Guide History to resume on page like Explore',['GuiderViewer']);
    }

    public function down()
    {
        echo "m201012_110016_guide_resume_history_perm_to_guideViewer cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
