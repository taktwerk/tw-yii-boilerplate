<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200205_111804_guide_setting_client_id_col extends TwMigration
{

    public function up()
    {
        $tbl = '{{%guide_setting}}';
        $t = $this->db->beginTransaction();
        try {
            $this->renameColumn($tbl, 'pdf_template_path', 'pdf_template');
            $this->addColumn($tbl, 'client_id', $this->integer(11)
                ->notNull()
                ->after('pdf_template')
                ->unique());
            $this->addForeignKey('fk_guide_setting_client_id', $tbl, 'client_id', '{{%client}}', 'id', 'CASCADE');
            
            
            $this->alterColumn($tbl, 'pdf_template', $this->getDb()
                ->getSchema()
                ->createColumnSchemaBuilder("ENUM('1-column','2-column')")
                ->notNull());
            $t->commit();
        } catch (\Exception $e) {
            $t->rollBack();
            throw $e;
        }
    }

    public function down()
    {
        echo "m200205_111804_guide_setting_client_id_col cannot be reverted.\n";
        
        return false;
        
    }
    
    /*
     * // Use safeUp/safeDown to run migration code within a transaction
     * public function safeUp()
     * {
     * }
     *
     * public function safeDown()
     * {
     * }
     */
}
