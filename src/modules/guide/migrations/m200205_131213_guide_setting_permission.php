<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200205_131213_guide_setting_permission extends TwMigration
{
    public function up()
    {
        $this->createCrudControllerPermissions('guide_guide-setting');
        // admin permission assignment
        $this->addAdminControllerPermission('guide_guide-setting','GuiderAdmin');
    }

    public function down()
    {
        echo "m200205_131213_guide_setting_permission cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
