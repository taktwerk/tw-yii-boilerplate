<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%production_line_history}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%production_line}}`
 * - `{{%guide}}`
 */
class m200910_041857_create_production_line_history_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%production_line_history}}', [
            'id' => $this->primaryKey(),
            'production_line_id' => $this->integer(11)->notNull(),
            'guide_id' => $this->integer(11)->notNull(),
            'reference' => $this->string(255),
            'reference_qrscan' => $this->text()->comment('{"inputtype":"qrcode"}'),
        ]);
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\guide"}';
        $this->addCommentOnTable('{{%production_line_history}}',$comment);
        // creates index for column `production_line_id`
        $this->createIndex(
            '{{%idx-production_line_history-production_line_id}}',
            '{{%production_line_history}}',
            'production_line_id'
        );

        // add foreign key for table `{{%production_line}}`
        $this->addForeignKey(
            '{{%fk-production_line_history-production_line_id}}',
            '{{%production_line_history}}',
            'production_line_id',
            '{{%production_line}}',
            'id',
            'CASCADE'
        );

        // creates index for column `guide_id`
        $this->createIndex(
            '{{%idx-production_line_history-guide_id}}',
            '{{%production_line_history}}',
            'guide_id'
        );

        // add foreign key for table `{{%guide}}`
        $this->addForeignKey(
            '{{%fk-production_line_history-guide_id}}',
            '{{%production_line_history}}',
            'guide_id',
            '{{%guide}}',
            'id',
            'CASCADE'
        );
        $this->createIndex('idx_production_line_history_deleted_at_production_line_id', '{{%production_line_history}}', ['deleted_at','production_line_id']);
        $this->createIndex('idx_production_line_history_deleted_at_guide_id', '{{%production_line_history}}', ['deleted_at','guide_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    $this->dropIndex('idx_production_line_history_deleted_at_production_line_id', '{{%production_line_history}}');
    $this->dropIndex('idx_production_line_history_deleted_at_guide_id', '{{%production_line_history}}');
            // drops foreign key for table `{{%production_line}}`
        $this->dropForeignKey(
            '{{%fk-production_line_history-production_line_id}}',
            '{{%production_line_history}}'
        );

        // drops index for column `production_line_id`
        $this->dropIndex(
            '{{%idx-production_line_history-production_line_id}}',
            '{{%production_line_history}}'
        );

        // drops foreign key for table `{{%guide}}`
        $this->dropForeignKey(
            '{{%fk-production_line_history-guide_id}}',
            '{{%production_line_history}}'
        );

        // drops index for column `guide_id`
        $this->dropIndex(
            '{{%idx-production_line_history-guide_id}}',
            '{{%production_line_history}}'
        );

        $this->dropTable('{{%production_line_history}}');
    }
}
