<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200127_093601_add_permission_error_to_public extends TwMigration
{
    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $auth = $this->getAuth();
        $permission = $auth->getPermission('guide_explore_error');
        if(!$permission instanceof yii\rbac\Permission){
            $permission = $auth->createPermission('guide_explore_error');
            $permission->description = 'Frontend error';
            $auth->add($permission);
        }

        $user = $auth->getRole('Public');
        $auth->addChild($user, $permission);
    }

    public function down()
    {
        echo "m200127_093601_add_permission_error_to_public cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
