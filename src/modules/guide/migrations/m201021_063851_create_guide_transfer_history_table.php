<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%guide_transfer_history}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%guide}}`
 * - `{{%user}}`
 */
class m201021_063851_create_guide_transfer_history_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%guide_transfer_history}}', [
            'id' => $this->primaryKey(),
            'guide_view_history_id' => $this->integer(11)->notNull(),
            'to_user_id' => $this->integer(11)->notNull()->comment('This guide was transfered to this user'),
            'step' => $this->string(255)->comment('step on which the guide was transfered'),
        ]);
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\guide"}';
        $this->addCommentOnTable('{{%guide_transfer_history}}', $comment);
        // creates index for column `guide_view_history_id`
        $this->createIndex(
            '{{%idx-guide_transfer_history-guide_view_history_id}}',
            '{{%guide_transfer_history}}',
            'guide_view_history_id'
        );

        // add foreign key for table `{{%guide}}`
        $this->addForeignKey(
            '{{%fk-guide_transfer_history-guide_view_history_id}}',
            '{{%guide_transfer_history}}',
            'guide_view_history_id',
            '{{%guide_view_history}}',
            'id',
            'CASCADE'
        );

        // creates index for column `to_user_id`
        $this->createIndex(
            '{{%idx-guide_transfer_history-to_user_id}}',
            '{{%guide_transfer_history}}',
            'to_user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-guide_transfer_history-to_user_id}}',
            '{{%guide_transfer_history}}',
            'to_user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
        $this->createIndex('idx_guide_transfer_history_deleted_at_guide_view_history_id', '{{%guide_transfer_history}}', ['deleted_at','guide_view_history_id']);
        $this->createIndex('idx_guide_transfer_history_deleted_at_to_user_id', '{{%guide_transfer_history}}', ['deleted_at','to_user_id']);
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    $this->dropIndex('idx_guide_transfer_history_deleted_at_guide_view_history_id', '{{%guide_transfer_history}}');
    $this->dropIndex('idx_guide_transfer_history_deleted_at_to_user_id', '{{%guide_transfer_history}}');
            // drops foreign key for table `{{%guide}}`
        $this->dropForeignKey(
            '{{%fk-guide_transfer_history-guide_view_history_id}}',
            '{{%guide_transfer_history}}'
        );

        // drops index for column `guide_view_history_id`
        $this->dropIndex(
            '{{%idx-guide_transfer_history-guide_view_history_id}}',
            '{{%guide_transfer_history}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-guide_transfer_history-to_user_id}}',
            '{{%guide_transfer_history}}'
        );

        // drops index for column `to_user_id`
        $this->dropIndex(
            '{{%idx-guide_transfer_history-to_user_id}}',
            '{{%guide_transfer_history}}'
        );

        $this->dropTable('{{%guide_transfer_history}}');
    }
}
