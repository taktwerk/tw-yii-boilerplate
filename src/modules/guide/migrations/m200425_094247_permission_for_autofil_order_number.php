<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200425_094247_permission_for_autofil_order_number extends TwMigration
{
    public function up()
    {
        $this->createPermission('guide_guide-step_order-number', 'Guide Step Order Number', 'GuiderAdmin');
    }

    public function down()
    {
        echo "m200425_094247_permission_for_autofil_order_number cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
