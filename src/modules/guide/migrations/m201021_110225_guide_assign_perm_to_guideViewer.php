<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m201021_110225_guide_assign_perm_to_guideViewer extends TwMigration
{
    public function up()
    {
        $this->createPermission('guide_explore_assign-guide','Guide Assigning to other user',['GuiderViewer']);
        
        $this->createPermission('guide_explore_user_group','Shows assignable user list from the groups');
    }

    public function down()
    {
        echo "m201021_110225_guide_assign_perm_to_guideViewer cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
