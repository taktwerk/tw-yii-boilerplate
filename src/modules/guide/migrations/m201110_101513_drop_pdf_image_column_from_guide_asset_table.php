<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles dropping columns from table `{{%guide_asset}}`.
 */
class m201110_101513_drop_pdf_image_column_from_guide_asset_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%guide_asset}}', 'pdf_image');
        $this->dropColumn('{{%guide_asset}}', 'pdf_image_filemeta');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%guide_asset}}', 'pdf_image', $this->string());
        $this->addColumn('{{%guide_asset}}', 'pdf_image_filemeta', $this->text());
    }
}
