<?php
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

/**
 * Handles the creation of table `{{%guide_child}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%guide}}`
 * - `{{%guide}}`
 */
class m200623_104614_create_guide_child_table extends TwMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%guide_child}}', [
            'id' => $this->primaryKey(),
            'parent_guide_id' => $this->integer(11)->notNull(),
            'guide_id' => $this->integer(11)->notNull(),
            'order_number' => $this->integer(11)->null()->comment('{"sortRestrictAttributes":["parent_guide_id"]}'),
        ]);
        $this->addCommentOnTable('{{%guide_child}}', '{"base_namespace":"app\\\\modules\\\\guide"}');
        // creates index for column `parent_guide_id`
        $this->createIndex(
            '{{%idx-guide_child-parent_guide_id}}',
            '{{%guide_child}}',
            'parent_guide_id'
        );

        // add foreign key for table `{{%guide}}`
        $this->addForeignKey(
            '{{%fk-guide_child-parent_guide_id}}',
            '{{%guide_child}}',
            'parent_guide_id',
            '{{%guide}}',
            'id',
            'CASCADE'
        );

        // creates index for column `guide_id`
        $this->createIndex(
            '{{%idx-guide_child-guide_id}}',
            '{{%guide_child}}',
            'guide_id'
        );

        // add foreign key for table `{{%guide}}`
        $this->addForeignKey(
            '{{%fk-guide_child-guide_id}}',
            '{{%guide_child}}',
            'guide_id',
            '{{%guide}}',
            'id',
            'CASCADE'
        );
        $this->createIndex('idx_guide_child_deleted_at_parent_guide_id', '{{%guide_child}}', ['deleted_at','parent_guide_id']);
        $this->createIndex('idx_guide_child_deleted_at_guide_id', '{{%guide_child}}', ['deleted_at','guide_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    $this->dropIndex('idx_guide_child_deleted_at_parent_guide_id', '{{%guide_child}}');
    $this->dropIndex('idx_guide_child_deleted_at_guide_id', '{{%guide_child}}');
            // drops foreign key for table `{{%guide}}`
        $this->dropForeignKey(
            '{{%fk-guide_child-parent_guide_id}}',
            '{{%guide_child}}'
        );

        // drops index for column `parent_guide_id`
        $this->dropIndex(
            '{{%idx-guide_child-parent_guide_id}}',
            '{{%guide_child}}'
        );

        // drops foreign key for table `{{%guide}}`
        $this->dropForeignKey(
            '{{%fk-guide_child-guide_id}}',
            '{{%guide_child}}'
        );

        // drops index for column `guide_id`
        $this->dropIndex(
            '{{%idx-guide_child-guide_id}}',
            '{{%guide_child}}'
        );

        $this->dropTable('{{%guide_child}}');
    }
}
