<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200821_081035_alter_guide_namespace extends TwMigration
{
    public function up()
    {
        $comment ='{"base_namespace":"taktwerk\\\\yiiboilerplate\\\\modules\\\\guide"}';
        
        $this->addCommentOnTable('{{%guide}}',$comment);
        $this->addCommentOnTable('{{%guide_asset}}',$comment);
        $this->addCommentOnTable('{{%guide_asset_pivot}}',$comment);
        $this->addCommentOnTable('{{%guide_category}}',$comment);
        $this->addCommentOnTable('{{%guide_category_binding}}',$comment);
        $this->addCommentOnTable('{{%guide_category_group}}',$comment);
        $this->addCommentOnTable('{{%guide_child}}',$comment);
        $this->addCommentOnTable('{{%guide_setting}}',$comment);
        $this->addCommentOnTable('{{%guide_step}}',$comment);
        $this->addCommentOnTable('{{%guide_tag}}',$comment);
        $this->addCommentOnTable('{{%guide_template}}',$comment);

        //$this->addCommentOnTable('{{%guide_child}}',$comment);
    }

    public function down()
    {
        echo "m200821_081035_alter_guide_namespace cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
