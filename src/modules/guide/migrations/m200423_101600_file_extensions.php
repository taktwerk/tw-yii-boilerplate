<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200423_101600_file_extensions extends TwMigration
{
    public function up()
    {
        $comment = <<<EOT
{"allowedExtensions":["jpg","jpeg","png","webp"]}
EOT;
        $this->addCommentOnColumn('{{%guide}}', 'preview_file', $comment);

        $comment = <<<EOT
{"allowedExtensions":["jpg","jpeg","png","webp","mp4","mov","avi","mpeg","mpg","m4v","mkv","ogv"]}
EOT;
        $this->addCommentOnColumn('{{%guide_step}}', 'attached_file', $comment);
    
    }

    public function down()
    {
        echo "m200423_101600_file_extensions cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
