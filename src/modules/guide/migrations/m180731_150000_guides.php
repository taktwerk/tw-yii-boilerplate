<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m180731_150000_guides extends TwMigration
{
    public function safeUp()
    {
        $this->createTable(
            '{{%guide_template}}',
            [
                'id' => $this->primaryKey(),
                'client_id' => $this->integer()->notNull(),
                'name' => $this->string(45)->notNull(),
                'code' => $this->text(),
            ]
        );
        $this->addForeignKey('fk_guide_template_client_id', '{{%guide_template}}', 'client_id', '{{%client}}', 'id');


        $this->createTable(
            '{{%guide}}',
            [
                'id' => $this->primaryKey(),
                'client_id' => $this->integer()->notNull(),
                'template_id' => $this->integer()->notNull(),
                'short_name' => $this->char(4),
                'title' => $this->string(45)->notNull(),
                'description' => $this->text(),
                'preview_file' => $this->string(255),
                'pdf_file' =>$this->string(255)
            ]
        );
        $this->addForeignKey('fk_guide_client_id', '{{%guide}}', 'client_id', '{{%client}}', 'id');
        $this->addForeignKey('fk_guide_template_id', '{{%guide}}', 'template_id', '{{%guide_template}}', 'id');

        $this->createTable(
            '{{%guide_step}}',
            [
                'id' => $this->primaryKey(),
                'guide_id' => $this->integer()->notNull(),
                'step' => $this->integer()->notNull(),
                'title' => $this->string(45)->notNull(),
                'description_html' => $this->text(),
                'attached_file' => $this->string(255)
            ]
        );
        $this->addForeignKey('fk_guide_step_guide_id', '{{%guide_step}}', 'guide_id', '{{%guide}}', 'id');

    }

    public function safeDown()
    {
        $this->dropTable('{{%guide_step}}');
        $this->dropTable('{{%guide}}');
        $this->dropTable('{{%guide_template}}');
    }
}