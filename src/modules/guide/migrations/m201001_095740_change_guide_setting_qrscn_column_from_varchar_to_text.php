<?php

use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m201001_095740_change_guide_setting_qrscn_column_from_varchar_to_text extends TwMigration
{
    public function up()
    {
        $this->alterColumn('{{%guide_setting}}', 'guide_qrscan', $this->text()->comment('{"inputtype":"qrcode"}'));
    }

    public function down()
    {
        echo "m201001_095740_change_guide_setting_qrscn_column_from_varchar_to_text cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
