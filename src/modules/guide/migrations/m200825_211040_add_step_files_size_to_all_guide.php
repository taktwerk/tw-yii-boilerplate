<?php

use taktwerk\yiiboilerplate\modules\guide\models\Guide;
use yii\db\Schema;
use taktwerk\yiiboilerplate\TwMigration;

class m200825_211040_add_step_files_size_to_all_guide extends TwMigration
{
    public function up()
    {
        foreach (Guide::find()->all() as $item) {
            /**@var $item Guide*/
            $item->updateSize();
        }

    }

    public function down()
    {
        echo "m200825_211040_add_step_files_size_to_all_guide cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
