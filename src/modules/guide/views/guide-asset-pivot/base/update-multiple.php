<?php
//Generation Date: 16-Sep-2020 03:54:52pm
use yii\helpers\Html;
use Yii;
/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\guide\models\GuideAssetPivot $model
 * @var array $pk
 * @var array $show
 */

$this->title = 'Guide Asset Pivot ' . $model->id . ', ' . Yii::t('twbp', 'Edit Multiple');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('twbp','Guide Asset Pivots'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('twbp', 'Edit Multiple');
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body guide-asset-pivot-update">

        <h1>
            <?= \Yii::t('twbp','Guide Asset Pivot'); ?>
        </h1>

        <div class="crud-navigation">
        </div>

        <?php echo $this->render('_form', [
            'model' => $model,
            'pk' => $pk,
            'show' => $show,
            'multiple' => true,
            'useModal' => $useModal,
            'action' => $action,
        ]); ?>

    </div>
</div>