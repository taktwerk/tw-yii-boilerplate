<?php
//Generation Date: 16-Sep-2020 03:54:52pm
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\guide\models\search\GuideAssetPivot $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="guide-asset-pivot-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'guide_id') ?>

        <?= $form->field($model, 'guide_asset_id') ?>

        <?= $form->field($model, 'created_by') ?>

        <?= $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

        <?php // echo $form->field($model, 'local_created_at') ?>

        <?php // echo $form->field($model, 'local_updated_at') ?>

        <?php // echo $form->field($model, 'local_deleted_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
