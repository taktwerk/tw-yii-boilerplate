<?php
use yii\helpers\Html;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\components\ClassDispenser;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use taktwerk\yiiboilerplate\widget\form\ActiveForm;

//Generation Date: 10-Sep-2020 06:26:01am
$css = <<<EO
#scan-alert{
  -webkit-transition: background-color 0.18s;
  -moz-transition: background-color 0.18s;
  transition: background-color 0.18s;
}
EO;
$this->registerCss($css);
?>
<?php 
$this->beginBlock('custom-block');
\yii\bootstrap\Modal::begin([
    'toggleButton' => ['label' => 'Scan','class'=>"btn btn-primary"],
    'size' => \yii\bootstrap\Modal::SIZE_DEFAULT,
    'header' => '<h4>' . Yii::t('twbp', 'Looking for a barcode scan') . ':</h4>',
    'id' => 'bar-scan-modal',
    'clientOptions' => [
        'backdrop' => 'static',
    ],
]);
?>
<?php $model = new taktwerk\yiiboilerplate\modules\guide\models\ProductionLineHistory(); ?>
<?php 
$form = ActiveForm::begin([
    'id' => 'scan-form',
    'autoSave'=>false,
    'layout' => 'default',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
    'options' => [
        'name' => 'ProductionLineHistory',
    ],
]);
?>
<?php
echo $form->field($model, 'production_line_id')->widget(
    Select2::class,[
       // 'model'=>$model,
      //  'name'=>'Guide',
        'data' => taktwerk\yiiboilerplate\modules\guide\models\ProductionLine::find()->count() > 50 ? null : ArrayHelper::map(taktwerk\yiiboilerplate\modules\guide\models\ProductionLine::find()->all(), 'id', 'toString'),
        'initValueText' => taktwerk\yiiboilerplate\modules\guide\models\ProductionLine::find()->count() > 50 ? \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\modules\guide\models\ProductionLine::find()->andWhere(['id' => $model->production_line_id])->all(), 'id', 'toString') : '',
        'options' => [
            'placeholder' => Yii::t('twbp', 'Select a Production Line...'),
            'id' => 'scan_production_line_id',
        ],
        'pluginOptions' => [
            'allowClear' => false,
            (taktwerk\yiiboilerplate\modules\guide\models\ProductionLine::find()->count() > 50 ? 'minimumInputLength' : '') => 3,
            (taktwerk\yiiboilerplate\modules\guide\models\ProductionLine::find()->count() > 50 ? 'ajax' : '') => [
                'url' => \yii\helpers\Url::to(['list']),
                'dataType' => 'json',
                'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'ProductionLine\'
                                        };
                                    }')
            ],
        ],
        'pluginEvents' => [
            "change" => "function() {
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
            Url::toRoute('/guide/production-line/entry-details?id=', true) .
            "' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#production_line_id_well').html(json.data);
                                                //$('#production_line_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }",
        ],
    ]);
?>
<?php 
$guideHistoryUrl = Url::toRoute(['/guide/production-line-history/scan-guide']);
echo taktwerk\yiiboilerplate\widget\barcodescanner\BarcodeScanner::widget([
    'scanButtonOptions' => [
        'id' => 'barscan-btn',
        'class' => 'btn btn-warning',
        'style'=>'display:none'
    ],
    'pluginOptions' => [
        "scanButtonKeyCode" => false,
        "scanButtonLongPressTime" => 500,
        "timeBeforeScanTest" => 100,
        "avgTimeByChar" => 30,
        "minLength" => 3,
        "suffixKeyCodes" => [
            9,
            13
        ],
        "prefixKeyCodes" => [],
        "ignoreIfFocusOn" => 'input',
        "stopPropagation" => false,
        "preventDefault" => false,
        "reactToKeydown" => true,
        "reactToPaste" => true,
        "singleScanQty" => 1,
        "reactToKeyDown" => true,
        "onScan"=>new JsExpression(
<<<EOT
function(sScanned, iQty){
    var \$form = $('#scan-form');
    var fdata = \$form.data("yiiActiveForm");
    $.each(fdata.attributes, function() {
                this.status = 3;
          });
    \$form.yiiActiveForm("validate");
    $('#scan-ajax-errors').empty();
    setTimeout(function(){
            var isProdLineValid = ($('div.field-scan_production_line_id.has-error').length==0);
            if(!isProdLineValid){
                    return;
            }
            var t = $("<span></span>").text(sScanned);
            var prod_line_id = $('#scan_production_line_id').val();
            var html = '<div class="alert alert-info alert-dismissible" id="scan-alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <h2></strong><span>'+t.html()+'<h2></div>';
            $("#scan-alert").remove();
            $("#bar-scan-modal").prepend(html);
            setTimeout(function(){
                $.ajax({
                url:"$guideHistoryUrl",
                method:'post',
                data:{q:sScanned,prod_line_id:prod_line_id},
                success:function(q){
                        if(q.success==true){
                            $('#scan-alert').removeClass('alert-info alert-danger').addClass('alert-success');
                        } else if(q.success==false){
                            $('#scan-alert').removeClass('alert-info alert-success').addClass('alert-danger');
                            var liHtml='';
                            q.errors.forEach(function(f){
                              var errText = $("<span></span>").text(f);console.log(errText);
                              liHtml += $("<li class='list-group-item text-danger'></li>").text(errText.text())[0].outerHTML;
                            });
                            var errorUl = $("<ul class='list-group'></ul>").html(liHtml);
                            $('#scan-ajax-errors').html(errorUl[0].outerHTML);
                        }
                    }
                });
            
        },400);
    },200);
}
EOT
)
    ],
    'hidden' => false
]);
?>
<div class="clearfix"></div>
<div id="scan-ajax-errors"></div>
<?php 
ClassDispenser::getMappedClass(ActiveForm::class)::end();
\yii\bootstrap\Modal::end();
$this->endBlock();?>

<?php 
$js = <<<EOT
$('#bar-scan-modal').on('show.bs.modal', function (e) {
     $("#scan-alert").remove();
     $('#scan-ajax-errors').empty();
     if($('#barscan-btn').data('scanEnabled')!=1){
        $('#barscan-btn').click();
     }
});
$('#bar-scan-modal').on('hide.bs.modal', function (e) {
     $("#scan-alert").remove();
     $('#scan-ajax-errors').empty();
     if($('#barscan-btn').data('scanEnabled')!=0){
        $('#barscan-btn').click();
     }
});

EOT;
$this->registerJs($js);
require(__DIR__ . DIRECTORY_SEPARATOR . 'base' . DIRECTORY_SEPARATOR . 'index.php');