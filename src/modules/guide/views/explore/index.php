<?php
/**
 * @var \taktwerk\yiiboilerplate\modules\guide\models\GuideCategory $tag
 */
$this->title = Yii::t('twbp', 'Categories') . ' - ' . Yii::t('twbp', 'Guider');
use yii\helpers\Html;
?>

<div class="py-4">
    <div class="py-5 container">
        <?php if ($pageContentType === 'no-category' && $guidesWithoutCategory) {
            $this->title = Yii::t('twbp', 'No Category');
        ?>
            <div class="row">
                <div class="col-md-12 pb-2">
                    <h1 class=""><?=Yii::t('twbp', 'No Category')?></h1>
                </div>
            </div>
            <div>
                <div>
                    <?php foreach ($guidesWithoutCategory as $guide):?>
                        <?=$this->render('blocks/guide', ['guide' => $guide]);?>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php } else { ?>
            <div class="row">
                <div class="col-md-12 pb-2">
                    <h1 class=""><?=Yii::t('twbp', 'Categories')?></h1>
                </div>
            </div>
            <div class="list">
                <?php foreach ($tags as $tag): ?>
                    <?php if (count($tag->guideCategoryBindings) == 0) {
                        continue;
                    } ?>
                    <?= Html::a(
                            '<div class="row card m-0 mb-4 p-2" style="flex-direction: unset;">
                                        <div class="col-10">
                                            <h4 style="font-weight: 500;">
                                                ' . $tag->name . '
                                            </h4>
                                        </div>
                                        <div class="col-2" style="text-align: right;">
                                            <h4 class="text-muted">
                                                ' . count($tag->guideCategoryBindings) . '
                                            </h4>
                                        </div>
                                    </div>',
                            ['explore/guides', 'guideCategoryId' => $tag->id],
                            [
                              'title' => Yii::t('language', 'Translate'),
                              'data-pjax' => '0',
                            ]
                     );
                    ?>
                <?php endforeach; ?>
                <?php if(count($guidesWithoutCategory)): ?>
                    <?= Html::a(
                            '<div class="row card m-0 mb-4 p-2" style="flex-direction: unset;">
                                        <div class="col-10">
                                            <h4 style="font-weight: 500;">
                                                ' . Yii::t('twbp', 'No Category') . '
                                            </h4>
                                        </div>
                                        <div class="col-2" style="text-align: right;">
                                            <h4 class="text-muted">
                                                ' . count($guidesWithoutCategory) . '
                                            </h4>
                                        </div>
                                    </div>',
                            ['explore/guides', 'guideCategoryId' => 0],
                            [
                              'title' => Yii::t('language', 'Translate'),
                              'data-pjax' => '0',
                            ]
                     );
                    ?>
                <?php endif; ?>
            </div>
        <?php } ?>
    </div>
</div>