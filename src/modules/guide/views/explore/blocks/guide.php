<?php

use yii\helpers\Url;
use \yii\helpers\Html;
use taktwerk\yiiboilerplate\modules\guide\models\Guide;
use taktwerk\yiiboilerplate\modules\guide\models\GuideAssetPivot;

/** @var Guide $guide */
/** @var GuideAssetPivot $pivot */

$guide = isset($binding) ? $binding->guide : $guide;
if (empty($guide)) {
    return;
}
$guideCategoryId = null;
if(!empty($guideCategory) && !empty($guideCategory->id)){
    $guideCategoryId = $guideCategory->id;
}
$guideViewLink = Url::to(['/guide/explore/guide', 'id' => $guide->id,'parent'=>($parentGuide)?$parentGuide->id:null]);
?>

<div class="card mt-0">
    <div class="row">
        <div class="col-6 pr-0">
            <a href="<?= $guideViewLink?>">
                <img class="w-100 rounded-left" src="<?=$guide->getPicture()?>">
                <div class="shortname-label badge badge-secondary"><?=$guide->short_name?></div>
            </a>
        </div>

        <div class="col-6 align-self-stretch">
            <div class="card-block">
                <h2 class=""><?=$guide->title?></h2>
                <p class=""><?=nl2br($guide->description)?></p>
                <?php if($durationTime = $guide->durationTime()) : ?>
                    <p class=""><?=Yii::t('twbp', 'Duration').': '.$durationTime?></p>
                <?php endif; ?>
                
                <?php if($stepCount = $guide->stepCount()) : ?>
                    <p class=""><?=Yii::t('twbp', 'Total Steps').': '.$stepCount?></p>
                <?php endif; ?>
                <?php
                    echo Html::a(Html::tag('i', '', [
                        'class' => 'fa fa-fw fa-play-circle'
                    ]) .' ' . (($guide->is_collection)?Yii::t('twbp', 'Show Collection'): Yii::t('twbp', 'Show Guide')), 
                         $guideViewLink, 
                        ['class' => 'btn btn-primary d-none d-md-table mb-3']);
                ?>
               
                <?php
                    echo Html::a(Html::tag('i', '', [
                        'class' => 'fa fa-fw fa-play-circle'
                    ]).' ' . Yii::t('twbp', 'Show Guide'), $guideViewLink, 
                        ['class' => 'btn btn-primary btn-sm d-md-none mb-3']);
                ?> 
                
                <?php if ($guide->protocolTemplate && Yii::$app->getUser()->can('protocol_protocol_quickstart')) : ?>
                <?php
                    echo Html::a(Html::tag('i', '', [
                        'class' => 'fa fa-fw fa-list'
                    ]).' ' . Yii::t('twbp', 'New Protocol'), [

                        '/protocol/protocol/quickstart',
                        'templateId' => $guide->protocolTemplate->id,
                        'referenceModel' => get_class($guide),
                        'referenceId' => $guide->id
                    ], [
                        'class' => 'btn d-none d-md-table mb-3',
                        'target' => "_blank"
                    ]);
                ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
