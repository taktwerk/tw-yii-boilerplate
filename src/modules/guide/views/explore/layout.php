<?php
use dmstr\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use taktwerk\yiiboilerplate\modules\guide\assets\ExploreGuideAsset;
use taktwerk\yiiboilerplate\modules\guide\models\Guide;

/* @var $this \yii\web\View */
/* @var $content string */

//$this->render('@taktwerk-boilerplate/views/blocks/raven');

// Disable debug bar
if (class_exists('yii\debug\Module')) {
    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
}
// Remove jquery from asset
$this->assetBundles = [];
ExploreGuideAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
		<?php $this->head() ?>
        <!-- https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons -->
        <!-- Theme style -->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="/js/html5shiv.js"></script>
            <script src="/js/respond.min.js"></script>
        <![endif]-->
        
    </head>

    <body>
    <div class="wrapper">
        <nav class="navbar bg-rose fixed-top">
            <div class="container">
                <a class="navbar-brand" href="<?=Url::to('/guide/explore')?>"><?=Yii::t('twbp', 'Guider')?></a>
                <?php if(isset($this->params['guide']) || isset($this->params['prevGuideId']) ||  isset($this->params['nextGuideId'])): ?>
                    <ul class="navbar-nav">
                    <li class="nav-item" id="prev-guide-nav" style="display: none">
                    <?php echo Html::a('<i class="material-icons">skip_previous</i>',
                        ['/guide/explore/guide','id' => $this->params['prevGuideId']
                            ,'parent'=>($this->params['parentGuideId'])?$this->params['parentGuideId']:null],[
                                'class'=>'nav-link cursor-pointer',
                                'id'=>'slickprevG-a'
                            ])?>
                    </li>
                    <li class="nav-item" id="prev-guidestep-nav"  style="display: none">
                            <a id="slickprev" class="nav-link cursor-pointer"><i class="material-icons">arrow_left</i></a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item" id="next-guidestep-nav"  style="display: none">
                            <a id="slicknext" class="nav-link cursor-pointer"><i class="material-icons">arrow_right</i></a>
                        </li>
                        <li class="nav-item"  id="next-guide-nav"  style="display: none">
                        <?php echo Html::a('<i class="material-icons">skip_next</i>',
                        ['/guide/explore/guide','id' => $this->params['nextGuideId']
                            ,'parent'=>($this->params['parentGuideId'])?$this->params['parentGuideId']:null],[
                                'class'=>'nav-link cursor-pointer',
                                'id'=>'slicknextG-a'
                            ])?>
                        </li>
                    </ul>
                    <ul class="navbar-nav mr-auto">
                    	
                    	
                    	<li class="nav-item" id="restart-guidestep-nav" style="display: none">
                    	<?php if($this->params['parentGuideId']){?>
                    	<?php 
                    	$parentG = Guide::find()->where(['guide.id'=>$this->params['parentGuideId']])->one();
                    	if($parentG){
                    	    $childG = $parentG->getGuideChildren0()->orderBy('order_number asc')->one();
                    	    if($childG){
                    	        echo Html::a('<i class="material-icons">refresh</i>',['/guide/explore/guide',
                    	            'id'=>$childG->guide_id,'parent'=>$parentG->id,'resume'=>0],
                    	            ['title'=>\Yii::t('twbp','Restarts the collection from first guide'),'class'=>'text-white']);
                    	    }
                    	}
                    	?>
                        <?php }else{?>
                        	<a id="slickRestart" class="nav-link cursor-pointer"><i class="material-icons">refresh</i></a>
                        <?php }?>
                        </li>
                        
                    </ul>
                <?php endif; ?>
                <button type="submit" class="btn btn-white btn-raised btn-fab btn-round ml-auto search-icon" data-toggle="collapse" data-target="#navbarSearch">
                    <i class="material-icons">search</i>
                </button>
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation" data-target="#navbarSupportedContent">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="<?=Url::to('/guide/explore')?>"><?=Yii::t('twbp', 'Guides') ?></a>
                        </li>
                        <?php if (Yii::$app->hasModule('feedback')) : ?>
                            <li class="nav-item">
                                <?=
                                \taktwerk\yiiboilerplate\modules\feedback\widgets\FeedbackButton::widget([
                                    'btn_class'=>'nav-link'
                                ]);
                                ?>
                            </li>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->can('Authority')): ?>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><?=Yii::t('twbp', 'Favorites')?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><?=Yii::t('twbp', 'Scan')?></a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
                <form id="navbarSearch" class="w-100 collapse" method="GET" action="<?=\yii\helpers\url::to(['/guide/explore/search'])?>">
                    <div class="w-100 form-group has-white bmd-form-group">
                        <input type="text" autocomplete="off" class="form-control typeahead" placeholder="Search" name="term" data-url="<?=\yii\helpers\Url::toRoute(['/guide/explore/search'])?>">
                    </div>
                </form>
            </div>
        </nav>
        <div id="guider-content">
<?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="flash-msg-sec text-right">
        <div class="alert alert-success alert-dismissible fade show flash-msg-alert" title="<?php echo \Yii::t('twbp','Guide resumed from where you left off');?>" role="alert">
          <strong><?php echo \Yii::$app->session->getFlash('success'); ?></strong>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    </div>
<?php endif;?>
<?php if (Yii::$app->session->hasFlash('error')): ?>
    <div class="flash-msg-sec text-right">
        <div class="alert alert-danger alert-dismissible fade show flash-msg-alert" title="<?php echo \Yii::t('twbp','Guide resumed from where you left off');?>" role="alert">
          <strong><?php echo \Yii::$app->session->getFlash('error'); ?></strong>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    </div>
<?php endif;?>
            <?php
                $this->beginBody();
                \taktwerk\yiiboilerplate\widget\assets\Model3dAssets::register($this);
            ?>
            <?= $content ?>
        </div>
        <footer class="footer footer-default">
            <div class="container">
                <div class="copyright float-right">
                    <?= \taktwerk\yiiboilerplate\components\Helper::version() ?> ©
                    <a href="https://taktwerk.ch" target="_blank">taktwerk.ch</a> | 2012-<?= date("Y", time()) ?>
                </div>
            </div>
        </footer>
    </div>
    <!-- ./wrapper -->

        <?php $this->endBody(); ?>
    </body>
    </html>
<?php
$collectionUrl=Url::to(['/guide/explore/guide']);
$js = <<<JS
$(document).ready(function() {
    var searchField = $('.typeahead');
    
    var searchEngine = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('term'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: searchField.data('url') + '?term=%QUERY%',
            wildcard: '%QUERY',
            transform: function(data) {
                return data && data[0] ? data[0].models : [];
            }
        }
    });
    
    // Clicking on the search icon should focus on the search field
    $('.search-icon').on('click', function() {
        setTimeout(function() {
            $('input[name="term"]').focus().click();
        }, 500);
    });
    
    searchField.typeahead({
        hint: true,
        highlight: true,
        autoselect: false,
        minLength: 3,
         classNames: {
            input: 'w-100',
            menu: 'tt-menu shadow bg-light',
        }
    },{
        source: searchEngine.ttAdapter(),
        
        name: 'guider-search',
        
        displayKey: function(row) {
            return row.data.short_name;
        },
        
        // the key from the array we want to display (name,id,email,etc...)
        templates: {
            empty: [
                '<div class="list-group search-results-dropdown">'
                + '<div class="list-group-item">' + searchField.data('empty') + '</div>'
                + '</div>'
            ],
            header: [
                ''
            ],
            suggestion: function (data) {
                /* var parentHtml = '';
                if(data.data.parent_guides.length>0){
                   data.data.parent_guides.forEach(function(val,index){
                    parentHtml +='<span class="text-warning"> Collection: </span><a href="{$collectionUrl}?id='+data.data.id+'&parent='+val.id+'">'+val.short_name+' '+ val.title +'</a>';
                        //console.log(parentHtml);
                   });
                } */

                var html = '<a href="' + data.data.url + '" title="' + data.data.title + '">'
                    + '<div class="row"><div class="col-4 col-md-2 pt-2 pl-4 pb-2"><img class="preview-img w-100" src="' + data.data.preview + '" title="' + data.data.title + '"/></div><div class="col-8 col-md-10 pl-0 pt-2">'
                    + data.data.short_name + '<br />' + data.data.title + '<br /><p>' + data.data.description + '</p></div></div>' +
                 '</a>';
                return html;
            }
        }
    })

    //Catch typeahead events
    .on('typeahead:select', submitSuggestion)
    .on('typeahead:autocomplete', submitSuggestion);
    
    $("input[name='term']").on('keypress', keypressForm);
});

/*
 * User triggered a submit, either via the typeahead:submit
 * or autocomplete event
 */
function submitSuggestion(ev, suggestion) {
    //liveSearchField.val(suggestion.name);
    // console.log('suggestion', suggestion);
    window.location = suggestion.data.url;
}

function submitForm(ev, suggestion) {
    $('#navbarSearch').submit();
}

function keypressForm(ev, suggestion) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    console.log('keycode', keycode);
    if (keycode == '13') {
        $('#navbarSearch').submit();
    }
}
JS;

$this->registerJs($js);

if (Yii::$app->hasModule('feedback')) {
    echo \taktwerk\yiiboilerplate\modules\feedback\widgets\FeedbackFormAssets::widget();
}

$this->endPage();