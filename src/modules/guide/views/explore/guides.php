<?php
/**
 * @var \taktwerk\yiiboilerplate\modules\guide\models\GuideCategory $tag
  * @var \taktwerk\yiiboilerplate\modules\guide\models\Guide $collection
 */
$this->title = $guideCategory->name . ' - ' . Yii::t('twbp', 'Category') . ' - ' . Yii::t('twbp', 'Guider');
?>
<div class="py-4">
    <div class="py-5 container">

    <?php if (count($guides) == 0) { ?>
        <div class="row">
            <div class="col-md-12">
                <h1 class=""><?=Yii::t('twbp', 'No guides by category')?></h1>
            </div>
        </div>
    <?php } else { ?>
        <div class="row">
            <div class="col-md-12 pb-2">
                <?php if($collection){?>
                <h1 class=""><?= Yii::t('twbp', 'Collection') . ': ' . $collection->title ?></h1>
                <h3 class=""><?= Yii::t('twbp', 'Category') . ': ' . $guideCategory->name ?></h3>
                <?php }else{?>
                <h1 class=""><?= Yii::t('twbp', 'Category') . ': ' . $guideCategory->name ?></h1>
                <?php }?>
            </div>
        </div>
        <?php if($collection==null){?>
            <?php foreach ($guides as $binding):?>
                <?php
                    if (!empty($guideCategory->id)) {
                        echo $this->render('blocks/guide', ['binding' => $binding]);
                    } else {
                        echo $this->render('blocks/guide', ['guide' => $binding]);
                    }
                ?>
            <?php endforeach; ?>
        <?php }else{?>
        <?php
        $guideChildren = $collection->getGuideChildren0()->orderBy('order_number asc')->all();
        foreach ($guideChildren as $guideChild):?>
            <?= $this->render('blocks/guide',['guide' => $guideChild->guide,'guideCategory'=>$guideCategory]);?>
        <?php endforeach; ?>
        <?php }?>
    <?php } ?>
    </div>
</div>