<?php
use yii\helpers\Html;
use yii\helpers\Url;
/** @var taktwerk\yiiboilerplate\modules\guide\models\guide $guide */
/** @var taktwerk\yiiboilerplate\modules\guide\models\GuideStep $step */

$this->title = Yii::t('twbp', 'Search');

?>
<div class="py-4">
    <div class="py-5 container">

        <div class="row">
            <div class="col-md-12">
                <h1 class=""><?=Yii::t('twbp', 'Results')?></h1>
            </div>
        </div>
        <?php if(count($results[0]['models'])>0){?>
        <?php 
        foreach ($results[0]['models'] as $result):
            ?>
                <?=$this->render('blocks/guide', ['guide' => $result['data']]);?>
        <?php endforeach; ?>
        <?php }else{?>
        <div class="card mt-0">
        <div class="text-center">
        <h3>No results for search</h3>
        </div>
        </div>
        <?php }?>
    </div>
</div>
