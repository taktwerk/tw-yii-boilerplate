<?php
use taktwerk\yiiboilerplate\widget\Select2;
 use taktwerk\yiiboilerplate\widget\Viewer3DModel;
 use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\base\Event;
use taktwerk\yiiboilerplate\components\View;
use kartik\select2\Select2Asset;
use kartik\select2\ThemeKrajeeBs4Asset;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;
use taktwerk\yiiboilerplate\modules\guide\models\GuideViewHistory;
use taktwerk\yiiboilerplate\modules\guide\models\GuideSetting;

/** @var taktwerk\yiiboilerplate\components\View $this */
/** @var taktwerk\yiiboilerplate\modules\guide\models\guide $guide */
/** @var taktwerk\yiiboilerplate\modules\guide\models\GuideStep $step */
$this->title = $guide->title . ' - ' . Yii::t('twbp', 'Guide') . ' - ' . Yii::t('twbp', 'Guider');

if($guide->is_collection == 0) {
    Yii::$app->view->params['guide'] = $guide;
}
$isHistoricised = GuideSetting::isHistoricised();
$parentGuideId = '';
if ($parentGuide) {
    $parentGuideId = $parentGuide->id;
    $guideChildRel = $parentGuide->getGuideChildren0()
        ->andWhere([
        'guide_id' => $guide->id
    ])
        ->one();
    $prevchildren = $parentGuide->getGuideChildren0()
        ->andWhere([
        'AND',
        [
            '<=',
            'order_number',
            $guideChildRel->order_number
        ],
        [
            '!=',
            'guide_id',
            $guide->id
        ]
    ])
        ->orderBy('order_number desc')
        ->one();

    $nextChildren = $parentGuide->getGuideChildren0()
        ->andWhere([
        'AND',
        [
            '>=',
            'order_number',
            $guideChildRel->order_number
        ],
        [
            '!=',
            'guide_id',
            $guide->id
        ]
    ])
        ->orderBy('order_number asc');
    if ($prevchildren) {
        $nextChildren = $nextChildren->andWhere([
            '!=',
            'id',
            $prevchildren->id
        ])->one();
    }else{
            $nextChildren = $nextChildren->one();
        }
  \Yii::$app->view->params['parentGuideId'] = $parentGuide->id;
  \Yii::$app->view->params['prevGuideId'] = $prevchildren->guide_id;
  \Yii::$app->view->params['nextGuideId'] = $nextChildren->guide_id;
}
$first = true;
$viewHistory = $guide->getGuideViewHistoryOfUser();
if ($parentGuide) {
    $viewHistory = $parentGuide->getGuideViewHistoryOfUser();
}
if($isHistoricised==false){
    $viewHistory = null;
}
?>
<?php if(Yii::$app->user->can('Authority') && !$guide->isCollection()){?>
<div class="send-guide-sec mt-5">
	<button type="submit"
		class="btn btn-primary btn-raised btn-fab btn-round ml-auto user-guide-btn collapsed"
		data-toggle="collapse" data-target="#handover-guide-sec" title="<?php echo \Yii::t('twbp','Click to handover this guide to other user')?>">
		<i class="material-icons">people</i>
	</button>
	<div id="handover-guide-sec" class="collapse user-guide-sec">
    	<?php echo Html::beginForm(['/guide/explore/assign-guide','id'=>$guide->id,'parent_id'=>$parentGuideId]);?>
    	<div class="form-group">
	<div class="user-select-sec">
	<?php 
	Event::on(get_class($this), View::EVENT_BEGIN_BODY, function($e){
	    $path = \Yii::$app->assetManager->publish(\Yii::getAlias('@vendor').'/kartik-v/yii2-widget-select2/src/assets/');
	    $files = [
	        'css/select2-addl.css',
	        'css/select2-krajee.css',
	        'css/select2-material.css',
	        'js/select2-krajee.min.js',
	    ];
	    
	    foreach($files as $f){
	       if(StringHelper::endsWith($f,'.css')){
	           $this->registerCssFile($path[1].'/'.$f,['position'=>View::POS_HEAD]);
	       }
	       if(StringHelper::endsWith($f,'.js')){
	           $this->registerJsFile($path[1].'/'.$f,['position'=>View::POS_BEGIN]);
	       }
	    }
	    
	    $path = \Yii::$app->assetManager->publish(\Yii::getAlias('@vendor/select2/select2/dist/'));;

	    $files = [
	        'js/select2.full.js',
	        'css/select2.css'
	    ];
	    foreach($files as $f){
	        if(StringHelper::endsWith($f,'.css')){
	            $this->registerCssFile($path[1].'/'.$f,['position'=>View::POS_HEAD]);
	        }
	        if(StringHelper::endsWith($f,'.js')){
	            $this->registerJsFile($path[1].'/'.$f,['position'=>View::POS_BEGIN]);
	        }
	    }
	    
	});
    
    $userQ = GuideViewHistory::getAssignableUserListQuery();
	echo Select2::widget(
	[
	    'theme'=>Select2::THEME_MATERIAL,
	    'name' => 'user_id',
	    'value' => '',
	    'data' => $userQ->count() > 50 ? null : ArrayHelper::map($userQ->all(), 'id', 'toString'),
	    //'initValueText' => $q->count() > 50 ? \yii\helpers\ArrayHelper::map($q->andWhere(['id' => $model->user_id])->all(), 'id', 'toString') : '',
	    'options' => [
	        'placeholder' => Yii::t('twbp', 'Select a user...'),
	        'id' => 'user_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
	    ],
	    'pluginOptions' => [
	        'allowClear' => false,
	        ($userQ->count() > 50 ? 'minimumInputLength' : '') => 3,
	        ($userQ->count() > 50 ? 'ajax' : '') => [
	            'url' => \yii\helpers\Url::to(['list']),
	            'dataType' => 'json',
	            'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'User\'
                                        };
                                    }')
	        ],
	    ],
	    'pluginEvents' => [
	        "change" => "function() {
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
	        Url::toRoute('/usermanager/user/entry-details?id=', true) .
	        "' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#user_id_well').html(json.data);
                                                //$('#user_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }",
	    ],
	    
	]);
	?>
	</div>
	<div class="text-right mt-2">
		<?php echo Html::submitButton('Assign',['class'=>'btn btn-primary'])?>
	</div>
</div>
    	<?php echo Html::endForm();?>
    </div>
</div>
<?php }?>
<div class="guide-step-pj guide-step-blur">
<?php if($viewHistory && (!isset($step) || $step->order_number==1)){?><?php
try{
$viewHistoryData = Json::decode($viewHistory->data);
}catch(\Exception $e){
    $viewHistoryData = [];
}
?>
<?php if($viewHistory->guide_id==$guide->id || (isset($viewHistoryData['child_guide_id']) && $viewHistoryData['child_guide_id']==$guide->id)){?>
<?php $alertCloseJs = <<<EOT
        setTimeout(function () {
        $('.guide-resume-notification').slideDown(350);
        }, 620); 
        setTimeout(function () {
                    $('.guide-resume-notification').slideUp(500,function(){
                        \$(this).alert('close'); 
                });
        }, 6000); 
        EOT;
$this->registerJs($alertCloseJs);
?>
<div class="guide-resume-notification text-right"  style="display:none;">
    <div class="alert alert-warning alert-dismissible fade show guide-resume-alert" title="<?php echo \Yii::t('twbp','Guide resumed from where you left off');?>" role="alert">
      <strong>Guide Resumed!</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
</div><?php }?>
<?php }?>
 <?php 
 echo Html::hiddenInput('prevGuideId',isset($prevchildren)?$prevchildren->guide_id:null);
 echo Html::hiddenInput('nextGuideId',isset($nextChildren)?$nextChildren->guide_id:null);
 echo Html::hiddenInput('parentGuideId',isset($parentGuide)?$parentGuide->id:null);
 ?>
    <div class="<?php echo ($guide->is_collection==0)?"h-100 d-flex flex-column pt-4 pt-lg-5":"py-4"?>">
    
    <?php if($guide->is_collection==0):?>
        <div class="guide-steps mb-2 single-item slick-dotted" role="toolbar">
            <?php
            $guideSteps = $guide->getGuideStepsOrderBySteps()->groupBy('guide_step.id')->all();
            foreach ($guideSteps as $gStep): ?>
            <div>
                <div class="pt-5 pb-2 container">
                    <div class="row">
                    <?php if($parentGuide){?>
                    <div class="col-2">
                            <h6 class="text-muted"><?= \Yii::t('twbp','Collection')?>: <?= Html::a($parentGuide->short_name,
                                ['/guide/explore/guide','id' => $parentGuide->id])?></h6>
                        </div>
                        <div class="col-10">
                            <h6 class="text-muted"><?= Html::a($parentGuide->title,
                                ['/guide/explore/guide','id' => $parentGuide->id])?></h6>
                        </div>
                    <?php }?>
                        <div class="col-2">
                            <h6 class="text-muted"><?=$guide->short_name?></h6>
                        </div>
                        <div class="col-10">
                            <h6 class="text-muted"><?=$guide->title?></h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <h2 class="mt-0"><?=$gStep->order_number?>/<?=$guide->getGuideSteps()->count()?></h2>
                        </div>
                        <div class="col-10">
                            <h2 class="mt-0"><?=$gStep->title?></h2>
                        </div>
                    </div>
                    <div class="row ipad-column">
                        <?php if ($media = $gStep->getAttachedFileHtml($first)): ?>
                            <div class="col-lg-7">
                                <div class="player-container h-100">
                                    <div class="player mb-2 h-100" ><?=$media?></div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <?php if (!empty($gStep->getDescription())): ?>
                                    <div class="card m-0 p-3"><?=$gStep->getDescription()?></div>
                                <?php endif; ?>
                            </div>
                        <?php else: ?>
                            <div class="col-lg-12">
                                <div class="card m-0 p-3"><?=$gStep->getDescription()?></div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php $first = false; endforeach ?>
        </div>
        <?php endif;?>
        <?php if($guide->is_collection==1){
        ?>
         <div class="py-5 container">
         
                    <div class="row">
                    <div class="col-md-12">
                    	<h1 class="">
                    	<?php echo Yii::t('twbp', 'Collection').': '.$guide->short_name.' '.$guide->title?>
                    	</h1> 
                    </div>
         </div>
           <?php $guideChildren = $guide->getGuideChildren0()->orderBy('order_number asc')->all();
           foreach ($guideChildren as $guideChild): $cGuide = $guideChild->guide;?>
           
          	    <div class="pt-2 pb-2 container">
                    <div class="row">                       
           	<?= $this->render('blocks/guide',['guide' => $cGuide,'guideCategory'=>false,'parentGuide'=>$guide]);?>
           			</div>
           		</div>
           	<?php $first = false; endforeach ?>
        </div>
        
        <?php }?>
        <div class="container">
            <hr />
            <style type="text/css">
               .modal-dialog
               {
                    max-width: 90% !important;
                    margin-left: 6%; 
               }
                .player-container {
                    max-height: 200%;
                }
            </style>
            <div class="row">
                    <?php $sliderGuideAssets = []; ?>
                    <?php foreach ($guide->guideAssetPivots as $pivot):?>
                        <div class="col">
                        <?php if ($pivot->guideAsset->isFile()): ?>
                            <?php $sliderGuideAssets[] = $pivot->guideAsset; ?>
                            <?php if ($pivot->guideAsset->isPdf()): ?>
                                <img src="<?=$pivot->guideAsset->getFileUrl('pdf_image', ['width' => 130], true)?>"
                                     data-elem="thumb"
                                     data-options="sliderId:guideSlider; index:<?=count($sliderGuideAssets) - 1?>; fullscreen:true; offCss:{alpha:1}"
                                >
                            <?php elseif ($pivot->guideAsset->is3dModelFile('asset_file')): ?>
                                <div class="3d-model-thumb"
                                     data-model-path="<?=
                                        !empty($pivot->guideAsset->getMinFilePath('asset_file')) ? 
                                        $pivot->guideAsset->getMinFilePath('asset_file') :
                                        $pivot->guideAsset->getFileUrl('asset_file', [], true)
                                     ?>"
                                     data-id="<?=$pivot->guideAsset->id?>"
                                     data-model-name="guideAsset"
                                     data-elem="thumb"
                                     data-options="sliderId:guideSlider; index:<?=count($sliderGuideAssets) - 1?>; fullscreen:true; offCss:{alpha:1}"
                                >
                                    <a href="#" class="btn mt-3">
                                        <i class="fa fa-fw fa-cube"></i><?=$pivot->guideAsset->name?>
                                    </a>
                                </div>
                            <?php else: ?>
                                <img src="<?=$pivot->guideAsset->getFileUrl('asset_file', ['width' => 130], true)?>"
                                     data-elem="thumb"
                                     data-options="sliderId:guideSlider; index:<?=count($sliderGuideAssets) - 1?>; fullscreen:true; offCss:{alpha:1}"
                                >
                            <?php endif; ?>
                        <?php else: ?>
                            <a href="#" class="btn mt-3" data-toggle="modal" data-target="#guideText<?=$pivot->id?>">
                                <i class="fa fa-fw fa-file"></i><?=$pivot->guideAsset->name?>
                            </a>

                            <!-- The Modal -->
                            <div class="modal fade" id="guideText<?=$pivot->id?>">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title"><?=$pivot->guideAsset->name?></h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>

                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            <?=$pivot->guideAsset->asset_html?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif ?>
                </div>
                <?php endforeach; ?>
                <?php if (count($sliderGuideAssets)) : ?>
                    <div class="pinchSlider">
                        <div id="guideSlider"
                             class="slider"
                             data-elem="touchnswipe"
                             style="width:1px; height: 1px; overflow: hidden; position: absolute"
                        >
                            <div class="slideHolder"
                                 data-elem="slides"
                                 data-options="loop:false; slideOptions:{ maxZoom:3; allowMouseWheelScroll:true}"
                            >
                                <?php $slideIndex = 0; ?>
                                <?php foreach ($sliderGuideAssets as $sliderGuideAsset):?>
                                    <?php if ($sliderGuideAsset->isPdf()): ?>
                                        <div class="slider-element"
                                             data-elem="slide"
                                             data-index="<?php echo $slideIndex; $slideIndex++;?>"
                                        >
                                            <img data-elem="bg"
                                                 data-src="<?=$sliderGuideAsset->getFileUrl('pdf_image', ['width' => 3000], true)?>"
                                            >
                                        </div>
                                    <?php elseif ($sliderGuideAsset->is3dModelFile('asset_file')): ?>
                                        <div class="3d-model-<?=$sliderGuideAsset->id?> slider-element"
                                             data-elem="slide"
                                             data-index="<?php echo $slideIndex; $slideIndex++;?>"
                                             data-model-path="<?=
                                                !empty($sliderGuideAsset->getMinFilePath('asset_file')) ? 
                                                $sliderGuideAsset->getMinFilePath('asset_file') :
                                                $sliderGuideAsset->getFileUrl('asset_file', [], true)
                                             ?>"
                                             data-model-name="guideAsset"
                                             data-id="<?=$sliderGuideAsset->id?>"
                                             style="width: 100%; height: 100%;"
                                        >
                                            <h3 style="position: absolute;padding: 15px;color: white;text-transform:uppercase;margin: 0;">
                                                <?=Yii::t('twbp', 'Use arrows to move to the next or previous slide')?>
                                            </h3>
                                            <?= Viewer3DModel::widget([
                                                'modelPath' => !empty($sliderGuideAsset->getMinFilePath('asset_file')) ? 
                                                    $sliderGuideAsset->getMinFilePath('asset_file') :
                                                    $sliderGuideAsset->getFileUrl('asset_file', [], true)
                                            ]) ?>
                                        </div>
                                    <?php else: ?>
                                        <div class="slider-element"
                                             data-elem="slide"
                                             data-index="<?php echo $slideIndex; $slideIndex++;?>"
                                        >
                                            <img data-elem="bg"
                                                 data-src="<?=$sliderGuideAsset->getFileUrl('asset_file', ['width' => 3000], true)?>"
                                            >
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php
$additionJs = '';
if($guide->is_collection){
    $additionJs = "setNavVisiblity(null);";
}
$initialSlideOption = '';
if($viewHistory){
    if(isset($viewHistoryData['step_order_number'])){
        if($viewHistory->guide_id==$guide->id || (isset($viewHistoryData['child_guide_id']) && $viewHistoryData['child_guide_id']==$guide->id)){
            $initialSlideOption =  'initialSlide:'.(($viewHistoryData['step_order_number']-1)<0?0:($viewHistoryData['step_order_number']-1));
        }
    }
}
if(isset($step) && $step->order_number>1){
    $initialSlideOption =  'initialSlide:'.(($step->order_number-1)<0?0:($step->order_number-1));
}

$saveHistoryUrl = Url::toRoute(['/guide/explore/save-history']);
$isHistoricisedJsVar = ($isHistoricised==true)?'true':'false';
$this->registerJs("
    $(document).ready(function() {
    var threeModels = [];
    function setNavVisiblity(slick){
         if(slick){
            var el = $('.guide-steps');
            var currentSlide = slick.getCurrent();
            var totalSlides = slick.\$slides.length;
            var showDefault = true;
            var preGuideBtn = $('#prev-guide-nav');
            var nextGuideBtn = $('#next-guide-nav');
            var nextGuideStpBtn = $('#next-guidestep-nav');
            var prevGuideStpBtn = $('#prev-guidestep-nav');
            
            if(currentSlide==0){
                showDefault = false;
                var prevGuideId = $('input[name=\"prevGuideId\"]').val();
                    if(prevGuideId){
                        preGuideBtn.show();
                        prevGuideStpBtn.hide();
                        nextGuideBtn.hide();
                    }
            }
            if (totalSlides-1 == currentSlide) {
                    showDefault = false;
                    var nextGuideId = $('input[name=\"nextGuideId\"]').val();
                    if(nextGuideId){
                        preGuideBtn.hide();
                        nextGuideBtn.show();
                        nextGuideStpBtn.hide();
                    }else{
                        nextGuideStpBtn.show();
                    }
                    if(totalSlides>1){
                        preGuideBtn.hide();
                        prevGuideStpBtn.show();
                    }
           }else{
                nextGuideStpBtn.show();
                nextGuideBtn.hide();
           }
           if(showDefault){
                nextGuideBtn.hide();
                nextGuideStpBtn.show();
                preGuideBtn.hide();
                prevGuideStpBtn.show();
            }
           
        }else{
            var nextGuideId = $('input[name=\"nextGuideId\"]').val();
                    if(nextGuideId){
                     nextGuideBtn.show();
                    }
            var prevGuideId = $('input[name=\"prevGuideId\"]').val();
                    if(prevGuideId){
                        preGuideBtn.show();
                    }
        }
        
    }
    function saveGuideHistory(slideNumber){
            if(slideNumber && $isHistoricisedJsVar){
                $.ajax({
                    url:'{$saveHistoryUrl}',
                    data:{guide_id:{$guide->id},parent_guide_id:'{$parentGuideId}',step:slideNumber}
                });
            }
    }
    function runCarousel(initialSlide=null){
            
       var slickElement = $('.guide-steps');
       slickElement.off();
       slickElement.on('init', function(event, slick){
            setNavVisiblity(slick);saveGuideHistory(slick.getCurrent()+1);
       });
       slickElement.on('afterChange', function(event, slick, direction){
            setNavVisiblity(slick);
       });

       slickElement.on('setPosition', function(event, slick, direction){
            window.dispatchEvent(new Event('resize'));
       });
       var slideOption = {
            //lazyLoad: 'ondemand',
            prevArrow: '#slickprev',
            nextArrow: '#slicknext',
            infinite:false,
            {$initialSlideOption}
       };
       if(initialSlide!==null){
        slideOption['initialSlide'] = initialSlide;
       }
       slickElement.slick(slideOption);
        
       slickElement.on('beforeChange', function(event, slick, currentSlide, nextSlide){
            saveGuideHistory((nextSlide+1));
        });
    }

    $('#slickRestart').click(function(){
        var slickElement = $('.guide-steps');
        $('.guide-steps').find('audio,video').each(function(x,item){
                item.currentTime = 0;
        });
        slickElement.slick('slickGoTo',0,false);
    });

    var prevGuideId = $('input[name=\"prevGuideId\"]').val();
    if(prevGuideId==''){
        $('#prev-guidestep-nav').show();
        $('#next-guidestep-nav').show();
    }
    $('#restart-guidestep-nav').show();
    runCarousel();
    $('.guide-step-blur').delay(400).removeClass('guide-step-blur');
    {$additionJs}
        for (var i = 0; TouchNSwipe.get(i); i++) {
            var tns = TouchNSwipe.get(i),
            zoomSlider = tns.slider;
            zoomSlider._curZoomer._vars.allowZoom = false;
            zoomSlider.on(ElemZoomSlider.INDEX_CHANGE, function(event) {
                if (!event ||
                    !event.currentTarget ||
                    !event.currentTarget._curSlide ||
                    !event.currentTarget._curSlide[0]
                ) {
                    return;
                }
                var currentSlideModels = event.currentTarget._curSlide[0].getElementsByClassName('slider-element');
                if (!currentSlideModels || !currentSlideModels[0]) {
                    return;
                }
                var currentIndex = currentSlideModels[0].dataset.index;
                if (!currentIndex) {
                    return;
                }
                var targetSlide = null;
                if (currentIndex < event.currentTarget.index() &&
                    event.currentTarget._nextSlide &&
                    event.currentTarget._nextSlide[0]
                ) {
                    targetSlide = event.currentTarget._nextSlide[0];
                } else if (currentIndex > event.currentTarget.index() &&
                    event.currentTarget._prevSlide &&
                    event.currentTarget._prevSlide[0]) {
                    targetSlide = event.currentTarget._prevSlide[0];
                }
                if (!targetSlide) {
                    return;
                }
                var sliderModelElements = targetSlide.getElementsByClassName('slider-element');
                if (!sliderModelElements || !sliderModelElements[0]) {
                    return;
                }
                var modelElements = sliderModelElements[0].getElementsByClassName('threed-model');
                var fileName = sliderModelElements[0].dataset.modelPath;
                var modelName = sliderModelElements[0].dataset.modelName;
                var modelId = sliderModelElements[0].dataset.id;
                if (!modelElements || !modelElements[0] || !fileName) {
                    return;
                }
                var modelElement = modelElements[0];
                var alreadyRenderedModels = modelElement.getElementsByTagName('canvas');
                if (!alreadyRenderedModels || !alreadyRenderedModels.length) {
                    var model = new Model3D();
                    model.init(modelElement, fileName);
                    if (!threeModels[modelName]) {
                        threeModels[modelName] = [];
                    }
                    threeModels[modelName][modelId] = model;
                    console.log(threeModels);
                }
            });
            zoomSlider._curZoomer.on(ElemZoomer.GESTURE_START, function(event) {
                var currentSlideModel = event.target.elem();
                if (!currentSlideModel) {
                    return;
                }
                console.log('currentSlideModel', currentSlideModel);
                var modelElements = currentSlideModel.get(0).getElementsByClassName('threed-model');
                if (modelElements && modelElements[0]) {
                    this._vars.allowDrag = false;
                    var modelName = currentSlideModel.get(0).dataset.modelName;
                    var modelId = currentSlideModel.get(0).dataset.id;
                    var currentModel = threeModels[modelName][modelId];
                    if (currentModel) {
                        currentModel.isRotateModel = false;
                    }
                }
                
                event.preventDefault();
            });
        }
        
        $('.3d-model-thumb').on('click', function() {
            var fileName = $(this).data('modelPath');
            var assetId = $(this).data('id');
            var modelName = $(this).data('modelName');
            var modelElement = $('.slider .3d-model-' + assetId + ' .threed-model').get(0);
            var alreadyRenderedModels = modelElement.getElementsByTagName('canvas');
            if (!alreadyRenderedModels || !alreadyRenderedModels.length) {
                var model = new Model3D();
                model.init(modelElement, fileName);
                if (!threeModels[modelName]) {
                    threeModels[modelName] = [];
                }
                threeModels[modelName][assetId] = model;
                console.log(threeModels);
            }
        })
        $('.3d-step-model-zoom').on('click', function() {
            var fileName = $(this).data('modelPath');
            var stepId = $(this).data('id');
            var modelName = $(this).data('modelName');
            var modelElement = $('#stepSlider' + stepId + ' .3d-step-model-' + stepId + ' .threed-model').get(0);
            var alreadyRenderedModels = modelElement.getElementsByTagName('canvas');
            if (!alreadyRenderedModels || !alreadyRenderedModels.length) {
                var model = new Model3D();
                model.init(modelElement, fileName);
                if (!threeModels[modelName]) {
                    threeModels[modelName] = [];
                }
                threeModels[modelName][stepId] = model;
                console.log(threeModels);
            }
        })
        var step3dModels = $('.3d-step-model-thumb');
        Array.from(step3dModels).forEach(function(stepModel) {
            var fileName = $(stepModel).data('modelPath');
            var parentModelElement = $(stepModel).get(0);
            if (!parentModelElement) {
                return;
            }
            var modelElements = parentModelElement.getElementsByClassName('threed-model');
            if (!modelElements || !modelElements[0]) {
                return;
            }
            var modelElement = modelElements[0];
            var alreadyRenderedModels = modelElement.getElementsByTagName('canvas');
            if (!alreadyRenderedModels || !alreadyRenderedModels.length) {
                var model = new Model3D();
                model.backgroundColor = null;
                model.enableUserRotate = false;
                model.willStopRotate = false;
                model.init(modelElement, fileName);
            }
        })
    });
");
