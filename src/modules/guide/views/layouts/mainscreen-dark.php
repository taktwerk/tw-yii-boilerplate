<?php 
/**
 * @var $this \yii\web\View
*/
use yii\helpers\Html;
use taktwerk\yiiboilerplate\modules\guide\assets\DarkScreenAsset;

DarkScreenAsset::register($this);
?><?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/> <?= Html::csrfMetaTags() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title><?php echo Html::encode($this->title); ?></title>
     <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php echo $content; ?>
<?php $this->endBody() ?>
</body>
</html><?php $this->endPage() ?>