<?php

use taktwerk\yiiboilerplate\modules\guide\models\Guide;
/**
 * @var $guide Guide
 */
?>
<!DOCTYPE html>
<html>
<head>
    <style>
        .page-break {
            page-break-before: always;
        }

        body {
            width: 100%;
        }

        .guide {
            font-size: 16pt;
            width: 100%;
        }

        img.guide-img {
            height: 800px;
        }

        img.guide-step-img {
            /* width: 700px; */
            height: 600px;
        }

        .guide td {
            text-align: center;
        }

        .steps {

        }

        .guide-description {
            font-size: 24pt;
        }

        .history {
            font-size: 9pt;
        }

        table {
            border-collapse:collapse;
        }

        .ck {
            display: none;
        }

        td.ck-editor__editable {
            border: 1px solid silver !important;
            padding: 10px;
        }

        /* specific */

        .text {
            vertical-align: top;
            padding-right: 20px;
        }
    </style>
</head>
<body>
<?php 
if(!function_exists('pdfGuideTitle')){
function pdfGuideTitle($guide){
?>
<table class="guide">
    <tr>
        <td>
            <?= $guide->getGuidePreview();?>
            <br /><br />
            <div class="info">
                <h1><?= $guide->title; ?></h1>
                <h2><?= $guide->short_name; ?></h2>
                <br />
                <?php if($guide->durationTime(false)) : ?>
                    <?= Yii::t('twbp', 'Duration').': ' . $guide->durationTime() ?>
                    <br/>
                    <br/>
                <?php endif; ?>
                <div class="history">
                    <?= taktwerk\yiiboilerplate\RecordHistory::widget(['model' => $guide]) ?>
                    <br />
                </div>
                <div class="guide-description">
                    <?= $guide->description; ?>
                </div>
                <br /><br /><br />
            </div>
        </td>
    </tr>
</table>
<div class="page-break"></div>
<?php }?>
<?php }?>
<?php 
if(!function_exists('pdfColumn1Gen')){
function pdfColumn1Gen($guide,$skipGuideIds=[]){?>
<?php 
if($guide==null || in_array($guide->id,$skipGuideIds)){
    return;
}

pdfGuideTitle($guide);
if($guide->isCollection()){
    $guideChildren = $guide->getGuideChildren0()->orderBy('order_number asc')->all();
    foreach($guideChildren as $gChild){
        pdfColumn1Gen($gChild->guide,array_merge($skipGuideIds,[$guide->id]));
    }
}else{?>
<?php
/**Guide Steps*/
$steps = $guide->getGuideSteps()->orderBy(['order_number' => SORT_ASC])->all();
if($steps):?>
    <table class="steps">
        <?php foreach ($steps as $step):
            /**@var $step GuideStep*/

            $type_string = '';
            if($step->isFileAudio()) $type_string = ' (' . Yii::t('twbp', 'Audio') . ')';
            if($step->isFileVideo()) $type_string = ' (' . Yii::t('twbp', 'Video') . ')';

            ?>
            <tr>
                <td>
                <?php if($stepPicture = $step->getStepPicture()): ?>
                    <?= $stepPicture;?>
                    <br /><br />
                <?php endif; ?>
                    <div class="text tabletext">
                        <h3><?= $step->order_number ;?>/<?= count($steps) ;?>: <?= $step->title . $type_string; ?></h3>
                        <?php if($description = $step->getDescription()): ?>
                            <br />
                            <?= $description; ?>
                        <?php endif; ?>
                    </div>
                    <hr>
                </td>
            </tr>
        <?php endforeach;?>
    </table>
<?php endif;?>
<?php }?>
<?php }?>
<?php }?>
<?php 
if ($guide->isCollection()){
    pdfGuideTitle($guide);
    $guideChildren = $guide->getGuideChildren0()
        ->orderBy('order_number asc')
        ->all();
    foreach ($guideChildren as $gChild) {
        if($gChild->guide){
            pdfColumn1Gen($gChild->guide,[$guide->id]);
        }
    }
} else {
    pdfColumn1Gen($guide);
}
?>
</body>
</html>