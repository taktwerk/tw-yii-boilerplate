<?php

use taktwerk\yiiboilerplate\modules\guide\models\Guide;
use taktwerk\yiiboilerplate\modules\guide\models\GuideStep;
use taktwerk\yiiboilerplate\components\Helper;

/**
 * @var $guide Guide
 */
?>
<!DOCTYPE html>
<html>
<head>
    <style>
        .page-break {
            page-break-before: always;
        }

        body {
            width: 100%;
        }

        .guide {
            font-size: 16pt;
            width: 100%;
        }

        img.guide-img {
            height: 800px;
        }

        img.guide-step-img {
            width: 800px;
        }

        .guide td {
            text-align: center;
        }

        .guide-description {
            font-size: 24pt;
        }

        .history {
            font-size: 9pt;
        }

        table {
            border-collapse:collapse;
        }

        .ck {
            display: none;
        }

        td.ck-editor__editable {
            border: 1px solid silver !important;
            padding: 10px;
        }

        /* specific */

        table.steps {
            font-size: 20pt;
        }

        td.text, td.image {
            padding-bottom: 20px;
        }

        td.text {
            width: 50%;
            vertical-align: top;
            padding-right: 20px;
        }

    </style>
</head>
<body>
<?php 
if(!function_exists('pdfCol2GuideTitle')){
function pdfCol2GuideTitle($guide){
?>
<table class="guide">
    <tr>
        <td>
            <?= $guide->getGuidePreview();?>
            <br /><br />
            <div class="info">
                <h1><?= $guide->title; ?></h1>
                <h2><?= $guide->short_name; ?></h2>
                <br />
                <?php if($guide->durationTime(false)) : ?>
                	<?= Yii::t('twbp', 'Duration').': ' . $guide->durationTime() ?>
                    <br />
                    <br />
                <?php endif; ?>
                <div class="history">
                    <?= taktwerk\yiiboilerplate\RecordHistory::widget(['model' => $guide]) ?>
                    <br />
                </div>
                <div class="guide-description">
                    <?= $guide->description; ?>
                </div>
                <br /><br /><br />
            </div>
        </td>
    </tr>
</table>
<div class="page-break"></div>
<?php }?>
<?php }?>

<?php 
if(!function_exists('pdfColumn2Gen')){
function pdfColumn2Gen($guide,$skipGuideIds=[]){?>
<?php 
if(in_array($guide->id,$skipGuideIds)){
    return;
}
pdfCol2GuideTitle($guide);
if($guide->isCollection()){
    $guideChildren = $guide->getGuideChildren0()->orderBy('order_number asc')->all();
    foreach($guideChildren as $gChild){
        pdfColumn2Gen($gChild->guide,array_merge($skipGuideIds,[$guide->id]));
    }
}else{?>
<?php
/**Guide Steps*/
$steps = $guide->getGuideSteps()->orderBy(['order_number' => SORT_ASC])->all();
if($steps):?>
    <table class="steps">
        <?php foreach ($steps as $step):
            /**@var $step GuideStep*/
            ?>
            <tr>
                <td class="text">
                    <h3><?= $step->order_number ;?>/<?= count($steps) ;?>: <?= $step->title; ?></h3>
                    <br />
                    <?= $step->description_html; ?>
                </td>
                <td class="image">
                    <?= $step->getStepPicture();?>
                </td>
            </tr>
        <?php endforeach;?>
    </table>
<?php endif;?>
<?php }?>
<?php }?>
<?php }?>
<?php 
if ($guide->isCollection()){
    pdfCol2GuideTitle($guide);
    $guideChildren = $guide->getGuideChildren0()
        ->orderBy('order_number asc')
        ->all();
    foreach ($guideChildren as $gChild) {
        pdfColumn2Gen($gChild->guide,[$guide->id]);
    }
} else {
    pdfColumn2Gen($guide);
}
?>
</body>
</html>