<?php
//Generation Date: 27-May-2021 08:48:21am
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\widget\form\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\components\ClassDispenser;
/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\guide\models\GuideViewHistory $model
 * @var taktwerk\yiiboilerplate\widget\form\ActiveForm $form
 * @var boolean $useModal
 * @var boolean $multiple
 * @var array $pk
 * @var array $show
 * @var string $action
 * @var string $owner
 * @var array $languageCodes
 * @var boolean $relatedForm to know if this view is called from ajax related-form action. Since this is passing from
 * select2 (owner) it is always inherit from main form
 * @var string $relatedType type of related form, like above always passing from main form
 * @var string $owner string that representing id of select2 from which related-form action been called. Since we in
 * each new form appending "_related" for next opened form, it is always unique. In main form it is always ID without
 * anything appended
 */

$owner = $relatedForm ? '_' . $owner . '_related' : '';
$relatedTypeForm = Yii::$app->request->get('relatedType')?:$relatedTypeForm;

if (!isset($multiple)) {
$multiple = false;
}

if (!isset($relatedForm)) {
$relatedForm = false;
}

$tableHint = (!empty(taktwerk\yiiboilerplate\modules\guide\models\GuideViewHistory::tableHint()) ? '<div class="table-hint">' . taktwerk\yiiboilerplate\modules\guide\models\GuideViewHistory::tableHint() . '</div><hr />' : '<br />');

if (!Yii::$app->getUser()->can('Authority')) {
    $clients = [];
    $clientUsers = \taktwerk\yiiboilerplate\modules\customer\models\ClientUser::findAll(['user_id' => Yii::$app->getUser()->id]);
    foreach ($clientUsers as $client) {
        $model->client_id = $client->client_id;
        continue;
    }
    $show['client_id'] = false;
}
?>
<div class="guide-view-history-form">
        <?php  $formId = 'GuideViewHistory' . ($ajax || $useModal ? '_ajax_' . $owner : ''); ?>    
    <?php $form = ActiveForm::begin([
        'id' => $formId,
        'layout' => 'default',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'action' => $useModal ? $action : '',
        'options' => [
        'name' => 'GuideViewHistory',
            ],
    ]); ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>
        <?php echo $tableHint; ?>

        <?php if ($multiple) : ?>
            <?=Html::hiddenInput('update-multiple', true)?>
            <?php foreach ($pk as $id) :?>
                <?=Html::hiddenInput('pk[]', $id)?>
            <?php endforeach;?>
        <?php endif;?>

        <?php $fieldColumns = [
                
            'client_id' => Yii::$app->getUser()->can('Authority')?
            $form->field(
                $model,
                'client_id'
            )
                    ->widget(
                        Select2::class,
                        [
                            'data' => taktwerk\yiiboilerplate\modules\customer\models\Client::find()->count() > 50 ? null : ArrayHelper::map(taktwerk\yiiboilerplate\modules\customer\models\Client::find()->groupBy('client.id')->all(), 'id', 'toString'),
                                    'initValueText' => taktwerk\yiiboilerplate\modules\customer\models\Client::find()->groupBy('client.id')->count() > 50 ? \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\modules\customer\models\Client::find()->andWhere(['client.id' => $model->client_id])->groupBy('client.id')->all(), 'id', 'toString') : '',
                            'options' => [
                                'placeholder' => Yii::t('twbp', 'Select a value...'),
                                'id' => 'client_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                
                            ],
                            'pluginOptions' => [
                                'allowClear' => false,
                                (taktwerk\yiiboilerplate\modules\customer\models\Client::find()->groupBy('client.id')->count() > 50 ? 'minimumInputLength' : '') => 3,
                                (taktwerk\yiiboilerplate\modules\customer\models\Client::find()->groupBy('client.id')->count() > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'Client\', page:params.page || 1
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
                                        Url::toRoute('/customer/client/entry-details?id=', true) .
                                        "' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#client_id_well').html(json.data);
                                                //$('#client_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }",
                            ],
                            'addon' => (Yii::$app->getUser()->can('modules_client_create') && (!$relatedForm || ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL && !$useModal))) ? [
                                'append' => [
                                    'content' => [
                                        ClassDispenser::getMappedClass(RelatedForms::class)::widget([
                                            'relatedController' => '/customer/client',
                                            'type' => $relatedTypeForm,
                                            'selector' => 'client_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'primaryKey' => 'id',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    ) . '
                    
                
                <div id="client_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' . 
                    (taktwerk\yiiboilerplate\modules\customer\models\Client::findOne(['client.id' => $model->client_id])
                        ->entryDetails == '' ?
                    ' display:none;' :
                    '') . '
                    ">' . 
                    (taktwerk\yiiboilerplate\modules\customer\models\Client::findOne(['client.id' => $model->client_id])->entryDetails != '' ?
                    taktwerk\yiiboilerplate\modules\customer\models\Client::findOne(['client.id' => $model->client_id])->entryDetails :
                    '') . ' 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>':
HTML::activeHiddenInput(
                $model,
                'client_id',
                [
                    'id' => Html::getInputId($model, 'client_id') . $owner,
                    
                ]
            ),
            'guide_id' => 
            $form->field(
                $model,
                'guide_id'
            )
                    ->widget(
                        Select2::class,
                        [
                            'data' => taktwerk\yiiboilerplate\modules\guide\models\Guide::find()->count() > 50 ? null : ArrayHelper::map(taktwerk\yiiboilerplate\modules\guide\models\Guide::find()->groupBy('guide.id')->all(), 'id', 'toString'),
                                    'initValueText' => taktwerk\yiiboilerplate\modules\guide\models\Guide::find()->groupBy('guide.id')->count() > 50 ? \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\modules\guide\models\Guide::find()->andWhere(['guide.id' => $model->guide_id])->groupBy('guide.id')->all(), 'id', 'toString') : '',
                            'options' => [
                                'placeholder' => Yii::t('twbp', 'Select a value...'),
                                'id' => 'guide_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                (taktwerk\yiiboilerplate\modules\guide\models\Guide::find()->groupBy('guide.id')->count() > 50 ? 'minimumInputLength' : '') => 3,
                                (taktwerk\yiiboilerplate\modules\guide\models\Guide::find()->groupBy('guide.id')->count() > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'Guide\', page:params.page || 1
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                                    var data_id = $(this);
                                $.post('" .
            Url::toRoute('/guide/guide/system-entry?id=', true) .
            "' + data_id.val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if(!json.success){
                                                if ((data_id.val() != null) && (data_id.val() != '')) {
                                        // Enable edit icon
                                        $(data_id.next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
            Url::toRoute('/guide/guide/entry-details?id=', true) .
            "' + data_id.val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#guide_id_well').html(json.data);
                                                //$('#guide_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $(data_id.next().next().children('button')[0]).prop('disabled', true);
                                    }
                                            }else{
                                        // Disable edit icon and remove sub-form
                                        $(data_id.next().next().children('button')[0]).prop('disabled', true);
                                            }
                                        });
                                }",
                            ],
                            'addon' => (Yii::$app->getUser()->can('modules_guide_create') && (!$relatedForm || ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL && !$useModal))) ? [
                                'append' => [
                                    'content' => [
                                        ClassDispenser::getMappedClass(RelatedForms::class)::widget([
                                            'relatedController' => '/guide/guide',
                                            'type' => $relatedTypeForm,
                                            'selector' => 'guide_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'primaryKey' => 'id',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    ) . '
                    
                
                <div id="guide_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' . 
                    (taktwerk\yiiboilerplate\modules\guide\models\Guide::findOne(['guide.id' => $model->guide_id])
                        ->entryDetails == '' ?
                    ' display:none;' :
                    '') . '
                    ">' . 
                    (taktwerk\yiiboilerplate\modules\guide\models\Guide::findOne(['guide.id' => $model->guide_id])->entryDetails != '' ?
                    taktwerk\yiiboilerplate\modules\guide\models\Guide::findOne(['guide.id' => $model->guide_id])->entryDetails :
                    '') . ' 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>',
            'user_id' => 
            $form->field(
                $model,
                'user_id'
            )
                    ->widget(
                        Select2::class,
                        [
                            'data' => taktwerk\yiiboilerplate\models\User::find()->count() > 50 ? null : ArrayHelper::map(taktwerk\yiiboilerplate\models\User::find()->groupBy('{{%user}}.id')->all(), 'id', 'toString'),
                                    'initValueText' => taktwerk\yiiboilerplate\models\User::find()->groupBy('{{%user}}.id')->count() > 50 ? \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\models\User::find()->andWhere(['{{%user}}.id' => $model->user_id])->groupBy('{{%user}}.id')->all(), 'id', 'toString') : '',
                            'options' => [
                                'placeholder' => Yii::t('twbp', 'Select a value...'),
                                'id' => 'user_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                (taktwerk\yiiboilerplate\models\User::find()->groupBy('{{%user}}.id')->count() > 50 ? 'minimumInputLength' : '') => 3,
                                (taktwerk\yiiboilerplate\models\User::find()->groupBy('{{%user}}.id')->count() > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'User\', page:params.page || 1
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
                                        Url::toRoute('/usermanager/user/entry-details?id=', true) .
                                        "' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#user_id_well').html(json.data);
                                                //$('#user_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }",
                            ],
                            'addon' => (Yii::$app->getUser()->can('models_user_create') && (!$relatedForm || ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL && !$useModal))) ? [
                                'append' => [
                                    'content' => [
                                        ClassDispenser::getMappedClass(RelatedForms::class)::widget([
                                            'relatedController' => '/usermanager/user',
                                            'type' => $relatedTypeForm,
                                            'selector' => 'user_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'primaryKey' => 'id',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    ) . '
                    
                
                <div id="user_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' . 
                    (taktwerk\yiiboilerplate\models\User::findOne(['{{%user}}.id' => $model->user_id])
                        ->entryDetails == '' ?
                    ' display:none;' :
                    '') . '
                    ">' . 
                    (taktwerk\yiiboilerplate\models\User::findOne(['{{%user}}.id' => $model->user_id])->entryDetails != '' ?
                    taktwerk\yiiboilerplate\models\User::findOne(['{{%user}}.id' => $model->user_id])->entryDetails :
                    '') . ' 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>',
            'data' => 
            $form->field(
                $model,
                'data',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'data') . $owner
                    ]
                ]
            )
                    ->textarea(
                        [
                            'rows' => 6,
                            'placeholder' => $model->getAttributePlaceholder('data'),
                            'id' => Html::getInputId($model, 'data') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('data')),]; ?>        <div class='clearfix'></div>
<?php $twoColumnsForm = true; ?>
<?php 
// form fields overwrite
$fieldColumns = Yii::$app->controller->crudColumnsOverwrite($fieldColumns, 'form', $model, $form, $owner);

foreach($fieldColumns as $attribute => $fieldColumn) {
            if (count($model->primaryKey())>1 && in_array($attribute, $model->primaryKey()) && $model->checkIfHidden($attribute)) {
            echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
            echo $fieldColumn;
            echo $twoColumnsForm ? '</div>' : '';
        } elseif ($model->checkIfHidden($attribute)) {
            echo $fieldColumn;
        } elseif (!$multiple || ($multiple && isset($show[$attribute]))) {
            if ((isset($show[$attribute]) && $show[$attribute]) || !isset($show[$attribute])) {
                echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
                echo $fieldColumn;
                echo $twoColumnsForm ? '</div>' : '';
            } else {
                echo $fieldColumn;
            }
        }
                
} ?><div class='clearfix'></div>

                <?php $this->endBlock(); ?>
        
        <?= ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_TAB) ?
            Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                    'label' => Yii::t('twbp', 'Guide View History'),
                    'content' => $this->blocks['main'],
                    'active' => true,
                ],
                ]
            ])
            : $this->blocks['main']
        ?>        
        <div class="col-md-12">
        <hr/>
        
        </div>
        
        <div class="clearfix"></div>
        <?php echo $form->errorSummary($model); ?>
        <?php echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::formButtons($model, $useModal, $relatedForm, $multiple, "twbp", ['id' => $model->id]);?>
        <?php ClassDispenser::getMappedClass(ActiveForm::class)::end(); ?>
        <?= ($relatedForm && $relatedType == ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL) ?
        '<div class="clearfix"></div>' :
        ''
        ?>
        <div class="clearfix"></div>
    </div>
</div>

<?php
if ($useModal) {
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::class)::register($this);
}
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
if(!isset($useDependentFieldsJs) || $useDependentFieldsJs==true){
$this->render('@taktwerk-views/_dependent-fields', ['model' => $model,'formId'=>$formId]);
}
?>