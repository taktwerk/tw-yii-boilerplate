<?php
/*If a guide is a collection then we don't show the guide steps and show the guide children or vice-versa*/
$this->context->crudRelations['GuideSteps'] = $model->is_collection != 1;
$this->context->crudRelations['GuideChildren0'] = $model->is_collection == 1;
require(__DIR__ . DIRECTORY_SEPARATOR . 'base' . DIRECTORY_SEPARATOR . 'view.php');

echo $this->blocks['custom-actions'];