<?php
//Generation Date: 27-May-2021 08:48:09am
use yii\helpers\ArrayHelper;
use taktwerk\yiiboilerplate\widget\Select2;
use taktwerk\yiiboilerplate\widget\form\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use yii\helpers\Url;
use kartik\helpers\Html;
use taktwerk\yiiboilerplate\widget\DepDrop;
use taktwerk\yiiboilerplate\modules\backend\widgets\RelatedForms;
use taktwerk\yiiboilerplate\components\ClassDispenser;
/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\guide\models\Guide $model
 * @var taktwerk\yiiboilerplate\widget\form\ActiveForm $form
 * @var boolean $useModal
 * @var boolean $multiple
 * @var array $pk
 * @var array $show
 * @var string $action
 * @var string $owner
 * @var array $languageCodes
 * @var boolean $relatedForm to know if this view is called from ajax related-form action. Since this is passing from
 * select2 (owner) it is always inherit from main form
 * @var string $relatedType type of related form, like above always passing from main form
 * @var string $owner string that representing id of select2 from which related-form action been called. Since we in
 * each new form appending "_related" for next opened form, it is always unique. In main form it is always ID without
 * anything appended
 */

$owner = $relatedForm ? '_' . $owner . '_related' : '';
$relatedTypeForm = Yii::$app->request->get('relatedType')?:$relatedTypeForm;

if (!isset($multiple)) {
$multiple = false;
}

if (!isset($relatedForm)) {
$relatedForm = false;
}

$tableHint = (!empty(taktwerk\yiiboilerplate\modules\guide\models\Guide::tableHint()) ? '<div class="table-hint">' . taktwerk\yiiboilerplate\modules\guide\models\Guide::tableHint() . '</div><hr />' : '<br />');

if (!Yii::$app->getUser()->can('Authority')) {
    $clients = [];
    $clientUsers = \taktwerk\yiiboilerplate\modules\customer\models\ClientUser::findAll(['user_id' => Yii::$app->getUser()->id]);
    foreach ($clientUsers as $client) {
        $model->client_id = $client->client_id;
        continue;
    }
    $show['client_id'] = false;
}
?>
<div class="guide-form">
        <?php  $formId = 'Guide' . ($ajax || $useModal ? '_ajax_' . $owner : ''); ?>    
    <?php $form = ActiveForm::begin([
        'id' => $formId,
        'layout' => 'default',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error',
        'action' => $useModal ? $action : '',
        'options' => [
        'name' => 'Guide',
    'enctype' => 'multipart/form-data'
        
        ],
    ]); ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>
        <?php echo $tableHint; ?>

        <?php if ($multiple) : ?>
            <?=Html::hiddenInput('update-multiple', true)?>
            <?php foreach ($pk as $id) :?>
                <?=Html::hiddenInput('pk[]', $id)?>
            <?php endforeach;?>
        <?php endif;?>

        <?php $fieldColumns = [
                
            'client_id' => Yii::$app->getUser()->can('Authority')?
            $form->field(
                $model,
                'client_id'
            )
                    ->widget(
                        Select2::class,
                        [
                            'data' => taktwerk\yiiboilerplate\modules\customer\models\Client::find()->count() > 50 ? null : ArrayHelper::map(taktwerk\yiiboilerplate\modules\customer\models\Client::find()->groupBy('client.id')->all(), 'id', 'toString'),
                                    'initValueText' => taktwerk\yiiboilerplate\modules\customer\models\Client::find()->groupBy('client.id')->count() > 50 ? \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\modules\customer\models\Client::find()->andWhere(['client.id' => $model->client_id])->groupBy('client.id')->all(), 'id', 'toString') : '',
                            'options' => [
                                'placeholder' => Yii::t('twbp', 'Select a value...'),
                                'id' => 'client_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                
                            ],
                            'pluginOptions' => [
                                'allowClear' => false,
                                (taktwerk\yiiboilerplate\modules\customer\models\Client::find()->groupBy('client.id')->count() > 50 ? 'minimumInputLength' : '') => 3,
                                (taktwerk\yiiboilerplate\modules\customer\models\Client::find()->groupBy('client.id')->count() > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'Client\', page:params.page || 1
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                                    if (($(this).val() != null) && ($(this).val() != '')) {
                                        // Enable edit icon
                                        $($(this).next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
                                        Url::toRoute('/customer/client/entry-details?id=', true) .
                                        "' + $(this).val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#client_id_well').html(json.data);
                                                //$('#client_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $($(this).next().next().children('button')[0]).prop('disabled', true);
                                    }
                                }",
                            ],
                            'addon' => (Yii::$app->getUser()->can('modules_client_create') && (!$relatedForm || ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL && !$useModal))) ? [
                                'append' => [
                                    'content' => [
                                        ClassDispenser::getMappedClass(RelatedForms::class)::widget([
                                            'relatedController' => '/customer/client',
                                            'type' => $relatedTypeForm,
                                            'selector' => 'client_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'primaryKey' => 'id',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    ) . '
                    
                
                <div id="client_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' . 
                    (taktwerk\yiiboilerplate\modules\customer\models\Client::findOne(['client.id' => $model->client_id])
                        ->entryDetails == '' ?
                    ' display:none;' :
                    '') . '
                    ">' . 
                    (taktwerk\yiiboilerplate\modules\customer\models\Client::findOne(['client.id' => $model->client_id])->entryDetails != '' ?
                    taktwerk\yiiboilerplate\modules\customer\models\Client::findOne(['client.id' => $model->client_id])->entryDetails :
                    '') . ' 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>':
HTML::activeHiddenInput(
                $model,
                'client_id',
                [
                    'id' => Html::getInputId($model, 'client_id') . $owner,
                    
                ]
            ),
            'short_name' => 
            $form->field(
                $model,
                'short_name',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'short_name') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('short_name'),
                            'id' => Html::getInputId($model, 'short_name') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('short_name')),
            'title' => 
            $form->field(
                $model,
                'title',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'title') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('title'),
                            'id' => Html::getInputId($model, 'title') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('title')),
            'description' => 
            $form->field(
                $model,
                'description',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'description') . $owner
                    ]
                ]
            )
                    ->textarea(
                        [
                            'rows' => 6,
                            'placeholder' => $model->getAttributePlaceholder('description'),
                            'id' => Html::getInputId($model, 'description') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('description')),
            'preview_file' => 
            $form->field(
                $model,
                'preview_file',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'preview_file') . $owner
                    ]
                ]
            )->widget(
                ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\FileInput::class),
                [
                    'pluginOptions' => [
                        'showUpload' => false,
                        'showRemove' => false,
                        
                        
                        'allowedFileExtensions' => ["jpg", "jpeg", "png", "webp"],
                                   'initialPreview' => (!empty($model->preview_file) ? [
                            !empty($model->getMinFilePath('preview_file')  && $model->getFileType('preview_file') == 'video') ? $model->getMinFilePath('preview_file') : $model->getFileUrl('preview_file',[],true)
                        ] : ''),
                        'initialCaption' => $model->preview_file,
                        'initialPreviewAsData' => true,
                        'initialPreviewFileType' => $model->getFileType('preview_file'),
                        'fileActionSettings' => [
                            'indicatorNew' => $model->preview_file === null ? '' : '<a href=" ' . (!empty($model->getMinFilePath('preview_file') && $model->getFileType('preview_file') == 'video') ? $model->getMinFilePath('preview_file') : $model->getFileUrl('preview_file')) . '" target="_blank"><i class="glyphicon glyphicon-hand-down text-warning"></i></a>',
                            'indicatorNewTitle' => \Yii::t('twbp','Download'),
                            'showDrag'=>false,
                        ],
                        'overwriteInitial' => true,
                         'initialPreviewConfig'=> [
                                                     [
                                                         'url' => Url::toRoute(['delete-file','id'=>$model->getPrimaryKey(), 'attribute' => 'preview_file']),
                                                         'downloadUrl'=> !empty($model->getMinFilePath('preview_file')  && $model->getFileType('preview_file') == 'video') ? $model->getMinFilePath('preview_file') : $model->getFileUrl('preview_file'),
                                                         'filetype' => !empty($model->getMinFilePath('preview_file') && $model->getFileType('preview_file') == 'video') ? 'video/mp4':'',
                                                     ],
                                                 ],
                    ],
                     'pluginEvents' => [
                         'fileclear' => 'function() { var prev = $("input[name=\'" + $(this).attr("name") + "\']")[0]; $(prev).val(-1); }',
                        
                        'filepredelete'=>'function(xhr){ 
                               var abort = true; 
                               if (confirm("Are you sure you want to remove this file? This action will delete the file even without pressing the save button")) { 
                                   abort = false; 
                               } 
                               return abort; 
                        }', 
                     ],
                    'options' => [
                        'id' => Html::getInputId($model, 'preview_file') . $owner
                    ]
                ]
            )
            ->hint($model->getAttributeHint('preview_file')),
            'is_collection' => 
            $form->field(
                $model,
                'is_collection',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'is_collection') . $owner
                    ]
                ]
            )
                     ->checkbox(
                         [
                             'id' => Html::getInputId($model, 'is_collection') . $owner
                         ]
                     )
                    ->hint($model->getAttributeHint('is_collection')),
            'revision_term' => 
            $form->field(
                $model,
                'revision_term',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'revision_term') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('revision_term'),
                            'id' => Html::getInputId($model, 'revision_term') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('revision_term')),
            'revision_counter' => 
            $form->field(
                $model,
                'revision_counter',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'revision_counter') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'type' => 'number',
                            'step' => '1',
                            'placeholder' => $model->getAttributePlaceholder('revision_counter'),
                            
                            'id' => Html::getInputId($model, 'revision_counter') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('revision_counter')),
            'duration' => 
            $form->field(
                $model,
                'duration',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'duration') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'type' => 'number',
                            'step' => '1',
                            'placeholder' => $model->getAttributePlaceholder('duration'),
                            
                            'id' => Html::getInputId($model, 'duration') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('duration')),
            'template_id' => 
            $form->field(
                $model,
                'template_id'
            )
                    ->widget(
                        Select2::class,
                        [
                            'data' => taktwerk\yiiboilerplate\modules\guide\models\GuideTemplate::find()->count() > 50 ? null : ArrayHelper::map(taktwerk\yiiboilerplate\modules\guide\models\GuideTemplate::find()->groupBy('guide_template.id')->all(), 'id', 'toString'),
                                    'initValueText' => taktwerk\yiiboilerplate\modules\guide\models\GuideTemplate::find()->groupBy('guide_template.id')->count() > 50 ? \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\modules\guide\models\GuideTemplate::find()->andWhere(['guide_template.id' => $model->template_id])->groupBy('guide_template.id')->all(), 'id', 'toString') : '',
                            'options' => [
                                'placeholder' => Yii::t('twbp', 'Select a value...'),
                                'id' => 'template_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                (taktwerk\yiiboilerplate\modules\guide\models\GuideTemplate::find()->groupBy('guide_template.id')->count() > 50 ? 'minimumInputLength' : '') => 3,
                                (taktwerk\yiiboilerplate\modules\guide\models\GuideTemplate::find()->groupBy('guide_template.id')->count() > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'GuideTemplate\', page:params.page || 1
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                                    var data_id = $(this);
                                $.post('" .
            Url::toRoute('/guide/guide-template/system-entry?id=', true) .
            "' + data_id.val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if(!json.success){
                                                if ((data_id.val() != null) && (data_id.val() != '')) {
                                        // Enable edit icon
                                        $(data_id.next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
            Url::toRoute('/guide/guide-template/entry-details?id=', true) .
            "' + data_id.val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#template_id_well').html(json.data);
                                                //$('#template_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $(data_id.next().next().children('button')[0]).prop('disabled', true);
                                    }
                                            }else{
                                        // Disable edit icon and remove sub-form
                                        $(data_id.next().next().children('button')[0]).prop('disabled', true);
                                            }
                                        });
                                }",
                            ],
                            'addon' => (Yii::$app->getUser()->can('modules_guide-template_create') && (!$relatedForm || ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL && !$useModal))) ? [
                                'append' => [
                                    'content' => [
                                        ClassDispenser::getMappedClass(RelatedForms::class)::widget([
                                            'relatedController' => '/guide/guide-template',
                                            'type' => $relatedTypeForm,
                                            'selector' => 'template_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'primaryKey' => 'id',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    ) . '
                    
                
                <div id="template_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' . 
                    (taktwerk\yiiboilerplate\modules\guide\models\GuideTemplate::findOne(['guide_template.id' => $model->template_id])
                        ->entryDetails == '' ?
                    ' display:none;' :
                    '') . '
                    ">' . 
                    (taktwerk\yiiboilerplate\modules\guide\models\GuideTemplate::findOne(['guide_template.id' => $model->template_id])->entryDetails != '' ?
                    taktwerk\yiiboilerplate\modules\guide\models\GuideTemplate::findOne(['guide_template.id' => $model->template_id])->entryDetails :
                    '') . ' 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>',
            'protocol_template_id' => 
            $form->field(
                $model,
                'protocol_template_id'
            )
                    ->widget(
                        Select2::class,
                        [
                            'data' => taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::find()->count() > 50 ? null : ArrayHelper::map(taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::find()->groupBy('protocol_template.id')->all(), 'id', 'toString'),
                                    'initValueText' => taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::find()->groupBy('protocol_template.id')->count() > 50 ? \yii\helpers\ArrayHelper::map(taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::find()->andWhere(['protocol_template.id' => $model->protocol_template_id])->groupBy('protocol_template.id')->all(), 'id', 'toString') : '',
                            'options' => [
                                'placeholder' => Yii::t('twbp', 'Select a value...'),
                                'id' => 'protocol_template_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                (taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::find()->groupBy('protocol_template.id')->count() > 50 ? 'minimumInputLength' : '') => 3,
                                (taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::find()->groupBy('protocol_template.id')->count() > 50 ? 'ajax' : '') => [
                                    'url' => \yii\helpers\Url::to(['list']),
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) {
                                        return {
                                            q:params.term, m: \'ProtocolTemplate\', page:params.page || 1
                                        };
                                    }')
                                ],
                            ],
                            'pluginEvents' => [
                                "change" => "function() {
                                    var data_id = $(this);
                                $.post('" .
            Url::toRoute('/protocol/protocol-template/system-entry?id=', true) .
            "' + data_id.val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if(!json.success){
                                                if ((data_id.val() != null) && (data_id.val() != '')) {
                                        // Enable edit icon
                                        $(data_id.next().next().children('button')[0]).prop('disabled', false);
                                        $.post('" .
            Url::toRoute('/protocol/protocol-template/entry-details?id=', true) .
            "' + data_id.val(), {
                                            dataType: 'json'
                                        })
                                        .done(function(json) {
                                            var json = $.parseJSON(json);
                                            if (json.data != '') {
                                                $('#protocol_template_id_well').html(json.data);
                                                //$('#protocol_template_id_well').show();
                                            }
                                        })
                                    } else {
                                        // Disable edit icon and remove sub-form
                                        $(data_id.next().next().children('button')[0]).prop('disabled', true);
                                    }
                                            }else{
                                        // Disable edit icon and remove sub-form
                                        $(data_id.next().next().children('button')[0]).prop('disabled', true);
                                            }
                                        });
                                }",
                            ],
                            'addon' => (Yii::$app->getUser()->can('modules_protocol-template_create') && (!$relatedForm || ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL && !$useModal))) ? [
                                'append' => [
                                    'content' => [
                                        ClassDispenser::getMappedClass(RelatedForms::class)::widget([
                                            'relatedController' => '/protocol/protocol-template',
                                            'type' => $relatedTypeForm,
                                            'selector' => 'protocol_template_id' . ($ajax || $useModal ? '_ajax_' . $owner : ''),
                                            'primaryKey' => 'id',
                                        ]),
                                    ],
                                    'asButton' => true
                                ],
                            ] : []
                        ]
                    ) . '
                    
                
                <div id="protocol_template_id_well" class="well col-sm-6 hidden"
                    style="margin-left: 8px; display: none;' . 
                    (taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::findOne(['protocol_template.id' => $model->protocol_template_id])
                        ->entryDetails == '' ?
                    ' display:none;' :
                    '') . '
                    ">' . 
                    (taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::findOne(['protocol_template.id' => $model->protocol_template_id])->entryDetails != '' ?
                    taktwerk\yiiboilerplate\modules\protocol\models\ProtocolTemplate::findOne(['protocol_template.id' => $model->protocol_template_id])->entryDetails :
                    '') . ' 
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 sub-form" id="address_id_inline" style="display: none;">
                </div>',
            'all_step_files_size' => 
            $form->field(
                $model,
                'all_step_files_size',
                [
                    'selectors' => [
                        'input' => '#' .
                            Html::getInputId($model, 'all_step_files_size') . $owner
                    ]
                ]
            )
                    ->textInput(
                        [
                            'maxlength' => true,
                            'placeholder' => $model->getAttributePlaceholder('all_step_files_size'),
                            'id' => Html::getInputId($model, 'all_step_files_size') . $owner
                        ]
                    )
                    ->hint($model->getAttributeHint('all_step_files_size')),]; ?>        <div class='clearfix'></div>
<?php $twoColumnsForm = true; ?>
<?php 
// form fields overwrite
$fieldColumns = Yii::$app->controller->crudColumnsOverwrite($fieldColumns, 'form', $model, $form, $owner);

foreach($fieldColumns as $attribute => $fieldColumn) {
             if($model->checkIfHidden($attribute))
            {
                echo $fieldColumn;
            }
            else if(!$model->isNewRecord || !$model->checkIfCanvas($attribute))
            {
                if (!$multiple || ($multiple && isset($show[$attribute]))) {
                    if ((isset($show[$attribute]) && $show[$attribute]) || !isset($show[$attribute])) {
                        echo $twoColumnsForm ? '<div class="col-md-6 form-group-block">' : '';
                        echo $fieldColumn;
                        echo $twoColumnsForm ? '</div>' : '';
                    }else{
                        echo $fieldColumn;
                    }
                }
            }
                
} ?><div class='clearfix'></div>

                <?php $this->endBlock(); ?>
        
        <?= ($relatedType != ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_TAB) ?
            Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                    'label' => Yii::t('twbp', 'Guide'),
                    'content' => $this->blocks['main'],
                    'active' => true,
                ],
                ]
            ])
            : $this->blocks['main']
        ?>        
        <div class="col-md-12">
        <hr/>
        
        </div>
        
        <div class="clearfix"></div>
        <?php echo $form->errorSummary($model); ?>
        <?php echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::formButtons($model, $useModal, $relatedForm, $multiple, "twbp", ['id' => $model->id]);?>
        <?php ClassDispenser::getMappedClass(ActiveForm::class)::end(); ?>
        <?= ($relatedForm && $relatedType == ClassDispenser::getMappedClass(RelatedForms::class)::TYPE_MODAL) ?
        '<div class="clearfix"></div>' :
        ''
        ?>
        <div class="clearfix"></div>
    </div>
</div>

<?php
if ($useModal) {
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ModalFormAsset::class)::register($this);
}
ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
if(!isset($useDependentFieldsJs) || $useDependentFieldsJs==true){
$this->render('@taktwerk-views/_dependent-fields', ['model' => $model,'formId'=>$formId]);
}
?>