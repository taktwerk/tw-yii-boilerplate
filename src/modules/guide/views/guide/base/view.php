<?php
//Generation Date: 27-May-2021 08:48:13am
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\guide\models\Guide $model
 * @var boolean $useModal
 */

$this->title = Yii::t('twbp', 'Guide') . ' #' . (is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey) . ', ' . Yii::t('twbp', 'View') . ' - ' . $model->toString();
if(!$fromRelation) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('twbp', 'Guides'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => (string)is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('twbp', 'View');
}
$basePermission = Yii::$app->controller->module->id . '_' . Yii::$app->controller->id;

Yii::$app->controller->renderCustomBlocks(Yii::$app->controller::POS_MAIN);

?>
<div class="box box-default">
	<div class="giiant-crud box-body"
		id="guide-view">

		<!-- flash message -->
        <?php if (Yii::$app->session->getFlash('deleteError') !== null) : ?>
            <span class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= Yii::$app->session->getFlash('deleteError') ?>
            </span>
        <?php endif; ?>
        <?php if (!$useModal) : ?>
            <div class="clearfix crud-navigation">
			<!-- menu buttons -->
			<div class='pull-left'>
			<?php if (!$model->deleted_at!=null) : ?>
                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_update'
                    ) && $model->editable() ?
                        Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span> ' . Yii::t('twbp', 'Edit'),
                            [
                                'update',
                                'id' => $model->id,                                 'fromRelation' => $fromRelation,
                                'show_deleted'=>Yii::$app->request->get('show_deleted')
                            ],
                            ['class' => 'btn btn-info']
                        )
                    :
                        null
                    ?>
                     <?php if(method_exists($model,'getUploadFields')  && Yii::$app->getUser()->can(
                        $basePermission . '_recompress'
                    )){
                    	$fields = $model->getUploadFields();
                    	$isCompressible = false;
                    	$compressibleFileTypes = $model::$compressibleFileTypes;
                    	foreach($fields as $field){
                        	if(in_array($model->getFileType($field),$compressibleFileTypes)){
                            	$isCompressible = true;
                            	break;
                        	}
                    	}
                    if($isCompressible){	
                       echo Html::a(
                            '<span class="glyphicon glyphicon-compressed"></span> ' . Yii::t('twbp', 'Recompress'),
                            ['recompress', 'id' => $model->id, 'fromRelation' => $fromRelation],
                            ['class' => 'btn btn-warning','title'=>Yii::t('twbp', 'Adds a compression job for this item\'s files')]
                        );}
                   
                    }?>

                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_create'
                    ) ?
                        Html::a(
                            '<span class="glyphicon glyphicon-copy"></span> ' . Yii::t('twbp', 'Copy'),
                            ['create', 'id' => $model->id, 'fromRelation' => $fromRelation],
                            ['class' => 'btn btn-success']
                        )
                    :
                        null
                    ?>
                    <?php endif; ?>
                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_create'
                    ) ?
                        Html::a(
                            '<span class="glyphicon glyphicon-plus"></span>
				' . Yii::t('twbp', 'New'), ['create', 'fromRelation' =>
				$fromRelation], ['class' => 'btn btn-success create-new'] ) : null
				?>
			</div>
			<div class="pull-right">
			<?php if (!$model->deleted_at!=null) : ?>
                    <?= \taktwerk\yiiboilerplate\widget\CustomActionDropdownWidget::widget(['model'=>$model]) ?>
                    <?php if(Yii::$app->controller->crudMainButtons['listall']): ?>
                    <?= Html::a(
                        '<span class="glyphicon glyphicon-list"></span> '. Yii::t('twbp', 'List {model}',
                            ['model' => \Yii::t('twbp','Guides')]                        ),
                        ['index'],
                        ['class' => 'btn btn-default']
                    ) ?>
                    <?php endif; ?>
            <?php endif; ?>                </div>
		</div>
        <?php endif; ?>
        <?php $this->beginBlock('taktwerk\yiiboilerplate\modules\guide\models\Guide'); ?>

        <?php $viewColumns = [
                   [
            'attribute'=> 'id'
           ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::attributeFormat
                [
                    'format' => 'html',
                    'attribute' => 'client_id',
                    'visible' => Yii::$app->getUser()->can('Authority'),
        
                    'value' => function ($model) {
                        $foreign = $model->getClient()->one();
                        if ($foreign) {
                            if (Yii::$app->getUser()->can('app_client_view') && $foreign->readable()) {
                                return Html::a($foreign->toString, [
                                    'client/view',
                                    'id' => $model->getClient()->one()->id,
                                ], ['data-pjax' => 0]);
                            }
                            return $foreign->toString;
                        }
                        return '<span class="label label-warning">?</span>';
                    },
                ],
[
        'attribute' => 'short_name',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->short_name != strip_tags($model->short_name)){
                return \yii\helpers\StringHelper::truncate($model->short_name,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'short_name'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'short_name'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->short_name,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'short_name'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'short_name'])]])));
            }
        },
    ],
[
        'attribute' => 'title',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->title != strip_tags($model->title)){
                return \yii\helpers\StringHelper::truncate($model->title,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'title'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'title'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->title,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'title'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'title'])]])));
            }
        },
    ],
[
        'attribute' => 'description',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->description != strip_tags($model->description)){
                return \yii\helpers\StringHelper::truncate($model->description,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'description'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'description'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->description,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'description'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'description'])]])));
            }
        },
    ],
[
        'attribute' => 'preview_file',
        'format' => 'html',
        'value' => function ($model) {
                return $model->showFilePreviewWithLink('preview_file');
                },
    ],
                'is_collection:boolean',
[
        'attribute' => 'revision_term',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->revision_term != strip_tags($model->revision_term)){
                return \yii\helpers\StringHelper::truncate($model->revision_term,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'revision_term'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'revision_term'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->revision_term,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'revision_term'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'revision_term'])]])));
            }
        },
    ],
[
        'attribute' => 'revision_counter',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->revision_counter != strip_tags($model->revision_counter)){
                return \yii\helpers\StringHelper::truncate($model->revision_counter,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'revision_counter'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'revision_counter'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->revision_counter,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'revision_counter'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'revision_counter'])]])));
            }
        },
    ],
[
        'attribute' => 'duration',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->duration != strip_tags($model->duration)){
                return \yii\helpers\StringHelper::truncate($model->duration,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'duration'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'duration'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->duration,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'duration'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'duration'])]])));
            }
        },
    ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::attributeFormat
                [
                    'format' => 'html',
                    'attribute' => 'template_id',
                    'value' => function ($model) {
                        $foreign = $model->getTemplate()->one();
                        if ($foreign) {
                            if (Yii::$app->getUser()->can('app_guide-template_view') && $foreign->readable()) {
                                return Html::a($foreign->toString, [
                                    'guide-template/view',
                                    'id' => $model->getTemplate()->one()->id,
                                ], ['data-pjax' => 0]);
                            }
                            return $foreign->toString;
                        }
                        return '<span class="label label-warning">?</span>';
                    },
                ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::attributeFormat
                [
                    'format' => 'html',
                    'attribute' => 'protocol_template_id',
                    'value' => function ($model) {
                        $foreign = $model->getProtocolTemplate()->one();
                        if ($foreign) {
                            if (Yii::$app->getUser()->can('app_protocol-template_view') && $foreign->readable()) {
                                return Html::a($foreign->toString, [
                                    'protocol-template/view',
                                    'id' => $model->getProtocolTemplate()->one()->id,
                                ], ['data-pjax' => 0]);
                            }
                            return $foreign->toString;
                        }
                        return '<span class="label label-warning">?</span>';
                    },
                ],
[
        'attribute' => 'all_step_files_size',
        'format' => 'html',
        'value' => function ($model) {
                return $model->showFilePreviewWithLink('all_step_files_size');
                },
    ],
        ];

        // view columns overwrite
        $viewColumns = Yii::$app->controller->crudColumnsOverwrite($viewColumns, 'view');

        echo DetailView::widget([
        'model' => $model,
        'attributes' => $viewColumns
        ]); ?>

        
        <hr />

        <?=  ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::deleteButton($model, "twbp", ['id' => $model->id], 'view')?>

        <?php $this->endBlock(); ?>


        
        <?php $this->beginBlock('Categories'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsCategories = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('guide_guide-category_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('guide_guide-category_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('guide_guide-category_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/guide/guide-category' . '/' . $action;
                        $params['GuideCategory'] = [
                            'id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'guide-category'
                ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'client_id',
                    'value' => function ($model) {
                        if ($rel = $model->getClient()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'client/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'name',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'name'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'name'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'name'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'name'])]])));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\guide\models\GuideCategory::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsCategories = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsCategories, 'tab'):$columnsCategories;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('guide_guide-category_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Category'),
                [
                    '/guide/guide-category/create',
                    'GuideCategory' => [
                        'id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getCategories(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-categories',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsCategories
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Groups'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsGroups = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('guide_group_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('guide_group_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('guide_group_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/user/group' . '/' . $action;
                        $params['Group'] = [
                            'client_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'group'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'name',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'name'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'name'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'name'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'name'])]])));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\user\models\Group::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsGroups = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGroups, 'tab'):$columnsGroups;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('guide_group_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Group'),
                [
                    '/user/group/create',
                    'Group' => [
                        'client_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getGroups(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-groups',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsGroups
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Guide Steps Order By Steps'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsGuideStepsOrderBySteps = [[       'headerOptions'=>['style'=>'width:60px'],
        'contentOptions'=>['align'=>'center'],
        'content'=>function($model){
                $spn = Html::tag('span','',['class'=>"glyphicon glyphicon-resize-vertical"]);
                $tspn = Html::tag('span',$model->order_number,['class'=>'label']);
            return Html::a($spn.' '.$tspn,'javascript:void(0)',['class'=>"sortable-row btn btn-default",'title'=>\Yii::t('twbp','Drag to Re-order')]);
        }
],
[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('guide_guide-step_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('guide_guide-step_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('guide_guide-step_recompress') ? '{recompress} ' : '') . (Yii::$app->getUser()->can('guide_guide-step_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/guide/guide-step' . '/' . $action;
                        $params['GuideStep'] = [
                            'guide_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'guide-step'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'title',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->title != strip_tags($model->title)){
                return \yii\helpers\StringHelper::truncate($model->title,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'title'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'title'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->title,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'title'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'title'])]])));
            }
        },
    ],
[
        'attribute' => 'description_html',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->description_html != strip_tags($model->description_html)){
                return \yii\helpers\StringHelper::truncate($model->description_html,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'description_html'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'description_html'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->description_html,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'description_html'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'description_html'])]])));
            }
        },
    ],
[
        'attribute' => 'attached_file',
        'format' => 'html',
        'value' => function ($model) {
                return $model->showFilePreviewWithLink('attached_file');
                },
    ],
[
        'attribute' => 'design_canvas',
        'format' => 'html',
        'value' => function ($model) {
            return $model->showFilePreview('design_canvas');
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\guide\models\GuideStep::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsGuideStepsOrderBySteps = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGuideStepsOrderBySteps, 'tab'):$columnsGuideStepsOrderBySteps;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\SortableGridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('guide_guide-step_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Guide Steps Order By Step'),
                [
                    '/guide/guide-step/create',
                    'GuideStep' => [
                        'guide_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getGuideStepsOrderBySteps()->orderBy('order_number asc'),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-guidestepsorderbysteps',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            'sortUrl' => Url::toRoute(['guide-step/sort-item']),
            'columns' => $columnsGuideStepsOrderBySteps
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Guide Asset Pivots'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsGuideAssetPivots = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('guide_guide-asset-pivot_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('guide_guide-asset-pivot_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('guide_guide-asset-pivot_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/guide/guide-asset-pivot' . '/' . $action;
                        $params['GuideAssetPivot'] = [
                            'guide_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'guide-asset-pivot'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'guide_asset_id',
                    'value' => function ($model) {
                        if ($rel = $model->getGuideAsset()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'guide-asset/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\guide\models\GuideAssetPivot::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsGuideAssetPivots = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGuideAssetPivots, 'tab'):$columnsGuideAssetPivots;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('guide_guide-asset-pivot_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Guide Asset Pivot'),
                [
                    '/guide/guide-asset-pivot/create',
                    'GuideAssetPivot' => [
                        'guide_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getGuideAssetPivots(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-guideassetpivots',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsGuideAssetPivots
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Guide Category Bindings'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsGuideCategoryBindings = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('guide_guide-category-binding_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('guide_guide-category-binding_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('guide_guide-category-binding_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/guide/guide-category-binding' . '/' . $action;
                        $params['GuideCategoryBinding'] = [
                            'guide_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'guide-category-binding'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'guide_category_id',
                    'value' => function ($model) {
                        if ($rel = $model->getGuideCategory()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'guide-category/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\guide\models\GuideCategoryBinding::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsGuideCategoryBindings = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGuideCategoryBindings, 'tab'):$columnsGuideCategoryBindings;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('guide_guide-category-binding_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Guide Category Binding'),
                [
                    '/guide/guide-category-binding/create',
                    'GuideCategoryBinding' => [
                        'guide_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getGuideCategoryBindings(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-guidecategorybindings',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsGuideCategoryBindings
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Guide Childre Parent Guides'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsGuideChildren0 = [[       'headerOptions'=>['style'=>'width:60px'],
        'contentOptions'=>['align'=>'center'],
        'content'=>function($model){
                $spn = Html::tag('span','',['class'=>"glyphicon glyphicon-resize-vertical"]);
                $tspn = Html::tag('span',$model->order_number,['class'=>'label']);
            return Html::a($spn.' '.$tspn,'javascript:void(0)',['class'=>"sortable-row btn btn-default",'title'=>\Yii::t('twbp','Drag to Re-order')]);
        }
],
[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('guide_guide-child_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('guide_guide-child_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('guide_guide-child_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/guide/guide-child' . '/' . $action;
                        $params['GuideChild'] = [
                            'parent_guide_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'guide-child'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'guide_id',
                    'value' => function ($model) {
                        if ($rel = $model->getGuide()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'guide/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\guide\models\GuideChild::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsGuideChildren0 = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGuideChildren0, 'tab'):$columnsGuideChildren0;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\SortableGridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('guide_guide-child_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Guide Children0'),
                [
                    '/guide/guide-child/create',
                    'GuideChild' => [
                        'parent_guide_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getGuideChildren0()->orderBy('order_number asc'),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-guidechildren0',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            'sortUrl' => Url::toRoute(['guide-child/sort-item']),
            'columns' => $columnsGuideChildren0
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Guide Datas'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsGuideDatas = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('guide_guide-data_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('guide_guide-data_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('guide_guide-data_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/guide/guide-data' . '/' . $action;
                        $params['GuideData'] = [
                            'guide_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'guide-data'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'client_model_type_id',
                    'value' => function ($model) {
                        if ($rel = $model->getClientModelType()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'client-model-type/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'type_ref',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->type_ref != strip_tags($model->type_ref)){
                return \yii\helpers\StringHelper::truncate($model->type_ref,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'type_ref'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'type_ref'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->type_ref,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'type_ref'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'type_ref'])]])));
            }
        },
    ],
[
        'attribute' => 'amount',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->amount != strip_tags($model->amount)){
                return \yii\helpers\StringHelper::truncate($model->amount,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'amount'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'amount'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->amount,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'amount'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'amount'])]])));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\guide\models\GuideData::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsGuideDatas = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGuideDatas, 'tab'):$columnsGuideDatas;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('guide_guide-data_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Guide Data'),
                [
                    '/guide/guide-data/create',
                    'GuideData' => [
                        'guide_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getGuideDatas(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-guidedatas',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsGuideDatas
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Guide Steps'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsGuideSteps = [[       'headerOptions'=>['style'=>'width:60px'],
        'contentOptions'=>['align'=>'center'],
        'content'=>function($model){
                $spn = Html::tag('span','',['class'=>"glyphicon glyphicon-resize-vertical"]);
                $tspn = Html::tag('span',$model->order_number,['class'=>'label']);
            return Html::a($spn.' '.$tspn,'javascript:void(0)',['class'=>"sortable-row btn btn-default",'title'=>\Yii::t('twbp','Drag to Re-order')]);
        }
],
[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('guide_guide-step_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('guide_guide-step_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('guide_guide-step_recompress') ? '{recompress} ' : '') . (Yii::$app->getUser()->can('guide_guide-step_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/guide/guide-step' . '/' . $action;
                        $params['GuideStep'] = [
                            'guide_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'guide-step'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'title',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->title != strip_tags($model->title)){
                return \yii\helpers\StringHelper::truncate($model->title,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'title'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'title'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->title,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'title'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'title'])]])));
            }
        },
    ],
[
        'attribute' => 'description_html',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->description_html != strip_tags($model->description_html)){
                return \yii\helpers\StringHelper::truncate($model->description_html,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'description_html'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'description_html'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->description_html,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'description_html'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'description_html'])]])));
            }
        },
    ],
[
        'attribute' => 'attached_file',
        'format' => 'html',
        'value' => function ($model) {
                return $model->showFilePreviewWithLink('attached_file');
                },
    ],
[
        'attribute' => 'design_canvas',
        'format' => 'html',
        'value' => function ($model) {
            return $model->showFilePreview('design_canvas');
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\guide\models\GuideStep::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsGuideSteps = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGuideSteps, 'tab'):$columnsGuideSteps;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\SortableGridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('guide_guide-step_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Guide Step'),
                [
                    '/guide/guide-step/create',
                    'GuideStep' => [
                        'guide_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getGuideSteps()->orderBy('order_number asc'),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-guidesteps',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            'sortUrl' => Url::toRoute(['guide-step/sort-item']),
            'columns' => $columnsGuideSteps
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Guide Tags'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsGuideTags = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('guide_guide-tag_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('guide_guide-tag_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('guide_guide-tag_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/guide/guide-tag' . '/' . $action;
                        $params['GuideTag'] = [
                            'guide_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'guide-tag'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
[
        'attribute' => 'name',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'name'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'name'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'name'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'name'])]])));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\guide\models\GuideTag::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsGuideTags = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGuideTags, 'tab'):$columnsGuideTags;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('guide_guide-tag_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Guide Tag'),
                [
                    '/guide/guide-tag/create',
                    'GuideTag' => [
                        'guide_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getGuideTags(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-guidetags',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsGuideTags
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Guide View Histories'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsGuideViewHistories = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('guide_guide-view-history_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('guide_guide-view-history_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('guide_guide-view-history_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/guide/guide-view-history' . '/' . $action;
                        $params['GuideViewHistory'] = [
                            'guide_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'guide-view-history'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'client_id',
                    'value' => function ($model) {
                        if ($rel = $model->getClient()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'client/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'user_id',
                    'value' => function ($model) {
                        if ($rel = $model->getUser()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'user/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'data',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->data != strip_tags($model->data)){
                return \yii\helpers\StringHelper::truncate($model->data,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'data'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'data'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->data,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'data'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'data'])]])));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\guide\models\GuideViewHistory::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsGuideViewHistories = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGuideViewHistories, 'tab'):$columnsGuideViewHistories;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('guide_guide-view-history_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Guide View History'),
                [
                    '/guide/guide-view-history/create',
                    'GuideViewHistory' => [
                        'guide_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getGuideViewHistories(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-guideviewhistories',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsGuideViewHistories
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Production Line Histories'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsProductionLineHistories = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('guide_production-line-history_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('guide_production-line-history_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('guide_production-line-history_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/guide/production-line-history' . '/' . $action;
                        $params['ProductionLineHistory'] = [
                            'guide_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'production-line-history'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'production_line_id',
                    'value' => function ($model) {
                        if ($rel = $model->getProductionLine()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'production-line/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
[
        'attribute' => 'reference',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->reference != strip_tags($model->reference)){
                return \yii\helpers\StringHelper::truncate($model->reference,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'reference'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'reference'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->reference,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'reference'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'reference'])]])));
            }
        },
    ],
[
        'attribute' => 'reference_qrscan',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->reference_qrscan != strip_tags($model->reference_qrscan)){
                return \yii\helpers\StringHelper::truncate($model->reference_qrscan,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'reference_qrscan'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'reference_qrscan'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->reference_qrscan,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'reference_qrscan'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'reference_qrscan'])]])));
            }
        },
    ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\guide\models\ProductionLineHistory::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsProductionLineHistories = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsProductionLineHistories, 'tab'):$columnsProductionLineHistories;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('guide_production-line-history_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Production Line History'),
                [
                    '/guide/production-line-history/create',
                    'ProductionLineHistory' => [
                        'guide_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getProductionLineHistories(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-productionlinehistories',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsProductionLineHistories
        ])?>
        </div>
                <?php $this->endBlock() ?>

<?php /* MAPPED MODELS RELATIONS - START*/?>
<?php 
$extraTabItems = [];
foreach(\Yii::$app->modelNewRelationMapper->newRelationsMapping as $modelClass=>$relations){
    if(is_a($model,$modelClass)){
        foreach($relations as $relName => $relation){
            $relationKey = array_key_first($relation[1]);
            $foreignKey= $relation[1][$relationKey];
            $relClass = $relation[0];
            $baseName = \yii\helpers\StringHelper::basename($relClass);
            $cntrler = Inflector::camel2id($baseName, '-', true);
            $pluralName= Inflector::camel2words(Inflector::pluralize($baseName));
            $singularName = Inflector::camel2words(Inflector::singularize($baseName));
            $basePerm =  \Yii::$app->controller->module->id.'_'.$cntrler;
            $this->beginBlock($pluralName);
?>
<?php 
$relatedModelObject = Yii::createObject($relClass::className());
$relationController = $relatedModelObject->getControllerInstance();
//TODO CrudHelper::getColumnFormat('\app\modules\guide\models\Guide', 'id');
?>
                    <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
       	<div class="clearfix"></div>
        <div>
        <?php $relMapCols = [[
                    'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can($basePerm.'_view') ? '{view} ' : '') . (Yii::$app->getUser()->can($basePerm.'_update') ? '{update} ' : '') . (Yii::$app->getUser()->can($basePerm.'_delete') ? '{delete} ' : ''),
            'urlCreator' => function ($action, $relation_model, $key, $index) use($model,$relationKey,$foreignKey,$baseName,$relatedBaseUrl) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = $relatedBaseUrl . $action;
                        
                        $params[$baseName] = [
                            $relationKey => $model->{$foreignKey}
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) { 
                             return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => $cntrler
                ],
                
        ];

        $relMapCols =array_merge($relMapCols,array_slice(array_keys($relatedModelObject->attributes),0,5));
        
        $relMapCols = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($relMapCols, 'tab'):$relMapCols;
        
        echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can($basePerm.'_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp',$singularName),
                [
                    '/'.$relationController->module->id.'/'.$cntrler.'/create',
                    $baseName => [
                        $relationKey => $model->{$foreignKey}
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[
                'class'=>'grid-outer-div'
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->{'get'.$relName}(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-'.$relName,
                    ]
                ]
                ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            'columns' =>$relMapCols
        ])?>
        </div>
                <?php $this->endBlock();
                $extraTabItems[] = [
                    'content' => $this->blocks[$pluralName],
                    'label' => '<small>' .
                    \Yii::t('twbp', $pluralName) .
                    '&nbsp;<span class="badge badge-default">' .
                    $model->{'get'.$relName}()->count() .
                    '</span></small>',
                    'active' => false,
                    'visible' =>
                    Yii::$app->user->can('x_'.$basePerm.'_see') && Yii::$app->controller->crudRelations(ucfirst($relName)),
                    ];
?>

<?php 
        }
    }
}
?>

                
<?php /* MAPPED MODELS RELATIONS - END*/ ?>
        <?= Tabs::widget(
            [
                'id' => 'relation-tabs',
                'encodeLabels' => false,
                'items' =>  array_filter(array_merge([
                    [
                        'label' => '<b class=""># ' . $model->id . '</b>',
                        'content' => $this->blocks['taktwerk\yiiboilerplate\modules\guide\models\Guide'],
                        'active' => true,
                    ],
        (\Yii::$app->hasModule('guide'))?[
                        'content' => $this->blocks['Categories'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Categories') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getCategories()->groupBy('guide_category.id')->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_guide_guide-category_see') && Yii::$app->controller->crudRelations('Categories'),
                    ]:null,
        (\Yii::$app->hasModule('user'))?[
                        'content' => $this->blocks['Groups'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Groups') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getGroups()->groupBy('group.id')->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_guide_group_see') && Yii::$app->controller->crudRelations('Groups'),
                    ]:null,
        (\Yii::$app->hasModule('guide'))?[
                        'content' => $this->blocks['Guide Steps Order By Steps'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Guide Steps Order By Steps') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getGuideStepsOrderBySteps()->groupBy('guide_step.id')->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_guide_guide-step_see') && Yii::$app->controller->crudRelations('GuideStepsOrderBySteps'),
                    ]:null,
        (\Yii::$app->hasModule('guide'))?[
                        'content' => $this->blocks['Guide Asset Pivots'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Guide Asset Pivots') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getGuideAssetPivots()->groupBy('guide_asset_pivot.id')->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_guide_guide-asset-pivot_see') && Yii::$app->controller->crudRelations('GuideAssetPivots'),
                    ]:null,
        (\Yii::$app->hasModule('guide'))?[
                        'content' => $this->blocks['Guide Category Bindings'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Guide Category Bindings') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getGuideCategoryBindings()->groupBy('guide_category_binding.id')->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_guide_guide-category-binding_see') && Yii::$app->controller->crudRelations('GuideCategoryBindings'),
                    ]:null,
        (\Yii::$app->hasModule('guide'))?[
                        'content' => $this->blocks['Guide Childre Parent Guides'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Guide Childre Parent Guides') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getGuideChildren0()->groupBy('guide_child.id')->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_guide_guide-child_see') && Yii::$app->controller->crudRelations('GuideChildren0'),
                    ]:null,
        (\Yii::$app->hasModule('guide'))?[
                        'content' => $this->blocks['Guide Datas'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Guide Datas') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getGuideDatas()->groupBy('guide_data.id')->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_guide_guide-data_see') && Yii::$app->controller->crudRelations('GuideDatas'),
                    ]:null,
        (\Yii::$app->hasModule('guide'))?[
                        'content' => $this->blocks['Guide Steps'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Guide Steps') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getGuideSteps()->groupBy('guide_step.id')->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_guide_guide-step_see') && Yii::$app->controller->crudRelations('GuideSteps'),
                    ]:null,
        (\Yii::$app->hasModule('guide'))?[
                        'content' => $this->blocks['Guide Tags'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Guide Tags') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getGuideTags()->groupBy('guide_tag.id')->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_guide_guide-tag_see') && Yii::$app->controller->crudRelations('GuideTags'),
                    ]:null,
        (\Yii::$app->hasModule('guide'))?[
                        'content' => $this->blocks['Guide View Histories'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Guide View Histories') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getGuideViewHistories()->groupBy('guide_view_history.id')->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_guide_guide-view-history_see') && Yii::$app->controller->crudRelations('GuideViewHistories'),
                    ]:null,
        (\Yii::$app->hasModule('guide'))?[
                        'content' => $this->blocks['Production Line Histories'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Production Line Histories') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getProductionLineHistories()->groupBy('production_line_history.id')->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_guide_production-line-history_see') && Yii::$app->controller->crudRelations('ProductionLineHistories'),
                    ]:null,
                    [
                        'content' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\HistoryTab::class)::widget(['model' => $model]),
                        'label' => '<small>' .
                            \Yii::t('twbp', 'History') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getHistory()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('Administrator'),
                    ],
                ]
                ,$extraTabItems))
            ]
        );
        ?>
        <?= ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\RecordHistory::class)::widget(['model' => $model]) ?>
    </div>
</div>

<?php ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
$js = <<<JS
$(document).ready(function() {
    var hash=document.location.hash;
	var prefix="tab_" ;
    if (hash) {
        $('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
    } 
    
    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash=e.target.hash.replace(	"#", "#" + prefix);
    });
});
JS;

$this->registerJs($js);

Yii::$app->controller->renderCustomBlocks(Yii::$app->controller::POS_END);
