<?php
//Generation Date: 28-Sep-2020 06:27:26am
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\guide\models\search\Guide $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="guide-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'client_id') ?>

        <?= $form->field($model, 'short_name') ?>

        <?= $form->field($model, 'title') ?>

        <?= $form->field($model, 'description') ?>

        <?php // echo $form->field($model, 'preview_file') ?>

        <?php // echo $form->field($model, 'preview_file_filemeta') ?>

        <?php // echo $form->field($model, 'is_collection') ?>

        <?php // echo $form->field($model, 'revision_term') ?>

        <?php // echo $form->field($model, 'revision_counter') ?>

        <?php // echo $form->field($model, 'duration') ?>

        <?php // echo $form->field($model, 'template_id') ?>

        <?php // echo $form->field($model, 'protocol_template_id') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

        <?php // echo $form->field($model, 'local_created_at') ?>

        <?php // echo $form->field($model, 'local_updated_at') ?>

        <?php // echo $form->field($model, 'local_deleted_at') ?>

        <?php // echo $form->field($model, 'all_step_files_size') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
