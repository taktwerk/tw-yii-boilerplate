<?php
//Generation Date: 17-Mar-2021 06:57:11am
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\guide\models\search\GuideStep $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="guide-step-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

            <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'guide_id') ?>

        <?= $form->field($model, 'order_number') ?>

        <?= $form->field($model, 'title') ?>

        <?= $form->field($model, 'description_html') ?>

        <?php // echo $form->field($model, 'attached_file') ?>

        <?php // echo $form->field($model, 'attached_file_filemeta') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'deleted_by') ?>

        <?php // echo $form->field($model, 'deleted_at') ?>

        <?php // echo $form->field($model, 'local_created_at') ?>

        <?php // echo $form->field($model, 'local_updated_at') ?>

        <?php // echo $form->field($model, 'local_deleted_at') ?>

        <?php // echo $form->field($model, 'design_canvas') ?>

        <?php // echo $form->field($model, 'design_canvas_filemeta') ?>

        <?php // echo $form->field($model, 'design_canvas_meta') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('twbp', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('twbp', 'Reset Search'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
