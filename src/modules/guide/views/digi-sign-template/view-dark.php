<?php
/* @var $model \taktwerk\yiiboilerplate\modules\digisign\components\DigiSignAbstract */
/* @var $viewwidth integer */
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\widgets\Pjax;
use taktwerk\yiiboilerplate\modules\guide\components\ProductionLineDigiSign;
use taktwerk\yiiboilerplate\helpers\CrudHelper;
use taktwerk\yiiboilerplate\models\TwActiveRecord;
$digiScreenModel = $model->getDigiScreenModel();
$pLine = $digiScreenModel ? $digiScreenModel->productionLine : null;
$guide = null;
if ($pLine) {
    $pLineHistory = $pLine->getProductionLineHistories()
        ->orderBy('created_at desc')
        ->one();
    if ($pLineHistory) {
        $guide = $pLineHistory->guide;
    }
}
$this->title = $digiScreenModel->name;
?>
<?php Pjax::begin(['id'=>'guide-digi-container','enablePushState'=>false,
    'clientOptions' => ['replace'=>false, 'push' => false]]);?>
    <?php
    if ($guide == null){?>
<div class="at-page-center">
    <i class="fa fa-exclamation-triangle"></i> <?php echo \Yii::t('twbp','Guide not found');?>
</div>
<?php }else{?>
    <?php
$gStep = $guide->getGuideSteps()
    ->orderBy('order_number asc')
    ->one();
?>
<header class="header d-flex align-items-center">
	<img
		src="<?php echo Yii::$app->request->baseUrl; ?>/images/theme/LAUFEN_white.png"
		alt="Laufen Bathrooms" /> &nbsp;
	<h3 id="station_title" class="td"><?php echo $model->getScreenTitle()?></h3>
	<div class="spacer flex-1"></div>
	<div id="loader" style="display:none;">
		<div class="spinner">
			<div class="circle circle-1"></div>
			<div class="circle circle-2"></div>
		</div>
	</div>
	<div class="text-right">
		<div id="metainfo_line" class="td">
			<div><?php echo \Yii::t('twbp','Location')?>: <?php echo Html::encode(\Yii::t('twbp',$pLine->name))?></div>
		</div>
	</div>

	<h3 id="clock" class="clock td">
	<?php if(\Yii::$app->request->getBodyParam('time')){?>
	<?php $time = \Yii::$app->request->getBodyParam('time');
	echo $time;
	?>
	<?php }else{?>
		<small><?php echo \Yii::t('twbp','Loading...')?></small>
		<?php }?>
	</h3>
</header>
<div class="d-flex align-items-stretch main">
    <?php if (!$model->getIframeUrl()) : ?>
	<div class="flex-1 d-flex flex-columns">
		<div class="table">
			<div class="tr">
				<div class="td td--data pt-0 pb-0">
					<h2 id="item_title" class="title"><?php echo Html::encode(\Yii::t('twbp',$guide->title))?></h2>
				</div>
			</div>
		</div>
<?php $model->trigger(ProductionLineDigiSign::EVENT_AFTER_TITLE);?>
		<!-- Tables -->
		<div id="tables" class="d-flex flex-1 flex-columns">
		<?php $guideDataModels = $guide->getGuideDatasModels(true,false);
		$tblCount = 0;
		$digiSignDataModels = $model->getDigiSignDataModels();
		//CrudHelper::getUploadFields($class);
		foreach($guideDataModels as $k => $query){
		    if(count($digiSignDataModels)>0 && !in_array($k, $digiSignDataModels)){
		        continue;
            }
		?>
			<div id="data_tables_<?php echo $tblCount?>" class="table flex-11">
				<div id="" class="table__title"><?php echo \Yii::t('app',Inflector::camel2words(StringHelper::basename($k)))?></div>
				<?php $rowCount = 0;?>
				<?php
				/*Getting & displaying upload fields if model class is  a TwActiveRecord object*/
				$uploadFields = is_subclass_of($query->modelClass, TwActiveRecord::class)?CrudHelper::getUploadFields($query->modelClass):[];
				$multiUploadFields = is_subclass_of($query->modelClass, TwActiveRecord::class)?CrudHelper::getMultiUploadFields($query->modelClass):[];
				?>
				<?php foreach($query->all() as $item){?>
				<div id="tr_<?= $tblCount?>_<?= $rowCount?>" class="tr flex-1">
					<div id="td_<?= $tblCount?>_<?= $rowCount?>_0" class="td flex flex-1"><?php echo $item->code?></div>
					<div id="td_<?= $tblCount?>_<?= $rowCount?>_1" class="td flex flex-1"><?php echo \Yii::t('twbp',$item->name)?></div>
					<?php $colCount = 3;
					foreach($uploadFields as $f){?>
					<div id="td_<?= $tblCount?>_<?= $rowCount?>_<?= $colCount ?>" class="td flex flex-1">
						<?php if($item->getFileType($f)=='image'){?>
							<?php echo $item->showFilePreview($f,['height'=>150])?>
						<?php }?>
					</div>
					<?php $colCount++;}?>
					<?php
					foreach($multiUploadFields as $f){?>
					<?php 
					$mediaBehavior = $item->getBehavior('mediafile');
					if($mediaBehavior==null){break;}
					$mediaCat = null;
					foreach($mediaBehavior->attributesConfig as $cnf){
					    if($cnf['attribute']==$f.'_ids'){
					        $mediaCat = $cnf['media_category'];break; //finding correct media category for the attribute
					    }
					}
					?>
					<div id="td_<?= $tblCount?>_<?= $rowCount?>_<?= $colCount ?>" class="td flex flex-1">
						<?php 
						if($mediaCat){
						$medias = $item->getMediaFiles($mediaBehavior->model_type,$mediaCat); if(count($medias)>0 && $medias[0]->getFileType('file_name')=='image'){?>
							<?php echo $medias[0]->showFilePreview('file_name',['height'=>150])?>
						<?php }?>
						<?php }?>
					</div>
					
					<?php $colCount++;}?>
				</div>
				<?php $rowCount++;}?>
			</div>
			<?php $tblCount++;}?>
		</div>
		<!-- /Tables -->
	</div>

	<!-- Right Col -->
	<div id="right_col" class="flex-1 d-flex flex-columns">
	<?php if($gStep){?>
		<div id="images" class="table flex-1 hide">
			<div id="box-images" class="tr align-items-stretch flex-rows">
				<div class="td flex-1">
				
				<div class="img-container" style="background-image:url('<?php
				echo $gStep->showFilePreview('design_canvas', ['height'=>''], true);?>')">&nbsp;</div>
				
				</div>
			</div>
		</div>
		
		<div id="hint" class="table table--red">
			<div class="table__title text-uppercase "><?php echo \Yii::t('twbp','hint')?></div>
			<div class="tr d-flex align-items-stretch h-100">
				<div
					class="td flex-1 d-flex align-items-center justify-content-center">
					<?php if($gStep){?>
					<h2 class="text text-center"><?php echo HtmlPurifier::process(\Yii::t('twbp',$gStep->description));?></h2>
					<?php }?>
				</div>
			</div>
		</div>
		<?php }?>
	</div>
    <?php elseif($model->getIframeUrl()): ?>
        <iframe src="<?=$model->getIframeUrl()?>" width="100%" height="80%" >Iframe not allowed</iframe>
        <script>
            document.autoUpdateStationView = false;
        </script>
    <?php endif; ?>
</div>
<?php }?>
<?php Pjax::end()?>