<?php
//Generation Date: 27-May-2021 08:48:15am
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\guide\models\GuideAsset $model
 * @var string $relatedTypeForm
 */

$this->title = Yii::t('twbp', 'Guide Asset') . ', ' . Yii::t('twbp', 'Create');
if(!$fromRelation) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('twbp', 'Guide Assets'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = Yii::t('twbp', 'Create');
}
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body guide-asset-create">

        <div class="clearfix crud-navigation">
            <div class="pull-left">
                <?= Html::a(
                    Yii::t('twbp', 'Cancel'),
                    $fromRelation ? urldecode($fromRelation) : \yii\helpers\Url::previous(\Yii::$app->controller->crudUrlKey),
                    ['class' => 'btn btn-default cancel-form-btn']
                ) ?>
            </div>
            <div class="pull-right">
                <?php                 \yii\bootstrap\Modal::begin([
                    'header' => '<h2>' . Yii::t('twbp', 'Information') . '</h2>',
                    'toggleButton' => [
                        'tag' => 'btn',
                        'label' => '?',
                        'class' => 'btn btn-default',
                        'title' => Yii::t('twbp', 'Information about possible search operators')
                    ],
                ]);?>

                <?= $this->render('@taktwerk-boilerplate/views/_shortcut_info_modal') ?>
                <?php  \yii\bootstrap\Modal::end();
                ?>
            </div>
        </div>

        <?= $this->render('_form', [
            'model' => $model,
            'inlineForm' => $inlineForm,
            'action' => $action,
            'relatedTypeForm' => $relatedTypeForm,
            'fromRelation' => $fromRelation,
                ]); ?>

    </div>
</div>
