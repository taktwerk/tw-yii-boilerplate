<?php
//Generation Date: 27-May-2021 08:48:15am
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\guide\models\GuideAsset $model
 * @var string $relatedTypeForm
 */

$this->title = Yii::t('twbp', 'Guide Asset') . ' #' . (is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey) . ', ' . Yii::t('twbp', 'Edit') . ' - ' . $model->toString();
if(!$fromRelation) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('twbp', 'Guide Assets'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => (string)is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('twbp', 'Edit');
}
?>
<div class="box box-default">
    <div
        class="giiant-crud box-body guide-asset-update">

        <div class="crud-navigation">

            <?= Html::a(
                Yii::t('twbp', 'Cancel'),
                $fromRelation ? urldecode($fromRelation) : \yii\helpers\Url::previous(\Yii::$app->controller->crudUrlKey),
                ['class' => 'btn btn-default cancel-form-btn']
            ) ?>
            <?= (!$fromRelation ? Html::a(
                '<span class="glyphicon glyphicon-eye-open"></span> ' . Yii::t('twbp', 'View'),
                [
                    'view',
                    'id' => $model->id,
                    'show_deleted'=>Yii::$app->request->get('show_deleted')
                ],
                ['class' => 'btn btn-default']
            ) : '') ?>
            <?=  \taktwerk\yiiboilerplate\widget\CustomActionDropdownWidget::widget(['model'=>$model]) ?>
            <div class="pull-right">
                <?php                 \yii\bootstrap\Modal::begin([
                'header' => '<h2>' . Yii::t('twbp', 'Information') . '</h2>',
                'toggleButton' => [
                'tag' => 'btn',
                'label' => '?',
                'class' => 'btn btn-default',
                'title' => Yii::t('twbp', 'Help')
                ],
                ]);?>

                <?= $this->render('@taktwerk-boilerplate/views/_shortcut_info_modal') ?>
                <?php  \yii\bootstrap\Modal::end();
                ?>
            </div>
        </div>

        <?php echo $this->render('_form', [
            'model' => $model,
            'inlineForm' => $inlineForm,
            'relatedTypeForm' => $relatedTypeForm,
            'fromRelation' => $fromRelation,
        ]); ?>

    </div>
</div>