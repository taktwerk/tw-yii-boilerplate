<?php
//Generation Date: 27-May-2021 08:48:18am
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use yii\helpers\Inflector;
use taktwerk\yiiboilerplate\components\ClassDispenser;

/**
 * @var yii\web\View $this
 * @var taktwerk\yiiboilerplate\modules\guide\models\GuideCategory $model
 * @var boolean $useModal
 */

$this->title = Yii::t('twbp', 'Guide Category') . ' #' . (is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey) . ', ' . Yii::t('twbp', 'View') . ' - ' . $model->toString();
if(!$fromRelation) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('twbp', 'Guide Categories'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => (string)is_array($model->primaryKey)?implode(',',$model->primaryKey):$model->primaryKey, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('twbp', 'View');
}
$basePermission = Yii::$app->controller->module->id . '_' . Yii::$app->controller->id;

Yii::$app->controller->renderCustomBlocks(Yii::$app->controller::POS_MAIN);

?>
<div class="box box-default">
	<div class="giiant-crud box-body"
		id="guide-category-view">

		<!-- flash message -->
        <?php if (Yii::$app->session->getFlash('deleteError') !== null) : ?>
            <span class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= Yii::$app->session->getFlash('deleteError') ?>
            </span>
        <?php endif; ?>
        <?php if (!$useModal) : ?>
            <div class="clearfix crud-navigation">
			<!-- menu buttons -->
			<div class='pull-left'>
			<?php if (!$model->deleted_at!=null) : ?>
                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_update'
                    ) && $model->editable() ?
                        Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span> ' . Yii::t('twbp', 'Edit'),
                            [
                                'update',
                                'id' => $model->id,                                 'fromRelation' => $fromRelation,
                                'show_deleted'=>Yii::$app->request->get('show_deleted')
                            ],
                            ['class' => 'btn btn-info']
                        )
                    :
                        null
                    ?>
                     <?php if(method_exists($model,'getUploadFields')  && Yii::$app->getUser()->can(
                        $basePermission . '_recompress'
                    )){
                    	$fields = $model->getUploadFields();
                    	$isCompressible = false;
                    	$compressibleFileTypes = $model::$compressibleFileTypes;
                    	foreach($fields as $field){
                        	if(in_array($model->getFileType($field),$compressibleFileTypes)){
                            	$isCompressible = true;
                            	break;
                        	}
                    	}
                    if($isCompressible){	
                       echo Html::a(
                            '<span class="glyphicon glyphicon-compressed"></span> ' . Yii::t('twbp', 'Recompress'),
                            ['recompress', 'id' => $model->id, 'fromRelation' => $fromRelation],
                            ['class' => 'btn btn-warning','title'=>Yii::t('twbp', 'Adds a compression job for this item\'s files')]
                        );}
                   
                    }?>

                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_create'
                    ) ?
                        Html::a(
                            '<span class="glyphicon glyphicon-copy"></span> ' . Yii::t('twbp', 'Copy'),
                            ['create', 'id' => $model->id, 'fromRelation' => $fromRelation],
                            ['class' => 'btn btn-success']
                        )
                    :
                        null
                    ?>
                    <?php endif; ?>
                    <?= Yii::$app->getUser()->can(
                        $basePermission . '_create'
                    ) ?
                        Html::a(
                            '<span class="glyphicon glyphicon-plus"></span>
				' . Yii::t('twbp', 'New'), ['create', 'fromRelation' =>
				$fromRelation], ['class' => 'btn btn-success create-new'] ) : null
				?>
			</div>
			<div class="pull-right">
			<?php if (!$model->deleted_at!=null) : ?>
                    <?= \taktwerk\yiiboilerplate\widget\CustomActionDropdownWidget::widget(['model'=>$model]) ?>
                    <?php if(Yii::$app->controller->crudMainButtons['listall']): ?>
                    <?= Html::a(
                        '<span class="glyphicon glyphicon-list"></span> '. Yii::t('twbp', 'List {model}',
                            ['model' => \Yii::t('twbp','Guide Categories')]                        ),
                        ['index'],
                        ['class' => 'btn btn-default']
                    ) ?>
                    <?php endif; ?>
            <?php endif; ?>                </div>
		</div>
        <?php endif; ?>
        <?php $this->beginBlock('taktwerk\yiiboilerplate\modules\guide\models\GuideCategory'); ?>

        <?php $viewColumns = [
                   [
            'attribute'=> 'id'
           ],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::attributeFormat
                [
                    'format' => 'html',
                    'attribute' => 'client_id',
                    'visible' => Yii::$app->getUser()->can('Authority'),
        
                    'value' => function ($model) {
                        $foreign = $model->getClient()->one();
                        if ($foreign) {
                            if (Yii::$app->getUser()->can('app_client_view') && $foreign->readable()) {
                                return Html::a($foreign->toString, [
                                    'client/view',
                                    'id' => $model->getClient()->one()->id,
                                ], ['data-pjax' => 0]);
                            }
                            return $foreign->toString;
                        }
                        return '<span class="label label-warning">?</span>';
                    },
                ],
[
        'attribute' => 'name',
        'format' => 'raw',
        'contentOptions'=>[
            'style'=>'white-space:pre-line;'
        ],
         
        'value' => function ($model) {
            if($model->name != strip_tags($model->name)){
                return \yii\helpers\StringHelper::truncate($model->name,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'name'],['class'=>'atr-val-a','data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'name'])]]),null,true);
            }else{
                return nl2br(\yii\helpers\StringHelper::truncate($model->name,500,Html::a('... see more',['view','id'=>$model->id,'atr'=>'name'],['class'=>'atr-val-a', 'data'=>['pjax'=>0,'atr-val-modal'=>true,'url'=> \yii\helpers\Url::toRoute(['view','id'=>$model->id,'atr'=>'name'])]])));
            }
        },
    ],
        ];

        // view columns overwrite
        $viewColumns = Yii::$app->controller->crudColumnsOverwrite($viewColumns, 'view');

        echo DetailView::widget([
        'model' => $model,
        'attributes' => $viewColumns
        ]); ?>

        
        <hr />

        <?=  ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\helpers\CrudHelper::class)::deleteButton($model, "twbp", ['id' => $model->id], 'view')?>

        <?php $this->endBlock(); ?>


        
        <?php $this->beginBlock('Guide Category Bindings'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsGuideCategoryBindings = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('guide_guide-category-binding_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('guide_guide-category-binding_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('guide_guide-category-binding_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/guide/guide-category-binding' . '/' . $action;
                        $params['GuideCategoryBinding'] = [
                            'guide_category_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'guide-category-binding'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'guide_id',
                    'value' => function ($model) {
                        if ($rel = $model->getGuide()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'guide/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\guide\models\GuideCategoryBinding::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsGuideCategoryBindings = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGuideCategoryBindings, 'tab'):$columnsGuideCategoryBindings;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('guide_guide-category-binding_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Guide Category Binding'),
                [
                    '/guide/guide-category-binding/create',
                    'GuideCategoryBinding' => [
                        'guide_category_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getGuideCategoryBindings(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-guidecategorybindings',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsGuideCategoryBindings
        ])?>
        </div>
                <?php $this->endBlock() ?>


        <?php $this->beginBlock('Guide Category Groups'); ?>
        <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
        <div class="clearfix"></div>
        <div>
        <?php $columnsGuideCategoryGroups = [[
                    'class' => ClassDispenser::getMappedClass(taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can('guide_guide-category-group_view') ? '{view} ' : '') . (Yii::$app->getUser()->can('guide_guide-category-group_update') ? '{update} ' : '') . (Yii::$app->getUser()->can('guide_guide-category-group_delete') ? '{delete} ' : ''),
                    'urlCreator' => function ($action, $relation_model, $key, $index) use($model) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = '/guide/guide-category-group' . '/' . $action;
                        $params['GuideCategoryGroup'] = [
                            'guide_category_id' => $model->id
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => 'guide-category-group'
                ],
// generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
[
    'attribute' => 'id',
],
                // generated by taktwerk\yiiboilerplate\templates\crud\providers\RelationProvider::columnFormat
                [
                    'class' => \yii\grid\DataColumn::class,
                    'attribute' => 'group_id',
                    'value' => function ($model) {
                        if ($rel = $model->getGroup()->one()) {
                            return Html::a(
                                $rel->toString,
                                [
                                    'group/view',
                                    'id' => $rel->id,
                                ],
                                [
                                   'data-pjax' => 0
                                ]
                            );
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
];
$relatedModelObject = Yii::createObject(taktwerk\yiiboilerplate\modules\guide\models\GuideCategoryGroup::class);
$relationController = $relatedModelObject->getControllerInstance();
$columnsGuideCategoryGroups = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($columnsGuideCategoryGroups, 'tab'):$columnsGuideCategoryGroups;
echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can('guide_guide-category-group_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp','Guide Category Group'),
                [
                    '/guide/guide-category-group/create',
                    'GuideCategoryGroup' => [
                        'guide_category_id' => $model->id
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[ 
               'class'=>'grid-outer-div' 
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->getGuideCategoryGroups(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-guidecategorygroups',
                    ]
                ]
            ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            
            'columns' => $columnsGuideCategoryGroups
        ])?>
        </div>
                <?php $this->endBlock() ?>

<?php /* MAPPED MODELS RELATIONS - START*/?>
<?php 
$extraTabItems = [];
foreach(\Yii::$app->modelNewRelationMapper->newRelationsMapping as $modelClass=>$relations){
    if(is_a($model,$modelClass)){
        foreach($relations as $relName => $relation){
            $relationKey = array_key_first($relation[1]);
            $foreignKey= $relation[1][$relationKey];
            $relClass = $relation[0];
            $baseName = \yii\helpers\StringHelper::basename($relClass);
            $cntrler = Inflector::camel2id($baseName, '-', true);
            $pluralName= Inflector::camel2words(Inflector::pluralize($baseName));
            $singularName = Inflector::camel2words(Inflector::singularize($baseName));
            $basePerm =  \Yii::$app->controller->module->id.'_'.$cntrler;
            $this->beginBlock($pluralName);
?>
<?php 
$relatedModelObject = Yii::createObject($relClass::className());
$relationController = $relatedModelObject->getControllerInstance();
//TODO CrudHelper::getColumnFormat('\app\modules\guide\models\Guide', 'id');
?>
                    <?php if ($useModal !== true) : ?>
                    <?php endif; ?>
       	<div class="clearfix"></div>
        <div>
        <?php $relMapCols = [[
                    'class' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\ActionColumn::class),
                    'template' => (Yii::$app->getUser()->can($basePerm.'_view') ? '{view} ' : '') . (Yii::$app->getUser()->can($basePerm.'_update') ? '{update} ' : '') . (Yii::$app->getUser()->can($basePerm.'_delete') ? '{delete} ' : ''),
            'urlCreator' => function ($action, $relation_model, $key, $index) use($model,$relationKey,$foreignKey,$baseName,$relatedBaseUrl) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$relation_model->primaryKey()[0] => (string) $key];
                        $params[0] = $relatedBaseUrl . $action;
                        
                        $params[$baseName] = [
                            $relationKey => $model->{$foreignKey}
                        ];
                        $params['fromRelation'] = urlencode(Url::current(['fromRelation'=>null]));
                        
                        return $params;
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) { 
                             return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>'.\Yii::t('twbp', 'Delete'),
                                $url,
                                [
                                    'title' => \Yii::t('twbp', 'Delete'),
                                    'data-confirm' => \Yii::t('twbp', 'Are you sure you want to delete the related item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                    'controller' => $cntrler
                ],
                
        ];

        $relMapCols =array_merge($relMapCols,array_slice(array_keys($relatedModelObject->attributes),0,5));
        
        $relMapCols = is_array($relationController->crudColumnsOverwrite)? $relationController->crudColumnsOverwrite($relMapCols, 'tab'):$relMapCols;
        
        echo ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\grid\GridView::class)::widget([
            'layout' => '<div class="sowing-outer">'.(($useModal !== true && Yii::$app->getUser()->can($basePerm.'_create'))?Html::a(
                '<span class=\"glyphicon glyphicon-plus\"></span>' . Yii::t('twbp', 'New') .
                ' ' .
                \Yii::t('twbp',$singularName),
                [
                    '/'.$relationController->module->id.'/'.$cntrler.'/create',
                    $baseName => [
                        $relationKey => $model->{$foreignKey}
                    ],
                    'fromRelation' => urlencode(Url::current(['fromRelation'=>null]))
                ],
                ['class' => 'btn btn-success']
                ):'').'<span class="hidden-sm hidden-xs">{pager}</span>{summary}</div>{items}{pager}',
            'options'=>[
                'class'=>'grid-outer-div'
            ],
            'summary'=>'<span>Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}</span>',
            'dataProvider' => new \yii\data\ActiveDataProvider(
                [
                    'query' => $model->{'get'.$relName}(),
                    'pagination' => [
                        'pageSize' => 20,
                        'pageParam' => 'page-'.$relName,
                    ]
                ]
                ),
            'pager' => [
                'class'          => yii\widgets\LinkPager::class,
                'firstPageLabel' => \Yii::t('twbp', 'First'),
                'lastPageLabel'  => \Yii::t('twbp', 'Last')
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            'columns' =>$relMapCols
        ])?>
        </div>
                <?php $this->endBlock();
                $extraTabItems[] = [
                    'content' => $this->blocks[$pluralName],
                    'label' => '<small>' .
                    \Yii::t('twbp', $pluralName) .
                    '&nbsp;<span class="badge badge-default">' .
                    $model->{'get'.$relName}()->count() .
                    '</span></small>',
                    'active' => false,
                    'visible' =>
                    Yii::$app->user->can('x_'.$basePerm.'_see') && Yii::$app->controller->crudRelations(ucfirst($relName)),
                    ];
?>

<?php 
        }
    }
}
?>

                
<?php /* MAPPED MODELS RELATIONS - END*/ ?>
        <?= Tabs::widget(
            [
                'id' => 'relation-tabs',
                'encodeLabels' => false,
                'items' =>  array_filter(array_merge([
                    [
                        'label' => '<b class=""># ' . $model->id . '</b>',
                        'content' => $this->blocks['taktwerk\yiiboilerplate\modules\guide\models\GuideCategory'],
                        'active' => true,
                    ],
        (\Yii::$app->hasModule('guide'))?[
                        'content' => $this->blocks['Guide Category Bindings'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Guide Category Bindings') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getGuideCategoryBindings()->groupBy('guide_category_binding.id')->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_guide_guide-category-binding_see') && Yii::$app->controller->crudRelations('GuideCategoryBindings'),
                    ]:null,
        (\Yii::$app->hasModule('guide'))?[
                        'content' => $this->blocks['Guide Category Groups'],
                        'label' => '<small>' .
                            \Yii::t('twbp', 'Guide Category Groups') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getGuideCategoryGroups()->groupBy('guide_category_group.id')->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('x_guide_guide-category-group_see') && Yii::$app->controller->crudRelations('GuideCategoryGroups'),
                    ]:null,
                    [
                        'content' => ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\widget\HistoryTab::class)::widget(['model' => $model]),
                        'label' => '<small>' .
                            \Yii::t('twbp', 'History') .
                            '&nbsp;<span class="badge badge-default">' .
                            $model->getHistory()->count() .
                            '</span></small>',
                        'active' => false,
                        'visible' => Yii::$app->user->can('Administrator'),
                    ],
                ]
                ,$extraTabItems))
            ]
        );
        ?>
        <?= ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\RecordHistory::class)::widget(['model' => $model]) ?>
    </div>
</div>

<?php ClassDispenser::getMappedClass(\taktwerk\yiiboilerplate\modules\backend\assets\ShortcutsAsset::class)::register($this);
$js = <<<JS
$(document).ready(function() {
    var hash=document.location.hash;
	var prefix="tab_" ;
    if (hash) {
        $('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
    } 
    
    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash=e.target.hash.replace(	"#", "#" + prefix);
    });
});
JS;

$this->registerJs($js);

Yii::$app->controller->renderCustomBlocks(Yii::$app->controller::POS_END);
