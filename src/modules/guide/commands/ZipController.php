<?php

namespace taktwerk\yiiboilerplate\modules\guide\commands;

use taktwerk\yiiboilerplate\modules\guide\models\Guide;
use taktwerk\yiiboilerplate\commands\ZipController as BaseZipController;

/**
 * Task runner command for development.
 *
 * @author Sharaddha>
 */
class ZipController extends BaseZipController
{
    /**
     * Generate zip of any guide model
     * php yii guide/zip guide_id
     *
     * @param int $id
     * @param string $name_space
     * @param string $type
     * @param array $exclude_relations
     * @param array $additional_files
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidRouteException
     */
    public function actionGenerate($id, $type='processed',$name_space=null, $exclude_relations = [], $additional_files = [])
    {
        $model_namespace = '\\'.get_class(new Guide());
        parent::actionGenerate($id, $model_namespace, $type, $exclude_relations, $additional_files);
    }
}
