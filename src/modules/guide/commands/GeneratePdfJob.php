<?php

namespace taktwerk\yiiboilerplate\modules\guide\commands;

use app\components\Helper;
use taktwerk\yiiboilerplate\modules\guide\components\PdfHelper;
use taktwerk\yiiboilerplate\modules\guide\models\Guide;
use taktwerk\yiiboilerplate\modules\queue\commands\BackgroundCommandInterface;
use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use Exception;

class GeneratePdfJob implements BackgroundCommandInterface
{
    /**
     * Don't notify on success.
     * @var bool
     */
    public $send_mail = false;

    /**
     * @param QueueJob $queue
     * @return mixed|void
     * @throws Exception
     */
    public function run(QueueJob $queue)
    {
        $params = json_decode($queue->parameters, true);
        $guide_id = $params['guide_id'];
        $guide = Guide::findOne([Guide::tableName().'.id' => $guide_id]);

        try {
            if (!$guide) {
                $queue->status = QueueJob::STATUS_FAILED;
                $queue->error = 'Guide not found';
            } else {
                /**@var $guide Guide*/

                $pdf_data = PdfHelper::renderPdf($guide);

                /** Generate/save pdf file*/
                $result = Helper::savePdf($pdf_data['content'], $pdf_data['header'], $pdf_data['footer'], $pdf_data['directory_path'], $pdf_data['file_name']);

                /**cleanup tmp files*/
                Helper::cleanupLocalTempFiles(\Yii::$app->params['clean_up']);

                if ($result) {
                    $queue->status = QueueJob::STATUS_FINISHED;
                } else {
                    $queue->status = QueueJob::STATUS_FAILED;
                    $queue->error = 'Generating pdf error';
                }
            }
            $queue->save();
        } catch (Exception $e) {
            $queue->status = QueueJob::STATUS_FAILED;
            $queue->error = $e->getMessage();
            throw new Exception($queue->error);
        }
    }
}