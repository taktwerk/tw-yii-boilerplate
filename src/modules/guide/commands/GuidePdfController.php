<?php

namespace taktwerk\yiiboilerplate\modules\guide\commands;

use app\components\Helper;
use taktwerk\yiiboilerplate\modules\guide\components\PdfHelper;
use taktwerk\yiiboilerplate\modules\guide\models\Guide;
use yii\console\Controller;

/**
 * Task runner command for development.
 *
 * @author Sharaddha>
 */
class GuidePdfController extends Controller
{
    public $defaultAction = 'generate-guide-pdf';

    /**
     * @var bool add job or update cache directly
     * - 1: add job to `queue_job` table
     * - 0: directly update cache
     */
    public $job = true;

    /**
     * Generate pdf for all guides
     */
    const GENERATION_TYPE_ALL = "ALL";

    /**
     * Generate pdf hourly for updated guides
     */
    const GENERATION_TYPE_HOURLY = "HOURLY";

    /**
     * Generate pdf based on guide id
     */
    const GENERATION_TYPE_SPECIFIC = "SPECIFIC";

    /**
     * @param string $actionID
     * @return array|string[]
     */
    public function options($actionID)
    {
        $actionParams = [];
        $paramsByAction = [
            'generate-guide-pdf' => ['job'],
        ];

        if (isset($paramsByAction[$actionID])) {
            $actionParams = $paramsByAction[$actionID];
        }

        return array_merge(parent::options($actionID), $actionParams);
    }

    /**
     * Generate Guide PDF
     * php yii guide/guide-pdf/generate-guide-pdf ALL
     * php yii guide/guide-pdf/generate-guide-pdf SPECIFIC 3
     * php yii guide/guide-pdf/generate-guide-pdf
     *
     * Generate jobs for all guide one by one
     * php yii guide/guide-pdf/generate-guide-pdf ALL --job=1
     * php yii guide/guide-pdf/generate-guide-pdf SPECIFIC 3 --job=1
     * php yii guide/guide-pdf/generate-guide-pdf --job=1
     *
     * @param string $type generate guides based on type HOURLY, SPECIFIC and ALL
     * @param null $id generate guides based on guide id
     * @throws \Exception
     */
    public function actionGenerateGuidePdf($type=self::GENERATION_TYPE_HOURLY, $id=null)
    {
        $all_guides = [];
        /**
         * All guides to generate file
         */
        if($type==self::GENERATION_TYPE_SPECIFIC){
            $all_guides = Guide::find()
                ->where([
                    Guide::tableName().'.deleted_at' => null
                ])
                ->andWhere([Guide::tableName().'.id'=>$id])
                ->all();
        }elseif($type==self::GENERATION_TYPE_HOURLY){
            $all_guides = Guide::find()
                ->where([
                    Guide::tableName().'.deleted_at' => null
                ])
                ->andWhere([
                    'or',
                    ['>=', Guide::tableName().'.updated_at', date('Y-m-d H:i:s', strtotime("-1 hour", time()))],
                    ['>=', Guide::tableName().'.created_at', date('Y-m-d H:i:s', strtotime("-1 hour", time()))],
                ])
                ->all();
        }elseif($type==self::GENERATION_TYPE_ALL){
            $all_guides = Guide::find()
                ->where([
                    Guide::tableName().'.deleted_at' => null
                ])
                ->all();
        }

        foreach ($all_guides as $guide){
            /**@var $this GuidePdfController*/
            if($this->job){
                /**@var $guide Guide*/
                try {
                    $queueJob = $guide->findPdfGenerateQueueJobModel($guide->id);
                    if ($queueJob) {
                        $this->stdout("\tJob #{$queueJob->id} is already on queue for guide {$guide->short_name}.\n");
                    } else {
                        if ($guide->createPdfGenerateQueueJobModel($guide->id)) {
                            $this->stdout("\tJob for guide {$guide->short_name} added.\n");
                        } else {
                            $this->stdout("\tCould not add job for guide {$guide->short_name}.\n");
                        }
                    }
                } catch (\Exception $exception) {
                    $this->stdout("\tCould not add job for guide {$guide->short_name}. Error: {$exception->getMessage()}\n");
                }
            }else{
                /**@var $guide Guide*/

                $pdf_data = PdfHelper::renderPdf($guide);

                /** Generate/save pdf file*/
                Helper::savePdf($pdf_data['content'], $pdf_data['header'], $pdf_data['footer'], $pdf_data['directory_path'], $pdf_data['file_name']);

                /**cleanup tmp files*/
                Helper::cleanupLocalTempFiles(\Yii::$app->params['clean_up']);
            }
        }
    }

}
