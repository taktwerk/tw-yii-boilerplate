<?php

namespace taktwerk\yiiboilerplate\modules\guide\commands;


use yii\console\Controller;
use mikehaertl\shellcommand\Command;
use Yii;
use taktwerk\yiiboilerplate\modules\customer\models\ClientUser;
use taktwerk\yiiboilerplate\modules\customer\models\Client;


class SeedController extends Controller
{
    public $defaultAction = 'index';

    /**
     *
     */
    public function actionIndex()
    {
//        $userAdmin = new \app\models\User();
//        $userAdmin->email = 'guideradmin@taktwerk.ch';
//        $userAdmin->password = 'guider';
//        $userAdmin->username = 'guideradmin';
//        $userAdmin->fullname = 'Guider Admin';
//        $userAdmin->confirmed_at = time();
//        if (!$userAdmin->save()) {
//            dd($userAdmin->getErrors());
//        }
//
//        $user = new \app\models\User();
//        $user->email = 'guideruser@taktwerk.ch';
//        $user->password = 'guider';
//        $user->username = 'guideruser';
//        $user->fullname = 'Guider User';
//        $user->confirmed_at = time();
//        if (!$user->save()) {
//            dd($user->getErrors());
//        }
//
//        $auth = Yii::$app->getAuthManager();
//        $auth->assign($auth->getRole('GuiderAdmin'), $userAdmin->id);
//        $auth->assign($auth->getRole('GuiderViewer'), $user->id);
//
        // Create the guide client
        $client = new Client();
        $client->name = 'Guider';
        $client->identifier = 'GUIDERDEMO';
        $client->save();

        // Assign both users to the client
        $user = \app\models\User::find()->where(['username' => 'guideradmin'])->one();
        $clientUser = new ClientUser();
        $clientUser->user_id = $user->id;
        $clientUser->client_id = $client->id;
        $clientUser->save();

        $user = \app\models\User::find()->where(['username' => 'guideruser'])->one();
        $clientUser = new ClientUser();
        $clientUser->user_id = $user->id;
        $clientUser->client_id = $client->id;
        $clientUser->save();
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionClear()
    {
        $user = \app\models\User::find()->where(['username' => 'guideradmin'])->one();
        if ($user) {
            $user->delete();
        }
        $user = \app\models\User::find()->where(['username' => 'guideruser'])->one();
        if ($user) {
            $user->delete();
        }

        $client = Client::find()->where(['name' => 'Guider'])->one();
        $this->execute("DELETE FROM {{%guide}} WHERE client_id = '" . $client->id . "'");
        foreach ($client->clientUsers as $clientUser) {
            $clientUser->forceDelete();
        }
        $client->forceDelete();
    }
}