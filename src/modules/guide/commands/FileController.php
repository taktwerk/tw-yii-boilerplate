<?php
namespace taktwerk\yiiboilerplate\modules\guide\commands;

use taktwerk\yiiboilerplate\modules\guide\models\mobile\Feedback;
use yii;
use yii\console\Controller;
use taktwerk\yiiboilerplate\helpers\FileHelper;
use taktwerk\yiiboilerplate\modules\guide\models\GuideStep;


class FileController extends Controller
{

    public const COMPRESSED_FILE_POSTFIX = '_min';

    public $job = true;

    /**
     *
     * @param string $actionID
     * @return array|string[]
     */
    public function options($actionID)
    {
        $actionParams = [];
        $paramsByAction = [
            'compress-guide-step' => [
                'job'
            ]
        ];
        
        if (isset($paramsByAction[$actionID])) {
            $actionParams = $paramsByAction[$actionID];
        }
        
        return array_merge(parent::options($actionID), $actionParams);
    }

    public function actionCompressGuideStep($id = null)
    {
        $this->compressModelFile(GuideStep::class,$id);
    }

    public function actionCompressFeedback($id = null)
    {
        $this->compressModelFile(Feedback::class);
    }

    protected function compressModelFile(
        $modelName,
        $id = null,
        $fileAttribute = 'attached_file',
        $videoExtension = 'mp4'
    )
    {
        if (! empty($id)) {
            $model = $modelName::findOne([$id]);
            if (! empty($model)) {
                if ($this->job) {
                    try {
                        $queueJob = $model->findCompressQueueJobModel($model, $fileAttribute);
                        if ($queueJob) {
                            echo PHP_EOL . "\tJob #{$queueJob->id} is already on queue for {$model->tableName()} id:{$model->id}.\n";
                        } else {
                            $jobQueue = $model->createCompressQueueJobModel($model, $fileAttribute);

                            if ($jobQueue) {
                                echo PHP_EOL . "\tJob for {$model->tableName()} id:{$model->id} added.\n";
                            } else {
                                echo PHP_EOL . "\tCould not add job for {$model->tableName()} id:{$model->id}.\n";
                            }
                        }
                    } catch (\Exception $exception) {
                        echo PHP_EOL . "\tCould not add job for {$model->tableName()} id:{$model->id}. Error: {$exception->getMessage()}\n";
                    }
                } else {
                    $result = FileHelper::compressFileByModel($model, $fileAttribute);
                    
                    if ($result['success'] == true) {
                        if ($this->job) {
                            echo PHP_EOL . \Yii::t('twbp', "Job added successfully");
                        } else {
                            echo PHP_EOL . \Yii::t('twbp', "Compressed successfully");
                        }
                    } else {
                        echo PHP_EOL . $result['error'];
                    }
                }
            } else {
                echo PHP_EOL . \Yii::t('twbp', "No {$model->tableName()} items is found");
            }
        } else {
            $query = $modelName::find();
            $error = [];

            foreach ($query->batch() as $models) {
                foreach ($models as $model) {
                    if ($this->job) {
                        /**@var $model Guide*/
                        try {
                            $queueJob = $model->findCompressQueueJobModel($model, $fileAttribute);
                            if ($queueJob) {
                                $this->stdout("\tJob #{$queueJob->id} is already on queue for {$model->tableName()} id:{$model->id}.\n");
                            } else {
                                if (empty($model->createCompressQueueJobModel($model, $fileAttribute))) {
                                    array_push($error, "\tCould not add job for {$model->tableName()} id:{$model->id}.\n");
                                }
                            }
                        } catch (\Exception $exception) {
                            array_push($error, "\tCould not add job for {$model->tableName()} id:{$model->id}. Error: {$exception->getMessage()}\n");
                        }
                    } else {
                        $result = FileHelper::compressFileByModel($model, $fileAttribute);
                        if($result['success']==false){
                            array_push($error, @$result['error']);
                        }
                    }
                }
            }
            if (empty($error)) {
                if ($this->job) {
                    echo PHP_EOL . \Yii::t('twbp', "Job added successfully");
                } else {
                    echo PHP_EOL . \Yii::t('twbp', "Compressed successfully");
                }
            } else {
                echo PHP_EOL . implode(',', $error);
            }
        }
    }

    
}