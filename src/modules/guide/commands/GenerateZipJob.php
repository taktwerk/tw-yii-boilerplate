<?php

namespace taktwerk\yiiboilerplate\modules\guide\commands;

use taktwerk\yiiboilerplate\modules\queue\models\QueueJob;
use Exception;
use taktwerk\yiiboilerplate\commands\GenerateZipJob as BaseGenerateZipJob;

class GenerateZipJob extends BaseGenerateZipJob
{

    /**
     * @param QueueJob $queue
     * @return mixed|void
     * @throws Exception
     */
    public function run(QueueJob $queue)
    {
        $params = json_decode($queue->parameters, true);
        $additional_files_with_cmd = isset($params['additional_files'])?$params['additional_files']:[];
        if($additional_files_with_cmd['cmd']){
            foreach ($additional_files_with_cmd['cmd'] as $file){
                shell_exec($file);
            }
        }
        parent::run($queue);
    }

}