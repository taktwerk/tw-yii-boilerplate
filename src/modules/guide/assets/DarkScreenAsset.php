<?php

namespace taktwerk\yiiboilerplate\modules\guide\assets;

use yii\web\AssetBundle;

class DarkScreenAsset extends AssetBundle
{
    public $sourcePath = '@vendor/taktwerk/yii-boilerplate/src/modules/guide/assets/web';
    public $css = [
        'css/reset.css',
        'css/base.css',
        'css/custom.css',
        'css/fontawesome-5.11.2/css/all.css',
        "https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap"
    ];
    public $js = [
        'js/moment.min.js',
        'js/production-line-main.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
