<?php

namespace taktwerk\yiiboilerplate\modules\guide\assets;

use yii\web\AssetBundle;

class ExploreGuideAsset extends AssetBundle
{
    public $sourcePath = '@vendor/taktwerk/yii-boilerplate/src/modules/guide/assets/web';
    public $css = [
        'css/google-fonts.css',
        'css/material-kit.css',
        'css/guide.css',
        'css/fontawesome-5.11.2/css/all.css',
        'css/ekko-lightbox.css',
        'css/material-kit.css',
        'css/guide.css?v=2',
        'css/touchnswipe.min.css',
        'css/slick/slick.css',
        'css/slick/slick-theme.css'
        
    ];
    public $js = [
        'js/afterglowplayer.js',
        'js/bootstrap.min.js?v=4.0.0',
        'js/popper.min.js?v=1.12.9',
        'js/plugins/typeahead.bundle.js?v=1.2.0',
        'js/core/bootstrap-material-design.min.js',
        'js/plugins/moment.min.js',
        'js/plugins/bootstrap-datetimepicker.js',
        'js/plugins/nouislider.min.js',
        'js/plugins/jquery.sharrre.js',
        'js/material-kit.js?v=2.0.4',
        'js/slick/1.8.1/slick.min.js',
        'js/plugins/hammer.min.js',
        'js/plugins/TweenMax.min.js',
        'js/plugins/jquery.touchnswipe.min.js'
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
