
$(document).ready(function() {
	document.autoUpdateStationView = true;
	var pjaxEnabledOnGuideContainer = true;
	function updateStationView()
	{
		if(Offline.state=='up' && document.autoUpdateStationView){
			if(pjaxEnabledOnGuideContainer){
				pjaxEnabledOnGuideContainer = false;
				$.pjax.reload({container: '#guide-digi-container', async: true,type:'post',data:{time:moment().add(1,'seconds').format("H:mm:ss")}});
			}
		}
	}

	// Time update function
	function clockUpdate() {
	    $("#clock").text(moment().format("H:mm:ss"));
	}
	
    // Clock update
    setInterval(clockUpdate, 1000);

    updateStationView();

    setInterval(updateStationView, 5000);

    $('#guide-digi-container').on('pjax:complete', function(event, data, status, xhr, options){
    	pjaxEnabledOnGuideContainer = true;
    	$('#loader').hide();
   });
    $('#guide-digi-container').on('pjax:start', function() { $('#loader').show(); });
});
