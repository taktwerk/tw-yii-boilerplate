<?php

namespace taktwerk\yiiboilerplate\modules\guide;

use yii\base\BootstrapInterface;
use taktwerk\yiiboilerplate\components\MenuItemInterface;
use taktwerk\yiiboilerplate\components\TwModule;
class Module extends TwModule implements BootstrapInterface,MenuItemInterface
{

    public $controllerNamespace = 'taktwerk\yiiboilerplate\modules\guide\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function bootstrap($app)
    {
        // TODO: Implement bootstrap() method.

        // When in console mode, switch controller namespace to commands
        if ($app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'taktwerk\yiiboilerplate\modules\guide\commands';
        }
    }
    public function getMenuItems()
    {
        $items = [
            [
                'label' => \Yii::t('twbp', 'Guider'),
                'url' => ['/guide/guide-setting'],
                'icon' => 'fa fa-cog',
                'visible' => \Yii::$app->user->can('x_guide_guide-setting_see')
            ],
        ];
        return $items;
    }
}
