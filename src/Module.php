<?php


namespace taktwerk\yiiboilerplate;

use yii\base\Module as BaseModule;
use Yii;


class Module extends BaseModule
{
    /**
     * Version of the module
     */
    const VERSION = '1.0.86';
    /**
     * @var array the class map. How the container should load specific classes
     * @see Bootstrap::buildClassMap() for more details
     */
    public $classMap = [];
    /**
     * Init process used to add the config of this package to the app
     */
    public function init()
    {
        parent::init();
    }
}
