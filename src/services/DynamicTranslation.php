<?php

namespace taktwerk\yiiboilerplate\services;

use Yii;
use Illuminate\Support\Arr;
class DynamicTranslation
{
    /**
     * @return int
     */
    public function run(): int
    {
        $models = Arr::get(Yii::$app->params, 'translate-models', []);
        $translations = [];
        foreach ($models as $class => $key) {
            $model = new $class();
            foreach ($model->find()->all() as $row) {
                $translations[] = addslashes($row->{$key});
            }
        }

        if($translations) {
            $content = implode("');\nYii::t('twbp', '", $translations);
            $content = "
<?php
Yii::t('twbp', '" . $content . "');";
        }

        $path = Yii::getAlias('@app/views/_generated.php');
        file_put_contents($path, $content);

        return count($translations);
    }
}
