<?php

namespace taktwerk\yiiboilerplate\services;

use lajax\translatemanager\models\LanguageSource;
use Yii;
use taktwerk\yiiboilerplate\components\ClassDispenser;

class Scanner extends \lajax\translatemanager\services\Scanner
{
    /**
     * Scanning project for text not stored in database.
     *
     * @return int The number of new language elements.
     */
    public function run()
    {
        // Before calling the scanner, we want to make sure the dynamic strings are generated
        $translator = ClassDispenser::makeObject(DynamicTranslation::class);
        $translator->run();

        return parent::run();
    }
}
