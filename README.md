# taktwerk Yii2 Boilerplate
This is the official README of taktwerk's Yii2 Boilerplate.

The `legacy` branch is used for old projects that haven't been updated to the new `2amigos/usuario` user module. For everything else, use the `develop` branch.

## Development lifecycle
Develop directly in the vendor folder of your current project. 

    Make sure you don't have local changes in this boilerplate package before calling sync. They are lost otherwise!!

Call the sync script in this repository from the root folder of the project, eg:
    
    sh ../tw-yii-boilerplate/syncbp.sh

All changes are reflected in this repository now. Commit & push the boilerplate changes.

Update the boilerplate package in the project:

    composer update taktwerk/tw-yii-boilerplate


## Installation
### 1. Requirements
The project needs to start from a `taktwerk/yii-tw-starter-kit` app. It isn't compatible with `yii2-advanced-template`. Using the `starter-kit`, everything will already be set up.

### 2. Boilerplate
Add the Boilerplate to the composer.json file
```json
"require": [{
    "taktwerk/yii-boilerplate": "*"
}]
...
"repositories": [
	{
	  "type": "git",
	  "url": "git@bitbucket.org:taktwerk/tw-yii-boilerplate.git",
	  "reference": "package"
	},
    {
        "type": "git",
        "url": "https://bitbucket.org/taktwerk/tw-pages.git",
        "reference": "package"
    }
]
```

#### 2.1. Composer update
Run ```composer update``` in your project-root

### 3 Binding
You need to tell your project to use the Boilerplate in several places.

#### 3.1 config/main.php
Add the following value to the configuration of your app.

```php
'log' => [
    'targets' => [
        // Sentry
        [
            'class' => 'sentry\SentryTarget',
            'enabled' => getenv('SENTRY_ENABLED'),
            'levels' => ['error', 'warning'],
            'dsn' => getenv('SENTRY_DSN')

        ],
```
#### 3.2 config/assets-prod.php
```php
'bundles' => [
	\taktwerk\yiiboilerplate\assets\TwAsset::className()
]
```

#### 3.3 config/bootstrap.php
Add the following at the end of the file
```php
// classmap
Yii::$classMap['yii\base\ArrayableTrait'] = '@app/../vendor/taktwerk/yii-boilerplate/src/traits/ArrayableTrait.php';
Yii::$classMap['yii\helpers\ArrayHelper'] = '@app/../vendor/taktwerk/yii-boilerplate/src/helpers/ArrayHelper.php';
```

#### 3.4 Sentry Config (optional)
If you wish to activate Sentry on your new application, add the SENTRY_DSN variables in your `.env` file.

### 4. Migration
Run the migrations with following command in your project-root:
```bash
$ php yii migrate/up
```
> **SPECIAL** the behavior-columns like 'created_at', 'created_by', 'deleted_at', 'deleted_by' will be auto generated per default for every new created migration. You must not add these columns to your new createTable(/* .. */) Calls .

### 5. Domain for API and Backend
Create two domains local on your personal machine and in the virutal machine.

Edit your vagrant-nginx-Config under **config/nginx-config/sites/default.conf** and add following lines:
```bash
server {
   listen       80;
   listen       443 ssl;
   server_name  local.{your-project}.dev;
   root         /srv/www/{your-project}/web;
   include      /etc/nginx/nginx-wp-common.conf;
}
server {
   listen       80;
   listen       443 ssl;
   server_name  local.{your-project}.api;
   root         /srv/www/{your-project}/web;
   include      /etc/nginx/nginx-wp-common.conf;
}
```
#### 5.1 Add Host on Windows
1. Add following lines to your host-File located at: C:\windows\system32\drivers\etc\hosts
```bash
# taktwerk local dev
192.168.50.4 local.taktwerk-{your-project-name}.dev
# taktwerk local api
192.168.50.4 local.taktwerk-{your-project-name}.api
```
> Make sure to replace {your-project-name} with your project-id like for example 'pq-tool' or 'nassag'

##### 5.2 Add Host on Linux/Unix
@todo
### 6 Browser-Ready
Make sure to have pretty url enabled in your .env-File:
```bash
APP_PRETTY_URLS=1
```
Open your dev-Host as defined in 5 like for example:
```http://local.taktwerk-tw-boilerplate.dev```

## Usage
### 1 Login
Login into backend with the following credentials:
> username:  admin
> password: admin

### 2 Giiant Model & CRUD Generation
Open your gii-site like for example: 
```http://local.taktwerk-erp.dev/de/gii```

Here you have to create Models / CRUDs with the **Giiant Model** and **Giiant CRUD Generator**.
> **IMPORTANT**: Please follow our MySQL-Conventions for best CRUD-Generator Support!

### 3 Adding your controllers to the config
For your controllers to be detected by Yii, you need to add your them in the `src\config\api.php` urlManager->rules->controller array. Follow the config conventions already set as examples.

### 4 API Controllers
API Controllers will auto-generated once with Giiant CRUD Generator. You have to define a Controler-Module which is per default 'v1'. You can test your generated APIs with for example: ```[GET] http://local.taktwerk-erp.api/v1/person```

### 5 Schema Builder
When having problems with foreign key contraints while creating/migrating tables let foreign key queries run at the end by putting an empty file to the project root named:

    run_foreignkeys_at_end.txt
    
### 6 Google Analytics
In order to turn on Google Analytics on project, you need to set variable `GOOGLE_ANALYTICS_ID` on `.env` file. For example: 
```bash
GOOGLE_ANALYTICS_ID=UA-1234567-03  
```
After that you just need to insert the code in a layout or specific view file like the following:
```php
\taktwerk\yiiboilerplate\widget\GoogleAnalytics::widget();
``` 

### 7 Authentication via Adfs (auth0)
To turn on authentication via Adfs (it uses service https://auth0.com/), you need to:

- Set next variables: 
```
AUTH0_DOMAIN=dev-taktwerk.eu.auth0.com
AUTH0_CLIENT_ID=cLRfzRlhBFVW6zk7QvikVoqOPu9bvM16
AUTH0_CLIENT_SECRET=T4p2pMDUQ-ijl7CgcmYmUZXi5A6aS2pNYYzOvrD8O62kxp3ZEuyg_xJQKYhhB2QV
AUTH0_CALLBACK_URL=http://kssg-v2-dev.devhost.taktwerk.ch/user/security/login-adfs-callback
AUTH0_LOGOUT_RETURN_URL=http://kssg-v2-dev.devhost.taktwerk.ch
AUTH0_CONNECTION=KSSG-FSDBv2
AUTH0_CLAIM_DOMAIN=https://research.kssg.ch/
AUTH0_DEFAULT_USER_ROLE_AFTER_SIGN_UP=Viewer
```
- Add component `adfs` to your config:
```php
    'components' => [
        'adfs' => [
            'class' => \taktwerk\yiiboilerplate\modules\user\components\adfs\Adfs::class,
            'profileClass' => \app\models\ResearchProfile::class,
        ],
    ...
    ],
```
where:  
`profileClass` is class, which implements interface `taktwerk\yiiboilerplate\modules\user\components\ExternalProfileInterface`

### 8 Authentication via LDAP
To turn on authentication via LDAP, you need to:

- Set next variables: 
```
#Ldap
LDAP_FULLNAME_ATTRIBUTE=cn
LDAP_LOGIN_ATTRIBUTE=uid
LDAP_DN=ou=people,dc=planetexpress,dc=com
LDAP_DEFAULT_ROLE=Viewer
LDAP_DOMAIN_CONTROLLERS=tw-ldaptest.jcloud.ik-server.com
LDAP_BASE_DN=dc=planetexpress,dc=com
LDAP_ADMIN_USERNAME=cn=admin,dc=planetexpress,dc=com
LDAP_ADMIN_PASSWORD=GoodNewsEveryone
LDAP_PORT=11000
```
- Add component `ldap` to your config:
```php
    'components' => [
                'ldap' => [
                    'class' => \app\modules\user\components\ldap\Ldap::class,
                    'profileClass' => \app\models\ResearchProfile::class,
                    'providers' => [
                        'default' => [
                            // Connect this provider on initialisation of the LdapWrapper Class automatically
                            'autoconnect' => false,
                            // The config has to be defined as described in the Adldap2 documentation.
                            // https://github.com/Adldap2/Adldap2/blob/master/docs/configuration.md
                            'config' => [
                                'account_suffix' => "",
                                'domain_controllers' => [getenv('LDAP_DOMAIN_CONTROLLERS')],
                                // Your base DN. This is usually your account suffix.
                                'base_dn'               => getenv('LDAP_BASE_DN'),
                                // The account to use for querying / modifying users. This
                                // does not need to be an actual admin account.
                                'admin_username'        => getenv('LDAP_ADMIN_USERNAME'),
                                'admin_password'        => getenv('LDAP_ADMIN_PASSWORD'),
                                // To enable SSL/TLS read the docs/SSL_TLS_AD.md and uncomment
                                // the variables below
                                'port' => getenv('LDAP_PORT'),
                            ]
                        ],
                    ],
                ],
    ...
    ],
```
where:  
`profileClass` is class, which implements interface `taktwerk\yiiboilerplate\modules\user\components\ExternalProfileInterface`

### 9 Queue
Default job handler is Cron.
Set SUPERVISOR_QUEUE=1 in .env file for using supervisor jobs handler

#### Supervisor
##### Installation
###### For Linux
- `sudo apt install supervisor`
###### For MacOS:
- `brew install distribute`
- `sudo easy_install pip`
- `sudo pip install supervisor`
- `mkdir /usr/local/etc/supervisor.d`
- `cp /usr/local/etc/supervisord.ini /usr/local/etc/supervisord.conf`

##### Create project configuration file content example:
###### For Linux
`sudo nano /etc/supervisor/conf.d/project.conf`
###### For MacOS
`nano /usr/local/etc/supervisor.d/project.ini`

Content example:
```
[program:{YOUR_WORKER_NAME}]
process_name=%(program_name)s_%(process_num)02d
directory={{YOUR_PROJECT_DIRECTORY}}
command=php yii queue/process/index
autostart=true
autorestart=true
redirect_stderr=true
numprocs={{NUMBER_OF_PROCESSES}}
startsecs = 0
stdout_logfile={{YOUR_APP_LOG_FILE}}
```

##### Enable monitoring webserver:
###### For Linux
`sudo nano /etc/supervisor/supervisord.conf`
###### For MacOS
`sudo nano /usr/local/etc/supervisord.conf`

````
[inet_http_server]
port = 9001
username = user
password = pass
````

#### Start
- `supervisord`
- `sudo supervisorctl reread`
- `sudo supervisorctl update`
- `sudo supervisorctl start all`
- `supervisorctl status` (for checking)

## Development
- use the [flystem](https://github.com/creocoder/yii2-flysystem) filesystem adapters for file modification, also for local!

## 3D models
### Compress 3D file
For compression 3D files should be installed library:
`npm install -g gltf-pipeline`
- 3D model should be gltf file or should zip file archive with gltf and bin model and textures.
- after uploading 3D model to the server then will generate job for generating model to bin file.
- If file is zip then cdoe check is consist zip file gltf file. Then in generate job check this file again, parse this file and generate new gltf file with textures (add base64 file, instead of paths). And then generate bin file.


## Offline project update commands
php yii zipupdate/extract-file-from-general-setting-model  Execute zip file from General Setting (one argument - general setting id)
php yii zipupdate/update-from-zip-file                     Execute zip file (one argument - filePath)
php yii zipupdate/index (default)                          If Taktwerk BP code is exist in vendor (execute shell command offlineUpdateProject.sh) (first argument is generalSettingId, second - filePath. First get file from general setting. If not - get from filePath)
php yii zipupdate/taktwerk-index                           If Taktwerk BP code is exist in vendor (first argument is generalSettingId, second - filePath. First get file from general setting. If not - get from filePath)

### Just command (when can't execute php yii commands):
#### Using file from db:
sh d/offlineUpdateProject.sh --destination /usr/local/var/www/alexandria-kssg-v2/ --dbpassword 1@Roottt --dbuser root --dbname test_kssg --dbRowId 1
#### Using file:
sudo sh d/offlineUpdateProject.sh --destination /usr/local/var/www/alexandria-kssg-v2/ --filePath ~/Downloads/vendor.zip

### How bring a new model offline to mobile client at the backend side. On the example of `feedback` table.
- Add to src/config/api.php in modules.sync.models models which will participate in synchronization
- For each model, you can make your own find method and toArray method, which returns a response.
    - toArray:
        - for deleted_at, updated_at, created_at, local_deleted_at, local_updated_at, local_created_at is exists methods from trait MobileDateTrait to get utc date in integer format.
        - for original files url is exist method `$this->getFilePathByAttribute('attached_file')` from MobileFileTrait
        - for thumbname is exist method `$this->getThumbFileName('attached_file')` from UploadTrait
        - for thumb_attached_file_path is exist method `$this->getAbsoluteUrl($this->getThumbFileUrl('attached_file'))` from MobileFileTrait and UploadTrait
- About pushing data to server:
    - For controllers that have a batch action, add to the modelClass property the class name with which data will be written to the database. Most often, this is the same model as for synchronization.
    - add behaviors, verbs and actions methods to controller. See exsmple in taktwerk\yiiboilerplate\modules\v1\controllers\GuideStepController.
    - add in config/api.php to urlManager->rules->controller 'v1/{uri-model-name}'
    - add to mobile model this load method:
        `public function load($data, $formName = null)
        {
            $scope = $formName === null ? $this->formName() : $formName;

            if ($scope === '' && !empty($data)) {
                $this->setAttributes($data);

                return true;
            } elseif (isset($data[$scope])) {
                $this->setAttributes($data[$scope]);

                return true;
            }

            return false;
        }`
### How to make read & write apis of a model
-Create a controller class extending the \taktwerk\yiiboilerplate\rest\TwActiveController in the v1 module
	and set the property $modelClass value to the model's class on which you want to perform the read write operations e.g
	 
```php
	<?php
	namespace app\modules\v1\controllers;
	use taktwerk\yiiboilerplate\rest\TwActiveController;
	
	class PanelSettingController extends TwActiveController
	{
	    public $modelClass = 'app\modules\panel\models\PanelSetting';
	}
```
- In the UrlManager component add the new controller
```php
...,
'urlManager'   => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'pluralize' => false,
                    'controller' => [
                        // ADD CONTROLLERS LIKE THIS: --------------------
                        'v1/login',
                        'v1/panel-setting',
                        // -----------------------------------------------
                    ],
               	]
              ]
            ]
...
```
- To add alllow the api access according to RBAC you'll need to add the below  'access' behavior in the controller, And then assign the action permissions to the roles. Permissions will be like v1_panel-setting_index, v1_panel-setting_create, v1_panel-setting_view. 
```php
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => \yii\filters\AccessControl::class,
                    'rules' => [
                        [ 
                            'allow' => true,
                            'matchCallback' => function ($rule, $action) {
                                return \Yii::$app->user->can($this->module->id . '_' . $this->id . '_' .$action->id,['route' => true]);
                            },
                        ]
                    ]
                ]
            ]
        );
    }
```
- Then simply access the rest api using `v1/panel-setting`, `v1/panel-setting/{id}` endpoints